<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait GrowCycleFilterable
{
    /**
     * @param Builder $query
     * @param int $growCycleId
     * @return Builder
     */
    public function scopeGrowCycleId($query, int $growCycleId)
    {
        return $query->where('grow_cycle_id', $growCycleId);
    }
}
