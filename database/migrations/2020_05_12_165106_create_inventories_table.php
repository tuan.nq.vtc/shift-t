<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'inventories',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('type_id')->index();
                $table->uuid('parent_id')->nullable();
                $table->uuid('state_category_id')->index()->nullable();
                $table->uuid('room_id')->index()->nullable();
                $table->uuid('subroom_id')->index()->nullable();
                $table->uuid('strain_id')->index()->nullable();
                $table->nullableUuidMorphs('product');
                $table->nullableUuidMorphs('source');
                $table->boolean('is_lotted')->default(false);
                $table->boolean('for_extraction')->default(false);
                $table->string('uom');
                $table->unsignedTinyInteger('qa_status')->default(0);
                $table->unsignedTinyInteger('sell_status')->default(0);
                $table->json('info')->nullable();
                $table->double('qty_on_hand', 10, 2)->default(0);
                $table->double('qty_allocated', 10, 2)->default(0);
                $table->double('qty_on_hold', 10, 2)->default(0);
                $table->double('net_weight', 10, 2)->nullable();
                // S2S Trace sync[Create/Update/Delete]
                $table->string('sync_code')->nullable()->index();
                $table->unsignedTinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('type_id')->references('id')->on('inventory_types');
                $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
                $table->foreign('subroom_id')->references('id')->on('subrooms')->nullOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
