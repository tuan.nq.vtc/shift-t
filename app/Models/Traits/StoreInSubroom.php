<?php

namespace App\Models\Traits;

use App\Models\Subroom;
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo};

trait StoreInSubroom
{
    public function subroom(): BelongsTo
    {
        return $this->belongsTo(Subroom::class);
    }

    public function scopeSubroomIds(Builder $query, ...$roomIds): Builder
    {
        return $query->whereIn($this->getTable().'.subroom_id', $roomIds);
    }

    public function scopeSubroomId(Builder $query, ...$roomId): Builder
    {
        return $query->whereIn($this->getTable().'.subroom_id', $roomId);
    }

    public function scopeSubroom(Builder $query, ...$roomId): Builder
    {
        return $this->scopeSubroomId($query, $roomId);
    }

    public function scopeOrderBySubroom(Builder $query, $direction): Builder
    {
        $subroomTable = $this->subroom()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($subroomTable, $this->getTable().'.subroom_id', '=', $subroomTable.'.id')
            ->orderBy($subroomTable.'.name', $direction);
    }
}
