<?php

namespace App\Cqrs\Values;

use App\Cqrs\Base\Valuable;

class AnalysisTypeResult extends Valuable
{
    /**
     * @var string|null
     */
    protected $code;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var
     */
    protected $result;

    /**
     * @var
     */
    protected $isRequired = false;

    /**
     * @var string[]
     */
    protected $mapping = [
        'code|string' => 'code',
        'name|string' => 'name',
        'result' => 'result',
        'isRequired|bool' => 'is_required'
    ];

    public function __construct(array $data)
    {
        $this->map($data);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'code' => $this->code,
            'name' => $this->name,
            'result' => $this->result,
            'isRequired' => $this->isRequired,
        ];
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getIsRequired(): bool
    {
        return $this->isRequired;
    }
}
