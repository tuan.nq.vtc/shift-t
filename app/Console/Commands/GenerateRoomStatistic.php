<?php


namespace App\Console\Commands;


use App\Models\Room;
use App\Services\RoomStatisticService;
use Illuminate\Console\Command;

class GenerateRoomStatistic extends Command
{
    /**
     * @var string
     */
    protected $description = "Statistic the all room or specific room of Trace system";
    /**
     * @var string
     */
    protected $signature = 'trace:generate-room-statistic {--id= : enter the id value to execute for a specific room or all value to statistic all room}';

    /**
     * @var RoomStatisticService
     */
    private RoomStatisticService $roomStatisticService;

    public function __construct(RoomStatisticService $roomStatisticService)
    {
        parent::__construct();
        $this->roomStatisticService = $roomStatisticService;
    }

    public function handle()
    {
        $executionStartTime = microtime(true);
        $this->info('Begin statistic the all room of Trace system');

        $id = $this->option('id');
        if (!$id) {
            Room::all()->each(fn(Room $room) => $this->roomStatisticService->generateRoomStatistic($room));
        } else {
            $this->roomStatisticService->generateRoomStatistic(Room::query()->findOrFail($id));
        }

        $time = microtime(true) - $executionStartTime;
        $this->info('End statistic the all room of Trace system - ' . $time . ' seconds');
    }
}
