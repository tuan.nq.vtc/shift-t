<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\AdditiveInventory;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class AdditiveInventoryService
 *
 * @package App\Services
 *
 * @method AdditiveInventory|Collection get($id)
 * @method AdditiveInventory|Builder getModel()
 */
class AdditiveInventoryService extends BaseService
{
    /**
     * @var array|string[]
     */
    protected array $relations = [
        'creator',
        'supplier'
    ];

    public function __construct(AdditiveInventory $additiveInventory)
    {
        parent::__construct($additiveInventory);
    }

    /**
     * @param array $data
     *
     * @return AdditiveInventory
     *
     * @throws GeneralException
     */
    public function create(array $data): AdditiveInventory
    {
        if (empty($data['user_id'])) {
            throw new GeneralException(__('exceptions.additive.create_failed'));
        }

        return parent::create(
            array_merge(
                $data,
                [
                    'created_by' => $data['user_id'],
                ]
            )
        );
    }

    /**
     * @param $additiveId
     * @param $licenseId
     *
     * @return Collection
     */
    public function getByAdditive($additiveId, $licenseId): Collection
    {
        return $this->newQuery()
            ->whereHas(
                'additive',
                function ($q) use ($licenseId) {
                    $q->where('license_id', $licenseId);
                }
            )
            ->where('additive_id', $additiveId)
            ->where('available_quantity', '>', 0)
            ->select(['id', 'supplier_id', 'available_quantity', 'uom'])
            ->get();
    }

    /**
     * @param AdditiveInventory $additiveInventory
     *
     * @return AdditiveInventory
     */
    public function calculateOnCreating(AdditiveInventory $additiveInventory): AdditiveInventory
    {
        $additiveInventory->total_price = $additiveInventory->unit_price * $additiveInventory->total_quantity;
        return $additiveInventory;
    }
}
