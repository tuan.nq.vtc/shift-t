<?php

namespace App\Synchronizations\Logic\Processes;

use App\Models\Trace;
use App\Synchronizations\Contracts\Http\ClientInterface;
use App\Synchronizations\Contracts\ProcessInterface;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Exceptions\Pull\SkipProcessItemException;
use App\Synchronizations\Resolvers\ClientResolver;
use Exception;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Staudenmeir\LaravelUpsert\Query\Builder;

abstract class BaseProcess implements ProcessInterface
{
    /**
     * @var Trace
     */
    protected Trace $trace;

    /**
     * @var TraceableModelInterface
     */
    protected TraceableModelInterface $traceableModel;

    protected ClientInterface $client;
    protected LoggerInterface $logger;
    protected array $payload;

    /**
     * @var Builder
     */
    protected Builder $builder;

    /**
     * @var array
     */
    protected $handlers = [];

    /**
     * BaseProcess constructor.
     * @param Trace $trace
     * @param array $payload
     * @throws Exception
     */
    public function __construct(Trace $trace, array $payload = [])
    {
        $this->trace = $trace;
        if (!$trace->getResource()) {
            throw new Exception('There is no resource for trace');
        }
        $this->traceableModel = $trace->getResource();
        $this->client = ClientResolver::resolve($trace->license);
        $this->logger = Log::channel('sync');
        $this->payload = $payload;
    }


    /**
     * @return array
     */
    protected function listHandlers(): array
    {
        return [];
    }

    public function Init()
    {
        $handlers = $this->listHandlers();

        if (!$handlers) {
            return;
        }

        foreach ($handlers as $handler) {
            $this->handlers[$handler] = app($handler);
        }
    }

    public function execute(string $action): void
    {
        if (!$action || !method_exists($this, $action)) {
            return;
        }

        call_user_func([$this, $action]);
    }
}
