<?php

namespace App\Http\Controllers;

use App\Cqrs\Commands\CreateQASampleCommand;
use App\Cqrs\Commands\RemoveQASampleCOACommand;
use App\Cqrs\Commands\UpdateQASampleCommand;
use App\Cqrs\Commands\UploadQASampleCOACommand;
use App\Http\Parameters\Criteria;
use App\Services\CreateQASampleService;
use App\Services\QASampleService;
use App\Services\RemoveQASampleCOAService;
use App\Services\UpdateQASampleResultService;
use App\Services\UploadQASampleCOAService;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\{JsonResponse, Request, Response};
use Laravel\Lumen\Http\ResponseFactory;
use Laravel\Lumen\Routing\Controller;

/**
 * Class QASampleController
 * @package App\Http\Controllers
 */
class QASampleController extends Controller
{

    /**
     * @var array
     */
    private $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param QASampleService $qaSampleService
     * @param Request $request
     * @return JsonResponse
     */
    public function list(QASampleService $qaSampleService, Request $request): JsonResponse
    {
        return response()->json(
            $qaSampleService->list(Criteria::createFromRequest($request))
        );
    }

    /**
     * @param QASampleService $qaSampleService
     * @param $id
     * @return JsonResponse
     */
    public function show(QASampleService $qaSampleService, $id): JsonResponse
    {
        return response()->json($qaSampleService->get($id));
    }

    /**
     * @param UpdateQASampleResultService $service
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function updateResults(UpdateQASampleResultService $service, Request $request, $id): JsonResponse
    {
        $request->merge(['id' => $id]);
        return $service->__invoke(new UpdateQASampleCommand(
            $this->validate($request, $this->config['update'] ?? [])
        ));
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request, CreateQASampleService $service): JsonResponse
    {
        return $service->__invoke(new CreateQASampleCommand(
            $this->validate($request, $this->config['create'] ?? [])
        ));
    }

    /**
     * @param UploadQASampleCOAService $service
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function uploadCOA(UploadQASampleCOAService $service, Request $request, $id): JsonResponse
    {
        return $service->__invoke(new UploadQASampleCOACommand(array_merge(
            $this->validate($request->merge(['qa_sample_id' => $id]), $this->config['upload'] ?? []),
            [
                'uploaded_file' => $request->file('file'),
            ]
        )));
    }

    /**
     * @param RemoveQASampleCOAService $service
     * @param Request $request
     * @param $coaId
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function removeCOA(RemoveQASampleCOAService $service, Request $request, $coaId)
    {
        return $service->__invoke(new RemoveQASampleCOACommand($this->validate(
                $request->merge(['coa_id' => $coaId]),
                $this->config['remove_coa'] ?? []))
        );
    }
}
