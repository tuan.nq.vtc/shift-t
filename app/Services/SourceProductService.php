<?php

namespace App\Services;

use App\Http\Parameters\Criteria;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\SourceCategory;
use App\Models\SourceProduct;
use App\Models\Taxonomy;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * @package App\Services
 */
class SourceProductService extends BaseService
{
    public array $relations = [
        'strain',
        'sourceCategory',
        'printLabel'
    ];

    public function __construct(SourceProduct $sourceProduct)
    {
        parent::__construct($sourceProduct);
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function getAvailableItems(Criteria $criteria): Collection
    {
        $categoryMappedStateIds = Taxonomy::query()
            ->whereNotNull('state_category_id')
            ->where('category_type', SourceCategory::class)
            ->get('category_id')->pluck(['category_id'])->toArray();

        return $this->newQuery(false)
            ->scopes($this->loadScopes($criteria->getFilters()))
            ->with($this->relations)
            ->whereIn('source_category_id', $categoryMappedStateIds)
            ->get();
    }

    public function list(Criteria $criteria): LengthAwarePaginator
    {
        if (Arr::get($criteria->getFilters(), 'is_map_state', false)) {
            $criteria->addFilter('source_category_id', Taxonomy::query()
                ->whereNotNull('state_category_id')
                ->where('category_type', SourceCategory::class)
                ->get('category_id')->pluck(['category_id'])->toArray());
        }

        return parent::list($criteria);
    }

    public function updateInventoryProductName(string $id)
    {
        $sourceProduct = $this->newQuery(true, false)->findOrFail($id);
        Inventory::query()
            ->where('product_id', $sourceProduct->id)
            ->update(['product_name' => $sourceProduct->name]);
    }
}
