<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraceDependencyTable extends Migration
{
    public function up()
    {
        Schema::create(
            'trace_dependency',
            function (Blueprint $table) {
                $table->uuid('trace_id')->index();
                $table->uuid('dependency_id')->index();

                $table->foreign('trace_id')->on('traces')->references('id')->cascadeOnDelete();
                $table->foreign('dependency_id')->on('traces')->references('id')->cascadeOnDelete();

                $table->primary(['trace_id', 'dependency_id'], 'trace_dependency_primary');
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('trace_dependency');
    }
}
