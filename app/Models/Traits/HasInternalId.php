<?php

namespace App\Models\Traits;

use App\Helpers\ModelHelper;

trait HasInternalId
{
    public function getSubLevelForInternalId()
    {
        return null;
    }

    protected static function bootHasInternalId(): void
    {
        static::creating(
            function (self $model): void {
                $modelClass = get_class($model);
                if (empty($model->getAttribute('internal_id'))) {
                    $internalId = ModelHelper::generateInternalId(
                        $modelClass,
                        $model->getAttribute('license_id'),
                        $model->getAttribute('account_holder_id'),
                        $model->getSubLevelForInternalId()
                    );

                    $model->setAttribute('internal_id', $internalId);
                }
            }
        );
    }
}
