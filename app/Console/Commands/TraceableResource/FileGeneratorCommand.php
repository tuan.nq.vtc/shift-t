<?php

namespace App\Console\Commands\TraceableResource;

use App\Facades\Portal;
use App\Integrations\FileGenerators\FileGenerator;
use App\Models\State;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class FileGeneratorCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'generate files for traceable resources';
    /**
     * @var string
     */
    protected $signature = 'trace:file-generator {--state= : The state resource(s) will be synced}';

    public function handle()
    {
        $state = $this->getState();

        if (is_null($state) || !$state->code) {
            $this->error('Invalid state code.');
            exit;
        }

        if (!$state->hasAsyncIntegration()) {
            $this->error('It is not possible to use a state without async integration');
            exit;
        }

        $this->info('Generating files for licenses with state: ' . $state->code);

        $licenses = Portal::fetchAllLicenses()->filter(
            fn($license) => ($license->state_code == $state->code && $license->is_test == 0)
        );
        foreach ($licenses as $license) {
            $this->info('Processing license: ' . $license->id);
            try {
                //To get account holder info, to avoid this, its needed to add account_holder in all endpoint in portal
                $license = Portal::fetchLicense($license->id);

                $fileGenerator = new FileGenerator($license);
                $fileGenerator->generate();
            } catch (Exception $e) {
                $this->error($e->getMessage());
            }
        }
    }

    protected function getState(): ?State
    {
        if ($stateCode = $this->option('state')) {
            $stateCode = Str::upper($stateCode);
            return State::where('code', $stateCode)->first();
        }

        return null;
    }
}
