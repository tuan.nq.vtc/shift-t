<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\HarvestGroup;
use App\Models\HarvestWetWeight;
use Throwable;

/**
 * Class HarvestWetWeightService
 * @package App\Services
 *
 * @method HarvestWetWeight get($id)
 */
class HarvestWetWeightService extends BaseService
{
    public function __construct(HarvestWetWeight $harvestWetWeight)
    {
        parent::__construct($harvestWetWeight);
    }

    /**
     * @param $id
     * @param array $data
     * @return HarvestWetWeight
     * @throws GeneralException
     */
    public function update($id, array $data): HarvestWetWeight
    {
        $wetWeight = $this->get($id);
        if ($wetWeight->harvest->group->status !== HarvestGroup::STATUS_OPEN) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'open']));
        }
        if (!$wetWeight->update($data)) {
            throw new GeneralException(__('exceptions.actions.update_failed'));
        }

        return $wetWeight;
    }

    /**
     * @param $id
     * @throws GeneralException
     */
    public function delete($id): void
    {
        $wetWeight = $this->newQuery()->findOrFail($id);
        if ($wetWeight->harvest->group->status !== HarvestGroup::STATUS_OPEN) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'open']));
        }
        try {
            $wetWeight->delete();
        } catch (Throwable $e) {
            throw new GeneralException(__('exceptions.actions.delete_failed'));
        }
    }
}
