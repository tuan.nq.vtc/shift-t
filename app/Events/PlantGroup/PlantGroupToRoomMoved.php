<?php

namespace App\Events\PlantGroup;

use App\Events\Event;
use App\Models\PlantGroup;

class PlantGroupToRoomMoved extends Event
{
    /**
     * @var PlantGroup
     */
    public PlantGroup $plantGroup;

    /**
     * @param PlantGroup $plantGroup
     */
    public function __construct(PlantGroup $plantGroup)
    {
        $this->plantGroup = $plantGroup;
    }
}
