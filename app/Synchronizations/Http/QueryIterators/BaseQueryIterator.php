<?php

namespace App\Synchronizations\Http\QueryIterators;

use App\Models\Trace;
use App\Synchronizations\Contracts\Http\ClientInterface;
use App\Synchronizations\Contracts\Http\QueryIteratorInterface;
use App\Synchronizations\Contracts\Http\ResourceInterface;
use App\Synchronizations\Resolvers\ClientResolver;
use App\Synchronizations\Resolvers\ResourceResolver;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

abstract class BaseQueryIterator implements QueryIteratorInterface
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @var ClientInterface
     */
    protected ClientInterface $httpClient;

    /**
     * @var ResourceInterface
     */
    protected ResourceInterface $httpResource;

    public function __construct(Trace $trace)
    {
        $this->httpClient = (new ClientResolver())->resolve($trace->license);
        $this->httpResource = (new ResourceResolver())->resolve($trace->license, $trace->getNewResource());
        $this->httpResource->build(ResourceInterface::ACTION_LIST);
        $this->logger = Log::channel('sync');
    }
}
