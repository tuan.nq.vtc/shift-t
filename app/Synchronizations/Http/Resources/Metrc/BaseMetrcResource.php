<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Http\Resources\BaseResource;

/**
 * Class MetrcBaseResource
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property TraceableModelInterface $traceableModel
 */
class BaseMetrcResource extends BaseResource
{
    public function __construct(TraceableModelInterface $traceableModel)
    {
        $licenseNumber = $traceableModel->license->code;
        if ('local' === app()->environment()) {
           $licenseNumber = env('CO_METRC_LICENSE_NUM');
        }
        $this->requestQuery = ['licenseNumber' => $licenseNumber];

        parent::__construct($traceableModel);
    }
}
