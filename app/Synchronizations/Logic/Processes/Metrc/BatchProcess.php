<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Exceptions\GeneralException;
use App\Models\{Batch, TraceableModel};
use Exception;
use Throwable;

/**
 * Class BatchProcess
 * @package App\Synchronizations\Logic\Resources\Metrc
 *
 * @property Batch|TraceableModel $traceableModel
 */
class BatchProcess extends BaseProcess
{
    /**
     * @throws Throwable
     */
    public function process()
    {
        switch ($this->traceableModel->type) {
            case Batch::TYPE_PROPAGATION:
                $this->processPropagation();
                break;
            default:
                $error = 'Invalid batch type';
                $this->logger->error($error);
                throw new Exception($error);
        }
    }

    /**
     * @throws Throwable
     */
    public function processPropagation(): void
    {
        $response = $this->client->getRequest()->get(
            'plantbatches/v1/active',
            ['licenseNumber' => $this->traceableModel->license->code]
        );

        /*if ($response->failed()) {
            throw new GeneralException('[METRC] Unable to get active plant batches');
        }*/
        // #todo: log error detail
        $response->throw();

        $this->traceableModel->updateWithoutEvents(['sync_code' => $this->getPropagationId($response->json())]);
    }

    /**
     * @param array $data
     *
     * @return string
     *
     * @throws Throwable
     */
    private function getPropagationId(array $data): string
    {
        foreach ($data as $item) {
            if (!empty($item['Name']) && $item['Name'] === $this->traceableModel->name) {
                return $item['Id'];
            }
        }

        throw new GeneralException('Propagation ID not found');
    }
}
