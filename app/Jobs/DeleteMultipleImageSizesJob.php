<?php

namespace App\Jobs;

use App\Facades\FileUpload;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Throwable;

class DeleteMultipleImageSizesJob extends Job
{
    public $queue = 'generate_image_sizes';
    private string $oldFile;
    private Model $model;

    /**
     * Create a new job instance.
     *
     * @return void
     * @throws Exception
     */
    public function __construct(Model $model, string $oldFile)
    {
        $this->model = $model;
        $this->oldFile = $oldFile;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        $imageSizeConfigs = config('image_sizes');
        if (empty($imageSizeConfigs[get_class($this->model)])) {
            return;
        }

        foreach ($imageSizeConfigs[get_class($this->model)] as $imgSize => $config) {
            if (FileUpload::existsResizedImage($this->oldFile, $imgSize)) {
                FileUpload::deleteResizedImage($this->oldFile, $imgSize);
            }
        }
    }

    /**
     * @param Throwable $exception
     * @return void
     */
    public function failed(\Throwable $exception)
    {
        Log::error("[StoreMultipleImageSizesJob] [queue:{$this->queue}] Delete images on " .
            get_class($this->model) . ': ' . $this->model->id .
            " was failed {$exception->getMessage()}");
    }
}
