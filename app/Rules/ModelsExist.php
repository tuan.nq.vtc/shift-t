<?php

namespace App\Rules;

use Spatie\ValidationRules\Rules\ModelsExist as BaseModelsExist;

class ModelsExist extends BaseModelsExist
{
    private $where;

    public function __construct(string $modelClassName, string $attribute = 'id', array $where = [])
    {
        parent::__construct($modelClassName, $attribute);
        $this->where = $where;
    }

    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        $this->modelIds = is_array($value) ? array_unique(array_filter($value)) : [$value];
        $query = $this->modelClassName::whereIn($this->modelAttribute, $this->modelIds);
        foreach ($this->where as $column => $condition) {
            if (is_callable($condition)) {
                $query->where($condition);
                continue;
            }
            if (is_array($condition)) {
                $query->whereIn($column, $condition);
                continue;
            }
            if (is_null($condition)) {
                $query->whereNull($column);
                continue;
            }
            $query->where($column, $condition);
        }

        return count($this->modelIds) === $query->count();
    }
}
