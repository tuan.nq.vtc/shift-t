<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Exceptions\GeneralException;
use App\Models\{Room, TraceableModel};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class LocationProcess
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 * @property Room|TraceableModel $traceableModel
 */
class LocationProcess extends BaseProcess
{
    /**
     * @throws Throwable
     */
    public function create()
    {
        try {
            DB::beginTransaction();
            $this->syncLocation();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        } catch (Throwable $throwable) {
            throw $throwable;
        }
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    private function syncLocation(): self
    {
        $activeLocationsResponse = $this->client->getRequest()->get(
            self::LOCATION_ENDPOINT . 'active',
            ['licenseNumber' => $this->traceableModel->license->code]
        );

        if ($activeLocationsResponse->failed()) {
            throw new GeneralException('Unable to fetch active locations from METRC regulator');
        }

        $activeLocations = $activeLocationsResponse->json();

        foreach ($activeLocations as $location) {
            if ($location['Name'] === $this->traceableModel->name) {
                $this->traceableModel->update(
                    ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'sync_code' => $location['Id']],
                );
                break;
            }
        }

        return $this;
    }

    /**
     * @param $item
     * @throws Exception
     */
    protected function processListItem($item): void
    {
        if (!($globalId = Arr::get($item, 'Id'))) {
            throw new Exception('ID Not Found');
        }
        $this->traceableModel->newInstance()->firstOrCreate(
            ['sync_code' => $globalId, 'license_id' => $this->traceableModel->license->id,],
            [
                'name' => Arr::get($item, 'Name'),
                'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                'synced_at' => Carbon::now(),
            ]
        );
    }
}
