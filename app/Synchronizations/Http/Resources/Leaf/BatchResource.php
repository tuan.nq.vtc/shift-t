<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use Exception;
use Throwable;

/**
 * Class Strain
 * @package App\Synchronizations\Http\Resources\Leaf
 */
class BatchResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_BATCH;
}
