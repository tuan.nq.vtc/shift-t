<?php
namespace App\Http\Controllers;

use App\Services\FileGeneratorService;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\{JsonResponse, Request, Response};

/**
 * Class FileGeneratorController
 * @package App\Http\Controllers
 */
class FileGeneratorController extends Controller
{
    private Request $request;
    private FileGeneratorService $service;

    public function __construct(FileGeneratorService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    public function setFilesUploadStatus($status): JsonResponse
    {
        $this->validate( $this->request, [
            'files' => [
                'required',
            ],
            'state' => [
                'required',
                'string',
            ], 
        ]);

        $files = $this->request->get('files');
        $archives = $this->request->get('archives') ?? [];
        $stateCode = $this->request->get('state');

        $response = $this->service->setFilesUploadStatus($stateCode, $status, $files, $archives);

        return response()->json([], $response ? Response::HTTP_OK : Response::HTTP_NO_CONTENT);
    }
}