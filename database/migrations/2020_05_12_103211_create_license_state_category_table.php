<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenseStateCategoryTable extends Migration
{
    public function up()
    {
        Schema::create(
            'license_state_category',
            function (Blueprint $table) {
                $table->uuid('license_id')->index();
                $table->uuid('state_category_id')->index();

                $table->foreign('state_category_id')->on('state_categories')->references('id')->cascadeOnDelete();

                $table->primary(['license_id', 'state_category_id'], 'license_state_category_primary');
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('license_state_category');
    }
}
