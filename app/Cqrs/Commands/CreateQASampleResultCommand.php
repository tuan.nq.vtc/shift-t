<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Executable;

class CreateQASampleResultCommand extends Executable
{
    /**
     * @var string|null
     */
    protected $qaSampleId;

    /**
     * @var string|null
     */
    protected $licenseId;

    /**
     * @var string|null
     */
    protected $analysisId;

    /**
     * @var float|null
     */
    protected $result;

    /**
     * @var string|null
     */
    protected $createdBy;

    /**
     * @var string[]
     */
    protected $mapping = [
        'qaSampleId|string' => 'qa_sample_id',
        'licenseId|string' => 'license_id',
        'analysisId|string' => 'analysis_id',
        'result|float' => 'result',
        'createdBy|string' => 'created_by',
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'qa_sample_id' => $this->qaSampleId,
            'license_id' => $this->licenseId,
            'analysis_id' => $this->analysisId,
            'result' => $this->result,
            'created_by' => $this->createdBy,
        ];
    }

    /**
     * @return string|null
     */
    public function getQaSampleId(): ?string
    {
        return $this->qaSampleId;
    }

    /**
     * @return string|null
     */
    public function getLicenseId(): ?string
    {
        return $this->licenseId;
    }

    /**
     * @return string|null
     */
    public function getAnalysisId(): ?string
    {
        return $this->analysisId;
    }

    /**
     * @return float|null
     */
    public function getResult(): ?float
    {
        return $this->result;
    }

    /**
     * @return string|null
     */
    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }
}
