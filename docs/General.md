# Cấu trúc CSDL Bamboo Trace


## Các nhóm bảng

### Company

Nhóm Company bao gồm:
- Company
- Client
    - Driver
    - Vehicle

### Plant

Nhóm Plant bao gồm các bảng liên quan đến chủng loại, nguồn gốc, việc chăm bón, sinh trưởng và nhân giống của cây

Bao gồm:

- Strain: Chủng loại cây, đc quản lý theo công ty
- Source: Nguồn gốc cây (gồm: Seed, Clone, Plant, Tissue - Từ hạt, Nhân bản, Cây, ~Bằng mô~)
- Grow status: Các trạng thái sinh trưởng.
- Grow cycle: Chu kỳ sinh trưởng.

- Plant: Cây
    - Quản lý theo công ty (**company_id**)
    - Phân biệt bằng mã thẻ (**code_tag**)
    - Tại phòng nào **Room** (**room_id**)
    - Trạng thái/Chu kỳ sinh trưởng (**grow_status_id**, **grow_cycle_id**)
    - Ngày trồng/thu hoạch


- Propagation: Nhân giống.
    - Quản lý theo công ty (**company_id**)
    - Phân biệt bằng mã thẻ (**code_tag**)
    - Tại phòng nào **Room** (**room_id**)
    - Mã chủng loại (**strain_id**)
    - Nhân giống từ cây nào **Plant** (**mother_id**, **mother_code** tương ứng với **id**, **code_tag** trong bảng **plants**)
    - Tại phòng nào **Room** (**room_id**)
    - Mã Nguồn gốc (**source_type**)
    - Số lượng (**quantity**)
    - Dự kiến kết thúc chu trình sống vào lúc (mdk->Murder Death Kill) (**mdk_scheduled**)
    - Lý do (**mdk_reason**)
    - Thời điểm kết thúc chu trình sống (**mdk_date**)
    - Mã code của lô(**batch_code_tag**)
    - Id của lô nhân giống (**propagation_batch_id**)
    - Mã code của kho nguồn (từ kho nào đến) (**from_inventory_code_tag**)
    - Mã biên bản/phiếu kê khai đi kèm (**manifest_id**)
    - Đã đc lập biên bản (**manifested**)
    - Seed-2-Sales đang đc tạo (**s2s_create_in_progress**)
    - Seed-2-Sales đang đc sửa (**s2s_update_in_progress**)


- Feed Operation: Việc chăm bón
    - Quản lý theo công ty (**company_id**)
    - Loại chăm bón (**nutrient_id**, **nutrient_name**)
    - Tại phòng (**feed_room_id**)
    - Vào chu kỳ sinh trưởng (**feed_grow_cycle_id**)
    - Ghi chú/bình luận (**comment**)
    - Thời gian (**feed_date**)
- Feed Operation Source: Nguồn gốc phân bón
    - Hoạt động chăm bón **Feed Operation** (**feed_id**)
    - Từ kho phân bón **Nutrient Inventory**(**nutrient_inventory_id**)
    - Số lượng/Giá trị (**nutrient_quantity**, **nutrient_cost**)
- Feed Plant: Chi tiết chăm bón
    - Cây **Plant** (**plant_id**)
    - Hoạt động chăm bón **Feed Operation** (**feed_id**)
    - Số lượng/Giá trị (**quantity**, **cost**)
    - Ngày chăm bón (**feed_date**)
    - Ghi chú/bình luận (**comment**)


- IPM Product: Sản phẩm trừ sâu bệnh (Integrated Pest Management)
    - Thuộc công ty **Company** (**company_id**)
    - Tên SP (**ipm_product_name**)
    - Số đơn vị (**box_units**)
    - Đơn giá (**unit_price**)
    - Tên đơn vị (**unit_name**)
    - Mô tả (**description**)
    - Ẩn/hiện (**hidden**)
- IPM Operation: Hoạt động trừ sâu    
    - Thuộc công ty **Company** (**company_id**)
    - Ngày thực hiện (**ipm_date**)
    - Tên sp trừ sâu (**ipm_product_name**)
    - Mã sp trừ sâu (**ipm_product_id**)
    - Thực hiện tại phòng (**ipm_room_id**)
    - Thực hiện vào chu kỳ phát triển (**ipm_grow_cycle_id**)
    - Ghi chú/Bình luận (**comment**)
- IPM Operation Source:
    - Thuộc công ty **Company** (**company_id**)
    - Mã hoạt động trừ sâu (**ipm_id**)
    - Mã kho sp trừ sâu (**ipm_product_inventory_id**)
    - Số lượng (**ipm_quantity**)
    - Giá (**ipm_cost**)
- IPM Plant: Trừ sâu bệnh trên cây
    - Thuộc công ty **Company** (**company_id**)
    - Mã cây (**plant_id**)
    - Mã hoạt động trừ sâu (**ipm_id**)
    - Số lượng (**quantity**)
    - Giá trị (**cost**)
    - Thời gian (**ipm_date**)
    - Ghi chú (**comment**)
- IPM Product Inventory Log: Nhật ký kho sp trừ sâu bệnh
    - Mã sp trừ sâu (**ipm_product_id**)
    - Tên hoạt động trừ sâu (**operation_type**)
    - Thông tin họat động (**operation_info**)
    - Số lượng (**quantity**)
    - Thời điểm (**operation_date**)


- Harvest: Thu hoạch
    - Thuộc công ty **Company** (**company_id**)
    - Tên (**harvest_name**)
    - Loại  (**harvest_type**)
    - Mã (**code_tag**)
    - Phòng **Room** (**harvest_room_id**)?
    - Phòng hoa **Room** (**flower**)?
    - Phòng khác **Room** (**other_room_id**)?
    - Trọng lượng hoa ướt/khô/thải loại (**flower_weight_wet**, **flower_weight_dry**, **flower_weight_waste**)
    - Trọng lượng rác loại (**waste_weight**)
    - Các chủng cây đc thu hoạch (**harvest_strains**)
    - Số lg cây trồng thêm (**no_plants_added**)
    - Số lg cây khả dụng (**no_plants_available**)
    - Dữ liệu thu hoạch (**harvest_data**)
    - Pha thu hoạch (**harvest_phase**)
    - Ngày thu hoạch/kết thúc  (**harvest_date**, **harvest_date_end**)
    - Trọng lượng sp khác ướt/khô/thải loại (**other_weight_wet**, **other_weight_dry**, **other_weight_waste**)

### Inventory

Nhóm Inventory bao gồm các bảng liên quan đến nhập/xuất kho

Bao gồm:

- Inventory Type Category: Danh mục chính kiểu loại kho
    -  Tên danh mục kiểu loại kho (**inventory_type_category_name**)
    -  Nhãn danh mục kiểu loại kho (**inventory_type_category_label**)
    -  Ẩn/hiện (**hidden**)
- Inventory Type Sub Category: Danh mục phụ kiểu loại kho
    -  Mã danh mục chính kiểu loại kho (**inventory_type_category_id**)
    -  Tên danh mục phụ kiểu loại kho (**inventory_type_subcategory_name**)
    -  Nhãn danh mục phụ kiê loại kho (**inventory_type_subcategory_label**)
    -  Ẩn/hiện (**hidden**)
- Inventory Type: Kiểu loại kho
    - Thuộc công ty **Company** (**company_id**)
    - Mã danh mục chính kiểu loại kho (**inventory_type_category_id**)
    - Mã danh mục phụ kiểu loại kho (**inventory_type_subcategory_id**)
    - Tên kiểu loại kho (**inventory_type_name**)
    - Khối lượng đơn vị tính theo gram (**weight_per_unit_in_grams**)
    - Đơn vị đo lường (unit of mesurement) UOM (_gm_ hoặc _ea_)(**uom**)
    - Ẩn/hiện (**hidden**)
- Inventory Form: Form kho
    - Thuộc công ty **Company** (**company_id**)
    -  Mã kiểu loại kho **Inventory Type** (**inventory_type_id**)
    -  Tên form kho (**inventory_form_name**)
    -  Ẩn/hiện (**hidden**)
- Inventory: Kho
    - Thuộc công ty **Company** (**company_id**)
    - Mã kho (**code_tag**)
    - ID lô (**inventory_batch_id**)
    - ID kho vận **Inventory Transfer** (**transfer_id**)
    - Mã code kho chuyển đến (**from_inventory_code_tag**)
    - Mã lô nguồn (**src_lot**)
    - Là lô con (**is_sublot**)
    - Mã phiếu kê khai **Manifest** (**manifest_id**)
    - Mã vụ thu hoạch **Harvest** (**harvest_id**)
    - Mã đơn hàng (**order_id**)
    - Trọng lượng (**weight**)
    - Mã sản phẩm (**sales_order_product_id**)
    - Mã size sản phẩm(**sales_product_size_id**)
    - *?(**convert_op_id**)
    - Tên loại (**strain_name_type**)
    - Mã loại (**strain_name_id**)
    - Form kho **Inventory Form** (**inventory_form**)
    - Kiểu kho **Inventory Type** (**inventory_type**)
    - Loại hoa (**flower_type**)
    - Mã phòng **Room** (**room_id**)
    - Có kiểm định chất lượng (**has_qa**)
    - Lượng THCA (**thca**)
    - Lượng THC (**thc**)
    - Lượng CBDA (**cbda**)
    - Lượng CBD (**cbd**)
    - Tổng lượng CBD (**total_cbd**)
    - Tổng lượng THC (**total_thc**)
    - KQ gửi từ lab **Lab Result**(**lab_results_sent**)
    - KQ Lab (**lab_results_extra_fields**)
    - KQ Lab cập nhật (**updated_lab_results**)
    - Đc cập nhật KQ Lab (**has_updated_lab_results**)
    - KQ Lab ban đầu (**original_lab_results**)
    - Có KQ Lab tùy chọn (**has_custom_lab_results**)
    - Mã KQ Lab (**lab_result_code_tag**)
    - Trạng thái kiểm tra (**testing_status**)
    - Lượng trích xuất đủ điều kiện(**extraction_eligible**)
    - Lượng trích xuất đủ đk khi kiểm tra lại (**retest_eligible**)
    - Dữ liệu phụ(**extra_data**)
    - Loại chất thải (**waste_type**)
    - Mã xử lý chất thải (**waste_process_id**)
    - Mã vụ thu hoạch có chất thải (**waste_harvest_id**)
    - Mã MDK (**mdk_scheduled**)
    - Lý do MDK (**mdk_reason**)
    - Ngày MDK (**mdk_date**)
    - Tuân thủ quy định y tế (**medically_compliant**)
    - Khả dụng để đưa tới portal (**available_to_portal**)
- Inventory Transfer: Chuyển kho
    - Thuộc công ty **Company** (**company_id**)
    - Mã code chuyển kho (**code_tag**)
    - Kiểu biên bản (**manifest_type**)
    - Mã room **Room** (**manifest_room_id**)
    - Mã đơn đặt hàng (**sales_order_id**)
    - Mã lái xe (**driver_id**)
    - Mã lái xe thứ hai (**secondary_driver_id**)
    - Mã người vận chuyển (**transporter_id**)
    - Mã phương tiện (**vehicle_id**)
    - Mã MME của điểm đến (**destination_mme_id**)
    - Mã bang của cty (**state_company_id**)
    - Tên khách hàng (**client_name**)
    - Mã code MME (**mme_code_tag**)
    - License của khách hàng (**client_license**)
    - Trạng thái outbount (**outbound_status**)
    - Tổng tiền hóa đơn (**invoice_total**)
    - Biên bản/phiếu lấy mẫu (**sample_only_manifest**)
    - Dừng nhiều điểm (**multi_stop**)
    - Xuất bản vào lúc (**published_at**)
    - Dự kiến xuất phát vào lúc (**est_departed_at**)
    - Dự kiến đến nơi vào lúc (**est_arrival_at**)
- Inventory Transfer Item: Hàng chuyển kho
    - Thuộc công ty **Company** (**company_id**)
    - Mã chuyển kho (**inventory_transfer_id**)
    - *Polymorph với **Inventory** hoặc **Propagation** hoặc **QA Sample** (**model_id**, **model**)
    - Mã code (**code_tag**)
    - Tên chủng cây (**strain_name**)
    - Kiểu loại kho (**type_name**)
    - Tên form kho (**form_name**)
    - Tên loại hoa (**flower_name**)
    - Số lượng (**quantity**)
    - Kiểu gốc (**source_type**)
    - Giá (**price**)
    - Là hàng mẫu (**is_sample**)
    - Là hàng để chiết xuất (**is_for_extraction**)
- Imported Inventory Transfer: Nhập kho
    - Thuộc công ty **Company** (**company_id**)
    - Global ID (**global_id**)
    - Tên MME của người gửi (**sender_mme_name**)
    - Global MME ID(**global_mme_id**)
    - Global User ID (**global_user_id**)
    - Chuyển từ Global MME ID (**global_from_mme_id**)
    - Chuyển tới Global MME ID (**global_to_mme_id**)
    - Chuyển từ Global User ID (**global_from_user_id**)
    - Chuyển tới Global User ID (**global_to_user_id**)
    - Chuyển tới Global Customer ID (**global_to_customer_id**)
    - Global User ID của người vận chuyển (**global_transporter_user_id**)
    - Global MME ID vận chuyển (**global_transporting_mme_id**)
    - Khoảng thời gian giữ (**hold_starts_at**, **hold_ends_at**)
    - Số lần sửa đổi (**number_of_edits**)
    - *?(**external_id**)
    - Vô hiệu hay ko (**void**)
    - Đc vận chuyển vào lúc (**transferred_at**)
    - Dự kiến chuyển vào lúc (**est_departed_at**)
    - Dự kiến đến nơi vào lúc(**est_arrival_at**)
    - Dừng nghỉ nhiều lần (có dừng nghỉ) (**multi_stop**)
    - Tuyến đường (**route**)
    - Số lần dừng nghỉ (**stops**)
    - Mô tả phương tiện vân chuyển (**vehicle_description**)
    - Năm sx phương tiện (**vehicle_year**)
    - Màu phương tiện (**vehicle_color**)
    - Số VIN của phương tiện (**vehicle_vin**)
    - Biển số phương tiện (**vehicle_license_plate**)
    - Ghi chú (**notes**)
    - Biên bản vân chuyển (bản kê khai) (**transfer_manifest**)
    - Kiểu biên bản (kiểu kê khai)(**manifest_type**)
    - Trạng thái (**status**)
    - Kiểu loại (**type**)
    - Kiểu vận chuyển (**transfer_type**)
    - Kiểm tra Terpene (**test_for_terpenes**)
    - Tên người vận chuyển 1(**transporter_name1**)
    - Tên người vận chuyển 2(**transporter_name2**)
- Imported Inventory Transfers Items Snapshots: Chi tiết nhập kho
    - Thuộc công ty **Company** (**company_id**)
    - ID chuyển kho **Imported Inventory Transfer** (**imported_inventory_transfer_id**)
    - Mã chuyển kho **Imported Inventory Transfer** (**imported_inventory_transfer_code_tag**)
    - Mã (**code_tag**)
    - *Polymorph với **Inventory** hoặc **Propagation** (**model_id**, **model**)
    - Đc chấp nhận thuộc chủng loại **Strain** (**accepting_as_strain_id**)
    - Đc chấp nhận tới phòng **Room** (**accepting_as_room_id**)
    - Đc chấp nhận tới phòng với tên **Room** (**accepting_room_name**)
    - Đc chấp nhận với mã form kho **Inventory Form** (**accepting_as_inventory_form**)
    - Đc chấp nhận với tên form kho (**accepting_as_inventory_form_name**)
    - Đc chấp nhận với mã kiểu kho(**accepting_as_inventory_type**)
    - Đc chấp nhận với tên kiểu kho(**accepting_as_inventory_type_name**)
    - Đc chấp nhận với tên model (**accepting_model_name**)
    - Giá nhận vào (**received_price**)
    - Mô tả hàng đến (**incoming_description**)
    - Hàng đến là hàng mẫu (**incoming_is_sample**)
    - Kiểu hàng mẫu của hàng mẫu(**incoming_sample_type**)
    - Số lượng hàng đến(**incoming_qty**)
    - Số lượng hàng đến đc nhận(**incoming_received_qty**)
    - Giá hàng đến(**incoming_price**)
    - Đơn vị đo lường (**incoming_uom**)
    - Kiểm tra lại hàng đến(**incoming_retest**)
    - Hàng đến dành cho chiết xuất(**incoming_is_for_extraction**)
    - Hàng đến dành cho việc nhân giống (**incoming_propagation_source**)
    - Tên kho hàng đến (**incoming_inventory_name**)
    - Kiểu trung gian của hàng đến (**incoming_intermediate_type**)
    - Tên chủng của hàng đến (**incoming_strain_name**)
    - Mã Global MME của hàng đến (**incoming_global_mme_id**)
    - Mã Global của kho hàng đến (**incoming_global_inventory_id**)
    - Mã Global của xuất kho hàng đến (**incoming_global_inventory_transfer_id**)
    - Mã Global của kiểu kho hàng đến (**incoming_global_inventory_type_id**)


### Manifest

Nhóm Manifest bao gồm các bảng liên quan đến biên bản **Propagation**(nhân giống), **QA Samples**(lấy mẫu kiểm định), **Inventory**
(Nhập/xuất kho), **Sales Order** (Đơn hàng), 

Bao gồm:

- Manifest:
    - Thuộc công ty **Company** (**company_id**)
    - Tên (**manifest_name**)
    - Mã lái xe (**driver_id**)
    - Mã người vận chuyển (**transporter_id**)
    - Mã phương tiện (**vehicle_id**)
    - Mã khách hàng (**client_id**)
    - Mã bang của cty (**state_company_id**)
    - Tên khách hàng (**client_name**)
    - Mã giấy phép (**license_id**)
    - Trạng thái outbound (**outbound_status**)
    - Trạng thái inbound (**inbound_status**)
    - Tổng tiền hóa đơn (**invoice_total**)
- Manifests Inbound
    - Thuộc công ty **Company** (**company_id**)
    - Mã outbound (**outbound_id**)
    - Tên (**manifest_name**)
    - Dữ liệu (**manifest_data**)
    - Trạng thái outbound (**outbound_status**)
    - Trạng thái inbound (**inbound_status**)
- Manifests Outbound
    - Thuộc công ty **Company** (**company_id**)
    - Mã thẻ (**code_tag**)
    - Tên (**manifest_name**)
    - Dữ liệu (**manifest_data**)
    - Thời gian xuất phát (**departure_time**)
    - Thời gian đến (**arrival_time**)
    - Trạng thái outbound (**outbound_status**)
    - Trạng thái inbound (**inbound_status**)
    - **s2s_create_in_progress
    - **s2s_update_in_progress


### Sales

Nhóm Sales bao gồm các bảng liên quan đến biên bản **Propagation**(nhân giống), **QA Samples**(lấy mẫu kiểm định), **Inventory**
(Nhập/xuất kho), **Sales Order** (Đơn hàng), 

- Sales Unit Size
    - Thuộc công ty **Company** (**company_id**)
    - ID kiểu đợn vị bán hàng (**sales_unit_type_id**)
    - Tên đơn vị kích thước (**sales_unit_size_name**)
    - Tên hiển thị đv kích thước (**display_size_name**)
    - Số phần (**num_servings**)
    - Loại tiêu thụ (**consumed_types**)
    - Mô tả (**description**)
    - Trạng thái ẩn/hiện (**hidden**)

- Sales Unit Type
    - Thuộc công ty **Company** (**company_id**)
    - Tên kiểu đv bán hàng (**sales_unit_type_name**)
    - Số lượng tiêu thụ (**consumed_amount**)
    - Kiểu đơn vị tiêu thụ (**consumed_unit_type**)
    - Mô tả (**description**)
    - Trạng thái ẩn/hiện (**hidden**)

Sales Order Product
    - Thuộc công ty **Company** (**company_id**)
    - Dữ liệu kho từ portal (Lưu dạng JSON) (**sales_portal_inventory**)
    - ID của kho từ portal (**sales_portal_inventory_id**)
    - ID của đơn hàng (**sales_order_id**)
    - ID của sản phẩm (**sales_product_id**)
    - ID của size sản phẩm (**sales_product_size_id**)
    - Số lượng (**quantity**)
    - Đơn giá (**item_price**)
    - Ghi nợ (**credit**)
    - ?*EDU (**edu**)
    - ?*NS (**ns**)
    - Trạng thái ẩn/hiện (**hidden**)

- Sales Order
    - Thuộc công ty **Company** (**company_id**)
    - ID của đơn hàng từ portal (**sales_portal_order_id**)
    - Ngày đặt hàng (**order_date**)
    - ID khách hàng (**client_id**)
    - ID biên bản (**manifest_id**)
    - Trạng thái đơn hàng (**status**)
    - Trạng thái ẩn/hiện (**hidden**)
    - Ghi nợ (**credit**)
    - Giấy phép (**license**)
    - ?*UBI (**ubi**)
    - Nhà bán lẻ (**retailer**)
    - Prefix đơn hàng tự động (Tạo prefix cho mã đơn hàng) (**auto_so_prefix**)
    - Số đơn hàng tự động (Tạo số theo sau prefix của mã đơn hàng) (**auto_so_number**)

- Sales Product Line
    - Thuộc công ty **Company** (**company_id**)
    - Tên dòng SP (**sales_product_line_name**)
    - Mô tả (**description**)
    - Trạng thái ẩn/hiện (**hidden**)

- Sales Product Unit
    - Thuộc công ty **Company** (**company_id**)
    - Mã code (**code_tag**)
    - ID sản phẩm (**sales_product_id**)
    - ID size sản phẩm (**sales_product_size_id**)
    - ID  (**sales_stock_product_id**)
    -  (**sales_order_id**)
    -  (**sales_order_unit_price**)
