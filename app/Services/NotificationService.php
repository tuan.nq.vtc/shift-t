<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Facades\Portal;
use App\Models\{Notification, User};
use App\Notification\Contracts\NotificationInterface;
use Illuminate\Support\Enumerable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class NotificationService
 *
 * @package App\Services
 */
class NotificationService extends BaseService
{
    protected array $relations = [
        'userNotifications',
    ];

    public function __construct(Notification $notification)
    {
        parent::__construct($notification);
    }

    /**
     * @param NotificationInterface $notificationResource
     * @param User $actor
     * @param Enumerable $users
     * @param string $eventName
     * @param int $alarmType
     * @param bool $isSuccessResponse
     *
     * @return Notification|null
     * @throws \Throwable
     */
    public function prepareDataToAlert(
        NotificationInterface $notificationResource,
        User $actor,
        Enumerable $users,
        string $eventName,
        int $alarmType,
        bool $isSuccessResponse = true
    ): ?Notification {
        try {
            DB::beginTransaction();
            $data = [
                'title' => $this->getNotificationTitle($eventName, $notificationResource),
                'message' => $this->getNotificationMessage($eventName, $notificationResource, $actor),
                'is_success_response' => $isSuccessResponse,
                'alarm_type' => $alarmType,
            ];

            /** @var Notification $notification */
            $notification = $notificationResource->notifications()->make($data);
            $notification->changedNotificationResourceBy()->associate($actor);
            $notification->save();
            $users->each(
                fn(User $user) => $notification->userNotifications()->create(
                    [
                        'user_id' => $user->id,
                        'license_id' => $notificationResource->license_id,
                        'event_name' => $notificationResource->getTable() . '_' . $eventName,
                    ]
                )
            );
            DB::commit();
            return $notification;
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param string $eventName
     *
     * @return string
     * @throws GeneralException
     */
    public function getNotificationTitle(string $eventName, NotificationInterface $notificationResource): string
    {
        $module = Str::singular($notificationResource->getTable());
        $regulator = ucfirst($notificationResource->license->state->regulator) ?? '';
        switch ($eventName) {
            case Notification::EVENT_CREATE_ACTION_SYNC_COMPLETED:
                return __('notification.' . $module . '.sync_completed_title', ['regulator' => $regulator,]);
            case Notification::EVENT_UPDATE_ACTION_SYNC_COMPLETED:
                return __('notification.' . $module . '.update_sync_completed_title',  ['regulator' => $regulator,]);
            case Notification::EVENT_UPDATED:
                return __('notification.' . $module . '.updated_title');
            case Notification::EVENT_CREATE_ACTION_SYNC_FAILED:
                return __('notification.' . $module . '.sync_failed_title');
            default:
                throw new GeneralException(
                    __('exceptions.notification.invalid_event', ['model' => $module, 'event' => $eventName])
                );
        }
    }

    /**
     * @param string $eventName
     * @param NotificationInterface $notificationResource
     * @param User $actor
     *
     * @return array|string|null
     * @throws GeneralException
     */
    protected function getNotificationMessage(
        string $eventName,
        NotificationInterface $notificationResource,
        User $actor
    ) {
        $module = Str::singular($notificationResource->getTable());
        switch ($eventName) {
            case Notification::EVENT_CREATE_ACTION_SYNC_COMPLETED:
                return __(
                    'notification.' . $module . '.sync_completed_message',
                    [
                        'regulator' => ucfirst($notificationResource->license->state->regulator) ?? '',
                        'label' => $notificationResource->getNotificationLabel()
                    ]
                );
            case Notification::EVENT_UPDATE_ACTION_SYNC_COMPLETED:
                return __(
                    'notification.' . $module . '.update_sync_completed_message',
                    [
                        'regulator' => ucfirst($notificationResource->license->state->regulator) ?? '',
                        'label' => $notificationResource->getNotificationLabel()
                    ]
                );
            case Notification::EVENT_UPDATED:
                return __(
                    'notification.' . $module . '.updated_message',
                    [
                        'first_name' => $actor->first_name,
                        'last_name' => $actor->last_name,
                        'label' => $notificationResource->getNotificationLabel()
                    ]
                );
            case Notification::EVENT_CREATE_ACTION_SYNC_FAILED:
                return __(
                    'notification.' . $module . '.sync_failed_message',
                    [
                        'regulator' => ucfirst($notificationResource->license->state->regulator) ?? '',
                    ]
                );
            default:
                throw new GeneralException(
                    __(
                        'exceptions.notification.invalid_event',
                        ['model' => $module, 'event' => $eventName]
                    )
                );
        }
    }

    /**
     * @param string|null $organizationId
     * @param string|null $accountHolderId
     * @return Enumerable
     * @throws GeneralException
     */
    public function getReceiversBy(?string $organizationId, ?string $accountHolderId): Enumerable
    {
        if ($organizationId) {
            return collect(Portal::allOrganizationUsers($organizationId))->map(fn($userData) => new User($userData));
        }

        if ($accountHolderId) {
            return collect(Portal::allAccountHolderUsers($accountHolderId))->map(fn($userData) => new User($userData));
        }

        throw new GeneralException('Receivers not found');
    }

}
