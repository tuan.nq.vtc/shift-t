<?php

namespace App\Services;

use App\Http\Parameters\Criteria;
use App\Models\{PrintLabel, PrintLabelItem, PrintLabelTemplate};
use Illuminate\Database\Eloquent\{Builder, Collection};
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class PrintLabelService
 * @package App\Services
 *
 * @method PrintLabel|Builder getModel()
 */
class PrintLabelService extends BaseService
{
    protected array $relations = ['items',];

    /**
     * @var PrintLabelItem
     */
    private $printLabelItemModel;

    public function __construct(
        PrintLabel $printLabel,
        PrintLabelItem $printLabelItem
    ) {
        parent::__construct($printLabel);
        $this->printLabelItemModel = $printLabelItem;
    }

    public function getAvailableItems(): Collection
    {
        return $this->newQuery()->where('status', PrintLabel::STATUS_ENABLED)->get();
    }

    /**
     * @param string $id
     * @param boolean $withRelation
     *
     * @return Model|Collection
     *
     * @throws ModelNotFoundException
     */
    public function get(string $id, $withRelation = true)
    {
        return $this->newQuery()->with(['items', 'template'])->findOrFail($id);
    }

    /**
     * @param array $data
     *
     * @return PrintLabel
     *
     * @throws Throwable
     */
    public function create(array $data): PrintLabel
    {
        try {
            DB::beginTransaction();
            $items = $data['items'] ?? [];
            unset($data['items']);
            $label = $this->newQuery()->create($data);
            $label->items()->createMany($items);
            DB::commit();

            return $label->load($this->relations);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param $printLabelId
     * @param array $data
     *
     * @return PrintLabel
     *
     * @throws Throwable
     */
    public function update($printLabelId, array $data): PrintLabel
    {
        try {
            DB::beginTransaction();

            /** @var PrintLabel $label */
            $label = $this->get($printLabelId);

            if (!empty($data['items'])) {
                $this->syncItems($label, collect($data['items']));
            }

            $label->update($data);

            DB::commit();

            return $label->load($this->relations);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    private function syncItems(PrintLabel $label, \Illuminate\Support\Collection $items)
    {
        $existedItems = $items->filter(fn($item) => optional($item)['id']);
        $newItems = $items->filter(fn($item) => !optional($item)['id']);

        $trashItems = $existedItems->count()
            ? $label->items()->whereNotIn('id', $existedItems->pluck('id')) : $label->items();
        $trashItems->delete();

        $existedItems->map(fn($item) => $this->printLabelItemModel->find($item['id'])->update($item));
        $label->items()->createMany($newItems);
    }

    public function getDetail($id)
    {
        return $this->newQuery()
            ->with(['items'])
            ->findOrFail($id);
    }

    public function getAll(Criteria $criteria): Collection
    {
        $scopes = $criteria->getFilters();
        $select = ['*'];
        if (!empty($criteria->getSelect())) {
            $select = $criteria->getSelect();
        }
        $query = $this->newQuery(false, false)
            ->scopes($this->loadScopes($scopes));
        return $query->get($select);
    }
}
