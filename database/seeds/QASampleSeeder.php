<?php

use App\Models\Manifest;
use App\Models\QASampleResult;
use App\Models\LabTestAnalysis;
use App\Models\QASample;
use App\Models\QASampleCoa;
use Illuminate\Database\Seeder;

class QASampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $labTestAnalysisIds = [];

        for ($i = 1; $i <= 5; $i++) {
            $labTestAnalysisIds[] = factory(LabTestAnalysis::class)->create()->id;
        }

        $arrManifest = ['', factory(Manifest::class)->create()->id];

        for ($i = 1; $i <= 50; $i++) {
            shuffle($arrManifest);
            $qaSample = factory(QASample::class)->create(['manifest_id' => $arrManifest[0]]);

            for ($j = 1; $j <= rand(1, 2); $j++) {
                factory(QASampleCoa::class)->create(['qa_sample_id' => $qaSample->id]);
            }

            foreach ($labTestAnalysisIds as $id) {
                factory(QASampleResult::class)->create([
                    'qa_sample_id' => $qaSample->id,
                    'analysis_id' => $id
                ]);
            }
        }
    }
}
