<?php

namespace App\Models\Traits;

use App\Models\{Batch, GrowCycle, PlantGroup, Propagation, Room, SeedToSale, State, Strain, TraceableModel};
use App\Synchronizations\Exceptions\Pull\PreSaveException;
use Illuminate\Support\{Arr, Carbon, Str};
use Throwable;

trait PullBatchHandler
{
    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            /*
           "id": "305394",
           "created_at": "2020-05-18 20:47:08",
           "updated_at": "2020-10-05 01:49:32",
           "mme_id": "3271",
           "user_id": "3774",
           "external_id": "1",
           "planted_at": "2020-05-18 00:00:00",
           "harvested_at": "0000-00-00 00:00:00",
           "batch_created_at": "2020-05-18 20:47:08",
           "created_by_mme_id": "0",
           "num_plants": "-7",
           "status": "closed",
           "qty_harvest": "0.00",
           "uom": "ea",
           "strain_id": "270377",
           "is_parent_batch": "1",
           "is_child_batch": "0",
           "type": "propagation material",
           "mother_plant_id": "0",
           "harvest_stage": "",
           "qty_accumulated_waste": "0.00",
           "qty_packaged_flower": "0.00",
           "qty_packaged_by_product": "0.00",
           "est_harvest_at": "0000-00-00 00:00:00",
           "packaged_completed_at": "0000-00-00 00:00:00",
           "area_id": "201570",
           "other_area_id": null,
           "flower_area_id": null,
           "origin": "seed",
           "source": "inhouse",
           "customer_id": "0",
           "qty_cure": "0.00",
           "plant_stage": "",
           "deleted_at": null,
           "flower_dry_weight": "0.00",
           "waste": "0.00",
           "other_waste": "0.00",
           "flower_waste": "0.00",
           "other_dry_weight": "0.00",
           "harvested_end_at": "0000-00-00 00:00:00",
           "flower_wet_weight": "0.00",
           "other_wet_weight": "0.00",
           "global_id": "WAJ1965.BA6JN6",
           "global_area_id": "WAJ1965.AR4BJ6",
           "area_name": "Propagation A",
           "global_mother_plant_id": null,
           "global_mme_id": "WASTATE1.MM2IV",
           "mme_name": "Bamboo_P/P",
           "mme_code": "J1965",
           "global_user_id": "WASTATE1.US2WU",
           "first_name": "BM",
           "last_name": "Dev",
           "global_strain_id": "WAJ1965.ST5SMH",
           "strain_name": "Strain X",
           "inventory_qty": "1.0000"
           */
            return [
                'global_id' => fn (self $instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'name' => 'name',
                'status' => fn (self $instance, $value) => $instance->setAttribute('status', 'closed' == $value ? 0 : 1),
            ];
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            /*
            "Id": 142402,
            "Name": "1A4FF0200000259000000192",
            "Type": "Seed",
            "LocationId": 125203,
            "LocationName": "Beginning Inventory",
            "LocationTypeName": "Default Location Type",
            "StrainId": 201903,
            "StrainName": "Beginning Inventory Kush",
            "PatientLicenseNumber": "",
            "UntrackedCount": 1,
            "TrackedCount": 1,
            "PackagedCount": 37,
            "HarvestedCount": 0,
            "DestroyedCount": 36,
            "SourcePackageId": null,
            "SourcePackageLabel": null,
            "SourcePlantId": 546004,
            "SourcePlantLabel": "1A4FF0200000259000004006",
            "SourcePlantBatchId": null,
            "SourcePlantBatchName": null,
            "PlantedDate": "2020-08-24",
            "LastModified": "2021-01-28T11:01:14-08:00"
            */
            return [
                'Id' => fn (self $instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'Name' => 'name',
            ];
        }

        // TODO: Implement getMappings() for METRC.

        return null;
    }

    /**
     * @return self|null
     *
     * @throws Throwable
     */
    public function preSave(): ?self
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            $leafBatchTypes = array_flip(Batch::getTypesByRegulator(State::REGULATOR_LEAF));
            $batchType = throw_unless(
                $leafBatchTypes[$this->info['type']] ?? null,
                new PreSaveException(__('exceptions.batches.invalid_fetched_type', ['type' => $this->info['type']]))
            );

            switch ($batchType) {
                case Batch::TYPE_PROPAGATION:
                    $this->createPropagation();

                    return $this;

                case Batch::TYPE_VEGETATION:
                    $this->createVegetation();

                    return $this;
                    // TODO: Implement for plant/harvest/intermediate/end product batches
                case Batch::TYPE_PRODUCTION:
                case Batch::TYPE_HARVEST:
                    // Return null to prevent saving harvest and intermediate batch
                    return null;
            }
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            $messageFormat = 'Cannot create propagation without %s';
            /*"StrainId": 201903,
            "StrainName": "Beginning Inventory Kush",*/
            $strainSyncCode = throw_unless(
                $this->info['StrainId'] ?? null,
                new PreSaveException(sprintf($messageFormat, 'strain'))
            );
            $strain = $this->firstOrNewResource(
                Strain::class,
                $strainSyncCode,
                empty($this->info['StrainName']) ? [] : ['name' => $this->info['StrainName']]
            );
            /*"LocationId": 125203,
            "LocationName": "Beginning Inventory",
            "LocationTypeName": "Default Location Type",*/
            $roomSyncCode = throw_unless(
                $this->info['LocationId'] ?? null,
                new PreSaveException(sprintf($messageFormat, 'room'))
            );
            $room = $this->firstOrNewResource(
                Room::class,
                $roomSyncCode,
                empty($this->info['LocationName']) ? [] : ['name' => $this->info['LocationName']]
            );
            /*"Type": "Seed",*/
            $sourceTypeName = throw_unless(
                $this->info['Type'] ?? null,
                new PreSaveException(sprintf($messageFormat, 'source type'))
            );
            $matchedSourceType = throw_unless(
                Arr::first(SeedToSale::SOURCE_TYPES, fn ($value, $key) => Str::lower($value) == Str::lower($sourceTypeName)),
                new PreSaveException(sprintf($messageFormat, 'source type'))
            );
            $this->setAttribute('info->source_type', $matchedSourceType);
            /*
            "UntrackedCount": 1,
            "TrackedCount": 1,
            "PackagedCount": 37,
            "HarvestedCount": 0,
            "DestroyedCount": 36,
            */
        }

        return $this;
    }

    protected function firstOrNewResource(string $class, string $syncCode, array $attributes = []): TraceableModel
    {
        return $class::firstOrNew(
            array_merge(
                ['sync_code' => $syncCode, 'license_id' => $this->license->id,],
                $attributes
            )
        );
    }

    /**
     * @return self|null
     *
     * @throws Throwable
     */
    public function postSave(): ?self
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
        }

        // TODO: Implement postSave() for METRC.

        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    public function createPropagation(): self
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            $messageFormat = 'Cannot create propagation without %s';
            // Handle strain
            $strainSyncCode = throw_unless(
                $this->info['global_strain_id'] ?? null,
                new PreSaveException(sprintf($messageFormat, 'strain'))
            );
            $strain = $this->firstOrNewResource(Strain::class, $strainSyncCode);
            // Handle room
            $roomSyncCode = throw_unless(
                $this->info['global_area_id'] ?? null,
                new PreSaveException(sprintf($messageFormat, 'room'))
            );
            $room = $this->firstOrNewResource(Room::class, $roomSyncCode);
            // Handle source type
            $sourceType = throw_unless(
                SeedToSale::getLeafSourceType($this->info['origin']),
                new PreSaveException(sprintf($messageFormat, 'source type'))
            );
            // Validate batch creation date from LEAF
            $date = $this->info['batch_created_at'] ?? null;
            $date = $date != '0000-00-00 00:00:00' ? $date : null;

            $propagation = Propagation::make(
                [
                    'license_id' => $this->license_id,
                    'name' => $this->name,
                    'mother_code' => $this->info['global_mother_plant_id'],
                    'source_type' => $sourceType,
                    'quantity' => (int)$this->info['num_plants'] > 0 ? (int)$this->info['num_plants'] : 0,
                    'date' => $date ? Carbon::createFromFormat('Y-m-d H:i:s', $date) : null,
                ]
            );
            // Associate plant group with strain
            // Check room existence. Stop saving propagation when room non-existed
            throw_unless(
                ($strain instanceof Strain && $strain->exists),
                new PreSaveException(sprintf($messageFormat, 'strain'))
            );
            $propagation->strain()->associate($strain);

            // Associate plant group with room
            // Check strain existence. Stop saving propagation when strain non-existed
            throw_unless(
                ($room instanceof Room && $room->exists),
                new PreSaveException(sprintf($messageFormat, 'room'))
            );
            $propagation->room()->associate($room);

            // Set UUID to id then do save new propagation
            $propagation->setAttribute($propagation->getKeyName(), $propagation->generateUuid());
            $propagation->saveWithoutEvents();

            $this->source()->associate($propagation);

            return $this;
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            // TODO: Block by Metrc APIs
            /*"Type": "Seed",*/
            $sourceTypeName = $this->info['Type'];
        }

        // TODO: Implement createPropagation() for METRC.

        return $this;
    }

    public function createVegetation(): self
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            $errorFormat = 'Cannot create vegetation without %s';
            // Handle strain
            $strainSyncCode = throw_unless(
                $this->info['global_strain_id'] ?? null,
                new PreSaveException(sprintf($errorFormat, 'strain'))
            );
            $strain = $this->firstOrNewResource(Strain::class, $strainSyncCode);
            // Handle room
            $roomSyncCode = throw_unless(
                $this->info['global_area_id'] ?? null,
                new PreSaveException(sprintf($errorFormat, 'room'))
            );
            $room = $this->firstOrNewResource(Room::class, $roomSyncCode);
            // Validate batch creation date from LEAF
            $date = $this->info['batch_created_at'] ?? null;
            $date = $date != '0000-00-00 00:00:00' ? $date : null;
            // Make plant group
            $plantGroup = PlantGroup::make(
                [
                    'license_id' => $this->license_id,
                    'name' => $this->name,
                ]
            );
            // Associate plant group with strain
            if ($strain instanceof Strain && $strain->exists) {
                $plantGroup->strain()->associate($strain);
            }
            // Associate plant group with room
            if ($room instanceof Room && $room->exists) {
                $plantGroup->room()->associate($room);
            }
            // Set UUID to id then do save new plant group
            $plantGroup->setAttribute($plantGroup->getKeyName(), $plantGroup->generateUuid());
            $plantGroup->saveWithoutEvents();
            // Associate plant group to morph on batch model
            $this->source()->associate($plantGroup);
            // Create a new grow cycle by cloning the plant group
            $growCycle = GrowCycle::create(
                [
                    'license_id' => $this->license_id,
                    'name' => $this->name ?? "Grow cycle of {$strain->name} planted {$date}",
                    'planted_at' => $date ? Carbon::createFromFormat('Y-m-d H:i:s', $date) : null,
                ]
            );
            // Attach grow cycle and plant group in many to many relation
            $growCycle->plantGroups()->attach($plantGroup);

            return $this;
        }

        // TODO: Implement createVegetation() for METRC.

        return $this;
    }
}
