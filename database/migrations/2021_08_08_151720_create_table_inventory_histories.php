<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInventoryHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_histories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('inventory_id')->index();
            $table->double('qty_current', 10, 2)->default(0);
            $table->double('qty_variable', 10, 2);
            $table->boolean('is_decrease')->default(true);
            $table->string('action')->comment('Action that causes the quantity of inventory to fluctuate, like: hold, create, create qa sample ...');
            $table->text('comment')->nullable();
            $table->string('reason')->nullable();

            $table->nullableUuidMorphs('source');
            $table->timestamps();

            $table->foreign('inventory_id')->references('id')->on('inventories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_histories');
    }
}
