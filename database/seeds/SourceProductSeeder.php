<?php

use App\Models\SourceCategory;
use App\Models\SourceProduct;
use App\Models\License;
use Illuminate\Database\Seeder;

class SourceProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = SourceCategory::all();
        $licenses = License::whereIn('account_holder_id', $categories->pluck('account_holder_id'))->get();
        foreach ($categories as $category) {
            factory(SourceProduct::class, 5)->create([
                'license_id' => $licenses->where('account_holder_id', $category->account_holder_id)->first()->id,
                'source_category_id' => $category->id
            ]);
        }
    }
}
