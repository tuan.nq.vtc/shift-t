<?php

use App\Models\SourceProduct;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceProductsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'source_products',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->string('sku')->index();
                $table->string('name')->index();
                $table->uuid('strain_id')->nullable()->index();
                $table->uuid('source_category_id')->nullable()->index();
                $table->uuid('print_label_id')->nullable()->index();
                $table->decimal('price')->nullable();
                $table->decimal('net_weight')->nullable();
                $table->string('uom', 15)->nullable();
                $table->integer('pieces')->nullable();
                $table->integer('quantity')->default(0);
                $table->text('description')->nullable();
                $table->text('disclaimer')->nullable();
                $table->unsignedTinyInteger('status')->default(SourceProduct::STATUS_ENABLED);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('strain_id')->references('id')->on('strains')->nullOnDelete();
                $table->foreign('source_category_id')->references('id')->on('source_categories')->nullOnDelete();
                $table->foreign('print_label_id')->references('id')->on('print_labels')->nullOnDelete();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('source_products');
    }
}
