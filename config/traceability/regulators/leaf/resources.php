<?php

return [
    \App\Models\Room::class => \App\Synchronizations\Http\Resources\Leaf\AreaResource::class,
    \App\Models\Strain::class => \App\Synchronizations\Http\Resources\Leaf\StrainResource::class,
    \App\Models\InventoryType::class => \App\Synchronizations\Http\Resources\Leaf\InventoryTypeResource::class,
    \App\Models\Batch::class => \App\Synchronizations\Http\Resources\Leaf\BatchResource::class,
    \App\Models\Plant::class => \App\Synchronizations\Http\Resources\Leaf\PlantResource::class,
    \App\Models\PlantGroup::class => \App\Synchronizations\Http\Resources\Leaf\PlantGroupResource::class,
    \App\Models\Propagation::class => \App\Synchronizations\Http\Resources\Leaf\PropagationResource::class,
    \App\Models\Harvest::class => \App\Synchronizations\Http\Resources\Leaf\HarvestResource::class,
    \App\Models\Disposal::class => \App\Synchronizations\Http\Resources\Leaf\DisposalResource::class,
    \App\Models\Inventory::class => \App\Synchronizations\Http\Resources\Leaf\InventoryResource::class,
    \App\Models\LabTest::class => \App\Synchronizations\Http\Resources\Leaf\LabTestResource::class,
    \App\Models\QASampleResult::class => \App\Synchronizations\Http\Resources\Leaf\QASampleResultResource::class,
];
