<?php

namespace App\Integrations\FileGenerators\Contracts;

use App\Models\License;

interface FileStrategyInterface
{
    public function __construct(License $license);

    public function getLicense(): License;

    public function setData(array $data): void;

    public function getContent(): string;

    public function getContentType(): string;

    public function getFilename(): string;
}
