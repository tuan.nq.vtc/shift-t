<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use Exception;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class Strain
 * @package App\Synchronizations\Http\Resources\Leaf
 */
class StrainResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_STRAIN;

    protected function create(): self
    {
        return parent::create()
            ->setBody(
                [
                    'strain' => [
                        [
                            'name' => $this->traceableModel->name,
                            'external_id' => $this->traceableModel->id,
                        ],
                    ],
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync strain update without LEAF GLOBAL_ID [sync_code]')
        );

        return parent::update()
            ->setBody(
                [
                    'strain' => [
                        'name' => $this->traceableModel->name,
                        'external_id' => $this->traceableModel->id,
                        'global_id' => $this->traceableModel->sync_code,
                    ],
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync strain deletion without LEAF GLOBAL_ID [sync_code]')
        );

        return parent::delete();
    }
}
