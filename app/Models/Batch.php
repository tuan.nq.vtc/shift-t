<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\PullBatchHandler;
use App\Models\Traits\Scope\{LicenseFilterable, NameFilterable};
use App\Models\Traits\WithoutTraceableModelEvents;
use App\Synchronizations\{Constants\LeafConstant, Contracts\TraceableModelInterface};
use Illuminate\Database\Eloquent\{Builder,
    Relations\BelongsTo,
    Relations\HasMany,
    Relations\HasManyThrough,
    Relations\MorphTo
};

/**
 * Class Batch
 * @package App\Models
 * @property-read Propagation|PlantGroup|Harvest $source
 * @property string sync_code
 */
class Batch extends BaseModel implements TraceableModelInterface
{
    use PullBatchHandler;
    use LicenseFilterable;
    use NameFilterable;
    use WithoutTraceableModelEvents;
    use HasInternalId;

    public const TYPE_PROPAGATION = 'propagation';
    public const TYPE_VEGETATION = 'vegetation';
    public const TYPE_HARVEST = 'harvest';
    public const TYPE_PRODUCTION = 'production';

    public const STATUS_CLOSED = 0;
    public const STATUS_OPEN = 1;

    protected $casts = [
        'info' => 'array'
    ];

    protected $fillable = [
        'name',
        'license_id',
        'source_id',
        'source_type',
        'parent_id',
        'status',
        'info',
        'sync_code',
    ];

    protected array $traceable = ['name'];

    /**
     * List of all batch types
     *
     * @return string[]
     */
    public static function getTypeList(): array
    {
        return [
            self::TYPE_PROPAGATION,
            self::TYPE_VEGETATION,
            self::TYPE_HARVEST,
            self::TYPE_PRODUCTION,
        ];
    }

    /**
     * Map list of batch types by vendor name (Leaf or Metrc)
     *
     * @param string $regulator
     *
     * @return array|null
     */
    public static function getTypesByRegulator(string $regulator): ?array
    {
        if (!in_array($regulator, [State::REGULATOR_LEAF, State::REGULATOR_METRC])) {
            return null;
        }

        if ($regulator == State::REGULATOR_LEAF) {
            return [
                self::TYPE_PROPAGATION => LeafConstant::BATCH_TYPE_PLANT,
                self::TYPE_VEGETATION => LeafConstant::BATCH_TYPE_PLANT,
                self::TYPE_HARVEST => LeafConstant::BATCH_TYPE_HARVEST,
                self::TYPE_PRODUCTION => LeafConstant::BATCH_TYPE_INTERMEDIATE_END_PRODUCT,
            ];
        }

        return [
            self::TYPE_PROPAGATION => self::TYPE_PROPAGATION,
            self::TYPE_VEGETATION => self::TYPE_VEGETATION,
            self::TYPE_HARVEST => self::TYPE_HARVEST,
            self::TYPE_PRODUCTION => self::TYPE_PRODUCTION,
        ];
    }

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    /**
     * Self reference relation
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Batch::class, 'parent_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(Batch::class, 'parent_id', 'id');
    }

    /**
     * @return MorphTo
     */
    public function source(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Relation with Inventory package
     *
     * @return HasMany
     */
    public function inventories(): HasMany
    {
        return $this->hasMany(Inventory::class);
    }

    /**
     * Relation with Plant through PlantGroup
     *
     * @return HasManyThrough
     */
    public function plants(): HasManyThrough
    {
        return $this->hasManyThrough(Plant::class, PlantGroup::class, 'batch_id', 'plant_group_id');
    }

    /**
     * Scope filter batch type
     *
     * @param Builder|self $query
     * @param string $type
     *
     * @return Builder
     */
    public function scopeType(Builder $query, string $type): Builder
    {
        return $query->whereType($type);
    }

    /**
     * Scope filter batch status (closed/open)
     *
     * @param Builder|self $query
     * @param $status $status
     *
     * @return Builder
     */
    public function scopeStatus(Builder $query, int $status): Builder
    {
        return $query->whereStatus($status);
    }

    /**
     * Get batch type by vendor and configured type const
     * Use to map with Leaf vendor for seed to sale sync
     *
     * @return string|null
     */
    public function getRegulatorType(): ?string
    {
        if (!($vendorTypeList = self::getTypesByRegulator($this->license->state->regulator))) {
            return null;
        }

        return $vendorTypeList[$this->type] ?? null;
    }

    /**
     * @return bool
     */
    public function isTraceUpdatable(): bool
    {
        return false;
    }

    public function updateSyncStatus(int $status): self
    {
        return $this;
    }

    public function updateSyncCode(string $code): self
    {
        return $this;
    }
}
