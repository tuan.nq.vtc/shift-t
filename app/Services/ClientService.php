<?php

namespace App\Services;

use App\Models\Client;
use Illuminate\Support\Collection;

/**
 * Class ClientService
 * @package App\Services
 */
class ClientService extends BaseService
{
    public function __construct(Client $client)
    {
        parent::__construct($client);
    }

    /**
     * @return Collection
     */
    public function getAvailableItems(): Collection
    {
        return $this->newQuery()->where('status', Client::STATUS_ENABLED)->get();
    }
}
