<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'plants',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('strain_id')->index();
                $table->uuid('room_id')->nullable()->index();
                $table->uuid('subroom_id')->nullable()->index();
                $table->string('source_type');
                $table->string('grow_status')->index();
                $table->uuid('grow_cycle_id')->nullable()->index();
                $table->uuid('plant_group_id')->nullable()->index();
                $table->string('mother_code')->nullable()->index();
                $table->dateTime('moved_room_at')->nullable();

                // Propagation
                $table->date('planted_at')->index();
                $table->uuid('propagation_id')->nullable();

                // Harvest
                $table->date('est_harvested_at')->nullable();
                $table->date('harvested_at')->nullable();

                // Reason to be destroyed
                $table->text('destroy_reason')->nullable();
                // Destroyed at
                $table->date('destroyed_at')->nullable();

                // Is destroyed
                $table->boolean('destroyed')->default(false)->index();
                $table->boolean('is_mother')->default(false)
                    ->comment('Plant marked as mother for creating propagation source');
                $table->boolean('is_manifested')->default(false)->index();

                // S2S Trace sync[Create/Update/Delete]
                $table->string('sync_code')->nullable()->index();
                $table->unsignedTinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();
                // Info when sync from regulator
                $table->json('info')->nullable();

                $table->timestamps();
                $table->softDeletes();

                // Foreign keys
                $table->foreign('strain_id')->references('id')->on('strains');
                $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
                $table->foreign('subroom_id')->references('id')->on('subrooms')->nullOnDelete();
                $table->foreign('grow_cycle_id')->references('id')->on('grow_cycles')->nullOnDelete();
                $table->foreign('plant_group_id')->references('id')->on('plant_groups')->nullOnDelete();
                $table->foreign('propagation_id')->references('id')->on('propagations')->nullOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plants');
    }
}
