<?php

namespace App\Listeners;

use App\Events\Harvest\HarvestWetWeightDeleted;
use App\Events\Harvest\HarvestWetWeightSaved;
use App\Services\HarvestService;
use Illuminate\Events\Dispatcher;

class HarvestEventSubscriber
{
    /**
     * @param HarvestWetWeightSaved|HarvestWetWeightDeleted $event
     */
    public function calculateWetWeights($event)
    {
        if ($event->harvestWetWeight->isDirty('weight') || $event instanceof HarvestWetWeightDeleted) {
            app(HarvestService::class)->calculateWetWeights($event->harvestWetWeight->harvest);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [HarvestWetWeightSaved::class, HarvestWetWeightDeleted::class],
            'App\Listeners\HarvestEventSubscriber@calculateWetWeights'
        );
    }
}
