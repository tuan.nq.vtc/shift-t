<?php

use App\Models\{Harvest, HarvestGroup, Inventory, InventoryType, License, Room, RoomType};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class InventoryUnLottedSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run(): void
    {
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
    }

    protected function iterateLicense($license): void
    {
        $rooms = Room::licenseId($license->id)
            ->with('subrooms')
            ->whereHas('roomType', fn($query) => $query->where('category', RoomType::CATEGORY_INVENTORY))
            ->get();
        if ($rooms->isEmpty()) {
            return;
        }

        $harvestCategory = $license->stateCategories()->whereHas(
            'parent',
            function (Builder $query) {
                $query->where('code', 'harvest_materials');
            }
        )->get();
        if (!$harvestCategory || $harvestCategory->isEmpty()) {
            return;
        }


        foreach ($rooms as $room) {
            try {
                $type = factory(InventoryType::class)->create(
                    [
                        'license_id' => $license->id,
                        'state_category_id' => $harvestCategory->random()->id
                    ]
                );

                $inventories = factory(Inventory::class, rand(2, 5))->create(
                    [
                        'license_id' => $license->id,
                        'type_id' => $type->id,
                        'room_id' => $room->id,
                        'subroom_id' => $room->subrooms->random()->id,
                        'is_lotted' => false,
                    ]
                );

                foreach ($inventories as $inventory) {
                    $harvestGroup = factory(HarvestGroup::class)->create(
                        [
                            'license_id' => $license->id,
                            'room_id' => $room->id,
                            'status' => HarvestGroup::STATUS_CLOSED
                        ]
                    );

                    $inventory->source()->associate(
                        factory(Harvest::class)->create(
                            [
                                'license_id' => $license->id,
                                'room_id' => $room->id,
                                'harvest_group_id' => $harvestGroup->id,
                            ]
                        )
                    )->save();
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }
        }

    }
}
