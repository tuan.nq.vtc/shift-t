<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Handleable;
use App\Cqrs\Values\AnalysisTypeResult;
use App\Models\Inventory;
use App\Models\LabTestAnalysis;
use App\Models\QASample;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;
use Throwable;

class SyncQASampleResultHandler extends Handleable
{
    /**
     * @var Inventory
     */
    private $inventory;

    /**
     * @var QASample
     */
    private $qaSample;

    /**
     * @var LabTestAnalysis
     */
    private $labTestAnalysis;

    /**
     * @var CreateQASampleResultHandler
     */
    private $createQASampleResultHandler;

    /**
     * @param Inventory $inventory
     * @param QASample $qaSample
     * @param LoggerInterface $logger
     */
    public function __construct(Inventory                   $inventory,
                                QASample                    $qaSample,
                                LabTestAnalysis             $labTestAnalysis,
                                CreateQASampleResultHandler $createQASampleResultHandler,
                                LoggerInterface             $logger)
    {
        $this->inventory = $inventory;
        $this->qaSample = $qaSample;
        $this->labTestAnalysis = $labTestAnalysis;
        $this->logger = $logger;
        $this->createQASampleResultHandler = $createQASampleResultHandler;
    }

    /**
     * @param SyncQASampleResultCommand $command
     * @return array
     * @throws Exception
     */
    public function handle(SyncQASampleResultCommand $command): array
    {
        try {
            DB::beginTransaction();

            $data = $this->performSyncQASampleResult($command);

            DB::commit();

            return $data;
        } catch (Exception $e) {
            DB::rollBack();
            $this->log($command, $e->getMessage(), $e->getTrace());
            throw $e;
        }
    }

    /**
     * @param SyncQASampleResultCommand $command
     * @return array
     * @throws Exception
     */
    private function performSyncQASampleResult(SyncQASampleResultCommand $command): array
    {
        if (!$command->getAnalysisResults()) {
            throw new Exception('The QA Sample Result not found');
        }

        $inventory = $this->inventory->where('sync_code', $command->getSyncCodeInventory())->first();
        if ($inventory instanceof Inventory !== true) {
            throw new Exception(sprintf('The Inventory has SynCode \'%s\' not found', $command->getSyncCodeInventory()));
        }

        if (!$inventory->getRegulator()) {
            throw new Exception(sprintf('The Regulator of the Inventory \'%s\' not found', $inventory->getAttribute('id')));
        }

        $qaSample = $this->getQASampleIsWaitingResult($inventoryId = $inventory->getAttribute('id'));
        if ($qaSample instanceof QASample !== true) {
            throw new Exception(sprintf('The inventory \'%s\' has no QASample is waiting for results', $inventoryId));
        }

        $data = $this->sync($command, $inventory, $qaSample);

        $this->updateQASample($command, $qaSample);

        $inventoryStatus = Inventory::getQAStatus($command->getStatus());
        if (!in_array($inventoryStatus, Inventory::getQAStatuses())) {
            throw new Exception('The inventory status is invalid');
        }

        $inventory->setAttribute('qa_status',$inventoryStatus);
        if (!$inventory->save()) {
            throw new Exception('Update Inventory failed');
        }

        return $data;
    }

    /**
     * @param SyncQASampleResultCommand $command
     * @param QASample $qaSample
     * @throws Exception
     */
    private function updateQASample(SyncQASampleResultCommand $command, QASample $qaSample)
    {
        $qaSample->setAttribute('sync_code', $command->getSyncCode());
        $qaSample->setAttribute('sync_status', TraceableModelInterface::SYNC_STATUS_SYNCED);
        $qaSample->setAttribute('status', QASample::getResultStatus($command->getStatus()));
        $qaSample->setAttribute('synced_at', date('Y-m-d H:i:s'));

        if (true !== $qaSample->save()) {
            throw new Exception('Update QA Sample failed');
        }
    }

    /**
     * @param SyncQASampleResultCommand $command
     * @param Inventory $inventory
     * @param QASample $qaSample
     * @return array
     * @throws Throwable
     */
    protected function sync(SyncQASampleResultCommand $command, Inventory $inventory, QASample $qaSample): array
    {
        $data = [];
        $regulator = $inventory->getRegulator();

        foreach ($command->getAnalysisResultsByRegulator($regulator) as $analysisTypeResult) {
            if ($analysisTypeResult instanceof AnalysisTypeResult !== true) {
                continue;
            }

            $labTestAnalysis = $this->labTestAnalysis->firstOrCreate(
                ['code' => $analysisTypeResult->getCode()],
                [
                    'name' => $analysisTypeResult->getName(),
                    'regulator' => $inventory->getRegulator(),
                    'is_required' => $analysisTypeResult->getIsRequired(),
                ]
            );

            if ($labTestAnalysis instanceof LabTestAnalysis !== true) {
                continue;
            }

            $cmd = new CreateQASampleResultCommand([
                'qa_sample_id' => $qaSample->getAttribute('id'),
                'license_id' => $inventory->getAttribute('license_id'),
                'analysis_id' => $labTestAnalysis->getAttribute('id'),
                'result' => $analysisTypeResult->getResult(),
            ]);

            try {
                $data[] = $this->createQASampleResultHandler->handle($cmd);;
            } catch (Throwable $e) {
                $this->log($cmd, $e->getMessage());
                throw $e;
            }
        }

        return $data;
    }

    /**
     * @param $inventoryId
     * @return mixed
     */
    private function getQASampleIsWaitingResult($inventoryId)
    {
        return $this->qaSample
            ->where('inventory_id', $inventoryId)
            ->where('status', QASample::STATUS_AWAITING_RESULT)
            ->first();
    }
}
