<?php

namespace App\Cqrs\Values;

use App\Cqrs\Base\Valuable;
use App\Helpers\ModelHelper;
use DateTimeImmutable;
use Illuminate\Support\Str;

class QASample extends Valuable
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $inventoryId;

    /**
     * @var string|null
     */
    protected $sampleType;

    /**
     * @var string|null
     */
    protected $labId;

    /**
     * @var float|null
     */
    protected $weight;

    /**
     * @var string
     */
    protected $status = 0;

    /**
     * @var DateTimeImmutable
     */
    protected $createdAt;

    /**
     * @var array
     */
    protected $results;

    /**
     * @var string|null
     */
    protected $analysisType;

    protected $testedAt;

    /**
     * @var boolean
     */
    protected $isModifyChildLot;


    /**
     * @var string[]
     */
    protected $mapping = [
        'id|string' => 'id',
        'inventoryId|string' => 'source_inventory_id',
        'sampleType|int' => 'sample_type',
        'labId|string' => 'lab_id',
        'status|int' => 'status',
        'weight|float' => 'weight',
        'createdAt' => 'created_at',
        'results' => 'results',
        'analysisType|string' => 'analysis_type',
        'testedAt' => 'tested_at',
        'isModifyChildLot|bool' => 'is_modify_child_lot'
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
        if (!$this->id) {
            $this->id = Str::uuid()->toString();
        }

        if (!$this->createdAt) {
            $this->createdAt = new DateTimeImmutable();
        }
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return !empty($this->inventoryId)
            && $this->sampleType
            && $this->labId
            && ($this->weight > 0);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'source_inventory_id' => $this->inventoryId,
            'sample_type' => $this->sampleType,
            'weight' => $this->weight,
            'lab_id' => $this->labId,
            'status' => $this->status,
            'created_at' => $this->createdAt,
            'results' => $this->results,
            'tested_at' => $this->testedAt,
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getInventoryId(): ?string
    {
        return $this->inventoryId;
    }

    /**
     * @return string|null
     */
    public function getSampleType(): ?string
    {
        return $this->sampleType;
    }

    /**
     * @return string|null
     */
    public function getLabId(): ?string
    {
        return $this->labId;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @return string|null
     */
    public function getAnalysisType(): ?string
    {
        return $this->analysisType;
    }

    /**
     * @return mixed
     */
    public function getTestedAt()
    {
        return $this->testedAt;
    }

    /**
     * @return string[]
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    /**
     * @return boolean
     */
    public function getIsModifyChildLot(): bool
    {
        return $this->isModifyChildLot;
    }
}
