<?php

/** @var Factory $factory */

use App\Models\{Room, Subroom};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Subroom::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true),
        "room_id" => function () {
            return factory(Room::class)->create()->id;
        },
        'status' => rand(0, 1),
        'type' => $faker->randomElement([Subroom::TYPE_RACK, Subroom::TYPE_BED]),
        'dimension' => ['width' => $faker->randomNumber(3), 'length' => $faker->randomNumber(3)],
        'color' => $faker->hexColor,
    ];
}
);
