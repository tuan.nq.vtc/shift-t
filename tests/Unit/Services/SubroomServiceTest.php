<?php

namespace Tests\Unit\Services;

use App\Models\Subroom;
use App\Services\SubroomService;
use Tests\TestCase;

class SubroomServiceTest extends TestCase
{
    protected static string $modelName = Subroom::class;

    protected function getService(): SubroomService
    {
        return app(SubroomService::class);
    }
}
