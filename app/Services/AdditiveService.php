<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\{Additive,
    AdditiveInventory,
    AdditiveLog,
    AdditiveOperation,
    GrowCycle,
    Plant,
    Propagation,
    Room
};
use Carbon\Carbon;
use Illuminate\Database\Eloquent\{Builder, Collection, Model};
use Illuminate\Support\{Arr, Enumerable, Facades\DB};
use Throwable;

/**
 * Class AdditiveService
 *
 * @package App\Services
 * @property Additive $additive
 *
 * @method Additive|Builder newQuery($withoutGlobalFilter = false)
 * @method Additive|Collection get($id)
 * @method Additive|Builder getModel()
 */
class AdditiveService extends BaseService
{

    /**
     * @var array|string[]
     */
    protected array $relations = [
        'creator',
        'modifier',
    ];

    public function __construct(Additive $additive)
    {
        parent::__construct($additive);
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return Model
     * @throws GeneralException
     */
    public function update($id, array $data): Model
    {
        if (isset($data['user_id'])) {
            $data['updated_by'] = $data['user_id'];
            unset($data['user_id']);
        }
        return parent::update($id, $data);
    }

    /**
     * @param array $data
     * @param       $type
     *
     * @return array|\Exception|Throwable
     * @throws GeneralException
     * @throws Throwable
     */
    public function bulkCreate(array $data, $type)
    {
        if (!isset($data['additives']) || !isset($data['user_id']) || !isset($data['license_id'])) {
            throw new GeneralException(
                __("exceptions.additive.missing_parameter", ['parameters' => 'additives, user_id,license_id '])
            );
        }
        $additives = [];
        try {
            DB::beginTransaction();
            foreach ($data['additives'] as $additiveData) {
                if (!is_array($additiveData)) {
                    throw new GeneralException(__("exceptions.additive.wrong_format", ['field' => 'additives']));
                }
                $additiveData = array_merge(
                    $additiveData,
                    [
                        'created_by' => $data['user_id'],
                        'updated_by' => $data['user_id'],
                        'license_id' => $data['license_id'],
                        'is_other_type' => Arr::get($additiveData, 'type', '') === Additive::TYPE_OTHER,
                        'type' => $type
                    ]
                );
                /** @var Additive $additive */
                $additives[] = $additive = $this->model->create($additiveData);
                $inventoryData = $additiveData['additive_inventory'];
                $inventoryData = array_merge(
                    $inventoryData,
                    [
                        'created_by' => $data['user_id'],
                        'license_id' => $data['license_id'],
                        'uom' => $additive->uom,
                        'available_quantity' => Arr::get($inventoryData, 'total_quantity', 0),
                        'order_date' => Carbon::now(),
                    ]
                );

                $additive->inventories()->create($inventoryData);
            }
            DB::commit();
            return $additives;
        } catch (\Throwable $throwable) {
            DB::rollBack();
            return $throwable;
        }
    }

    /**
     * @param       $licenseId
     * @param       $type
     *
     * @return Collection
     */
    public function getAvailableItems($licenseId, $type): Collection
    {
        return $this->newQuery()
            ->licenseId($licenseId)
            ->where('type', $type)
            ->available()
            ->select('id', 'name', 'available_quantity', 'uom', 'epa_regulation_number', 'ingredients')
            ->get();
    }

    public function storeInventory(Additive $additive, array $data): AdditiveInventory
    {
        if (empty($data['user_id']) || empty($data['total_quantity'])) {
            throw new GeneralException(__('exceptions.additive.create_inventory_failed'));
        }
        try {
            DB::beginTransaction();
            return tap(
                $additive->inventories()->create(
                    array_merge(
                        $data,
                        [
                            'available_quantity' => $data['total_quantity'],
                            'created_by' => $data['user_id'],
                            'uom' => $additive->uom,
                        ]
                    )
                ),
                function (AdditiveInventory $model) {
                    if (!$model->save()) {
                        throw new GeneralException(__('exceptions.additive.create_inventory_failed'));
                    }
                    DB::commit();
                }
            );
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param AdditiveInventory $additiveInventory
     *
     * @return Additive
     */
    public function calculateAfterInventorySaved(AdditiveInventory $additiveInventory): Additive
    {
        $additive = $additiveInventory->additive;
        $inventories = $additive->inventories;
        if ($additiveInventory->isDirty('available_quantity')) {
            $additive->available_quantity = $inventories->sum('available_quantity');
        }

        if ($additiveInventory->isDirty(['total_price', 'total_quantity'])) {
            $sumInventoryQuantity = $inventories->sum('total_quantity');
            $sumInventoryQuantity != 0 &&
            ($additive->avg_price = $inventories->sum('total_price') / $sumInventoryQuantity);
            $additive->total_quantity = $sumInventoryQuantity;
        }

        if (!$additive->save()) {
            new GeneralException(__('exceptions.additive.calculate_failed'));
        }

        return $additive;
    }

    /**
     * @param Enumerable $targets
     * @param array $suppliers
     * @param array $data
     *
     * @return array
     * @throws Throwable
     */
    public function applyOnTargets(Enumerable $targets, array $suppliers, array $data): array
    {
        $operations = [];
        try {
            DB::beginTransaction();

            foreach ($suppliers as $key => $supplierInfo) {
                throw_if(!is_array($supplierInfo), new GeneralException("Additive inventory {$key} has wrong format"));
                /** @var AdditiveInventory $inventory */
                $inventory = AdditiveInventory::query()->find(Arr::get($supplierInfo, 'id'));
                throw_if(!$inventory, new GeneralException("Additive inventory {$key} does not exist"));
                $appliedQty = Arr::get($supplierInfo, 'applied_quantity', 0);
                throw_if(
                    $appliedQty == 0 || !is_numeric($appliedQty) || $inventory->available_quantity < $appliedQty,
                    new GeneralException(
                        __(
                            'validation.quantity.insufficient',
                            ['entity' => 'pesticide inventory', 'value' => $appliedQty]
                        )
                    )
                );
                $targetsCount = $targets->count();
                foreach ($targets as $target) {
                    $operations[] = $this->applyOnTarget($targetsCount, $target, $inventory, $appliedQty, $data);
                }
                $inventory->available_quantity = $inventory->available_quantity - $appliedQty;
                $inventory->save();
            }
            DB::commit();

            return $operations;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param int $targetsCount
     * @param Model $target
     * @param AdditiveInventory $inventory
     * @param                   $appliedQty
     * @param array $data
     *
     * @return AdditiveOperation|null
     *
     * @throws GeneralException
     * @throws Throwable
     */
    protected function applyOnTarget(
        int $targetsCount,
        Model $target,
        AdditiveInventory $inventory,
        $appliedQty,
        array $data
    ): ?AdditiveOperation {
        throw_if(
            0 === $targetsCount,
            new GeneralException(
                __(
                    'exceptions.pesticide.apply_failed',
                    compact($target)
                )
            )
        );
        switch (true) {
            // When target contains plants
            case $target instanceof GrowCycle:
            case $target instanceof Room:
                $plantsCount = $target->plants()->count();
                throw_if(
                    0 === $plantsCount,
                    new GeneralException(
                        __('exceptions.pesticide.apply_failed_with_no_plant', ['target' => 'grow cycle or room'])
                    )
                );
                $operation = $this->createAdditiveOperation($target, $inventory, $data, $appliedQty);
                throw_unless(
                    $operation,
                    new GeneralException(__('exceptions.pesticide.apply_failed', compact($target)))
                );
                $unitCost = round(
                    ($operation->applied_quantity * $operation->unit_price) / $targetsCount / $plantsCount,
                    2
                );
                $target->plants->each(
                    fn(Plant $plant) => $this->createAdditiveLogs(
                        $operation,
                        $unitCost,
                        $inventory,
                        $plant,
                        Arr::get($data, 'application_device', '')
                    )
                );
                return $operation;
            // When target is propagation
            case $target instanceof Propagation:
                throw_if(
                    $target->quantity <= 0,
                    new GeneralException(__('apply_failed_on_empty_propagation'))
                );
            // When target is plant
            case $target instanceof Plant:
                $operation = $this->createAdditiveOperation($target, $inventory, $data, $appliedQty);
                throw_unless(
                    $operation,
                    new GeneralException(__('exceptions.pesticide.apply_failed', compact($target)))
                );
                $unitCost = round(($operation->applied_quantity * $operation->unit_price) / $targetsCount, 2);
                $this->createAdditiveLogs(
                    $operation,
                    $unitCost,
                    $inventory,
                    $target,
                    Arr::get($data, 'application_device', '')
                );
                return $operation;
            default:
                throw  new GeneralException(__('exceptions.pesticide.apply_failed', compact($target)));
        }
    }

    /**
     * @param Model $target
     * @param AdditiveInventory $inventory
     * @param array $data
     *
     * @param                   $appliedQty
     *
     * @return AdditiveOperation
     */
    protected function createAdditiveOperation(
        Model $target,
        AdditiveInventory $inventory,
        array $data,
        $appliedQty
    ): AdditiveOperation {
        $operationData = array_merge(
            $data,
            [
                'additive_type' => optional($inventory->additive)->isPesticide() ?
                    AdditiveLog::ADDITIVE_TYPE_PESTICIDE : AdditiveLog::ADDITIVE_TYPE_NUTRIENT,
                'unit_price' => $inventory->unit_price,
                'uom' => $inventory->uom,
                'created_by' => Arr::get($data, 'user_id'),
                'updated_by' => Arr::get($data, 'user_id'),
                'applied_quantity' => $appliedQty,
                'supplier_id' => $inventory->supplier_id,
            ]
        );
        /** @var AdditiveOperation $operation */
        $operation = $inventory->additiveOperations()->make($operationData);
        $operation->additive()->associate($inventory->additive);
        $operation->target()->associate($target);
        $operation->save();

        return $operation;
    }

    /**
     * @param AdditiveOperation $operation
     * @param float $cost
     * @param AdditiveInventory $inventory
     * @param Model $mainTarget
     *
     * @return AdditiveLog|null
     * @throws Throwable
     */
    public function createAdditiveLogs(
        AdditiveOperation $operation,
        float $cost,
        AdditiveInventory $inventory,
        Model $mainTarget,
        string $applicationDevice
    ): ?AdditiveLog {
        $additiveLogData = [];
        if ($mainTarget instanceof Plant) {
            $additiveLogData = [
                'grow_cycle_id' => $mainTarget->grow_cycle_id,
                'plant_id' => $mainTarget->id,
            ];
        }
        if ($mainTarget instanceof Propagation) {
            $additiveLogData = [
                'propagation_id' => $mainTarget->id
            ];
        }
        throw_if(
            empty($additiveLogData),
            new GeneralException(__('exceptions.pesticide.apply_failed', compact($mainTarget)))
        );
        /** @var AdditiveLog $additiveLog */
        $additiveLog = $operation->additiveLogs()->create(
            array_merge(
                [
                    'additive_type' => optional($inventory->additive)->isPesticide() ?
                        AdditiveLog::ADDITIVE_TYPE_PESTICIDE : AdditiveLog::ADDITIVE_TYPE_NUTRIENT,
                    'supplier_id' => $inventory->supplier_id,
                    'license_id' => $operation->license_id,
                    'additive_id' => $operation->additive_id,
                    'room_id' => $mainTarget->room_id,
                    'subroom_id' => $mainTarget->subroom_id,
                    'application_device' => $applicationDevice,
                    'cost' => $cost,
                    'applied_date' => $operation->applied_date,
                    'applied_by' => $operation->applied_by,
                    'created_by' => $operation->created_by,
                    'updated_by' => $operation->updated_by,
                ],
                $additiveLogData
            )
        );
        return $additiveLog;
    }

}
