<?php

namespace App\Http\Controllers;

use App\Services\LabTestService;
use Illuminate\Http\{JsonResponse, Request, Response};
use Laravel\Lumen\Routing\Controller;

/**
 * Class LabTestController
 * @package App\Http\Controllers
 */
class LabTestController extends Controller
{
    /**
     * @var LabTestService
     */
    private LabTestService $labTestService;

    public function __construct(LabTestService $labTestService)
    {
        $this->labTestService = $labTestService;
    }

    public function store(Request $request): JsonResponse
    {
        $licenseId = $request->get('license_id');

        return response()->json(
            $this->labTestService->create($this->validate($request, [
                'qa_sample_id' => [
                    'required',
                    'uuid',
                    'exists:App\Models\QASample,id,license_id,'.$licenseId,
                    'unique:App\Models\LabTest,qa_sample_id',
                ],
                'tested_at' => ['nullable', 'date'],
                'analyses' => ['required', 'array'],
                'analyses.*.analysis_type_id' => [
                    'required',
                    'uuid',
                    'exists:App\Models\LabTestAnalysis,id',
                ],
                'analyses.*.result' => ['required', 'numeric'],
                'license_id' => [],
                'user_id' => [],
            ])),
            Response::HTTP_CREATED
        );
    }

    public function update(Request $request, $id): JsonResponse
    {
        return response()->json(
            $this->labTestService->update(
                $id,
                $this->validate($request, [
                    'tested_at' => ['nullable', 'date'],
                    'analyses' => ['sometimes', 'array'],
                    'analyses.*.analysis_type_id' => [
                        'required',
                        'uuid',
                        'exists:App\Models\LabTestAnalysis,id',
                    ],
                    'analyses.*.result' => ['required', 'numeric'],
                    'license_id' => [],
                    'user_id' => [],
                ])
            ),
            Response::HTTP_ACCEPTED
        );
    }
}
