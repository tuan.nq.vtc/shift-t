<?php

namespace Bamboo\StreamProcessing\Contracts;

use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface as StreamableResourceContract;
use Illuminate\Database\Eloquent\Model;
use Interop\Queue\{Destination, Message};
use Throwable;

interface ProducerHandlerInterface
{
    /**
     * @param string $topicName
     * @param StreamEventInterface $streamEvent
     * @return void
     *
     * @throws Throwable
     */
    public function sendEvent(string $topicName, StreamEventInterface $streamEvent): void;
}
