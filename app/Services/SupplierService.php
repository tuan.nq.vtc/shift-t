<?php

namespace App\Services;

use App\Services\Contracts\AccountHolderServiceInterface;
use App\Services\Traits\HasAccountHolderId;
use App\Models\{Supplier};
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class SupplierService
 *
 * @package App\Services
 * @property Supplier $supplier
 *
 * @method Supplier|Builder newQuery($withoutGlobalFilter = false)
 * @method Supplier|Collection get($id)
 * @method Supplier|Builder getModel()
 */
class SupplierService extends BaseService implements AccountHolderServiceInterface
{
    use HasAccountHolderId;

    public function __construct(Supplier $supplier)
    {
        parent::__construct($supplier);
    }

    /**
     *
     * @return Collection
     */
    public function getAvailableItems(): Collection
    {
        return $this->newQuery()
            ->accountHolderId($this->getAccountHolderId())
            ->select('id', 'name')
            ->get();
    }

}
