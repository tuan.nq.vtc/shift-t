<?php

use App\Models\{Disposal, Plant, Propagation, Room, RoomType};
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class DisposalSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        $propagationIteration = function ($propagation) {
            call_user_func([$this, 'iteratePropagation'], $propagation);
        };
        $plantIteration= function ($plant) {
            call_user_func([$this, 'iteratePlant'], $plant);
        };
        Propagation::destroyed(true)->get()->each($propagationIteration);
        Plant::destroyed(true)->get()->each($plantIteration);
    }

    protected function iteratePropagation(Propagation $propagation)
    {
        $room = Room::licenseId( $propagation->license_id)
            ->whereHas(
                'roomType',
                function ($roomTypeQuery) {
                    $roomTypeQuery->where('category', '<>', RoomType::CATEGORY_DISPOSAL);
                })
            ->first();
        if ($room) {
            factory(Disposal::class)->create(
                [
                    'license_id' => $propagation->license_id,
                    'uom' => Disposal::UOM_EACH,
                    'wastable_id' => $propagation->id,
                    'wastable_type' => Propagation::class,
                    'strain_id' => $propagation->strain ? $propagation->strain->id : null,
                    'room_id' => $room->id,
                ]
            );
        }
    }

    protected function iteratePlant(Plant $plant)
    {
        $room = Room::licenseId( $plant->license_id)
            ->whereHas(
                'roomType',
                function ($roomTypeQuery) {
                    $roomTypeQuery->where('category', '<>', RoomType::CATEGORY_DISPOSAL);
                })
            ->first();
        if ($room) {
            factory(Disposal::class)->create(
                [
                    'license_id' => $plant->license_id,
                    'uom' => Disposal::UOM_EACH,
                    'wastable_id' => $plant->id,
                    'wastable_type' => Plant::class,
                    'strain_id' => $plant->strain ? $plant->strain->id : null,
                    'room_id' => $room->id,
                ]
            );
        }
    }
}
