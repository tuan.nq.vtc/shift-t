<?php

namespace App\Events\Propagation;

use App\Events\Event;
use App\Models\PlantGroup;

class PropagationToVegetationMoved extends Event
{
    /**
     * @var PlantGroup
     */
    public PlantGroup $plantGroup;

    /**
     * @param PlantGroup $plantGroup
     */
    public function __construct(PlantGroup $plantGroup)
    {
        $this->plantGroup = $plantGroup;
    }
}
