<?php

namespace App\Models\Traits;

use App\Models\Room;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait StoreInRoom
{
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }

    public function scopeRoomId(Builder $query, ...$roomId): Builder
    {
        return $query->whereIn($this->getTable().'.room_id', $roomId);
    }

    public function scopeRoomIds(Builder $query, ...$roomIds): Builder
    {
        return $query->whereIn($this->getTable().'.room_id', $roomIds);
    }

    public function scopeRoom(Builder $query, ...$roomId): Builder
    {
        return $this->scopeRoomId($query, ...$roomId);
    }

    public function scopeOrderByRoom(Builder $query, $direction): Builder
    {
        $roomTable = $this->room()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($roomTable, $this->getTable().'.room_id', '=', $roomTable.'.id')
            ->orderBy($roomTable.'.name', $direction);
    }
}
