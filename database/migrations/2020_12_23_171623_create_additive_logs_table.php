<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditiveLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'additive_logs',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->unsignedTinyInteger('additive_type')->default(0)->comment('0- pesticide, 1- nutrient');
                $table->uuid('additive_id')->references('id')->on('additives')->cascadeOnDelete();
                $table->uuid('additive_operation_id')->references('id')->on('additive_operations')->nullOnDelete();
                $table->uuid('grow_cycle_id')->index()->nullable()->references('id')->on('grow_cycles')->nullOnDelete();
                $table->uuid('room_id')->index()->nullable()->references('id')->on('rooms')->nullOnDelete();
                $table->uuid('subroom_id')->index()->nullable()->references('id')->on('subrooms')->nullOnDelete();
                $table->uuid('plant_id')->index()->nullable()->references('id')->on('plants')->nullOnDelete();
                $table->uuid('propagation_id')->index()->nullable()->references('id')->on('propagations')->nullOnDelete();
                $table->uuid('supplier_id')->index()->nullable()->references('id')->on('suppliers')->nullOnDelete();

                $table->double('cost', 10, 2);
                $table->dateTime('applied_date')->nullable();
                $table->string('application_device')->nullable();

                $table->uuid('applied_by')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('updated_by')->nullable();

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additive_logs');
    }
}
