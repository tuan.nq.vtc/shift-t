<?php

namespace App\Console\Commands\TraceableResource;

use App\Models\{License, State};
use App\Synchronizations\Contracts\Http\{ClientInterface, ResourceInterface};
use App\Synchronizations\Http\Clients\{LeafClient, MetrcClient};
use Exception;
use Illuminate\Console\Command;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Str;

class RemoveSyncedCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'Remove sync all traceable resources to state API on development environment';
    /**
     * @var string
     */
    protected $signature = 'trace:remove-synced {--state= : The state resource(s) synced will be removed}
                {--resource= : The resource type [Strain, Location, Propagation, Plant, ...] will be removed}
                {--config= : API config}';

    /**
     * @var State
     */
    protected $state;
    /**
     * @var string
     */
    protected $resourceType;
    /**
     * @var License
     */
    protected $license;
    /**
     * @var ClientInterface
     */
    protected $client;
    /**
     * @var ResourceInterface
     */
    protected $resource;

    /**
     * @throws Exception
     */
    public function handle()
    {
        if (app()->environment() == 'production') {
            $this->error("Can not do this action on production environment");
            exit();
        }

        $stateCode = $this->getStateCode();
        $resourceType = $this->getResourceType();

        if (!$stateCode) {
            $this->error("Please specify state");
            exit();
        }

        if (!$resourceType) {
            $this->error("Please specify resource type");
            exit();
        }


        if (!$stateCode || !($this->state = State::whereCode($stateCode)->first())) {
            $this->error("Invalid state [{$stateCode}]");
            exit();
        }

        if (!isset($this->state->getRegulatorResources()["App\Models\\{$resourceType}"])) {
            $this->error("Resource [{$resourceType}] not configured for state [{$stateCode}]");
            exit();
        }

        $this->resourceType = $resourceType;

        $this->initLicense()->buildClient()->buildResource();

        $listResponse = $this->sendRequest();

        if (State::REGULATOR_LEAF == $this->state->regulator) {
            $body = $listResponse->json();
            foreach ($body['data'] as $item) {
                $this->buildResource(['sync_code' => $item['global_id']], ResourceInterface::ACTION_DELETE);
                if ($this->sendRequest()->successful()) {
                    $this->line("Removed item [{$item['global_id']}] sucessfully");
                } else {
                    $this->error("Remove item [{$item['global_id']}] failed");
                }
            }
        }

        if (State::REGULATOR_METRC == $this->state->regulator) {
            $this->client = new MetrcClient($this->license);
        }
    }

    /**
     * @return string|null
     */
    protected function getStateCode(): ?string
    {
        if ($state = $this->option('state')) {
            return Str::upper($state);
        }

        return null;
    }

    /**
     * @return string|null
     */
    protected function getResourceType(): ?string
    {
        if ($resource = $this->option('resource')) {
            return Str::ucfirst($resource);
        }

        return null;
    }

    /**
     * @return self
     */
    protected function initLicense(): self
    {
        $configOptions = null;
        if (empty($this->option('config'))) {
            $this->error('Please input config option');
            exit();
        }
        $config = explode(';', $this->option('config'));

        foreach ($config as $item) {
            $keyValue = explode(':', $item);
            if (count($keyValue) != 2) {
                $this->error('Invalid input config option. Valid format: \'key_1:value_1;key_2:value_2;...\'');
                exit();
            }
            $configOptions[$keyValue[0]] = $keyValue[1];
        }

        if (State::REGULATOR_LEAF == $this->state->regulator) {
            if (!isset($configOptions['code']) || !isset($configOptions['api_key'])) {
                $this->error('LEAF need [code] and [api_key] in config option');
                exit();
            }
            $this->license = new License(
                [
                    'state_code' => $this->state->code,
                    'code' => $configOptions['code'],
                    'api_configuration' => [
                        'api_key' => $configOptions['api_key'],
                    ],
                ]
            );
        }

        if (State::REGULATOR_METRC == $this->state->regulator) {
            if (!isset($configOptions['code']) || !isset($configOptions['vendor_api_key']) || !isset($configOptions['user_api_key'])) {
                $this->error('METRC need [code], [vendor_api_key] and [user_api_key] in config option');
                exit();
            }
            $this->license = new License(
                [
                    'state_code' => $this->state->code,
                    'code' => $configOptions['code'],
                    'api_configuration' => [
                        'vendor_api_key' => $configOptions['vendor_api_key'],
                        'user_api_key' => $configOptions['user_api_key'],
                    ],
                ]
            );
        }

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function buildClient(): self
    {
        if (State::REGULATOR_LEAF == $this->state->regulator) {
            $this->client = new LeafClient($this->license);
        }

        if (State::REGULATOR_METRC == $this->state->regulator) {
            $this->client = new MetrcClient($this->license);
        }

        return $this;
    }

    /**
     * @param array $resourceData
     * @param string $action
     *
     * @return self
     *
     * @throw Exception
     */
    protected function buildResource($resourceData = [], $action = ResourceInterface::ACTION_LIST): self
    {
        $regulatorResourceClass = $this->getRegulatorResourceClass();
        $traceableResourceClass = $this->getTraceableResourceClass();

        $traceableResource = new $traceableResourceClass($resourceData);
        $traceableResource->license = $this->license;

        $this->resource = new $regulatorResourceClass($traceableResource, $action);

        return $this;
    }

    /**
     * @return Response
     *
     * @throws Exception
     */
    private function sendRequest(): Response
    {
        return $this->client->getRequest()->send(
            $this->resource->getMethod(),
            $this->resource->getUrl(),
            [
                'query' => $this->resource->getQuery(),
                'json' => $this->resource->getBody(),
            ]
        );
    }

    private function getTraceableResourceClass(): string
    {
        return "App\Models\\{$this->resourceType}";
    }

    private function getRegulatorResourceClass(): string
    {
        return $this->state->getRegulatorResources()[$this->getTraceableResourceClass()];
    }
}
