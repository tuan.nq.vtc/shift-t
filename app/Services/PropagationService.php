<?php

namespace App\Services;

use App\Events\Disposal\DisposalScheduled;
use App\Events\Propagation\PropagationToVegetationMoved;
use App\Exceptions\GeneralException;
use App\Helpers\ModelHelper;
use App\Http\Parameters\Criteria;
use App\Models\{Disposal, GrowCycle, Plant, PlantGroup, Propagation, Room};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection};
use Illuminate\Support\{Arr, Carbon, Enumerable, Facades\DB, Facades\Event};
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class PropagationService
 * @package App\Services
 *
 * @method Propagation|Collection get($id)
 * @method Propagation|Builder getModel()
 * @method Enumerable|Propagation[] getEnumerable(array $ids, $relations = [], $counts = [])
 */
class PropagationService extends BaseService
{
    protected array $relations = [
        'batch:id,sync_code,source_type,source_id',
        'strain:id,name',
        'room:id,name',
        'subroom:id,name',
        'cutBy:id,first_name,last_name',
        'pluggedBy:id,first_name,last_name',
    ];

    public function __construct(Propagation $propagation)
    {
        parent::__construct($propagation);
    }

    /**
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        return parent::list($criteria->addFilter('destroyed', false)->addFilter('available', true));
    }

    public function bulkCreate(array $data): array
    {
        $propagations = [];
        foreach (Arr::get($data, 'propagations') as $item) {
            $item['license_id'] = Arr::get($data, 'license_id');
            $propagation = $this->create($item);
            $propagations[$propagation->id] = $propagation;
        }

        return $propagations;
    }

    /**
     * @param GrowCycle $growCycle
     * @param array $vegetationData
     * @return GrowCycle
     *
     * @throws GeneralException
     * @throws Throwable
     */
    public function moveToVegetation(GrowCycle $growCycle, array $vegetationData): GrowCycle
    {
        $roomId = Arr::get($vegetationData, 'room_id');
        foreach (Arr::get($vegetationData, 'transplant', []) as $transplant) {
            $propagation = $this->get(Arr::get($transplant, 'propagation_id'));
            $strain = $propagation->strain;
            /** @var PlantGroup $plantGroup */
            $plantGroup = $growCycle->plantGroups()->firstOrCreate(
                [
                    'license_id' => $growCycle->license_id,
                    'name' => 'Group of ' . $strain->name . ' in ' . $growCycle->name,
                    'room_id' => $roomId,
                    'strain_id' => $strain->id,
                    'propagation_id' => $propagation->id,
                ]
            );
            $quantity = (int)Arr::get($transplant, 'quantity', 0);
            if ($propagation->quantity < $quantity) {
                throw new GeneralException(
                    __('exceptions.propagation.quantity_insufficient', ['name' => $propagation->name])
                );
            }

            $newPlants = [];
            for ($i = 0; $i < $quantity; $i++) {
                $newPlants[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                    'plant_group_id' => $plantGroup->id,
                    'license_id' => $plantGroup->license_id,
                    'internal_id' => ModelHelper::generateInternalId(
                        Plant::class,
                        $plantGroup->license_id
                    ),
                    'strain_id' => $plantGroup->strain_id,
                    'room_id' => $roomId,
                    'subroom_id' => Arr::get($vegetationData, 'subroom_id'),
                    'planted_at' => Arr::get($vegetationData, 'planted_at'),
                    'propagation_id' => $propagation->id,
                    'source_type' => $propagation->source_type,
                    'mother_code' => $propagation->mother_code,
                    'grow_cycle_id' => $growCycle->id,
                    'grow_status' => $growCycle->grow_status,
                    'est_harvested_at' => $growCycle->est_harvested_at,
                    'moved_room_at' => \Carbon\Carbon::now(),
                ];
            }
            Plant::insert($newPlants);

            if (!$propagation->decreaseQty($quantity)->save()) {
                throw new GeneralException(__('exceptions.propagation.update_qty_failed'));
            }

            Event::dispatch(new PropagationToVegetationMoved($plantGroup));
        }

        return $growCycle;
    }

    /**
     * @param Enumerable $propagations
     * @param Room $disposalRoom
     * @param array $data
     *
     * @return Enumerable
     *
     * @throws Throwable
     */
    public function bulkDestroy(Enumerable $propagations, Room $disposalRoom, array $data): Enumerable
    {
        if (!$disposalRoom->isDisposal()) {
            throw new GeneralException(__('exceptions.disposal.invalid_room'));
        }
        $quarantineStart = Carbon::createFromFormat(
            'Y-m-d H:i:s',
            Arr::get($data, 'quarantine_start', '')
        );
        if ($quarantineStart === false) {
            throw new GeneralException(__('exceptions.disposal.invalid_date'));
        }
        $quarantineEnd = $quarantineStart->clone()->addHours(Disposal::DEFAULT_EXTENDED_QUARANTINE_HOUR);
        $status = $quarantineEnd->lt(Carbon::now()) ? Disposal::STATUS_READY : Disposal::STATUS_SCHEDULED;

        try {
            DB::beginTransaction();

            $propagationIterator = $propagations->getIterator();
            while ($propagationIterator->valid()) {
                /** @var Propagation $propagation */
                $propagation = $propagationIterator->current();
                $disposeQty = Arr::first(
                    Arr::get($data, 'quantities', []),
                    fn($item) => Arr::get($item, 'id') === $propagation->id
                );
                if (!$disposeQty || empty($disposeQty['quantity'])) {
                    throw new GeneralException(__('exceptions.disposal.invalid_quantity'));
                }
                $this->destroy(
                    $propagation,
                    $disposalRoom,
                    [
                        'license_id' => $propagation->license_id,
                        'status' => $status,
                        'uom' => Disposal::UOM_EACH,
                        'quarantine_start' => $quarantineStart,
                        'quarantine_end' => $quarantineEnd,
                        'reason' => Arr::get($data, 'reason'),
                        'comment' => Arr::get($data, 'comment', ''),
                        'refer_mother_code' => optional($propagation->motherPlant)->sync_code,
                        'strain_name' => $propagation->strain->name,
                        'scheduled_by_id' => Arr::get($data, 'scheduled_by_id'),
                    ],
                    $disposeQty['quantity']
                );
                $propagationIterator->next();
            }

            DB::commit();

            return $propagations;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.propagation.destroy_failed'));
        }
    }

    /**
     * @param Propagation $propagation
     * @param Room $disposalRoom
     * @param array $disposalData
     * @param int $qty
     *
     * @return Propagation
     *
     * @throws Throwable
     */
    public function destroy(
        Propagation $propagation,
        Room $disposalRoom,
        array $disposalData = [],
        int $qty = 0
    ): Propagation {
        if ($qty === 0 || $qty > $propagation->quantity) {
            throw new GeneralException(__('exceptions.disposal.invalid_quantity'));
        }
        $disposalData['quantity'] = $qty;
        if ($qty === $propagation->quantity) {
            $propagation->update(['destroyed' => true]);
            /** @var Disposal $disposal */
            $disposal = $propagation->disposals()->make($disposalData);
        } else {
            $newPropagation = $propagation->replicate(['batch_id']);
            $propagation->update(['quantity' => $propagation->quantity - $qty]);
            $newPropagation->fill(['quantity' => $qty, 'destroyed' => true])->save();
            $disposal = $newPropagation->disposals()->make($disposalData);
        }
        $disposal->room()->associate($disposalRoom);
        $disposal->strain()->associate($propagation->strain);
        $disposal->save();

        // dispatch disposal scheduled
        Event::dispatch(new DisposalScheduled($disposal));
        return $propagation;
    }
}
