<?php

namespace App\Synchronizations\Http\QueryIterators;

use Illuminate\Http\Client\RequestException;

class LeafQueryIterator extends BaseQueryIterator
{
    public const MAX_NUMBER_OF_PAGE = 1000;
    private int $page = 1;
    private array $responseData = [];

    /**
     * @return array
     * @throws RequestException
     */
    public function current(): array
    {
        $this->httpResource->addQuery('page', $this->page);
        $this->logger->info('Leaf query', [
            'http_method' => $this->httpResource->getMethod(),
            'request_url' => $this->httpResource->getUrl(),
            'request_query' => $this->httpResource->getQuery(),
            'request_body' => $this->httpResource->getBody(),
        ]);
        $this->responseData = $this->httpClient->doRequest($this->httpResource);
        return $this->responseData['data'];
    }

    public function next(): void
    {
        if ($this->hasMore()) {
            $this->page++;
        }
    }

    /**
     * @return bool
     */
    public function hasMore(): bool
    {
        if ($this->page === 1) {
            return true;
        }

        return !empty($this->responseData['next_page_url']) && $this->page <= self::MAX_NUMBER_OF_PAGE;
    }
}
