<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{License, Strain};
use App\Rules\ModelsExist;
use App\Services\{LicenseService, StrainService};
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Arr;
use Illuminate\Validation\Rules\{In, RequiredIf};
use Throwable;

/**
 * Class StrainController
 *
 * @package App\Http\Controllers
 */
class StrainController extends CRUDController
{
    private LicenseService $licenseService;

    public function __construct(StrainService $service, LicenseService $licenseService, Request $request)
    {
        $this->licenseService = $licenseService;
        parent::__construct($service, $request);
    }

    public function getBulkCreateRules(License $license): array
    {
        $rules = [
            'strains' => ['array', 'required'],
            'strains.*.name' => [
                'required',
                'string',
                'min:3',
                'max:50',
                'distinct',
                "unique:App\Models\Strain,name,NULL,id,license_id,\"{$this->request->get('license_id')}\"",
            ],
            'strains.*.custom_id' => ['nullable', 'string'],
            'strains.*.description' => ['nullable', 'string'],
            'strains.*.image' => [
                'nullable',
                'image',
                'mimetypes:image/jpeg,image/png,image/jpg,image/bmp',
            ],
            'license_id' => ['required', 'uuid'],
            'user_id' => ['required', 'uuid'],
        ];

        if ($license->isMetrcSystem()) {
            $rules = array_merge(
                $rules,
                [
                    'strains.*.testing_status' => ['required', 'string', new In(Strain::TESTING_STATUSES),],
                    'strains.*.thc' => ['nullable', 'numeric', 'min:0', 'not_in:0',],
                    'strains.*.cbd' => ['nullable', 'numeric', 'min:0', 'not_in:0',],
                    'strains.*.indica' => ['required', 'numeric', 'min:0', 'max:100',],
                    'strains.*.sativa' => ['required', 'numeric', 'min:0', 'max:100',],
                    'strains.*' => function ($attribute, $value, $fail) use ($license) {
                        if ($license->isMetrcSystem() && $value['indica'] + $value['sativa'] != 100) {
                            return $fail(
                                __(
                                    'validation.eq',
                                    ['value' => 100, 'attribute' => 'sum of indica and sativa parameters']
                                )
                            );
                        }
                        return true;
                    },
                ]
            );
        }
        return $rules;
    }

    public function getUpdateRules(string $id): array
    {
        $indica = $this->request->get('indica');
        $sativa = $this->request->get('sativa');
        return [
            'name' => [
                'sometimes',
                'required',
                'string',
                'min:3',
                'max:50',
                "unique:App\Models\Strain,name,{$id},id,license_id,\"{$this->request->get('license_id')}\""
            ],
            'description' => ['sometimes', 'nullable', 'string', 'max:2000'],
            'image' => [
                'sometimes',
                'nullable',
                'image',
                'mimetypes:image/jpeg,image/png,image/jpg',
            ],
            'status' => ['sometimes', 'required', 'boolean'],
            'testing_status' => [
                'sometimes',
                'string',
                new In(Strain::TESTING_STATUSES),
            ],
            'thc' => ['nullable', 'numeric', 'min:0', 'not_in:0',],
            'cbd' => ['nullable', 'numeric', 'min:0', 'not_in:0',],
            'indica' => [
                new RequiredIf(!is_null($sativa)),
                'numeric',
                function ($attribute, $value, $fail) use ($sativa) {
                    if (!is_null($sativa) && $sativa + $value != 100) {
                        return $fail(
                            __('validation.eq', ['value' => 100, 'attribute' => 'sum of Indica and Sativa'])
                        );
                    }
                    return true;
                },
            ],
            'sativa' => [
                new RequiredIf(!is_null($indica)),
                'numeric',
                function ($attribute, $value, $fail) use ($indica) {
                    if (!is_null($indica) && $indica + $value != 100) {
                        return $fail(
                            __('validation.eq', ['value' => 100, 'attribute' => 'sum of Indica and Sativa'])
                        );
                    }
                    return true;
                },
            ],
            'user_id' => ['uuid'],
            'custom_id' => ['nullable', 'string']
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems());
    }

    /**
     * @param string $organizationId
     * @return JsonResponse
     */
    public function getByOrganization(string $organizationId): JsonResponse
    {
        $licenses = $this->licenseService->getByOrganization($organizationId);
        return response()->json($this->service->getAllByLicenses($licenses));
    }

    /**
     * @param string $stateCode
     * @return JsonResponse
     */
    public function getByState(string $stateCode): JsonResponse
    {
        $filter = $this->request->get('filter', []);

        return response()->json($this->service->getByLicenses(
            $this->licenseService->getByState($stateCode, Arr::get($filter, 'account_holder_id', '')),
            Criteria::createFromRequest($this->request)
        ));
    }

    public function getByAccountHolder($accountHolderId): JsonResponse
    {
        $filter = $this->request->get('filter', []);
        return response()->json($this->service->getByAccountHolder(Arr::get($filter, 'ids', []), $accountHolderId));
    }

    public function indexByIds(): JsonResponse
    {
        return response()->json($this->service->getItemByIds($this->request->get('ids', []), $this->request->get('strain_name', '')));
    }

    public function getDetailByAccountHolder($id, $accountHolderId): JsonResponse
    {
        return response()->json($this->service->getDetailByAccountHolder($id, $accountHolderId));
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function store(): JsonResponse
    {
        try {
            /** @var License $license */
            $license = $this->licenseService->get($this->request->get('license_id'));
        } catch (ModelNotFoundException $e) {
            throw new GeneralException(__('exceptions.license.invalid'));
        }
        $customAttributes = [
            "strains.*.name" => "name",
            "strains.*.custom_id" => "custom ID",
            "strains.*.description" => "description",
            "strains.*.image" => "image",
        ];

        if ($license->isMetrcSystem()) {
            $customAttributes = array_merge(
                $customAttributes,
                [
                    'strains.*.testing_status' => 'testing status',
                    'strains.*.thc' => 'THC',
                    'strains.*.cbd' => 'CBD',
                    'strains.*.indica' => 'indica',
                    'strains.*.sativa' => 'sativa',
                ]
            );
        }
        return response()->json(
            $this->service->bulkCreate(
                $this->validate($this->request, $this->getBulkCreateRules($license), [], $customAttributes)
            ),
            Response::HTTP_CREATED
        );
    }

    public function changeStatus($status)
    {
        $rules = [
            'ids' => ['array', 'required'],
            'ids.*' => [
                'uuid',
                new ModelsExist('App\Models\Strain')
            ]
        ];
        $strains = $this->service->getEnumerable(Arr::get($this->validate($this->request, $rules), 'ids'));
        return response()->json(
            $this->service->changeStatus($strains, $status),
            Response::HTTP_OK
        );
    }

    public function getCreateRules(): array
    {
        // TODO: Implement getCreateRules() method.
        return [];
    }

    public function import()
    {
        try {
            /** @var License $license */
            $license = $this->licenseService->get($this->request->get('license_id'));
        } catch (ModelNotFoundException $e) {
            throw new GeneralException(__('exceptions.license.invalid'));
        }
        $rules = [
            'strains' => ['array', 'required'],
            'strains.*.name' => [
                'required',
                'string',
                'min:3',
                'max:200',
                'distinct',
            ],
            'strains.*.custom_id' => ['nullable', 'string',],
            'license_id' => ['required', 'uuid'],
            'user_id' => ['required', 'uuid'],
        ];
        return response()->json(
            $this->service->import(
                $this->validate($this->request, $rules)
            ),
            Response::HTTP_CREATED
        );
    }
}
