<?php

namespace App\Events\EndProduct;

use App\Events\Event;
use App\Models\EndProduct;

class EndProductSaved extends Event
{
    /**
     * @var EndProduct
     */
    public EndProduct $endProduct;

    /**
     * @param EndProduct $endProduct
     */
    public function __construct(EndProduct $endProduct)
    {
        $this->endProduct = $endProduct;
    }
}
