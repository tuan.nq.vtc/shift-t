<?php

namespace App\Http\Controllers;

use App\Models\SeedToSale;
use App\Rules\{ModelsExist, SubroomBelongToLicense};
use App\Services\{GrowCycleService, RoomService, SubroomService, PlantService};
use Illuminate\Validation\Rules\In;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Arr;
use Throwable;

/**
 * Class GrowCycleController
 * @package App\Http\Controllers
 *
 * @property GrowCycleService $service
 */
class GrowCycleController extends CRUDController
{
    private RoomService $roomService;
    private SubroomService $subroomService;

    public function __construct(
        GrowCycleService $service,
        RoomService $roomService,
        SubroomService $subroomService,
        Request $request
    )
    {
        parent::__construct($service, $request);
        $this->roomService = $roomService;
        $this->subroomService = $subroomService;
    }

    public function getCreateRules(): array
    {
        return [
            'license_id' => ['required'],
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                "unique:App\Models\GrowCycle,name,null,id,license_id,\"{$this->request->get('license_id')}\",deleted_at,NULL"
            ],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'license_id' => ['required'],
            'name' => [
                'sometimes',
                'required',
                'string',
                'min:3',
                'max:150',
                "unique:App\Models\GrowCycle,name,null,{$id},license_id,\"{$this->request->get('license_id')}\",deleted_at,NULL"
            ],
        ];
    }

    public function stats()
    {
        return response()->json($this->service->stats($this->request->get('license_id', '')), Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function changeGrowStatus(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $rules = [
            'license_id' => ['required'],
            'grow_status' => [
                'required',
                'string',
                new In(SeedToSale::GROW_CYCLE_GROW_STATUSES),
            ],
            'ids' => [
                'required',
                'array',
                new ModelsExist(
                    'App\Models\GrowCycle', 'id',
                    ['license_id' => $licenseId, 'deleted_at' => null]
                ),
            ],
        ];
        $data = $this->validate($this->request, $rules);

        return response()->json(
            $this->service->updateGrowCyclesGrowStatus(
                $this->service->getEnumerable(Arr::get($data, 'ids', [])),
                Arr::get($data, 'grow_status')
            )
        );
    }

    public function index(): JsonResponse
    {
        return response()->json(
            $this->service->getAvailableItems($this->request->get('license_id', '')),
            Response::HTTP_OK
        );
    }

    public function moveToRoom(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');
        $rules = [
            'license_id' => ['required'],
            'ids' => [
                'required',
                new ModelsExist(
                    'App\Models\GrowCycle',
                    'id',
                    ['license_id' => $licenseId, 'deleted_at' => null]
                ),
            ],
            'room_id' => [
                'required',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'subroom_id' => [
                'nullable',
                'exists:App\Models\Subroom,id,room_id,' . $roomId . ',deleted_at,NULL',
            ]
        ];

        $data = $this->validate($this->request, $rules);
        return response()->json(
            $this->service->moveToRoom(
                $this->service->getEnumerable(
                    Arr::get($data, 'ids'),
                    ['plantGroups', 'plantGroups.plants']
                ),
                $this->roomService->get(Arr::get($data, 'room_id')),
                ($subroomId = Arr::get($data, 'subroom_id')) ? $this->subroomService->get($subroomId) : null
            ),
            Response::HTTP_OK
        );
    }
}
