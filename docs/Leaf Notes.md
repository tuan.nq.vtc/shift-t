### Propagations

#### Related Entities
- Strain
- Room (aka Area)
- Room Type (quarantine or non-quarantine)
- Batch
- Inventory Category
- Inventory Sub Category
- Inventory (aka Lot)
- Plant

#### Functions
- Create a propagation then do sync to LEAF via `batches` API endpoint, the workflow will be:
    - Create new `batch`:
        - Type: `propagation material`.
        - Child/Parent: none.
    - Create a `inventory type`:
        - Name format: `{strain_name} {batch_type} {source_type}`(ex: **"Strain X propagation material seed"**).
        - Category: **Immature Plant**.
        - Sub category: **Seed**.
        - Unit of mesure: *ea** (each).
    - Create an `inventory`:
        - Belongs to the created `batch`.
        - Belongs to the created `inventory type` with `category`, `sub category`, `uom`.
        - Has plant propagation source (`source_type`).
        - Has quantity
        - Has `room` name & `room type` (quarantine or non-quarantine) 
- Do `move_inventory_to_plant` on a `batch` of `propagation material` type, the workflow will be:
    - Update propagation `batch`:
        - Status: `closed`
    - Create a new plant `batch`:
        - Type: `plant`
    - Set propagation `batch` as parent of plant `batch`.
    - Decrease quantity of the `inventory` of the propagation `batch`.
    - Create `plant(s)` based on decreased quantity of propagation `inventory`.
        - Room: Same with parent batch.
        - Strain: Same with parent batch.
        - Growth phase (Growth status): `growing`

### Plants

#### Related Entities
- Strain
- Source Type
- Room
- Grow Cycle
- Grow Status
- Inventory

#### Functions
- Create plant by `move_inventory_to_plant`



### Inventory

"created_at": "06/30/2020 01:28am",
"updated_at": "06/30/2020 01:28am",
"external_id": "",
"released_by_state": null,
"lab_retest_id": null,
"is_initial_inventory": "0",
"net_weight": "0.00",
"inventory_created_at": "",
"inventory_expires_at": "",
"inventory_packaged_at": "",
"qty": "35.0000",
"packed_qty": null,
"cost": "0.00",
"value": "0.00",
"source": null,
"propagation_source": "seed",
"uom": "ea",
"total_marijuana_in_grams": "0.00",
"additives": "",
"serving_num": "",
"serving_size": "",
"marijuana_type": null,
"sent_for_testing": "0",
"deleted_at": null,
"last_harvest_stage": null,
"medically_compliant": null,
"medically_compliant_status": "no",
"global_id": "WAJ1965.IN5IIE",
"legacy_id": null,
"lab_result_file_path": null,
"lab_results_attested": "0",
"lab_results_date": "",
"global_original_id": "",
"batch_type": "propagation material",
"global_batch_id": "WAJ1965.BA6LPQ",
"global_area_id": "WAJ1965.AR4CPE",
"global_lab_result_id": null,
"global_strain_id": "WAJ1965.ST5SOY",
"global_inventory_type_id": "WAJ1965.TY7Q0T",
"global_created_by_mme_id": "",
"global_mme_id": "WASTATE1.MM2IV",
"global_user_id": "WASTATE1.US2WU",
"high_cbd": false,
"high_thc": false,
"general_use": false,
"labResults": []
