<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\Disposal;
use App\Rules\UnscheduleDisposalRules;
use App\Services\DisposalService;
use Illuminate\Http\{JsonResponse, Request, Response};
use Laravel\Lumen\Routing\Controller;

/**
 * Class DisposalController
 * @package App\Http\Controllers
 */
class DisposalController extends Controller
{
    /**
     * @var DisposalService
     */
    protected DisposalService $service;

    /**
     * @var Request
     */
    protected Request $request;

    public function __construct(DisposalService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     *
     * @param string $status
     * @return JsonResponse
     */
    public function listByStatus(string $status)
    {
        $criteria = Criteria::createFromRequest($this->request);
        $statusMapping = [
            'scheduled' => Disposal::STATUS_SCHEDULED,
            'processing' => Disposal::STATUS_READY,
            'destroyed' => Disposal::STATUS_DESTROYED
        ];
        $criteria->addFilter('status', $statusMapping[$status]);
        return response()->json($this->service->list($criteria));
    }

    /**
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function destroy()
    {
        $rules = [
            'ids' => ['required', 'array'],
            'ids.*' => [
                'required',
                "exists:App\Models\Disposal,id,license_id,\"{$this->request->get('license_id')}\",status," . Disposal::STATUS_READY,
            ],
            'destroyed_at' => ['required', 'date'],
            'destroyed_by_id' => ['required', 'uuid', 'exists:App\Models\User,id',],

        ];
        $data = $this->validate($this->request, $rules);
        $disposals = $this->service->getEnumerable($data['ids']);
        return response()->json($this->service->destroy($disposals, $data));
    }


    public function unschedule()
    {
        $rules = [
            'disposals' => ['required', 'array'],
            'disposals.*' => ['required', new UnscheduleDisposalRules($this->request->get('license_id')),],
            'comment' => ['nullable', 'string'],
            'license_id' => ['required', 'uuid']
        ];
        return response()->json(
            $this->service->unschedule($this->validate($this->request, $rules)),
            Response::HTTP_CREATED
        );
    }
}
