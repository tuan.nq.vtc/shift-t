<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintLabelItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_label_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('print_label_id')->index();
            $table->string('type')->index()->comment('qrcode,barcode,text,date');
            $table->string('mapping_field')->nullable();
            $table->json('box')->nullable(); // width, height, top, left
            $table->integer('zIndex')->nullable();
            $table->integer('font_size')->default(14);
            $table->string('font_weight')->nullable();
            $table->string('barcode_width')->default(0);
            $table->string('content')->nullable();
            $table->string('label')->nullable();
            $table->unsignedTinyInteger('show_label')->default(0);
            $table->unsignedTinyInteger('auto_fit')->default(0);
            $table->unsignedTinyInteger('is_custom')->default(0);
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('print_label_id')->references('id')->on('print_labels')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_label_items');
    }
}
