<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\SourceProduct;
use App\Services\SourceProductService;
use Illuminate\Validation\Rules\In;
use Illuminate\Http\{JsonResponse, Request};

class SourceProductController extends CRUDController
{
    public function __construct(SourceProductService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        $licenseId = $this->request->get('license_id', null);
        return [
            'sku' => [
                'required',
                'string',
                'regex:/^[a-zA-Z0-9-]+$/i',
                'unique:App\Models\SourceProduct,sku,NULL,id,deleted_at,NULL,license_id,' . $licenseId
            ],
            'name' => ['required', 'string', 'max:40'],
            'price' => ['nullable', 'numeric', 'min:0'],
            'strain_id' => [
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,' . $licenseId,
            ],
            'source_category_id' => [
                'required',
                'uuid',
                'exists:App\Models\SourceCategory,id,deleted_at,NULL,account_holder_id,' . $this->request->get(
                    'account_holder_id',
                    null
                ),
            ],
            'net_weight' => ['sometimes', 'nullable', 'numeric', 'min:0'],
            'uom' => ['sometimes', 'nullable', 'string', new In(SourceProduct::UOM_LIST)],
            'pieces' => ['sometimes', 'nullable', 'numeric'],
            'print_label_id' => [
                'sometimes',
                'nullable',
                'uuid',
                'exists:App\Models\PrintLabel,id,deleted_at,NULL'
            ],
            'description' => ['sometimes', 'nullable', 'string', 'max:200'],
            'disclaimer' => ['sometimes', 'nullable', 'string', 'max:200'],
            'status' => ['sometimes', 'required', 'boolean'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id', null);
        return [
            'sku' => [
                'sometimes',
                'required',
                'string',
                'regex:/^[a-zA-Z0-9-]+$/i',
                'unique:App\Models\SourceProduct,sku,' . $id . ',id,deleted_at,NULL,license_id,' . $licenseId
            ],
            'name' => ['sometimes', 'required', 'string', 'max:40'],
            'price' => ['sometimes', 'nullable', 'numeric', 'min:0'],
            'strain_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,' . $licenseId,
            ],
            'source_category_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\SourceCategory,id,deleted_at,NULL,account_holder_id,' . $this->request->get(
                    'account_holder_id',
                    null
                ),
            ],
            'net_weight' => ['sometimes', 'nullable', 'numeric', 'min:0'],
            'uom' => ['sometimes', 'nullable', 'string'],
            'pieces' => ['sometimes', 'nullable', 'numeric'],
            'print_label_id' => [
                'sometimes',
                'nullable',
                'uuid',
                'exists:App\Models\PrintLabel,id,deleted_at,NULL'
            ],
            'description' => ['sometimes', 'nullable', 'string', 'max:200'],
            'disclaimer' => ['sometimes', 'nullable', 'string', 'max:200'],
            'status' => ['sometimes', 'required', 'boolean'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems(Criteria::createFromRequest($this->request)));
    }
}
