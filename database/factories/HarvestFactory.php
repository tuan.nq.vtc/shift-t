<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Harvest, HarvestGroup, InventoryType, License, Room, Strain, TraceableModel};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Harvest::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Harvest::class,
                $data['license_id']
            ),
            'harvest_group_id' => function ($data) {
                return factory(HarvestGroup::class)->create(['license_id' => $data['license_id']])->id;
            },
            'room_id' => function ($data) {
                return factory(Room::class)->create(['license_id' => $data['license_id']])->id;
            },
            'strain_id' => function ($data) {
                return factory(Strain::class)->create(['license_id' => $data['license_id']])->id;
            },
            'flower_wet_weight' => $faker->randomFloat(2, 0, 1000),
            'material_wet_weight' => $faker->randomFloat(2, 0, 1000),
            'waste_wet_weight' => $faker->randomFloat(2, 0, 1000),
            'flower_dry_weight' => $faker->randomFloat(2, 0, 1000),
            'material_dry_weight' => $faker->randomFloat(2, 0, 1000),
            'waste_dry_weight' => $faker->randomFloat(2, 0, 1000),
            'flower_room_id' => fn($data) => factory(Room::class)->create(['license_id' => $data['license_id']])->id,
            'material_room_id' => fn($data) => factory(Room::class)->create(['license_id' => $data['license_id']])->id,
            'waste_room_id' => fn($data) => factory(Room::class)->create(['license_id' => $data['license_id']])->id,
            'flower_inventory_type_id' => fn($data) => factory(InventoryType::class)->create(['license_id' => $data['license_id']])->id,
            'material_inventory_type_id' => fn($data) => factory(InventoryType::class)->create(['license_id' => $data['license_id']])->id,
            'start_at' => $faker->dateTimeThisYear(),
            'finished_at' => $faker->dateTimeThisYear(),
            'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
            'synced_at' => $faker->dateTimeThisYear(),
        ];
    }
);
