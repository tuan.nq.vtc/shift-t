### Create planting
```bash
curl --location --request POST 'https://sandbox-api-co.metrc.com/plantbatches/v1/createplantings?licenseNumber=403R-X0002' \
--header 'Content-Type: application/json' \
--header 'Authorization: Basic b0dib3BJR0ZaR0lCWm53LTlVRnFqT3VmTmwzVjBPWnRsV2hwUFY3U3VaaXk5RGxPOkVCUk9keS1ORFVGaGhMNk04eFF2OXk0Z3hjNlZrR2tpT2xCU2pVRzhZQnZyNnNzbQ==' \
--data-raw '[
  {
    "Name": "B. Kush 5-30-232",
    "Type": "Clone",
    "Count": 10,
    "Strain": ".99 La Marijuana",
    "Location": null,
    "PatientLicenseNumber": "",
    "ActualDate": "2020-10-05"
  }
]'
```

# Flow

### Create Propagation
`POST {{EndPoint}}/plantbatches/v1/createplantings?licenseNumber={{licenseNumber}}`
```
[
  {
    "Name": "Cody Test 2",
    "Type": "Clone",
    "Count": 6,
    "Strain": "Spring Hill Kush",
    "Location": null,
    "ActualDate": "2020-12-15"
  }
]
```

### Propagation to Plants
`POST {{EndPoint}}/plantbatches/v1/changegrowthphase?licenseNumber={{licenseNumber}}`
- Vegetative
```
[
  {
    "Name": "Cody Test 2",
    "Count": 3,
    "StartingTag": "1A4FFFE203D7E31000000563",
    "GrowthPhase": "Vegetative",
    "NewLocation": "A1 - Vegetative",
    "GrowthDate": "2020-12-15",
    "PatientLicenseNumber": "X00002"
  }
]
```
- Flowering
```
[
  {
    "Name": "Cody Test 2",
    "Count": 3,
    "StartingTag": "1A4FFFE203D7E31000000563",
    "GrowthPhase": "Flowering",
    "NewLocation": "A1 - Flowering",
    "GrowthDate": "2020-12-15",
    "PatientLicenseNumber": "X00002"
  }
]
```

### Harvest Plants
Note: need to get Harvest ID for the finish step
`POST {{EndPoint}}/plants/v1/harvestplants?licenseNumber={{licenseNumber}}`
```
[
  {
    "Plant": "1A4FFFE203D7E31000000564",
    "Weight": 68.68,
    "UnitOfWeight": "Grams",
    "DryingLocation": "Drying Room",
    "HarvestName": "2020-11-12-Drying Room-Cody",
    "PatientLicenseNumber": "X00001",
    "ActualDate": "2020-11-12"
  },
  {
    "Plant": "1A4FFFE203D7E31000000565",
    "Weight": 96.69,
    "UnitOfWeight": "Grams",
    "DryingLocation": "Drying Room",
    "HarvestName": "2020-11-12-Drying Room-Cody",
    "PatientLicenseNumber": "X00001",
    "ActualDate": "2020-11-12"
  }
]
```

### Create Package (Unlotted Inventory)
`POST {{EndPoint}}/harvests/v1/create/packages?licenseNumber={{licenseNumber}}`
```
[
  {
    "Tag": "1A4FFFD203D7E31000000119",
    "Location": null,
    "Item": "Buds",
    "UnitOfWeight": "Grams",
    "PatientLicenseNumber": "X00001",
    "Note": "This is a note.",
    "IsProductionBatch": false,
    "ProductionBatchNumber": null,
    "IsTradeSample": false,
    "IsDonation": false,
    "ProductRequiresRemediation": false,
    "RemediateProduct": false,
    "RemediationMethodId": null,
    "RemediationDate": null,
    "RemediationSteps": null,
    "ActualDate": "2020-11-15",
    "Ingredients": [
      {
        "HarvestName": "2020-11-12-Drying Room-Cody",
        "Weight": 100.37,
        "UnitOfWeight": "Grams"
      }
    ]
  }
]
```

### Finish Harvest
`POST {{EndPoint}}/harvests/v1/finish?licenseNumber={{licenseNumber}}`
```
[
  {
    "Id": 64201,
    "ActualDate": "2020-11-14"
  }
]
```

### Split package (inventory)
`POST {{EndPoint}}/packages/v1/create?licenseNumber={{licenseNumber}}`
```
[
  {
    "Tag": "1A4FFFD203D7E31000000118",
    "Location": null,
    "Item": "Buds",
    "Quantity": 80.0,
    "UnitOfMeasure": "Grams",
    "PatientLicenseNumber": "X00001",
    "Note": "This is a note.",
    "IsProductionBatch": false,
    "ProductionBatchNumber": null,
    "IsDonation": false,
    "ProductRequiresRemediation": false,
    "UseSameItem": false,
    "ActualDate": "2015-12-15",
    "Ingredients": [
      {
        "Package": "1A4FFFD203D7E31000000113",
        "Quantity": 18.0,
        "UnitOfMeasure": "Grams"
      }
    ]
  }
]
```
