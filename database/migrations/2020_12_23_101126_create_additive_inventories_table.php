<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditiveInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(
            'additive_inventories',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('additive_id')->references('id')->on('additives')->cascadeOnDelete();
                $table->double('total_quantity', 10, 2)->default(0);
                $table->double('available_quantity', 10, 2)->default(0);
                $table->double('unit_price', 10, 2)->default(0);
                $table->double('total_price', 10, 2)->default(0);
                $table->string('uom', 50)->default('Gallons');
                $table->date('order_date')->nullable();
                $table->uuid('supplier_id')->references('id')->on('suppliers')->cascadeOnDelete();

                $table->uuid('created_by')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additive_inventories');
    }
}
