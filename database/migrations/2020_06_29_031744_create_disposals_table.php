<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'disposals',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('room_id')->index()->nullable();
                $table->integer('status')->default(0)->index();
                $table->decimal('quantity', 12, 2)->default(0)->index();
                $table->string('uom')->nullable();
                $table->timestamp('destroyed_at')->nullable();
                $table->timestamp('cancelled_at')->nullable();
                $table->timestamp('wasted_at')->nullable();
                $table->timestamp('quarantine_start')->nullable();
                $table->timestamp('quarantine_end')->nullable();
                $table->string('reason')->nullable();
                $table->text('comment')->nullable();
                $table->text('comment_unschedule')->nullable();
                $table->nullableUuidMorphs('wastable');
                $table->string('refer_mother_code')->nullable();
                $table->uuid('scheduled_by_id')->index()->nullable();
                $table->uuid('destroyed_by_id')->index()->nullable();
                $table->json('info')->nullable();

                $table->uuid('strain_id')->nullable();
                // S2S Trace sync[Create/Update/Delete]
                $table->string('sync_code')->nullable()->index();
                $table->unsignedTinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposal');
    }
}
