<?php

namespace Bamboo\StreamProcessing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FailedEvent extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'topic',
        'payload',
        'metadata',
        'exception',
    ];

    protected $casts = [
        'payload' => 'array',
        'metadata' => 'array',
    ];

    protected $dates = ['failed_at'];
}
