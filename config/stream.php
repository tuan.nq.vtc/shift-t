<?php

use App\Streams\ConsumerEventProcessors\LicenseProcessor;
use App\Streams\ConsumerEventProcessors\UserProcessor;
use App\Streams\ConsumerEventProcessors\ProductProcessor;

return [
    'default' => env('STREAM_CHANNEL', 'kafka'),
    'channels' => [
        'kafka' => [
            'connection' => [
                'global' => [
                    'group.id' => env('KAFKA_GROUP_ID'),
                    'metadata.broker.list' => env('KAFKA_HOST', 'kafka').':'.env('KAFKA_PORT', '9092'),
                    'enable.auto.commit' => 'false',
//                        'sasl.username' => env('KAFKA_SYNC_PASSWORD'),
//                        'sasl.password' => env('KAFKA_SYNC_USERNAME'),

                ],
                'topic' => ['auto.offset.reset' => 'beginning',],
            ]
        ],
    ],
    'consumers' => [
        'processes' => [
            env('STREAM_TOPIC_USER', 'user') => UserProcessor::class,
            env('STREAM_TOPIC_LICENSE', 'license') => LicenseProcessor::class,
            env('STREAM_TOPIC_PRODUCT', 'product') => ProductProcessor::class,
        ]
    ],
    'producers' => [
        'topics' => [
            'user' => env('STREAM_TOPIC_USER', 'user'),
            'license-sync-aggregate' => env('STREAM_TOPIC_LICENSE_SYNC_AGGREGATE', 'license-sync-aggregate'),
        ]
    ]
];
