<?php

namespace Bamboo\StreamProcessing\Resolvers;

use Bamboo\StreamProcessing\Contracts\ConsumerEventProcessInterface;
use Exception;

class ConsumerEventProcessResolver
{
    /**
     * @param string $topicName
     * @return ConsumerEventProcessInterface
     *
     * @throws Exception
     */
    public function resolve(string $topicName): ConsumerEventProcessInterface
    {
        $processes = config('stream.consumers.processes');;
        if (!empty($processes[$topicName])) {
            return new $processes[$topicName]();
        }

        throw new Exception(
            "Can not resolve consumer event process of topic {$topicName}"
        );
    }
}
