<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteBagStrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_bag_strains', function (Blueprint $table) {
            $table->uuid('waste_bag_id');
            $table->uuid('strain_id');

            $table->foreign('waste_bag_id')->references('id')->on('waste_bags')->cascadeOnDelete();
            $table->foreign('strain_id')->references('id')->on('strains')->cascadeOnDelete();

            $table->primary(['waste_bag_id', 'strain_id'], 'waste_bag_strain_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_bag_strains');
    }
}
