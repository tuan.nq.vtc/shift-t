<?php

namespace Bamboo\StreamProcessing\Listeners;

use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Events\{StreamableResourceCreated, StreamableResourceDeleted, StreamableResourceUpdated};
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Events\Dispatcher;
use Ramsey\Uuid\Uuid;
use Throwable;

class StreamableResourceEventSubscriber
{
    /**
     * @param StreamableResourceCreated $event
     *
     * @throws Throwable
     */
    public function onCreated(StreamableResourceCreated $event)
    {
        $this->publishStreamEvent($event->streamableResource, StreamableResourceInterface::ACTION_CREATE);
    }

    /**
     * @param StreamableResourceInterface $streamableResource
     * @param string $action
     * @throws Exception
     */
    private function publishStreamEvent(StreamableResourceInterface $streamableResource, string $action)
    {
        if (!$streamableResource->getTopic()) {
            throw new Exception('No topic is configured');
        }

        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            $streamableResource->getStreamEventPayload(),
            [
                'action' => $action,
            ]
        );
        dispatch(new PublishStreamEventJob($streamableResource->getTopic(), $messageEvent));
    }

    /**
     * @param StreamableResourceDeleted $event
     *
     * @throws Throwable
     */
    public function onDeleted(StreamableResourceDeleted $event)
    {
        $this->publishStreamEvent($event->streamableResource, StreamableResourceInterface::ACTION_DELETE);
    }

    /**
     * @param StreamableResourceUpdated $event
     *
     * @throws Throwable
     */
    public function onUpdated(StreamableResourceUpdated $event)
    {
        $streamableResource = $event->streamableResource;
        // skip if the changed field is not streamable
        if (!$streamableResource->isStreamUpdatable()) {
            return;
        }
        $this->publishStreamEvent($streamableResource, StreamableResourceInterface::ACTION_UPDATE);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            StreamableResourceCreated::class,
            'Bamboo\StreamProcessing\Listeners\StreamableResourceEventSubscriber@onCreated'
        );

        $events->listen(
            StreamableResourceDeleted::class,
            'Bamboo\StreamProcessing\Listeners\StreamableResourceEventSubscriber@onDeleted'
        );

        $events->listen(
            StreamableResourceUpdated::class,
            'Bamboo\StreamProcessing\Listeners\StreamableResourceEventSubscriber@onUpdated'
        );
    }
}
