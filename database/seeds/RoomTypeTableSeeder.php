<?php

use App\Models\{RoomType, User};
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

/**
 * Class RoomTypeTableSeeder.
 */
class RoomTypeTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        $defaultTypes = [
            'Cultivation' => [
                RoomType::CATEGORY_PLANT
            ],
            'Inventory' => [
                RoomType::CATEGORY_INVENTORY,
            ],
            'Seedling' => [
                RoomType::CATEGORY_PROPAGATION,
            ],
            'Flowering' => [
                RoomType::CATEGORY_PLANT,
            ],
            'Harvest' => [
                RoomType::CATEGORY_HARVEST,
            ],
            'Drying' => [
                RoomType::CATEGORY_HARVEST,
            ],
            'Quarantine' => [
                RoomType::CATEGORY_HARVEST,
            ],
            'Mothers' => [
                RoomType::CATEGORY_PROPAGATION,
            ],
            'Preveg' => [
                RoomType::CATEGORY_PROPAGATION,
            ],
            'Clones' => [
                RoomType::CATEGORY_PROPAGATION,
            ],
            'Waste' => [
                RoomType::CATEGORY_DISPOSAL,
            ],
            'Disposal' => [
                RoomType::CATEGORY_DISPOSAL,
            ]
        ];

        User::pluck('account_holder_id')->unique()
            ->each(function($accountHolderId) use ($defaultTypes) {
                foreach ($defaultTypes as $typeName => $categoryCodes) {
                    factory(RoomType::class)->create(
                        [
                            'name' => $typeName,
                            'status' => RoomType::STATUS_ENABLED,
                            'account_holder_id' => $accountHolderId,
                            'category' => Arr::random(
                                $categoryCodes
                            )
                        ]
                    );
                }
            });
    }
}
