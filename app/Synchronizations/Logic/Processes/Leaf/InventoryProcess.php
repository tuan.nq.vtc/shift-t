<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch, Harvest, Inventory, InventoryType, Room, SourceProduct, Strain, Trace, TraceableModel};
use App\Services\LicenseService;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Exceptions\Pull\NoImportableDataException;
use App\Synchronizations\Helpers\LeafHelper;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class InventoryProcess
 * @property InventoryType $traceableModel
 */
class InventoryProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'm/d/Y G:ia';

    /**
     * @return void
     * @throws Throwable
     */

    /**
     * @return void
     * @throws Throwable
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;

        $hasNextImport = true;
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }
        try {
            $reports = $this->importData($payload['data'] ?? []);
        } catch (NoImportableDataException $e) {
            if ($currentPage !== 1) {
                $hasNextImport = false;
            }
            $reports = [];
        } catch (Throwable $e) {
            throw $e;
        }
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));

        if ($lastPage === 1) {
            $hasNextImport = false;
        }
        $nextPage = $currentPage === 1 ? $lastPage : $currentPage - 1;
        if ($hasNextImport && $nextPage <= 1) {
            $hasNextImport = false;
        }
        if ($hasNextImport) {
            $children = $this->trace->children;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }

            // create trace from last page to second page
            $conditions['page'] = $nextPage;
            $nextPageTrace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => get_classname($this->traceableModel),
                    'status' => Trace::STATUS_PENDING,
                    'resource_conditions' => $conditions,
                ]
            );

            // reassign the children of current trace to next page trace
            $nextPageTrace->children()->attach($children);
            dispatch(new SyncData($nextPageTrace));

            $this->trace->children()->detach();
        }

        // dispatch result after imported
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $hasNextImport ? StreamableResourceInterface::ACTION_UPDATE : 'finish',
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     * @throws Exception
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $licenseId = $this->traceableModel->license->id;

        // prepare data
        $syncCodes = $batchSyncCodes = $strainSyncCodes = $roomSyncCodes = $inventoryTypeSyncCodes = [];
        $importableData = [];
        foreach ($data as $item) {
            if (!empty($item['global_id'])) {
                $syncCodes[$item['global_id']] = 1;
            }
            if (!empty($item['global_strain_id'])) {
                $strainSyncCodes[$item['global_strain_id']] = 1;
            }
            if (!empty($item['global_area_id'])) {
                $roomSyncCodes[$item['global_area_id']] = 1;
            }
            if (!empty($item['global_inventory_type_id'])) {
                $inventoryTypeSyncCodes[$item['global_inventory_type_id']] = 1;
            }
            if (!empty($item['global_batch_id'])) {
                $batchSyncCodes[$item['global_batch_id']] = 1;
            }
            $importableData[] = $item;
        }

        /**
         * ['sync_code' => 'id']
         */
        $availableStrainArray = Strain::licenseId($licenseId)->whereIn('sync_code', array_keys($strainSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $availableRoomArray = Room::licenseId($licenseId)->whereIn('sync_code', array_keys($roomSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => ['id', 'sync_code', 'name']]
         */
        $availableInventoryTypeCollection = InventoryType::licenseId($licenseId)->whereIn(
            'sync_code',
            array_keys($inventoryTypeSyncCodes)
        )->select('id', 'sync_code', 'name',)->get();
        $availableInventoryTypeArray = $availableInventoryTypeCollection->keyBy('sync_code')->toArray();
        $availableInventoryTypeNames = $availableInventoryTypeCollection->pluck('id', 'name')->toArray();

        /**
         * ['name' => 'id']
         */
        $availableSourceProductArray = SourceProduct::licenseId($licenseId)->whereIn(
            'name',
            $availableInventoryTypeNames
        )->pluck('id', 'name')->toArray();
        /**
         * ['sync_code' => 'id']
         */
        $existingInventoryArray = Inventory::licenseId($licenseId)->whereIn('sync_code', array_keys($syncCodes))
            ->pluck('id', 'sync_code')->toArray();

        $existingBatchCollection = Batch::licenseId($licenseId)->whereIn('sync_code', array_keys($batchSyncCodes))
            ->select('id', 'sync_code', 'source_id', 'source_type')->get();
        /**
         * ['sync_code' => ['id', 'sync_code', 'source_id', 'source_type']]
         */
        $existingBatchArray = $existingBatchCollection->keyBy('sync_code')->toArray();
        $usedStrainIds = $usedRoomIds = [];
        $newInventoryData = [];
        foreach ($importableData as $item) {
            try {
                if (!($inventoryTypeCode = Arr::get($item, 'global_inventory_type_id'))
                    || empty($availableInventoryTypeArray[$inventoryTypeCode])) {
                    continue;
                }
                $inventoryTypeId = $availableInventoryTypeArray[$inventoryTypeCode]['id'];
                $inventoryTypeName = $availableInventoryTypeArray[$inventoryTypeCode]['name'];

                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingInventoryArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }

                $roomSyncCode = Arr::get($item, 'global_area_id');
                if (!$roomSyncCode || empty($availableRoomArray[$roomSyncCode])) {
                    throw new Exception('Room not found');
                }
                $roomId = $availableRoomArray[$roomSyncCode];
                $usedRoomIds[$roomId] = 1;

                $strainId = null;
                if (($strainSyncCode = Arr::get($item, 'global_strain_id'))
                    && !empty($availableStrainArray[$strainSyncCode])) {
                    $strainId = $availableStrainArray[$strainSyncCode];
                    $usedStrainIds[$strainId] = 1;
                }

                $isLotted = true;
                $sourceId = $sourceType = $productId = $productType = null;
                if (!empty($availableSourceProductArray[$inventoryTypeName])) {
                    $productId = $availableSourceProductArray[$inventoryTypeName];
                    $productType = SourceProduct::class;
                    #todo implement End product
                }
                if (($batchSyncCode = Arr::get($item, 'global_batch_id'))
                    && !empty($existingBatchArray[$batchSyncCode])) {
                    $sourceId = $existingBatchArray[$batchSyncCode]['source_id'];
                    $sourceType = $existingBatchArray[$batchSyncCode]['source_type'];
                    if ($sourceType === Harvest::class) {
                        $isLotted = false;
                    }
                }
                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                $uom = null;
                if (Arr::get($item, 'uom')) {
                    $uom = LeafHelper::mapToLeafUom(Arr::get($item, 'uom'), true);
                }
                $newInventoryData[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'license_id' => $licenseId,
                    'internal_id' => ModelHelper::generateInternalId(
                        Inventory::class,
                        $licenseId
                    ),
                    'type_id' => $inventoryTypeId,
                    'room_id' => $roomId,
                    'qty_on_hand' => Arr::get($item, 'qty', 0),
                    'strain_id' => $strainId,
                    'source_id' => $sourceId,
                    'is_lotted' => $isLotted,
                    'source_type' => $sourceType,
                    'product_id' => $productId,
                    'product_type' => $productType,
                    'sync_code' => $globalId,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => $today,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        try {
            DB::beginTransaction();
            if (!empty($newInventoryData)) {
                Inventory::insert($newInventoryData);
            }

            // enable used strains
            if (!empty($usedStrainIds)) {
                Strain::query()->where('status', Strain::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedStrainIds))
                    ->update(['status' => Strain::STATUS_ENABLED]);
            }

            // enable used rooms
            if (!empty($usedRoomIds)) {
                Room::query()->where('status', Room::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedRoomIds))
                    ->update(['status' => Room::STATUS_ENABLED]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['success' => $success, 'error' => $error];
    }

    /**
     * @throws Exception
     */
    protected function splitLot(): void
    {
        $payload = $this->payload;
        $globalId = $this->getGlobalId($payload);
        if (!$globalId) {
            throw new Exception(
                "Not found any record matched with [external_id: {$this->traceableModel->id}] in LEAF API responseData"
            );
        }

        $this->traceableModel->updateSyncCode($globalId);
        // update inventory type or batch?!
    }
}
