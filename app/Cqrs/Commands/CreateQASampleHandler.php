<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Handleable;
use App\Exceptions\GeneralException;
use App\Helpers\ModelHelper;
use App\Models\Inventory;
use App\Models\InventoryHistory;
use App\Models\License;
use App\Models\QASample;
use App\Models\StateCategory;
use App\Models\TraceableModel;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Throwable;

class CreateQASampleHandler extends Handleable
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var Inventory
     */
    private $inventory;

    /**
     * @var QASample
     */
    private $model;

    /**
     * @param Inventory $inventory
     * @param array $config
     */
    public function __construct(
        QASample $model,
        Inventory $inventory,
        LoggerInterface $logger,
        array $config
    ) {
        $this->config = $config;
        $this->logger = $logger;
        $this->model = $model;
        $this->inventory = $inventory;
    }

    /**
     * @param CreateQASampleCommand $command
     * @return Collection
     * @throws GeneralException
     * @throws Throwable
     */
    public function handle(CreateQASampleCommand $command): Collection
    {
        try {
            $errorsBag = $this->ensureCanCreateQASample($command);

            if (!empty($errorsBag)) {
                $this->log($command, $errorsBag);
                throw new Exception('Invalid request');
            }

            return $this->performCreateQASample($command);
        } catch (Throwable $e) {
            if (!$this->isRecordedErrors()) {
                $this->log($command, $e->getMessage(), $e->getTrace());
            }
            throw $e;
        }
    }

    /**
     * @param CreateQASampleCommand $command
     * @return array
     */
    private function ensureCanCreateQASample(CreateQASampleCommand $command): array
    {
        $errorsBag = [];

        foreach ($command->getItems() as $item) {
            try {
                $inventory = Inventory::find($inventoryId = $item->getInventoryId());
                if ($inventory instanceof Inventory !== true) {
                    throw new Exception(sprintf('The inventory %s wasn\'t found', $inventoryId));
                }

                $stateCategory = $inventory->getStateCategory();
                if ($stateCategory instanceof StateCategory !== true) {
                    throw new Exception(sprintf('The State Category  of inventory \'%s\' not found', $inventoryId));
                }

                if (!$this->canCreateQASample($stateCategory)) {
                    throw new Exception(
                        sprintf('The regulator \'%s\' isn\'t permitted to create qa Sample', $stateCategory->name),
                        ResponseAlias::HTTP_FORBIDDEN
                    );
                }

                $qtyAvailable = $inventory->getAttribute('qty_available');
                if ($qtyAvailable < $item->getWeight()) {
                    throw new Exception('weight exceeds the Quantity Available');
                }
            } catch (Exception $e) {
                $errorsBag[] = $e->getMessage();
            }
        }

        return $errorsBag;
    }

    /**
     * @param StateCategory $stateCategory
     * @return bool
     */
    private function canCreateQASample(StateCategory $stateCategory): bool
    {
        $regulator = strtolower($stateCategory->getAttribute('regulator'));
        $name = $stateCategory->getAttribute('name');

        return isset($this->config[$regulator])
            && is_array($this->config[$regulator])
            && in_array($name, $this->config[$regulator]);
    }

    /**
     * @param CreateQASampleCommand $command
     * @return Collection
     * @throws Throwable
     */
    private function performCreateQASample(CreateQASampleCommand $command): Collection
    {
        try {
            DB::beginTransaction();

            $qaSampleData = [];
            $license = License::query()->findOrFail($command->getLicenseId());
            foreach ($command->getItems() as $qaSample) {
                $inventory = Inventory::find($qaSample->getInventoryId());
                if ($inventory instanceof Inventory !== true) {
                    continue;
                }
                $internalId = ModelHelper::generateInternalId(get_class($this->model), $command->getLicenseId());
                $qaSampleData[] =
                    [
                        'license_id' => $command->getLicenseId(),
                        'internal_id' => $internalId,
                        'sync_code' => $internalId,
                        'created_by' => $command->getUserId(),
                        'strain_id' => $inventory->getAttribute('strain_id'),
                    ]
                    + $qaSample->toArray()
                    + ($license->is_test ?
                        [
                            'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                            'synced_at' => Carbon::now(),
                        ] :
                        [
                            'sync_status' => TraceableModel::SYNC_STATUS_PENDING,
                        ]);
            }
            if (true !== $this->model->insert($qaSampleData)) {
                DB::rollBack();
                throw new Exception('Create qa sample failed');
            }

            $collection = $this->getQASamplesHaveCreated($command->getIds());
            if ($collection instanceof Collection !== true) {
                DB::rollBack();
                throw new Exception('Create qa sample failed');
            }

            $this->updateInventories($command->getItems());

            DB::commit();

            return $collection;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @throws Exception
     */
    private function updateInventories(array $items)
    {
        /**
         * @var \App\Cqrs\Values\QASample $item
         */
        foreach ($items as $item) {
            $inventory = $this->inventory->find($inventoryId = $item->getInventoryId());
            if ($inventory instanceof Inventory !== true) {
                throw new Exception(sprintf('The inventory \'%s\' not found', $inventoryId));
            }

            $qtyOnHand = (int)$inventory->getAttribute('qty_on_hand');

            $inventory->setAttribute('qty_on_hand', $qtyOnHand - $item->getWeight());
            $inventory->setAttribute('final_qa_sample_id', $item->getId());
            $inventory->setQAStatusByQASampleCount($this->model->countByInventoryId($inventoryId));

            if (true !== $inventory->save()) {
                throw new Exception(sprintf('Update qty_on_hand of inventory \'%s\' failed', $inventoryId));
            }

            $inventoryHistory = new InventoryHistory([
                'qty_current' => $qtyOnHand,
                'inventory_id' => $inventoryId,
                'qty_variable' => $item->getWeight(),
                'action' => 'qa_sample.create',
                'source_type' => QASample::class,
                'is_decrease' => 1,
            ]);

            if (true !== $inventoryHistory->save()) {
                throw new Exception('Create Inventory History failed');
            }
        }
    }

    /**
     * @param array $ids
     * @return Collection|null
     */
    private function getQASamplesHaveCreated(array $ids): ?Collection
    {
        $collection = new Collection();

        foreach ($ids as $id) {
            $model = $this->model->find($id);
            if ($model instanceof QASample !== true) {
                return null;
            }

            $collection->add($model);
        }

        return $collection;
    }
}
