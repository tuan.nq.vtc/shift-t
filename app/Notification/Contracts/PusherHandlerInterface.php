<?php

namespace App\Notification\Contracts;


interface PusherHandlerInterface
{
    /**
     * @param string $channel
     * @param string $event
     * @param string $message
     */
    public function pushNotification(string $channel, string $event, string $message);
}
