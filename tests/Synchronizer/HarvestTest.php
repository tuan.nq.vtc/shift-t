<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch, Harvest, HarvestGroup, License, Room, SeedToSale, Strain, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;

/**
 * Class HarvestTest
 * @package Tests\Synchronizer
 */
class HarvestTest extends TestBase
{
    /** @test */
    public function it_can_push_create_harvest_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $harvest = factory(Harvest::class)->create(['license_id' => $licenseId]);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Harvest::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $harvest->id,
                'resource_type' => get_classname($harvest),
                'resource_data' => $harvest->attributesToArray(),
                'resource_changes' => $harvest->getChanges(),
            ]
        );

        $batchCode = "WAG010101.BA" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.BA7D9O;
        Http::fake(
            [
                '/plants/harvest_plants' => Http::response(
                    [
                        'external_id' => $harvest->id,
                        'global_id' => $batchCode,
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
        $this->assertEquals($batchCode, $result->resource->batch->sync_code);
    }

    /** @test */
    public function it_can_push_confirm_dry_harvest_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $harvest = factory(Harvest::class)->create(['license_id' => $licenseId]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId]);
        $batch->source()->associate($harvest)->save();
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Harvest::SYNC_ACTION_CONFIRM_DRY,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $harvest->id,
                'resource_type' => get_classname($harvest),
                'resource_data' => $harvest->attributesToArray(),
                'resource_changes' => $harvest->getChanges(),
            ]
        );

        Http::fake(
            [
                '/batches/cure_lot' => Http::response(
                    [
                        'external_id' => $harvest->id,
                        'global_id' => "WAG010101.BA" . $this->faker->regexify('[A-Z0-9]{4}')
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_push_finalize_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $harvest = factory(Harvest::class)->create(['license_id' => $licenseId]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId]);
        $batch->source()->associate($harvest)->save();
        $harvest->inventories()->createMany(
            [
                [
                    'license_id' => $harvest->license_id,
                    'room_id' => $harvest->flower_room_id,
                    'type_id' => $harvest->flower_inventory_type_id,
                    'qty_on_hand' => $harvest->flower_dry_weight,
                    'uom' => SeedToSale::UOM_GRAMS
                ],
                [
                    'license_id' => $harvest->license_id,
                    'room_id' => $harvest->material_room_id,
                    'type_id' => $harvest->material_inventory_type_id,
                    'qty_on_hand' => $harvest->material_dry_weight,
                    'uom' => SeedToSale::UOM_GRAMS
                ],
            ]
        );
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Harvest::SYNC_ACTION_FINALIZE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $harvest->id,
                'resource_type' => get_classname($harvest),
                'resource_data' => $harvest->attributesToArray(),
                'resource_changes' => $harvest->getChanges(),
            ]
        );

        $flowerInventoryCode = "WAG010101.IN" . $this->faker->regexify('[A-Z0-9]{4}');
        $materialInventoryCode = "WAG010101.IN" . $this->faker->regexify('[A-Z0-9]{4}');
        Http::fake(
            [
                '/batches/finish_lot' => Http::response(
                    [
                        [
                            'global_id' => $flowerInventoryCode,
                            'external_id' => $batch->id,
                            'global_inventory_type_id' => $harvest->flowerInventoryType->sync_code,
                        ],
                        [
                            'global_id' => $materialInventoryCode,
                            'external_id' => $batch->id,
                            'global_inventory_type_id' => $harvest->materialInventoryType->sync_code,
                        ]
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
        $inventories = $result->resource->inventories;
        $flowerInventory = $inventories->where('type_id', $harvest->flower_inventory_type_id)->first();
        $this->assertEquals($flowerInventoryCode, $flowerInventory->sync_code);
        $materialInventory = $inventories->where('type_id', $harvest->material_inventory_type_id)->first();
        $this->assertEquals($materialInventoryCode, $materialInventory->sync_code);
    }

    /** @test */
    public function it_can_chain_sync_finalize_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $harvest = factory(Harvest::class)->create(['license_id' => $licenseId]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId]);
        $batch->source()->associate($harvest)->save();
        $harvest->inventories()->createMany(
            [
                [
                    'license_id' => $harvest->license_id,
                    'room_id' => $harvest->flower_room_id,
                    'type_id' => $harvest->flower_inventory_type_id,
                    'qty_on_hand' => $harvest->flower_dry_weight,
                    'uom' => SeedToSale::UOM_GRAMS
                ],
                [
                    'license_id' => $harvest->license_id,
                    'room_id' => $harvest->material_room_id,
                    'type_id' => $harvest->material_inventory_type_id,
                    'qty_on_hand' => $harvest->material_dry_weight,
                    'uom' => SeedToSale::UOM_GRAMS
                ],
            ]
        );

        // Mock API
        $flowerInventoryCode = "WAG010101.IN" . $this->faker->regexify('[A-Z0-9]{4}');
        $materialInventoryCode = "WAG010101.IN" . $this->faker->regexify('[A-Z0-9]{4}');
        Http::fake(
            [
                '/batches/cure_lot' => Http::response(
                    [
                        'external_id' => $harvest->id,
                        'global_id' => "WAG010101.BA" . $this->faker->regexify('[A-Z0-9]{4}'),
                    ],
                    200
                ),
                '/batches/finish_lot' => Http::response(
                    [
                        [
                            'global_id' => $flowerInventoryCode,
                            'external_id' => $batch->id,
                            'global_inventory_type_id' => $harvest->flowerInventoryType->sync_code,
                        ],
                        [
                            'global_id' => $materialInventoryCode,
                            'external_id' => $batch->id,
                            'global_inventory_type_id' => $harvest->materialInventoryType->sync_code,
                        ]
                    ],
                    200
                ),
            ]
        );
        // End mock API

        $confirmDryTrace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Harvest::SYNC_ACTION_CONFIRM_DRY,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $harvest->id,
                'resource_type' => get_classname($harvest),
                'resource_data' => $harvest->attributesToArray(),
                'resource_changes' => $harvest->getChanges(),
            ]
        );
        $confirmDryResult = app(Synchronizer::class)->push($confirmDryTrace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $confirmDryResult->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $confirmDryResult->resource->sync_status);

        $finalizeTrace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Harvest::SYNC_ACTION_FINALIZE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $harvest->id,
                'resource_type' => get_classname($harvest),
                'resource_data' => $harvest->attributesToArray(),
                'resource_changes' => $harvest->getChanges(),
            ]
        );
        $finalizeResult = app(Synchronizer::class)->push($finalizeTrace);

        $inventories = $finalizeResult->resource->inventories;
        $flowerInventory = $inventories->where('type_id', $harvest->flower_inventory_type_id)->first();
        $this->assertEquals($flowerInventoryCode, $flowerInventory->sync_code);
        $materialInventory = $inventories->where('type_id', $harvest->material_inventory_type_id)->first();
        $this->assertEquals($materialInventoryCode, $materialInventory->sync_code);

        $this->assertEquals(Trace::STATUS_COMPLETED, $finalizeTrace->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $finalizeTrace->resource->sync_status);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );
        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Harvest::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ],
            ]
        );
        $strain = factory(Strain::class)->create(['license_id' => $license->id,]);
        $room = factory(Room::class)->create(['license_id' => $license->id,]);
        $data = [];
        $totalRecord = rand(5, 10);
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = $this->generateGlobalId("WAG010101.BA" );
            $data[] = [
                "type" => "harvest",
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
                "global_strain_id" => $strain->sync_code,
                "global_area_id" => $room->sync_code,
                "origin" => $this->faker->randomElement(SeedToSale::SOURCE_TYPES),
                "status" => rand(0, 1) ? "open" : "closed",
                "quantity" => rand(5, 20),
                "batch_created_at" => $this->faker->dateTimeThisYear()->format('Y-m-d H:i:s'),
                "harvested_at" => $this->faker->dateTimeThisYear()->format('Y-m-d H:i:s'),
                "harvested_end_at" => $this->faker->dateTimeThisYear()->format('Y-m-d H:i:s'),
                "updated_at" => $this->faker->dateTimeBetween($fromDate, $toDate)->format('Y-m-d H:i:s'),
                "global_mother_plant_id" => null
            ];
        }
        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
            'f_type' => 'harvest',
        ];
        Http::fake(
            [sprintf("/batches?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response($responseData, 200),]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Batch::count());
        $this->assertEquals($totalRecord, HarvestGroup::count());
        $this->assertEquals($totalRecord, Harvest::count());
    }
}
