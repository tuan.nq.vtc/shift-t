<?php

namespace App\Services;

use App\Models\InventoryType;
use Illuminate\{Database\Eloquent\Builder, Database\Eloquent\Collection};
use Illuminate\Support\Arr;

/**
 * Class InventoryTypeService
 * @package App\Services
 *
 * @method InventoryType|Collection get($id)
 * @method InventoryType|Builder getModel()
 */
class InventoryTypeService extends BaseService
{
    protected array $relations = ['inventoryCategory', 'inventoryCategory.parent'];

    public function __construct(InventoryType $inventoryType)
    {
        parent::__construct($inventoryType);
    }

    public function index()
    {
        return $this->model->get(['id', 'name']);
    }

    /**
     * @param array $data
     *
     * @return InventoryType
     *
     * @throws GeneralException
     */
    public function create(array $data): InventoryType
    {
        $data = $this->transformData($data);

        return parent::create($data);
    }

    /**
     * @param $id
     * @param array $data
     *
     * @return InventoryType
     *
     * @throws ModelNotFoundException|GeneralException
     */
    public function update($id, array $data): InventoryType
    {
        $data = $this->transformData($data);

        return parent::update($id, $data);
    }

    private function transformData(array $data): array
    {
        $syncData = Arr::only($data, ['serving_num', 'serving_size', 'weight', 'volume', 'uom', 'unit_uom']);
        $data = Arr::only($data, ['name', 'category_id', 'strain_id', 'license_id']);
        if (count($syncData) > 0)
            $data['options'] = $syncData;

        return $data;
    }
}
