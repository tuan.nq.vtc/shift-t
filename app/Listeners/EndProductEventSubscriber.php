<?php

namespace App\Listeners;

use App\Events\EndProduct\EndProductSaved;
use App\Models\EndProduct;
use App\Services\EndProductService;
use Illuminate\Events\Dispatcher;

class EndProductEventSubscriber
{
    /**
     * @param EndProductSaved $event
     */
    public function handler($event)
    {
        $this->updateInventoryProductName($event->endProduct);
    }

    /**
     * @param EndProduct $endProduct
     */
    private function updateInventoryProductName($endProduct)
    {
        if ($endProduct->isDirty(['name', 'state_code'])) {
            app(EndProductService::class)->updateInventoryProductName($endProduct->product_id);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [EndProductSaved::class],
            'App\Listeners\EndProductEventSubscriber@handler'
        );
    }
}
