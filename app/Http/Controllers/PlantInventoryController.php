<?php

namespace App\Http\Controllers;

use App\Rules\ModelsExist;
use App\Rules\ModelsHaveTheSameProp;
use App\Rules\SubroomBelongToLicense;
use App\Services\{InventoryTypeService, InventoryService, PlantService, SubroomService};
use Illuminate\Http\{JsonResponse, Request};
use Laravel\Lumen\Routing\Controller;
use Throwable;

class PlantInventoryController extends Controller
{
    protected InventoryTypeService    $inventoryTypeService;
    protected InventoryService $inventoryService;
    protected PlantService            $plantService;
    protected SubroomService          $subroomService;
    protected Request                 $request;

    public function __construct(
        InventoryTypeService $inventoryTypeService,
        InventoryService $inventoryService,
        PlantService $plantService,
        SubroomService $subroomService,
        Request $request
    ) {
        $this->inventoryTypeService = $inventoryTypeService;
        $this->inventoryService = $inventoryService;
        $this->plantService = $plantService;
        $this->subroomService = $subroomService;
        $this->request = $request;
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function movePlantsToInventory(): JsonResponse
    {
        # TODO: Need confirmation about strain, inventory item (classification)
        # Leaf supports moving plants (immature/mature) with same strain to one storage location (room)
        # Need to mark plants that are moved to inventory and they can not be shown or maybe be soft deleted
        # Plants belong to inventory item (aka Product on frontend UI) and this inventory item has to be created and synced
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');
        $plantConditions = ['license_id' => $licenseId, 'destroyed' => '0'];
        $data = $this->validate(
            $this->request,
            [
                'license_id' => [
                    'required',
                    'uuid',
                ],
                'plant_ids' => [
                    'required',
                    new ModelsExist('App\Models\Plant', 'id', $plantConditions),
                    new ModelsHaveTheSameProp('App\Models\Plant', 'strain_id', 'id', $plantConditions),
                ],
                'inventory_type_id' => [
                    'required',
                    'uuid',
                    'exists:App\Models\InventoryType,id,license_id,'.$licenseId,
                ],
                'room_id' => [
                    'required',
                    'uuid',
                    'exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',
                ],
                'subroom_id' => [
                    'nullable',
                    new SubroomBelongToLicense($roomId, $licenseId),
                ],
            ]
        );
        $inventoryType = $this->inventoryTypeService->get($data['inventory_type_id']);
        $plants = $this->plantService->get($data['plant_ids']);
        $subroom = $this->subroomService->get($data['subroom_id']);

        return response()->json(
            $this->inventoryService->createFromPlants($inventoryType, $plants, $subroom)
        );
    }

    public function moveInventoryToPlants(): JsonResponse
    {
        return response()->json(
            []
        );
    }
}
