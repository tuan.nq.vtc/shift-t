<?php

namespace Bamboo\StreamProcessing\Handlers\Kafka;

use Bamboo\StreamProcessing\Contracts\ConsumerHandlerInterface;
use Bamboo\StreamProcessing\Contracts\StreamEventInterface;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Bamboo\StreamProcessing\Resolvers\ConsumerEventProcessResolver;
use Enqueue\RdKafka\RdKafkaConnectionFactory;
use Exception;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Throwable;

class ConsumerHandler implements ConsumerHandlerInterface
{
    /**
     * @var \Enqueue\RdKafka\RdKafkaContext|\Interop\Queue\Context
     */
    private $context;
    private ConsumerEventProcessResolver $consumerProcessResolver;
    private LoggerInterface $logger;

    /**
     * ConsumerHandler constructor.
     *
     * @param RdKafkaConnectionFactory $connection
     * @param ConsumerEventProcessResolver $consumerProcessResolver
     * @throws Throwable
     */
    public function __construct(RdKafkaConnectionFactory $connection, ConsumerEventProcessResolver $consumerProcessResolver)
    {
        $this->logger = Log::channel('stream');
        try {
            $this->context = $connection->createContext();
        } catch (Throwable $throwable) {
            $this->logger->error('Initialize KAFKA consumer handler failed: ' . $throwable->getMessage());
            throw $throwable;
        }
        $this->consumerProcessResolver = $consumerProcessResolver;
    }

    /**
     * @param string $topicName
     * @return StreamEventInterface
     * @throws Exception
     */
    public function receiveEvent(string $topicName): StreamEventInterface
    {
        $destination = $this->context->createTopic($topicName);
        $consumer = $this->context->createConsumer($destination);

        $message = $consumer->receive();
        $context = [
            'id' => $message->getMessageId(),
            'body' => $message->getBody(),
            'header' => $message->getHeaders(),
            'properties' => $message->getProperties(),
        ];
        try {
            $this->logger->info('Kafka consumer receives message', $context);
            $consumer->acknowledge($message);
            $this->logger->info('Kafka consumer acknowledged message', $context);

            return new StreamEvent(
                $message->getMessageId(),
                unserialize($message->getBody()),
                $message->getProperties(),
                $message->getHeaders(),
            );
        } catch (Exception $e) {
            $this->logger->info('Kafka consumer acknowledged message failed', $context);
            $this->logger->error($e);
            throw $e;
        }
    }
}
