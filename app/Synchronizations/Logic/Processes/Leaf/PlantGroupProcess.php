<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\Batch;
use App\Models\GrowCycle;
use App\Models\Plant;
use App\Models\PlantGroup;
use App\Models\Room;
use App\Models\SeedToSale;
use App\Models\Strain;
use App\Models\Trace;
use App\Models\TraceableModel;
use App\Services\LicenseService;
use App\Synchronizations\{Constants\LeafConstant,
    Contracts\TraceableModelInterface,
    Exceptions\Pull\NoImportableDataException,
    Helpers\LeafHelper
};
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\{Arr, Carbon, Collection, Facades\DB};
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class PlantGroupProcess
 * @package App\Synchronizations\Logic\Resources\Leaf
 */
class PlantGroupProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @return void
     * @throws Throwable
     */
    protected function create(): void
    {
        $payload = $this->payload;
        $plantGroup = $this->traceableModel;
        $globalId = $this->getGlobalId($payload[0] ?? []);
        if (!$globalId) {
            throw new Exception(
                "Not found any record matched with [external_id: {$plantGroup->id}] in LEAF API response"
            );
        }
        try {
            DB::beginTransaction();
            $batch = Batch::create(
                [
                    'name' => "PlantGroup {$plantGroup->id}",
                    'status' => Batch::STATUS_OPEN,
                    'license_id' => $plantGroup->license_id,
                    'sync_code' => $globalId
                ]
            );
            $batch->source()->associate($plantGroup)->save();
            $plantGroup->update(['sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED]);
            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            $this->logger->error($throwable->getMessage(), ['PlantGroup ID' => $plantGroup->id]);
            throw $throwable;
        }
    }

    /**
     * @param array $payload
     * @return PlantGroupProcess
     *
     * @throws Throwable
     */
    protected function moveToVegetation(): self
    {
        $payload = $this->payload;
        $plantGroup = $this->traceableModel;
        try {
            DB::beginTransaction();

            /** @var Collection $plants */
            $plants = $plantGroup->plants;
            // #todo: how to handle if $plants->count() <> count($response)

            $isBatchCreated = false;
            foreach ($payload as $statePlantData) {
                if (empty($statePlantData['global_id']) || empty($statePlantData['global_batch_id'])) {
                    throw new Exception('global id or global batch id not found');
                }
                /** @var Plant $plant */
                if ($plant = $plants->shift()) {
                    $plant->updateWithoutTraceableModelEvents(
                        [
                            'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                            'sync_code' => $statePlantData['global_id'],
                            'synced_at' => Carbon::now(),
                        ]
                    );

                    if (!$isBatchCreated) {
                        $parentBatchId = null;
                        if ($plantGroup->propagations && $plantGroup->propagations->batch) {
                            $parentBatchId = $plantGroup->propagations->batch->id;
                        }
                        $batch = Batch::create(
                            [
                                'name' => $plantGroup->name,
                                'status' => Batch::STATUS_OPEN,
                                'license_id' => $plantGroup->license_id,
                                'sync_code' => $statePlantData['global_batch_id'],
                                'parent_id' => $parentBatchId,
                            ]
                        );
                        $batch->source()->associate($plantGroup)->save();
                        $isBatchCreated = true;
                    }
                }
            }
            DB::commit();

            return $this;
        } catch (Throwable $throwable) {
            DB::rollBack();
            $this->logger->error($throwable->getMessage(), ['Plant Group ID' => $plantGroup->id]);
            throw $throwable;
        }
    }

    /**
     * @return void
     * @throws Throwable
     */

    /**
     * @return void
     * @throws Throwable
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;

        $hasNextImport = true;
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }
        try {
            $reports = $this->importData($payload['data'] ?? []);
        } catch (NoImportableDataException $e) {
            if ($currentPage !== 1) {
                $hasNextImport = false;
            }
            $reports = [];
        } catch (Throwable $e) {
            throw $e;
        }
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));

        if ($lastPage === 1) {
            $hasNextImport = false;
        }
        $nextPage = $currentPage === 1 ? $lastPage : $currentPage - 1;
        if ($hasNextImport && $nextPage <= 1) {
            $hasNextImport = false;
        }
        if ($hasNextImport) {
            $children = $this->trace->children;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }

            // create trace from last page to second page
            $conditions['page'] = $nextPage;
            $nextPageTrace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => get_classname($this->traceableModel),
                    'status' => Trace::STATUS_PENDING,
                    'resource_conditions' => $conditions,
                ]
            );

            // reassign the children of current trace to next page trace
            $nextPageTrace->children()->attach($children);
            dispatch(new SyncData($nextPageTrace));

            $this->trace->children()->detach();
        }

        // dispatch result after imported
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $hasNextImport ? StreamableResourceInterface::ACTION_UPDATE : 'finish',
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     * @throws Exception
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $licenseId = $this->traceableModel->license->id;

        // prepare data
        $syncCodes = $batchSyncCodes = $strainSyncCodes = $roomSyncCodes = [];
        $importableData = [];
        foreach ($data as $item) {
            if (!empty($item['global_id'])) {
                $syncCodes[$item['global_id']] = 1;
            }
            if (!empty($item['global_strain_id'])) {
                $strainSyncCodes[$item['global_strain_id']] = 1;
            }
            if (!empty($item['global_area_id'])) {
                $roomSyncCodes[$item['global_area_id']] = 1;
            }
            $importableData[] = $item;
        }

        /**
         * ['sync_code' => ['id', 'sync_code', 'name']]
         */
        $availableStrainArray = Strain::licenseId($licenseId)->whereIn('sync_code', array_keys($strainSyncCodes))
            ->select('id', 'sync_code', 'name')->get()->keyBy('sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $availableRoomArray = Room::licenseId($licenseId)->whereIn('sync_code', array_keys($roomSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $existingPlantGroupArray = Batch::licenseId($licenseId)->whereIn('sync_code', array_keys($syncCodes))
            ->pluck('id', 'sync_code')->toArray();

        $usedStrainIds = $usedRoomIds = [];
        $newGrowCycleData = $newPlantGroupData = $newBatchData = $newGrowCyclePlantGroupRelationData = [];
        foreach ($importableData as $item) {
            try {
                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingPlantGroupArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }
                if (Arr::get($item, 'status') !== LeafConstant::BATCH_STATUS_OPEN) {
                    throw new Exception('Status is not open');
                }
                if (!($type = Arr::get($item, 'type')) || $type !== LeafConstant::BATCH_TYPE_PLANT) {
                    throw new Exception('Invalid Type');
                }

                $strainSyncCode = Arr::get($item, 'global_strain_id');
                if (!$strainSyncCode || empty($availableStrainArray[$strainSyncCode])) {
                    throw new Exception('Strain not found');
                }
                $strain = $availableStrainArray[$strainSyncCode];

                $roomSyncCode = Arr::get($item, 'global_area_id');
                if (!$roomSyncCode || empty($availableRoomArray[$roomSyncCode])) {
                    throw new Exception('Room not found');
                }
                $roomId = $availableRoomArray[$roomSyncCode];

                $usedStrainIds[$strain['id']] = 1;
                $usedRoomIds[$roomId] = 1;

                $plantGroupId = Uuid::uuid4()->toString();
                $growCycleId = Uuid::uuid4()->toString();
                $name = "Grow cycle {$globalId}";
                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                $newGrowCycleData[] = [
                    'id' => $growCycleId,
                    'name' => $name,
                    'license_id' => $licenseId,
                    'internal_id' => ModelHelper::generateInternalId(
                        GrowCycle::class,
                        $licenseId
                    ),
                    'planted_at' => LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'planted_at')),
                    'grow_status' => SeedToSale::GROW_STATUS_VEGETATIVE,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $plantGroupName = "Group of {$strain['name']} in {$name}";
                $newPlantGroupData[] = [
                    'id' => $plantGroupId,
                    'name' => $plantGroupName,
                    'license_id' => $licenseId,
                    'strain_id' => $strain['id'],
                    'room_id' => $roomId,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => $today,
                ];
                $newBatchData[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'name' => $plantGroupName,
                    'license_id' => $licenseId,
                    'status' => Batch::STATUS_OPEN,
                    'source_type' => PlantGroup::class,
                    'source_id' => $plantGroupId,
                    'sync_code' => $globalId,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $newGrowCyclePlantGroupRelationData[] = [
                    'grow_cycle_id' => $growCycleId,
                    'plant_group_id' => $plantGroupId,
                ];
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        try {
            DB::beginTransaction();
            if (!empty($newGrowCycleData)) {
                GrowCycle::insert($newGrowCycleData);
            }
            if (!empty($newPlantGroupData)) {
                PlantGroup::insert($newPlantGroupData);
            }
            if (!empty($newBatchData)) {
                Batch::insert($newBatchData);
            }
            if (!empty($newGrowCyclePlantGroupRelationData)) {
                DB::table('grow_cycle_plant_group')->insert($newGrowCyclePlantGroupRelationData);
            }

            // enable used strains
            if (!empty($usedStrainIds)) {
                Strain::query()->where('status', Strain::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedStrainIds))
                    ->update(['status' => Strain::STATUS_ENABLED]);
            }

            // enable used rooms
            if (!empty($usedRoomIds)) {
                Room::query()->where('status', Room::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedRoomIds))
                    ->update(['status' => Room::STATUS_ENABLED]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['success' => $success, 'error' => $error];
    }
}
