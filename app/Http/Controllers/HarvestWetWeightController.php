<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Models\HarvestWetWeight;
use App\Services\HarvestService;
use App\Services\HarvestWetWeightService;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

/**
 * Class HarvestWetWeightController
 * @package App\Http\Controllers
 */
class HarvestWetWeightController extends Controller
{
    /**
     * @var HarvestService
     */
    private HarvestService $harvestService;

    /**
     * @var HarvestWetWeightService
     */
    private HarvestWetWeightService $harvestWetWeightService;

    /**
     * @var Request
     */
    private Request $request;

    public function __construct(
        HarvestService $harvestService,
        HarvestWetWeightService $harvestWetWeightService,
        Request $request
    ) {
        $this->harvestService = $harvestService;
        $this->harvestWetWeightService = $harvestWetWeightService;
        $this->request = $request;
    }

    /**
     *
     * @param string $harvestId
     *
     * @return JsonResponse
     * @throws ValidationException|GeneralException
     */
    public function store(string $harvestId): JsonResponse
    {
        $rules = [
            'weight' => ['required', 'numeric', 'between:0,100000000'],
            'type' => ['required', new In(HarvestWetWeight::TYPES)],
            'harvested_at' => ['required', 'date_format:Y-m-d'],
        ];

        return response()->json(
            $this->harvestService->addWetWeight($harvestId, $this->validate($this->request, $rules)),
            Response::HTTP_CREATED
        );
    }

    /**
     *
     * @param string $id
     *
     * @return JsonResponse
     * @throws ValidationException|GeneralException
     */
    public function update(string $id): JsonResponse
    {
        $rules = [
            'weight' => ['sometimes', 'numeric', 'between:0,100000000'],
            'harvested_at' => ['sometimes', 'required', 'date_format:Y-m-d'],
        ];

        $harvestWetWeight = $this->harvestWetWeightService->update($id, $this->validate($this->request, $rules));
        return response()->json(
            $harvestWetWeight->harvest,
            Response::HTTP_ACCEPTED
        );
    }

    /**
     *
     * @param string $id
     * @return JsonResponse
     * @throws GeneralException
     */
    public function delete(string $id): JsonResponse
    {
        $this->harvestWetWeightService->delete($id);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
