<?php

namespace App\Console\Commands\TraceableResource;

use App\Models\{Inventory, LabTest, License, QASample, State, Trace};
use App\Synchronizations\Contracts\Http\{ClientInterface, ResourceInterface};
use App\Synchronizations\Http\Clients\{LeafClient, MetrcClient};
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Str;

class SyncLabTestCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'Sync all lab test to state API on development environment';
    /**
     * @var string
     */
    protected $signature = 'lab-test:sync {--state= : The state resource(s) need to sync}
                {--config= : API config}';

    /**
     * @var State
     */
    protected $state;
    /**
     * @var License
     */
    protected $license;
    /**
     * @var ClientInterface
     */
    protected $client;
    /**
     * @var ResourceInterface
     */
    protected $resource;

    /**
     * @throws Exception
     */
    public function handle()
    {
        $stateCode = $this->getStateCode();

        if (!$stateCode) {
            $this->error("Please specify state");
            exit();
        }


        if (!$stateCode || !($this->state = State::whereCode($stateCode)->first())) {
            $this->error("Invalid state [{$stateCode}]");
            exit();
        }

        if (!isset($this->state->getRegulatorResources()["App\Models\LabTest"])) {
            $this->error("Resource [lab-test] not configured for state [{$stateCode}]");
            exit();
        }

        $this->initLicense()->buildClient()->buildResource();
        $page = 1;

        // TODO: need to test
        while (true) {
            $this->resource->addQuery('page', $page);
            $listResponse = $this->sendRequest();

            if (State::REGULATOR_LEAF == $this->state->regulator) {
                $body = $listResponse->json();
                if (count($body['data']) === 0) break;
                
                foreach ($body['data'] as $item) {
                    $forInventorySyncCode = $item['global_for_inventory_id'];
                    $this->info($forInventorySyncCode);
                    $inventory = Inventory::where('sync_code', $forInventorySyncCode)->first();
                    if ($inventory) {
                        $qaSample = QASample::create([
                            'license_id' => $inventory->license_id,
                            'sample_type' => QASample::LAB_SAMPLE,
                            'quantity' => 0, // TODO: can not get quantity
                            'lab_id' => $item['mme_id'],
                            'inventory_id' => $inventory->id,
                        ]);
                        $labTest = LabTest::create([
                            'license_id' => $inventory->license_id,
                            'tested_at' => !empty($item['tested_at']) ? $item['tested_at'] : null,
                            'qa_sample_id' => $qaSample->id,
                            'sync_code' => $item['global_id'],
                            'sync_status' => Trace::STATUS_CREATED,
                            'synced_at' => Carbon::now(),
                        ]);
                    }
                }

                $page++;
            }

            if (State::REGULATOR_METRC == $this->state->regulator) {
                // Only LEAF
                break;
            }
        }
    }

    /**
     * @return string|null
     */
    protected function getStateCode(): ?string
    {
        if ($state = $this->option('state')) {
            return Str::upper($state);
        }

        return null;
    }

    /**
     * @return self
     */
    protected function initLicense(): self
    {
        $configOptions = null;
        if (empty($this->option('config'))) {
            $this->error('Please input config option');
            exit();
        }
        $config = explode(';', $this->option('config'));

        foreach ($config as $item) {
            $keyValue = explode(':', $item);
            if (count($keyValue) != 2) {
                $this->error('Invalid input config option. Valid format: \'key_1:value_1;key_2:value_2;...\'');
                exit();
            }
            $configOptions[$keyValue[0]] = $keyValue[1];
        }

        if (State::REGULATOR_LEAF == $this->state->regulator) {
            if (!isset($configOptions['code']) || !isset($configOptions['api_key'])) {
                $this->error('LEAF need [code] and [api_key] in config option');
                exit();
            }
            $this->license = new License(
                [
                    'state_code' => $this->state->code,
                    'code' => $configOptions['code'],
                    'api_configuration' => [
                        'api_key' => $configOptions['api_key'],
                    ],
                ]
            );
        }

        if (State::REGULATOR_METRC == $this->state->regulator) {
            if (!isset($configOptions['code']) || !isset($configOptions['vendor_api_key']) || !isset($configOptions['user_api_key'])) {
                $this->error('METRC need [code], [vendor_api_key] and [user_api_key] in config option');
                exit();
            }
            $this->license = new License(
                [
                    'state_code' => $this->state->code,
                    'code' => $configOptions['code'],
                    'api_configuration' => [
                        'vendor_api_key' => $configOptions['vendor_api_key'],
                        'user_api_key' => $configOptions['user_api_key'],
                    ],
                ]
            );
        }

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function buildClient(): self
    {
        if (State::REGULATOR_LEAF == $this->state->regulator) {
            $this->client = new LeafClient($this->license);
        }

        if (State::REGULATOR_METRC == $this->state->regulator) {
            $this->client = new MetrcClient($this->license);
        }

        return $this;
    }

    /**
     * @param array $resourceData
     * @param string $action
     *
     * @return self
     *
     * @throw Exception
     */
    protected function buildResource($resourceData = [], $action = ResourceInterface::ACTION_LIST): self
    {
        $regulatorResourceClass = $this->getRegulatorResourceClass();
        $traceableResourceClass = $this->getTraceableResourceClass();

        $traceableResource = new $traceableResourceClass($resourceData);
        $traceableResource->license = $this->license;

        $this->resource = new $regulatorResourceClass($traceableResource, $action);

        return $this;
    }

    /**
     * @return Response
     *
     * @throws Exception
     */
    private function sendRequest(): Response
    {
        return $this->client->getRequest()->send(
            $this->resource->getMethod(),
            $this->resource->getUrl(),
            [
                'query' => $this->resource->getQuery(),
                'json' => $this->resource->getBody(),
            ]
        );
    }

    private function getTraceableResourceClass(): string
    {
        return "App\Models\LabTest";
    }

    private function getRegulatorResourceClass(): string
    {
        return $this->state->getRegulatorResources()[$this->getTraceableResourceClass()];
    }
}
