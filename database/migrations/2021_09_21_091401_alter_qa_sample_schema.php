<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterQaSampleSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qa_samples', function($table) {
            $table->string('sync_code')->nullable()->index();
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qa_samples', function($table) {
            $table->dropColumn([
                'sync_code',
                'sync_status',
                'synced_at'
            ]);
        });
    }
}
