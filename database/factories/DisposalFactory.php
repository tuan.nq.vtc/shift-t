<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Disposal, License, TraceableModel, User, Propagation, Room};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
|   Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Disposal::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Disposal::class,
                $data['license_id']
            ),
            'uom' => Disposal::UOM_EACH,
            'wastable_id' => function ($plant) {
                return factory(Propagation::class)->create(['license_id' => $plant['license_id']])->id;
            },
            'room_id' => function ($data) {
                return factory(Room::class)->create(['license_id' => $data['license_id']])->id;
            },
            'quantity' => rand(1, 10),
            'scheduled_by_id' => function () {
                return factory(User::class)->create()->id;
            },
            'reason' => $this->faker->randomElement(Disposal::REASONS),
            'quarantine_start' => $faker->dateTimeThisYear(),
            'quarantine_end' => $faker->dateTimeThisYear(),
            'sync_code' => 'WAG010101.IV' . $faker->regexify('[A-Z0-9]{4}'), // WAG010101.PL15AU
            'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
            "synced_at" => $faker->dateTimeThisYear(),
        ];
    }
);
