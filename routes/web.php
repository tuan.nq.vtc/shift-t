<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Laravel\Lumen\Routing\Router $router */

$router->get(
    '/',
    function () use ($router) {
        return $router->app->version();
    }
);

$router->group(['prefix' => 'internal'], function () use ($router) {
    $router->group(
        ['prefix' => 'strains',], function () use ($router) {
        $router->post('/index', 'StrainController@indexByIds');
        $router->get('/get-by-organization/{organizationId}', 'StrainController@getByOrganization');
        $router->get('/get-by-state/{stateCode}', 'StrainController@getByState');
        $router->get('/get-by-account-holder/{accountHolderId}', 'StrainController@getByAccountHolder');
        $router->get('{id}/account-holder/{accountHolderId}', 'StrainController@getDetailByAccountHolder');
        $router->post('import', 'StrainController@import');
    });
    $router->group(
        ['prefix' => 'print-labels',], function () use ($router) {
            $router->get('/all', 'PrintLabelController@getAll');
            $router->get('{id}', 'PrintLabelController@getDetail');
    });
    $router->group(
        ['prefix' => 'file-generators',],
        function () use ($router) {
            $router->addRoute('POST', '/set-files-upload-status/{status:success|error}', 'FileGeneratorController@setFilesUploadStatus');
        }
    );
});

$router->group(
    ['middleware' => 'auth_header'],
    function () use ($router) {
        $uuidParam = '{id:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}}';

        $router->group(
            ['prefix' => 'strains',],
            function () use ($router) {
                // | GET|HEAD  | strains               | App\Http\Controllers\StrainController@index   |
                $router->addRoute(['GET'], '/', 'StrainController@index');
                // | GET|HEAD  | strains/list               | App\Http\Controllers\StrainController@list   |
                $router->addRoute(['GET'], '/list', 'StrainController@list');
                $router->addRoute(['GET'], '/show/{id}', 'StrainController@list');
                // | POST      | strains               | App\Http\Controllers\StrainController@store   |
                $router->addRoute('POST', '/', 'StrainController@store');
                // | GET|HEAD  | strains/{id}          | App\Http\Controllers\StrainController@show    |
                $router->addRoute(['GET'], '/{id}', 'StrainController@show');
                // | POST|PATCH | strains/{id}          | App\Http\Controllers\StrainController@update  |
                $router->addRoute(['POST', 'PATCH'], '/{id}', 'StrainController@update');
                // | DELETE    | strains/{id}          | App\Http\Controllers\StrainController@destroy |
                $router->addRoute('DELETE', '/{id}', 'StrainController@destroy');
                $router->post('change-status/{status:enabled|disabled}', 'StrainController@changeStatus');
            }
        );

        $router->group(
            ['prefix' => 'room-types',],
            function () use ($router) {
                // | GET|HEAD  | room-types               | App\Http\Controllers\RoomTypeController@index   |
                $router->addRoute(['GET'], '/', 'RoomTypeController@index');
                // | GET|HEAD  | room-types/list               | App\Http\Controllers\RoomTypeController@list   |
                $router->addRoute(['GET'], '/list', 'RoomTypeController@list');
                // | POST      | room-types               | App\Http\Controllers\RoomTypeController@store   |
                $router->addRoute('POST', '/', 'RoomTypeController@store');
                // | GET|HEAD  | room-types/{id}          | App\Http\Controllers\RoomTypeController@show    |
                $router->addRoute(['GET'], '/{id}', 'RoomTypeController@show');
                // | PUT|PATCH | room-types/{id}          | App\Http\Controllers\RoomTypeController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'RoomTypeController@update');
                // | DELETE    | room-types/{id}          | App\Http\Controllers\RoomTypeController@destroy |
                $router->addRoute('DELETE', '/{id}', 'RoomTypeController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'flower-types',],
            function () use ($router) {
                // | GET|HEAD  | flower-types               | App\Http\Controllers\FlowerTypeController@index   |
                $router->addRoute(['GET'], '/', 'FlowerTypeController@index');
                // | GET|HEAD  | flower-types/list               | App\Http\Controllers\FlowerTypeController@list   |
                $router->addRoute(['GET'], '/list', 'FlowerTypeController@list');
                // | POST      | flower-types               | App\Http\Controllers\FlowerTypeController@store   |
                $router->addRoute('POST', '/', 'FlowerTypeController@store');
                // | GET|HEAD  | flower-types/{id}          | App\Http\Controllers\FlowerTypeController@show    |
                $router->addRoute(['GET'], '/{id}', 'FlowerTypeController@show');
                // | PUT|PATCH | flower-types/{id}          | App\Http\Controllers\FlowerTypeController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'FlowerTypeController@update');
                // | DELETE    | flower-types/{id}          | App\Http\Controllers\FlowerTypeController@destroy |
                $router->addRoute('DELETE', '/{id}', 'FlowerTypeController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'rooms',],
            function () use ($router) {
                $router->get('index', 'RoomController@index');
                // | GET|HEAD  | rooms/stats                | App\Http\Controllers\RoomController@stats            |
                $router->addRoute(['GET'], '/stats', 'RoomController@getStatistics');
                // | GET|HEAD  | rooms/list                 | App\Http\Controllers\RoomController@list             |
                $router->addRoute(['GET'], '/list', 'RoomController@list');
                // | GET|HEAD  | rooms/category/{category}  | App\Http\Controllers\RoomController@list             |
                $router->addRoute(
                    ['GET'],
                    '/category/{category:propagation|plant|harvest|disposal|inventory}',
                    'RoomController@getByCategory'
                );
                // | POST      | rooms                      | App\Http\Controllers\RoomController@store            |
                $router->addRoute('POST', '/', 'RoomController@store');
                // | GET|HEAD  | rooms/{id}                 | App\Http\Controllers\RoomController@show             |
                $router->get('/{id}', 'RoomController@getDetail');
                // | POST      | rooms/{id}/subrooms        | App\Http\Controllers\RoomSubroomsController@store    |
                $router->addRoute(['POST'], '/{id}/subrooms', 'RoomSubroomsController@store');
                // | PUT|PATCH | rooms/{id}                 | App\Http\Controllers\RoomController@update           |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'RoomController@update');
                // | DELETE    | rooms/{id}                 | App\Http\Controllers\RoomController@destroy          |
                $router->addRoute(['DELETE'], '/{id}', 'RoomController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'subrooms',],
            function () use ($router) {
                // | GET|HEAD  | subrooms/{id}          | App\Http\Controllers\SubroomController@show    |
                $router->addRoute(['GET'], '/{id}', 'SubroomController@show');
                // | PUT|PATCH | subrooms/{id}          | App\Http\Controllers\SubroomController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'SubroomController@update');
                // | DELETE    | subrooms/{id}          | App\Http\Controllers\SubroomController@destroy |
                $router->addRoute('DELETE', '/{id}', 'SubroomController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'propagations',],
            function () use ($router) {
                // | GET|HEAD  | propagations/list                              | App\Http\Controllers\PropagationController@list               |
                $router->addRoute(['GET'], '/list', 'PropagationController@list');
                $router->get('/{id}', 'PropagationController@show');
                // | POST      | propagations                                   | App\Http\Controllers\PropagationController@store              |
                $router->addRoute('POST', '/', 'PropagationController@store');
                // | POST      | propagations/bulk               | App\Http\Controllers\PropagationController@bulkCreate                        |
                $router->addRoute('POST', '/bulk', 'PropagationController@bulkCreate');
                // | POST      | propagations/move-to-vegetation/{?growCycleId}  | App\Http\Controllers\PropagationController@moveToVegetation   |
                $router->addRoute(
                    'POST',
                    '/move-to-vegetation[/{growCycleId}]',
                    'PropagationController@moveToVegetation'
                );
                // | PUT|PATCH | propagations/{id}                              | App\Http\Controllers\PropagationController@update             |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'PropagationController@update');
                // | DELETE    | propagations/{id}                              | App\Http\Controllers\PropagationController@destroy            |
                $router->addRoute('DELETE', '/{id}', 'PropagationController@destroy');
                // | POST     | propagations/destroy                            | App\Http\Controllers\PropagationController@destroy            |
                $router->post('/destroy', 'PropagationController@bulkDestroy');
            }
        );

        $router->group(
            ['prefix' => 'plants',],
            function () use ($router, $uuidParam) {
                // | GET|HEAD  | plants/search                            | App\Http\Controllers\PlantController@search             |
                $router->get('/search', 'PlantController@search');
                // | GET|HEAD  | plants/list                              | App\Http\Controllers\PlantController@list               |
                $router->addRoute(['GET'], '/list', 'PlantController@list');
                // | POST      | plants                                   | App\Http\Controllers\PlantController@store              |
                $router->addRoute('POST', '/', 'PlantController@store');
                // | GET|HEAD  | plants/{id}                              | App\Http\Controllers\PlantController@show
                $router->get('/{id}', 'PlantController@getDetail');
//        $router->addRoute(['GET'], '/' . $uuidParam, 'PlantController@show');
                // | PUT|PATCH | plants/{id}                              | App\Http\Controllers\PlantController@update             |
                $router->addRoute(['PUT', 'PATCH'], '/'.$uuidParam, 'PlantController@update');
                // | DELETE    | plants/{id}                              | App\Http\Controllers\PlantController@destroy            |
                $router->addRoute('DELETE', '/'.$uuidParam, 'PlantController@destroy');
                // | POST      | plants/destroy                           | App\Http\Controllers\PlantController@bulkDestroy        |
                $router->post('destroy', 'PlantController@bulkDestroy');
                // | PUT|PATCH | plants/
                $router->addRoute(['PUT', 'PATCH'], '/', 'PlantController@updatePlant');
                // | POST      | plants/move-to-grow-cycle/{?growCycleId} | App\Http\Controllers\PlantController@moveToGrowCycle    |
                $router->post('/move-to-grow-cycle[/{growCycleId}]', 'PlantController@moveToGrowCycle');
                // | POST      | plants/move-to-room                      | App\Http\Controllers\PlantController@moveToRoom         |
                $router->post('/move-to-room', 'PlantController@moveToRoom');
                // | POST      | plants/make-mothers                      | App\Http\Controllers\PlantController@makeMothers        |
                $router->addRoute(['PUT', 'PATCH'], '/make-mothers', 'PlantController@makeMothers');
                // | PUT|PATCH | plants/change-grow-status                | App\Http\Controllers\PlantController@changeGrowStatus   |
                $router->addRoute(['PUT', 'PATCH'], '/change-grow-status', 'PlantController@changeGrowStatus');
                // | PUT|PATCH | plants/edit-strain                       | App\Http\Controllers\PlantController@editStrain         |
                $router->addRoute(['PUT', 'PATCH'], '/edit-strain', 'PlantController@editStrain');

                $router->get('/list-for-harvest/{target:room|grow-cycle}', 'PlantController@listForHarvest');
            }
        );

        $router->group(
            ['prefix' => 'grow-cycles',],
            function () use ($router) {
                $router->get('/', 'GrowCycleController@index');
                // | GET|HEAD  | grow-cycles                       | App\Http\Controllers\GrowCycleController@list                 |
                $router->addRoute(['GET', 'HEAD'], '/list', 'GrowCycleController@list');
                // | POST      | grow-cycles                | App\Http\Controllers\GrowCycleController@create                |
                $router->addRoute('POST', '/', 'GrowCycleController@create');
                // | GET|HEAD  | grow-cycles/{id}              | App\Http\Controllers\GrowCycleController@show                  |
                $router->addRoute(['GET', 'HEAD'], '/{id}', 'GrowCycleController@show');
                // | GET|HEAD  | grow-cycles/{id}/plants              | App\Http\Controllers\GrowCyclePlantsController@index                  |
                $router->addRoute(['GET', 'HEAD'], '/{id}/plants', 'GrowCyclePlantsController@index');
                // | PUT|PATCH | grow-cycles/{id}              | App\Http\Controllers\GrowCycleController@update                |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'GrowCycleController@update');

                $router->post('/change-grow-status', 'GrowCycleController@changeGrowStatus');
                // | DELETE    | grow-cycles/{id}              | App\Http\Controllers\GrowCycleController@destroy               |
                $router->addRoute('DELETE', '/{id}', 'GrowCycleController@destroy');

                 $router->post('/move-to-room', 'GrowCycleController@moveToRoom');
            }
        );

        $router->group(
            ['prefix' => 'print-labels',],
            function () use ($router) {
                $router->get('/', 'PrintLabelController@index');
                // | GET|HEAD  | print-labels                         | App\Http\Controllers\PrintLabelController@index   |
                $router->addRoute(['GET'], '/', 'PrintLabelController@index');
                // | GET|HEAD  | print-labels/list                    | App\Http\Controllers\PrintLabelController@list   |
                $router->addRoute(['GET'], '/list', 'PrintLabelController@list');
                // | POST      | print-labels                         | App\Http\Controllers\PrintLabelController@store   |
                $router->addRoute('POST', '/', 'PrintLabelController@store');
                // | GET|HEAD  | print-labels/{id}          | App\Http\Controllers\PrintLabelController@show    |
                $router->addRoute(['GET'], '/{id}', 'PrintLabelController@show');
                // | PUT|PATCH | print-labels/{id}          | App\Http\Controllers\PrintLabelController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'PrintLabelController@update');
                // | DELETE    | print-labels/{id}          | App\Http\Controllers\PrintLabelController@destroy |
                $router->addRoute('DELETE', '/{id}', 'PrintLabelController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'print-label-templates',],
            function () use ($router) {
                $router->get('/', 'PrintLabelTemplateController@index');
                $router->get('/list', 'PrintLabelTemplateController@list');
                $router->addRoute('POST', '/', 'PrintLabelTemplateController@store');
                $router->addRoute(['GET'], '/{id}', 'PrintLabelTemplateController@show');
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'PrintLabelTemplateController@update');
                $router->addRoute('DELETE', '/{id}', 'PrintLabelTemplateController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'pesticides'],
            function () use ($router) {
                $router->get('/', 'PesticideController@index');
                $router->get('/list', 'PesticideController@list');
                $router->get('/{id}', 'PesticideController@show');
                $router->get('/{additiveId}/inventories/', 'AdditiveInventoriesController@index');
                $router->get('/{additiveId}/inventories/list', 'AdditiveInventoriesController@list');
                $router->post('/{additiveId}/inventories', 'AdditiveInventoriesController@store');
                $router->put('/{id}', 'PesticideController@update');
                $router->post('/', 'PesticideController@bulkCreate');
                $router->post('/apply/{target}', 'PesticideController@apply');
            }
        );

        $router->group(
            ['prefix' => 'nutrients'],
            function () use ($router) {
                $router->get('/', 'NutrientController@index');
                $router->get('/list', 'NutrientController@list');
                $router->get('/{id}', 'NutrientController@show');
                $router->get('/{additiveId}/inventories/', 'AdditiveInventoriesController@index');
                $router->get('/{additiveId}/inventories/list', 'AdditiveInventoriesController@list');
                $router->post('/{additiveId}/inventories', 'AdditiveInventoriesController@store');
                $router->put('/{id}', 'NutrientController@update');
                $router->post('/', 'NutrientController@bulkCreate');
                $router->post('/apply/{target}', 'NutrientController@apply');
            }
        );

        $router->group(
            ['prefix' => 'suppliers'],
            function () use ($router) {
                $router->get('/', 'SupplierController@index');
                $router->get('/list', 'SupplierController@list');
                $router->get('/{id}', 'SupplierController@show');
                $router->get('/{id}/statistic', 'SupplierController@getStatistic');
                $router->post('/', 'SupplierController@store');
                $router->put('/{id}', 'SupplierController@update');
            }
        );

        $router->group(
            ['prefix' => 'harvests'],
            function () use ($router) {
                $router->get('/', 'HarvestGroupController@index');
                $router->get('/list', 'HarvestGroupController@list');
                $router->get('/{id}', 'HarvestGroupController@show');
                $router->put('/{id}', 'HarvestGroupController@update');
                $router->post('/add-{source:room|grow-cycle|plants}', 'HarvestGroupController@addPlants');
                $router->post('/{id}/add-{source:room|grow-cycle|plants}', 'HarvestGroupController@addPlants');
                $router->post('/{id}/remove-plants', 'HarvestGroupController@removePlants');
                $router->get('/{id}/plants', 'HarvestGroupPlantsController@list');
                $router->post('/add-wet-weights/{harvestId}', 'HarvestWetWeightController@store');
                $router->post('/update-dry-weights/{harvestId}', 'HarvestGroupController@updateDryWeights');
                $router->post('/{id}/confirm-{phase:wet|dry}', 'HarvestGroupController@confirm');
                $router->post('/{id}/finalize', 'HarvestGroupController@finalize');
            }
        );

        $router->group(
            ['prefix' => 'harvest-wet-weights',],
            function () use ($router) {
                $router->put('/{id}', 'HarvestWetWeightController@update');
                $router->delete('/{id}', 'HarvestWetWeightController@delete');
            }
        );

        $router->group(
            ['prefix' => 'disposals',],
            function () use ($router) {
                $router->get('/list/{status:scheduled|processing|destroyed}', 'DisposalController@listByStatus');
                $router->post('/destroy', 'DisposalController@destroy');
                $router->post('/unschedule', 'DisposalController@unschedule');
            }
        );

        $router->group(
            ['prefix' => 'waste-bags',],
            function () use ($router) {
                $router->post('create', 'WasteBagController@bulkCreate');
            }
        );

        $router->group(
            ['prefix' => 'statistic',],
            function () use ($router) {
                $router->get('/harvests', 'StatisticController@harvests');
                $router->get('/plants-by-grow-statuses', 'StatisticController@plantsByGrowStatuses');
                $router->addRoute(['GET', 'HEAD'], '/plants-by-strains', 'StatisticController@plantsByStrains');
                $router->addRoute(
                    ['GET', 'HEAD'],
                    '/harvested-flower-weight',
                    'StatisticController@harvestedFlowerWeights'
                );
                $router->addRoute(
                    ['GET', 'HEAD'],
                    '/harvested-other-weight',
                    'StatisticController@harvestedOtherWeights'
                );
            }
        );

        $router->group(
            ['prefix' => 'inventory-types',],
            function () use ($router) {
                // | GET|HEAD  | inventory/items                | App\Http\Controllers\InventoryTypeController@index    |
                $router->addRoute(['GET'], '/', 'InventoryTypeController@index');
                // | GET|HEAD  | inventory/items/list           | App\Http\Controllers\InventoryTypeController@list     |
                $router->addRoute(['GET'], '/list', 'InventoryTypeController@list');
                // | POST      | inventory/items                | App\Http\Controllers\InventoryTypeController@store    |
                $router->addRoute('POST', '/', 'InventoryTypeController@store');
                // | GET|HEAD  | inventory/items/{id}           | App\Http\Controllers\InventoryTypeController@show     |
                $router->addRoute(['GET'], '/{id}', 'InventoryTypeController@show');
                // | PUT|PATCH | inventory/items/{id}           | App\Http\Controllers\InventoryTypeController@update   |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'InventoryTypeController@update');
            }
        );

        $router->addRoute(['POST'], '/move-plants-to-inventory', 'PlantInventoryController@movePlantsToInventory');
        $router->addRoute(['POST'], '/move-inventory-to-plants', 'PlantInventoryController@moveInventoryToPlant');

        $router->group(
            ['prefix' => 'inventories',],
            function () use ($router) {
                $router->get('list/unlotted', 'InventoryController@listUnlotted');
                $router->get('list-{type:source|end}', 'InventoryController@listLotted');
                $router->post('{id}/create-lots', 'InventoryController@createLots');
                $router->post('{id}/create-sublots', 'InventoryController@createSublots');
                $router->post('{id}/hold', 'InventoryController@hold');
                $router->post('map-to-product', 'InventoryController@mapToProduct');
                $router->post('{id}/adjust', 'InventoryController@adjust');
                $router->post('/move-to-room', 'InventoryController@moveToRoom');
                $router->get('{id}/conversions', 'InventoryController@getConversions');
                // | GET|HEAD  | inventory/{id}           | App\Http\Controllers\InventoryController@show          |
                $router->addRoute(['GET'], '/{id}', 'InventoryController@getDetail');
                $router->addRoute(['PUT', 'PATCH'], '/make-{sellStatus:available|unavailable}', 'InventoryController@changeSellStatus');
                // | PUT|PATCH | inventory/{id}           | App\Http\Controllers\InventoryController@update        |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'InventoryController@update');
                // | DELETE    | inventory/{id}           | App\Http\Controllers\InventoryController@destroy       |
                $router->addRoute('DELETE', '/{id}', 'InventoryController@destroy');
                $router->post('destroy', 'InventoryController@bulkDestroy');
            }
        );

        $router->group(
            ['prefix' => 'qa-samples',],
            function () use ($router) {
                // | GET|HEAD  | qa-samples/list           | App\Http\Controllers\QASampleController@list      |
                $router->addRoute(['GET'], '/list', 'QASampleController@list');
                $router->addRoute(['GET'], '/{id}', 'QASampleController@show');
                // | POST      | qa-samples                | App\Http\Controllers\QASampleController@create    |
                $router->addRoute('POST', '/create', 'QASampleController@store');

                $router->addRoute(['POST'], '{id}/upload-coa', 'QASampleController@uploadCOA');

                $router->addRoute(['DELETE'], 'remove-coa/{coaId}', 'QASampleController@removeCOA');

                $router->addRoute(['PUT'], '{id}/results', 'QASampleController@updateResults');
            }
        );

        $router->group(
            ['prefix' => 'manifests',],
            function () use ($router) {
                $router->get('/list/{method:inbound|outbound}', 'ManifestController@listByMethod');
                $router->get('/{method:inbound|outbound}', 'ManifestController@getByMethod');
                $router->get('/{id}', 'ManifestController@show');
                $router->post('/', 'ManifestController@save');
                $router->post('/{id}/add-items', 'ManifestController@addItems');
                $router->put('/{id}', 'ManifestController@save');
                $router->post('/{id}/publish', 'ManifestController@publish');
                $router->post('/publish', 'ManifestController@bulkPublish');
                $router->post('/{id}/mark-in-transit', 'ManifestController@markInTransit');
                $router->post('/cancel', 'ManifestController@bulkCancel');
                $router->post('/void', 'ManifestController@bulkVoid');
                $router->post('/receive', 'ManifestController@bulkReceive');
                $router->post('/reject', 'ManifestController@bulkReject');
                $router->delete('/delete', 'ManifestController@bulkDelete');
            }
        );

        $router->group(
            ['prefix' => 'lab-tests',],
            function () use ($router) {
                // | POST      | qa-samples            | App\Http\Controllers\LabTestController@create    |
                $router->addRoute('POST', '/create', 'LabTestController@store');
                // | POST  | qa-samples/{id}           | App\Http\Controllers\LabTestController@reset     |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'LabTestController@update');
            }
        );

        $router->group(
            ['prefix' => 'drivers',],
            function () use ($router) {
                // | GET|HEAD  | flower-types               | App\Http\Controllers\DriverController@index   |
                $router->addRoute(['GET'], '/', 'DriverController@index');
                // | GET|HEAD  | flower-types/list          | App\Http\Controllers\DriverController@list    |
                $router->addRoute(['GET'], '/list', 'DriverController@list');
                // | POST      | flower-types               | App\Http\Controllers\DriverController@store   |
                $router->addRoute('POST', '/', 'DriverController@store');
                // | GET|HEAD  | flower-types/{id}          | App\Http\Controllers\DriverController@show    |
                $router->addRoute(['GET'], '/{id}', 'DriverController@show');
                // | PUT|PATCH | flower-types/{id}          | App\Http\Controllers\DriverController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'DriverController@update');
                // | DELETE    | flower-types/{id}          | App\Http\Controllers\DriverController@destroy |
                $router->addRoute('DELETE', '/{id}', 'DriverController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'vehicles',],
            function () use ($router) {
                // | GET|HEAD  | flower-types               | App\Http\Controllers\VehicleController@index   |
                $router->addRoute(['GET'], '/', 'VehicleController@index');
                // | GET|HEAD  | flower-types/list          | App\Http\Controllers\VehicleController@list    |
                $router->addRoute(['GET'], '/list', 'VehicleController@list');
                // | POST      | flower-types               | App\Http\Controllers\VehicleController@store   |
                $router->addRoute('POST', '/', 'VehicleController@store');
                // | GET|HEAD  | flower-types/{id}          | App\Http\Controllers\VehicleController@show    |
                $router->addRoute(['GET'], '/{id}', 'VehicleController@show');
                // | PUT|PATCH | flower-types/{id}          | App\Http\Controllers\VehicleController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'VehicleController@update');
                // | DELETE    | flower-types/{id}          | App\Http\Controllers\VehicleController@destroy |
                $router->addRoute('DELETE', '/{id}', 'VehicleController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'clients',],
            function () use ($router) {
                // | GET|HEAD  | flower-types               | App\Http\Controllers\ClientController@index   |
                $router->addRoute(['GET'], '/', 'ClientController@index');
                // | GET|HEAD  | flower-types/list               | App\Http\Controllers\ClientController@list   |
                $router->addRoute(['GET'], '/list', 'ClientController@list');
                // | POST      | flower-types               | App\Http\Controllers\ClientController@store   |
                $router->addRoute('POST', '/', 'ClientController@store');
                // | GET|HEAD  | flower-types/{id}          | App\Http\Controllers\ClientController@show    |
                $router->addRoute(['GET'], '/{id}', 'ClientController@show');
                // | PUT|PATCH | flower-types/{id}          | App\Http\Controllers\ClientController@update  |
                $router->addRoute(['PUT', 'PATCH'], '/{id}', 'ClientController@update');
                // | DELETE    | flower-types/{id}          | App\Http\Controllers\ClientController@destroy |
                $router->addRoute('DELETE', '/{id}', 'ClientController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'source-products',],
            function () use ($router) {
                $router->get('/search', 'SourceProductController@search');
                $router->get('/', 'SourceProductController@index');
                $router->get('/list', 'SourceProductController@list');
                $router->post('/', 'SourceProductController@store');
                $router->get('/{id}', 'SourceProductController@show');
                $router->put('/{id}', 'SourceProductController@update');
                $router->delete('/{id}', 'SourceProductController@destroy');
            }
        );

        $router->group(
            ['prefix' => 'end-products',],
            function () use ($router) {
                $router->get('/list', 'EndProductController@list');

            }
        );

        $router->group(
            ['prefix' => 'source-categories',],
            function () use ($router) {
                $router->get('/search', 'SourceCategoryController@search');
                $router->get('/', 'SourceCategoryController@index');
                $router->get('/list', 'SourceCategoryController@list');
                $router->post('/', 'SourceCategoryController@store');
                $router->get('/{id}', 'SourceCategoryController@show');
                $router->put('/{id}', 'SourceCategoryController@update');
                $router->post('/delete', 'SourceCategoryController@deleteAndReassign');
            }
        );

        $router->get('/state-categories/{type:unlotted|source|end}-type', 'StateCategoryController@getByType');
        $router->get('/taxonomy/{type:source|end}-type', 'TaxonomyController@getByType');
        $router->get('/taxonomy/state-category/get-by-{type:source|end}-category/{id}', 'TaxonomyController@getByCategory');
        $router->post('/taxonomy/assign-{type:source|end}-categories', 'TaxonomyController@assignByType');

        $router->group(
            ['prefix' => 'resource-verify',],
            function () use ($router) {
                $router->addRoute(['GET'], '/metrc', 'Verification\MetrcVerifyController');
            }
        );

        $router->group(
            ['prefix' => 'notifications',],
            function () use ($router) {
                $router->get('/', 'UserNotificationController@getUserNotifications');
                $router->put('/read', 'UserNotificationController@markAsRead');
            }
        );

        $router->group(
            ['prefix' => 'additive-logs',],
            function () use ($router) {
                $router->get('/{target:plant|propagation}/{targetId}', 'AdditiveLogController@listBaseOnTarget');
            }
        );

        $router->group(
            ['prefix' => 'licensees',],
            function () use ($router) {
                $router->get('/{type:lab|transporter}', 'LicenseeController@indexByType');
                $router->get('/list', 'LicenseeController@list');
                $router->get('/{type:lab|transporter}/list', 'LicenseeController@list');
            }
        );

        $router->group(
            ['prefix' => 'conversions',],
            function () use ($router) {
                $router->get('/', 'ConversionController@index');
                $router->get('/list', 'ConversionController@list');
                $router->get('/search', 'ConversionController@search');
                $router->post('/', 'ConversionController@store');
                $router->post('/finalize', 'ConversionController@createAndFinalize');
                $router->get('/{id}/input-lots', 'ConversionController@listInputLots');
                $router->get('/{id}', 'ConversionController@show');
                $router->put('/{id}', 'ConversionController@update');
                $router->put('/{id}/finalize', 'ConversionController@updateAndFinalize');
                $router->post('/{id}/add-lots', 'ConversionController@addLots');
            }
        );
    }
);
