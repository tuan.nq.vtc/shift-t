<?php

namespace App\Synchronizations\Contracts\Http;

use App\Models\License;
use Exception;
use Illuminate\Http\Client\RequestException;

interface ClientInterface
{
    /**
     * @param License $license
     * @return ClientInterface
     *
     * @throws Exception
     */
    public function __construct(License $license);

    /**
     * @param ResourceInterface $httpResource
     * @return array
     * @throws RequestException
     */
    public function doRequest(ResourceInterface $httpResource): ?array;
}
