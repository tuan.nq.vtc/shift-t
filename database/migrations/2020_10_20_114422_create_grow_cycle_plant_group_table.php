<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrowCyclePlantGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'grow_cycle_plant_group',
            function (Blueprint $table) {
                $table->uuid('grow_cycle_id');
                $table->uuid('plant_group_id');

                $table->foreign('grow_cycle_id')->references('id')->on('grow_cycles')->cascadeOnDelete();
                $table->foreign('plant_group_id')->references('id')->on('plant_groups')->cascadeOnDelete();

                $table->primary(['grow_cycle_id', 'plant_group_id'], 'grow_cycle_plant_group_primary');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grow_cycle_plant_group');
    }
}
