# Bamboo Trace
- Using Lumen 7.0, PHP >= 7.4

## Setup
We use <a href="https://k3d.io/" target="_blank">K3D</a> to develop locally in Kubernetes. It's lightweight and provides the most similar environment to production.
- First, run `docker login reg.webprovise.io` and log in to the WebProvise registry using your email and password.
- Next, use repo [bamboo-k3d](https://gitlab.webprovise.com/bamboo/bamboo-k3d) in your devhost and run `bash run.sh` in project root to install your private dev cluster with all dependencies.

## DevSpace
We use DevSpace to provide Kubernetes dev hot reloading with two-way sync and simple deployment management.
- To get started the following options are available, but feel free to inspect `./run.sh` to so you can understand what the commands are doing:
  1. `bash run.sh --prep` to cleanly install Composer packages
  2. `bash run.sh --dev` for devspace dev (two-way sync hot reloading)
  3. `bash run.sh --deploy` for devspace deploy (no sync)
  4. `bash run.sh --reset` to uninstall local Kubernetes deployment and reset devspace
  5. `bash run.sh --seed` to populate database with sample data (usually for new projects)
- Environment variables are git tracked in `./api-trace/files/dev.env` and will automatically copy with dynamic values to `./.env` when using `bash run.sh --dev` or `bash run.sh --deploy` in project root
- You can enter pods for debugging easily by using command `devspace enter` and selecting the pod you wish to enter
- You can access MySQL directly from your IDE using `mysql -h wp-*.vcoder.host -u bamboo -pbamboo`

## Document
- Generate API doc (/public/docs)
  
  `php artisan scribe:generate`

## Kafka (For deprecated Docker Compose only)
- Copy the configurations below from .env.example to .env:
   ```
    STREAM_PLATFORM=kafka
    KAFKA_GROUP_ID=trace
    KAFKA_HOST=kafka.kafka.svc.cluster.local
    KAFKA_PORT=9092
    KAFKA_SYNC_TOPIC=trace
    KAFKA_SYNC_USERNAME=
    KAFKA_SYNC_PASSWORD=
   ```
- Run the command and make sure it is always running to push streams to Kafka:
  ```
    php artisan queue:work --queue=stream_queue
  ```
- Run the command and make sure it is always running to receive streams from Kafka:
  ```
    php artisan stream:consume
  ```
