<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch,
    GrowCycle,
    Harvest,
    HarvestPlant,
    Plant,
    PlantGroup,
    Room,
    SeedToSale,
    Strain,
    Trace,
    TraceableModel};
use App\Services\LicenseService;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Exceptions\Pull\NoImportableDataException;
use App\Synchronizations\Helpers\LeafHelper;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class PlantProcess
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 */
class PlantProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'm/d/Y G:ia';
    private const DATE_FORMAT = 'm/d/Y';

    /**
     * @return void
     * @throws Throwable
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;

        $hasNextImport = true;
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }
        try {
            $reports = $this->importData($payload['data'] ?? []);
        } catch (NoImportableDataException $e) {
            if ($currentPage !== 1) {
                $hasNextImport = false;
            }
            $reports = [];
        } catch (Throwable $e) {
            throw $e;
        }
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));

        if ($lastPage === 1) {
            $hasNextImport = false;
        }
        $nextPage = $currentPage + 1;
        if ($hasNextImport && $nextPage > $lastPage) {
            $hasNextImport = false;
        }
        if ($hasNextImport) {
            $children = $this->trace->children;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }

            // create trace from second page to last page
            $conditions['page'] = $nextPage;
            $nextPageTrace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => get_classname($this->traceableModel),
                    'status' => Trace::STATUS_PENDING,
                    'resource_conditions' => $conditions,
                ]
            );

            // reassign the children of current trace to next page trace
            $nextPageTrace->children()->attach($children);
            dispatch(new SyncData($nextPageTrace));

            $this->trace->children()->detach();
        }

        // dispatch result after imported
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $hasNextImport ? StreamableResourceInterface::ACTION_UPDATE : 'finish',
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     * @throws Exception
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $licenseId = $this->traceableModel->license->id;
        $conditions = $this->trace->resource_conditions ?? [];
        $fromDate = null;
        $toDate = null;
        if (!empty($conditions['from_date'])) {
            $fromDate = Carbon::createFromDate($conditions['from_date']);
        }
        if (!empty($conditions['to_date'])) {
            $toDate = Carbon::createFromDate($conditions['to_date']);
        }
        // prepare data
        $syncCodes = $batchSyncCodes = $strainSyncCodes = $roomSyncCodes = [];
        $importableData = [];
        foreach ($data as $item) {
            try {
                $updatedAt = LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'updated_at'));
                if (!$updatedAt || ($fromDate && $fromDate->greaterThanOrEqualTo($updatedAt))
                    || ($toDate && $toDate->lessThanOrEqualTo($updatedAt))) {
                    continue;
                }
            } catch (Exception $e) {
                $this->logger->info($item);
                continue;
            }
            if (!empty($item['global_batch_id'])) {
                $batchSyncCodes[$item['global_batch_id']] = 1;
            } else {
                continue;
            }
            if (!empty($item['global_id'])) {
                $syncCodes[$item['global_id']] = 1;
            }
            if (!empty($item['global_strain_id'])) {
                $strainSyncCodes[$item['global_strain_id']] = 1;
            }
            if (!empty($item['global_area_id'])) {
                $roomSyncCodes[$item['global_area_id']] = 1;
            }
            $importableData[] = $item;
        }
        if (empty($importableData)) {
            throw new NoImportableDataException();
        }
        /**
         * ['sync_code' => 'id']
         */
        $availableStrainArray = Strain::licenseId($licenseId)->whereIn('sync_code', array_keys($strainSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $availableRoomArray = Room::licenseId($licenseId)->whereIn('sync_code', array_keys($roomSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $existingPlantArray = Plant::licenseId($licenseId)->whereIn('sync_code', array_keys($syncCodes))
            ->pluck('id', 'sync_code')->toArray();

        $availableBatchCollection = Batch::licenseId($licenseId)->whereIn('sync_code', array_keys($batchSyncCodes))
            ->select('id', 'sync_code', 'source_id', 'source_type')->get();
        $availableBatchArray = $availableBatchCollection->keyBy('sync_code')->toArray();
        $plantGroupIds = $availableBatchCollection->filter(
            function ($batch) {
                return $batch['source_type'] === PlantGroup::class;
            }
        )->pluck('source_id')->toArray();

        $growCyclePlantGroupRelationTable = 'grow_cycle_plant_group';
        /**
         * ['plant_group_id' => 'grow_cycle_id']
         */
        $existingGrowCyclePlantGroupRelationArray = DB::table($growCyclePlantGroupRelationTable)
            ->whereIn('plant_group_id', $plantGroupIds)
            ->pluck('grow_cycle_id', 'plant_group_id')->toArray();

        /**
         * ['id' => ['id', 'est_harvested_at', 'grow_status']]
         */
        $existingGrowCycleArray = GrowCycle::licenseId($licenseId)
            ->whereIn('id', $existingGrowCyclePlantGroupRelationArray)
            ->select('id', 'est_harvested_at', 'grow_status')
            ->get()->keyBy('id')->toArray();
        $existingPlantGroupArray = PlantGroup::licenseId($licenseId)->whereIn('id', $plantGroupIds)
            ->pluck('propagation_id', 'id')->toArray();
        $usedStrainIds = $usedRoomIds = [];

        $newPlantData = $newHarvestPlantRelationData = [];
        foreach ($importableData as $item) {
            try {
                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingPlantArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }

                $batchSyncCode = Arr::get($item, 'global_batch_id');
                if (!$batchSyncCode || empty($availableBatchArray[$batchSyncCode])) {
                    continue; // skip
                }
                $batch = $availableBatchArray[$batchSyncCode];

                $strainSyncCode = Arr::get($item, 'global_strain_id');
                if (!$strainSyncCode || empty($availableStrainArray[$strainSyncCode])) {
                    throw new Exception('Strain not found');
                }
                $strainId = $availableStrainArray[$strainSyncCode];

                $roomSyncCode = Arr::get($item, 'global_area_id');
                if (!$roomSyncCode || empty($availableRoomArray[$roomSyncCode])) {
                    throw new Exception('Room not found');
                }
                $roomId = $availableRoomArray[$roomSyncCode];

                $usedStrainIds[$strainId] = 1;
                $usedRoomIds[$roomId] = 1;

                $growStatus = $growCycleId = $plantGroupId = $estHarvestedAt = $propagationId = null;

                $plantId = Uuid::uuid4()->toString();
                switch ($batch['source_type']) {
                    case Harvest::class:
                        $growStatus = SeedToSale::GROW_STATUS_HARVESTED;
                        $harvestId = $batch['source_id'];
                        $newHarvestPlantRelationData[] = [
                            'id' => Uuid::uuid4()->toString(),
                            'harvest_id' => $harvestId,
                            'plant_id' => $plantId,
                        ];
                        break;
                    case PlantGroup::class:
                        $plantGroupId = $batch['source_id'];

                        // grow_cycle_plant_group table to get grow_cycle_id
                        // -> grow_cycles table to get grow_status, est_harvested_at
                        // plant_groups table to get propagation_id
                        if (!empty($existingGrowCyclePlantGroupRelationArray[$plantGroupId])) {
                            $growCycleId = $existingGrowCyclePlantGroupRelationArray[$plantGroupId];
                            if (!empty($existingGrowCycleArray[$growCycleId])) {
                                $growStatus = $existingGrowCycleArray[$growCycleId]['grow_status'];
                                $estHarvestedAt = $existingGrowCycleArray[$growCycleId]['est_harvested_at'];
                            }
                        }
                        if (!empty($existingPlantGroupArray[$plantGroupId])) {
                            $propagationId = $existingPlantGroupArray[$plantGroupId];
                        }
                        break;
                    default:
                        throw new Exception('Batch source is not plant group or harvest');
                }
                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                $newPlantData[] = [
                    'id' => $plantId,
                    'license_id' => $licenseId,
                    'internal_id' => ModelHelper::generateInternalId(
                        Plant::class,
                        $licenseId
                    ),
                    'strain_id' => $strainId,
                    'room_id' => $roomId,
                    'source_type' => Arr::get($item, 'origin'),
                    'grow_status' => $growStatus,
                    'grow_cycle_id' => $growCycleId,
                    'plant_group_id' => $plantGroupId,
                    'est_harvested_at' => $estHarvestedAt,
                    'propagation_id' => $propagationId,
                    'planted_at' => LeafHelper::getDateObject(
                            self::DATE_FORMAT,
                            Arr::get($item, 'plant_created_at')
                        ) ?? LeafHelper::getDateTimeObject(
                            self::DATETIME_FORMAT,
                            Arr::get($item, 'created_at') // use created_at by default
                        ),
                    'harvested_at' => LeafHelper::getDateObject(
                        self::DATE_FORMAT,
                        Arr::get($item, 'plant_harvested_at')
                    ),
                    'mother_code' => Arr::get($item, 'global_mother_plant_id'),
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                    'sync_code' => $globalId,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => $today,
                ];
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        try {
            DB::beginTransaction();
            if (!empty($newPlantData)) {
                Plant::insert($newPlantData);
            }
            if (!empty($newHarvestPlantRelationData)) {
                HarvestPlant::insert($newHarvestPlantRelationData);
            }

            // enable used strains
            if (!empty($usedStrainIds)) {
                Strain::query()->where('status', Strain::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedStrainIds))
                    ->update(['status' => Strain::STATUS_ENABLED]);
            }

            // enable used rooms
            if (!empty($usedRoomIds)) {
                Room::query()->where('status', Room::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedRoomIds))
                    ->update(['status' => Room::STATUS_ENABLED]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['success' => $success, 'error' => $error];
    }
}
