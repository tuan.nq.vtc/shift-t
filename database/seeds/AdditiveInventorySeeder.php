<?php

use App\Models\{Additive, AdditiveInventory, Supplier, User};
use Illuminate\Database\Eloquent\{Collection};
use Illuminate\Database\Seeder;

/**
 * Class AdditiveInventorySeeder
 */
class AdditiveInventorySeeder extends Seeder
{
    private ?Collection $users = null;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $suppliers = Supplier::where('account_holder_id','f136252a-adfd-42a9-96a1-4ece04730600')->get();

        $additiveIteration = fn(Additive $additive) => call_user_func(
            [$this, 'iterateAdditive'],
            $additive,
            $suppliers
        );
        Additive::all()->each($additiveIteration);
    }

    /**
     * @param Additive $additive
     * @param          $suppliers
     */
    protected function iterateAdditive(Additive $additive, $suppliers)
    {
        foreach ($suppliers as $supplier) {
            $quantity = rand(10, 99);
            factory(AdditiveInventory::class)->create(
                [
                    'license_id' => $additive->license_id,
                    'additive_id' => $additive->id,
                    'supplier_id' => $supplier->id,
                    'total_price' => rand(20, 50),
                    'total_quantity' => $quantity,
                    'available_quantity' => $quantity,
                    'uom' => AdditiveInventory::UOM_GALLON,
                    'created_by' => $this->getRandomUserId(),
                ]
            );
        }
    }

    private function getRandomUserId(): ?string
    {
        if (!$this->users) {
            $this->users = User::all();
        }
        if ($this->users->count() === 0) {
            return null;
        }

        return $this->users->random()->id;
    }
}
