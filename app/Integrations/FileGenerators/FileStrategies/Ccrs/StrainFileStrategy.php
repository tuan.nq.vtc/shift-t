<?php

namespace App\Integrations\FileGenerators\FileStrategies\Ccrs;

class StrainFileStrategy extends FileStrategy
{
    protected static string $fileIdentifier = 'strain';

    public function getHeaders(): array
    {
        $headers = parent::getHeaders();
        $headers[] = ['Strain', 'StrainType', 'CreatedBy', 'CreatedDate'];
        return $headers;
    }

    public function getRow($model): array
    {
        $strain = $model->getResource();

        return [
            $strain->name,
            $strain->type,
            $this->getShortenedtUserName($strain->creator),
            $this->formatDateRow($strain->created_at),
        ];
    }
}
