<?php

namespace App\Models;

use App\Events\WasteBag\WasteBagCreated;
use App\Models\Traits\{Scope\LicenseFilterable, StoreInRoom};
use Illuminate\Database\Eloquent\Relations\{BelongsTo, BelongsToMany, MorphOne};

/**
 * Class WasteBag
 *
 * @package App\Models
 */
class WasteBag extends TraceableModel
{
    use LicenseFilterable;
    use StoreInRoom;

    /**
     * @return string[]
     */
    public const REASONS = [
        'unhealthy_died',
        'infestation',
        'pruning',
        'other'
    ];

    protected $fillable = [
        'license_id',
        'room_id',
        'cut_by_id',
        'reason',
        'comment',
        'weight',
        'destroy_at',
        'wasted_at',
    ];

    protected $casts = [
        'strain_ids' => 'array',
    ];

    protected $dates = ['destroy_at', 'wasted_at', 'synced_at'];

    protected $dispatchesEvents = [
        'created' => WasteBagCreated::class,
    ];

    public function getMappings(): ?array
    {
        // TODO: Implement getMappings() method.
        return null;
    }

    /**
     * @return BelongsTo
     */
    public function cutBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function disposal(): MorphOne
    {
        return $this->morphOne(Disposal::class, 'wastable');
    }

    public function strains(): BelongsToMany
    {
        return $this->belongsToMany(Strain::class, 'waste_bag_strains');
    }
}
