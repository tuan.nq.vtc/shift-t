<?php

namespace App\Integrations\FileGenerators\Resolvers;

use App\Integrations\FileGenerators\Contracts\FileStrategyInterface;
use App\Integrations\FileGenerators\Exceptions\FileGeneratorException;
use App\Models\License;
use App\Models\State;
use ReflectionClass;

class FileStrategyResolver
{
    private const FILE_STRATEGIES_NAMESPACE_PREFIX = "App\\Integrations\\FileGenerators\\FileStrategies\\";

    public static function resolve(License $license, string $resourceClassName): FileStrategyInterface
    {
        $fileStrategyClassName = self::FILE_STRATEGIES_NAMESPACE_PREFIX;
        $resource = new ReflectionClass($resourceClassName);
        $resourceShortName = $resource->getShortName();

        switch ($license->state->regulator) {
            case State::REGULATOR_CCRS:
                $fileStrategyClassName .= "Ccrs\\{$resourceShortName}FileStrategy";
                if (!class_exists($fileStrategyClassName)) {
                    throw new FileGeneratorException("Cannot resolve FileStrategy for resource {$resourceClassName}");
                }

                return new $fileStrategyClassName($license);
            default:
                throw new FileGeneratorException(
                    "Can not resolve FileGeneratorStrategy for state [{$license->state_code}]"
                );
        }
    }
}
