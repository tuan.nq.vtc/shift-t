<?php

use App\Models\{GrowCycle, Plant, PlantGroup, Propagation, Room};
use Illuminate\Database\Seeder;

/**
 * Class PlantTableSeeder.
 */
class PlantTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run(): void
    {
        Propagation::all()->load(['batch', 'strain'])->each(
            fn(Propagation $propagation) => $this->iteratePropagation($propagation)
        );
    }

    protected function iteratePropagation(Propagation $propagation): void
    {
        /** @var GrowCycle $growCycle */
        $growCycle = GrowCycle::licenseId($propagation->license_id)->get();
        $room = Room::licenseId($propagation->license_id)->get();
        if ($growCycle->isEmpty() || $room->isEmpty()) {
            return;
        }
        $growCycle = $growCycle->random();
        $plantGroup = factory(PlantGroup::class)->create(
            [
                'license_id' => $propagation->license_id,
                'propagation_id' => $propagation->id,
                'room_id' => $room->random()->id,
                'strain_id' => $propagation->strain_id,
                'name' => 'Group of '.$propagation->strain->name.' in '.$growCycle->name,
            ]
        );
        $growCycle->plantGroups()->attach($plantGroup);

        foreach ($plantGroup->room->subrooms as $subroom) {
            $maxQuantity = 5;
            $plantQuantity = $propagation->quantity < $maxQuantity ? $propagation->quantity : $maxQuantity;
            factory(Plant::class, $plantQuantity)->create(
                [
                    'license_id' => $propagation->license_id,
                    'propagation_id' => $propagation->id,
                    'plant_group_id' => $plantGroup->id,
                    'strain_id' => $propagation->strain_id,
                    'source_type' => $propagation->source_type,
                    'destroyed' => 0,
                    'grow_cycle_id' => $growCycle->id,
                    'grow_status' => $growCycle->grow_status,
                    'room_id' => $plantGroup->room_id,
                    'subroom_id' => $subroom->id,
                ]
            );
        }
    }
}
