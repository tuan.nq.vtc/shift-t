<?php

namespace App\Streams\ConsumerEventProcessors;

use App\Models\{EndProduct};
use App\Services\{EndProductService};
use Bamboo\StreamProcessing\Contracts\StreamEventInterface;

class ProductProcessor extends BaseProcessor
{
    protected static string $modelName = EndProduct::class;

    protected function create(StreamEventInterface $streamEvent): void
    {
        $payload = $streamEvent->getPayload();
        app(EndProductService::class)->syncProduct($payload);
    }

    public function update(StreamEventInterface $streamEvent): void
    {
        $payload = $streamEvent->getPayload();
        app(EndProductService::class)->syncProduct($payload);
    }
}
