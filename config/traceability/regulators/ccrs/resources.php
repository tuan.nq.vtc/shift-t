<?php

return [
    [
        'resourceClassName' =>  \App\Models\Strain::class,
        'dependencies' => [],
        'dependency_group' => 1,
        'license_types' => ['cultivator','production','cultivator_production','dispensary'],
    ],
    [
        'resourceClassName' =>  \App\Models\Room::class,
        'dependencies' => [],
        'dependency_group' => 1,
        'license_types' => ['cultivator','production','cultivator_production','dispensary'],
    ],
    /*
    [
        'resourceClassName' =>  \App\Models\SourceProduct::class,
        'dependencies' => [],
        'dependency_group' => 1,
        'license_types' => ['cultivator','production','cultivator_production','dispensary'],
    ],
    [
        'resourceClassName' =>  \App\Models\Inventory::class,
        'dependencies' => [ \App\Models\Strain::class,  \App\Models\Room::class,  \App\Models\SourceProduct::class],
        'dependency_group' => 2,
        'license_types' => ['cultivator','production','cultivator_production','dispensary'],
    ],
    [
        'resourceClassName' =>  \App\Models\Plant::class,
        'dependencies' => [ \App\Models\Strain::class,  \App\Models\Room::class,  \App\Models\SourceProduct::class],
        'dependency_group' => 2,
        'license_types' => ['cultivator','cultivator_production'],
    ],
    */
];
