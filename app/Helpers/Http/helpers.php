<?php

use App\Helpers\Http\ResponseHelper;
use Illuminate\Http\JsonResponse;

if (!function_exists('wpApiResponse')) {
    function wpApiResponse($data, $httpStatus = 200): JsonResponse
    {
        return ResponseHelper::getInstance()
            ->withData($data)
            ->withHttpStatus($httpStatus)
            ->response();
    }
}

if (!function_exists('wpApiResponseWithErrors')) {
    function wpApiResponseWithErrors($errors, string $errorMessage, $httpStatus = 400): JsonResponse
    {
        return ResponseHelper::getInstance()
            ->withErrors($errors)
            ->withErrorMessage($errorMessage)
            ->withHttpStatus($httpStatus)
            ->responseWithErrors();
    }
}
