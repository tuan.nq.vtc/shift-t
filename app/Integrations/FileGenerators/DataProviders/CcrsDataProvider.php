<?php

namespace App\Integrations\FileGenerators\DataProviders;

use App\Models\License;
use App\Models\Strain;
use App\Models\Trace;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class CcrsDataProvider extends BaseDataProvider
{
    /*
    TRACE STATUS
    ------------
    STATUS_PENDING = Traces ready to generate files
    STATUS_SEND_PROCESSING = Traces being processed to generate a file
    STATUS_SEND_COMPLETED = Traces with generated file
    STATUS_COMPLETED = Traces with uploaded file
    STATUS_SEND_FAILED = File generation error
    STATUS_PROCESS_FAILED = File upload error


    MODEL SYNC_STATUS
    -----------------
    SYNC_STATUS_PENDING = Trace.{STATUS_PENDING,STATUS_SEND_PROCESSING}
    SYNC_STATUS_PROCESSING = Trace.{STATUS_SEND_COMPLETED}
    SYNC_STATUS_SYNCED = Trace.STATUS_COMPLETED
    SYNC_STATUS_FAILED = Trace.{STATUS_SEND_FAILED,STATUS_PROCESS_FAILED}
    */
    public static function getFilesToUploadByLicense(License $license, string $resourceClassName): array
    {
        $files = Trace::withoutGlobalScope(Trace::DEFAULT_ORDER_SCOPE)
            ->select('file_reference')
            ->distinct('file_reference')
            ->where('license_id', $license->id)
            ->where('status', Trace::STATUS_SEND_COMPLETED)
            ->where('resource_type', $resourceClassName)
            ->whereNotNull('file_reference');

        return $files->get()->pluck('file_reference')->toArray();
    }

    private static function setFileUploadStatus(string $fileReference, string $newFileReference, int $status, int $resourcesStatus = -1): bool 
    {
        if($resourcesStatus > -1) {
            $records = Trace::select('resource_type', 'resource_id')
            ->where('file_reference', $fileReference)->get();
    
            if($records->count() == 0) {
                return false;
            }
    
            $resourceClassName = $records->pluck('resource_type')->first();
            
            if (!is_null($resourceClassName)) {
                app($resourceClassName)->whereIn('id', $records->pluck('resource_id'))
                ->update(
                    ['sync_status' => $resourcesStatus] +
                    ( $resourcesStatus == TraceableModelInterface::SYNC_STATUS_SYNCED  ? ['synced_at' => Carbon::now()] : [] )
                );
            }    
        }

        Trace::where('file_reference', $fileReference)
        ->update(
            ['status' => $status] + ($fileReference != $newFileReference ? ['file_reference' => $newFileReference] : [])
        );

        return true;
    }

    public static function setFileUploadSuccess(string $fileReference, string $newFileReference): bool
    {
        return self::setFileUploadStatus($fileReference, $newFileReference, Trace::STATUS_COMPLETED, TraceableModelInterface::SYNC_STATUS_SYNCED);
    }

    public static function setFileUploadError(string $fileReference, string $newFileReference = ''): bool
    {
        return self::setFileUploadStatus($fileReference, $newFileReference, Trace::STATUS_PROCESS_FAILED, TraceableModelInterface::SYNC_STATUS_FAILED);
    }

    public static function setFileUploadInProcess(string $fileReference): bool
    {
        return self::setFileUploadStatus($fileReference, $fileReference, Trace::STATUS_PROCESS_PROCESSING);
    }
    

    public function setRecordsFailed(): void
    {
        Trace::whereIn('id', $this->currentRecords->pluck('id'))
            ->update(['status' => Trace::STATUS_SEND_FAILED]);

        app($this->resourceClassName)->whereIn('id', $this->currentRecords->pluck('resource_id'))
            ->update(['sync_status' => TraceableModelInterface::SYNC_STATUS_FAILED]);
    }

    public function setRecordsInProcess(): void
    {
        Trace::whereIn('id', $this->currentRecords->pluck('id'))
            ->update(['status' => Trace::STATUS_SEND_PROCESSING]);
    }

    public function setRecordsCompleted(string $fileReference = ''): void
    {
        Trace::whereIn('id', $this->currentRecords->pluck('id'))
            ->update(['status' => Trace::STATUS_SEND_COMPLETED, 'file_reference' => $fileReference]);

        app($this->resourceClassName)::whereIn('id', $this->currentRecords->pluck('resource_id'))
            ->update(['sync_status' => TraceableModelInterface::SYNC_STATUS_PROCESSING]);
    }

    public function prepareData(): void
    {
        //Set traces with STATUS_SEND_FAILED to STATUS_PENDING, to process them in the next run
        $builder = Trace::where('status', Trace::STATUS_SEND_FAILED);
        $builder = $this->addBuilderGeneralFilters($builder);
        $builder->update(['status' => Trace::STATUS_PENDING]);
    }

    protected function uniqueCallback($item)
    {
        if ($this->resourceClassName == Strain::class) {
            return $item->resource_type . $item->resource_id;
        }

        return $item->resource_type . $item->resource_id . $item->action;
    }

    protected function getBuilder(): Builder
    {
        $builder = Trace::select('id', 'resource_type', 'resource_id', 'action')
            ->where('status', Trace::STATUS_PENDING)
            ->orderBy('created_at', 'asc');
        $builder = $this->addBuilderGeneralFilters($builder);

        return $builder;
    }

    private function addBuilderGeneralFilters(Builder $builder): Builder
    {
        $builder = $builder->where('resource_type', $this->resourceClassName)
            ->where('license_id', $this->license->id);

        if ($this->resourceClassName == Strain::class) {
            $builder->whereIn('action', [Trace::ACTION_CREATE, Trace::ACTION_UPDATE]);
        }

        return $builder;
    }
}
