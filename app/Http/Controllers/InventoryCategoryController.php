<?php

namespace App\Http\Controllers;

use App\Services\InventoryCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\{Request, Response};
use Laravel\Lumen\Routing\Controller;

/**
 * Class InventoryCategoryController
 * @package App\Http\Controllers
 *
 * @property InventoryCategoryService $service
 */
class InventoryCategoryController extends CRUDController
{

    public function __construct(InventoryCategoryService $service, Request $request)
    {

        parent::__construct($service, $request);
    }

    public function index() : JsonResponse
    {
        return response()->json(
            $this->service->index(),
            Response::HTTP_OK
        );
    }

    public function getCreateRules(): array
    {
        return [];
    }

    public function getUpdateRules(string $id): array
    {
        return [];
    }
}
