<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManifestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifest_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('manifest_id');
            $table->unsignedSmallInteger('status')->index()->default(0);
            $table->string('type', 32)->index();
            $table->uuid('strain_id')->nullable();
            $table->uuidMorphs('resource');
            $table->decimal('quantity', 12, 2)->default(0);
            $table->decimal('price', 12, 2)->default(0);
            $table->decimal('total', 12, 2)->default(0);
            $table->unsignedTinyInteger('is_sample')->default(0);
            $table->unsignedTinyInteger('is_for_extraction')->default(0);
            $table->timestamps();

            $table->foreign('manifest_id')->references('id')->on('manifests')->cascadeOnDelete();
            $table->foreign('strain_id')->references('id')->on('strains')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manifest_items');
    }
}
