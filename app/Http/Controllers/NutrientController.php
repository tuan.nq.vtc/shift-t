<?php


namespace App\Http\Controllers;


use App\Models\Additive;
use App\Services\{AdditiveInventoryService,
    AdditiveService,
    GrowCycleService,
    PlantService,
    PropagationService,
    RoomService
};
use Illuminate\Http\{Request};

class NutrientController extends AdditiveController
{
    public function __construct(
        AdditiveService $service,
        AdditiveInventoryService $additiveInventoryService,
        PlantService $plantService,
        RoomService $roomService,
        GrowCycleService $growCycleService,
        PropagationService $propagationService,
        Request $request
    ) {
        $this->setOtherTypeForRequest($request);
        $request->merge(['type' => Additive::TYPE_NUTRIENT]);
        parent::__construct(
            $service,
            $additiveInventoryService,
            $plantService,
            $roomService,
            $growCycleService,
            $propagationService,
            $request,
            Additive::TYPE_NUTRIENT
        );
    }

}
