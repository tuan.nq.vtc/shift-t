<?php

namespace App\Rules;

use App\Models\{Harvest, HarvestGroup, Room, RoomType};
use Illuminate\Support\Arr;

class HarvestingRoomRule
{
    private static $harvests = [];
    const ROOM_FLOWER = 'flower_room_id';
    const ROOM_MATERIAL = 'material_room_id';
    const ROOM_WASTE = 'waste_room_id';

    public static function requireif(HarvestGroup $harvestGroup, array $harvestRequest, string $attribute, $value, $fail)
    {
        // attribute must has format like: harvests.0.flower_room_id
        $attributes = explode('.', $attribute);
        if (count($attributes) !== 3) {
            return $fail(__('exceptions.harvest.confirm.harvest_invalid'));
        }
        /** @var Harvest $harvest * */
        $harvest = self::getHarvestObject($harvestGroup, $harvestRequest, $attributes[1]);

        $weightField = self::getWeightField($attributes[2]);

        $conditionCheckWeight = ($harvest->group->type === HarvestGroup::TYPE_DRY) ?
            $attributes[2] !== self::ROOM_WASTE && $harvest->flower_dry_weight == 0 && $harvest->material_dry_weight == 0 :
            $attributes[2] !== self::ROOM_WASTE && $harvest->flower_wet_weight == 0 && $harvest->material_wet_weight == 0;
        if ($conditionCheckWeight) {
            return $fail(__('exceptions.harvest.confirm.final_weight_invalid'));
        }

        if ($harvest->$weightField == 0) {
            return;
        }

        if (!Room::query()
            ->where('license_id', $harvestGroup->license_id)
            ->whereNotNull('sync_code')
            ->whereNull('deleted_at')
            ->whereIn('room_type_id',
                RoomType::query()
                    ->whereIn('category', $attributes[2] === self::ROOM_WASTE ?
                        [RoomType::CATEGORY_DISPOSAL] : [RoomType::CATEGORY_INVENTORY, RoomType::CATEGORY_HARVEST])
                    ->select('id')
            )
            ->where('id', $value)
            ->count()) {
            return $fail(__('exceptions.harvest.confirm.room_invalid', ['type' => self::getRoomType($attributes[2])]));
        }
    }


    private static function getWeightField($roomKey): string
    {
        switch ($roomKey) {
            case self::ROOM_FLOWER:
                return 'flower_dry_weight';
            case self::ROOM_MATERIAL:
                return 'material_dry_weight';
            case self::ROOM_WASTE:
                return 'waste_dry_weight';
        }
        return '';
    }

    private static function getRoomType($type): string
    {
        switch ($type) {
            case self::ROOM_FLOWER:
                return 'flower';
            case self::ROOM_MATERIAL:
                return 'material';
            case self::ROOM_WASTE:
                return 'waste';
        }
        return '';
    }

    private static function getHarvestObject(HarvestGroup $harvestGroup, array $harvestRequest, string $index): ?Harvest
    {
        if (!isset($harvestRequest[$index])) {
            return null;
        }

        $strainId = Arr::get($harvestRequest[$index], 'strain_id', '');
        return self::pullObject($harvestGroup, $strainId);
    }

    private static function pullObject(HarvestGroup $harvestGroup, string $strainId): ?Harvest
    {
        if (!empty(self::$harvests[$strainId])) {
            return self::$harvests[$strainId];
        }
        return self::$harvests[$strainId] = $harvestGroup->harvests->firstWhere('strain_id', $strainId);
    }

}
