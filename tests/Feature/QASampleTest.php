<?php

namespace Tests\Feature;

use App\Cqrs\Commands\CreateQASampleCommand;
use App\Cqrs\Commands\CreateQASampleHandler;
use App\Models\Inventory;
use App\Models\InventoryType;
use App\Models\License;
use App\Models\Licensee;
use App\Models\QASample;
use App\Models\StateCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

class QASampleTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $modelName = QASample::class;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

    /** @test */
    public function user_can_create_qa_sample()
    {
        /**
         * @var User $user
         */
        $user = factory(User::class)->create();
        $license = factory(License::class)->create();
        $lab = factory(Licensee::class)->create();

        $stateCategory = factory(StateCategory::class)->create([
            'regulator' => 'leaf',
            'name' => 'Harvest Materials - Flower Lots'
        ]);

        $inventoryType = factory(InventoryType::class)->create([
            'state_category_id' => $stateCategory->getAttribute('id')
        ]);

        $inventory = factory(Inventory::class)->create(
            [
                'qty_on_hand' => 300,
                'type_id' => $inventoryType->getAttribute('id'),
                'license_id' => $licenseId = $license->getAttribute('id')
            ]);

        /**
         * @var CreateQASampleHandler $handler
         */
        $handler = app(CreateQASampleHandler::class);
        $collection = $handler->handle(new CreateQASampleCommand([
            'user_id' => $user->id,
            'license_id' => $licenseId,
            'inventories' => [
                [
                    'inventory_id' => $inventory->getAttribute('id'),
                    'sample_type' => QASample::SAMPLE_MANDATORY,
                    'lab_id' => $lab->getAttribute('id'),
                    'status' => QASample::STATUS_OPEN,
                    'weight' => 4,
                    'created_at' => date('Y-m-d H:i:s')
                ]
            ]
        ]));

        /**
         * @var Inventory $in
         */
        $in = Inventory::find($inventory->getAttribute('id'));

        $this->assertTrue($collection instanceof Collection);
        $this->assertTrue($collection[0] instanceof QASample);
        $this->assertTrue($in->getAttribute('qa_status') == Inventory::QA_STATUS_SAMPLED_TEST);
    }
}
