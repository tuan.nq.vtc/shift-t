<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait SyncCodesFilterable
{
    /**
     * @param Builder $query
     * @param mixed ...$syncCodes
     * @return Builder
     */
    public function scopeSyncCodes(Builder $query, ...$syncCodes)
    {
        return $query->whereIn('sync_code', $syncCodes);
    }
}
