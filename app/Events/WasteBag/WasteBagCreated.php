<?php

namespace App\Events\WasteBag;

use App\Events\Event;
use App\Models\WasteBag;

class WasteBagCreated extends Event
{
    /**
     * @var WasteBag
     */
    public WasteBag $wasteBag;

    /**
     * @param WasteBag $wasteBag
     */
    public function __construct(WasteBag $wasteBag)
    {
        $this->wasteBag = $wasteBag;
    }
}
