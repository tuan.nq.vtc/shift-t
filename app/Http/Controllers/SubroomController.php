<?php

namespace App\Http\Controllers;

use App\Services\SubroomService;
use Illuminate\Http\Request;

/**
 * Class SubroomController
 * @package App\Http\Controllers
 */
class SubroomController extends CRUDController
{
    public function __construct(SubroomService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        $licenseId = $this->request->get('license_id');

        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\Subroom,name,NULL,id,license_id,'.$licenseId,
            ],
            'room_id' => ['required','exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',],
            'dimension.width' => ['required', 'numeric'],
            'dimension.length' => ['required', 'numeric'],
            'number' => ['required', 'integer', 'gte:1'],
            'type' => ['required'],
            'color' => ['required'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id');

        return [
            'name' => [
                'sometimes',
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\Subroom,name,'.$id.',id,room_id,'.$this->request->get('room_id'),
            ],
            'status' => ['sometimes', 'required', 'boolean'],
            'room_id' => ['sometimes', 'required','exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',],
            'dimension.width' => ['sometimes', 'required', 'numeric'],
            'dimension.length' => ['sometimes', 'required', 'numeric'],
            'number' => ['required', 'integer', 'gte:1'],
            'type' => ['sometimes', 'required'],
            'color' => ['sometimes', 'required'],
        ];
    }
}
