<?php

namespace App\Jobs;

use App\Models\Room;
use App\Services\RoomStatisticService;
use Illuminate\Support\Facades\Log;
use Throwable;

class GenerateRoomStatistic extends Job
{
    private Room $room;

    public function __construct(Room $room)
    {
        $this->queue = 'generate_room_statistic';
        $this->room = $room;
    }

    /**
     * @param RoomStatisticService $roomStatService
     */
    public function handle(RoomStatisticService $roomStatService)
    {
        $roomStatService->generateRoomStatistic($this->room);
        Log::info("Statistic the room's ID: " . $this->room->id . ' done');
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception)
    {
        Log::error("Queue {$this->queue} - push [{$this->room->id}] failed: {$exception->getMessage()}");
    }
}
