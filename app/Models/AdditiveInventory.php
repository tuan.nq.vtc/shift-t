<?php

namespace App\Models;

use App\Events\AdditiveInventory\{AdditiveInventoryCreating, AdditiveInventorySaved};
use App\Models\Traits\Scope\{CreatorFilterable, DatetimeFilterable, LicenseFilterable, ModifierFilterable};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, Relations\HasMany};

/**
 * Class AdditiveInventory
 *
 * @package App\Models
 * @property string                                                                        $id
 * @property string                                                                        $license_id
 * @property string                                                                        $additive_id
 * @property float                                                                         $total_quantity
 * @property float                                                                         $available_quantity
 * @property float                                                                         $unit_price
 * @property float                                                                         $total_price
 * @property string                                                                        $uom
 * @property string                                                                        $created_by
 * @property \Illuminate\Support\Carbon|null                                               $created_at
 * @property \Illuminate\Support\Carbon|null                                               $updated_at
 * @property string                                                                        $supplier_id
 * @property-read \App\Models\Additive                                                     $additive
 * @property-read \App\Models\User                                                         $creator
 * @property-read bool                                                                     $depleted
 * @property-read \App\Models\Supplier                                                     $supplier
 * @method static Builder|AdditiveInventory additiveId($value)
 * @method static Builder|AdditiveInventory createdAt($firstValue, $secondValue = null)
 * @method static Builder|AdditiveInventory createdBy($value)
 * @method static Builder|BaseModel defaultOrderBy()
 * @method static Builder|AdditiveInventory depleted($value)
 * @method static Builder|AdditiveInventory licenseId(string $licenseId)
 * @method static Builder|AdditiveInventory newModelQuery()
 * @method static Builder|AdditiveInventory newQuery()
 * @method static Builder|AdditiveInventory orderByDepleted($direction)
 * @method static Builder|AdditiveInventory query()
 * @method static Builder|AdditiveInventory search($value)
 * @method static Builder|AdditiveInventory supplierName($value)
 * @method static Builder|AdditiveInventory updatedAt($firstValue, $secondValue = null)
 * @method static Builder|AdditiveInventory updatedBy($value)
 * @method static Builder|AdditiveInventory whereAdditiveId($value)
 * @method static Builder|AdditiveInventory whereApplicationDevice($value)
 * @method static Builder|AdditiveInventory whereAvailableQuantity($value)
 * @method static Builder|AdditiveInventory whereCreatedAt($value)
 * @method static Builder|AdditiveInventory whereCreatedBy($value)
 * @method static Builder|AdditiveInventory whereId($value)
 * @method static Builder|AdditiveInventory whereLicenseId($value)
 * @method static Builder|AdditiveInventory whereSupplierId($value)
 * @method static Builder|AdditiveInventory whereTotalPrice($value)
 * @method static Builder|AdditiveInventory whereTotalQuantity($value)
 * @method static Builder|AdditiveInventory whereUnitPrice($value)
 * @method static Builder|AdditiveInventory whereUom($value)
 * @method static Builder|AdditiveInventory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdditiveInventory extends BaseModel
{
    use CreatorFilterable;
    use DatetimeFilterable;
    use ModifierFilterable;
    use LicenseFilterable;

    // uom
    public const UOM_GALLON = 'gallon';
    public const UOM_LITER = 'liter';

    protected $fillable = [
        'additive_id',
        'license_id',
        'supplier_id',
        'total_price',
        'total_quantity',
        'available_quantity',
        'unit_price',
        'uom',
        'order_date',
        'created_by',
        'created_at',
        'updated_at',

    ];

    protected $appends = ['depleted', 'purchased_quantity', 'supplier_name'];

    protected $dispatchesEvents = [
        'creating' => AdditiveInventoryCreating::class,
        'saved' => AdditiveInventorySaved::class,
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'total_price' => 'double',
        'total_quantity' => 'double',
        'available_quantity' => 'double',
        'unit_price' => 'double',
    ];

    protected $dates = [
        'order_date',
    ];

    protected $hidden = ['additive'];

    /**
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function additive(): BelongsTo
    {
        return $this->belongsTo(Additive::class, 'additive_id', 'id');
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function additiveOperations(): HasMany
    {
        return $this->hasMany(AdditiveOperation::class, 'additive_inventory_id', 'id');
    }

    /**
     * @param $query Builder
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (!empty($value)) {
            $query
                ->whereHas(
                    'supplier',
                    fn($query) => $query->where("name", 'like', '%'.$value.'%')
                );
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param         $value
     *
     * @return Builder
     */
    public function scopeSupplierName(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (!empty($value)) {
            $query
                ->whereHas(
                    'supplier',
                    fn($query) => $query->where("name", 'like', '%'.$value.'%')
                );
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param         $value
     *
     * @return Builder
     */
    public function scopeDepleted(Builder $query, $value): Builder
    {
        return $query->where('available_quantity', $value ? "=" : ">", 0);
    }

    /**
     * @param Builder $query
     * @param         $value
     *
     * @return Builder
     */
    public function scopeAdditiveId(Builder $query, $value): Builder
    {
        return $query->where('additive_id', $value);
    }

    /**
     * @param Builder $query
     * @param         $direction
     *
     * @return Builder
     */
    public function scopeOrderByDepleted(Builder $query, $direction): Builder
    {
        return $query->orderBy('available_quantity', $direction);
    }

    public function scopeOrderByPurchasedQuantity(Builder $query, $direction): Builder
    {
        return $query->orderByRaw('total_quantity - available_quantity '.$direction);
    }

    /**
     * @param $quantity
     *
     * @return self
     */
    public function decreaseAvailableQuantity($quantity): self
    {
        $this->available_quantity -= $quantity;
        return $this;
    }

    public function getDepletedAttribute(): bool
    {
        return ($this->available_quantity === 0);
    }

    public function getPurchasedQuantityAttribute()
    {
        return $this->total_quantity - $this->available_quantity;
    }

    public function getSupplierNameAttribute()
    {
        return $this->supplier->name;
    }
}
