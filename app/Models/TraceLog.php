<?php

namespace App\Models;

use App\Models\Traits\WithoutEvents;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Trace
 * @property int status
 * @property array response_data
 * @package App\Models
 */
class TraceLog extends BaseModel
{
    use WithoutEvents;

    public const STATUS_PENDING = 0;
    public const STATUS_SUCCESS = 1;
    public const STATUS_FAILED = 2;

    public $timestamps = false;

    protected $fillable = [
        'trace_id',
        'http_method',
        'request_url',
        'request_query',
        'request_body',
        'response_data',
        'status',
        'exception',
        'sent_at',
    ];

    protected $casts = [
        'request_query' => 'array',
        'request_body' => 'array',
        'response_data' => 'array',
        'sent_at' => 'datetime'
    ];

    public function trace(): BelongsTo
    {
        return $this->belongsTo(Trace::class);
    }

    /**
     * Convert request query params from array to string
     *
     * @return string|null
     */
    public function getRequestQueryString(): ?string
    {
        return http_build_query(empty($this->request_query) ? [] : $this->request_query);
    }
}
