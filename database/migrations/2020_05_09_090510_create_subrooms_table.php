<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'subrooms',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('name')->index()->nullable();
                $table->uuid('room_id')->index();
                $table->unsignedTinyInteger('status')->default(1);
                $table->integer('number')->nullable();
                $table->enum('type', ['rack', 'bed']);
                $table->json('dimension')->nullable();
                $table->string('color', 7)->nullable();

                $table->softDeletes();
                $table->timestamps();

                $table->foreign('room_id')->references('id')->on('rooms')->cascadeOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subrooms');
    }
}
