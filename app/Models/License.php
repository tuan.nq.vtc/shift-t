<?php

namespace App\Models;

use App\Jobs\FetchLicenseResource;
use Illuminate\Database\Eloquent\{Builder,
    Relations\BelongsTo,
    Relations\BelongsToMany,
    Relations\HasMany,
    SoftDeletes
};
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class License
 *
 * @property string code
 * @property string state_code
 * @property State state
 * @package App\Models
 * @method Builder isTest()
 */
class License extends BaseModel
{
    use SoftDeletes;

    public const STATUS_UNLINKED = 0;
    public const STATUS_ACTIVE_LINKED = 1;
    public const STATUS_ON_TRIAL = 2;

    public const SYNC_PENDING = 0;
    public const SYNCING = 1;
    public const SYNC_SUCCEED = 2;
    public const SYNC_FAILED = 3;

    protected $fillable = [
        'organization_id',
        'account_holder_id',
        'type',
        'name',
        'state_code',
        'code',
        'status',
        'api_configuration',
        'sync_date_range',
        'sync_status',
        'synced_at',
        'is_test',
        'account_holder',
    ];

    protected $casts = [
        'api_configuration' => 'array',
        'status' => 'integer',
        'sync_status' => 'integer',
        'is_test' => 'boolean',
    ];

    protected $dates = [
        'synced_at'
    ];

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class, 'state_code', 'code');
    }

    public function licenseResources(): HasMany
    {
        return $this->hasMany(LicenseResource::class);
    }

    public function plantGroups(): HasMany
    {
        return $this->hasMany(PlantGroup::class, 'license_id', 'id');
    }

    public function stateCategories(): BelongsToMany
    {
        return $this->belongsToMany(StateCategory::class);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeIsTest(Builder $query)
    {
        return $query->where('is_test', true);
    }

    public function getStateBaseUrl(): ?string
    {
        return $this->state->getRegulatorBaseUrl();
    }

    public function getSortedResources(): Collection
    {
        $this->loadMissing(['licenseResources']);

        $regulator = $this->isRegulator(State::REGULATOR_LEAF) ? State::REGULATOR_LEAF : null;
        $regulator = $this->isRegulator(State::REGULATOR_METRC) ? State::REGULATOR_METRC : $regulator;
        $chainedLicenseResources = LicenseResource::RESOURCE_CHAIN_BY_REGULATOR[$regulator];
        return $this->licenseResources->sortByDesc(
            function (LicenseResource $licenseResource) use ($chainedLicenseResources) {
                return (int)array_search($licenseResource->resource, $chainedLicenseResources);
            }
        );
    }

    public function isRegulator(string $regulator): bool
    {
        return optional($this->state)->regulator === $regulator;
    }

    public function getStateResource(string $resourceClass): ?string
    {
        $stateResources = $this->getStateResources();
        if (!$stateResources || empty($stateResources[$resourceClass])) {
            return null;
        }

        return $stateResources[$resourceClass];
    }

    public function getStateResources(): ?array
    {
        $resources = $this->state->getRegulatorResources();

        if($this->isRegulator(State::REGULATOR_CCRS)) {
            $licenseType = $this->type;
            $resources = array_filter($resources, function($resource) use ($licenseType) {
                return isset($resource['license_types']) ? in_array($licenseType, $resource['license_types']) : false;
            });
        }

        return $resources;
    }

    public function getLogic(): ?array
    {
        return $this->state->getRegulatorLogic();
    }

    /**
     * @return array|null
     */
    public function getDateRange(): ?array
    {
        if (!$this->sync_date_range) {
            return null;
        }
        $availableDateRanges = config('licenses.date_ranges');
        if (empty($availableDateRanges[$this->sync_date_range])) {
            return null;
        }
        $dateRange = $availableDateRanges[$this->sync_date_range];
        $fromDate = Carbon::now()->modify('-' . $dateRange);
        $toDate = Carbon::now();
        return [$fromDate, $toDate];
    }

    public function isMetrcSystem(): bool
    {
        return $this->isRegulator(State::REGULATOR_METRC);
    }

    public function getRegulator(): string
    {
        $state = $this->state;
        if ($state instanceof State !== true) {
            return '';
        }

        return $state->regulator;
    }

    public function hasAsyncIntegration(): bool
    {
        return $this->state->hasAsyncIntegration();
    }
}
