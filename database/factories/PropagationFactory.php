<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Inventory, License, Propagation, Room, SeedToSale, Strain, Subroom, TraceableModel, User};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Propagation::class,
    fn(Faker $faker) => [
        "license_id" => function () {
            return factory(License::class)->create()->id;
        },
        "internal_id" => fn($data) => ModelHelper::generateInternalId(
            Propagation::class,
            $data['license_id']
        ),
        "strain_id" => function ($data) {
            return factory(Strain::class)->create(['license_id' => $data['license_id']])->id;
        },
        "name" => $faker->words(2, true),
        "source_type" => $faker->randomElement(SeedToSale::SOURCE_TYPES),
        "room_id" => function ($data) {
            return factory(Room::class)->create(['license_id' => $data['license_id']])->id;
        },
        "subroom_id" => function ($data) {
            return factory(Subroom::class)->create(['room_id' => $data['room_id']])->id;
        },
        "quantity" => $faker->randomNumber(2),
        "date" => $faker->dateTimeThisYear(),
        "cut_by_id" => function () {
            return factory(User::class)->create()->id;
        },
        "plugged_by_id" => function () {
            return factory(User::class)->create()->id;
        },
        'destroyed' => 0,
        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
    ]
);
