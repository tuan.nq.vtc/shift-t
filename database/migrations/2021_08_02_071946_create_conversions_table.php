<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Conversion;

class CreateConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->string('name')->index();
            $table->uuid('output_room_id')->nullable();
            $table->integer('waste_quantity')->nullable();
            $table->string('uom')->default('g');
            $table->uuid('waste_room_id')->nullable();
            $table->uuidMorphs('output_product');
            $table->uuid('output_state_category_id');
            $table->integer('output_quantity')->default(0);
            $table->unsignedTinyInteger('is_altered')->default(Conversion::IS_ALTERED)->index();
            $table->unsignedTinyInteger('status')->default(Conversion::STATUS_OPEN)->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversions');
    }
}
