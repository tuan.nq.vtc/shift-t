<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Services\HarvestGroupService;
use App\Services\PlantService;
use Illuminate\Http\{JsonResponse, Request};
use Laravel\Lumen\Routing\Controller;

/**
 * Class HarvestGroupPlantsController
 * @package App\Http\Controllers
 */
class HarvestGroupPlantsController extends Controller
{
    /**
     * @var HarvestGroupService
     */
    private HarvestGroupService $harvestGroupService;

    /**
     * @var PlantService
     */
    private PlantService $plantService;

    /**
     * @var Request
     */
    private Request $request;

    public function __construct(
        HarvestGroupService $harvestGroupService,
        PlantService $plantService,
        Request $request
    ) {
        $this->harvestGroupService = $harvestGroupService;
        $this->plantService = $plantService;
        $this->request = $request;
    }


    /**
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function list($id)
    {
        return response()->json(
            $this->plantService->listByHarvest(
                $this->harvestGroupService->get($id),
                Criteria::createFromRequest($this->request)
            )
        );
    }

}
