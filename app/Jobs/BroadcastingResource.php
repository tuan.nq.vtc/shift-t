<?php

namespace App\Jobs;

use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Log;
use App\Models\{License, Notification, User};
use App\Notification\Contracts\NotificationInterface;
use App\Services\NotificationService;
use Throwable;

class BroadcastingResource extends Job
{
    public $chained = [];
    public $deleteWhenMissingModels = true;
    public $delay = 5;
    public $maxTries = 5;

    protected NotificationInterface $notifiableResource;
    protected User $changedNotifiableResourceBy;
    protected string $eventName;
    protected int $alarmType;
    protected bool $isSuccess;

    public function __construct(
        NotificationInterface $notifiableResource,
        User $changedNotifiableResourceBy,
        string $eventName,
        int $alarmType = Notification::ALARM_TYPE_INFO,
        bool $isSuccess = true
    )
    {
        $this->queue = 'prepare_notification_resource';
        $this->notifiableResource = $notifiableResource;
        $this->changedNotifiableResourceBy = $changedNotifiableResourceBy;
        $this->eventName = $eventName;
        $this->alarmType = $alarmType;
        $this->isSuccess = $isSuccess;
    }

    /**
     * @throws GeneralException
     * @throws Throwable
     */
    public function handle()
    {
        /** @var NotificationService $notificationService * */
        $notificationService = app(NotificationService::class);
        if (!$this->changedNotifiableResourceBy) {
            throw new GeneralException(
                "Queue {$this->queue} - push [{$this->notifiableResource->id}] failed: the actor is empty"
            );
        }
        $notificationService->prepareDataToAlert(
            $this->notifiableResource,
            $this->changedNotifiableResourceBy,
            $notificationService->getReceiversBy(
                $this->getOrganizationId(),
                $this->changedNotifiableResourceBy->account_holder_id
            ),
            $this->eventName,
            $this->alarmType,
            $this->isSuccess
        );
    }

    private function getOrganizationId()
    {
        $license = License::query()->find($this->notifiableResource->license_id);
        return optional($license)->organization_id;
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception)
    {
        Log::error("Queue {$this->queue} - push [{$this->notifiableResource->id}] failed: {$exception->getMessage()}");
    }
}
