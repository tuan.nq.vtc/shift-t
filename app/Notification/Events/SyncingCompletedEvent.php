<?php

namespace App\Notification\Events;

use App\Events\Event;
use App\Models\User;
use App\Synchronizations\Contracts\TraceableModelInterface;

class SyncingCompletedEvent extends Event
{
    public TraceableModelInterface $traceableModel;
    public ?User $actor;
    public string $action;

    public function __construct(TraceableModelInterface $traceableModel, ?User $actor, string $action)
    {
        $this->traceableModel = $traceableModel;
        $this->actor = $actor;
        $this->action = $action;
    }
}
