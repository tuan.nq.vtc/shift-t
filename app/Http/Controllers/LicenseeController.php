<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\License;
use App\Services\LicenseeService;
use App\Services\LicenseService;
use Illuminate\Http\{JsonResponse, Request};
use Laravel\Lumen\Routing\Controller;

/**
 * Class LicenseeController
 * @package App\Http\Controllers
 */
class LicenseeController extends Controller
{
    /**
     * @var LicenseeService
     */
    private LicenseeService $licenseeService;
    /**
     * @var LicenseService
     */
    private LicenseService $licenseService;

    public function __construct(LicenseeService $licenseeService, LicenseService $licenseService)
    {
        $this->licenseeService = $licenseeService;
        $this->licenseService = $licenseService;
    }

    public function indexByType(Request $request, string $type): JsonResponse
    {
        /** @var License $license */
        $license = $this->licenseService->get($request->get('license_id'));
        return response()->json($this->licenseeService->getByType($type, $license ? $license->state_code : ''));
    }

    public function list(Request $request, ?string $type = null): JsonResponse
    {
        /** @var License $license */
        $license = $this->licenseService->get($request->get('license_id'));
        $criteria = Criteria::createFromRequest($request);
        if ($type) {
            $criteria->addFilter('type', $type);
        }
        $criteria->addFilter('state_code', $license ? $license->state_code : '');
        return response()->json(
            $this->licenseeService->list($criteria)
        );
    }
}
