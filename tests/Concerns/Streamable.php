<?php

namespace Tests\Concerns;

use Bamboo\StreamProcessing\Events\StreamableResourceCreated;
use Bamboo\StreamProcessing\Events\StreamableResourceDeleted;
use Bamboo\StreamProcessing\Events\StreamableResourceUpdated;
use Illuminate\Support\Facades\Event;

trait Streamable
{
    /**
     * @return void
     */
    public function withoutStreamableEvents()
    {
        Event::fake([StreamableResourceCreated::class, StreamableResourceDeleted::class, StreamableResourceUpdated::class]);
    }
}
