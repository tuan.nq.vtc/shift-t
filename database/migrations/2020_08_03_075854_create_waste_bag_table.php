<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteBagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'waste_bags',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('cut_by_id')->nullable();
                $table->uuid('room_id')->index()->nullable();
                $table->string('reason')->nullable();
                $table->text('comment')->nullable();
                $table->decimal('weight', 12, 2)->default(0);

                $table->timestamp('destroy_at')->nullable();
                $table->timestamp('wasted_at')->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_bags');
    }
}
