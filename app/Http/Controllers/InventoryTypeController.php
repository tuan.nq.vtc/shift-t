<?php

namespace App\Http\Controllers;

use App\Models\{InventoryCategory, License, State};
use App\Rules\ModelAttributeRule;
use App\Services\{InventoryTypeService};
use Illuminate\Http\JsonResponse;
use Illuminate\Http\{Request, Response};
use Illuminate\Validation\Rule;

/**
 * Class InventoryTypeController
 * @package App\Http\Controllers
 *
 * @property InventoryTypeService $service
 */
class InventoryTypeController extends CRUDController
{
    public function __construct(
        InventoryTypeService $service,
        Request $request
    )
    {
        parent::__construct($service, $request);
    }

    /**
     * @inheritDoc
     */
    public function getCreateRules(): array
    {
        $licenseId = $this->request->get('license_id');
        $license = License::findOrFail($licenseId);

        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\InventoryType,name,NULL,NULL,license_id,'.$licenseId,
            ],
            'category_id' => [
                'required',
                'uuid',
                "exists:App\Models\InventoryCategory,id,regulator,\"{$license->state->regulator}\""
            ],
            'uom' => $license->state->regulator === State::REGULATOR_LEAF ?
                ['sometimes', 'required', 'string'] :
                [
                    'required',
                    'string',
                    ModelAttributeRule::in(
                        '\App\Models\InventoryCategory',
                        $this->request->get('category_id'),
                        function($inventoryCategory) {
                            switch ($inventoryCategory->quantity_type) {
                                case InventoryCategory::COUNT_BASED:
                                    return Rule::in(InventoryCategory::getCountBasedUnitList());
                                case InventoryCategory::WEIGHT_BASED:
                                    return Rule::in(InventoryCategory::getWeightBasedUnitList());
                                case InventoryCategory::VOLUME_BASED:
                                    return Rule::in(InventoryCategory::getVolumeBasedUnitList());
                                default:
                                    return '';
                            }
                        }
                    ),
                ],
            'strain_id' => [
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_strain_required
                ),
                'uuid'
            ],
            'serving_num' => [
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_serving_required
                ),
                'integer'
            ],
            'serving_size' => [
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_serving_required
                ),
                'integer'
            ],
            'weight' => [
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_weight_per_unit_required
                ),
                'numeric'
            ],
            'volume' => [
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_volume_per_unit_required
                ),
                'numeric'
            ],
            'unit_uom' => $license->state->regulator === State::REGULATOR_METRC ? [
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_weight_per_unit_required || $inventoryCategory->is_volume_per_unit_required
                ),
                'string',
                ModelAttributeRule::in(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    function($inventoryCategory) {
                        if ($inventoryCategory->is_weight_per_unit_required) {
                            return Rule::in(InventoryCategory::getWeightBasedUnitList());
                        }

                        if ($inventoryCategory->is_volume_per_unit_required) {
                            return Rule::in(InventoryCategory::getVolumeBasedUnitList());
                        }
                    }
                ),
            ] : [],
            'license_id' => [],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id');
        $license = License::findOrFail($licenseId);

        return [
            'name' => [
                'sometimes',
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\InventoryType,name,'.$id.',id,license_id,'.$licenseId,
            ],
            'category_id' => [
                'sometimes',
                'required',
                'uuid',
                "exists:App\Models\InventoryCategory,id,regulator,\"{$license->state->regulator}\""
            ],
            'uom' => $license->state->regulator === State::REGULATOR_LEAF ?
                ['sometimes', 'required', 'string'] :
                [
                    'sometimes',
                    'required',
                    'string',
                    ModelAttributeRule::in(
                        '\App\Models\InventoryCategory',
                        $this->request->get('category_id'),
                        function($inventoryCategory) {
                            switch ($inventoryCategory->quantity_type) {
                                case InventoryCategory::COUNT_BASED:
                                    return Rule::in(InventoryCategory::getCountBasedUnitList());
                                case InventoryCategory::WEIGHT_BASED:
                                    return Rule::in(InventoryCategory::getWeightBasedUnitList());
                                case InventoryCategory::VOLUME_BASED:
                                    return Rule::in(InventoryCategory::getVolumeBasedUnitList());
                                default:
                                    return '';
                            }
                        }
                    ),
                ],
            'strain_id' => [
                'sometimes',
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_strain_required
                ),
                'uuid'
            ],
            'serving_num' => [
                'sometimes',
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_serving_required
                ),
                'integer'
            ],
            'serving_size' => [
                'sometimes',
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_serving_required
                ),
                'integer'
            ],
            'weight' => [
                'sometimes',
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_weight_per_unit_required
                ),
                'numeric'
            ],
            'volume' => [
                'sometimes',
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_volume_per_unit_required
                ),
                'numeric'
            ],
            'unit_uom' => $license->state->regulator === State::REGULATOR_METRC ? [
                'sometimes',
                ModelAttributeRule::required(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    fn($inventoryCategory) => $inventoryCategory->is_weight_per_unit_required || $inventoryCategory->is_volume_per_unit_required
                ),
                'string',
                ModelAttributeRule::in(
                    '\App\Models\InventoryCategory',
                    $this->request->get('category_id'),
                    function($inventoryCategory) {
                        if ($inventoryCategory->is_weight_per_unit_required) {
                            return Rule::in(InventoryCategory::getWeightBasedUnitList());
                        }

                        if ($inventoryCategory->is_volume_per_unit_required) {
                            return Rule::in(InventoryCategory::getVolumeBasedUnitList());
                        }
                    }
                ),
            ] : [],
            'status' => ['sometimes', 'required', 'boolean'],
        ];
    }

    public function index() : JsonResponse
    {
        return response()->json(
            $this->service->index(),
            Response::HTTP_OK
        );
    }

}
