<?php

use App\Models\TraceableModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SampleSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        TraceableModel::withoutTraceableModelEvents(function () {
            $this->call(LicenseTableSeeder::class);
            $this->call(LicenseeTableSeeder::class);
            $this->call(RoomTableSeeder::class);
            $this->call(SubroomTableSeeder::class);
            $this->call(StrainTableSeeder::class);
            $this->call(PropagationTableSeeder::class);
            $this->call(GrowCycleTableSeeder::class);
            $this->call(PlantTableSeeder::class);
            $this->call(AdditiveSeeder::class);
            $this->call(SupplierTableSeeder::class);
            $this->call(AdditiveInventorySeeder::class);
            $this->call(DisposalSeeder::class);
            $this->call(BatchTableSeeder::class);
            $this->call(InventoryUnLottedSeeder::class);
            $this->call(HarvestTableSeeder::class);
            $this->call(SourceCategorySeeder::class);
            $this->call(SourceProductSeeder::class);
            $this->call(InventoryLottedSeeder::class);
            $this->call(ConversionTableSeeder::class);
            $this->call(ManifestSeeder::class);
        });

        Model::reguard();
    }
}
