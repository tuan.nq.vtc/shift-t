<?php

namespace App\Events\Message;

use App\Events\Event;
use App\Models\Message;

class MessageUpdated extends Event
{
    public Message $message;
    public string            $action;

    /**
     * MessageUpdated constructor.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->action = Message::ACTION_UPDATE;
    }
}
