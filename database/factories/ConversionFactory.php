<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Conversion};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
|   Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Conversion::class,
    function (Faker $faker) {
        return [
            'license_id' => $faker->uuid,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Conversion::class,
                $data['license_id']
            ),
            'name' => $faker->word,
            'output_room_id' => $faker->uuid,
            'waste_quantity' => rand(1, 10),
            'uom' => 'g',
            'waste_room_id' => $faker->uuid,
            'output_product_id' => $faker->uuid,
            'output_product_type' => \App\Models\EndProduct::class,
            'output_state_category_id' => $faker->uuid,
            'output_quantity' => rand(1, 10),
            'is_altered' => Conversion::IS_NOT_ALTERED,
            'status' => Conversion::STATUS_OPEN,
        ];
    }
);
