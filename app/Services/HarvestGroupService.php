<?php

namespace App\Services;

use App\Events\Disposal\DisposalScheduled;
use App\Events\Harvest\HarvestFinalized;
use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{Disposal,
    GrowCycle,
    Harvest,
    HarvestGroup,
    HarvestPlant,
    Inventory,
    InventoryType,
    License,
    Plant,
    Room,
    SeedToSale,
    State,
    StateCategory,
    Strain
};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection, ModelNotFoundException};
use Illuminate\Support\{Arr, Carbon, Enumerable, Facades\DB, Facades\Event};
use Throwable;

/**
 * Class HarvestGroupService
 *
 * @package App\Services
 *
 * @property HarvestGroup|Builder $model
 *
 * @method HarvestGroup|Builder getModel()
 */
class HarvestGroupService extends BaseService
{
    protected array $relations = [
        'room:id,name',
        'harvests.strain:id,name',
        'harvests.plants',
    ];

    public function __construct(HarvestGroup $harvestGroup)
    {
        parent::__construct($harvestGroup);
    }

    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $paginator = parent::list($criteria);
        $paginator->getCollection()->transform(fn($item) => $this->transform($item))->makeHidden('harvests');

        return $paginator;
    }

    /**
     * @param HarvestGroup $harvestGroup
     * @return HarvestGroup
     */
    private function transform(HarvestGroup $harvestGroup)
    {
        $strains = [];
        $numberOfPlants = 0;
        foreach ($harvestGroup->harvests as $harvest) {
            if ($harvest->strain) {
                $strains[] = $harvest->strain->name;
            }
            $numberOfPlants += $harvest->harvestPlants->count();
        }
        !empty($harvestGroup->source_rooms) && $harvestGroup->source_rooms = Room::query()
            ->whereIn('id', $harvestGroup->source_rooms)
            ->get(['id', 'name']);
        $harvestGroup->strains = $strains;
        $harvestGroup->number_of_plants = $numberOfPlants;

        return $harvestGroup;
    }

    /**
     * @param string $id
     * @param bool $withRelation
     *
     * @return HarvestGroup
     *
     * @throws ModelNotFoundException
     */
    public function get(string $id, $withRelation = true): HarvestGroup
    {
        return $this->withDetail($this->newQuery()->findOrFail($id));
    }

    /**
     * @param HarvestGroup $harvestGroup
     * @return HarvestGroup
     */
    private function withDetail(HarvestGroup $harvestGroup): HarvestGroup
    {
        $harvestGroup->load(array_merge($this->relations, ['harvests.wetWeights']));

        foreach ($harvestGroup->harvests as $harvest) {
            $harvest->number_of_plants = $harvest->harvestPlants->count();
        }
        return $harvestGroup;
    }

    /**
     * @param $id
     * @param array $data
     *
     * @return HarvestGroup
     * @throws GeneralException
     */
    public function update($id, array $data): HarvestGroup
    {
        /** @var HarvestGroup $harvestGroup */
        $harvestGroup = $this->newQuery()->findOrFail($id);
        if ($harvestGroup->status !== HarvestGroup::STATUS_OPEN) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'open']));
        }
        if (!$harvestGroup->update($data)) {
            throw new GeneralException(__('exceptions.actions.update_failed'));
        }

        return $this->withDetail($harvestGroup);
    }

    /**
     * @param HarvestGroup $harvestGroup
     * @param Collection | Enumerable $plants
     * @return HarvestGroup
     * @throws GeneralException
     */
    public function harvestPlants(HarvestGroup $harvestGroup, $plants): HarvestGroup
    {
        if ($harvestGroup->status !== HarvestGroup::STATUS_OPEN) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'open']));
        }
        $sourceRoom = [];
        $strainHarvest = [];
        $harvestedPlantIds = [];
        $growCycleIds = [];
        foreach ($plants as $plant) {
            if (!in_array(
                $plant->grow_status,
                [SeedToSale::GROW_STATUS_VEGETATIVE, SeedToSale::GROW_STATUS_FLOWERING]
            )) {
                continue;
            }
            !in_array($plant->room_id, $sourceRoom) && $sourceRoom[] = $plant->room_id;
            if (!isset($strainHarvest[$plant->strain_id])) {
                $harvest = $harvestGroup->harvests()->firstWhere('strain_id', $plant->strain_id);
                if (!$harvest) {
                    $harvest = $harvestGroup->harvests()->create(
                        array_merge(
                            $harvestGroup->only(['license_id', 'room_id', 'start_at']),
                            ['strain_id' => $plant->strain_id]
                        )
                    );
                }
                $strainHarvest[$plant->strain_id] = $harvest;
            } else {
                $harvest = $strainHarvest[$plant->strain_id];
            }
            $harvest->harvestPlants()->create(['plant_id' => $plant->id]);

            $harvestedPlantIds[] = $plant->id;
            if ($plant->growCycle && in_array(
                    $plant->growCycle->grow_status,
                    [SeedToSale::GROW_STATUS_VEGETATIVE, SeedToSale::GROW_STATUS_FLOWERING]
                ) && !in_array($plant->grow_cycle_id, $growCycleIds)) {
                $growCycleIds[] = $plant->grow_cycle_id;
            }
        }

        if (empty($strainHarvest)) {
            throw new GeneralException(__('exceptions.harvest.no_harvested_plants'));
        }
        !empty($sourceRoom) && $harvestGroup->update(
            ['source_rooms' => array_unique(array_merge($harvestGroup->source_rooms ?? [], $sourceRoom))]
        );
        // update grow status to `harvest in progress`
        if (!Plant::whereIn('id', $harvestedPlantIds)
            ->update(['grow_status' => SeedToSale::GROW_STATUS_HARVESTING])) {
            throw new GeneralException(__('exceptions.harvest.harvest_plants.failed_update_plant_grow_status'));
        }
        // update grow cycle to `harvest in progress`
        if (!empty($growCycleIds) && !GrowCycle::whereIn('id', $growCycleIds)
                ->update(['grow_status' => SeedToSale::GROW_STATUS_HARVESTING])) {
            throw new GeneralException(__('exceptions.harvest.harvest_plants.failed_update_grow_cycle_grow_status'));
        }

        return $this->withDetail($harvestGroup);
    }

    /**
     * @param string $id
     * @param Collection | Enumerable $plants
     * @return HarvestGroup
     * @throws GeneralException
     * @throws Throwable
     */
    public function removePlants(string $id, $plants): HarvestGroup
    {
        /** @var HarvestGroup $harvestGroup */
        $harvestGroup = $this->newQuery()->findOrFail($id);
        if ($harvestGroup->status !== HarvestGroup::STATUS_OPEN) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'open']));
        }
        $harvestIds = $harvestGroup->harvests()->pluck('id')->toArray();

        try {
            DB::beginTransaction();
            $plantIds = $plants->pluck('id')->all();
            $deletedCount = HarvestPlant::whereIn('harvest_id', $harvestIds)
                ->whereIn('plant_id', $plantIds)->delete();
            if ($deletedCount === 0) {
                throw new GeneralException(__('exceptions.harvest.no_deleted_plants'));
            }

            // update grow status to `flowering`
            if (!Plant::whereIn('id', $plantIds)->update(['grow_status' => SeedToSale::GROW_STATUS_FLOWERING])) {
                throw new GeneralException(__('exceptions.harvest.harvest_plants.failed_update_plant_grow_status'));
            }

            // update grow cycle to `flowering`
            $growCycleIds = $plants->whereNotNull('grow_cycle_id')->pluck('grow_cycle_id')->all();
            if (!empty($growCycleIds) && !GrowCycle::whereIn('id', array_unique($growCycleIds))->update(
                    ['grow_status' => SeedToSale::GROW_STATUS_FLOWERING]
                )) {
                throw new GeneralException(
                    __('exceptions.harvest.harvest_plants.failed_update_grow_cycle_grow_status')
                );
            }
            $sourceRoom = [];
            foreach ($harvestGroup->fresh()->harvests as $harvest) {
                $harvestPlants = $harvest->harvestPlants;
                if ($harvestPlants->count() === 0) {
                    $harvest->delete();
                } else {
                    $sourceRoom = array_unique(
                        array_merge($sourceRoom, $harvest->plants()->pluck('room_id')->unique()->all())
                    );
                }
            }
            $harvestGroup->update(['source_rooms' => $sourceRoom]);
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return $this->withDetail($harvestGroup);
    }

    /**
     * @param HarvestGroup $harvestGroup
     * @param array $confirmData
     * @param string|null $userId
     * @return HarvestGroup
     * @throws GeneralException
     * @throws Throwable
     */
    public function finalize(HarvestGroup $harvestGroup, array $confirmData, ?string $userId): HarvestGroup
    {
        $finishedAt = Carbon::now();
        $harvests = $harvestGroup->harvests->fresh();
        try {
            DB::beginTransaction();
            foreach ($confirmData as $item) {
                if ($strainId = Arr::get($item, 'strain_id')) {
                    unset($item['strain_id']);
                    /** @var Harvest $harvest */
                    if ($harvest = $harvests->firstWhere('strain_id', $strainId)) {
                        $harvest->update(array_merge($item, ['finished_at' => $finishedAt,]));
                        $license = $harvest->license;
                        $materialCategoryCode = $flowerCategoryCode = '';
                        if ($license->isRegulator(State::REGULATOR_LEAF)) {
                            $flowerCategoryCode = 'flower';
                            $materialCategoryCode = 'other_material';
                        } elseif ($license->isRegulator(State::REGULATOR_METRC)) {
                            $flowerCategoryCode = 'Shake'; // CO state #todo temporary hardcode
                            $materialCategoryCode = 'Kief';
                        } elseif ($license->isRegulator(State::REGULATOR_CCRS)) {
                            $flowerCategoryCode = 'Flower Unlotted';
                            $materialCategoryCode = 'Other Material Unlotted';
                        }
                        if ($harvest->group->type === HarvestGroup::TYPE_EXTRACTION) {
                            $flowerWeight = $harvest->flower_wet_weight;
                            $materialWeight = $harvest->material_wet_weight;
                        } else {
                            $flowerWeight = $harvest->flower_dry_weight;
                            $materialWeight = $harvest->material_dry_weight;
                        }

                        if ($flowerWeight > 0) {
                            if (!$harvest->flower_room_id) {
                                throw new GeneralException('Inventory Room not found');
                            }
                            $inventoryType = $this->getInventoryType($flowerCategoryCode, $license, $harvest->strain);
                            $harvest->flowerInventoryType()->associate($inventoryType);
                            $this->createInventory(
                                $harvest,
                                $harvest->strain,
                                $harvest->flowerRoom,
                                $inventoryType,
                                $flowerWeight
                            );
                        }
                        if ($materialWeight > 0) {
                            if (!$harvest->material_room_id) {
                                throw new GeneralException('Inventory Room not found');
                            }
                            $inventoryType = $this->getInventoryType($materialCategoryCode, $license, $harvest->strain);
                            $harvest->materialInventoryType()->associate($inventoryType);
                            $this->createInventory(
                                $harvest,
                                $harvest->strain,
                                $harvest->materialRoom,
                                $inventoryType,
                                $materialWeight
                            );
                        }
                        $harvest->save();
                        $totalWaste = $harvest->waste_wet_weight + $harvest->waste_dry_weight;
                        if ($totalWaste > 0) {
                            $this->createDisposal($harvest, $totalWaste, $userId);
                        }

                        // dispatch harvest finished
                        Event::dispatch(new HarvestFinalized($harvest));
                    }
                }
            }
            if ($harvestGroup->harvests()->whereNull('finished_at')->count() === 0) {
                $harvestGroup->update(['status' => HarvestGroup::STATUS_CLOSED, 'finished_at' => $finishedAt,]);
            }
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return $this->withDetail($harvestGroup);
    }

    /**
     * @param Harvest $harvest
     * @param float $totalWaste
     * @param string|null $scheduledById
     */
    private function createDisposal(Harvest $harvest, float $totalWaste, ?string $scheduledById): void
    {
        $quarantineEnd = $harvest->finished_at->clone()->addHours(Disposal::DEFAULT_EXTENDED_QUARANTINE_HOUR);
        $status = $quarantineEnd->lt(Carbon::now()) ? Disposal::STATUS_READY : Disposal::STATUS_SCHEDULED;
        /** @var Disposal $disposal */
        $disposal = $harvest->disposals()->create(
            [
                'license_id' => $harvest->license_id,
                'status' => $status,
                'uom' => Disposal::UOM_GRAM,
                'quarantine_start' => $harvest->finished_at,
                'quarantine_end' => $quarantineEnd,
                'room_id' => $harvest->waste_room_id,
                'reason' => Disposal::REASON_WASTE,
                'quantity' => $totalWaste,
                'strain_id' => $harvest->strain_id,
                'scheduled_by_id' => $scheduledById,
            ]
        );

        // dispatch disposal scheduled
        Event::dispatch(new DisposalScheduled($disposal));
    }

    /**
     * @param HarvestGroup $harvestGroup
     * @throws GeneralException
     */
    public function updateGrowStatus(HarvestGroup $harvestGroup): void
    {
        $plantIds = [];
        foreach ($harvestGroup->harvests as $harvest) {
            $plantIds = array_merge($plantIds, $harvest->harvestPlants->pluck('plant_id')->all());
        }

        // remove grow cycle has no plants
        $growCycleIds = Plant::query()->whereIn('id', $plantIds)
            ->whereNotNull('grow_cycle_id')
            ->pluck('grow_cycle_id')->unique()->all();
        // update plant's grow status to `harvested` and remove plants out of grow cycle
        $isUpdated = Plant::whereIn('id', $plantIds)->update(
            [
                'grow_status' => SeedToSale::GROW_STATUS_HARVESTED,
                'harvested_at' => $harvestGroup->start_at,
                'grow_cycle_id' => null,
            ]
        );
        if (!$isUpdated) {
            throw new GeneralException(
                __('exceptions.harvest.harvest_plants.failed_update_plant_grow_status')
            );
        }
        if (!empty($growCycleIds)) {
            $deletableGrowCycles = GrowCycle::withCount(['plants'])->having('plants_count', 0)
                ->whereIn('id', $growCycleIds)->get();
            foreach ($deletableGrowCycles as $deletableGrowCycle) {
                $deletableGrowCycle->delete();
            }
        }
    }

    /**
     * @param Harvest $harvest
     * @param Strain $strain
     * @param Room $room
     * @param InventoryType $inventoryType
     * @param float $weight
     */
    private function createInventory(Harvest $harvest, Strain $strain, Room $room, InventoryType $inventoryType, float $weight): void
    {
        $license = $harvest->license;
        $harvest->inventories()->createMany(
            [
                [
                    'license_id' => $license->id,
                    'room_id' => $room->id,
                    'type_id' => $inventoryType->id,
                    'state_category_id' => optional($inventoryType->stateCategory)->id,
                    'qty_on_hand' => $weight,
                    'strain_id' => $strain->id,
                    'uom' => Inventory::UOM_GRAMS,
                    'is_lotted' => false,
                    'for_extraction' => $harvest->group->type === HarvestGroup::TYPE_EXTRACTION,
                ],
            ]
        );
    }

    /**
     * @param string $categoryCode
     * @param License $license
     * @param Strain $strain
     * @return InventoryType
     * @throws GeneralException
     */
    private function getInventoryType(string $categoryCode, License $license, Strain $strain): InventoryType
    {
        if (!$categoryCode || !($category = StateCategory::where(
                ['regulator' => optional($license->state)->regulator, 'code' => $categoryCode]
            )->first())) {
            throw new GeneralException('Invalid category code');
        }

        return InventoryType::query()->updateOrCreate(
            [
                'license_id' => $license->id,
                'state_category_id' => $category->id,
                'name' => "{$strain->name} Bulk {$category->name}",
            ],
            ['strain_id' => $strain->id, 'uom' => $category->uom]
        );
    }
}
