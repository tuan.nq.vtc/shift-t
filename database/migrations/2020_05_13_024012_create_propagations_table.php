<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropagationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propagations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->string('name')->index()->nullable();
            $table->string('mother_code')->nullable();
            $table->uuid('strain_id')->nullable()->index();
            $table->uuid('room_id')->nullable()->index();
            $table->uuid('subroom_id')->nullable()->index();
            $table->string('source_type')->index();

            $table->integer('qty_allocated')->default(0);
            $table->integer('quantity')->default(0);
            $table->date('date');

            $table->uuid('cut_by_id')->nullable()->index();
            $table->uuid('plugged_by_id')->nullable()->index();
            $table->boolean('destroyed')->default(false);
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();

            $table->timestamps();
            $table->softDeletes();

            // Foreign keys
            $table->foreign('strain_id')->references('id')->on('strains')->nullOnDelete();
            $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
            $table->foreign('subroom_id')->references('id')->on('subrooms')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propagations');
    }
}
