<?php

namespace App\Synchronizations\Http\QueryIterators;

use App\Models\Propagation;
use App\Models\Trace;
use Illuminate\Support\Carbon;

class MetrcQueryIterator extends BaseQueryIterator
{
    public const MAX_NUMBER_OF_PAGE = 1000;

    private int $page = 1;

    private ?Carbon $date = null;

    private ?Carbon $startDate = null;

    private ?Carbon $endDate = null;

    public function __construct(Trace $trace)
    {
        parent::__construct($trace);

        if (in_array($trace->resource_type, [Propagation::class])) {
            $this->initDateRange($trace);
        }
    }

    private function initDateRange(Trace $trace): void
    {
        if (empty($trace->options)) {
            return;
        }
        $syncDateRange = $trace->options['sync_date_range'];
        $availableDateRanges = config('licenses.date_ranges');
        // Default sync date range is 6 months
        $dateRange = empty($availableDateRanges[$syncDateRange]) ? '-6 months' : '-' . $availableDateRanges[$syncDateRange];
        $this->endDate = $trace->created_at;
        $this->startDate = $this->endDate->copy()->modify($dateRange);
        $this->date = $this->startDate->copy();
    }

    public function next(): void
    {
        if ($this->hasMore()) {
            $this->page++;
            if ($this->date) {
                $this->date->addDay();
            }
        }
    }

    public function hasMore(): bool
    {
        if ($this->page === 1) {
            return true;
        }
        return $this->date && $this->date->between($this->startDate, $this->endDate)
            && $this->page <= self::MAX_NUMBER_OF_PAGE;
    }

    public function current(): array
    {
        if ($this->date) {
            $this->httpResource->addQuery('lastModifiedStart', $this->date->format('Y-m-d'));
            $this->httpResource->addQuery('lastModifiedEnd', $this->date->copy()->addDay()->format('Y-m-d'));
        }
        return $this->httpClient->doRequest($this->httpResource);
    }
}
