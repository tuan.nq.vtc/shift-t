<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch,
    Inventory,
    License,
    Plant,
    PlantGroup,
    Propagation,
    Room,
    RoomType,
    SeedToSale,
    Strain,
    Trace,
    TraceableModel};
use App\Synchronizations\Constants\LeafConstant;
use App\Synchronizations\Exceptions\SyncException;
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;

/**
 * Class PlantGroupTest
 * @package Tests\Synchronizer
 */
class PlantGroupTest extends TestBase
{
    /** @test */
    public function it_can_push_move_to_vegetation_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;

        $expectedNumberOfPlants = rand(1, 10);
        $propagation = factory(Propagation::class)->create(['license_id' => $licenseId]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId]);
        $batch->source()->associate($propagation)->save();
        $inventory = factory(Inventory::class)->create(['license_id' => $licenseId]);
        $inventory->source()->associate($propagation)->save();
        $plantGroup = factory(PlantGroup::class)->create(
            ['license_id' => $licenseId, 'propagation_id' => $propagation->id]
        );

        factory(Plant::class, $expectedNumberOfPlants)->create(
            [
                'license_id' => $licenseId,
                'plant_group_id' => $plantGroup->id,
                'sync_code' => null,
                'sync_status' => TraceableModel::SYNC_STATUS_PENDING
            ]
        );

        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => PlantGroup::SYNC_ACTION_MOVE_TO_VEGETATION,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $plantGroup->id,
                'resource_type' => get_classname($plantGroup),
                'resource_data' => $plantGroup->attributesToArray(),
                'resource_changes' => $plantGroup->getChanges(),
            ]
        );

        Http::fake(
            [
                '/inventory_types*' => Http::response(
                    [
                        "total" => 1,
                        "data" => [
                            [
                                "name" => "Charlotte's Web Eighths",
                                "global_inventory_type_id" => "WAM200002.TYIGQ",
                                "global_id" => "WAM200002.IN7DNC",
                                "type" => "immature_plant",
                                "intermediate_type" => "seed",
                            ]
                        ]
                    ],
                    200
                ),
            ]
        );

        Http::fake(
            [
                '/inventories*' => Http::response(
                    [
                        "total" => 1,
                        "data" => [
                            [
                                "global_inventory_type_id" => "WAM200002.TYIGQ",
                                "global_id" => "WAM200002.IN7DNC",
                                "qty" => "140.0000",
                            ]
                        ]
                    ],
                    200
                ),
            ]
        );
        $fakeResponseData = [];
        $batchCode = $this->generateGlobalId("WAG010101.BA"); // WAG010101.BA7D9O;
        for ($i = 0; $i < $expectedNumberOfPlants; $i++) {
            $fakeResponseData[] = [
                'global_id' => $this->generateGlobalId("WAG010101.PL"),
                'global_batch_id' => $batchCode,
            ];
        }

        Http::fake(['/move_inventory_to_plants' => Http::response($fakeResponseData, 200),]);

        $result = app(Synchronizer::class)->push($trace);
        /** @var PlantGroup $plantGroupResult */
        $plantGroupResult = $result->resource;
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($batchCode, $plantGroupResult->batch->sync_code);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $plantGroupResult->sync_status);

        $this->assertCount(
            $expectedNumberOfPlants,
            $plantGroupResult->plants
                ->where('sync_status', TraceableModel::SYNC_STATUS_SYNCED)
                ->whereNotNull('sync_code')
        );
        foreach ($fakeResponseData as $data) {
            $this->assertNotEmpty(
                $plantGroupResult->plants->where('sync_status', TraceableModel::SYNC_STATUS_SYNCED)->where(
                    'sync_code',
                    $data['global_id']
                )
            );
        }
    }

    /** @test */
    public function it_failed_push_move_to_vegetation_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;

        $expectedNumberOfPlants = rand(1, 10);
        $propagation = factory(Propagation::class)->create(['license_id' => $licenseId]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId]);
        $batch->source()->associate($propagation)->save();
        $inventory = factory(Inventory::class)->create(['license_id' => $licenseId]);
        $inventory->source()->associate($propagation)->save();
        $plantGroup = factory(PlantGroup::class)->create(
            ['license_id' => $licenseId, 'propagation_id' => $propagation->id]
        );

        factory(Plant::class, $expectedNumberOfPlants)->create(
            [
                'license_id' => $licenseId,
                'plant_group_id' => $plantGroup->id,
                'sync_code' => null,
                'sync_status' => TraceableModel::SYNC_STATUS_PENDING
            ]
        );

        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => PlantGroup::SYNC_ACTION_MOVE_TO_VEGETATION,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $plantGroup->id,
                'resource_type' => get_classname($plantGroup),
                'resource_data' => $plantGroup->attributesToArray(),
                'resource_changes' => $plantGroup->getChanges(),
            ]
        );

        Http::fake(['/inventory_types*' => Http::response([], 401),]);
        Http::fake(['/inventories*' => Http::response([], 401),]);

        $fakeResponseData = [];
        $batchCode = $this->generateGlobalId("WAG010101.BA"); // WAG010101.BA7D9O;
        for ($i = 0; $i < $expectedNumberOfPlants; $i++) {
            $fakeResponseData[] = [
                'global_id' => $this->generateGlobalId("WAG010101.PL"),
                'global_batch_id' => $batchCode,
            ];
        }

        Http::fake(['/move_inventory_to_plants' => Http::response([], 401),]);

        $this->expectException(SyncException::class);
        $this->expectExceptionMessage(SyncException::buildResourceFailed()->getMessage());
        app(Synchronizer::class)->push($trace);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );
        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => PlantGroup::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ],
            ]
        );
        $strain = factory(Strain::class)->create(['license_id' => $license->id,]);
        $room = factory(Room::class)->create(['license_id' => $license->id,]);
        $data = [];
        $totalRecord = 8;
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = $this->generateGlobalId("WAG010101.BA");
            $data[] = [
                "type" => "plant",
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
                "global_strain_id" => $strain->sync_code,
                "global_area_id" => $room->sync_code,
                "origin" => $this->faker->randomElement(SeedToSale::SOURCE_TYPES),
                "status" => "open",
                "quantity" => rand(5, 20),
                "batch_created_at" => $this->faker->dateTimeThisYear()->format('Y-m-d H:i:s'),
                "global_mother_plant_id" => null
            ];
        }
        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
            'f_type' => 'plant',
            'f_status' => 'open',
            'f_planted_at1' => $fromDate->format('m/d/Y'),
            'f_planted_at2' => $toDate->format('m/d/Y'),
        ];
        Http::fake(
            [
                sprintf("/batches?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response(
                    $responseData,
                    200
                ),
            ]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Batch::count());
        $this->assertEquals($totalRecord, PlantGroup::count());
    }

    /** @test */
    public function it_can_push_move_to_room_to_leaf()
    {
        $this->factoryMoveToRoom($trace, $plantGroup, PlantGroup::SYNC_ACTION_MOVE_TO_ROOM);
        Http::fake(
            [
                '/batches/update' => Http::response(
                    [
                        'batch' => [
                            'global_id' => $this->generateGlobalId("WAG010101.PL"),
                            'origin' => $plantGroup->propagation->source_type,
                            'global_area_id' => $plantGroup->room->sync_code,
                            'global_strain_id' => $plantGroup->strain->sync_code,
                            'num_plants' => $plantGroup->propagation->quantity,
                            'type' => LeafConstant::BATCH_TYPE_PLANT
                        ]
                    ],
                    200
                ),
            ]
        );
        $result = app(Synchronizer::class)->push($trace);
        /** @var PlantGroup $plantGroupResult */
        $plantGroupResult = $result->resource;
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $plantGroupResult->sync_status);
    }

    /** @test */
    public function it_failed_push_move_to_room_to_leaf()
    {
        $this->factoryMoveToRoom($trace, $plantGroup, PlantGroup::SYNC_ACTION_MOVE_TO_ROOM);
        Http::fake(['/batches/update' => Http::response([], 401),]);

        $this->expectException(SyncException::class);
        $this->expectExceptionMessage(SyncException::sendFailed()->getMessage());
        app(Synchronizer::class)->push($trace);
    }

    /** @test */
    public function it_can_push_create_plant_group_to_leaf()
    {
        $this->factoryMoveToRoom($trace, $plantGroup, PlantGroup::SYNC_ACTION_CREATE);
        Http::fake(
            [
                '/batches' => Http::response(
                    [
                        [
                            'global_id' => $plantGroup->batch->sync_code,
                            'origin' => $plantGroup->propagation->source_type,
                            'global_area_id' => $plantGroup->room->sync_code,
                            'global_strain_id' => $plantGroup->strain->sync_code,
                            'num_plants' => $plantGroup->propagation->quantity,
                            'external_id' => $plantGroup->id,
                            'type' => LeafConstant::BATCH_TYPE_PLANT
                        ]
                    ],
                    200
                )
            ]
        );
        $result = app(Synchronizer::class)->push($trace);
        /** @var PlantGroup $plantGroupResult */
        $plantGroupResult = $result->resource;
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $plantGroupResult->sync_status);
    }

    /** @test */
    public function it_failed_push_create_plant_group_to_leaf()
    {
        $this->factoryMoveToRoom($trace, $plantGroup, PlantGroup::SYNC_ACTION_CREATE);
        Http::fake(['/batches' => Http::response([], 401),]);

        $this->expectException(SyncException::class);
        $this->expectExceptionMessage(SyncException::sendFailed()->getMessage());
        app(Synchronizer::class)->push($trace);
    }

    private function factoryMoveToRoom(&$trace, &$plantGroup, $action)
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;

        $propagation = factory(Propagation::class)->create(['license_id' => $licenseId]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId]);
        $batch->source()->associate($propagation)->save();
        if ($action === PlantGroup::SYNC_ACTION_CREATE) {
            $batch->update(['sync_code' => $this->generateGlobalId("WAG010101.PL")]);
        }
        $strain = factory(Strain::class)->create(['license_id' => $licenseId]);
        $roomType = factory(RoomType::class)->create();
        $room = factory(Room::class)->create(['license_id' => $licenseId]);
        $room->roomType()->associate($roomType)->save();
        /** @var PlantGroup $plantGroup */
        $plantGroup = factory(PlantGroup::class)->create(
            [
                'license_id' => $licenseId,
                'propagation_id' => $propagation->id,
                'room_id' => $room->id,
                'strain_id' => $strain->id
            ]
        );
        $batch->source_id = $plantGroup->id;
        $batch->source_type = PlantGroup::class;
        $batch->save();

        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => $action,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $plantGroup->id,
                'resource_type' => get_classname($plantGroup),
                'resource_data' => $plantGroup->attributesToArray(),
                'resource_changes' => $plantGroup->getChanges(),
            ]
        );
    }
}
