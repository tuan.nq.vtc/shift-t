<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait UomFilterable
{
    public function scopeUom(Builder $query, $value)
    {
        return $query->where('uom', $value);
    }

    public function scopeOrderByUom(Builder $query, $direction): Builder
    {
        return $query->orderBy($this->getTable() . '.uom', $direction);
    }
}
