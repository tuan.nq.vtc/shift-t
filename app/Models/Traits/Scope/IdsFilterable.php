<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait IdsFilterable
{
    /**
     * @param Builder $query
     * @param mixed ...$ids
     * @return Builder
     */
    public function scopeIds(Builder $query, ...$ids)
    {
        return $query->whereIn('id', $ids);
    }
}
