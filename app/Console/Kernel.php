<?php

namespace App\Console;

use App\Console\Commands\CheckScheduledForDestructionCommand;
use App\Console\Commands\GenerateMultipleImageSizes;
use App\Console\Commands\LicenseResource\CleanCommand;
use App\Console\Commands\GenerateRoomStatistic;
use App\Console\Commands\TraceableResource\{FileGeneratorCommand, FileUploaderCommand, RemoveSyncedCommand,
    SyncCommand,
    SyncLabTestCommand,
    SyncQASampleResultCommand};
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Laravelista\LumenVendorPublish\VendorPublishCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CheckScheduledForDestructionCommand::class,
        CleanCommand::class,
        RemoveSyncedCommand::class,
        SyncCommand::class,
        SyncLabTestCommand::class,
        VendorPublishCommand::class,
        GenerateRoomStatistic::class,
        SyncQASampleResultCommand::class,
        GenerateMultipleImageSizes::class,
        FileGeneratorCommand::class,
        FileUploaderCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('trace:scheduled-disposal')->daily()->timezone(config('app.timezone'));
        $schedule->command('qa-sample:sync')->everyThirtyMinutes()->timezone(config('app.timezone'));
    }
}
