<?php

namespace App\Services;

use App\Models\Vehicle;
use Illuminate\Support\Collection;

/**
 * Class VehicleService
 * @package App\Services
 */
class VehicleService extends BaseService
{
    public function __construct(Vehicle $vehicle)
    {
        parent::__construct($vehicle);
    }

    /**
     * @return Collection
     */
    public function getAvailableItems(): Collection
    {
        return $this->newQuery()->where('status', Vehicle::STATUS_ENABLED)->get();
    }
}
