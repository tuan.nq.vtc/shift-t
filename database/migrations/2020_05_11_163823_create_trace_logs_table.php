<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraceLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'trace_logs',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('trace_id')->index();
                $table->string('http_method')->nullable();
                $table->string('request_url')->nullable();
                $table->json('request_query')->nullable();
                $table->json('request_body')->nullable();
                $table->json('response_data')->nullable();
                $table->unsignedSmallInteger('status')->default(0);
                $table->longText('exception')->nullable();
                $table->timestamp('sent_at', 0)->nullable();

                $table->foreign('trace_id')->on('traces')->references('id')->cascadeOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trace_logs');
    }
}
