<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Services\PrintLabelTemplateService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class PrintLabelTemplateController
 * @package App\Http\Controllers
 */
class PrintLabelTemplateController extends CRUDController
{
    public function __construct(PrintLabelTemplateService $service, Request $request)
    {
        $request->merge(
            [
                'filter' => array_merge(
                    $request->get('filter', []),
                    $request->only(['account_holder_id'])
                )
            ]
        );
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'account_holder_id' => ['required', 'uuid'],
            'name' => ['required', 'string', 'max:150'],
            'background' => ['nullable', 'string'],
            'box.width' => ['required', 'numeric', 'min:0.01', 'max:10'],
            'box.height' => ['required', 'numeric', 'min:0.01', 'max:10'],
            'box.top' => ['required', 'numeric', 'min:0', 'max:10'],
            'box.right' => ['required', 'numeric', 'min:0', 'max:10'],
            'box.bottom' => ['required', 'numeric', 'min:0', 'max:10'],
            'box.left' => ['required', 'numeric', 'min:0', 'max:10'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'max:150'],
            'background' => ['sometimes', 'nullable', 'string'],
            'box.width' => ['sometimes', 'required', 'numeric', 'min:0.01', 'max:10'],
            'box.height' => ['sometimes', 'required', 'numeric', 'min:0.01', 'max:10'],
            'box.top' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'box.right' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'box.bottom' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'box.left' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'status' => ['sometimes', 'required'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems(Criteria::createFromRequest($this->request)));
    }
}
