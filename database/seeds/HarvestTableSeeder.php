<?php

use App\Models\{Harvest,
    HarvestGroup,
    HarvestPlant,
    HarvestWetWeight,
    License,
    Plant,
    Room,
    RoomType,
    SeedToSale,
    Strain};
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

/**
 * Class HarvestTableSeeder.
 */
class HarvestTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run(): void
    {
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
    }

    protected function iterateLicense($license): void
    {
        $rooms = Room::licenseId($license->id)
            ->whereHas('roomType', fn($query) => $query->where('category', RoomType::CATEGORY_PLANT))
            ->get();
        $harvestRooms = Room::licenseId($license->id)
            ->whereHas('roomType', fn($query) => $query->where('category', RoomType::CATEGORY_HARVEST))
            ->get();
        $disposalRooms = Room::licenseId($license->id)
            ->whereHas('roomType', fn($query) => $query->where('category', RoomType::CATEGORY_DISPOSAL))
            ->get();
        if ($rooms->isEmpty() || $harvestRooms->isEmpty() || $disposalRooms->isEmpty()) {
            return;
        }
        $strains = Strain::licenseId($license->id)->get();
        if ($strains->isEmpty()) {
            return;
        }

        foreach ($rooms as $room) {
            $harvestGroups = factory(HarvestGroup::class, rand(2, 5))->create(
                [
                    'license_id' => $license->id,
                    'room_id' => $room->id,
                ]
            );

            foreach ($harvestGroups as $harvestGroup) {
                $count = 0;
                foreach ($strains->random(rand(1, 3)) as $strain) {
                    $harvest = factory(Harvest::class)->create(
                        [
                            'license_id' => $license->id,
                            'strain_id' => $strain->id,
                            'room_id' => $room->id,
                            'harvest_group_id' => $harvestGroup->id,
                            'flower_room_id' => $harvestRooms->random()->id,
                            'material_room_id' => $harvestRooms->random()->id,
                            'waste_room_id' => $disposalRooms->random()->id,
                        ]
                    );

                    $plants = Plant::where(
                        [
                            'license_id' => $license->id,
                            'strain_id' => $strain->id,
                            'room_id' => $room->id,
                        ]
                    )->inRandomOrder()->limit(5)->get();
                    if ($plants->isEmpty()) {
                        $harvest->delete();
                        continue;
                    }
                    $count++;
                    foreach ($plants as $plant) {
                        factory(HarvestPlant::class)->create(['harvest_id' => $harvest->id, 'plant_id' => $plant->id,]);
                        $plant->update(['grow_status' => SeedToSale::GROW_STATUS_HARVESTED, 'harvested_at' => Carbon::now()]);
                    }

                    factory(HarvestWetWeight::class, rand(3, 10))->create(['harvest_id' => $harvest->id,]);
                }
                if ($count === 0) {
                    $harvestGroup->delete();
                } else {
                    $harvestGroup->update(['source_rooms' => [$room->id]]);
                }
            }
        }
    }
}
