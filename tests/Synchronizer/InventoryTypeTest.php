<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{InventoryType, License, SourceProduct, State, StateCategory, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;

/**
 * @package Tests\Synchronizer
 */
class InventoryTypeTest extends TestBase
{
    /** @test */
    public function it_can_push_create_action_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $stateCategory = StateCategory::where('regulator', State::REGULATOR_LEAF)
            ->whereNotNull('parent_id')->inRandomOrder()->first();
        $inventoryType = factory(InventoryType::class)->create(
            ['license_id' => $licenseId, 'state_category_id' => $stateCategory->id]
        );
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $inventoryType->id,
                'resource_type' => get_classname($inventoryType),
                'resource_data' => $inventoryType->attributesToArray(),
                'resource_changes' => $inventoryType->getChanges(),
            ]
        );

        $syncCode = "WAG010101.TY" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.TY4U;
        $responseData = [
            [
                "name" => $inventoryType->name,
                "global_id" => $syncCode,
                "external_id" => $inventoryType->id
            ]
        ];
        Http::fake(['/inventory_types' => Http::response($responseData, 200),]);

        $result = app(Synchronizer::class)->push($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($syncCode, $result->resource->sync_code);
    }

    /** @test */
    public function it_can_push_create_action_to_metrc()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $inventoryType = factory(InventoryType::class)->create(['license_id' => $licenseId]);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $inventoryType->id,
                'resource_type' => get_classname($inventoryType),
                'resource_data' => $inventoryType->attributesToArray(),
                'resource_changes' => $inventoryType->getChanges(),
            ]
        );

        Http::fake(['/items/v1/create*' => Http::response([], 200),]);

        $randomId = $this->faker->randomNumber();
        Http::fake(
            [
                '/items/v1/active*' => Http::response(
                    [['Id' => $randomId, 'Name' => $inventoryType->name]],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($randomId, $result->resource->sync_code);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );
        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => InventoryType::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ],
            ]
        );
        $data = [];
        $stateCategories = StateCategory::whereRegulator(State::REGULATOR_LEAF)->whereNull('parent_id')
            ->whereIn('code', ['intermediate_product',])->with('children')->get();
        $totalRecord = rand(5, 10);
        $numberOfSourceProduct = 0;
        for ($j = 1; $j <= $totalRecord; $j++) {
            $category = $stateCategories->random();
            $intermediateType = $category->children->random();
            $syncCode = "WAG010101.TY" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.TYA1Y;
            $data[] = [
                "name" => $this->faker->name,
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
                "type" => $category->code,
                "intermediate_type" => $intermediateType->code,
                "updated_at" => $this->faker->dateTimeBetween($fromDate, $toDate)->format('m/d/Y G:ia'),
            ];
            if ($category->code === 'intermediate_product') {
                $numberOfSourceProduct++;
            }
        }

        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
        ];
        Http::fake(
            [
                sprintf("/inventory_types?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response(
                    $responseData,
                    200
                ),
            ]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, InventoryType::count());
        $this->assertEquals($numberOfSourceProduct, SourceProduct::count());
    }

    public function it_can_pull_from_metrc_api()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => InventoryType::class,
            ]
        );
        $totalRecord = rand(1, 100);
        $data = [];
        for ($i = 1; $i <= $totalRecord; $i++) {
            $data[] = ['Id' => $this->faker->randomNumber(), 'Name' => $this->faker->name];
        }
        Http::fake(
            [
                '/items/v1/active*' => Http::response(
                    $data,
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, $result->response_data['success']);
    }
}
