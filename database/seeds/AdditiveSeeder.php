<?php

use App\Models\{Additive, License, User};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class AdditiveSeeder extends Seeder
{
    private ?Collection $users = null;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $licenseIteration = function ($license) {
            call_user_func([$this, 'iterateLicense'], $license);
        };
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each($licenseIteration);
    }

    protected function iterateLicense(License $license)
    {
        for ($i = 0; $i <= 5; $i++) {
            factory(Additive::class)->create(
                [
                    "name" => 'Additive ' . $license->id . ' ' . $i,
                    "license_id" => $license->id,
                    "type" => Additive::TYPES[rand(0, 1)],
                    "uom" => Additive::UOM_GALLON,
                    'created_by' => $this->getRandomUserId(),
                    'updated_by' => $this->getRandomUserId(),
                ]
            );
        }
    }

    private function getRandomUserId(): ?string
    {
        if (!$this->users) {
            $this->users = User::all();
        }
        if ($this->users->count() === 0) {
            return null;
        }

        return $this->users->random()->id;
    }
}
