<?php

/** @var Factory $factory */

use App\Models\License;
use App\Models\Trace;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Trace::class,
    function (Faker $faker) {
        return [
            "license_id" => function() {
                return factory(License::class)->create()->id;
            },
            'method' => $faker->randomElement([Trace::METHOD_PUSH, Trace::METHOD_PULL]),
            'action' => $faker->randomElement([Trace::ACTION_CREATE, Trace::ACTION_UPDATE, Trace::ACTION_DELETE]),
            'status' => $faker->randomElement(
                [
                    Trace::STATUS_PENDING,
                    Trace::STATUS_SEND_PROCESSING,
                    Trace::STATUS_SEND_COMPLETED,
                    Trace::STATUS_PROCESS_PROCESSING,
                    Trace::STATUS_COMPLETED
                ]
            ),
        ];
    }
);
