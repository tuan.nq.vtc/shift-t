<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Handleable;
use App\Cqrs\Values\ClientIdentifier;
use App\Models\QASampleCoa;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class UploadQASampleCOAHandler extends Handleable
{
    const UPLOAD_LOCATION = 'qa-samples';

    const UPLOAD_LABEL = 'coa';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var ClientIdentifier
     */
    protected $clientIdentifier;

    /**
     * @var array
     */
    private $mimeTypes;

    /**
     * @var string
     */
    private $uploadUrl;

    /**
     * @var QASampleCoa
     */
    private $model;

    /**
     * @param QASampleCoa $model
     * @param ClientInterface $httpClient
     * @param ClientIdentifier $clientIdentifier
     * @param LoggerInterface $logger
     * @param string $uploadUrl
     * @param array $mimeTypes
     */
    public function __construct(QASampleCoa      $model,
                                ClientInterface  $httpClient,
                                ClientIdentifier $clientIdentifier,
                                LoggerInterface  $logger,
                                string           $uploadUrl,
                                array            $mimeTypes)
    {
        $this->httpClient = $httpClient;
        $this->mimeTypes = $mimeTypes;
        $this->uploadUrl = $uploadUrl;
        $this->logger = $logger;
        $this->model = $model;
        $this->clientIdentifier = $clientIdentifier;
    }

    /**
     * @param UploadQASampleCOACommand $command
     * @return QASampleCoa
     * @throws GuzzleException
     * @throws Throwable
     */
    public function handle(UploadQASampleCOACommand $command): QASampleCoa
    {
        try {
            $this->ensureCanUploadedFile($uploadedFile = $command->getUploadedFile());
            $path = $uploadedFile->getRealPath();

            $uploaded = $this->uploadFile(sprintf('%s/%s', self::UPLOAD_LOCATION, $command->getQaSampleId()), $uploadedFile);
            if (!$uploaded) {
                throw new Exception('Failed upload file');
            }

            if (file_exists($path)) {
                @unlink($path);
            }

            return $this->createQASampleCoa($command->getQaSampleId(), $uploaded, $uploadedFile->getClientOriginalName());
        } catch (Throwable $e) {
            $this->log($command, $e->getMessage());
            throw $e;
        }
    }

    /**
     * @param $qaSampleId
     * @param $fileName
     * @param string $originalFilename
     * @return QASampleCoa
     * @throws Exception
     */
    private function createQASampleCoa($qaSampleId, $fileName,string $originalFilename): QASampleCoa
    {
        $model = new QASampleCoa();

        $model->setAttribute('filename', $fileName);
        $model->setAttribute('original_filename', $originalFilename);
        $model->setAttribute('qa_sample_id', $qaSampleId);
        $model->setAttribute('uploaded_at', date('Y-m-d H:i:s'));

        if (true !== $model->save()) {
            throw new Exception('Create QA Sample COA failed');
        }

        return $model;
    }

    /**
     * @param $qaSampleId
     * @param $path
     * @param $fileName
     * @return StreamInterface
     * @throws GuzzleException
     */
    private function upload($qaSampleId, $path, $fileName): StreamInterface
    {
        $res = $this->httpClient->request('POST', $this->uploadUrl, [
            'headers' => [
                'X-Client-Id' => $this->clientIdentifier->getClientId(),
                'X-Client-Secret' => $this->clientIdentifier->getClientSecret(),
            ],
            'multipart' => [
                ['name' => 'file', 'contents' => Utils::tryFopen($path, 'r'), 'filename' => $fileName],
                ['name' => 'owner', 'contents' => $qaSampleId],
                [
                    'name' => 'location',
                    'contents' => sprintf('%s/qa-sample-%s', self::UPLOAD_LOCATION, $qaSampleId)
                ],
                ['name' => 'label', 'contents' => self::UPLOAD_LABEL]
            ]
        ]);

        return $res->getBody();
    }

    /**
     * @param $uploadedFile
     * @throws Throwable
     */
    private function ensureCanUploadedFile($uploadedFile)
    {
        if ($uploadedFile instanceof UploadedFile !== true) {
            throw new Exception('The COA File is required');
        }

        try {
            if (!in_array($uploadedFile->getMimeType(), $this->mimeTypes)) {
                $msg = count($this->mimeTypes) > 1 ? ' one of these:' : '';
                throw new Exception(
                    sprintf('The Type of the COA File must be%s \'%s\'',
                        $msg,
                        implode(', ', $this->mimeTypes))
                );
            }
        } catch (Throwable $e) {
            $filePath = $uploadedFile->getRealPath();

            if (file_exists($filePath)) {
                @unlink($filePath);
            }

            throw $e;
        }
    }

    /**
     * @param string $fileName
     * @param UploadedFile|null $uploadedFile
     * @return string
     */
    private function uploadFile(string $fileName, ?UploadedFile $uploadedFile): string
    {
        try {
            return throw_unless(Storage::putFile($fileName, $uploadedFile), new Exception('Failed save file'));
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
            return '';
        }
    }
}
