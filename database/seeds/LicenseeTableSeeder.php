<?php

use App\Models\{Licensee, License, State};
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use \App\Synchronizations\Resolvers\ClientResolver;
use \App\Synchronizations\Http\Clients\LeafClient;

/**
 * Class LicenseeTableSeeder.
 */
class LicenseeTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $licenses = License::all();
        $states = [];
        foreach ($licenses as $license) {
            if (isset($states[$license->state_code]) || $license->isRegulator(State::REGULATOR_METRC)) {
                continue;
            }
            $states[$license->state_code] = $license->state_code;
            if ($license->is_test) {
                $this->createTestLicensee($license);
                continue;
            }
            try {
                /** @var LeafClient $client */
                $client = (new ClientResolver())->resolve($license);
                $response = $client->getRequest()->get('/mmes');
                foreach ($response->json() as $licenseeData) {
                    Licensee::query()->create(
                        [
                            'name' => Arr::get($licenseeData, 'name'),
                            'regulator' => State::REGULATOR_LEAF,
                            'address1' => Arr::get($licenseeData, 'address1'),
                            'address2' => Arr::get($licenseeData, 'address2'),
                            'city' => Arr::get($licenseeData, 'city'),
                            'state_code' => Arr::get($licenseeData, 'state_code'),
                            'phone' => Arr::get($licenseeData, 'phone'),
                            'type' => Arr::get($licenseeData, 'type'),
                            'code' => Arr::get($licenseeData, 'code'),
                            'sync_code' => Arr::get($licenseeData, 'global_id'),
                        ]
                    );
                }
            } catch (\Exception $e) {
                // just echo the message and ignore this error
                echo "Cant fetch licensee with code :" . $license->code . "\n";
            }
        }

    }

    private function createTestLicensee(License $license)
    {
        factory(Licensee::class, 5)->create([
            'state_code' => $license->state_code,
            'regulator' => $license->getRegulator()
        ]);
    }
}
