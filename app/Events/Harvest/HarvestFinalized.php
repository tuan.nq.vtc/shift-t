<?php

namespace App\Events\Harvest;

use App\Events\Event;
use App\Models\Harvest;

class HarvestFinalized extends Event
{
    /**
     * @var Harvest
     */
    public Harvest $harvest;

    /**
     * @param Harvest $harvest
     */
    public function __construct(Harvest $harvest)
    {
        $this->harvest = $harvest;
    }
}
