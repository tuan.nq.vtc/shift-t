<?php


namespace App\Services;


use App\Exceptions\GeneralException;
use App\Models\AdditiveLog;

class AdditiveLogService extends BaseService
{
    public function __construct(AdditiveLog $additiveLog)
    {
        parent::__construct($additiveLog);
    }

    /**
     * @param $target
     * @param string $targetId
     *
     * @return \Illuminate\Support\Collection
     * @throws GeneralException
     */
    public function getLogs($target, string $targetId)
    {
        $query = $this->model->newQuery();
        switch ($target) {
            case 'plant':
                $query->where('plant_id', $targetId);
                break;
            case 'propagation':
                $query->where('propagation_id', $targetId);
                break;
            default:
                throw new GeneralException(__('exceptions.additive_log.target_invalid', ['target' => $target]));
        }

        return $query->with(
            [
                'operation:id,applied_quantity',
                'additive:id,name,type,epa_regulation_number,is_other_type,uom',
                'supplier:id,name',
                'appliedBy'
            ]
        )->orderBy('applied_date')->get();
    }
}
