<?php

namespace App\Services;

use App\Models\FlowerType;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class FlowerTypeService
 * @package App\Services
 *
 * @method FlowerType|Collection get($id)
 * @method FlowerType|Builder getModel()
 */
class FlowerTypeService extends BaseService
{
    public function __construct(FlowerType $flowerType)
    {
        parent::__construct($flowerType);
    }

    /**
     * @return Collection
     */
    public function getAvailableItems(): Collection
    {
        return $this->newQuery()->where('status', FlowerType::STATUS_ENABLED)->get();
    }
}
