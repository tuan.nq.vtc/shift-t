<?php

namespace App\Console\Commands;

use App\Services\DisposalService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckScheduledForDestructionCommand extends Command
{
    /**
     * @var string
     */
    protected $description = "Check all scheduled disposal record, change its status to 'ready to destroy' if wasted_at >= now()  ";
    /**
     * @var string
     */
    protected $signature = 'trace:scheduled-disposal';

    private $disposalService;

    public function __construct(DisposalService $disposalService)
    {
        parent::__construct();
        $this->disposalService = $disposalService;
    }

    public function handle()
    {
        try {
            $check = $this->disposalService->changeStatusScheduledDisposal();
            Log::info("trace:scheduled-disposal - result : $check");
        } catch (\Throwable $throwable) {
            Log::error("trace:scheduled-disposal " . $throwable->getMessage());
            error_log($throwable->getMessage());
            throw $throwable;
        }
    }
}
