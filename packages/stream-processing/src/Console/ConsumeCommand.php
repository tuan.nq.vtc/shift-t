<?php

namespace Bamboo\StreamProcessing\Console;

use Bamboo\StreamProcessing\Contracts\ConsumerHandlerInterface;
use Bamboo\StreamProcessing\Models\FailedEvent;
use Bamboo\StreamProcessing\Resolvers\ConsumerEventProcessResolver;
use Exception;
use Illuminate\Console\Command;
use Throwable;

class ConsumeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stream:consume {topic}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stream consumer';

    /**
     * Execute the console command.
     *
     * @param ConsumerHandlerInterface $handler
     *
     * @param ConsumerEventProcessResolver $consumerProcessResolver
     * @return void
     */
    public function handle(ConsumerHandlerInterface $handler, ConsumerEventProcessResolver $consumerProcessResolver)
    {
        $topicName = $this->argument('topic');
        while (true) {
            try {
                $streamEvent = $handler->receiveEvent($topicName);
                $this->info(
                    '[' . date('Y-m-d H:i:s') . '][' . $topicName . '][' .
                    $streamEvent->getId() . '] Stream Event Received'
                );
            } catch (Throwable $throwable) {
                $error = '[' . date('Y-m-d H:i:s') . '][' . $topicName . '] Receive Stream Event Failed';
                $this->error($error);
                $this->error($throwable->getMessage());
                continue;
            }

            try {
                $eventProcess = $consumerProcessResolver->resolve($topicName);
            } catch (Exception $e) {
                $this->error($e->getMessage());
                // skip process the event
                continue;
            }

            try {
                $eventProcess->execute($streamEvent);
                $this->info(
                    '[' . date('Y-m-d H:i:s') . '][' . $topicName . '][' .
                    $streamEvent->getId() . '] Stream Processed'
                );
            } catch (Throwable $throwable) {
                $error = '[' . date('Y-m-d H:i:s') . '][' . $topicName . '][' .
                    $streamEvent->getId() . '] Process Stream Event Failed';
                $this->error($error);
                $this->error($throwable->getMessage());

                // create failed event record
                FailedEvent::create(
                    [
                        'topic' => $topicName,
                        'payload' => $streamEvent->getPayload(),
                        'metadata' => $streamEvent->getMetadata(),
                        'exception' => $throwable->getTraceAsString(),
                    ]
                );
            }
            sleep(1);
        }
    }
}
