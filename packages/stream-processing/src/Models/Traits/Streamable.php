<?php

namespace Bamboo\StreamProcessing\Models\Traits;

use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Events\StreamableResourceCreated;
use Bamboo\StreamProcessing\Events\StreamableResourceDeleted;
use Bamboo\StreamProcessing\Events\StreamableResourceUpdated;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Facades\Event;
use Ramsey\Uuid\Uuid;
use Throwable;

trait Streamable
{
    /**
     * @throws Throwable
     */
    public static function bootStreamable(): void
    {
        static::created(
            static fn(StreamableResourceInterface $instance) => Event::dispatch(new StreamableResourceCreated($instance))
        );
        static::deleted(
            static fn(StreamableResourceInterface $instance) => Event::dispatch(new StreamableResourceDeleted($instance))
        );
        static::updated(
            static fn(StreamableResourceInterface $instance) => Event::dispatch(new StreamableResourceUpdated($instance))
        );
    }

    protected function getStreamableFields(): array
    {
        return [];
    }

    public function getTopic(): string
    {
        return '';
    }

    public function getStreamEventPayload(): array
    {
        return array_merge(
            [$this->getKeyName() => $this->getKey()],
            $this->fresh()->only($this->getStreamableFields())
        );
    }

    public function isStreamUpdatable(): bool
    {
        return !empty(array_intersect(array_keys($this->changes), $this->getStreamableFields()));
    }
}
