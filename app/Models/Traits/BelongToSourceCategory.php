<?php

namespace App\Models\Traits;

use App\Models\SourceCategory;
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo};

trait BelongToSourceCategory
{
    public function sourceCategory(): BelongsTo
    {
        return $this->belongsTo(SourceCategory::class);
    }

    public function scopeSourceCategoryId(Builder $query, ...$sourceCateIds): Builder
    {
        return $query->whereIn('source_category_id', $sourceCateIds);
    }

    public function scopeSourceCategory(Builder $query, ...$sourceCateIds): Builder
    {
        return $this->scopeSourceCategoryId($query, ...$sourceCateIds);
    }

    public function scopeOrderBySourceCategory(Builder $query, $direction): Builder
    {
        $sourceCategoryTable = $this->sourceCategory()->getRelated()->getTable();

        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($sourceCategoryTable, $this->getTable() . '.source_category_id', '=', $sourceCategoryTable . '.id')
            ->orderBy($sourceCategoryTable . '.name', $direction);
    }
}
