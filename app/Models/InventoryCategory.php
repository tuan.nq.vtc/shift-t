<?php

namespace App\Models;

use App\Models\Traits\{HasSluggableName, Scope\IdsFilterable, Scope\NameFilterable, Scope\SlugFilterable};
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

/**
 * Class InventoryCategory
 * @package App\Models
 */
class InventoryCategory extends BaseModel
{
    use IdsFilterable;
    use HasSluggableName;
    use NameFilterable;
    use SlugFilterable;

    public const COUNT_BASED = 'count_based';
    public const VOLUME_BASED = 'volume_based';
    public const WEIGHT_BASED = 'weight_based';

    // UOM LEAF
    public const UOM_LEAF_EACH = 'ea';
    public const UOM_LEAF_GRAM = 'gm';

    // COUNT BASED
    public const UOM_EACH = 'Each';
    // WEIGHT BASED
    public const UOM_KILOGRAM = 'Kilograms';
    public const UOM_GRAM = 'Grams';
    public const UOM_MILLIGRAM = 'Milligrams';
    public const UOM_OUNCE = 'Ounces';
    public const UOM_POUND = 'Pounds';
    // VOLUME BASED
    public const UOM_FLUID_OUNCE = 'Fluid Ounces';
    public const UOM_LITTER = 'Liters';
    public const UOM_MILLI_LITTER = 'Milliliters';
    public const UOM_PINT = 'Pints';
    public const UOM_QUART = 'Quarts';
    public const UOM_GALLON = 'Gallons';
    // slug
    // leaf system
    public const SLUG_HARVEST_FLOWER = 'flower';
    public const SLUG_HARVEST_FLOWER_LOTS = 'flower_lots';
    public const SLUG_HARVEST_MATERIAL = 'other_material';
    public const SLUG_HARVEST_MATERIAL_LOTS  = 'other_material_lots';
    public const SLUG_HARVEST_HARVEST_MATERIAL = 'harvest_materials';

    protected $table = 'inventory_categories';
    protected $fillable = [
        'regulator',
        'parent_id',
        'name',
        'slug',
        'is_strain_required',
        'is_serving_required',
        'is_weight_per_unit_required',
        'is_volume_per_unit_required',
        'quantity_type',
        'uom',
    ];

    /**
     * Pre-configured count units
     *
     * @return string[]
     */
    public static function getCountBasedUnitList(): array
    {
        return [
            self::UOM_EACH,
        ];
    }

    /**
     * Pre-configured weight units
     *
     * @return string[]
     */
    public static function getWeightBasedUnitList(): array
    {
        return [
            self::UOM_KILOGRAM,
            self::UOM_GRAM,
            self::UOM_MILLIGRAM,
            self::UOM_OUNCE,
            self::UOM_POUND,
        ];
    }

    /**
     * Pre-configured units of measure
     *
     * @return string[]
     */
    public static function getLeafUnitList(): array
    {
        return [
            self::UOM_LEAF_EACH,
            self::UOM_LEAF_GRAM,
        ];
    }

    /**
     * Pre-configured units of measure
     *
     * @return string[]
     */
    public static function getMetrcUnitList(): array
    {
        return array_merge(
            self::getCountBasedUnitList(),
            self::getWeightBasedUnitList(),
            self::getVolumeBasedUnitList()
        );
    }

    /**
     * Pre-configured quantity types
     *
     * @return string[]
     */
    public static function getQuantityTypeList(): array
    {
        return [
            self::COUNT_BASED,
            self::VOLUME_BASED,
            self::WEIGHT_BASED,
        ];
    }

    /**
     * Pre-configured volume units
     *
     * @return string[]
     */
    public static function getVolumeBasedUnitList(): array
    {
        return [
            self::UOM_FLUID_OUNCE,
            self::UOM_LITTER,
            self::UOM_MILLI_LITTER,
            self::UOM_PINT,
            self::UOM_QUART,
            self::UOM_GALLON,
        ];
    }

    /**
     * Relation with Inventory Type
     *
     * @return HasMany
     */
    public function inventoryTypes(): HasMany
    {
        return $this->hasMany(InventoryType::class, 'category_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(InventoryCategory::class, 'parent_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(InventoryCategory::class, 'parent_id', 'id');
    }
}
