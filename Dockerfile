FROM reg.webprovise.io/webprovise/nginx-php:v1

COPY ./php.ini /etc/php7/conf.d/90_php.ini
COPY --chown=www . /var/www/html

RUN composer install --prefer-dist --no-autoloader --no-scripts --no-progress --no-interaction && \
  composer clear-cache --no-interaction && \
  composer dump-autoload --optimize --classmap-authoritative --no-interaction
