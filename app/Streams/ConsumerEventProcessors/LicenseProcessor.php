<?php

namespace App\Streams\ConsumerEventProcessors;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{License, State};
use App\Models\Trace;
use App\Models\TraceableModel;
use App\Services\LicenseService;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Contracts\StreamEventInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Arr;
use Ramsey\Uuid\Uuid;

class LicenseProcessor extends BaseProcessor
{
    protected static string $modelName = License::class;

    /**
     * @param StreamEventInterface $streamEvent
     * @throws Exception
     */
    protected function startResourceSyncing(StreamEventInterface $streamEvent): void
    {
        $payload = $streamEvent->getPayload();
        /** @var License $license */
        $license = $this->getModel()->find($this->getIdFromPayload($payload));
        if (!$license) {
            $license = $this->getModel()->create($payload);
        } else {
            $license->update(Arr::except($payload, ['id']));
        }

        if ($license->is_test) {
            throw new Exception('Syncing Process is not allowed for the test license');
        }

        $conditions = [];
        $dateTimeRange = [];
        if ($license->isRegulator(State::REGULATOR_LEAF)) {
            $conditions = [
                'page' => 1,
            ];
            if ($license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $dateTimeRange = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }
        }

        $traceArray = [];
        $syncResources = config('licenses.sync_resources');
        foreach ($syncResources as $syncResourceKey => $syncResource) {
            if ($syncResource['filter_by_date']?? false) {
                $conditions = array_merge($conditions, $dateTimeRange);
            }
            $trace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => $syncResource['model'],
                    'resource_conditions' => $conditions,
                    'status' => Trace::STATUS_PENDING
                ]
            );
            $traceArray[$syncResourceKey] = $trace;
            $messageEvent = new StreamEvent(
                Uuid::uuid4()->toString(),
                [
                    'license_id' => $license->id,
                    'name' => $syncResource['name'],
                ],
                [
                    'action' => StreamableResourceInterface::ACTION_CREATE,
                ]
            );

            // #todo: handle exception
            dispatch(
                new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
            );
        }

        foreach ($syncResources as $syncResourceKey => $syncResource) {
            if (empty($traceArray[$syncResourceKey])) {
                continue;
            }
            $trace = $traceArray[$syncResourceKey];
            $dependencyIds = [];
            foreach ($syncResource['dependencies']?? [] as $dependency) {
                if (!empty($traceArray[$dependency])) {
                    $dependencyIds[] = $traceArray[$dependency]->id;
                }
            }
            if (!empty($dependencyIds)) {
                $trace->dependencies()->attach($dependencyIds);
            } else {
                dispatch(new SyncData($trace));
            }
        }
    }

    protected function create(StreamEventInterface $streamEvent): void
    {
        $payload = $streamEvent->getPayload();
        if ($this->getModel()->find($this->getIdFromPayload($payload))) {
            throw new Exception('The license already exists');
        }
        $license = $this->getModel()->create($payload);
        app(LicenseService::class)->syncStateCategories($license);
    }
}
