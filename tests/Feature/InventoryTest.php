<?php

namespace Tests\Feature;

use App\Models\Inventory;
use Illuminate\Http\Response;
use Tests\Concerns\{ActingWithAttachedHeaders, Traceable, WithFaker};
use Tests\TestCase;

/**
 * Class InventoryTest
 *
 * @package Tests\Feature
 */
class InventoryTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $modelName = Inventory::class;

    /** @test */
    public function guest_cannot_call_inventory_endpoints()
    {
        $this->actingAsGuest();
        $this->get($this->getEndpoint("/list"))->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_can_create_lotted_inventory()
    {
        /** @var Inventory $parentInventory * */
        $parentInventory = factory(Inventory::class)->create(
            [
                'quantity' => 300,
                'is_lotted' => false,
                'license_id' => $this->licenseId
            ]);
        $numberLot = 4;

        $fitData = [
            'inventory_id' => $parentInventory->id,
            'weight_per_lot' => $parentInventory->quantity / $numberLot,
            'number_of_lot' => $numberLot,
            'room_id' => $parentInventory->room_id
        ];

        $this->post($this->getEndpoint('/create-lots'), array_merge($fitData, ['number_of_lot' => $numberLot + 1]))
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        $fitResponse = $this->post($this->getEndpoint('/create-lots'), $fitData)
            ->seeStatusCode(Response::HTTP_CREATED)
            ->response->original;
        $this->assertEquals(is_countable($fitResponse), true);
        $this->assertEquals(count($fitResponse), $numberLot);
        $this->assertEquals($parentInventory->fresh()->is_lotted, false);
        // re-make inventory to test exceeded weight case
        $quantity = rand(100, 1000);
        $parentInventory = factory(Inventory::class)->create(
            [
                'quantity' => $quantity,
                'is_lotted' => true,
                'license_id' => $this->licenseId,
            ]);

        $weightPerLot = ($parentInventory->quantity / $numberLot) - 1;
        $excessData = [
            'inventory_id' => $parentInventory->id,
            'weight_per_lot' => $weightPerLot,
            'number_of_lot' => $numberLot,
            'room_id' => $parentInventory->room_id
        ];
        $excessResponse = $this->post($this->getEndpoint('/create-lots'), $excessData)
            ->seeStatusCode(Response::HTTP_CREATED)
            ->response->original;
        $this->assertEquals(is_countable($excessResponse), true);
        $this->assertEquals(count($excessResponse), $numberLot);
        $freshInventory = $parentInventory->fresh();
        $this->assertEquals($freshInventory->is_lotted, true);
        $this->assertEquals( (float)$freshInventory->quantity, $quantity - ($weightPerLot * $numberLot));
    }

    private function getEndpoint($path = '')
    {
        return "/inventories{$path}";
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

}
