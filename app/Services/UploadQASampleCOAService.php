<?php

namespace App\Services;

use App\Cqrs\Commands\UploadQASampleCOACommand;
use App\Cqrs\Commands\UploadQASampleCOAHandler;
use Illuminate\Http\JsonResponse;
use Throwable;

class UploadQASampleCOAService
{
    const MSG_FAILED = 'Upload COA failed';

    /**
     * @var UploadQASampleCOAHandler
     */
    private $handler;

    /**
     * @param UploadQASampleCOAHandler $handler
     */
    public function __construct(UploadQASampleCOAHandler $handler)
    {
       $this->handler = $handler;
    }

    /**
     * @param UploadQASampleCOACommand $command
     * @return JsonResponse
     */
    public function __invoke(UploadQASampleCOACommand $command): JsonResponse
    {
        try {
            return wpApiResponse($this->handler->handle($command));
        } catch (Throwable $e) {
            return wpApiResponseWithErrors([$e->getMessage()], self::MSG_FAILED);
        }
    }
}
