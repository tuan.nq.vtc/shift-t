<?php

namespace App\Models;

use App\Models\Traits\Scope\DatetimeFilterable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 */
class User extends BaseModel
{
    use SoftDeletes;
    use DatetimeFilterable;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $appends = [
        'name'
    ];

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'active',
        'name',
        'account_holder_id'
    ];

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
