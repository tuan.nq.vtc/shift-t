<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{LicenseFilterable, NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

/**
 * Class InventoryType
 * @package App\Models
 */
class InventoryType extends TraceableModel
{
    use LicenseFilterable;
    use NameFilterable;
    use StatusFilterable;
    use HasInternalId;

    protected $fillable = [
        'license_id',
        'state_category_id',
        'strain_id',
        'name',
        'status',
        'uom',
        'options',
        'info',
        'sync_code',
    ];

    protected $casts = [
        'options' => 'array',
        'info' => 'array'
    ];

    protected array $traceable = [
        'name'
    ];

    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            return [
                'global_id' => fn($instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'name' => fn($instance, $value) => $instance->setAttribute('name', $value),
                'intermediate_type' => function ($instance, $value) {
                    $inventoryCategory = InventoryCategory::whereRegulator(State::REGULATOR_LEAF)->whereNotNull('parent_id')->whereSlug($value)->first();
                    if ($inventoryCategory) {
                        $instance->setAttribute('category_id', $inventoryCategory->id);
                        return $instance;
                    }

                    return null;
                },
                'weight_per_unit_in_grams' => fn($instance, $value) => $instance->setAttribute('options->weight', $value),
                'serving_num' => fn($instance, $value) => $instance->setAttribute('options->serving_num', $value),
                'serving_size' => fn($instance, $value) => $instance->setAttribute('options->serving_size', $value),
                // Save external_id from LEAF as an info attribute
                'external_id' => fn($instance, $value) => $instance->setAttribute('info', ['external_id' => $value]),
            ];
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            return [
                'Id' => fn($instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'Name' => fn($instance, $value) => $instance->setAttribute('name', $value),
                'ProductCategoryName' => function ($instance, $value) {
                    $inventoryCategory = InventoryCategory::whereRegulator(State::REGULATOR_METRC)->whereName($value)->first();
                    if ($inventoryCategory) {
                        $instance->setAttribute('category_id', $inventoryCategory->id);
                        return $instance;
                    }

                    return null;
                },
                'UnitOfMeasureName' => fn($instance, $value) => $instance->setAttribute('options->uom', $value),
                'StrainId' => function ($instance, $value) {
                    $strain = Strain::where('sync_code', $value)->first();
                    return $instance->setAttribute('strain_id', $strain ? $strain->id : null);
                },
                'UnitVolume' => fn($instance, $value) => $instance->setAttribute('options->volume', $value),
                'UnitVolumeUnitOfMeasureName' => fn($instance, $value) => $instance->setAttribute('options->unit_uom', $value),
                'UnitWeight' => fn($instance, $value) => $instance->setAttribute('options->weight', $value),
                'UnitWeightUnitOfMeasureName' => fn($instance, $value) => $instance->setAttribute('options->unit_uom', $value ? $value : $instance->options['unit_uom']),
            ];
        }

        return null;
    }

    public function stateCategory(): BelongsTo
    {
        return $this->belongsTo(StateCategory::class, 'state_category_id', 'id');
    }

    public function inventories(): HasMany
    {
        return $this->hasMany(Inventory::class, 'type_id', 'id');
    }

    public function strain(): BelongsTo
    {
        return $this->belongsTo(Strain::class, 'strain_id', 'id');
    }


    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%' . $value . '%');
    }

    public function scopeInventoryCategory(Builder $query, ...$inventoryCategoryId): Builder
    {
        return $this->scopeInventoryCategoryId($query, ...$inventoryCategoryId);
    }

    public function scopeInventoryCategoryId(Builder $query, ...$inventoryCategoryId): Builder
    {
        return $query->whereIn('state_category_id', $inventoryCategoryId);
    }
}
