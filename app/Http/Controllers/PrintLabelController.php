<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\PrintLabel;
use App\Services\PrintLabelService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

/**
 * Class PrintLabelController
 * @package App\Http\Controllers
 */
class PrintLabelController extends CRUDController
{
    public function __construct(PrintLabelService $baseService, Request $request)
    {
        $request->merge(
            [
                'filter' => array_merge(
                    $request->get('filter', []),
                    $request->only(['account_holder_id'])
                )
            ]
        );
        parent::__construct($baseService, $request);
    }

    public function getCreateRules(): array
    {
        $accountHolderId = $this->request->get('account_holder_id');
        return [
            'account_holder_id' => ['required', 'uuid'],
            'print_label_template_id' => [
                'nullable',
                'exists:App\Models\PrintLabelTemplate,id,deleted_at,NULL,account_holder_id,' . $accountHolderId,
            ],
            'category' => ['required', 'string', new In(PrintLabel::CATEGORIES)],
            'name' => ['required', 'string', 'max:150'],
            'base64' => ['sometimes', 'required'],
            'box.width' => ['required', 'numeric', 'min:0.01', 'max:10'],
            'box.height' => ['required', 'numeric', 'min:0.01', 'max:10'],
            'box.top' => ['required', 'numeric', 'min:0', 'max:10'],
            'box.right' => ['required', 'numeric', 'min:0', 'max:10'],
            'box.bottom' => ['required', 'numeric', 'min:0', 'max:10'],
            'box.left' => ['required', 'numeric', 'min:0', 'max:10'],
            'items.*.mapping_field' => [],
            'items.*.box.width' => ['required', 'numeric'],
            'items.*.box.height' => ['required', 'numeric'],
            'items.*.box.top' => ['required', 'numeric'],
            'items.*.box.left' => ['required', 'numeric'],
            'items.*.zIndex' => 'required',
            'items.*.font_size' => 'required',
            'items.*.font_weight' => 'nullable',
            'items.*.barcode_width' => 'required',
            'items.*.content' => [],
            'items.*.label' => [],
            'items.*.show_label' => 'required',
            'items.*.auto_fit' => 'required',
            'items.*.is_custom' => 'required',
        ];
    }

    public function getUpdateRules(string $id): array
    {
        $accountHolderId = $this->request->get('account_holder_id');
        return [
            'print_label_template_id' => [
                'sometimes',
                'nullable',
                'exists:App\Models\PrintLabelTemplate,id,deleted_at,NULL,account_holder_id,' . $accountHolderId,
            ],
            'category' => ['sometimes', 'required', 'string'],
            'name' => ['sometimes', 'string', 'max:150'],
            'base64' => 'sometimes',
            'box.width' => ['sometimes', 'required', 'numeric', 'min:0.01', 'max:10'],
            'box.height' => ['sometimes', 'required', 'numeric', 'min:0.01', 'max:10'],
            'box.top' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'box.right' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'box.bottom' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'box.left' => ['sometimes', 'required', 'numeric', 'min:0', 'max:10'],
            'items' => 'sometimes',
            'items.*.id' => ['sometimes', 'required'],
            'items.*.mapping_field' => [],
            'items.*.box.width' => ['required', 'numeric'],
            'items.*.box.height' => ['required', 'numeric'],
            'items.*.box.top' => ['required', 'numeric'],
            'items.*.box.left' => ['required', 'numeric'],
            'items.*.zIndex' => 'required',
            'items.*.font_size' => 'required',
            'items.*.font_weight' => 'nullable',
            'items.*.barcode_width' => 'required',
            'items.*.content' => [],
            'items.*.content_label' => [],
            'items.*.show_label' => 'required',
            'items.*.auto_fit' => 'required',
            'items.*.is_custom' => 'required',
            'status' => 'sometimes',
        ];
    }

    public function categories()
    {
        return response()->json($this->service->categories());
    }

    public function index()
    {
        return response()->json($this->service->getAvailableItems());
    }

    public function getDetail($id): JsonResponse
    {
        return response()->json($this->service->getDetail($id));
    }

    public function getAll()
    {
        return response()->json($this->service->getAll(Criteria::createFromRequest($this->request)));
    }
}
