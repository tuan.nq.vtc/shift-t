<?php

namespace App\Models;

use App\Events\Harvest\HarvestWetWeightDeleted;
use App\Events\Harvest\HarvestWetWeightSaved;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property Harvest harvest
 * @property float weight
 */
class HarvestWetWeight extends BaseModel
{
    // type
    public const TYPE_FLOWER = 0;
    public const TYPE_MATERIAL = 1;
    public const TYPE_WASTE = 2;

    public const TYPES = [
        self::TYPE_FLOWER,
        self::TYPE_MATERIAL,
        self::TYPE_WASTE,
    ];

    protected $fillable = [
        'harvest_id',
        'harvested_at',
        'type',
        'weight',
    ];

    protected $dates = [
        'harvested_at',
    ];

    protected $dispatchesEvents = ['saved' => HarvestWetWeightSaved::class, 'deleted' => HarvestWetWeightDeleted::class,];

    public function harvest(): BelongsTo
    {
        return $this->belongsTo(Harvest::class);
    }

    /**
     * @param float $weight
     *
     * @return bool
     */
    public function increaseWeight(float $weight): bool
    {
        return $this->update(['weight' => $this->weight + $weight]);
    }
}
