<?php

namespace Tests\Feature;

use App\Models\Room;
use Illuminate\Http\Response;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

/**
 * Class RoomTest
 * @package Tests\Feature
 */
class RoomTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $endpoint = '/rooms';

    protected static string $modelName = Room::class;

    protected static array $requiredFields = ['name', 'room_type_id'];

    protected static array $expectedRelationFields = ['subrooms',];

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

    /** @test */
    public function guest_cannot_list_items()
    {
        $this->actingAsGuest();
        $this->get(static::$endpoint)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function guest_cannot_view_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $this->get(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function guest_cannot_create_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->post(static::$endpoint . '/' . $id, $expectedData)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function guest_cannot_update_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->put(static::$endpoint . '/' . $id, $expectedData)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function guest_cannot_delete_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $this->delete(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_list_items_without_license()
    {
        $this->actingWithoutLicense();
        $this->get(static::$endpoint)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_view_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $this->get(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_create_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->post(static::$endpoint . '/' . $id, $expectedData)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_update_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->put(static::$endpoint . '/' . $id, $expectedData)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_delete_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $this->delete(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_can_list_items()
    {
        $this->get(static::$endpoint . '/list')
            // Validate that a paginator is returned.
            ->seeJsonStructure(
                [
                    'data',
                    'current_page',
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total'
                ]
            );
    }

    /** @test */
    public function user_cannot_create_item_without_a_required_field()
    {
        foreach (static::$requiredFields as $requiredField) {
            $fields = array_diff(static::$requiredFields, [$requiredField]); // remove field
            $expectedData = !empty($fields) ? factory(static::$modelName)->make()->only($fields) : [];
            $this->post(static::$endpoint, $expectedData)->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /** @test */
    public function user_can_create_item()
    {
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);

        $this->post(static::$endpoint, $expectedData)
            ->seeStatusCode(Response::HTTP_CREATED)
            ->seeJsonStructure(['id'])
            ->seeJson($expectedData);
    }

    /** @test */
    public function user_can_view_item()
    {
        $model = factory(static::$modelName)->create(['license_id' => $this->licenseId]);
        $this->get(static::$endpoint . '/' . $model->id)
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonStructure(static::$expectedRelationFields)
            ->seeJson($model->only(array_merge(['id',], static::$requiredFields)));
    }

    /** @test */
    public function user_can_update_item()
    {
        $id = factory(static::$modelName)->create(['license_id' => $this->licenseId])->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->put(static::$endpoint . '/' . $id, $expectedData)
            ->seeStatusCode(Response::HTTP_ACCEPTED)
            ->seeJsonStructure(static::$expectedRelationFields)
            ->seeJson($expectedData);
    }

    /** @test */
    public function user_can_delete_item()
    {
        $id = factory(static::$modelName)->create(['license_id' => $this->licenseId])->id;

        $this->delete(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_NO_CONTENT);
    }
}
