<?php

const IMAGE_SIZE_THUMBNAIL = 'thumbnail';
const IMAGE_SIZE_FULL = 'full';

return [
    \App\Models\Strain::class => [
        IMAGE_SIZE_THUMBNAIL => [
            'width' => 80,
            'height' => 80,
        ],
        IMAGE_SIZE_FULL => [],
    ],
];
