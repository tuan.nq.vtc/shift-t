<?php

namespace App\Synchronizations\Contracts\Http;

interface QueryIteratorInterface
{
    /**
     * @return array
     */
    public function current(): array;

    public function next(): void;

    /**
     * @return bool
     */
    public function hasMore(): bool;
}
