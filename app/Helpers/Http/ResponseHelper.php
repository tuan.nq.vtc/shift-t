<?php

namespace App\Helpers\Http;

use Illuminate\Http\JsonResponse;

class ResponseHelper
{
    const STATUS_ERROR = 0;

    const STATUS_OK = 1;

    /**
     * @var null
     */
    private static $instacne = null;

    /**
     * @var array
     */
    private $data;

    /**
     * @var
     */
    private $errors;

    /**
     * @var string
     */
    private $errorMessage = 'Something went wrong';

    /**
     * @var int
     */
    private $httpStatus = 200;

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        if (self::$instacne) {
            return self::$instacne;
        }

        self::$instacne = new self();

        return self::$instacne;
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function response(int $status = self::STATUS_OK): JsonResponse
    {
        if ($status === self::STATUS_ERROR) {
            return response()->json([
                'message' => $this->errorMessage,
                'errors' => $this->errors
            ], $this->httpStatus);
        }

        return response()->json(['data' => $this->data], $this->httpStatus);
    }

    /**
     * @return JsonResponse
     */
    public function responseWithErrors(): JsonResponse
    {
        return $this->response(self::STATUS_ERROR);
    }

    /**
     * @param string $msg
     * @return $this
     */
    public function withErrorMessage(string $msg): self
    {
        $this->errorMessage = $msg;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function withData($data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param $errors
     * @return $this
     */
    public function withErrors($errors): self
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function withHttpStatus(int $status): self
    {
        $this->httpStatus = $status;
        return $this;
    }
}
