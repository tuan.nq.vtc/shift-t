<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManifestInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifest_inventory', function (Blueprint $table) {
            $table->uuid('manifest_id')->index();
            $table->uuid('inventory_id')->index();
            $table->decimal('quantity', 12, 2)->default(0);

            $table->foreign('manifest_id')->references('id')->on('manifests')->cascadeOnDelete();
            $table->foreign('inventory_id')->references('id')->on('inventories')->cascadeOnDelete();

            $table->primary(['manifest_id', 'inventory_id'], 'manifest_inventory_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manifest_inventory');
    }
}
