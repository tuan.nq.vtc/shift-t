<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropQaSampleResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qa_sample_results', function (Blueprint $table) {
            Schema::dropIfExists('qa_sample_results');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('qa_sample_results', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('qa_sample_id');
            $table->uuid('license_id')->index();
            $table->uuid('analysis_id');
            $table->json('results')->nullable();
            $table->string('analysis_type')->default('total_cannabinoid');
            $table->boolean('is_passed')->default(true);
            $table->uuid('created_by_id')->nullable();
            $table->uuid('updated_by_id')->nullable();

            $table->string('sync_code')->nullable()->index();
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('qa_sample_id')->references('id')->on('qa_samples');
            $table->foreign('analysis_id')->references('id')->on('lab_test_analyses');
        });
    }
}
