<?php

namespace Bamboo\StreamProcessing\Contracts;

use Throwable;

interface ConsumerEventProcessInterface
{
    /**
     * @param StreamEventInterface $streamEvent
     * @return void
     *
     * @throws Throwable
     */
    public function execute(StreamEventInterface $streamEvent): void;
}
