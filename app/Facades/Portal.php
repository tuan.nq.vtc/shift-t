<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Models\{License, User};
use Illuminate\Support\Collection;

/**
 * Class Portal
 * @package App\Facades
 *
 * @method static Collection allLicenses()
 * @method static array fetchAllLicenses()
 * @method static License|null fetchLicense()
 * @method static array allUsers()
 * @method static Collection fetchAllUsers
 * @method static User|null fetchUser()
 * @method static array allOrganizationUsers($id)
 * @method static array allAccountHolderUsers($id)
 *
 * @see \App\Services\PortalService
 */
class Portal extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'portal_service';
    }
}
