<?php

namespace App\Listeners;

use App\Events\SourceProduct\SourceProductSaved;
use App\Models\SourceProduct;
use App\Services\SourceProductService;
use Illuminate\Events\Dispatcher;

class SourceProductEventSubscriber
{
    /**
     * @param SourceProductSaved $event
     */
    public function handler($event)
    {
        $this->updateInventoryProductName($event->sourceProduct);
    }

    /**
     * @param SourceProduct $sourceProduct
     */
    private function updateInventoryProductName($sourceProduct)
    {
        if ($sourceProduct->isDirty(['name'])) {
            app(SourceProductService::class)->updateInventoryProductName($sourceProduct->id);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [SourceProductSaved::class],
            'App\Listeners\SourceProductEventSubscriber@handler'
        );
    }
}
