<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models\{Strain, TraceableModel};
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class Strain
 * @package App\Synchronizations\Http\Resources\Metrc
 *
 * @property Strain|TraceableModel $traceableModel
 */
class StrainResource extends BaseMetrcResource
{
    protected $path = '/strains/v1';

    /**
     * @return self
     */
    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = $this->path.'/active';

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function fetch(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = $this->path.'/'.$this->traceableModel->sync_code;

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function create(): self
    {
        // Validate indica & sativa percentage
        $this->checkSpicesPercentages('creation');
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/create';
        $this->requestBody = [
            [
                /*[
                    {
                        "Name": "Spring Hill Kush",
                        "TestingStatus": "None",
                        "ThcLevel": 0.1865,
                        "CbdLevel": 0.1075,
                        "IndicaPercentage": 25.0,
                        "SativaPercentage": 75.0
                    }
                ]*/
                'Name' => $this->traceableModel->name,
                'TestingStatus' => $this->traceableModel->info['testing_status'] ?? 'None',
                'ThcLevel' => $this->traceableModel->info['thc'] ?? null,
                'CbdLevel' => $this->traceableModel->info['cbd'] ?? null,
                'IndicaPercentage' => $this->traceableModel->info['indica'] ?? null,
                'SativaPercentage' => $this->traceableModel->info['sativa'] ?? null,
            ]
        ];

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function update(): self
    {
        if (!$this->traceableModel->sync_code) {
            $error = 'Unable to sync strain update without METRC Id [sync_code]';
            Log::error($error);
            throw new Exception($error);
        }
        $this->checkSpicesPercentages('update');
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/update';
        $this->requestQuery = ['licenseNumber' => $this->traceableModel->license->code];
        $this->requestBody = [
            /*[
                {
                    "Id": 1,
                    "Name": "Spring Hill Kush",
                    "TestingStatus": "InHouse",
                    "ThcLevel": 0.1865,
                    "CbdLevel": 0.1075,
                    "IndicaPercentage": 25.0,
                    "SativaPercentage": 75.0
                }
            ]*/
            [
                'Id' => $this->traceableModel->sync_code,
                'Name' => $this->traceableModel->name,
                'TestingStatus' => $this->traceableModel->info['testing_status'] ?? 'None',
                'ThcLevel' => $this->traceableModel->info['thc'] ?? null,
                'CbdLevel' => $this->traceableModel->info['cbd'] ?? null,
                'IndicaPercentage' => $this->traceableModel->info['indica'] ?? null,
                'SativaPercentage' => $this->traceableModel->info['sativa'] ?? null,
            ],
        ];

        return $this;
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function delete(): self
    {
        if (!$this->traceableModel->sync_code) {
            $error = 'Unable to sync strain deletion without METRC Id [sync_code]';
            Log::error($error);
            throw new Exception($error);
        }
        $this->httpMethod = Request::METHOD_DELETE;
        $this->requestUrl = $this->path.'/'.$this->traceableModel->sync_code;
        $this->requestQuery = ['licenseNumber' => $this->license->code];

        return $this;
    }

    /**
     * @param string $action
     *
     * @throws Exception
     */
    private function checkSpicesPercentages(string $action): void
    {
        // Validate indica & sativa percentage
        if (empty($this->traceableModel->info['indica']) || empty($this->traceableModel->info['sativa'])) {
            $error = 'Unable to sync strain '.$action.' without India or Sativa percentage';
            Log::error($error);
            throw new Exception($error);
        }
        if (100 != $this->traceableModel->info['indica'] + $this->traceableModel->info['sativa']) {
            $error = 'Unable to sync strain '.$action.' without total of India and Sativa percentages not equal to 100';
            Log::error($error);
            throw new Exception($error);
        }
    }
}
