<?php

namespace App\Facades;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Facade;

/**
 * Class FileUpload
 *
 * @package App\Facades
 *
 * @method static Model upload(Model $model, UploadedFile $file, string $accountHolderId, string $fieldName)
 * @method static void uploadImage(Model $model, UploadedFile $file, string $id, string $fieldName)
 * @method static Model remove(Model $model, string $fieldName)
 * @method static string generateResizedImage(string $imagePath, string $imageSize, array $config = [])
 * @method static bool existsResizedImage(string $imagePath, string $imageSize)
 * @method static bool deleteResizedImage(string $imagePath, string $imageSize)
 *
 * @see \App\Services\FileUploadService
 */
class FileUpload extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'file_upload';
    }
}
