<?php

namespace App\Listeners;

use App\Events\AdditiveInventory\AdditiveInventorySaved;
use App\Services\AdditiveService;
use Illuminate\Events\Dispatcher;

class AdditiveEventSubscriber
{
    /**
     * @var AdditiveService
     */
    private AdditiveService $additiveService;

    /**
     * AdditiveEventListener constructor.
     *
     * @param AdditiveService $additiveService
     */
    public function __construct(AdditiveService $additiveService)
    {
        $this->additiveService = $additiveService;
    }

    /**
     * @param AdditiveInventorySaved $event
     */
    public function onInventorySaved($event)
    {
        $this->additiveService->calculateAfterInventorySaved($event->additiveInventory);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            AdditiveInventorySaved::class,
            'App\Listeners\AdditiveEventSubscriber@onInventorySaved'
        );
    }
}
