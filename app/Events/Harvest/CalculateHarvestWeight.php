<?php

namespace App\Events\Harvest;

use App\Events\Event;

class CalculateHarvestWeight extends Event
{
    public array $harvests;

    public function __construct(array $harvests)
    {
        $this->harvests = $harvests;
    }
}
