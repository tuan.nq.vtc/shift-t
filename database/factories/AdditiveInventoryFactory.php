<?php

/** @var Factory $factory */

use App\Models\{Additive, AdditiveInventory, Supplier, User};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    AdditiveInventory::class,
    function (Faker $faker) {
        return [
            'additive_id' => function () {
                return factory(Additive::class)->create()->id;
            },
            'supplier_id' => function () {
                return factory(Supplier::class)->create()->id;
            },
            'unit_price' => $faker->numberBetween(1, 10),
            'total_quantity' => $faker->numberBetween(1, 10),
            'order_date' => $faker->date(),
            'uom' => AdditiveInventory::UOM_GALLON,
            'created_at' => $faker->dateTime(),
            'updated_at' => $faker->dateTime(),
            "created_by" => function () {
                return factory(User::class)->create()->id;
            },
        ];
    }
);
