<?php

namespace App\Models\Traits;

use Spatie\Sluggable\{HasSlug, SlugOptions};

trait HasSluggableName
{
    use HasSlug;

    public function getSlugOptions(): SlugOptions
    {
        $slugOption = SlugOptions::create()->generateSlugsFrom('name');
        $slugOption->generateUniqueSlugs = false;
        if ($this->getAttribute('slug')) {
            $slugOption->generateSlugsFrom('slug');
        }

        return $slugOption->saveSlugsTo('slug');
    }
}
