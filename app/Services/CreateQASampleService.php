<?php

namespace App\Services;

use App\Cqrs\Commands\CreateQASampleCommand;
use App\Cqrs\Commands\CreateQASampleHandler;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Throwable;

class CreateQASampleService
{
    const MSG_FAILED = 'Create qa sample failed';

    /**
     * @var CreateQASampleHandler
     */
    private $handler;

    /**
     * @param CreateQASampleHandler $handler
     */
    public function __construct(CreateQASampleHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param CreateQASampleCommand $command
     * @return JsonResponse
     */
    public function __invoke(CreateQASampleCommand $command): JsonResponse
    {
        try {
            return wpApiResponse($this->handler->handle($command), ResponseAlias::HTTP_CREATED);
        } catch (Throwable $e) {
            $msg = $this->handler->isRecordedErrors() ?
                $this->handler->getErrorBag() : [$e->getMessage()];
            return wpApiResponseWithErrors($msg, self::MSG_FAILED);
        }
    }
}
