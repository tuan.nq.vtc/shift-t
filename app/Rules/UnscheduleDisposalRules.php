<?php

namespace App\Rules;


use App\Models\Disposal;
use App\Models\GrowCycle;
use App\Models\Plant;
use App\Models\Propagation;
use App\Models\Subroom;
use App\Models\WasteBag;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class UnscheduleDisposalRules implements Rule
{

    private $licenseId;
    /**
     * @var string
     */
    private string $message = "System has some errors while validate request's data";

    public function __construct($licenseId)
    {
        $this->licenseId = $licenseId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $disposalData
     * @return bool
     */
    public function passes($attribute, $disposalData)
    {
        if (!is_array($disposalData)) {
            return $this->setErrorMessage('The disposal must be object format');
        }
        if (!isset($disposalData['id']) || !isset($disposalData['subroom_id'])) {
            return $this->setErrorMessage('The id and subroom_id fields is required');
        }
        $disposal = Disposal::query()
            ->where('license_id', $this->licenseId)
            ->where('status', Disposal::STATUS_SCHEDULED)
            ->find($disposalData['id']);
        if (!$disposal) {
            return $this->setErrorMessage('The disposal does not exist');
        }

        return $this->validateWasteType($disposal->wastable_type, $disposalData);
    }

    /**
     * @param $message
     * @return bool
     */
    private function setErrorMessage($message)
    {
        $this->message = $message;
        return false;
    }

    /**
     * @param $wasteType
     * @param $disposalData
     * @return bool
     */
    private function validateWasteType($wasteType, array $disposalData)
    {
        switch ($wasteType) {
            case WasteBag::class:
                return $this->setErrorMessage('Can not unschedule the waste bag object');
                break;
            case Plant::class:
                if (!isset($disposalData['grow_cycle_id'])) {
                    return $this->setErrorMessage("The grow_cycle_id field is required");
                }
                if (GrowCycle::query()->where('license_id', $this->licenseId)
                        ->where('id', $disposalData['grow_cycle_id'])->count() !== 1) {
                    return $this->setErrorMessage("The grow cycle does not exist");
                }
                return $this->validateSubroom(Arr::get($disposalData, 'subroom_id'));
                break;
            case Propagation::class:
                return $this->validateSubroom(Arr::get($disposalData, 'subroom_id'));
                break;
            default:
                return $this->setErrorMessage('The waste type is invalid');
        }
    }

    /**
     * @param $subroomId
     * @return bool
     */
    private function validateSubroom($subroomId)
    {
        $subroom = Subroom::query()->find($subroomId);
        if (!$subroom || $subroom->room->isDisposal()) {
            return $this->setErrorMessage('The subroom does not exist');
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}

