<?php

namespace App\Notification\Events;

use App\Events\Event;

class PushNotificationEvent extends Event
{
    public string $event;
    public string $message;
    public string $channel;

    /**
     * PushNotificationEvent constructor.
     *
     * @param string $event
     * @param string $message
     * @param string $channel
     */
    public function __construct(string $event, string $message, string $channel)
    {
        $this->event = $event;
        $this->message = $message;
        $this->channel = $channel;
    }

}
