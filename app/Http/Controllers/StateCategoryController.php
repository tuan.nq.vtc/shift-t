<?php

namespace App\Http\Controllers;

use App\Models\License;
use App\Services\LicenseService;
use App\Services\StateCategoryService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class StateCategoryController extends Controller
{
    /**
     * @var StateCategoryService
     */
    private StateCategoryService $stateCategoryService;

    /**
     * @var LicenseService
     */
    private LicenseService $licenseService;

    /**
     * @var Request
     */
    private Request $request;

    public function __construct(
        StateCategoryService $stateCategoryService,
        LicenseService $licenseService,
        Request $request
    ) {
        $this->stateCategoryService = $stateCategoryService;
        $this->licenseService = $licenseService;
        $this->request = $request;
    }

    /**
     * @param string $type
     * @return JsonResponse
     * @throws Exception
     */
    public function getByType(string $type)
    {
        /** @var License $license */
        return response()->json($this->stateCategoryService->getByType($type));
    }
}
