<?php

return [
    'sync_resources' => [
        'strains' => [
            'model' => \App\Models\Strain::class,
            'name' => 'Strains',
            'dependencies' => [],
        ],
        'rooms' => [
            'model' => \App\Models\Room::class,
            'name' => 'Rooms',
            'dependencies' => [],
        ],
        'inventory_types' => [
            'model' => \App\Models\InventoryType::class,
            'name' => 'Inventory Types',
            'filter_by_date' => true,
            'dependencies' => ['strains'],
        ],
        'propagations' => [
            'model' => \App\Models\Propagation::class,
            'name' => 'Propagations',
            'filter_by_date' => true,
            'dependencies' => ['strains', 'rooms'],
        ],
        'plant_groups' => [
            'model' => \App\Models\PlantGroup::class,
            'name' => 'Grow Cycles',
            'filter_by_date' => true,
            'dependencies' => ['strains', 'rooms'],
        ],
        'harvests' => [
            'model' => \App\Models\Harvest::class,
            'name' => 'Harvests',
            'filter_by_date' => true,
            'dependencies' => ['strains', 'rooms'],
        ],
        'plants' => [
            'model' => \App\Models\Plant::class,
            'name' => 'Plants',
            'filter_by_date' => true,
            'dependencies' => ['strains', 'rooms', 'plant_groups'],
        ],
        'disposals' => [
            'model' => \App\Models\Disposal::class,
            'name' => 'Disposals',
            'filter_by_date' => true,
            'dependencies' => ['strains', 'rooms',],
        ],
        'inventories' => [
            'model' => \App\Models\Inventory::class,
            'name' => 'Inventory',
            'filter_by_date' => true,
            'dependencies' => ['strains', 'rooms', 'inventory_types', 'propagations', 'plant_groups', 'harvests',],
        ],
    ],
    // Mapped by state => license type
    'types' => [
        # CA
        'CA' => [
            'Cannabis - Distributor License',
            'A-Type 6: Non Volatile Solvent Extraction',
            'M-Medium Mixed-Light Tier 2',
            'Cannabis - Microbusiness License',
            'A-Processor',
            'Cannabis - Retailer License',
            'Cannabis - Testing Laboratory License',
            'Cannabis - Distributor-Transport Only',
        ],
        # CO
        'CO' => [
            'MMC Type 1',
            'OPC',
            'Retail Store',
            'Retail Cultivation',
            'MMC Type 3',
            'MIP',
            'Retail MIP',
            'Retail Testing Lab',
        ],
        # WA
        'WA' => [
            'cultivator',
            'production',
            'cultivator_production',
            'dispensary',
            'tribe',
            'co-op',
        ],
    ],
    // na -> not available
    'resources' => [
        # CA
        'CA' => [
            'na' => [
                // Have no room: Distributor, A-Type 6: Non Volatile Solvent Extraction, A-Processor, Retailer, Test Laboratory
                'Cannabis - Distributor License' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'A-Type 6: Non Volatile Solvent Extraction' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'M-Medium Mixed-Light Tier 2' => [],
                'Cannabis - Microbusiness License' => [],
                'A-Processor' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'Cannabis - Retailer License' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'Cannabis - Testing Laboratory License' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'Cannabis - Distributor-Transport Only' => [],
            ]
        ],
        'CO' => [
            'na' => [
                'MMC Type 1',
                'OPC',
                'Retail Store' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'Retail Cultivation',
                'MMC Type 3' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'MIP' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'Retail MIP' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
                'Retail Testing Lab' => [
                    \App\Models\LicenseResource::METRC_LOCATION_RESOURCE
                ],
            ]
        ]
    ],
    // Sync date range
    'date_ranges' => [
        'one_month' => '1 month',
        'three_months' => '3 months',
        'six_months' => '6 months',
        'a_year' => '1 year',
    ],
];
