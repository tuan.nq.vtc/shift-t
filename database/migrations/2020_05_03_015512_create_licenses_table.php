<?php

use Illuminate\Database\{Migrations\Migration, Schema\Blueprint};
use Illuminate\Support\Facades\Schema;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'licenses',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('organization_id')->nullable();
                $table->uuid('account_holder_id');
                $table->string('name')->index();
                $table->string('type');
                $table->string('state_code',10);
                $table->string('code')->index();
                $table->unsignedTinyInteger('status')->default(1);
                $table->json('api_configuration')->nullable();
                $table->string('sync_date_range')->nullable();
                $table->tinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();
                $table->boolean('is_test')->default(false);

                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licenses');
    }
}
