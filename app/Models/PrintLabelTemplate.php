<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\AccountHolderFilterable;
use App\Models\Traits\Scope\NameFilterable;
use App\Models\Traits\Scope\StatusFilterable;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PrintLabelTemplate
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property array|null $box
 * @property int status
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class PrintLabelTemplate extends BaseModel
{
    use SoftDeletes;
    use NameFilterable;
    use StatusFilterable;
    use AccountHolderFilterable;
    use HasInternalId;
    
    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $table = 'print_label_templates';

    protected $fillable = [
        'account_holder_id',
        'name',
        'background',
        'box',
        'status',
    ];

    protected $casts = [
        'status' => 'bool',
        'box' => 'array',
    ];

     /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%'.$value.'%');
    }
}
