<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\SourceCategory;
use App\Models\StateCategory;
use App\Rules\AvailableModelRule;
use App\Services\SourceCategoryService;
use Illuminate\Support\Arr;
use Illuminate\Http\{JsonResponse, Request, Response};

class SourceCategoryController extends CRUDController
{
    public function __construct(SourceCategoryService $service, Request $request)
    {
        $request->merge(
            [
                'filter' => array_merge(
                    $request->get('filter', []),
                    $request->only(['account_holder_id'])
                )
            ]
        );
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:150',
                'unique:App\Models\SourceCategory,name,NULL,id,deleted_at,NULL,account_holder_id,' . $this->request->get('account_holder_id', NULL)
            ],
            'parent_id' => [
                'nullable',
                'uuid',
                'exists:App\Models\SourceCategory,id,deleted_at,NULL',
            ],
            'account_holder_id' => ['required', 'uuid'],
            'description' => [],
            'status' => ['sometimes', 'required', 'boolean'],
            'license_id' => [],
            'state_category_id' => ['sometimes', 'required', 'uuid', 'exists:App\Models\StateCategory,id,type,' . StateCategory::TYPE_SOURCE,],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => [
                'sometimes',
                'required',
                'string',
                'max:150',
                'unique:App\Models\SourceCategory,name,' . $id . ',id,deleted_at,NULL,account_holder_id,' . $this->request->get('account_holder_id', NULL)
            ],
            'parent_id' => [
                'sometimes',
                'nullable',
                'uuid',
                'exists:App\Models\SourceCategory,id,deleted_at,NULL',
            ],
            'description' => [],
            'status' => ['sometimes', 'required', 'boolean'],
            'license_id' => [],
            'state_category_id' => ['sometimes', 'required', 'uuid', 'exists:App\Models\StateCategory,id,type,' . StateCategory::TYPE_SOURCE,],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems(Criteria::createFromRequest($this->request)));
    }

    public function list()
    {
        $this->request->merge([
            'filter' => array_merge(
                ['parent_id' => SourceCategory::PARENT_ROOT_ID],
                $this->request->get('filter', [])
            ),
        ]);
        return parent::list();
    }

    public function deleteAndReassign()
    {
        $rules = [
            'from_category_id' => [
                'required',
                'uuid',
                'exists:App\Models\SourceCategory,id,deleted_at,NULL',
            ],
            'to_category_id' => [
                'sometimes',
                'nullable',
                'uuid',
                'exists:App\Models\SourceCategory,id,deleted_at,NULL',
            ]
        ];
        $data = $this->validate($this->request, $rules);
        $this->service->destroy(
            $this->service->getModel()->findOrFail($data['from_category_id']),
            $this->service->getModel()->find($data['to_category_id'] ?? null)
        );

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
