<?php

namespace App\Cqrs\Values;

use App\Cqrs\Base\Valuable;

class ClientIdentifier extends Valuable
{
    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @var string[]
     */
    protected $mapping = [
        'clientId|string' => 'clientId',
        'clientSecret|string' => 'clientSecret',
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }
}
