<?php

namespace App\Models;

use App\Models\Traits\Scope\LicenseFilterable;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany, MorphTo};

/**
 * Class AdditiveOperation
 *
 * @package App\Models
 */
class AdditiveOperation extends BaseModel
{
    use LicenseFilterable;

    public const PROPAGATION_OPERATION = 'propagation';
    public const GROW_CYCLE_OPERATION = 'grow-cycle';
    public const ROOM_OPERATION = 'room';
    public const PLANT_OPERATION = 'plant';

    public const ADDITIVE_TYPE_PESTICIDE  = 0;
    public const ADDITIVE_TYPE_NUTRIENT  = 1;

    public const DEFAULT_LIMIT = 20;

    protected $fillable = [
        'additive_type',
        'license_id',
        'additive_id',
        'additive_inventory_id',
        'applied_quantity',
        'target',
        'uom',
        'unit_price',
        'applied_date',
        'comment',
        'applied_by',
        'created_by',
        'updated_by',
        'supplier_id',
    ];

    protected $dates = ['applied_date', 'created_at', 'updated_at',];

    public function additive(): BelongsTo
    {
        return $this->belongsTo(Additive::class, 'additive_id', 'id');
    }

    public function inventory(): BelongsTo
    {
        return $this->belongsTo(AdditiveInventory::class, 'additive_inventory_id', 'id');
    }

    public function additiveLogs(): HasMany
    {
        return $this->hasMany(AdditiveLog::class, 'additive_operation_id', 'id');
    }

    public function target(): MorphTo
    {
        return $this->morphTo();
    }

    public function applicator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'applied_by', 'id');
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

}
