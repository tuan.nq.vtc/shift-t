<?php

namespace App\Integrations\FileGenerators;

use App\Integrations\FileGenerators\Exceptions\FileGeneratorException;
use App\Integrations\FileGenerators\Resolvers\DataProviderResolver;
use App\Integrations\FileGenerators\Resolvers\FileStrategyResolver;
use App\Models\License;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Psr\Log\LoggerInterface;

class FileGenerator
{
    private LoggerInterface $logger;
    private const MAX_RECORDS_PER_FILE = 5000;
    private array $failedResources = [];
    private License $license;
    public const PENDING_INTEGRATIONS_PATH = "integrations/";
    public const SUCCESS_INTEGRATIONS_PATH = "integrations/archives/";

    public function __construct(License $license)
    {
        $this->logger = Log::channel('file_generator');
        $this->license = $license;
    }

    private function generateResourceFiles(string $resourceClassName): bool
    {
        $fileStrategy = FileStrategyResolver::resolve($this->license, $resourceClassName);
        $dataProvider = DataProviderResolver::resolve($this->license, $resourceClassName);

        $dataProvider->setMaxRecordsPerFile(self::MAX_RECORDS_PER_FILE);
        $dataProvider->prepareData();

        $data = $dataProvider->getData();

        if (count($data) == 0) {
            $this->logger->info(
                'No records found for License ID: ' . $this->license->id . ' | Resource: ' . $resourceClassName
            );
            return false;
        }

        do {
            try {
                $dataProvider->setRecordsInProcess();
                $fileStrategy->setData($data);

                $filename = self::PENDING_INTEGRATIONS_PATH . $fileStrategy->getFilename();

                Storage::disk()->put(
                    $filename,
                    $fileStrategy->getContent(),
                    [
                        'mimetype' => $fileStrategy->getContentType()
                    ]
                );

                $this->logger->info($filename . ' file generated.');

                $dataProvider->setRecordsCompleted($filename);

                $data = $dataProvider->getData();
            } catch (Exception $e) {
                $dataProvider->setRecordsFailed();
                $this->failedResources[] = $resourceClassName;
                $this->logger->error($e);
                return false;
            }
        } while (count($data) > 0);

        return true;
    }

    public function generate(): void
    {
        $resources = $this->license->getStateResources();

        if (empty($resources)) {
            throw FileGeneratorException::sendNoResourcesFound();
        }

        $this->logger->info('Generating files for License ID: ' . $this->license->id);

        foreach ($resources as $resource) {
            if (count(array_intersect($this->failedResources, $resource['dependencies'])) == 0) {
                $this->logger->info(
                    'Generating files for License ID: ' . $this->license->id . ' | Resource: ' . $resource['resourceClassName']
                );

                $result = $this->generateResourceFiles($resource['resourceClassName']);
                if ($result) {
                    $this->logger->info(
                        'Successful files Generation for License ID: ' . $this->license->id . ' | Resource: ' . $resource['resourceClassName']
                    );
                }
            } else {
                $this->logger->error(
                    'Generating files for License ID: ' . $this->license->id . ' | Resource: ' . $resource['resourceClassName'] . ' FAILED, one or more of its dependencies has failed.'
                );
            }
        }
    }
}
