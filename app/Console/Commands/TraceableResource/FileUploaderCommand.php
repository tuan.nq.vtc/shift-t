<?php

namespace App\Console\Commands\TraceableResource;

use App\Facades\StateFileUploader;
use App\Models\State;
use App\Services\FileGeneratorService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class FileUploaderCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'upload files to state for traceable resources';
    /**
     * @var string
     */
    protected $signature = 'trace:file-uploader {--state= : The state file(s) to be uploaded}';

    public function handle()
    {
        $state = $this->getState();

        if (is_null($state) || !$state->code) {
            $this->error('Invalid state code.');
            exit;
        }

        if (!$state->hasAsyncIntegration()) {
            $this->error('It is not possible to use a state without async integration');
            exit;
        }      

        $this->info('Uploading files for licenses with state: ' . $state->code);
        $fileGeneratorService = new FileGeneratorService();

        try {
            $licenseGroups = $fileGeneratorService->getFilesToUploadByState($state->code);

            foreach($licenseGroups as $licenseCode => $licenseGroup) {
                $this->info('Uploading files for licenses code ' . $licenseCode);
                try {
                    $result = StateFileUploader::upload($state->code, $licenseGroup);
                } catch (Exception $e) {
                    $this->error('Error uploading files for licenses code ' . $licenseCode . ': ' . $e->getMessage());
                    continue;
                }

                $this->info('Successful response uploading files for licenses code ' . $licenseCode . ': ' . $result);
                foreach($licenseGroup as $dependencyGroup) {
                    $fileGeneratorService->setFilesUploadStatus($state->code, FileGeneratorService::FILE_UPLOAD_IN_PROCESS_STATUS, $dependencyGroup);
                }

                sleep(1);
            }
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }        
    }

    protected function getState(): ?State
    {
        if ($stateCode = $this->option('state')) {
            $stateCode = Str::upper($stateCode);
            return State::where('code', $stateCode)->first();
        }

        return null;
    }
}
