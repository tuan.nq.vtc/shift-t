<?php

use App\Models\State;

return [
    State::REGULATOR_LEAF => [
        'resources' => require('regulators/leaf/resources.php'),
        'logic' => require('regulators/leaf/logic.php'),
    ],
    State::REGULATOR_METRC => [
        'resources' => require('regulators/metrc/resources.php'),
        'logic' => require('regulators/metrc/logic.php'),
    ],
    State::REGULATOR_CCRS => [
        'resources' => require('regulators/ccrs/resources.php'),
    ],
];
