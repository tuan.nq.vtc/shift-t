<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\HarvestPlant
 *
 * @property-read \App\Models\Harvest $harvest
 * @property-read \App\Models\Plant $plant
 * @method static Builder |BaseModel defaultOrderBy()
 * @method static Builder|HarvestPlant newModelQuery()
 * @method static Builder|HarvestPlant newQuery()
 * @method static Builder|HarvestPlant query()
 * @mixin \Eloquent
 */
class HarvestPlant extends BaseModel
{
    protected $fillable = [
        'harvest_id',
        'plant_id',
    ];

    public function harvest(): BelongsTo
    {
        return $this->belongsTo(Harvest::class);
    }

    public function plant(): BelongsTo
    {
        return $this->belongsTo(Plant::class);
    }
}
