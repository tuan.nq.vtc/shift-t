<?php

namespace App\Synchronizations\Http\Clients;

use App\Models\License;
use Exception;

class MetrcClient extends BaseClient
{
    public function __construct(License $license)
    {
        parent::__construct($license);

        $apiConfig = throw_unless($license->api_configuration, new Exception('API not configured'));
        $vendorKey = throw_unless($apiConfig['vendor_key']?? null, new Exception('Metrc Vendor key not configured'));
        $userKey = throw_unless($apiConfig['user_key']?? null, new Exception('Metrc User key not configured'));

        if ('local' === app()->environment()) {
            $vendorKey = env('CO_METRC_VENDOR_KEY');
            $userKey = env('CO_METRC_USER_KEY');
        }
        $this->request->withBasicAuth($vendorKey, $userKey);
    }
}
