<?php

namespace App\Http\Controllers;

use App\Services\UserNotificationService;
use Illuminate\Http\{Request};
use Laravel\Lumen\Routing\Controller;

/**
 * Class UserNotificationController
 *
 * @package App\Http\Controllers
 */
class UserNotificationController extends Controller
{
    /**
     * @var UserNotificationService
     */
    protected UserNotificationService $service;

    /**
     * @var Request
     */
    protected Request $request;

    public function __construct(UserNotificationService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    public function getUserNotifications()
    {
        return response()->json(
            $this->service->getUserNotifications($this->request->get('user_id'), $this->request->get('license_id'))
        );
    }

    public function markAsRead()
    {
        return response()->json(
            $this->service->markAsRead(
                $this->request->get('user_id'),
                $this->validate(
                    $this->request,
                    [
                        'user_notification_ids' => ['required', 'array'],
                        'user_notification_ids.*' => ['required', "exists:App\Models\UserNotification,id"],
                    ]
                )
            )
        );
    }

}
