<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\Inventory;
use App\Models\License;
use App\Models\QASample;
use App\Models\QASampleResult;
use App\Models\Trace;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Events\SyncPullCompletedEvent;
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Http;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Streamable;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

class QASampleResultTest  extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;
    use Streamable;

    /** @test */
    public function it_can_pull_from_leaf_api() {

        $this->withoutTraceableEvents();
        $this->withoutStreamableEvents();
        Event::fake([SyncPullCompletedEvent::class]);
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $inventory = factory(Inventory::class)->create(['sync_code' => 'WAG010101.IN2D0B', 'license_id' => $licenseId]);
        factory(QASample::class)->create([
            'inventory_id' => $inventory->getAttribute('id'),
            'status' => QASample::STATUS_AWAITING_RESULT,
        ]);

        $currentPage = rand(1, 10);
        $totalRecord = rand(10, 20);

        $data = function ($totalRecord) {
            $data = [];

            for ($j = 1; $j <= $totalRecord; $j++) {
                $id = "WAJ1965.IN" . $this->faker->regexify('[A-Z0-9]{4}');

                $data[] = [
                    'global_for_inventory_id' => $id,
                    'global_id' => $id,
                    'mme_id' => rand(3000, 4000),
                    'status' => $this->faker->randomElement(['passed', 'failed']),
                ];
            }

            return $data;
        };

        $responseData = [
            "total" =>  $totalRecord,
            "per_page" => 2500,
            "current_page" => $currentPage,
            'data' => $data($totalRecord)
        ];

        Http::fake(
            [
                '/lab_results' => Http::response($responseData, 200),
            ]
        );

        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'action' => TraceableModelInterface::SYNC_ACTION_LIST,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => QASampleResult::class,
            ]
        );

        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
    }
}
