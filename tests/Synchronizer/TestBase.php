<?php

namespace Tests\Synchronizer;

use Tests\Concerns\{Streamable, Traceable, WithFaker};
use Tests\TestCase;

/**
 * @package Tests\Synchronizer
 */
class TestBase extends TestCase
{
    use WithFaker;
    use Traceable;
    use Streamable;

    /**
     * Set up the test.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->withoutStreamableEvents();
    }

    /**
     * @var String $prefix
     * @return String
     */
    protected function generateGlobalId(string $prefix): string
    {
        return $prefix . $this->faker->regexify('[A-Z0-9]{4}');
    }
}
