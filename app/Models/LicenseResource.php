<?php

namespace App\Models;

use App\Jobs\FetchLicenseResource;
use App\Synchronizations\Contracts\Http\ResourceInterface;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Models\Traits\Streamable;
use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Throwable;

class LicenseResource extends BaseModel implements StreamableResourceInterface
{
    use Streamable;

    public const SYNC_PENDING = 0;
    public const SYNCING = 1;
    public const SYNC_SUCCEED = 2;
    public const SYNC_FAILED = 3;

    public const LEAF_AREA_RESOURCE = 'leaf.area';
    public const LEAF_STRAIN_RESOURCE = 'leaf.strain';
    public const LEAF_BATCH_RESOURCE = 'leaf.batch';
    public const LEAF_PLANT_RESOURCE = 'leaf.plant';
    public const LEAF_INVENTORY_TYPE_RESOURCE = 'leaf.inventory_type';
    public const LEAF_INVENTORY_LOT_RESOURCE = 'leaf.inventory_lot';
    public const LEAF_INVENTORY_TRANSFER_RESOURCE = 'leaf.inventory_transfer';
    public const LEAF_LAB_RESULT_RESOURCE = 'leaf.lab_result';
    public const LEAF_WASTE_RESOURCE = 'leaf.waste';
    public const LEAF_SALES_RESOURCE = 'leaf.sales';

    public const METRC_LOCATION_RESOURCE = 'metrc.location';
    public const METRC_STRAIN_RESOURCE = 'metrc.strain';
    public const METRC_INVENTORY_ITEM_RESOURCE = 'metrc.inventory_item';
    public const METRC_INVENTORY_PACKAGE_RESOURCE = 'metrc.inventory_package';
    public const METRC_INVENTORY_TRANSFER_RESOURCE = 'metrc.inventory_transfer';
    public const METRC_LAB_RESULT_RESOURCE = 'metrc.lab_result';
    public const METRC_PLANTING_RESOURCE = 'metrc.planting';
    public const METRC_PLANT_RESOURCE = 'metrc.plant';
    public const METRC_WASTE_RESOURCE = 'metrc.waste';
    public const METRC_SALES_RESOURCE = 'metrc.sales';

    public const RESOURCE_CHAIN_BY_REGULATOR = [
        State::REGULATOR_LEAF => [
            self::LEAF_AREA_RESOURCE,
            self::LEAF_STRAIN_RESOURCE,
            self::LEAF_INVENTORY_TYPE_RESOURCE,
            self::LEAF_BATCH_RESOURCE,
            self::LEAF_PLANT_RESOURCE,
            self::LEAF_INVENTORY_LOT_RESOURCE,
            self::LEAF_WASTE_RESOURCE,
            self::LEAF_LAB_RESULT_RESOURCE,
            self::LEAF_INVENTORY_TRANSFER_RESOURCE,
        ],
        State::REGULATOR_METRC => [
            self::METRC_LOCATION_RESOURCE,
            self::METRC_STRAIN_RESOURCE,
            self::METRC_INVENTORY_ITEM_RESOURCE,
            self::METRC_PLANTING_RESOURCE,
            self::METRC_PLANT_RESOURCE,
            self::METRC_INVENTORY_PACKAGE_RESOURCE,
            self::METRC_WASTE_RESOURCE,
            self::METRC_LAB_RESULT_RESOURCE,
            self::METRC_INVENTORY_TRANSFER_RESOURCE,
        ],
    ];

    protected $fillable = [
        'license_id',
        'resource',
        'records',
        'status',
        'syncable',
    ];

    protected $casts = [
        'records' => 'int',
        'status' => 'int',
        'syncable' => 'bool',
    ];

    public function getFetchingResourceJobs(): array
    {
        $endpoints = $this->getEndpoints();
        if (empty($endpoints)) {
            return [new FetchLicenseResource($this)];
        }
        $jobs = [];
        foreach ($this->getEndpoints() as $endpoint) {
            $jobs[] = new FetchLicenseResource($this, $endpoint);
        }

        return $jobs;
    }

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function getRegulatorResources(string $regulator): ?array
    {
        switch ($regulator) {
            case State::REGULATOR_LEAF:
                return [
                    self::LEAF_AREA_RESOURCE,
                    self::LEAF_STRAIN_RESOURCE,
                    self::LEAF_BATCH_RESOURCE,
                    self::LEAF_LAB_RESULT_RESOURCE,
                    self::LEAF_INVENTORY_TYPE_RESOURCE,
                    self::LEAF_INVENTORY_LOT_RESOURCE,
                    self::LEAF_INVENTORY_TRANSFER_RESOURCE,
                    self::LEAF_WASTE_RESOURCE,
                    self::LEAF_PLANT_RESOURCE,
                    self::LEAF_SALES_RESOURCE,
                ];

            case State::REGULATOR_METRC:
                return [
                    self::METRC_LOCATION_RESOURCE,
                    self::METRC_STRAIN_RESOURCE,
                    self::METRC_LAB_RESULT_RESOURCE,
                    self::METRC_INVENTORY_ITEM_RESOURCE,
                    self::METRC_INVENTORY_PACKAGE_RESOURCE,
                    self::METRC_INVENTORY_TRANSFER_RESOURCE,
                    self::METRC_WASTE_RESOURCE,
                    self::METRC_PLANT_RESOURCE,
                    self::METRC_PLANTING_RESOURCE,
                    self::METRC_SALES_RESOURCE,
                ];
            default:
                return null;
        }
    }

    public function getModelClass(): ?string
    {
        $licenseResources = config('traceability.license_resources');
        if (!isset($licenseResources[$this->resource])) {
            return null;
        }

        return $licenseResources[$this->resource]['model'] ?? null;
    }

    public function getEndpoints(): ?array
    {
        $licenseResources = config('traceability.license_resources');
        if (!isset($licenseResources[$this->resource])) {
            return null;
        }

        return $licenseResources[$this->resource]['endpoints'] ?? null;
    }

    public function getResourceClass(): ?string
    {
        $modelClass = $this->getModelClass();
        $stateResources = $this->license->getStateResources();
        if (!$stateResources) {
            return null;
        }

        return $stateResources[$modelClass] ?? null;
    }

    /**
     * @return TraceableModel
     *
     * @throws Throwable
     */
    public function initModel(): TraceableModel
    {
        $modelClass = throw_unless(
            $this->getModelClass(),
            new Exception(
                'Not found any model class configured with license resource ['.$this->resource.']'
            )
        );

        $model = new $modelClass();
        $model->setRelation('license', $this->license);

        return $model;
    }

    /**
     * @param TraceableModel $model
     *
     * @return ResourceInterface
     *
     * @throws Throwable
     */
    public function initResource(TraceableModel $model): ResourceInterface
    {
        $resourceClass = throw_unless(
            $this->getResourceClass(),
            new Exception(
                'Not found any resource class configured with license resource ['.$this->resource.']'
            )
        );

        return new $resourceClass($model, ResourceInterface::ACTION_LIST);
    }

    /**
     * @return string
     *
     * @throws Throwable
     */
    public function loadResourceClass(): string
    {
        return throw_unless(
            $this->getResourceClass(),
            new Exception(
                'Not found any resource class configured with license resource ['.$this->resource.']'
            )
        );
    }
}
