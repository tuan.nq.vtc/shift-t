<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Exceptions\GeneralException;
use App\Models\{Harvest, TraceableModel};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Exception;
use Illuminate\Support\Facades\{DB, Log};
use Throwable;

/**
 * Class HarvestProcess
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 * @property Harvest|TraceableModel $traceableModel
 */
class HarvestProcess extends BaseProcess
{
    /**
     * @throws Throwable
     */
    public function process()
    {
        try {
            DB::beginTransaction();
            Log::info("process: created harvest done");
            //$this->syncInventoryType();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        } catch (Throwable $throwable) {
            throw $throwable;
        }
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    private function syncHarvest(): self
    {
        $fetchListInventoryTypeResponse = $this->client->getRequest()->get(
            self::INVENTORY_ITEM_ENDPOINT . 'active?licenseNumber=' . $this->traceableModel->license->code
        );

        if ($fetchListInventoryTypeResponse->clientError() || $fetchListInventoryTypeResponse->serverError()) {
            throw new GeneralException('Unable to fetch inventory list from METRC regulator');
        }

        $metrcInventoryTypes = $fetchListInventoryTypeResponse->json();

        foreach ($metrcInventoryTypes as $item) {
            if ($item['Name'] === $this->traceableModel->name) {
                $this->traceableModel->update(
                    ['sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED, 'sync_code' => $item['Id']],
                );
                break;
            }
        }

        return $this;
    }
}
