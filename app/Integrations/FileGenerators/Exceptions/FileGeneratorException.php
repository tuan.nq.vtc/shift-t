<?php

namespace App\Integrations\FileGenerators\Exceptions;

use Exception;

class FileGeneratorException extends Exception
{
    public static function sendNoRecordsFound()
    {
        return new static("No records found");
    }

    public static function sendNoResourcesFound()
    {
        return new static("No resources found");
    }
}
