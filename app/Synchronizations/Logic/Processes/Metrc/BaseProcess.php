<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Logic\Processes\BaseProcess as LogicBaseProcess;
use Exception;
use Illuminate\Support\Carbon;

/**
 * @package App\Synchronizations\Logic\Processes\Metrc
 */
class BaseProcess extends LogicBaseProcess
{
    public const LOCATION_ENDPOINT = '/locations/v1/';
    public const STRAIN_ENDPOINT = '/strains/v1/';
    public const INVENTORY_ITEM_ENDPOINT = '/items/v1/';

    /**
     * @throws Exception
     */
    protected function create()
    {
        $this->traceableModel->update(
            [
                'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                'synced_at' => Carbon::now()
            ]
        );
    }

    /**
     * @throws Exception
     */
    protected function update()
    {
        $this->traceableModel->update(
            [
                'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                'synced_at' => Carbon::now()
            ]
        );
    }
}
