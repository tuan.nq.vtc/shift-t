<?php

/** @var Factory $factory */

use App\Models\QASampleResult;
use App\Models\QASampleCoa;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    QASampleCoa::class,
    function (Faker $faker) {
        return [
            'qa_sample_id' => factory(QASampleResult::class)->create()->id,
            'filename' => sprintf("Certificate of Analysis %s.pdf", $faker->numberBetween(2000, 4000)),
            'uploaded_at' => $faker->dateTime,
        ];
    }
);
