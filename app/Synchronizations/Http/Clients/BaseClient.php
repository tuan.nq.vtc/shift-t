<?php

namespace App\Synchronizations\Http\Clients;

use App\Models\License;
use App\Synchronizations\Contracts\Http\ClientInterface;
use App\Synchronizations\Contracts\Http\ResourceInterface;
use Exception;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

abstract class BaseClient implements ClientInterface
{
    protected PendingRequest $request;
    private LoggerInterface $logger;

    public function __construct(License $license)
    {
        $stateBaseUrl = throw_unless(
            $license->getStateBaseUrl(),
            new Exception('State ['.$license->state_code.'] API [endpoint] not configured')
        );
        $this->request = Http::baseUrl($stateBaseUrl)
            # TODO: Remove option verify
            ->withoutVerifying()
            ->asJson()
            ->accept('*/*');
        $this->logger = Log::channel('sync');
    }

    public function getRequest(): PendingRequest
    {
        return $this->request;
    }

    /**
     * @param ResourceInterface $httpResource
     * @return array
     * @throws RequestException
     */
    public function doRequest(ResourceInterface $httpResource): ?array
    {
        $response = $this->request->send(
            $httpResource->getMethod(),
            $httpResource->getUrl(),
            [
                'query' => $httpResource->getQuery(),
                'json' => $httpResource->getBody(),
            ]
        );

        try {
            $response->throw();
        } catch (Exception $e) {
            $this->logger->info(
                "Failed Request Information",
                [
                    'http_method' => $httpResource->getMethod(),
                    'request_url' => $httpResource->getUrl(),
                    'request_query' => $httpResource->getQuery(),
                    'request_body' => $httpResource->getBody(),
                ]
            );
            if (!empty($response->json())) {
                $this->logger->error($e->getMessage(), $response->json());
            }
            throw $e;
        }

        return $response->json();
    }
}
