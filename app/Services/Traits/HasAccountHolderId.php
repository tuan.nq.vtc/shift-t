<?php

namespace App\Services\Traits;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait HasAccountHolderId
{
    protected string $accountHolderId = '';

    public function setAccountHolderId($accountHolderId): self
    {
        $this->accountHolderId = $accountHolderId;
        return $this;
    }

    public function getAccountHolderId(): string
    {
        return $this->accountHolderId;
    }

    /**
     * @param bool $withoutGlobalOrder
     * @param bool $withLicenseFilter
     *
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    protected function newQuery($withoutGlobalOrder = true, $withLicenseFilter = true)
    {
        $query = $this->model;
        if ($withoutGlobalOrder) {
            $query->withoutGlobalScopes([BaseModel::DEFAULT_ORDER_SCOPE]);
        }
        
        $scopes = ['accountHolderId' => $this->getAccountHolderId()];

        if ($withLicenseFilter) {
            $scopes['licenseId'] = $this->licenseId;
        }

        return $query->scopes($this->loadScopes($scopes));
    }

    /**
     * @param array $data
     * @return Model
     * @throws \App\Exceptions\GeneralException
     */
    public function create(array $data): Model
    {
        $data['account_holder_id'] = $this->getAccountHolderId();
        return parent::create($data);
    }
}
