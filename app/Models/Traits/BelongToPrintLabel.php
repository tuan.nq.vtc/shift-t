<?php

namespace App\Models\Traits;

use App\Models\PrintLabel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongToPrintLabel
{
    public function printLabel(): BelongsTo
    {
        return $this->belongsTo(PrintLabel::class);
    }

    public function scopePrintLabelId(Builder $query, ...$printLabelIds): Builder
    {
        return $query->whereIn($this->getTable().'.print_label_id', $printLabelIds);
    }

    public function scopeOrderByPrintLabel(Builder $query, $direction): Builder
    {
        $printLabelTable = $this->printLabel()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($printLabelTable, $this->getTable().'.print_label_id', '=', $printLabelTable.'.id')
            ->orderBy($printLabelTable.'.name', $direction);
    }
}
