<?php

namespace App\Models\Traits\Scope;

trait SlugFilterable
{
    /**
     * Scope a query to only include records have slug matches with $slug.
     *
     * @param $query
     * @param $slug
     *
     * @return mixed
     */
    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }
}
