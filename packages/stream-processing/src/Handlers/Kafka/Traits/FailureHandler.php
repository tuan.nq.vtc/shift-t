<?php

namespace Bamboo\StreamProcessing\Handlers\Kafka\Traits;

use App\Models\FailedStream;
use Illuminate\Database\Eloquent\Model;
use Interop\Queue\{Message, Destination};
use Throwable;

trait FailureHandler
{
    /**
     * @param int $handleType
     * @param Destination $topic
     * @param Message $message
     * @param Throwable $exception
     *
     * @return FailedStream|Model
     */
    protected function handleFailedMessage(int $handleType, Destination $topic, Message $message, Throwable $exception)
    {
        return FailedStream::create(
            [
                'platform' => 'kafka',
                'body' => $message->getBody(),
                'properties' => $message->getProperties(),
                'headers' => $message->getHeaders(),
                'topic' => $topic->getTopicName(),
                'exception' => $exception->getTraceAsString(),
                'handle'=>$handleType,
            ]
        );
    }
}
