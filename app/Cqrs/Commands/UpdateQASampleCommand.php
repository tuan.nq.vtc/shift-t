<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Executable;
use App\Cqrs\Values\QASample;
use Illuminate\Support\Arr;

class UpdateQASampleCommand extends Executable
{
    /**
     * @var
     */
    protected $id;

    /**
     * @var string
     */
    protected $userId;


    protected $item;

    /**
     * @var string[]
     */
    protected $mapping = [
        'id' => 'id',
        'userId' => 'user_id'
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
        $data['results'] = Arr::only($data, ['thc', 'thca', 'cbd','cbda', 'total_thc','total_cbd']);
        $this->item = new QASample($data);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'qaSampleId' => $this->id,
            'userId' => $this->userId,
            'item' => $this->item
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return QASample
     */
    public function getItem(): QASample
    {
        return $this->item;
    }
}
