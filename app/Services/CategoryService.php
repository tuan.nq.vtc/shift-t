<?php

namespace App\Services;

use App\Models\Category;

/**
 * @package App\Services
 */
class CategoryService extends BaseService
{
    public array $relations = ['children'];

    public function __construct(Category $category)
    {
        parent::__construct($category);
    }
}
