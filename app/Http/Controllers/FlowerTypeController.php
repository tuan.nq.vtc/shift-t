<?php

namespace App\Http\Controllers;

use App\Services\FlowerTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class FlowerTypeController
 * @package App\Http\Controllers
 */
class FlowerTypeController extends CRUDController
{
    public function __construct(FlowerTypeService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:150', 'unique:flower_types'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'min:3', 'max:150', 'unique:flower_types,name,' . $id,],
            'status' => ['sometimes', 'required', 'boolean'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems());
    }
}
