<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use Exception;
use Illuminate\Http\Request;

/**
 * Class PropagationResource
 * @package App\Synchronizations\Http\Resources\Leaf
 */
class PropagationResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_BATCH;

    protected function fetchAll(): self
    {
        parent::fetchAll();
        $this->requestQuery['f_type'] = LeafConstant::BATCH_TYPE_PROPAGATION;
        $this->requestQuery['f_status'] = LeafConstant::BATCH_STATUS_OPEN;
        return $this;
    }

    protected function list(): BaseLeafResource
    {
        $this->requestQuery['f_type'] = LeafConstant::BATCH_TYPE_PROPAGATION;
        return parent::list();
    }

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function create(): self
    {
        parent::create();
        $propagation = $this->traceableModel;
        $errorFormat = 'Unable to sync propagation creation %s';
        $context = ['propagation' => $propagation];
        if (!$propagation->source_type) {
            $error = sprintf($errorFormat, 'without source type [name]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!($room = $propagation->room)) {
            $error = sprintf($errorFormat, 'with non-existed room');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!$room->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!($strain = $propagation->strain)) {
            $error = sprintf($errorFormat, 'with non-existed strain');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!$strain->sync_code) {
            $error = sprintf($errorFormat, 'without strain LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }

        $this->requestBody = [
            'batch' => [
                [
                    'type' => LeafConstant::BATCH_TYPE_PROPAGATION,
                    'external_id' => $propagation->id,
                    'origin' => $propagation->source_type,
                    'global_area_id' => $room->sync_code,
                    'global_strain_id' => $strain->sync_code,
                    'global_mother_plant_id' => $propagation->mother_code,
                    'num_plants' => $propagation->quantity,
                ],
            ],
        ];

        return $this;
    }


    /**
     * @return self
     *
     * @throws Exception
     */
    protected function update(): self
    {
        $propagation = $this->traceableModel->source;
        $errorFormat = 'Unable to sync propagation update without %s';
        if (!$this->traceableModel->sync_code) {
            $error = sprintf($errorFormat, 'batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, ['propagation' => $propagation]);
            throw new Exception($error);
        }
        if (!$propagation->source_type) {
            $error = sprintf($errorFormat, 'source type [name]');
            $this->logger->error($error, ['propagation' => $propagation]);
            throw new Exception($error);
        }
        if (!$propagation->subroom->room->sync_code) {
            $error = sprintf($errorFormat, 'room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, ['propagation' => $propagation]);
            throw new Exception($error);
        }
        if (!$propagation->strain->sync_code) {
            $error = sprintf($errorFormat, 'strain LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, ['propagation' => $propagation]);
            throw new Exception($error);
        }

        //#todo
        $this->batchResource->setMethod(Request::METHOD_POST)
            ->setUrl(LeafConstant::ENDPOINT_BATCH . '/update')
            ->setBody(
                [
                    'batch' => [
                        [
                            'global_id' => $this->traceableModel->sync_code,
                            'origin' => $propagation->source_type,
                            'global_area_id' => $propagation->subroom->room->sync_code,
                            'global_strain_id' => $propagation->strain->sync_code,
                            'num_plants' => $propagation->quantity,
                        ],
                    ],
                ]
            );

        return $this;
    }

    /**
     * @throws Exception
     */
    protected function delete(): self
    {
        $propagation = $this->traceableModel->source;
        $errorFormat = 'Unable to sync propagation deletion without %s';
        if (!$this->traceableModel->sync_code) {
            $error = sprintf($errorFormat, 'batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, ['propagation' => $propagation]);
            throw new Exception($error);
        }

        //#todo
        $this->batchResource->setMethod(Request::METHOD_DELETE)
            ->setUrl(LeafConstant::ENDPOINT_BATCH . '/' . $this->traceableModel->sync_code);

        return $this;
    }
}
