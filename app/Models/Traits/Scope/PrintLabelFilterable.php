<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait PrintLabelFilterable
{
    /**
     * @param Builder $query
     * @param string $category
     * @return Builder
     */
    public function scopeCategory($query, string $category)
    {
        return $query->where('category', $category);
    }

    /**
     * @param Builder $query
     * @param int $dimensionWidth
     * @return Builder
     */
    public function scopeDimension($query, int $dimensionWidth)
    {
        return $query->where('box->width', $dimensionWidth);
    }
}
