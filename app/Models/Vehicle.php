<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{LicenseFilterable, NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vehicle
 * @package App\Models
 */
class Vehicle extends BaseModel
{
    use SoftDeletes;
    use LicenseFilterable;
    use NameFilterable;
    use StatusFilterable;
    use HasInternalId;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $fillable = [
        'license_id',
        'name',
        'color',
        'make',
        'model',
        'year',
        'plate',
        'vin',
        'status',
    ];

    /**
     * @param  Builder  $query
     * @param  string  $value
     *
     * @return Builder
     */
    public function scopeMake($query, $value)
    {
        return $query->where('make', 'LIKE', "%{$value}%");
    }

    /**
     * @param  Builder  $query
     * @param  string  $value
     *
     * @return Builder
     */
    public function scopeModel($query, $value)
    {
        return $query->where('model', 'LIKE', "%{$value}%");
    }

    public function scopeYear($query, $value)
    {
        return $query->where('year', 'LIKE', "%{$value}%");
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%'.$value.'%');
    }
}
