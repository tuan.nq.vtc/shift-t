<?php


namespace App\Rules;


use App\Models\{HarvestGroup, Plant};
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class ValidateHarvestWeight implements Rule
{
    private string $licenseId;
    private HarvestGroup $groupHarvest;
    private string $message;

    public function __construct(string $licenseId, HarvestGroup $groupHarvest)
    {
        $this->groupHarvest = $groupHarvest;
        $this->licenseId = $licenseId;
    }

    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            return $this->setErrorMessage(
                __('validation.format', ['attribute' => 'harvest_items', 'format' => 'array'])
            );
        }

        return true;
    }

    private function setErrorMessage(string $message)
    {
        $this->message = $message;
        return false;
    }

    public function message()
    {
        return $this->message;
    }
}
