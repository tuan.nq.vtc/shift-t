<?php

namespace App\Models;

use App\Models\Traits\Scope\{NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 * @package App\Models
 */
class Client extends BaseModel
{
    use SoftDeletes;
    use NameFilterable;
    use StatusFilterable;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $fillable = [
        'name',
        'license_id',
        'license_type_id',
        'address',
        'state',
        'zip_code',
        'phone',
        'email',
        'status',
    ];

    public function scopeState($query, $value)
    {
        return $query->where('state', 'LIKE', "%{$value}%");
    }

    public function scopeLicenseTypeId($query, $value)
    {
        return $query->where('license_type_id', $value);
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%'.$value.'%');
    }
}
