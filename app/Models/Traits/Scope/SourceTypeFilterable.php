<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait SourceTypeFilterable
{
    /**
     * @param $query
     * @param mixed ...$sourceType
     * @return mixed
     */
    public function scopeSourceType($query, ...$sourceType)
    {
        return $query->whereIn('source_type', $sourceType);
    }
}
