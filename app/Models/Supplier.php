<?php


namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{AccountHolderFilterable, LicenseFilterable, NameFilterable};
use Illuminate\Database\Eloquent\{Builder, Relations\HasMany, Relations\HasManyThrough, SoftDeletes};

/**
 * App\Models\Supplier
 *
 * @property string                          $id
 * @property string                          $license_id
 * @property string                          $name
 * @property string|null                     $city
 * @property string|null                     $state
 * @property string|null                     $zip_code
 * @property string|null                     $email
 * @property string|null                     $phone_number
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|BaseModel defaultOrderBy()
 * @method static Builder|Supplier licenseId(string $licenseId)
 * @method static Builder|Supplier name($name)
 * @method static Builder|Supplier newModelQuery()
 * @method static Builder|Supplier newQuery()
 * @method static \Illuminate\Database\Query\Builder|Supplier onlyTrashed()
 * @method static Builder|Supplier orderByEmail($direction)
 * @method static Builder|Supplier orderByName($direction)
 * @method static Builder|Supplier orderByPhoneNumber($direction)
 * @method static Builder|Supplier query()
 * @method static Builder|Supplier search($value)
 * @method static Builder|Supplier whereCity($value)
 * @method static Builder|Supplier whereCreatedAt($value)
 * @method static Builder|Supplier whereDeletedAt($value)
 * @method static Builder|Supplier whereEmail($value)
 * @method static Builder|Supplier whereId($value)
 * @method static Builder|Supplier whereLicenseId($value)
 * @method static Builder|Supplier whereName($value)
 * @method static Builder|Supplier wherePhoneNumber($value)
 * @method static Builder|Supplier whereState($value)
 * @method static Builder|Supplier whereUpdatedAt($value)
 * @method static Builder|Supplier whereZipCode($value)
 * @method static \Illuminate\Database\Query\Builder|Supplier withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Supplier withoutTrashed()
 * @mixin \Eloquent
 */
class Supplier extends BaseModel
{
    use SoftDeletes;
    use NameFilterable;
    use AccountHolderFilterable;
    use HasInternalId;

    protected $fillable = [
        'account_holder_id',
        'name',
        'address',
        'city',
        'zip_code',
        'state',
        'email',
        'phone_number'
    ];

    public function additiveInventories(): HasMany
    {
        return $this->hasMany(AdditiveInventory::class);
    }

    public function additives(): HasManyThrough
    {
        return $this->hasManyThrough(
            Additive::class,
            AdditiveInventory::class,
            'supplier_id',
            'id',
            'id',
            'additive_id'
        );
    }

    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%'.$value.'%')
            ->orWhere('email', 'like', '%'.$value.'%');
    }

    public function scopeOrderByName(Builder $query, $direction): Builder
    {
        return $query->orderBy('name', $direction);
    }

    public function scopeOrderByEmail(Builder $query, $direction): Builder
    {
        return $query->orderBy('email', $direction);
    }

    public function scopeOrderByPhoneNumber(Builder $query, $direction): Builder
    {
        return $query->orderBy('phone_number', $direction);
    }
}
