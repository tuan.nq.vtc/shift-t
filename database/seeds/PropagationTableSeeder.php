<?php

use App\Models\{License, Propagation, Strain, Subroom};
use Illuminate\Database\Seeder;

/**
 * Class PropagationTableSeeder.
 */
class PropagationTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run(): void
    {
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
    }

    protected function iterateLicense($license): void
    {
        $strains = Strain::licenseId($license->id)->get();
        if ($strains->isEmpty()) {
            return;
        }
        $strains->each(fn(Strain $strain) => $this->iterateStrain($strain));
    }

    protected function iterateStrain(Strain $strain): void
    {
        $subrooms = Subroom::with('room')
            ->whereHas('room', fn($query) => $query->licenseId($strain->license_id))->get();

        if ($subrooms->isEmpty()) {
            return;
        }
        $subroom = $subrooms->random();
        factory(Propagation::class)->create(
            [
                'license_id' => $strain->license_id,
                'strain_id' => $strain->id,
                'room_id' => $subroom->room->id,
                'subroom_id' => $subroom->id,
            ]
        );
    }
}
