<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

abstract class CRUDController extends Controller
{
    /**
     * @var BaseService
     */
    protected BaseService $service;

    /**
     * @var Request
     */
    protected Request $request;

    /**
     * Validation rules of create action
     *
     * @return array
     */
    abstract public function getCreateRules(): array;

    /**
     * Validation rules of update action
     *
     * @param string $id
     *
     * @return array
     */
    abstract public function getUpdateRules(string $id): array;

    /**
     * CRUDController constructor.
     * @param BaseService $service
     * @param Request $request
     */
    public function __construct(BaseService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * @return JsonResponse
     */
    public function list()
    {
        return response()->json(
            $this->service->list(Criteria::createFromRequest($this->request))
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function show(string $id)
    {
        return response()->json($this->service->get($id));
    }

    /**
     * @return JsonResponse
     *
     * @throws ValidationException|GeneralException
     */
    public function store()
    {
        return response()->json(
            $this->service->create($this->validate($this->request, $this->getCreateRules())),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     *
     * @throws ValidationException
     * @throws ModelNotFoundException
     * @throws GeneralException
     */
    public function update(string $id)
    {
        return response()->json(
            $this->service->update(
                $id,
                $this->validate($this->request, $this->getUpdateRules($id))
            ),
            Response::HTTP_ACCEPTED
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     *
     * @throws ModelNotFoundException
     * @throws GeneralException
     */
    public function destroy(string $id)
    {
        $this->service->delete($id);

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param     $response
     * @param int $httpCode
     *
     * @return JsonResponse
     */
    public function renderResponse($response, $httpCode = Response::HTTP_OK)
    {
        return response()->json(
            $response,
            $response instanceof \Exception ? Response::HTTP_INTERNAL_SERVER_ERROR : $httpCode
        );
    }
}
