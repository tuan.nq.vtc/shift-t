<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabTestAnalysesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_test_analyses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('regulator', 50)->index();
            $table->string('name', 150);
            $table->boolean('is_required')->default(false);
            $table->string('code');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_test_analyses');
    }
}
