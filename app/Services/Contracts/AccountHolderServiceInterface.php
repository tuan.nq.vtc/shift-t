<?php

namespace App\Services\Contracts;

interface AccountHolderServiceInterface
{
    public function getAccountHolderId(): string;

    public function setAccountHolderId(string $accountHolderId): self;
}
