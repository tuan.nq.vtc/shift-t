<?php

namespace App\Services;

use App\Events\Disposal\DisposalScheduled;
use App\Events\PlantGroup\PlantGroupCreated;
use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{AdditiveOperation,
    Disposal,
    GrowCycle,
    HarvestGroup,
    HarvestPlant,
    License,
    Plant,
    PlantGroup,
    Propagation,
    Room,
    SeedToSale,
    Subroom,
    Strain
};
use App\Services\Traits\{PlantHandler};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection};
use Illuminate\Support\{Arr, Carbon, Enumerable, Facades\DB, Facades\Event};
use Throwable;

/**
 * Class PlantService
 *
 * @package App\Services
 *
 * @property Plant|Builder $model
 *
 * @method Plant get($id)
 * @method Plant|Builder getModel()
 * @method Enumerable|Plant[] getEnumerable(array $plantIds, $relations = [], $counts = [])
 * @method Collection|Plant[] getByIds(array $ids, $relations = [], $counts = [])
 */
class PlantService extends BaseService
{
    use PlantHandler;

    protected array $relations = [
        'strain:id,name',
        'room:id,name',
        'subroom:id,name',
        'growCycle',
        'plantGroup',
        'motherPropagation',
        'propagations',
    ];

    public function __construct(Plant $plant)
    {
        parent::__construct($plant);
    }

    public function getDetail($id)
    {
        return $this->newQuery()->with(
            [
                'strain:id,name',
                'room:id,name',
                'subroom:id,name',
                'growCycle',
                'plantGroup',
                'propagations',
                'motherPropagation',
                'motherPropagation.cutBy',
                'motherPropagation.pluggedBy',
                'motherPropagation.room:id,name',
            ]
        )->findOrFail($id);
    }

    /**
     * @param $growCycleId
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function listByGrowCycle($growCycleId, Criteria $criteria): LengthAwarePaginator
    {
        return $this->list($criteria->addFilter('grow_cycle_id', $growCycleId));
    }

    /**
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        return parent::list($criteria->addFilter('available', true));
    }

    /**
     * @param $harvestGroup
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function listByHarvest(HarvestGroup $harvestGroup, Criteria $criteria): LengthAwarePaginator
    {
        $harvestIds = $harvestGroup->harvests()->pluck('id')->toArray();
        $plantIds = HarvestPlant::whereIn('harvest_id', $harvestIds)->pluck('plant_id')->toArray();
        return $this->list($criteria->addFilter('ids', $plantIds));
    }

    /**
     * When user searches the results should be refined.
     * The search field should search the plant code, strain name and grow cycle name columns.
     *
     * @param string $code
     *
     * @param Criteria $criteria
     * @return \Illuminate\Support\Collection
     */
    public function search(string $code, Criteria $criteria): \Illuminate\Support\Collection
    {
        if (!$code) {
            return collect();
        }
        $query = $this->newQuery(false)
            ->select(['id', 'sync_code', 'sync_status', 'strain_id'])
            ->with('strain:id,name')
            ->scopes($this->loadScopes($criteria->getFilters()));

        return $this->applyOrderBy($query, $criteria->getSorts())
            ->available()
            ->whereNotNull('sync_code')
            ->where('sync_code', 'like', '%' . $code . '%')
            ->limit($criteria->getLimit())
            ->get();
    }

    /**
     * @param GrowCycle $growCycle
     *
     * @param array $plantIds
     * @return GrowCycle
     */
    public function movePlantsToGrowCycle(GrowCycle $growCycle, array $plantIds): GrowCycle
    {
        // remove grow cycle has no plants
        $growCycleIds = $this->getGrowCycleIdsByPlants($plantIds);
        $plantGroupIds = [];

        Plant::query()->whereIn('id', $plantIds)->where('grow_cycle_id','<>',$growCycle->id)->get()->each(function ($plant) use ($growCycle, &$plantGroupIds) {
            $plantGroupIds[] = $plant->plant_group_id;            
            $plant->grow_cycle_id = $growCycle->id;
            $plant->grow_status = $growCycle->grow_status;
            $plant->save();
        });

        if (!empty($growCycleIds)) {
            $deletableGrowCycles = GrowCycle::withCount(['plants'])->having('plants_count', 0)
                ->whereIn('id', $growCycleIds)->get();
            foreach ($deletableGrowCycles as $deletableGrowCycle) {
                $deletableGrowCycle->delete();
            }
        }

        $growCycle->plantGroups()->syncWithoutDetaching(array_unique($plantGroupIds));

        return $growCycle;
    }

    /**
     * @param Plant $plant
     * @param PlantGroup $plantGroup
     * @param GrowCycle $growCycle
     *
     * @return Plant
     */
    public function movePlantToGrowCycle(Plant $plant, PlantGroup $plantGroup, GrowCycle $growCycle): Plant
    {
        $plant->growCycle()->associate($growCycle);
        $plant->plantGroup()->associate($plantGroup);
        $plant->grow_status = $growCycle->grow_status;

        return tap($plant, fn(Plant $plant) => $plant->save());
    }

    /**
     * @param Enumerable|Plant[] $plants
     * @param Room $disposalRoom
     * @param array $data
     *
     * @return Enumerable|Plant[]
     *
     * @throws Throwable
     */
    public function movePlantsToDisposal(Enumerable $plants, Room $disposalRoom, array $data): Enumerable
    {
        if (!$disposalRoom->isDisposal()) {
            throw new GeneralException(__('exceptions.rooms.disposal_room_invalid'));
        }
        $quarantineStart = Carbon::createFromFormat(
            'Y-m-d H:i:s',
            Arr::get($data, 'quarantine_start', '')
        );
        if ($quarantineStart === false) {
            throw new GeneralException(__('exceptions.disposal.invalid_date'));
        }
        try {
            $quarantineEnd = $quarantineStart->clone()->addHours(Disposal::DEFAULT_EXTENDED_QUARANTINE_HOUR);
            $status = $quarantineEnd->lt(Carbon::now()) ?
                Disposal::STATUS_READY : Disposal::STATUS_SCHEDULED;
            DB::beginTransaction();

            // remove grow cycle has plants destroyed
            $growCycleIds = $this->getGrowCycleIdsByPlants($data['plant_ids']);

            $plants->each(
                fn(Plant $plant, $key) => $this->movePlantToDisposal(
                    $plant,
                    $disposalRoom,
                    [
                        'license_id' => $plant->license_id,
                        'status' => $status,
                        'uom' => Disposal::UOM_EACH,
                        'quarantine_start' => $quarantineStart,
                        'quarantine_end' => $quarantineEnd,
                        'reason' => Arr::get($data, 'reason'),
                        'quantity' => 1,
                        'comment' => $data['comment'] ?? '',
                        'refer_mother_code' => optional($plant->mother)->sync_code,
                        'scheduled_by_id' => Arr::get($data, 'scheduled_by_id'),
                    ]
                )
            );

            if (!empty($growCycleIds)) {
                GrowCycle::withCount(['plants' => function ($query) {
                    $query->where('destroyed', false);
                }])->having('plants_count', 0)->whereIn('id', $growCycleIds)->delete();
            }

            DB::commit();
            return $plants;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.plants.destroy_failed'));
        }
    }

    /**
     * @param Plant $plant
     * @param Room $room
     * @param array $disposalData
     *
     * @return Plant
     */
    public function movePlantToDisposal(Plant $plant, Room $room, array $disposalData): Plant
    {
        $plant->growCycle()->dissociate();
        /** @var Disposal $disposal */
        $disposal = $plant->disposals()->make($disposalData);
        $disposal->room()->associate($room);
        $disposal->strain()->associate($plant->strain);
        $disposal->save();

        $plant->update(['destroyed' => true]);

        // dispatch disposal scheduled
        Event::dispatch(new DisposalScheduled($disposal));

        return $plant;
    }

    /**
     * @param array $plants
     * @return array
     */
    public function getGrowCycleIdsByPlants(array $plants): array
    {
        return Plant::query()->whereIn('id', $plants)
            ->whereNotNull('grow_cycle_id')
            ->pluck('grow_cycle_id')->unique()->all();
    }

    /**
     * @param Enumerable|Plant[] $plants
     *
     * @return Enumerable|Plant[]
     *
     * @throws Throwable
     */
    public function makeMothers(Enumerable $plants): Enumerable
    {
        try {
            DB::beginTransaction();
            $plants = $plants->map(fn(Plant $plant) => $this->makePlantAsMother($plant));
            DB::commit();

            return $plants;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.plants.make_mother_failed'));
        }
    }

    /**
     * @param Plant $plant
     *
     * @return Plant
     *
     * @throws GeneralException
     */
    public function makePlantAsMother(Plant $plant): Plant
    {
        $this->validatePlant($plant);

        return tap($plant, fn(Plant $instance) => $instance->update(['is_mother' => 1]));
    }

    /**
     *
     * @param string $target
     * @param array $ids
     *
     * @return Enumerable|null
     */
    public function getPlantEnumerableForAdditive(string $target, array $ids): ?Enumerable
    {
        $query = $this->newQuery()->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE]);
        switch ($target) {
            case AdditiveOperation::GROW_CYCLE_OPERATION:
                return $query->whereHas(
                    'growCycle',
                    fn(Builder $query) => $query->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])->whereIn('id', $ids)
                )->cursor();
            case AdditiveOperation::ROOM_OPERATION:
                return $query->whereHas(
                    'room',
                    fn(Builder $query) => $query->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])->whereIn('id', $ids)
                )->cursor();
            case AdditiveOperation::PLANT_OPERATION:
                return $query->whereIn('id', $ids)->cursor();
            default:
                return null;
        }
    }

    /**
     * Get all plant base on additive object(s) to harvest
     * @param string $additiveType
     * @param array $ids
     * @param array $relations
     * @return Enumerable|null
     */
    public function getPlantForAdditive(string $additiveType, array $ids, array $relations = []): ?Enumerable
    {
        $query = $this->newQuery()->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])->limit(1000);
        if (!empty($relations)) {
            $query->with($relations);
        }
        switch ($additiveType) {
            case AdditiveOperation::GROW_CYCLE_OPERATION:
                return $query->whereHas(
                    'growCycle',
                    fn(Builder $query) => $query->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])->whereIn('id', $ids)
                )->get();
            case AdditiveOperation::ROOM_OPERATION:
                return $query->whereHas(
                    'subroom.room',
                    fn(Builder $query) => $query->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])->whereIn('id', $ids)
                )->get();
            default:
                return null;
        }
    }

    /**
     * @param Enumerable $plants
     * @param Strain $strain
     *
     * @return Enumerable
     *
     * @throws Throwable
     */
    public function editPlantsStrain(Enumerable $plants, Strain $strain): Enumerable
    {
        try {
            DB::beginTransaction();
            $plants = $plants->map(fn(Plant $plant) => $this->editPlantStrain($plant, $strain));
            DB::commit();

            return $plants;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param Plant $plant
     * @param Strain $strain
     *
     * @return Plant
     *
     * @throws Throwable
     */
    public function editPlantStrain(Plant $plant, Strain $strain): Plant
    {
        // Validate plant before move room
        $this->validatePlant($plant);
        if ($plant->strain_id == $strain->id || !$plant->growCycle) {
            return $plant;
        }
        /*
         * Check grow cycle & plant group
         * Grow cycle has any plant group belong to the new strain
         * and plant group is on the same room with current plant
         */
        $plantGroup = $plant->growCycle->plantGroups->first(
            fn(PlantGroup $group) => $group->strain_id == $strain->id && $group->room_id == $plant->room_id
        );
        // There's no plant group of current plant grow cycle that matched with the above conditions
        if (!$plantGroup) {
            // Create a new plant group (meaning create a new plant/vegetation batch)
            $plantGroup = $this->createPlantGroup(
                $plant->license,
                $strain,
                $plant->room,
                $plant->motherPropagation,
                $plant->growCycle,
            );
        }

        $plant->plantGroup()->associate($plantGroup);
        $plant->strain()->associate($strain);
        $plant->save();

        return $plant->fresh(['strain', 'room', 'growCycle', 'plantGroup', 'motherPropagation', 'propagations']);
    }

    /**
     * #todo refactor the function
     * @param License $license
     * @param Strain $strain
     * @param Room $room
     * @param Propagation|null $propagation
     * @param GrowCycle|null $growCycle
     *
     * @return PlantGroup
     *
     * @throws Throwable
     */
    public function createPlantGroup(
        License $license,
        Strain $strain,
        Room $room,
        ?Propagation $propagation,
        ?GrowCycle $growCycle
    ): PlantGroup {
        try {
            DB::beginTransaction();
            $name = 'Group of ' . $strain->name;
            if ($growCycle) {
                $name .= ' in ' . $growCycle->name;
            }
            /** @var PlantGroup $plantGroup */
            $plantGroup = $license->plantGroups()
                ->make(['name' => $name])
                ->strain()->associate($strain)
                ->room()->associate($room)
                ->propagation()->associate($propagation);
            $plantGroup->save();
            $plantGroup->growCycles()->attach($growCycle);

            // Dispatch Sync Plant Group
            Event::dispatch(new PlantGroupCreated($plantGroup));

            DB::commit();

            return $plantGroup;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function updatePlants(Enumerable $plants, $data): Enumerable
    {
        try {
            DB::beginTransaction();
            if (isset($data['grow_cycle_id'])) { // Move to Grow Cycle
                $plantIds = $data['plant_ids'];
                $growCycle = GrowCycle::findOrFail($data['grow_cycle_id']);

                $this->movePlantsToGrowCycle($growCycle, $plantIds);
            }

            $plants = $plants->map(fn(Plant $plant) => $this->updatePlant($plant, $data));

            DB::commit();

            return $plants;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.plants.update_failed'));
        }
    }

    public function updatePlant(Plant $plant, $data): Plant
    {
        $this->validatePlant($plant);

        if (isset($data['grow_status']) && !in_array($data['grow_status'], SeedToSale::GROW_CYCLE_GROW_STATUSES)) {
            throw new GeneralException('Invalid grow status');
        }
        // edit strain
        if (isset($data['strain_id'])) {
            $strain = Strain::findOrFail($data['strain_id']);

            $this->editPlantStrain($plant, $strain);
        }

        // Move to room
        if (isset($data['room_id'])) {
            $room = Room::findOrFail($data['room_id']);
            $subroom = isset($data['subroom_id']) ? Subroom::findOrFail($data['subroom_id']) : null;

            $this->movePlantToRoom($plant, $room, $subroom);
        }

        $plant->grow_status = $data['grow_status'] ?? $plant->grow_status;
        $plant->is_mother = $data['is_mother'] ?? $plant->is_mother;
        $plant->save();

        return $plant->fresh(['strain', 'room', 'growCycle', 'plantGroup', 'motherPropagation', 'propagations']);
    }
}
