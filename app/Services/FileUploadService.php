<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Jobs\DeleteMultipleImageSizesJob;
use App\Jobs\GenerateMultipleImageSizesJob;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Throwable;

class FileUploadService
{
    /**
     * @param Model $model
     * @param UploadedFile $file
     * @param string $id
     * @param string $fieldName
     * @return Model
     * @throws GeneralException
     * @throws Throwable
     */
    public function upload(Model $model, UploadedFile $file, string $id, string $fieldName): Model
    {
        $oldFile = $model->getRawOriginal($fieldName);

        try {
            DB::beginTransaction();
            $filePath = sprintf("%s/%s", $id, $model->getTable());
            $uploadedFile = Storage::putFile($filePath, $file);
            if (!$model->update([$fieldName => $uploadedFile,])) {
                if (Storage::exists($uploadedFile)) {
                    Storage::delete($uploadedFile);
                }
                throw new GeneralException(__("exceptions.file_upload.update_file_failed"));
            }

            if ($oldFile && Storage::exists($oldFile)) {
                Storage::delete($oldFile);
                $imageConfigs = config('image_sizes');
                if (!empty($imageConfigs[get_class($model)])) {
                    dispatch(new DeleteMultipleImageSizesJob($model, $oldFile));
                }
            }

            DB::commit();
            return $model;
        } catch (Throwable $e) {
            // #todo: log error to sentry
            Log::error($e);
            DB::rollBack();
            throw new GeneralException(__("exceptions.file_upload.upload_file_failed"));
        }
    }

    /**
     * @param Model $model
     * @param UploadedFile $file
     * @param string $id
     * @param string $fieldName
     * @return void
     * @throws GeneralException
     * @throws Throwable
     */
    public function uploadImage(Model $model, UploadedFile $file, string $id, string $fieldName): void
    {
        $this->upload($model, $file, $id, $fieldName);

        $imageConfigs = config('image_sizes');
        if (!empty($imageConfigs[get_class($model)])) {
            dispatch(new GenerateMultipleImageSizesJob($model, $model->getAttribute($fieldName)));
        }
    }

    /**
     * @param Model $model
     * @param string $fieldName
     * @return Model
     * @throws GeneralException|Throwable
     */
    public function remove(Model $model, string $fieldName): Model
    {
        $oldFile = $model->getRawOriginal($fieldName);

        try {
            DB::beginTransaction();
            if (!$model->update([$fieldName => null])) {
                throw new GeneralException(__("exceptions.file_upload.update_file_failed"));
            }

            if ($oldFile && Storage::exists($oldFile)) {
                Storage::delete($oldFile);
            }

            DB::commit();
            return $model;
        } catch (Throwable $e) {
            // #todo: log error to sentry
            Log::error($e);
            DB::rollBack();
            throw new GeneralException(__("exceptions.file_upload.remove_file_failed"));
        }
    }

    /**
     * @param string $imagePath
     * @param string $imageSize
     * @param array $config
     * @return string
     * @throws FileNotFoundException
     */
    public function generateResizedImage(string $imagePath, string $imageSize, array $config = []): string
    {
        $file = Storage::get($imagePath);
        $image = Image::make($file);
        if (!empty($config) && !empty($config['width'])) {
            $image->fit($config['width'], $config['height'] ?? null);
        }
        $image->encode('webp');
        $resizedImagePath = self::getPathByImageSize($imagePath, $imageSize);
        Storage::disk('public_storage')->put($resizedImagePath, $image);
        $image->destroy();
        return $resizedImagePath;
    }

    public function existsResizedImage(string $imagePath, string $imageSize): bool
    {
        return Storage::disk('public_storage')->exists(self::getPathByImageSize($imagePath, $imageSize));
    }

    public function deleteResizedImage(string $imagePath, string $imageSize): bool
    {
        return Storage::disk('public_storage')->delete(self::getPathByImageSize($imagePath, $imageSize));
    }

    private static function getPathByImageSize(string $imagePath, string $imageSize): string
    {
        return $imageSize . '/' . $imagePath . '.webp';
    }
}
