<?php

namespace App\Synchronizations\Http\Resources;

use App\Synchronizations\Contracts\Http\ResourceInterface;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Exception;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Throwable;

abstract class BaseResource implements ResourceInterface
{
    protected LoggerInterface $logger;
    public TraceableModelInterface $traceableModel;
    protected array $conditions = [];
    protected array $changes = [];
    public string $action = '';

    protected string $httpMethod = '';
    protected string $requestUrl = '';
    protected array $requestQuery = [];
    protected array $requestBody = [];

    /**
     * @param TraceableModelInterface $traceableModel
     * @param array $changes
     * @param array $conditions
     */
    public function __construct(TraceableModelInterface $traceableModel, array $changes = [], array $conditions = [])
    {
        $this->logger = Log::channel('sync');
        $this->traceableModel = $traceableModel;
        $this->conditions = $conditions;
        $this->changes = $changes;
    }

    /**
     * @param string $action
     * @throws Throwable
     */
    public function build(string $action): void
    {
        if (!$action || !method_exists($this, $action)) {
            throw new Exception("$action function is not found");
        }

        call_user_func([$this, $action]);
    }

    public function setMethod(string $method): ResourceInterface
    {
        $this->httpMethod = $method;

        return $this;
    }

    public function getMethod(): string
    {
        return $this->httpMethod;
    }

    public function setUrl($url): ResourceInterface
    {
        $this->requestUrl = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->requestUrl;
    }

    public function setQuery($query): ResourceInterface
    {
        $this->requestQuery = $query;

        return $this;
    }

    public function addQuery($param, $value): ResourceInterface
    {
        $this->requestQuery[$param] = $value;

        return $this;
    }

    public function getQuery(): array
    {
        return $this->requestQuery;
    }

    public function setBody($body): ResourceInterface
    {
        $this->requestBody = $body;

        return $this;
    }

    public function getBody(): array
    {
        return $this->requestBody;
    }
}
