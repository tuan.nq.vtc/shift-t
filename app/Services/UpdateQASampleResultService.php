<?php

namespace App\Services;

use App\Cqrs\Commands\UpdateQASampleHandler;
use App\Cqrs\Commands\UpdateQASampleCommand;
use Illuminate\Http\JsonResponse;
use Throwable;

class UpdateQASampleResultService
{
    const MSG_FAILED = 'Update QA Sample Result failed';

    /**
     * @var UpdateQASampleHandler
     */
    private $handler;

    /**
     * @param UpdateQASampleHandler $handler
     */
    public function __construct(UpdateQASampleHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param UpdateQASampleCommand $command
     * @return JsonResponse
     */
    public function __invoke(UpdateQASampleCommand $command): JsonResponse
    {
        try {
            return wpApiResponse($this->handler->handle($command));
        } catch (Throwable $e) {
            $errors = $this->handler->isRecordedErrors() ?
                $this->handler->getErrorBag() : [$e->getMessage()];

            return wpApiResponseWithErrors($errors, self::MSG_FAILED);
        }
    }
}
