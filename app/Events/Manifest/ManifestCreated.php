<?php

namespace App\Events\Manifest;

use App\Events\Event;
use App\Models\Manifest;

class ManifestCreated extends Event
{
    /**
     * @var Manifest
     */
    public Manifest $manifest;

    /**
     * @param Manifest $manifest
     */
    public function __construct(Manifest $manifest)
    {
        $this->manifest = $manifest;
    }
}
