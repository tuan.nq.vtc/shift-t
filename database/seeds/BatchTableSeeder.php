<?php

use App\Models\{Batch, License, Propagation};
use Illuminate\Database\Seeder;

class BatchTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(
            function ($license) {
                $propagations = Propagation::where('license_id', $license->id)->get();
                $data = [
                    'license_id' => $license->id,
                ];
                foreach ($propagations as $propagation) {
                    $data['source_id'] = $propagation->id;
                    $data['source_type'] = Propagation::class;
                    factory(Batch::class, 5)->create($data);
                }
            }
        );
    }
}
