<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use App\Models\{Disposal, Harvest, Plant, Propagation, TraceableModel, WasteBag};
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Throwable;

/**
 * Class DisposalResource
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property Disposal|TraceableModel $traceableModel
 */
class DisposalResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_DISPOSAL;

    protected function fetchAll(): self
    {
        parent::fetchAll();
        $errorFormat = 'Unable to fetch all disposals without %s';
        if (empty($this->conditions['from_date']) || empty($this->conditions['to_date'])
            || !($fromDate = Carbon::createFromDate($this->conditions['from_date']))
            || !($toDate = Carbon::createFromDate($this->conditions['to_date']))) {
            $error = sprintf($errorFormat, 'date range LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error);
            throw new Exception($error);
        }

        // f_date1={mm}%2F{dd}%2F{yyyy}&f_date2={mm}%2F{dd}%2F{yyyy}
        $this->requestQuery['f_date1'] = $fromDate->format('m/d/Y');
        $this->requestQuery['f_date2'] = $toDate->format('m/d/Y');
        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function schedule(): self
    {
        $disposal = $this->traceableModel;
        $globalBatchId = '';
        $globalAreaId = '';
        $globalPlantId = '';

        $errorFormat = 'Unable to sync disposal %s';
        $context = ['disposal' => $disposal];

        switch (true) {
            case $disposal->wastable instanceof WasteBag:
                if (!optional($disposal->wastable->room)->sync_code) {
                    $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
                    $this->logger->error($error, $context);
                    throw new Exception($error);
                }
                $source = Disposal::SOURCE_LEAF_DAILY_PLANT_WASTE;
                $globalAreaId = $disposal->wastable->room->sync_code;
                break;
            case $disposal->wastable instanceof Plant:
                if (!$disposal->wastable->sync_code) {
                    $error = sprintf($errorFormat, 'without plant LEAF GLOBAL_ID [sync_code]');
                    $this->logger->error($error, $context);
                    throw new Exception($error);
                }
                $source = Disposal::SOURCE_LEAF_PLANT;
                $globalPlantId = $disposal->wastable->sync_code;
                break;
            case $disposal->wastable instanceof Propagation:
            case $disposal->wastable instanceof Harvest:
                if (!optional($disposal->wastable->batch)->sync_code) {
                    $error = sprintf($errorFormat, 'without batch LEAF GLOBAL_ID [sync_code]');
                    $this->logger->error($error, $context);
                    throw new Exception($error);
                }
                $source = Disposal::SOURCE_LEAF_BATCH;
                $globalBatchId = $disposal->wastable->batch->sync_code;
                break;
            default:
                throw new Exception('The disposal is invalid');
        }

        return parent::create()
            ->setBody(
                [
                    'disposal' => [
                        [
                            'external_id' => $disposal->id,
                            'reason' => $disposal->reason,
                            'qty' => $disposal->quantity,
                            'uom' => $disposal->uom,
                            'source' => $source,
                            'hold_starts_at' => $disposal->quarantine_start->format('m/d/Y h:ia'),
                            'hold_ends_at' => $disposal->quarantine_end->format('m/d/Y h:ia'),
                            'global_batch_id' => $globalBatchId,
                            'global_area_id' => $globalAreaId,
                            'global_plant_id' => $globalPlantId,
                            'global_inventory_id' => '',
                        ],
                    ]
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function destroy(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync disposal destroy without LEAF global_id [sync_code]')
        );

        return parent::update()
            ->setUrl(self::$endpoint . '/dispose')
            ->setBody(
                [
                    'external_id' => $this->traceableModel->id,
                    'global_id' => $this->traceableModel->sync_code,
                    'disposal_at' => $this->traceableModel->destroyed_at->format('m/d/Y h:ia'),
                ]
            );
    }
}
