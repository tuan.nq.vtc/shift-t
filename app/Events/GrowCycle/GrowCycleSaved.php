<?php

namespace App\Events\GrowCycle;

use App\Events\Event;
use App\Models\GrowCycle;

class GrowCycleSaved extends Event
{
    /**
     * @var GrowCycle
     */
    public GrowCycle $growCycle;

    /**
     * @param GrowCycle $growCycle
     */
    public function __construct(GrowCycle $growCycle)
    {
        $this->growCycle = $growCycle;
    }
}
