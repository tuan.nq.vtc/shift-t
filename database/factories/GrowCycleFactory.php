<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\GrowCycle;
use App\Models\License;
use App\Models\SeedToSale;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    GrowCycle::class,
    function (Faker $faker) {
        return [
            "name" => $faker->word() . ' ' . $faker->randomNumber(1),
            "license_id" => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                GrowCycle::class,
                $data['license_id']
            ),
            'grow_status' => $faker->randomElement(SeedToSale::GROW_CYCLE_GROW_STATUSES),
            'planted_at' => $faker->dateTimeThisYear(),
            'est_harvested_at' => $faker->dateTimeThisYear(),
        ];
    }
);
