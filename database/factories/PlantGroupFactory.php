<?php

/** @var Factory $factory */

use App\Models\{Batch, PlantGroup, Propagation, Room, Strain};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(
    PlantGroup::class,
    fn(Faker $faker) => [
        'license_id' => $faker->uuid,
        'propagation_id' => fn($data) => factory(Propagation::class)->create(
            ['license_id' => $data['license_id'],]
        )->id,
        'room_id' => fn($data) => factory(Room::class)->create(['license_id' => $data['license_id']])->id,
        'strain_id' => fn($data) => factory(Strain::class)->create(['license_id' => $data['license_id']])->id,
        'name' => 'Plant Group '.$faker->regexify('[A-Z0-9]{2}'),
    ]
);
