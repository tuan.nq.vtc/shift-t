<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models;
use Exception;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class InventoryTypeResource
 * @package App\Synchronizations\Http\Resources\Metrc
 *
 * @property Models\InventoryType|Models\TraceableModel $traceableModel
 */
class InventoryTypeResource extends BaseMetrcResource
{
    protected string $path = '/items/v1';

    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = $this->path.'/active';

        return $this;
    }

    protected function create(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/create';
        $this->requestBody = [
            [
                /*[
                    {
                        "ItemCategory": "Buds",
                        "Name": "Buds Item",
                        "UnitOfMeasure": "Ounces",
                        "Strain": "Spring Hill Kush",
                        "ItemBrand": null,
                        "AdministrationMethod": null,
                        "UnitCbdPercent": null,
                        "UnitCbdContent": null,
                        "UnitCbdContentUnitOfMeasure": null,
                        "UnitCbdContentDose": null,
                        "UnitCbdContentDoseUnitOfMeasure": null,
                        "UnitThcPercent": null,
                        "UnitThcContent": null,
                        "UnitThcContentUnitOfMeasure": null,
                        "UnitThcContentDose": null,
                        "UnitThcContentDoseUnitOfMeasure": null,
                        "UnitVolume": null,
                        "UnitVolumeUnitOfMeasure": null,
                        "UnitWeight": null,
                        "UnitWeightUnitOfMeasure": null,
                        "ServingSize": null,
                        "SupplyDurationDays": null,
                        "NumberOfDoses": null,
                        "Ingredients": null,
                        "Description": null
                    }
                ]*/
                'ItemCategory' => $this->traceableModel->stateCategory->name,
                'Name' => $this->traceableModel->name,
                'UnitOfMeasure' => $this->traceableModel->options['uom'] ?? 'Each',
                'Strain' => $this->traceableModel->strain->name ?? null,
                'UnitVolume' => $this->traceableModel->options['volume'] ?? null,
                'UnitVolumeUnitOfMeasure' => isset($this->traceableModel->options['volume']) ? $this->traceableModel->options['unit_uom'] ?? null : null,
                'UnitWeight' => $this->traceableModel->options['weight'] ?? null,
                'UnitWeightUnitOfMeasure' => isset($this->traceableModel->options['weight']) ? $this->traceableModel->options['unit_uom'] ?? null : null,
            ]
        ];

        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync inventory item update without METRC Id [sync_code]')
        );
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/update';
        $this->requestBody = [
            /*[
                {
                    "Id": 1,
                    "Name": "Buds Item",
                        "UnitOfMeasure": "Ounces",
                        "Strain": "Spring Hill Kush",
                        "ItemBrand": null,
                        "AdministrationMethod": null,
                        "UnitCbdPercent": null,
                        "UnitCbdContent": null,
                        "UnitCbdContentUnitOfMeasure": null,
                        "UnitCbdContentDose": null,
                        "UnitCbdContentDoseUnitOfMeasure": null,
                        "UnitThcPercent": null,
                        "UnitThcContent": null,
                        "UnitThcContentUnitOfMeasure": null,
                        "UnitThcContentDose": null,
                        "UnitThcContentDoseUnitOfMeasure": null,
                        "UnitVolume": null,
                        "UnitVolumeUnitOfMeasure": null,
                        "UnitWeight": null,
                        "UnitWeightUnitOfMeasure": null,
                        "ServingSize": null,
                        "SupplyDurationDays": null,
                        "NumberOfDoses": null,
                        "Ingredients": null,
                        "Description": null
                }
            ]*/
            [
                'Id' => $this->traceableModel->sync_code,
                'ItemCategory' => $this->traceableModel->stateCategory->name,
                'Name' => $this->traceableModel->name,
                'UnitOfMeasure' => $this->traceableModel->options['uom'] ?? 'Each',
                'Strain' => $this->traceableModel->strain->name ?? null,
                'UnitVolume' => $this->traceableModel->options['volume'] ?? null,
                'UnitVolumeUnitOfMeasure' => isset($this->traceableModel->options['volume']) ? $this->traceableModel->options['unit_uom'] ?? null : null,
                'UnitWeight' => $this->traceableModel->options['weight'] ?? null,
                'UnitWeightUnitOfMeasure' => isset($this->traceableModel->options['weight']) ? $this->traceableModel->options['unit_uom'] ?? null : null,
            ],
        ];

        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync inventory item deletion without METRC Id [sync_code]')
        );
        $this->httpMethod = Request::METHOD_DELETE;
        $this->requestUrl = $this->path.'/'.$this->traceableModel->sync_code;

        return $this;
    }
}
