<?php

use App\Facades\Sales;
use App\Models\{EndProduct, Inventory, Taxonomy};
use Illuminate\Database\Seeder;

class UpdateStateCategoryForLottedInventory extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        Inventory::query()
            ->where('is_lotted', Inventory::IS_LOTTED)
            ->where('product_type', EndProduct::class)
            ->whereNull('state_category_id')
            ->each(function ($inventory) {
                $product = Sales::setState($inventory->license_id)->getProductsByIds([$inventory->product_id])->first();
                if ($product) {
                    $stateCategoryId = Taxonomy::query()->where('category_id', $product->category_id)
                        ->where('license_id', $inventory->license_id)
                        ->pluck('state_category_id')->first();
                    if ($stateCategoryId) {
                        $inventory->update([
                            'state_category_id' => $stateCategoryId
                        ]);
                    }
                }
            });
    }
}
