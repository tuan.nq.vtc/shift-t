<?php

namespace Bamboo\StreamProcessing\Jobs;

use Exception;
use Bamboo\StreamProcessing\Contracts\{ProducerHandlerInterface, StreamEventInterface};
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\{InteractsWithQueue, SerializesModels};
use Illuminate\Support\Facades\Log;
use Throwable;

class PublishStreamEventJob implements ShouldQueue
{
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private StreamEventInterface $streamEvent;
    private string $topicName;

    public function __construct(string $topicName, StreamEventInterface $streamEvent)
    {
        if (!$topicName) {
            throw new Exception('No topic is configured');
        }
        $this->queue = 'stream_publish_event';
        $this->streamEvent = $streamEvent;
        $this->topicName = $topicName;
    }

    /**
     * @param ProducerHandlerInterface $handler
     *
     * @throws Throwable
     */
    public function handle(ProducerHandlerInterface $handler)
    {
        $handler->sendEvent($this->topicName, $this->streamEvent);
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $e
     *
     * @return void
     */
    public function failed(Throwable $e)
    {
        Log::error('Publish stream [' . get_class($this->streamEvent) . '] failed: ' . $e->getMessage());
    }
}
