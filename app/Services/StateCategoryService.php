<?php

namespace App\Services;

use App\Models\License;
use App\Models\StateCategory;
use Exception;
use Illuminate\Support\Collection;

/**
 * @package App\Services
 */
class StateCategoryService
{
    protected string $licenseId = '';

    /**
     * @param $licenseId
     *
     * @return $this
     */
    public function setLicenseId($licenseId)
    {
        $this->licenseId = $licenseId;

        return $this;
    }

    /**
     * @param string $id
     * @return StateCategory
     */
    public function get(string $id): StateCategory
    {
        return StateCategory::findOrFail($id);
    }

    /**
     * @param string $type
     * @return Collection
     * @throws Exception
     */
    public function getByType(string $type): Collection
    {
        $license = License::find($this->licenseId);
        if (!$license) {
            return collect();
        }
        return $license->stateCategories()->where('type', $type)->orderBy('name', 'asc')->get();
    }
}
