<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'source_categories',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('account_holder_id')->index();
                $table->string('name')->index();
                $table->uuid('parent_id')->nullable()->index();
                $table->text('path')->nullable();
                $table->text('path_name')->nullable();
                $table->text('description')->nullable();
                $table->unsignedTinyInteger('status')->default(1);
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('source_categories');
    }
}
