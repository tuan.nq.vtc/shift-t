<?php

namespace App\Models\Traits;

use App\Models\{Batch, Harvest, PlantGroup, Propagation};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait MorphToBatch
{
    public static function bootMorphToBatch(): void
    {
        // Create batch before create model
        static::created(static fn (self $model) => static::morphCreated($model));
    }

    /**
     * @param Model|Propagation|PlantGroup|Harvest $model
     */
    private static function morphCreated(self $model): void
    {
        if ($model->batch instanceof Batch && $model->batch->exists) {
            return;
        }

        $data = [
            'license_id' => $model->license_id,
            'name' => $model->name,
        ];

        /** @var Batch $batch */
        $batch = $model->batch()->make($data);
        $batch->source()->associate($model);
        if ($model instanceof PlantGroup && $model->propagation instanceof Propagation) {
            $batch->parent()->associate(optional($model->propagation)->batch);
        }
        $batch->save();
    }

    public function batch(): MorphOne
    {
        return $this->morphOne(Batch::class, 'source');
    }
}
