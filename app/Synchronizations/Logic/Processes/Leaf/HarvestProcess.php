<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch, Harvest, HarvestGroup, Room, Strain, Trace, TraceableModel};
use App\Services\LicenseService;
use App\Synchronizations\{Constants\LeafConstant, Contracts\TraceableModelInterface, Helpers\LeafHelper};
use App\Synchronizations\Exceptions\Pull\NoImportableDataException;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class HarvestProcess
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 * @property Harvest|TraceableModelInterface $traceableModel
 */
class HarvestProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @return void
     * @throws Throwable
     */
    protected function create(): void
    {
        $harvest = $this->traceableModel;
        $payload = $this->payload;
        $globalId = $this->getGlobalId($payload);
        if (!$globalId) {
            throw new Exception(
                "Not found any record matched with [external_id: {$harvest->id}] in LEAF API response"
            );
        }
        try {
            DB::beginTransaction();
            $batch = Batch::create(
                [
                    'name' => "Harvest {$harvest->id}",
                    'status' => Batch::STATUS_OPEN,
                    'license_id' => $harvest->license_id,
                    'sync_code' => $globalId,
                ]
            );
            $batch->source()->associate($harvest)->save();

            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            $this->logger->error($throwable->getMessage(), ['Harvest ID' => $harvest->id]);
            throw $throwable;
        }
    }

    /**
     * @throws Throwable
     */
    protected function finalize(): void
    {
        $payload = $this->payload;
        $harvest = $this->traceableModel;

        try {
            DB::beginTransaction();
            foreach ($payload as $inventory) {
                if (empty($inventory['global_id']) || empty($inventory['global_inventory_type_id'])) {
                    continue;
                }
                $harvest->inventories()
                    ->whereHas('type', fn($q) => $q->where('sync_code', $inventory['global_inventory_type_id']))
                    ->update(
                        [
                            'sync_code' => $inventory['global_id'],
                            'synced_at' => Carbon::now(),
                            'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                        ]
                    );
            }
            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            $this->logger->error($throwable->getMessage(), ['Harvest ID' => $harvest->id]);
            throw $throwable;
        }
    }

    /**
     * @return void
     * @throws Throwable
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;

        $hasNextImport = true;
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }
        try {
            $reports = $this->importData($payload['data'] ?? []);
        } catch (NoImportableDataException $e) {
            if ($currentPage !== 1) {
                $hasNextImport = false;
            }
            $reports = [];
        } catch (Throwable $e) {
            throw $e;
        }
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));

        if ($lastPage === 1) {
            $hasNextImport = false;
        }
        $nextPage = $currentPage === 1 ? $lastPage : $currentPage - 1;
        if ($hasNextImport && $nextPage <= 1) {
            $hasNextImport = false;
        }
        if ($hasNextImport) {
            $children = $this->trace->children;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }

            // create trace from last page to second page
            $conditions['page'] = $nextPage;
            $nextPageTrace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => get_classname($this->traceableModel),
                    'status' => Trace::STATUS_PENDING,
                    'resource_conditions' => $conditions,
                ]
            );

            // reassign the children of current trace to next page trace
            $nextPageTrace->children()->attach($children);
            dispatch(new SyncData($nextPageTrace));

            $this->trace->children()->detach();
        }

        // dispatch result after imported
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $hasNextImport ? StreamableResourceInterface::ACTION_UPDATE : 'finish',
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     * @throws Exception
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $licenseId = $this->traceableModel->license->id;
        $conditions = $this->trace->resource_conditions ?? [];
        $fromDate = null;
        $toDate = null;
        if (!empty($conditions['from_date'])) {
            $fromDate = Carbon::createFromDate($conditions['from_date']);
        }
        if (!empty($conditions['to_date'])) {
            $toDate = Carbon::createFromDate($conditions['to_date']);
        }

        // prepare data
        $syncCodes = $strainSyncCodes = $roomSyncCodes = [];
        $importableData = [];
        foreach ($data as $item) {
            try {
                $updatedAt = LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'updated_at'));
                if (!$updatedAt || ($fromDate && $fromDate->greaterThanOrEqualTo($updatedAt))
                    || ($toDate && $toDate->lessThanOrEqualTo($updatedAt))) {
                    continue;
                }
            } catch (Exception $e) {
                $this->logger->info($item);
                continue;
            }
            if (!empty($item['global_id'])) {
                $syncCodes[$item['global_id']] = 1;
            }
            if (!empty($item['global_strain_id'])) {
                $strainSyncCodes[$item['global_strain_id']] = 1;
            }
            if (!empty($item['global_area_id'])) {
                $roomSyncCodes[$item['global_area_id']] = 1;
            }
            $importableData[] = $item;
        }
        if (empty($importableData)) {
            throw new NoImportableDataException();
        }
        /**
         * ['sync_code' => 'id']
         */
        $availableStrainArray = Strain::licenseId($licenseId)->whereIn('sync_code', array_keys($strainSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $availableRoomArray = Room::licenseId($licenseId)->whereIn('sync_code', array_keys($roomSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $existingHarvestArray = Batch::licenseId($licenseId)->whereIn('sync_code', array_keys($syncCodes))
            ->pluck('id', 'sync_code')->toArray();

        $usedStrainIds = $usedRoomIds = [];
        $newHarvestData = $newBatchData = $newHarvestGroupData = [];
        foreach ($importableData as $item) {
            try {
                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingHarvestArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }

                if (!($type = Arr::get($item, 'type')) || $type !== LeafConstant::BATCH_TYPE_HARVEST) {
                    throw new Exception('Invalid type');
                }

                $strainSyncCode = Arr::get($item, 'global_strain_id');
                if (!$strainSyncCode || empty($availableStrainArray[$strainSyncCode])) {
                    throw new Exception('Strain not found');
                }
                $strainId = $availableStrainArray[$strainSyncCode];

                $roomSyncCode = Arr::get($item, 'global_area_id');
                if (!$roomSyncCode || empty($availableRoomArray[$roomSyncCode])) {
                    throw new Exception('Room not found');
                }
                $roomId = $availableRoomArray[$roomSyncCode];

                $usedStrainIds[$strainId] = 1;
                $usedRoomIds[$roomId] = 1;

                $harvestId = Uuid::uuid4()->toString();
                $harvestGroupId = Uuid::uuid4()->toString();
                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                $startAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'harvested_at')
                    ) ?? $createdAt; // use created_at as default if harvested_at is empty
                $finishedAt = LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'harvested_end_at'));
                $name = "Harvest Group {$globalId}";
                $newBatchData[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'name' => $name,
                    'license_id' => $licenseId,
                    'status' => Arr::get(
                        $item,
                        'status'
                    ) === LeafConstant::BATCH_STATUS_OPEN ? Batch::STATUS_OPEN : Batch::STATUS_CLOSED,
                    'source_type' => Harvest::class,
                    'source_id' => $harvestId,
                    'sync_code' => $globalId,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $harvestStatus = $this->getHarvestStatus(Arr::get($item, 'harvest_stage'));
                $newHarvestGroupData[] = [
                    'id' => $harvestGroupId,
                    'name' => $name,
                    'license_id' => $licenseId,
                    'room_id' => $roomId,
                    'status' => $harvestStatus,
                    'type' => HarvestGroup::TYPE_DRY, // set dry as default
                    'start_at' => $startAt,
                    'finished_at' => $finishedAt,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $newHarvestData[] = [
                    'id' => $harvestId,
                    'license_id' => $licenseId,
                    'internal_id' => ModelHelper::generateInternalId(
                        Harvest::class,
                        $licenseId
                    ),
                    'harvest_group_id' => $harvestGroupId,
                    'strain_id' => $strainId,
                    'room_id' => $roomId,
                    'start_at' => $startAt,
                    'finished_at' => $finishedAt,
                    'flower_wet_weight' => Arr::get($item, 'flower_wet_weight', 0),
                    'material_wet_weight' => Arr::get($item, 'other_wet_weight', 0),
                    'flower_dry_weight' => Arr::get($item, 'flower_dry_weight', 0),
                    'material_dry_weight' => Arr::get($item, 'other_dry_weight', 0),
                    'flower_room_id' => $this->getRoomIdByCode($availableRoomArray, Arr::get($item, 'flower_area_id')),
                    'material_room_id' => $this->getRoomIdByCode($availableRoomArray, Arr::get($item, 'other_area_id')),
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => $today,
                ];
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        try {
            DB::beginTransaction();
            if (!empty($newBatchData)) {
                Batch::insert($newBatchData);
            }
            if (!empty($newHarvestGroupData)) {
                HarvestGroup::insert($newHarvestGroupData);
            }
            if (!empty($newHarvestData)) {
                Harvest::insert($newHarvestData);
            }

            // enable used strains
            if (!empty($usedStrainIds)) {
                Strain::query()->where('status', Strain::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedStrainIds))
                    ->update(['status' => Strain::STATUS_ENABLED]);
            }

            // enable used rooms
            if (!empty($usedRoomIds)) {
                Room::query()->where('status', Room::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedRoomIds))
                    ->update(['status' => Room::STATUS_ENABLED]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['success' => $success, 'error' => $error];
    }

    /**
     * @param $stage
     * @return int
     */
    private function getHarvestStatus($stage): int
    {
        switch ($stage) {
            case 'wet':
                return HarvestGroup::STATUS_WET_CONFIRMED;
            case 'cure':
                return HarvestGroup::STATUS_DRY_CONFIRMED;
            case 'finished':
            default:
                return HarvestGroup::STATUS_CLOSED;
        }
    }

    private function getRoomIdByCode(array $availableRoomCodes, ?string $syncCode): ?string
    {
        if (!$syncCode || empty($availableRoomCodes[$syncCode])) {
            return null;
        }

        return $availableRoomCodes[$syncCode];
    }
}
