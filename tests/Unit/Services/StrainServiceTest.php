<?php

namespace Tests\Unit\Services;

use App\Models\Strain;
use App\Services\StrainService;
use Tests\TestCase;

class StrainServiceTest extends TestCase
{
    protected static string $modelName = Strain::class;

    protected function getService(): StrainService
    {
        return app(StrainService::class);
    }
}
