<?php

namespace App\Events\SourceProduct;

use App\Events\Event;
use App\Models\SourceProduct;

class SourceProductSaved extends Event
{
    /**
     * @var SourceProduct
     */
    public SourceProduct $sourceProduct;

    /**
     * @param SourceProduct $sourceProduct
     */
    public function __construct(SourceProduct $sourceProduct)
    {
        $this->sourceProduct = $sourceProduct;
    }
}
