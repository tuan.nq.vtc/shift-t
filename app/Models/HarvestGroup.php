<?php

namespace App\Models;

use Illuminate\Support\Collection;
use App\Models\Traits\{Scope\LicenseFilterable, Scope\StatusFilterable, StoreInRoom};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * @property int status
 * @property int type
 * @property Carbon start_at
 * @property Carbon est_end_at
 * @property Carbon finished_at
 * @property Collection|Harvest[] harvests
 */
class HarvestGroup extends BaseModel
{
    use SoftDeletes;
    use StoreInRoom;
    use LicenseFilterable;
    use StatusFilterable;

    // status
    public const STATUS_OPEN = 0;
    public const STATUS_WET_CONFIRMED = 1;
    public const STATUS_DRY_CONFIRMED = 2;
    public const STATUS_CLOSED = 3;

    public const STATUSES = [
        self::STATUS_OPEN,
        self::STATUS_WET_CONFIRMED,
        self::STATUS_DRY_CONFIRMED,
        self::STATUS_CLOSED,
    ];

    // type
    public const TYPE_EXTRACTION = 0;
    public const TYPE_DRY = 1;

    public const TYPES = [
        self::TYPE_EXTRACTION,
        self::TYPE_DRY,
    ];

    protected $fillable = [
        'license_id',
        'propagation_id',
        'name',
        'room_id',
        'status',
        'type',
        'start_at',
        'est_end_at',
        'finished_at',
        'source_rooms',
    ];

    protected $dates = [
        'start_at',
        'est_end_at',
        'finished_at',
    ];

    protected $casts = [
        'source_rooms' => 'array',
    ];

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function harvests(): HasMany
    {
        return $this->hasMany(Harvest::class);
    }

    public function scopeStrainId(Builder $query, ...$strainId): Builder
    {
        return $query->whereHas('harvests', fn($subQuery) => $subQuery->whereIn('strain_id', $strainId));
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        if (empty($value)) {
            return $query;
        }

        return $query->where('name', 'like', '%' . $value . '%');
    }

    public function scopeClosed(Builder $query, bool $isClosed): Builder
    {
        return $isClosed ? $query->where('status', self::STATUS_CLOSED) :
            $query->whereIn('status', [
                self::STATUS_OPEN,
                self::STATUS_DRY_CONFIRMED,
                self::STATUS_WET_CONFIRMED,
            ]);
    }

    public function scopeType(Builder $builder, $type): Builder
    {
        return $builder->where('type', $type);
    }
}
