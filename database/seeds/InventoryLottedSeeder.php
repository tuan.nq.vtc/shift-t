<?php

use App\Models\{Harvest,
    HarvestGroup,
    Inventory,
    InventoryType,
    License,
    Room,
    RoomType,
    SourceProduct,
    SourceCategory,
    EndProduct
};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class InventoryLottedSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run(): void
    {
        License::isTest()->where('account_holder_id', 'b15b93da-f76c-41b1-ad54-666b9080b996')->get()->each(
            fn(License $license) => $this->iterateLicense($license)
        );
    }

    protected function iterateLicense(License $license): void
    {
        $rooms = Room::licenseId($license->id)
            ->with('subrooms')
            ->whereHas('roomType', fn($query) => $query->where('category', RoomType::CATEGORY_INVENTORY))
            ->get();
        if ($rooms->isEmpty()) {
            return;
        }
        $sourceStateCategory = $license->stateCategories()->whereHas(
            'parent',
            function (Builder $query) {
                $query->where('code', 'intermediate_product');
            }
        )->get();
        if (!$sourceStateCategory || $sourceStateCategory->isEmpty()) {
            return;
        }

        $categoryId = SourceCategory::inRandomOrder()->first()->id;
        foreach ($rooms as $room) {
            try {
                $type = factory(InventoryType::class)->create(
                    [
                        'license_id' => $license->id,
                        'state_category_id' => $sourceStateCategory->random()->id
                    ]
                );
                $inventories = factory(Inventory::class, 2)->create(
                    [
                        'license_id' => $license->id,
                        'room_id' => $room->id,
                        'type_id' => $type->id,
                        'is_lotted' => true,
                    ]
                );
                $endProduct = EndProduct::query()
                    ->where('status',EndProduct::STATUS_ENABLED)
                    ->where('state_code', 'WA')
                    ->get()->random(1)->first();
                /** @var Inventory $inventory */
                foreach ($inventories as $inventory) {
                    $harvestGroup = factory(HarvestGroup::class)->create(
                        [
                            'license_id' => $license->id,
                            'room_id' => $room->id,
                            'status' => HarvestGroup::STATUS_CLOSED
                        ]
                    );
                    $inventory->source()->associate(
                        factory(Harvest::class)->create(
                            [
                                'license_id' => $license->id,
                                'room_id' => $room->id,
                                'harvest_group_id' => $harvestGroup->id,
                            ]
                        )
                    )->save();

                    $inventory->product()->associate($endProduct);
                    $inventory->saveWithoutTraceableModelEvents();
                }
            } catch (\Exception $exception) {
              dd($exception->getTrace());
            }
        }
    }
}
