<?php

namespace App\Facades;


/**
 * Class Trace
 * @package App\Facades
 *
 * @method static \App\Models\Trace create(\App\Models\TraceableModel $resource, string $action):
 *
 * @see \App\Services\TraceService
 */
class Trace extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'trace_service';
    }
}
