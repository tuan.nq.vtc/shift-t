<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{GrowCycle, Plant};
use App\Models\SeedToSale;
use App\Rules\{AvailablePlantRule, IsDisposalRoom, ModelsExist, SubroomBelongToLicense};
use App\Services\{GrowCycleService, PlantService, RoomService, StrainService, SubroomService};
use Illuminate\Support\Facades\DB;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Arr;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Class PlantController
 *
 * @package App\Http\Controllers
 *
 * @property PlantService $service
 */
class PlantController extends CRUDController
{
    private GrowCycleService $growCycleService;
    private RoomService $roomService;
    private StrainService $strainService;
    private SubroomService $subroomService;

    public function __construct(
        GrowCycleService $growCycleService,
        PlantService $plantService,
        RoomService $roomService,
        StrainService $strainService,
        SubroomService $subroomService,
        Request $request
    ) {
        $this->growCycleService = $growCycleService;
        $this->roomService = $roomService;
        $this->strainService = $strainService;
        $this->subroomService = $subroomService;
        parent::__construct($plantService, $request);
    }

    public function getCreateRules(): array
    {
        $licenseId = $this->request->get('license_id');

        return [
            'strain_id' => [
                'required',
                'uuid',
                'exists:App\Models\Strain,id,status,1,deleted_at,NULL',
            ],
            'grow_status' => [
                'required',
                'string',
                new In(SeedToSale::GROW_STATUSES)
            ],
            'propagation_id' => [
                'required',
                'uuid',
                'exists:App\Models\Propagation,id,status,1,deleted_at,NULL',
            ],
            'room_id' => [
                'sometimes',
                'required',
                'exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
            'subroom_id' => [
                'nullable',
                'exists:App\Models\Subroom,id,status,1,deleted_at,NULL',
            ],
            'source_type' => [
                'required',
            ],
            'planted_at' => ['required'],
            'uuid',

            'grow_cycle_id' => [
                'required',
            ],
            'license_id' => [],
            'exists:App\Models\GrowCycle,id,deleted_at,NULL',
        ];
    }

    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id');

        return [
            'strain_id' => ['sometimes', 'required'],
            'grow_status' => ['sometimes', 'required'],
            'propagation_id' => ['sometimes', 'required'],
            'room_id' => [
                'sometimes',
                'required',
                'exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
            'source_type' => ['sometimes', 'required'],
            'planted_at' => ['sometimes', 'required'],
            'grow_cycle_id' => ['sometimes', 'required'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function search(): JsonResponse
    {
        return response()->json(
            $this->service->search(
                $this->request->get('code', ''),
                Criteria::createFromRequest($this->request)
            )
        );
    }

    public function getDetail($id)
    {
        return response()->json($this->service->getDetail($id));
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function bulkDestroy(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $rules = [
            'plant_ids' => ['required', 'array', new AvailablePlantRule($licenseId)],
            'reason' => ['required', 'in:infestation,quality control,unhealthy,mandated'],
            'room_id' => ['required', new IsDisposalRoom($licenseId)],
            'comment' => ['nullable', 'string'],
            'quarantine_start' => ['date_format:Y-m-d H:i:s', 'required'],
            'license_id' => ['required'],
            'scheduled_by_id' => ['required', 'uuid', 'exists:App\Models\User,id',],
        ];
        $data = $this->validate($this->request, $rules);
        return response()->json(
            $this->service->movePlantsToDisposal(
                $this->service->getEnumerable(
                    Arr::get($data, 'plant_ids'),
                    ['mother', 'strain',]
                ),
                $this->roomService->get(Arr::get($data, 'room_id')),
                $data
            )
        );
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function makeMothers(): JsonResponse
    {
        $rules = [
            'plant_ids' => [
                'required',
                'array',
                new AvailablePlantRule($this->request->get('license_id'))
            ],
        ];

        return response()->json(
            $this->service->makeMothers(
                $this->service->getEnumerable(
                    Arr::get($this->validate($this->request, $rules), 'plant_ids')
                )
            )
        );
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function changeGrowStatus(): JsonResponse
    {
        $rules = [
            'plant_ids' => [
                'required',
                'array',
                new AvailablePlantRule($this->request->get('license_id')),
            ],
            'grow_status' => [
                'required',
                'string',
//                Rule::in($this->growStatusService->getGrowStatusForPlant()->pluck('id')->toArray())
            ],
        ];
        $data = $this->validate($this->request, $rules);
        return response()->json(
            $this->service->updatePlantsGrowStatus(
                $this->service->getEnumerable(Arr::get($data, 'plant_ids')),
                Arr::get($data, 'grow_status')
            )
        );
    }

    /**
     * @param string|null $growCycleId
     *
     * @return JsonResponse
     *
     * @throws ValidationException|Throwable
     */
    public function moveToGrowCycle($growCycleId = null): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        try {
            DB::beginTransaction();
            if ($growCycleId) {
                $growCycle = $this->growCycleService->get($growCycleId);
            } else {
                $growCycleRules = [
                    'license_id' => [
                        'required',
                        'uuid',
                    ],
                    'name' => [
                        'required',
                        'string',
                        'max:255',
                    ],
                    'grow_status' => [
                        'required',
                        'string',
                        new In(SeedToSale::GROW_STATUSES)
                    ],
                ];
                $growCycle = $this->growCycleService->create($this->validate($this->request, $growCycleRules));
            }

            $this->validate(
                $this->request,
                [
                    'plant_ids' => [
                        'required',
                        'array',
                        new AvailablePlantRule($licenseId),
                    ],
                ]
            );

            $growCycle = $this->service->movePlantsToGrowCycle($growCycle, $this->request->get('plant_ids'));
            DB::commit();

            return response()->json($growCycle, Response::HTTP_CREATED);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @return JsonResponse
     *
     * @throws ValidationException|Throwable
     */
    public function moveToRoom(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');
        $rules = [
            'license_id' => [
                'required',
                'uuid',
            ],
            'plant_ids' => [
                'required',
                'array',
                new ModelsExist(
                    'App\Models\Plant',
                    'id',
                    ['license_id' => $licenseId]
                ),
                function ($attribute, $value, $fail) {
                    $plants = Plant::find(Arr::wrap($value));

                    $destroyedPlants = $plants->filter(fn(Plant $plant) => $plant->isDestroyed());
                    $harvestedPlants = $plants->filter(fn(Plant $plant) => $plant->isHarvested());

                    $errMsg = $destroyedPlants->isEmpty() ? '' :
                        $destroyedPlants->pluck('sync_code')
                            ->map(
                                fn($syncCode) => 'The plant '.$syncCode." is destroyed and cannot be moved. <br/> "
                            )
                            ->implode("");
                    $errMsg .= $harvestedPlants->isEmpty() ? '' : $harvestedPlants->pluck('sync_code')
                        ->map(fn($syncCode) => 'The plant '.$syncCode." is harvested and cannot be moved. <br/> ")
                        ->implode("");

                    if (!blank($errMsg)) {
                        return $fail($errMsg);
                    }
                }
            ],
            'room_id' => [
                'required',
                'uuid',
                'exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
            'subroom_id' => [
                'nullable',
                new SubroomBelongToLicense($roomId, $licenseId),
            ],
        ];

        $data = $this->validate($this->request, $rules);

        return response()->json(
            $this->service->movePlantsToRoom(
                $this->service->getEnumerable(
                    Arr::get($data, 'plant_ids'),
                    ['growCycle', 'growCycle.plantGroups', 'plantGroup']
                ),
                $this->roomService->get(Arr::get($data, 'room_id')),
                ($subroomId = Arr::get($data, 'subroom_id')) ? $this->subroomService->get($subroomId) : null
            )
        );
    }

    /**
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function editStrain(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $rules = [
            'license_id' => [
                'required',
                'uuid',
            ],
            'plant_ids' => [
                'required',
                'array',
                new AvailablePlantRule($licenseId),
            ],
            'strain_id' => [
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
        ];

        $data = $this->validate($this->request, $rules);
        $strain = $this->strainService->get(Arr::get($data, 'strain_id'));

        return response()->json(
            $this->service->editPlantsStrain(
                $this->service->getEnumerable(Arr::get($data, 'plant_ids'), ['growCycle', 'plantGroup']),
                $strain
            )
        );
    }

    public function listForHarvest(string $target = '')
    {
        $mappings = [
            'grow-cycle' => 'App\Models\GrowCycle',
            'room' => 'App\Models\Room',
        ];
        $modelClass = $mappings[$target] ?? null;
        if (!$modelClass) {
            throw new GeneralException(__('exceptions.plants.list_for_harvest.target_invalid', ['target' => $target]));
        }
        $rules = [
            'ids' => [
                'required',
                'array',
                new ModelsExist($modelClass, 'id', ['license_id' => $this->request->get('license_id', '')]),
            ],
        ];
        $data = $this->validate($this->request, $rules);
        $plants = $this->service->getPlantForAdditive(
            $target,
            Arr::get($data, 'ids'),
            [
                'strain',
                'growCycle',
                'plantGroup',
                'motherPropagation',
                'propagations',
                'mother',
            ]
        );
        return response()->json($plants);
    }
    
    public function updatePlant(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');
        
        $rules = [
            'plant_ids' => [
                'required',
                'array',
                new AvailablePlantRule($licenseId),
            ],
            'strain_id' => [
                'sometimes', 
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
            'grow_status' => ['sometimes', 'required', 'string'],
            'room_id' => [
                'sometimes',
                'required_without:subroom_id',
                'uuid',
                'exists:App\Models\Room,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
            'subroom_id' => [
                'sometimes',
                'nullable',
                'uuid',
                new SubroomBelongToLicense($roomId, $licenseId),
            ],
            'grow_cycle_id' => [
                'sometimes', 
                'required',
                'uuid',
                'exists:App\Models\GrowCycle,id,license_id,'.$licenseId.',deleted_at,NULL',
            ],
            'is_mother' => ['sometimes', 'required', 'boolean'],
        ];
        
        $data = $this->validate($this->request, $rules);
        
        return response()->json(
                        $this->service->updatePlants(
                                $this->service->getEnumerable(Arr::get($data, 'plant_ids')),
                                $data
                        )
        );
    }
}
