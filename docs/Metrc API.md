###Facilities

#### Terminologies:
* Types of license facility:
    * Cultivator
    * Cultivator Processor only
    * Distributor
    * Distributor Transporter only
    * Manufacturer
    * Manufacturer Packager only
    * Retailer
    * Testing Laboratory
    * Micro business

#### APIs:
 * GET /facilities/v1


###Harvests
 * GET /harvests/v1/{id}
 * GET /harvests/v1/active
 * GET /harvests/v1/onhold
 * GET /harvests/v1/inactive
 * GET /harvests/v1/waste/types
 * POST /harvests/v1/create/packages
 * POST /harvests/v1/create/packages/testing
 * PUT /harvests/v1/move
 * POST /harvests/v1/removewaste
 * POST /harvests/v1/finish
 * POST /harvests/v1/unfinish


###Items
 * GET /items/v1/{id}
 * GET /items/v1/active
 * GET /items/v1/categories
 * GET /items/v1/brands
 * POST /items/v1/create
 * POST /items/v1/update
 * DELETE /items/v1/{id}


###Lab Tests
 * GET /labtests/v1/states
 * GET /labtests/v1/types
 * GET /labtests/v1/results
 * POST /labtests/v1/record
 * PUT /labtests/v1/labtestdocument
 * PUT /labtests/v1/results/release


###Locations
 * GET /locations/v1/{id}
 * GET /locations/v1/active
 * GET /locations/v1/types
 * POST /locations/v1/create
 * POST /locations/v1/update
 * DELETE /locations/v1/{id}
 * GET /rooms/v1/{id}
 * GET /rooms/v1/active
 * POST /rooms/v1/create
 * POST /rooms/v1/update
 * DELETE /rooms/v1/{id}


###Packages
 * GET /packages/v1/{id}
 * GET /packages/v1/{label}
 * GET /packages/v1/active
 * GET /packages/v1/onhold
 * GET /packages/v1/inactive
 * GET /packages/v1/types
 * GET /packages/v1/adjust/reasons
 * POST /packages/v1/create
 * POST /packages/v1/create/testing
 * POST /packages/v1/create/plantings
 * POST /packages/v1/change/item
 * PUT /packages/v1/change/note
 * POST /packages/v1/change/rooms
 * POST /packages/v1/change/locations
 * POST /packages/v1/adjust
 * POST /packages/v1/finish
 * POST /packages/v1/unfinish
 * POST /packages/v1/remediate


###Plant Batches
 * GET /plantbatches/v1/{id}
 * GET /plantbatches/v1/active
 * GET /plantbatches/v1/inactive
 * GET /plantbatches/v1/types
 * POST /plantbatches/v1/createplantings
 * POST /plantbatches/v1/create/plantings
 * POST /plantbatches/v1/split
 * POST /plantbatches/v1/create/packages/frommotherplant
 * POST /plantbatches/v1/changegrowthphase
 * PUT /plantbatches/v1/moveplantbatches
 * POST /plantbatches/v1/additives
 * POST /plantbatches/v1/destroy


###Plants

#### Terminologies:
 * “Immature plant” or “immature” means a cannabis plant that is not flowering.
 * Cây chưa trưởng thành nghĩa là cây chưa ra hoa
 
 * “Flowering” means that a cannabis plant has formed a mass of pistils measuring greater than one half inch wide at its widest point
 * Cây trưởng thành  nghĩa là cây có một khối nhụy hoa rộng hơn một nửa inch tại điểm rộng nhất của nó



#### APIs:
 * GET /plants/v1/{id}
 * GET /plants/v1/{label}
 * GET /plants/v1/vegetative
 * GET /plants/v1/flowering
 * GET /plants/v1/onhold
 * GET /plants/v1/inactive
 * GET /plants/v1/additives
 * GET /plants/v1/growthphases
 * GET /plants/v1/additives/types
 * GET /plants/v1/waste/methods
 * GET /plants/v1/waste/reasons
 * POST /plants/v1/moveplants
 * POST /plants/v1/changegrowthphases
 * POST /plants/v1/destroyplants
 * POST /plants/v1/additives
 * POST /plants/v1/additives/byroom
 * POST /plants/v1/additives/bylocation
 * POST /plants/v1/create/plantings
 * POST /plants/v1/create/plantbatch/packages
 * POST /plants/v1/manicureplants
 * POST /plants/v1/harvestplants


###Sales
 * GET /sales/v1/customertypes
 * GET /sales/v1/receipts
 * GET /sales/v1/receipts/{id}
 * POST /sales/v1/receipts
 * PUT /sales/v1/receipts
 * DELETE /sales/v1/receipts/{id}
 * GET /sales/v1/transactions
 * GET /sales/v1/transactions/{date}
 * GET /sales/v1/transactions/{salesDateStart}/{salesDateEnd}
 * POST /sales/v1/transactions/{date}
 * PUT /sales/v1/transactions/{date}


###Strains
 * GET /strains/v1/{id}
 * GET /strains/v1/active
 * POST /strains/v1/create
 * POST /strains/v1/update
 * DELETE /strains/v1/{id}


###Transfers
 * GET /transfers/v1/incoming
 * GET /transfers/v1/outgoing
 * GET /transfers/v1/rejected
 * GET /transfers/v1/{id}/deliveries
 * GET /transfers/v1/delivery/{id}/packages
 * GET /transfers/v1/delivery/{id}/packages/wholesale
 * GET /transfers/v1/delivery/package/{id}/requiredlabtestbatches
 * GET /transfers/v1/delivery/packages/states
 * POST /transfers/v1/external/incoming
 * PUT /transfers/v1/external/incoming
 * DELETE /transfers/v1/external/incoming/{id}
 * GET /transfers/v1/templates
 * GET /transfers/v1/templates/{id}/deliveries
 * GET /transfers/v1/templates/delivery/{id}/packages
 * POST /transfers/v1/templates
 * PUT /transfers/v1/templates
 * DELETE /transfers/v1/templates/{id}
 * GET /transfers/v1/types


###Units Of Measure
 * GET /unitsofmeasure/v1/active
