<?php

namespace App\Services;

use App\Events\Manifest\ManifestCreated;
use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{Driver,
    Inventory,
    InventoryType,
    Licensee,
    Manifest,
    ManifestItem,
    Plant,
    PlantGroup,
    Propagation,
    QASample,
    SeedToSale,
    SourceProduct,
    StateCategory,
    Vehicle};
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\{Arr, Collection, Facades\DB, Facades\Event};
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * @package App\Services
 */
class ManifestService extends BaseService
{
    protected array $relations = [
        'client:id,name',
    ];
    protected array $counts = ['items'];

    public function __construct(Manifest $manifest)
    {
        parent::__construct($manifest);
    }

    public function get(string $id, $withRelation = true)
    {
        /** @var Manifest $manifest */
        $manifest = $this->newQuery()->findOrFail($id);
        $items = $manifest->loadCount('items')->load(
            [
                'client',
                'transporter:id,name,code',
                'driver:id,name',
                'driverHelper:id,name',
                'vehicle:id,name',
                'items.strain:id,name',
                'items.resource'
            ]
        )->items;
        $inventoryTypeIds = $items->filter(
            function ($item) {
                return $item->resource_type === Inventory::class;
            }
        )->pluck('resource.type_id');
        $stateCategoryNamePerType = InventoryType::whereIn('id', $inventoryTypeIds)
            ->with('stateCategory')->get()
            ->pluck('stateCategory.name', 'id')->toArray();
        $items->map(
            function ($item) use ($stateCategoryNamePerType) {
                $sourceType = null;
                switch ($item->resource_type) {
                    case Plant::class:
                    case Propagation::class:
                        $sourceType = $item->resource->source_type;
                        break;
                    case QASample::class:
                        $inventory = $item->resource->inventory;
                        $item->lot_code = $inventory->sync_code;
                        $item->product_name = optional($inventory->product)->name;
                        $item->is_retest = $inventory->qa_status === Inventory::QA_STATUS_SAMPLED_RETEST;
                        break;
                    case Inventory::class:
                        if (isset($stateCategoryNamePerType[$item->resource->type_id])) {
                            $sourceType = $stateCategoryNamePerType[$item->resource->type_id];
                        }
                        break;
                }
                $item->source_type = $sourceType;
                return $item;
            }
        );

        return $manifest;
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function getOpenItems(Criteria $criteria): Collection
    {
        $query = $this->newQuery(false)
            ->whereIn('status', [Manifest::STATUS_OPEN, Manifest::STATUS_VOIDED])
            ->scopes($this->loadScopes($criteria->getFilters()))
            ->with(['client', 'items.strain:id,name',])->limit($criteria->getLimit());

        return $this->applyOrderBy($query, $criteria->getSorts())->get()->each(
            function (Manifest $manifest) {
                $contents = [];
                $strains = [];
                foreach ($manifest->items as $item) {
                    if ($item->type && !in_array($item->type, $contents)) {
                        $contents[] = $item->type;
                    }
                    if ($item->strain && !in_array($item->strain, $strains)) {
                        $strains[] = $item->strain;
                    }
                }
                $manifest->contents = $contents;
                $manifest->strains = $strains;
                return $manifest;
            }
        )->makeHidden('items');
    }

    /**
     * @param Criteria $criteria
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $paginator = parent::list($criteria);
        $paginator->getCollection()->transform(fn(Manifest $manifest) => $this->listTransform($manifest));

        return $paginator;
    }

    /**
     * @param Manifest $manifest
     * @return Manifest
     */
    private function listTransform(Manifest $manifest)
    {
        $contents = [];
        foreach ($manifest->items as $item) {
            if (!in_array($item->type, $contents)) {
                $contents[] = $item->type;
            }
        }
        $manifest->contents = $contents;
        return $manifest;
    }

    /**
     * @param array $ids
     * @return Collection
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkPublish(array $ids): Collection
    {
        // validate
        $manifests = $this->newQuery()->whereIn('id', $ids)->get();
        if (count($ids) !== $manifests->count()) {
            throw ValidationException::withMessages(
                [
                    'ids' => __('exceptions.manifest.publish.invalid_record')
                ]
            );
        }

        $errorMessages = [];
        foreach ($manifests as $manifest) {
            if (!in_array($manifest->status, [Manifest::STATUS_OPEN, Manifest::STATUS_VOIDED])) {
                $errorMessages[] = __(
                    'exceptions.manifest.publish.status_not_allowed',
                    ['code' => $manifest->sync_code]
                );
            }
        }
        if (!empty($errorMessages)) {
            throw ValidationException::withMessages(['status' => $errorMessages]);
        }

        // execute
        $publishedRecords = collect();
        $errorItems = [];
        foreach ($manifests as $manifest) {
            try {
                $publishedRecords->push($this->doPublish($manifest));
            } catch (Exception $e) {
                // #todo log error
                $errorItems[] = $manifest->sync_code;
            }
        }
        if (!empty($errorItems)) {
            throw new GeneralException(
                __(
                    'exceptions.manifest.publish.action_failed',
                    ['count' => $publishedRecords->count(), 'errors' => implode(',', $errorItems)]
                )
            );
        }

        return $publishedRecords;
    }

    /**
     * @param string $id
     * @return Manifest
     * @throws Throwable
     */
    public function publish(string $id): Manifest
    {
        /** @var Manifest $manifest */
        $manifest = $this->newQuery()->findOrFail($id);
        // validate
        if (!in_array($manifest->status, [Manifest::STATUS_OPEN, Manifest::STATUS_VOIDED])) {
            throw ValidationException::withMessages(
                [
                    'status' => __(
                        'exceptions.manifest.publish.status_not_allowed',
                        ['code' => $manifest->sync_code]
                    )
                ]
            );
        }

        return $this->doPublish($manifest);
    }

    /**
     * @param array $ids
     * @return Collection
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkVoid(array $ids): Collection
    {
        // validate
        $manifests = $this->newQuery()->whereIn('id', $ids)->get();
        if (count($ids) !== $manifests->count()) {
            throw ValidationException::withMessages(
                [
                    'ids' => __('exceptions.manifest.void.invalid_record')
                ]
            );
        }

        $errorMessages = [];
        foreach ($manifests as $manifest) {
            if (!in_array($manifest->status, [Manifest::STATUS_IN_TRANSIT, Manifest::STATUS_READY_FOR_PICKUP])) {
                $errorMessages[] = __(
                    'exceptions.manifest.void.status_not_allowed',
                    ['code' => $manifest->sync_code]
                );
            }
        }
        if (!empty($errorMessages)) {
            throw ValidationException::withMessages(['status' => $errorMessages]);
        }

        // execute
        $voidedRecords = collect();
        $errorItems = [];
        foreach ($manifests as $manifest) {
            try {
                $voidedManifest = DB::transaction(
                    function () use ($manifest) {
                        throw_unless(
                            $manifest->update(['status' => Manifest::STATUS_VOIDED]),
                            __('exceptions.manifest.update_failed')
                        );

                        // update items
                        $plantGroupIds = [];
                        $propagationIds = [];
                        foreach ($manifest->items as $item) {
                            $item->update(['status' => ManifestItem::STATUS_PENDING]);
                            switch ($item->resource_type) {
                                case Plant::class:
                                    $plant = $item->resource;
                                    $plantGroupIds[$plant->plant_group_id] = 1;

                                    break;
                                case Propagation::class:
                                    /** @var Propagation $propagation */
                                    $propagation = $item->resource;
                                    $propagationIds[] = $propagation->id;
                                    $propagation->increaseQty($item->quantity);
                                    $propagation->calculateAllocatedQuantity();
                                    $propagation->save();

                                    break;
                                case QASample::class:
                                    /** @var QASample $qaSample */
                                    $qaSample = $item->resource;
                                    /** @var Inventory $inventory */
                                    if ($inventory = $qaSample->inventory) {
                                        $inventory->increaseQty($item->quantity);
                                        $inventory->calculateAllocatedQuantity();
                                        $inventory->save();
                                    }
                                    break;
                                case Inventory::class:
                                    /** @var Inventory $inventory */
                                    $inventory = $item->resource;
                                    $inventory->increaseQty($item->quantity);
                                    $inventory->calculateAllocatedQuantity();
                                    $inventory->save();
                                    break;
                            }
                        }

                        if (!empty($plantGroupIds)) {
                            $manifest->inventories->whereIn('source_id', array_keys($plantGroupIds))
                                ->where('source_type', PlantGroup::class)->each->delete();
                        }
                        if (!empty($propagationIds)) {
                            $manifest->inventories->whereIn('source_id', $propagationIds)
                                ->where('source_type', Propagation::class)->each->delete();
                        }
                        $manifest->inventories()->detach();

                        return $manifest;
                    }
                );
                $voidedRecords->push($voidedManifest);
            } catch (Exception $e) {
                //#todo log exception
                $errorItems[] = $manifest->sync_code;
            }
        }
        if (!empty($errorItems)) {
            throw new GeneralException(
                __('exceptions.manifest.void.action_failed', $voidedRecords->count(), implode(',', $errorItems))
            );
        }

        return $voidedRecords;
    }

    /**
     * @param array $ids
     * @return Collection
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkCancel(array $ids): Collection
    {
        // validate
        $manifests = $this->newQuery()->whereIn('id', $ids)->get();
        if (count($ids) !== $manifests->count()) {
            throw ValidationException::withMessages(
                [
                    'ids' => __('exceptions.manifest.cancel.invalid_record')
                ]
            );
        }

        $errorMessages = [];
        foreach ($manifests as $manifest) {
            if (!in_array($manifest->status, [Manifest::STATUS_OPEN, Manifest::STATUS_VOIDED])) {
                $errorMessages[] = __(
                    'exceptions.manifest.cancel.status_not_allowed',
                    ['code' => $manifest->sync_code]
                );
            }
        }
        if (!empty($errorMessages)) {
            throw ValidationException::withMessages(['status' => $errorMessages]);
        }

        // execute
        $canceledRecords = collect();
        $errorItems = [];
        foreach ($manifests as $manifest) {
            try {
                $canceledManifest = DB::transaction(
                    function () use ($manifest) {
                        throw_unless(
                            $manifest->update(['status' => Manifest::STATUS_CANCELED]),
                            __('exceptions.manifest.update_failed')
                        );

                        // update items
                        foreach ($manifest->items as $item) {
                            $item->update(['status' => ManifestItem::STATUS_CANCELED]);
                            switch ($item->resource_type) {
                                case Plant::class:
                                    $plant = $item->resource;
                                    $plant->update(['is_manifested' => false]);
                                    break;
                                case Propagation::class:
                                    /** @var Propagation $propagation */
                                    $propagation = $item->resource;

                                    $propagation->calculateAllocatedQuantity();
                                    $propagation->save();
                                    break;
                                case QASample::class:
                                    /** @var QASample $qaSample */
                                    $qaSample = $item->resource;
                                    $qaSample->update(['status' => QASample::STATUS_OPEN]);
                                    if ($item->resource->inventory) {
                                        // #todo: inventory logic should be handled when QA Sample status is updated
                                        $qaCount = QASample::query()->where(
                                            'inventory_id',
                                            $qaSample->inventory_id
                                        )->count();
                                        $qaStatus = $qaCount > 1 ? Inventory::QA_STATUS_SAMPLED_RETEST : Inventory::QA_STATUS_SAMPLED_TEST;
                                        $item->resource->inventory->update(
                                            ['qa_status' => $qaStatus]
                                        );
                                    }
                                    break;
                                case Inventory::class:
                                    /** @var Inventory $inventory */
                                    $inventory = $item->resource;
                                    $inventory->calculateAllocatedQuantity();
                                    $inventory->save();
                                    break;
                            }
                        }
                        $manifest->inventories()->detach();

                        return $manifest;
                    }
                );
                $canceledRecords->push($canceledManifest);
            } catch (Exception $e) {
                $errorItems[] = $manifest->sync_code;
            }
        }
        if (!empty($errorItems)) {
            throw new GeneralException(
                __('exceptions.manifest.cancel.action_failed', $canceledRecords->count(), implode(',', $errorItems))
            );
        }

        return $canceledRecords;
    }

    /**
     * @param array $ids
     * @return int
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkDelete(array $ids): int
    {
        // validate
        $manifests = $this->newQuery()->whereIn('id', $ids)->get();
        if (count($ids) !== $manifests->count()) {
            throw ValidationException::withMessages(
                [
                    'ids' => __('exceptions.manifest.delete.invalid_record')
                ]
            );
        }

        $errorMessages = [];
        foreach ($manifests as $manifest) {
            if ($manifest->status !== Manifest::STATUS_CANCELED) {
                $errorMessages[] = __(
                    'exceptions.manifest.delete.status_not_allowed',
                    ['code' => $manifest->sync_code]
                );
            }
        }
        if (!empty($errorMessages)) {
            throw ValidationException::withMessages(['status' => $errorMessages]);
        }

        // execute
        $success = 0;
        $errorItems = [];
        foreach ($manifests as $manifest) {
            try {
                DB::transaction(
                    function () use ($manifest) {
                        // delete item
                        $manifest->items()->delete();
                        throw_unless(
                            $manifest->delete(),
                            __('exceptions.manifest.delete_failed', ['code' => $manifest->sync_code])
                        );
                    }
                );
                $success++;
            } catch (Exception $e) {
                $errorItems[] = $manifest->sync_code;
            }
        }
        if (!empty($errorItems)) {
            throw new GeneralException(
                __('exceptions.manifest.delete.action_failed', $success, implode(',', $errorItems))
            );
        }

        return $success;
    }

    /**
     * @param array $ids
     * @return Collection
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkReceive(array $ids): Collection
    {
        // validate
        $manifests = $this->newQuery()->whereIn('id', $ids)->get();
        if (count($ids) !== $manifests->count()) {
            throw ValidationException::withMessages(
                [
                    'ids' => __('exceptions.manifest.receive.invalid_record')
                ]
            );
        }

        $errorMessages = [];
        foreach ($manifests as $manifest) {
            if (!in_array($manifest->status, [Manifest::STATUS_IN_TRANSIT, Manifest::STATUS_READY_FOR_PICKUP])) {
                $errorMessages[] = __(
                    'exceptions.manifest.receive.status_not_allowed',
                    ['code' => $manifest->sync_code]
                );
            }
        }
        if (!empty($errorMessages)) {
            throw ValidationException::withMessages(['status' => $errorMessages]);
        }

        // execute
        $receivedRecords = collect();
        $errorItems = [];
        foreach ($manifests as $manifest) {
            try {
                throw_unless(
                    $manifest->update(['status' => Manifest::STATUS_RECEIVED]),
                    __('exceptions.manifest.update_failed')
                );;
                $receivedRecords->push($manifest);
            } catch (Exception $e) {
                //#todo log exception
                $errorItems[] = $manifest->sync_code;
            }
        }
        if (!empty($errorItems)) {
            throw new GeneralException(
                __('exceptions.manifest.receive.action_failed', $receivedRecords->count(), implode(',', $errorItems))
            );
        }

        return $receivedRecords;
    }

    /**
     * @param array $ids
     * @return Collection
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkReject(array $ids): Collection
    {
        // validate
        $manifests = $this->newQuery()->whereIn('id', $ids)->get();
        if (count($ids) !== $manifests->count()) {
            throw ValidationException::withMessages(
                [
                    'ids' => __('exceptions.manifest.reject.invalid_record')
                ]
            );
        }

        $errorMessages = [];
        foreach ($manifests as $manifest) {
            if (!in_array($manifest->status, [Manifest::STATUS_IN_TRANSIT, Manifest::STATUS_READY_FOR_PICKUP])) {
                $errorMessages[] = __(
                    'exceptions.manifest.reject.status_not_allowed',
                    ['code' => $manifest->sync_code]
                );
            }
        }
        if (!empty($errorMessages)) {
            throw ValidationException::withMessages(['status' => $errorMessages]);
        }

        // execute
        $rejectedRecords = collect();
        $errorItems = [];
        foreach ($manifests as $manifest) {
            try {
                $rejectedManifest = DB::transaction(
                    function () use ($manifest) {
                        throw_unless(
                            $manifest->update(['status' => Manifest::STATUS_REJECTED]),
                            __('exceptions.manifest.update_failed')
                        );

                        // update items
                        $plantGroupIds = [];
                        $propagationIds = [];
                        foreach ($manifest->items as $item) {
                            $item->update(['status' => ManifestItem::STATUS_PENDING]);
                            switch ($item->resource_type) {
                                case Plant::class:
                                    $plant = $item->resource;
                                    $plantGroupIds[$plant->plant_group_id] = 1;

                                    break;
                                case Propagation::class:
                                    /** @var Propagation $propagation */
                                    $propagation = $item->resource;
                                    $propagationIds[] = $propagation->id;
                                    $propagation->increaseQty($item->quantity);
                                    $propagation->calculateAllocatedQuantity();
                                    $propagation->save();

                                    break;
                                case QASample::class:
                                    /** @var QASample $qaSample */
                                    $qaSample = $item->resource;
                                    /** @var Inventory $inventory */
                                    if ($inventory = $qaSample->inventory) {
                                        $inventory->increaseQty($item->quantity);
                                        $inventory->calculateAllocatedQuantity();
                                        $inventory->save();
                                    }
                                    break;
                                case Inventory::class:
                                    /** @var Inventory $inventory */
                                    $inventory = $item->resource;
                                    $inventory->increaseQty($item->quantity);
                                    $inventory->calculateAllocatedQuantity();
                                    $inventory->save();
                                    break;
                            }
                        }

                        if (!empty($plantGroupIds)) {
                            $manifest->inventories->whereIn('source_id', array_keys($plantGroupIds))
                                ->where('source_type', PlantGroup::class)->each->delete();
                        }
                        if (!empty($propagationIds)) {
                            $manifest->inventories->whereIn('source_id', $propagationIds)
                                ->where('source_type', Propagation::class)->each->delete();
                        }
                        $manifest->inventories()->detach();

                        return $manifest;
                    }
                );
                $rejectedRecords->push($rejectedManifest);
            } catch (Exception $e) {
                //#todo log exception
                $errorItems[] = $manifest->sync_code;
            }
        }
        if (!empty($errorItems)) {
            throw new GeneralException(
                __('exceptions.manifest.reject.action_failed', $rejectedRecords->count(), implode(',', $errorItems))
            );
        }

        return $rejectedRecords;
    }

    /**
     * @param $id
     * @return Manifest
     * @throws Throwable
     * @throws ValidationException
     */
    public function markInTransit(string $id): Manifest
    {
        /** @var Manifest $manifest */
        $manifest = $this->newQuery()->findOrFail($id);

        if ($manifest->status !== Manifest::STATUS_READY_FOR_PICKUP) {
            throw ValidationException::withMessages(
                ['status' => __('exceptions.manifest.mark_in_transit.status_not_allowed')]
            );
        }

        throw_unless(
            $manifest->update(['status' => Manifest::STATUS_IN_TRANSIT]),
            new GeneralException(__('exceptions.manifest.mark_in_transit.action_failed'))
        );

        return $manifest;
    }

    /**
     * @param array $data
     * @return Model
     * @throws GeneralException
     * @throws Throwable
     * @throws ValidationException
     */
    public function create(array $data): Model
    {
        $this->validateDetailData($data);
        $items = Arr::pull($data, 'items', []);
        $this->validateItems(Arr::get($data, 'type'), $items);
        try {
            DB::beginTransaction();

            /** @var Manifest $manifest */
            $manifest = parent::create(array_merge($data, ['method' => Manifest::METHOD_OUTBOUND]));

            foreach ($items as $itemData) {
                $this->createItem($manifest, $itemData);
            }

            $manifest->update(['invoice_total' => $manifest->items()->sum('total')]);

            // Dispatch manifest
            Event::dispatch(new ManifestCreated($manifest));

            DB::commit();
            return $manifest->refresh();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param $id
     * @param array $data
     * @return Model
     * @throws Throwable
     * @throws ValidationException
     */
    public function update($id, array $data): Model
    {
        /** @var Manifest $manifest */
        $manifest = $this->newQuery()->findOrFail($id);

        $items = Arr::get($data, 'items');
        $shouldUpdateItems = null !== $items;
        $updatingData = $this->validateUpdatingData($manifest, $data, $shouldUpdateItems);

        try {
            DB::beginTransaction();

            $manifest->fill($updatingData);
            if ($manifest->isDirty() && true !== $manifest->save()) {
                throw new Exception(__('exceptions.manifest.update_failed'));
            }

            if ($shouldUpdateItems) {
                $updatedIds = [];
                $existingItems = $manifest->items;
                foreach ($items as $itemData) {
                    if (!($itemType = Arr::get($itemData, 'type')) || !($id = Arr::get($itemData, 'resource_id'))) {
                        continue;
                    }
                    $existingItem = $existingItems->where('type', $itemType)->where('resource_id', $id)->first();
                    if ($existingItem) {
                        $this->updateItem($existingItem, $itemData);
                        $updatedIds[$existingItem->id] = 1;
                    } else {
                        $this->createItem($manifest, $itemData);
                    }
                }

                foreach ($existingItems->whereNotIn('id', array_keys($updatedIds)) as $item) {
                    $this->deleteItem($item);
                }
            }

            $manifest->update(['invoice_total' => $manifest->items()->sum('total')]);
            DB::commit();
            return $manifest->refresh();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param $id
     * @param array $items
     * @return Model
     * @throws Throwable
     * @throws ValidationException
     */
    public function addItems($id, array $items): Model
    {
        /** @var Manifest $manifest */
        $manifest = $this->newQuery()->findOrFail($id);

        $existingItems = $manifest->items;
        $this->validateItems($manifest->type, $items, $existingItems);

        try {
            DB::beginTransaction();

            foreach ($items as $itemData) {
                if (!($itemType = Arr::get($itemData, 'type')) || !($id = Arr::get($itemData, 'resource_id'))) {
                    continue;
                }
                $existingItem = $existingItems->where('type', $itemType)->where('resource_id', $id)->first();
                if ($existingItem) {
                    continue;
                }
                $this->createItem($manifest, $itemData);
            }

            $manifest->update(['invoice_total' => $manifest->items()->sum('total')]);
            DB::commit();
            return $manifest->refresh();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param $data
     * @throws ValidationException
     */
    private function validateDetailData($data): void
    {
        $errorMessages = [];
        if (!in_array(Arr::get($data, 'type'), Manifest::TYPES)) {
            $errorMessages['type'] = __('exceptions.manifest.invalid_type');
        }
        $transportType = Arr::get($data, 'transport_type');
        if (!in_array($transportType, Manifest::TRANSPORT_TYPES)) {
            $errorMessages['transport_type'] = __('exceptions.manifest.invalid_transport_type');
        }
        $clientId = Arr::get($data, 'client_id');
        if (!$clientId || !Licensee::find($clientId)) {
            $errorMessages['client_id'] = __('exceptions.manifest.client_not_found');
        }
        switch ($transportType) {
            case Manifest::TRANSPORT_TYPE_DELIVERY:
            case Manifest::TRANSPORT_TYPE_PICKUP:
                if (!($driverId = Arr::get($data, 'driver_id'))
                    || !Driver::licenseId($this->licenseId)->find($driverId)) {
                    $errorMessages['driver_id'] = __('exceptions.manifest.driver_not_found');
                }
                if (($driverHelperId = Arr::get($data, 'driver_helper_id'))
                    && !Driver::licenseId($this->licenseId)->find($driverHelperId)) {
                    $errorMessages['driver_helper_id'] = __('exceptions.manifest.driver_helper_not_found');
                }
                if (!($vehicleId = Arr::get($data, 'vehicle_id'))
                    || !Vehicle::licenseId($this->licenseId)->find($vehicleId)) {
                    $errorMessages['vehicle_id'] = __('exceptions.manifest.vehicle_not_found');
                }
                break;
            case Manifest::TRANSPORT_TYPE_LICENSED_TRANSPORTER:
                $transporterId = Arr::get($data, 'transporter_id');
                if (!$transporterId || !Licensee::find($transporterId)) {
                    $errorMessages['transporter_id'] = __('exceptions.manifest.transporter_not_found');
                }
                break;
        }

        if (!empty($errorMessages)) {
            throw ValidationException::withMessages($errorMessages);
        }
    }

    /**
     * @param Manifest $manifest
     * @param array $data
     * @param bool $shouldUpdateItems
     * @return array
     * @throws ValidationException
     */
    private function validateUpdatingData(Manifest $manifest, array $data, bool $shouldUpdateItems): array
    {
        if (!in_array($manifest->status, [Manifest::STATUS_OPEN, Manifest::STATUS_VOIDED])) {
            throw ValidationException::withMessages(
                [
                    'status' => __('exceptions.manifest.action_not_allowed', ['status' => $manifest->status])
                ]
            );
        }
        $this->validateDetailData($data);

        if ($shouldUpdateItems) {
            $this->validateItems(Arr::get($data, 'type'), Arr::get($data, 'items', []), $manifest->items);
        }
        return $data;
    }

    /**
     * @param Model $manifest
     * @param $itemData
     * @return null|ManifestItem
     * @throws Exception
     */
    private function createItem(Model $manifest, $itemData): ?ManifestItem
    {
        if (!($type = Arr::get($itemData, 'type')) || !($id = Arr::get($itemData, 'resource_id'))) {
            return null;
        }
        try {
            switch ($type) {
                case ManifestItem::TYPE_PLANT:
                    $plant = Plant::query()->findOrFail($id);
                    $item = $manifest->items()->create(
                        [
                            'type' => $type,
                            'status' => ManifestItem::STATUS_PENDING,
                            'resource_id' => $plant->id,
                            'resource_type' => get_class($plant),
                            'strain_id' => $plant->strain_id,
                            'is_for_extraction' => Arr::get($itemData, 'is_for_extraction', 0),
                            'price' => Arr::get($itemData, 'price', 0),
                            'quantity' => Arr::get($itemData, 'quantity', 0),
                        ]
                    );
                    $plant->update(['is_manifested' => true]);
                    break;
                case ManifestItem::TYPE_PROPAGATION:
                    $propagation = Propagation::query()->findOrFail($id);
                    $item = $manifest->items()->create(
                        [
                            'type' => $type,
                            'status' => ManifestItem::STATUS_PENDING,
                            'resource_id' => $propagation->id,
                            'resource_type' => get_class($propagation),
                            'strain_id' => $propagation->strain_id,
                            'is_for_extraction' => Arr::get($itemData, 'is_for_extraction', 0),
                            'price' => Arr::get($itemData, 'price', 0),
                            'quantity' => Arr::get($itemData, 'quantity', 0),
                        ]
                    );

                    $propagation->calculateAllocatedQuantity();
                    $propagation->save();
                    break;
                case ManifestItem::TYPE_QA_SAMPLE:
                    $qaSample = QASample::query()->findOrFail($id);
                    $item = $manifest->items()->create(
                        [
                            'type' => $type,
                            'status' => ManifestItem::STATUS_PENDING,
                            'resource_id' => $qaSample->id,
                            'resource_type' => get_class($qaSample),
                            'strain_id' => optional($qaSample->inventory)->strain_id,
                            'quantity' => $qaSample->weight,
                        ]
                    );
                    $qaSample->update(['status' => QASample::STATUS_AWAITING_RESULT]);
                    $qaSample->inventory->update(['qa_status' => Inventory::QA_STATUS_AWAITING_RESULTS]);
                    break;
                case ManifestItem::TYPE_UNLOTTED_INVENTORY:
                case ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY:
                case ManifestItem::TYPE_END_PRODUCT_INVENTORY:
                    $inventory = Inventory::query()->findOrFail($id);
                    $item = $manifest->items()->create(
                        [
                            'type' => $type,
                            'status' => ManifestItem::STATUS_PENDING,
                            'resource_id' => $inventory->id,
                            'resource_type' => get_class($inventory),
                            'strain_id' => $inventory->strain_id,
                            'is_sample' => Arr::get($itemData, 'is_sample', 0),
                            'is_for_extraction' => Arr::get($itemData, 'is_for_extraction', 0),
                            'price' => Arr::get($itemData, 'price', 0),
                            'quantity' => Arr::get($itemData, 'quantity', 0),
                        ]
                    );
                    $inventory->calculateAllocatedQuantity()->save();
                    break;
                default:
                    throw new GeneralException(__('exceptions.manifest.item.invalid_type'));
            }

            return $item;
        } catch (Exception $e) {
            throw new GeneralException(__('exceptions.manifest.failed_create_item'));
        }
    }

    /**
     * @param ManifestItem $existingItem
     * @param array $updatingData
     * @return ManifestItem
     * @throws Exception
     */
    private function updateItem(ManifestItem $existingItem, array $updatingData): ManifestItem
    {
        switch (Arr::get($updatingData, 'type')) {
            case ManifestItem::TYPE_PLANT:
                $existingItem->update(
                    Arr::only(
                        $updatingData,
                        [
                            'is_for_extraction',
                            'price',
                            'quantity',
                        ]
                    )
                );
                break;
            case ManifestItem::TYPE_PROPAGATION:
                $recalculateQuantity = $existingItem->quantity !== Arr::get($updatingData, 'quantity');
                $existingItem->update(
                    Arr::only(
                        $updatingData,
                        [
                            'is_for_extraction',
                            'price',
                            'quantity',
                        ]
                    )
                );

                /** @var Propagation $propagation */
                $propagation = $existingItem->resource;
                $recalculateQuantity && $propagation->calculateAllocatedQuantity()->save();
                break;
            case ManifestItem::TYPE_QA_SAMPLE:
                // do nothing
                break;
            case ManifestItem::TYPE_UNLOTTED_INVENTORY:
            case ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY:
            case ManifestItem::TYPE_END_PRODUCT_INVENTORY:
                $recalculateQuantity = $existingItem->quantity !== Arr::get($updatingData, 'quantity');
                $existingItem->update(
                    Arr::only(
                        $updatingData,
                        [
                            'is_sample',
                            'is_for_extraction',
                            'price',
                            'quantity',
                        ]
                    )
                );

                /** @var Inventory $inventory */
                $inventory = $existingItem->resource;
                $recalculateQuantity && $inventory->calculateAllocatedQuantity()->save();
                break;
            default:
                throw new GeneralException(__('exceptions.manifest.item.invalid_type'));
        }

        return $existingItem;
    }

    /**
     * @param ManifestItem $item
     * @throws Exception
     */
    private function deleteItem(ManifestItem $item): void
    {
        // #todo throw exception if delete is failed?
        $item->delete();
        if (!$item->resource) {
            return;
        }
        switch ($item->type) {
            case ManifestItem::TYPE_PLANT:
                $item->resource->update(['is_manifested' => false]);
                break;
            case ManifestItem::TYPE_PROPAGATION:
                $propagation = $item->resource;
                $propagation->calculateAllocatedQuantity()->save();
                break;
            case ManifestItem::TYPE_QA_SAMPLE:
                $qaSample = $item->resource;
                $qaSample->update(['status' => QASample::STATUS_OPEN]);
                if ($item->resource->inventory) {
                    // #todo: inventory logic should be handled when QA Sample status is updated
                    $qaCount = QASample::query()->where('inventory_id', $qaSample->inventory_id)->count();
                    $qaStatus = $qaCount > 1 ? Inventory::QA_STATUS_SAMPLED_RETEST : Inventory::QA_STATUS_SAMPLED_TEST;
                    $item->resource->inventory->update(
                        ['qa_status' => $qaStatus]
                    );
                }
                break;
            case ManifestItem::TYPE_UNLOTTED_INVENTORY:
            case ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY:
            case ManifestItem::TYPE_END_PRODUCT_INVENTORY:
                $inventory = $item->resource;
                $inventory->calculateAllocatedQuantity()->save();
                break;
            default:
                throw new GeneralException(__('exceptions.manifest.item.invalid_type'));
        }
    }

    /**
     * @param Manifest $manifest
     * @return Manifest
     * @throws Throwable
     */
    private function doPublish(Manifest $manifest): Manifest
    {
        // execute
        return DB::transaction(
            function () use ($manifest) {
                $status = Manifest::STATUS_READY_FOR_PICKUP;
                if (Manifest::TRANSPORT_TYPE_DELIVERY === $manifest->transport_type) {
                    $status = Manifest::STATUS_IN_TRANSIT;
                }
                throw_unless(
                    $manifest->update(['published_at' => Carbon::now(), 'status' => $status]),
                    __('exceptions.manifest.update_failed')
                );

                // update items
                $plantsPerStrain = [];
                $inventoryIds = [];
                foreach ($manifest->items as $item) {
                    $item->update(['status' => ManifestItem::STATUS_TRANSFERRED]);
                    switch ($item->resource_type) {
                        case Plant::class:
                            $plant = $item->resource;
                            $plantsPerStrain[$plant->strain_id][] = $plant;

                            // $plant->delete();
                            break;
                        case Propagation::class:
                            /** @var Propagation $propagation */
                            $propagation = $item->resource;
                            $license = $propagation->license;
                            $strain = $propagation->strain;

                            switch ($propagation->source_type) {
                                case SeedToSale::SOURCE_TYPE_PLANT:
                                case SeedToSale::SOURCE_TYPE_TISSUE:
                                    $stateCategoryCode = 'plant_tissue';
                                    break;
                                case SeedToSale::SOURCE_TYPE_SEED:
                                    $stateCategoryCode = 'seeds';
                                    break;
                                case SeedToSale::SOURCE_TYPE_CLONE:
                                    $stateCategoryCode = 'clones';
                                    break;
                                default:
                                    throw new Exception('Source Type not found');
                            }
                            $category = StateCategory::where(
                                ['regulator' => optional($license->state)->regulator, 'code' => $stateCategoryCode]
                            )->whereNotNull('parent_id')->first();
                            $inventoryType = InventoryType::query()->updateOrCreate(
                                [
                                    'license_id' => $license->id,
                                    'state_category_id' => $category->id,
                                    'name' => sprintf(
                                        "Immature Plant - %s - %s",
                                        $strain->name,
                                        $propagation->source_type
                                    ),
                                ],
                                ['strain_id' => $strain->id, 'uom' => $category->uom]
                            );
                            $inventory = Inventory::create(
                                [
                                    'license_id' => $license->id,
                                    'room_id' => $propagation->room_id,
                                    'source_type' => Propagation::class,
                                    'source_id' => $propagation->id,
                                    'type_id' => $inventoryType->id,
                                    'product_type' => SourceProduct::class,
                                    'state_category_id' => optional($inventoryType->stateCategory)->id,
                                    'qty_on_hand' => $item->quantity,
                                    'qty_allocated' => $item->quantity,
                                    'strain_id' => $strain->id,
                                    'is_lotted' => true,
                                ],
                            );
                            $propagation->decreaseQty($item->quantity);
                            $propagation->calculateAllocatedQuantity();
                            $propagation->save();
                            $inventoryIds[$inventory->id] = ['quantity' => $item->quantity];
                            $inventory->update(['qty_on_hand' => 0, 'qty_allocated' => 0,]);
                            break;
                        case QASample::class:
                            /** @var QASample $qaSample */
                            $qaSample = $item->resource;
                            /** @var Inventory $inventory */
                            if ($inventory = $qaSample->inventory) {
                                $inventoryIds[$inventory->id] = ['quantity' => $item->quantity];

                                $inventory->decreaseQty($item->quantity);
                                $inventory->calculateAllocatedQuantity();
                                $inventory->save();
                            }

                            break;
                        case Inventory::class:
                            $inventoryIds[$item->resource_id] = ['quantity' => $item->quantity];
                            /** @var Inventory $inventory */
                            $inventory = $item->resource;
                            $inventory->decreaseQty($item->quantity);
                            $inventory->calculateAllocatedQuantity();
                            $inventory->save();
                            break;
                    }
                }

                foreach ($plantsPerStrain as $plants) {
                    // use information of first plant #todo: there is no logic to choose the room
                    $firstPlant = $plants[0];
                    $license = $firstPlant->license;
                    $strain = $firstPlant->strain;
                    $room = $firstPlant->room;
                    $plantGroup = PlantGroup::create(
                        [
                            'license_id' => $license->id,
                            'name' => sprintf('Group of %s for manifest %s', $strain->name, $manifest->sync_code),
                            'strain_id' => $strain->id,
                            'room_id' => $room->id,
                        ]
                    );

                    // move plants to new plant group
                    foreach ($plants as $plant) {
                        $plant->update(['plant_group_id' => $plantGroup->id, 'grow_cycle_id' => null]);
                    }

                    $category = StateCategory::where(
                        ['regulator' => optional($license->state)->regulator, 'code' => 'mature_plant']
                    )->whereNotNull('parent_id')->first();
                    $inventoryType = InventoryType::query()->updateOrCreate(
                        [
                            'license_id' => $license->id,
                            'state_category_id' => $category->id,
                            'name' => sprintf("Mature Plant - %s - Plant", $strain->name),
                        ],
                        ['strain_id' => $strain->id, 'uom' => $category->uom]
                    );
                    $quantity = count($plants);
                    $inventory = Inventory::create(
                        [
                            'license_id' => $license->id,
                            'room_id' => $room->id,
                            'source_type' => PlantGroup::class,
                            'source_id' => $plantGroup->id,
                            'type_id' => $inventoryType->id,
                            'product_type' => SourceProduct::class,
                            'state_category_id' => optional($inventoryType->stateCategory)->id,
                            'strain_id' => $strain->id,
                            'qty_on_hand' => $quantity,
                            'is_lotted' => true,
                        ],
                    );

                    $inventoryIds[$inventory->id] = ['quantity' => $quantity];
                    $inventory->update(['qty_on_hand' => 0, 'qty_allocated' => 0,]);
                }

                $manifest->inventories()->sync($inventoryIds);
                return $manifest;
            }
        );
    }

    /**
     * @param string $type
     * @param array $items
     * @param Collection|null $existingItems
     * @throws ValidationException
     */
    private function validateItems(string $type, array $items, ?Collection $existingItems = null): void
    {
        $errorMessages = [];
        $uniqueResourceIds = [];
        foreach ($items as $index => $itemData) {
            $itemType = Arr::get($itemData, 'type');
            if (!in_array($itemType, ManifestItem::TYPES)) {
                $errorMessages[sprintf('items.%s.type', $index)] = __('exceptions.manifest.item.invalid_type');
                continue;
            }

            if ($type === Manifest::TYPE_QA_SAMPLE && $itemType !== ManifestItem::TYPE_QA_SAMPLE) {
                $errorMessages[sprintf('items.%s.type', $index)] = __(
                    'exceptions.manifest.item.failed_add_to_qa_sample'
                );
                continue;
            }

            if (!($resourceId = Arr::get($itemData, 'resource_id'))) {
                $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                    'exceptions.manifest.item.resource_is_required'
                );
                continue;
            }
            if (isset($uniqueResourceIds[$resourceId])) {
                $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                    'exceptions.manifest.item.duplicated_item'
                );
                break;
            }
            $uniqueResourceIds[$resourceId] = 1;

            $existingItem = $existingItems ? $existingItems->where('type', $itemType)
                ->where('resource_id', $resourceId)->first() : null;
            $isNewItem = !$existingItem;
            $qty = (int)Arr::get($itemData, 'quantity', 0);
            switch ($itemType) {
                case ManifestItem::TYPE_PLANT:
                    /** @var Plant $plant */
                    $plant = Plant::query()->licenseId($this->licenseId)->find($resourceId);
                    if (!$plant) {
                        $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                            'exceptions.manifest.item.plant_not_found'
                        );
                        continue;
                    }
                    if ($isNewItem && !$plant->isAvailable()) {
                        $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                            'exceptions.manifest.item.plant_not_available',
                            ['code' => $plant->sync_code]
                        );
                        continue;
                    }
                    break;
                case ManifestItem::TYPE_PROPAGATION:
                    $propagation = Propagation::query()->licenseId($this->licenseId)->find($resourceId);
                    if (!$propagation) {
                        $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                            'exceptions.manifest.item.propagation_not_found',
                        );
                        continue;
                    }
                    $availableQty = $propagation->qty_available;
                    if ($existingItem) {
                        $availableQty += $existingItem->quantity;
                    }
                    if ($qty > $availableQty) {
                        $errorMessages[sprintf('items.%s.quantity', $index)] = __(
                            'exceptions.manifest.item.propagation_quantity_insufficient',
                            ['code' => $propagation->sync_code]
                        );
                        continue;
                    }
                    break;
                case ManifestItem::TYPE_QA_SAMPLE:
                    if ($type !== Manifest::TYPE_QA_SAMPLE) {
                        $errorMessages[sprintf('items.%s.type', $index)] = __(
                            'exceptions.manifest.item.qa_type_not_allowed',
                            ['type' => $type]
                        );
                        continue;
                    }
                    $qaSample = QASample::query()->licenseId($this->licenseId)->find($resourceId);
                    if (!$qaSample) {
                        $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                            'exceptions.manifest.item.qa_sample_not_found',
                        );
                        continue;
                    }
                    if ($isNewItem && $qaSample->status !== QASample::STATUS_OPEN) {
                        $errorMessages[sprintf('items.%s.status', $index)] = __(
                            'exceptions.manifest.item.qa_status_must_be_open'
                        );
                        continue;
                    }

                    break;
                case ManifestItem::TYPE_UNLOTTED_INVENTORY:
                case ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY:
                case ManifestItem::TYPE_END_PRODUCT_INVENTORY:
                    $inventory = Inventory::query()->licenseId($this->licenseId)->find($resourceId);
                    if (!$inventory) {
                        $errorMessages[sprintf('items.%s.resource_id', $index)] = __(
                            'exceptions.manifest.item.inventory_not_found',
                        );
                        continue;
                    }
                    $availableQty = $inventory->qty_available;
                    if ($existingItem) {
                        $availableQty += $existingItem->quantity;
                    }
                    if ($qty > $availableQty) {
                        $errorMessages[sprintf('items.%s.quantity', $index)] = __(
                            'exceptions.manifest.item.inventory_quantity_insufficient',
                            ['code' => $inventory->sync_code]
                        );
                        continue;
                    }

                    break;
                default:
                    $errorMessages[sprintf('items.%s.type', $index)] = __('exceptions.manifest.item.invalid_type');
                    continue;
            }
        }

        if (!empty($errorMessages)) {
            throw ValidationException::withMessages($errorMessages);
        }
    }
}
