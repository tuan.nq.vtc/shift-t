<?php

return [
    'headers' => [
        'current_license_id' => env('CURRENT_LICENSE_ID', 'CURRENT-LICENSE-ID'),
        'account_holder_id' => env('FORWARDED_DATA_ACCOUNT_HOLDER_ID', 'X-Data-Account-Holder-Id'),
    ]
];
