<?php

namespace App\Listeners;

use App\Events\Inventory\SavingInventoryHistory;
use App\Models\InventoryHistory;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Arr;

class InventoryHistoryEventSubscriber
{

    public function onChangeTheQty(SavingInventoryHistory $event)
    {
        $freshInventory = $event->sourceInventory->fresh();
        /** @var InventoryHistory $history */
        $history = $freshInventory->histories()->create([
            'qty_current' => $freshInventory->qty_on_hand,
            'qty_variable' => $event->variableQty,
            'action' => $event->action,
            'is_decrease' => $event->isDecrease,
            'comment' => Arr::get($event->extraInfo, 'comment', ''),
            'reason' =>  Arr::get($event->extraInfo, 'reason', ''),
        ]);

        if (!is_null($event->source)) {
            $history->source()->associate($event->source)->save();
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            SavingInventoryHistory::class,
            'App\Listeners\InventoryHistoryEventSubscriber@onChangeTheQty'
        );
    }
}
