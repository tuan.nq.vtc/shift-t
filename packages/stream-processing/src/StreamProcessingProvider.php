<?php

namespace Bamboo\StreamProcessing;

use Bamboo\StreamProcessing\Console\{ConsumeCommand, ListFailedCommand, RetryCommand};
use Bamboo\StreamProcessing\Contracts\{ConsumerHandlerInterface, ProducerHandlerInterface};
use Bamboo\StreamProcessing\Handlers\Kafka\{ConsumerHandler, ProducerHandler};
use Bamboo\StreamProcessing\Listeners\StreamableResourceEventSubscriber;
use Enqueue\RdKafka\RdKafkaConnectionFactory;
use Exception;
use Illuminate\Support\{Facades\Log, ServiceProvider};
use Laravel\Lumen\Application;
use Throwable;

class StreamProcessingProvider extends ServiceProvider
{
    private const STREAM_CONFIG_PATH = __DIR__ . '/../config/stream.php';
    private const STREAM_MIGRATION_PATH = __DIR__ . '/../database/migrations/';

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        StreamableResourceEventSubscriber::class
    ];

    protected $commands = [
        ConsumeCommand::class,
        ListFailedCommand::class,
        RetryCommand::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function register()
    {
        try {
            $channel = config('stream.default');
            switch ($channel) {
                case 'kafka' :
                    $config = throw_unless(
                        config('stream.channels.kafka.connection'),
                        new Exception('Can not bind stream processing without connection config')
                    );

                    $this->app->singleton(
                        RdKafkaConnectionFactory::class,
                        fn(Application $application) => new RdKafkaConnectionFactory($config)
                    );

                    $this->registerKafkaHandlers();
                    break;
                default:
            }

            $this->registerCommands();
        } catch (Throwable $e) {
            Log::channel('stream')->error($e->getMessage(), ['exception' => $e]);
            throw $e;
        }
    }

    private function registerKafkaHandlers()
    {
        $this->app->bind(ProducerHandlerInterface::class, fn(Application $app) => $app->make(ProducerHandler::class));
        $this->app->bind(ConsumerHandlerInterface::class, fn(Application $app) => $app->make(ConsumerHandler::class));
    }

    private function registerCommands()
    {
        $this->commands($this->commands);
    }

    public function boot()
    {
        $this->mergeConfigFrom(static::STREAM_CONFIG_PATH, 'stream');

        if ($this->app->runningInConsole()) {
            $this->publishes(
                [
                    self::STREAM_CONFIG_PATH => base_path('config/stream.php'),
                ],
                'stream'
            );

            $this->publishes(
                [
                    self::STREAM_MIGRATION_PATH => database_path('migrations'),
                ],
                'stream'
            );
        }

        $events = $this->app->get('events');
        $this->bootEvents($events);
        $this->bootSubscribers($events);
    }

    private function bootEvents($events)
    {
        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    private function bootSubscribers($events)
    {
        foreach ($this->subscribe as $subscriber) {
            $events->subscribe($subscriber);
        }
    }
}
