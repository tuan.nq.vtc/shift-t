<?php

namespace App\Models;

use Illuminate\Support\{Arr, Str};

class SeedToSale
{
    public const UOM_GRAMS = 'g';
    public const UOM_EACH = 'ea';

    public const GROW_STATUS_GERMINATION = 'germination';
    public const GROW_STATUS_SEEDLING = 'seedling';
    public const GROW_STATUS_PRUNING = 'pruning';
    public const GROW_STATUS_CLONING = 'cloning';
    public const GROW_STATUS_VEGETATIVE = 'vegetative';
    public const GROW_STATUS_FLOWERING = 'flowering';
    public const GROW_STATUS_HARVESTING = 'harvesting';
    public const GROW_STATUS_HARVESTED = 'harvested';

    public const GROW_STATUSES = [
        self::GROW_STATUS_GERMINATION,
        self::GROW_STATUS_SEEDLING,
        self::GROW_STATUS_PRUNING,
        self::GROW_STATUS_CLONING,
        self::GROW_STATUS_VEGETATIVE,
        self::GROW_STATUS_FLOWERING,
        self::GROW_STATUS_HARVESTING,
        self::GROW_STATUS_HARVESTED,
    ];

    /** Available plant stages on LEAF: propagation_material, growing, harvested, packaged, destroyed */
    public const LEAF_GROW_STATUSES = [
        'propagation_material' => self::GROW_STATUS_SEEDLING,
        'growing' => self::GROW_STATUS_VEGETATIVE,
        'harvested' => self::GROW_STATUS_HARVESTED,
        'packaged' => self::GROW_STATUS_VEGETATIVE,
        'destroyed' => self::GROW_STATUS_VEGETATIVE,
    ];

    public const GROW_CYCLE_GROW_STATUSES = [
        self::GROW_STATUS_VEGETATIVE,
        self::GROW_STATUS_FLOWERING,
    ];

    public const SOURCE_TYPE_PLANT = 'plant';
    public const SOURCE_TYPE_SEED = 'seed';
    public const SOURCE_TYPE_CLONE = 'clone';
    public const SOURCE_TYPE_TISSUE = 'tissue';

    public const SOURCE_TYPES = [
        self::SOURCE_TYPE_PLANT,
        self::SOURCE_TYPE_SEED,
        self::SOURCE_TYPE_CLONE,
        self::SOURCE_TYPE_TISSUE,
    ];

    public static function getLeafSourceType($origin): ?string
    {
        return Arr::first(
            SeedToSale::SOURCE_TYPES,
            fn ($item) => Str::lower($origin) == Str::lower($item)
        );
    }

    public static function getLeafGrowStatus($stage): ?string
    {
        return Arr::first(
            SeedToSale::LEAF_GROW_STATUSES,
            fn ($value, $key) => Str::lower($stage) == Str::lower($key)
        );
    }
}
