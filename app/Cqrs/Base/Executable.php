<?php

namespace App\Cqrs\Base;

abstract class Executable extends Valuable
{
    /**
     * @return bool
     */
    public function isExecutable(): bool
    {
        return true;
    }
}

