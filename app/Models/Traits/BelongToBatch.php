<?php

namespace App\Models\Traits;

use App\Models\Batch;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongToBatch
{
    public function batch(): BelongsTo
    {
        return $this->belongsTo(Batch::class, 'batch_id');
    }
}
