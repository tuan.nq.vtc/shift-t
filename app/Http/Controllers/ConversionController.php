<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\{Conversion, Inventory};
use App\Services\ConversionService;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Validation\Rules\In;

/**
 * Class ConversionController
 * @package App\Http\Controllers
 *
 * @property ConversionService $service
 */
class ConversionController extends CRUDController
{

    public function __construct(ConversionService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    /**
     * @inheritDoc
     */
    public function getCreateRules(): array
    {
        $licenseId = $this->request->get('license_id');
        $outputCategoryType = $this->service->getOutputCategoryModelClass($this->request->get('output_type'));
        $outputStateCategoryId = $this->request->get('output_state_category_id');

        return [
            'name' => ['required', 'string'],
            'output_room_id' => [
                'required',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'waste_quantity' => ['sometimes', 'nullable', 'numeric', 'min:0'],
            'uom' => ['required', 'string', new In(Conversion::UOM_LIST)],
            'waste_room_id' => [
                'sometimes',
                'nullable',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'output_product_id' => ['required', 'uuid'],
            'output_type' => ['required', new In([Conversion::TYPE_SOURCE, Conversion::TYPE_END])],
            'output_category_id' => [
                'required',
                'uuid',
                'exists:App\Models\Taxonomy,category_id,license_id,' . $licenseId . ',category_type,' . $outputCategoryType . ',state_category_id,' . $outputStateCategoryId,
            ],
            'output_state_category_id' => [
                'required',
                'uuid',
                'exists:App\Models\StateCategory,id',
            ],
            'output_quantity' => ['required', 'numeric', 'min:0'],
            'is_altered' => ['sometimes', 'boolean'],
            'input_lots' => ['required', 'array'],
            'input_lots.*.inventory_id' => [
                'required',
                'uuid',
                'exists:App\Models\Inventory,id,license_id,' . $licenseId . ',is_lotted,' . Inventory::IS_LOTTED,
            ],
            'input_lots.*.quantity' => ['required', 'numeric', 'gt:0'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id');
        $outputCategoryType = $this->service->getOutputCategoryModelClass($this->request->get('output_type'));
        $outputStateCategoryId = $this->request->get('output_state_category_id');
        return [
            'name' => ['sometimes', 'required', 'string'],
            'output_room_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'waste_quantity' => ['sometimes', 'nullable', 'numeric', 'min:0'],
            'uom' => ['sometimes', 'required', 'string', new In(Conversion::UOM_LIST)],
            'waste_room_id' => [
                'sometimes',
                'nullable',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'output_product_id' => ['sometimes', 'required', 'uuid'],
            'output_type' => ['sometimes', 'required', new In([Conversion::TYPE_SOURCE, Conversion::TYPE_END])],
            'output_category_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Taxonomy,category_id,license_id,' . $licenseId . ',category_type,' . $outputCategoryType . ',state_category_id,' . $outputStateCategoryId
            ],
            'output_state_category_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\StateCategory,id',
            ],
            'output_quantity' => ['sometimes', 'required', 'numeric', 'min:0'],
            'is_altered' => ['sometimes', 'boolean'],
            'input_lots' => ['sometimes', 'required', 'array'],
            'input_lots.*.inventory_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Inventory,id,license_id,' . $licenseId . ',is_lotted,' . Inventory::IS_LOTTED,
            ],
            'input_lots.*.quantity' => ['sometimes', 'required', 'numeric', 'gt:0'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems(Criteria::createFromRequest($this->request)));
    }

    public function search()
    {
        return response()->json($this->service->search(Criteria::createFromRequest($this->request)));
    }

    public function listInputLots(string $id)
    {
        return response()->json($this->service->listInputLots($id));
    }

    public function createAndFinalize()
    {
        $data = $this->validate($this->request, $this->getCreateRules());
        $data['status'] = Conversion::STATUS_CLOSED;

        return response()->json(
            $this->service->create($data),
            Response::HTTP_ACCEPTED
        );
    }

    public function updateAndFinalize(string $id)
    {
        $data = $this->validate($this->request, $this->getUpdateRules($id));
        $data['status'] = Conversion::STATUS_CLOSED;

        return response()->json(
            $this->service->update($id, $data),
            Response::HTTP_ACCEPTED
        );
    }

    public function addLots(string $id)
    {
        $licenseId = $this->request->get('license_id');
        $rules = [
            'input_lots' => ['sometimes', 'required', 'array'],
            'input_lots.*.inventory_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Inventory,id,license_id,' . $licenseId . ',is_lotted,' . Inventory::IS_LOTTED,
            ],
            'input_lots.*.quantity' => ['sometimes', 'required', 'numeric', 'gt:0'],
        ];

        $data = $this->validate($this->request, $rules);
        $this->service->addLots($id, $data['input_lots']);

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
