<?php

namespace Tests\Feature;

use App\Models\Additive;
use App\Models\Supplier;
use Illuminate\Http\Response;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

/**
 * Class AdditiveTest
 *
 * @package Tests\Feature
 */
class AdditiveTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $modelName = Additive::class;

    /** @test */
    public function guest_cannot_call_additive_endpoints()
    {
        $this->actingAsGuest();
        $this->get($this->getPesticideEndpoint("/list"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getPesticideEndpoint())->seeStatusCode(Response::HTTP_FORBIDDEN);

        $this->get($this->getNutrientEndpoint("/list"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getNutrientEndpoint())->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    private function getPesticideEndpoint($path = '')
    {
        return "/pesticides{$path}";
    }

    private function getNutrientEndpoint($path = '')
    {
        return "/nutrients{$path}";
    }

    /** @test */
    public function user_can_create_additive_items()
    {
        $expectedData = $this->setupExpectedCreatingData();
        $additionalData = $this->setupAdditionalCreatingData();
        $this->post(
            $this->getPesticideEndpoint(),
            [
                'additives' => [
                    array_merge(
                        $expectedData,
                        $additionalData,
                        ['type' => Additive::TYPE_PESTICIDE,]
                    )
                ]
            ]
        )
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonContains($expectedData);

        $this->post(
            $this->getNutrientEndpoint(),
            [
                'additives' => [
                    array_merge(
                        $expectedData,
                        $additionalData,
                        ['type' => Additive::TYPE_NUTRIENT,]
                    )
                ]
            ]
        )
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonContains($expectedData);
    }

    private function setupExpectedCreatingData(): array
    {
        return factory(static::$modelName)
            ->make(['license_id' => $this->licenseId])
            ->only(
                [
                    'name',
                    'epa_regulation_number',
                    'ingredients',
                    'uom',
                    'applied_quantity',
                ]
            );
    }

    private function setupAdditionalCreatingData(): array
    {
        return [
            'additive_inventory' => [
                'supplier_id' => factory(Supplier::class)->create()->id,
                'total_quantity' => $this->faker->numberBetween(1, 2)
            ]
        ];
    }

    /** @test */
    public function user_can_create_additive_items_with_other_type()
    {
        $expectedData = $this->setupExpectedCreatingData();
        $additionalData = array_merge($this->setupAdditionalCreatingData(), ['type' => Additive::TYPE_OTHER]);
        $this->post(
            $this->getPesticideEndpoint(),
            ['additives' => [array_merge($expectedData, $additionalData,)]]
        )
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonContains($expectedData);

        $this->post(
            $this->getNutrientEndpoint(),
            ['additives' => [array_merge($expectedData, $additionalData)]]
        )
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonContains($expectedData);
    }

    /** @test */
    public function user_can_update_additive_item()
    {
        /** @var Additive $pesticide */
        $pesticide = factory(self::$modelName)->create(['license_id' => $this->licenseId]);
        $updatingData = factory(self::$modelName)->make()->only(['visibility', 'epa_regulation_number', 'ingredients']);
        $this->put(
            $this->getPesticideEndpoint('/'.$pesticide->id),
            array_merge($updatingData, ['type' => 'other'])
        )
            ->seeStatusCode(Response::HTTP_ACCEPTED)
            ->seeJsonContains(array_merge($updatingData, ['is_other_type' => false]));

        /** @var Additive $nutrient */
        $nutrient = factory(self::$modelName)->create(
            ['license_id' => $this->licenseId, 'type' => Additive::TYPE_NUTRIENT]
        );
        $updatingData = factory(self::$modelName)->make()->only(['visibility', 'epa_regulation_number', 'ingredients']);
        $this->put(
            $this->getNutrientEndpoint('/'.$nutrient->id),
            array_merge($updatingData, ['type' => 'other'])
        )
            ->seeStatusCode(Response::HTTP_ACCEPTED)
            ->seeJsonContains(array_merge($updatingData, ['is_other_type' => false]));
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

}
