<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarvestPlants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_plants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('harvest_id')->references('id')->on('harvests')->cascadeOnDelete();
            $table->uuid('plant_id')->references('id')->on('plants')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvest_plants');
    }
}
