<?php

namespace App\Http\Controllers\Verification;

use App\Models\{License, TraceableModel};
use App\Synchronizations\Contracts\Http\ResourceInterface;
use App\Synchronizations\Http\Clients\MetrcClient;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Str;
use Laravel\Lumen\Routing\Controller;
use Ramsey\Uuid\Uuid;

class MetrcVerifyController extends Controller
{

    public function __invoke(Request $request): JsonResponse
    {
        $resourceId = $request->get('id');
        $resourceType = $request->get('resource');
        $licenseId = $request->get('license_id');

        try {
            if ($resourceId && !Uuid::isValid($resourceId)) {
                return response()->json(
                    ['error' => "Invalid resource id [{$resourceId}]!"],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            if (!$resourceType) {
                return response()->json(
                    ['error' => 'Please specify resource to verify!'],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $resourceClass = "App\\Models\\".Str::ucfirst($resourceType);
            if (!class_exists($resourceClass)) {
                return response()->json(
                    ['error' => "Invalid resource [{$resourceType}]!"],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $license = License::findOrFail($licenseId);
            $stateResourceClass = $license->getStateResource($resourceClass);
            if (!$stateResourceClass) {
                return response()->json(
                    ['error' => "Resource [$resourceType] not configured in traceablility!"],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $client = new MetrcClient($license);

            if ($resourceId) {
                $resource = $resourceClass::licenseId($license)->findOrFail($resourceId);
                /** @var ResourceInterface $stateResource */
                $stateResource = new $stateResourceClass($resource, ResourceInterface::ACTION_FETCH);
            } else {
                /** @var TraceableModel $resource */
                $resource = $resourceClass::make()->setRelation('license', $license);
                /** @var ResourceInterface $stateResource */
                $stateResource = new $stateResourceClass($resource, ResourceInterface::ACTION_LIST);
            }

            $response = $client->getRequest()->send(
                $stateResource->getMethod(),
                $stateResource->getUrl(),
                [
                    'query' => $stateResource->getQuery(),
                    'json' => $stateResource->getBody(),
                ]
            );

            $response->throw();

            return response()->json($response->json());
        } catch (ModelNotFoundException $modelNotFoundException) {
            return response()->json(
                ['error' => "License or Resource [$resourceType] has been removed or not exists!"],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        } catch (Exception $exception) {
            return response()->json(
                ['error' => "Error while fetching resources from METRC regulator API!"],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
