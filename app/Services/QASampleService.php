<?php

namespace App\Services;

use App\Models\QASample;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class QASampleService
 * @package App\Services
 */
class QASampleService extends BaseService
{
    /**
     * @var array|string[]
     */
    protected array $relations = [
        'lab:id,name,sync_code',
        'manifest:id,sync_code',
        'coas',
        'sourceInventory:id,strain_id,product_id,product_type,sync_code',
        'sourceInventory.strain:id,name',
        'sourceInventory.product:id,name',
    ];

    protected array $counts = [
        'coas'
    ];

    /**
     * @param QASample $qaSample
     */
    public function __construct(QASample $qaSample)
    {
        parent::__construct($qaSample);
    }

    public function get(string $id, $withRelation = true, array $relations = [])
    {
        $query = $this->newQuery();
        if ($withRelation) {
            $query->with(array_merge($this->relations, $relations));
        }

        return $query->findOrFail($id);
    }
}
