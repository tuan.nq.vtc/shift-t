<?php

namespace App\Models;

use App\Jobs\BroadcastingResource;
use App\Models\Traits\BelongToLicense;
use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{CreatorFilterable,
    DatetimeFilterable,
    IdsFilterable,
    LicenseFilterable,
    ModifierFilterable,
    NameFilterable,
    StatusFilterable
};
use App\Notification\Contracts\NotificationInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, Relations\HasMany, Relations\MorphMany};
use Illuminate\Support\Facades\Storage;

/**
 * Class Strain
 *
 * @property String sync_code
 * @package App\Models
 */
class Strain extends TraceableModel implements NotificationInterface
{
    use IdsFilterable;
    use LicenseFilterable;
    use BelongToLicense;
    use NameFilterable;
    use StatusFilterable;
    use CreatorFilterable;
    use ModifierFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const TESTING_STATUSES = [
        'None',
        'InHouse',
        'ThirdParty',
    ];

    public const TYPE_INDICA = 'Indica';
    public const TYPE_SATIVA = 'Sativa';
    public const TYPE_HYBRID = 'Hybrid';

    protected array $traceable = ['name'];

    protected $fillable = [
        'name',
        'image',
        'description',
        'status',
        'info',
        'created_by',
        'updated_by',
        'custom_id',
        'sync_code',
        'synced_at',
    ];

    protected $casts = ['info' => 'array', 'status' => 'bool'];

    protected $appends = [
        'image_url',
        'type'
    ];

    public static function boot()
    {
        parent::boot();

        static::updated(
            function (Strain $strain) {
                if ($strain->isDirty($strain->getBoardcastingUpdatable())) {
                    dispatch(
                        new BroadcastingResource(
                            $strain,
                            $strain->modifier,
                            Notification::EVENT_UPDATED
                        )
                    );
                }
            }
        );
    }

    public function getImageUrlAttribute()
    {
        if ($this->image) {
            try {
                return Storage::temporaryUrl($this->image, Carbon::now()->addWeek());
            } catch (\Throwable $e) {
                \Log::error($e);
            }
        }
        return null;
    }

    public function isTraceUpdatable(): bool
    {
        $isTraceUpdatable = false;

        $originalInfo = $this->getOriginal('info', '');

        if (is_array($originalInfo) && isset($originalInfo['sativa']) && isset($originalInfo['indica'])) {
            $originalType = $this->resolveType($originalInfo['sativa'], $originalInfo['indica']);
            $isTraceUpdatable = ($originalType !== $this->type);
        }

        return $isTraceUpdatable || parent::isTraceUpdatable();
    }

    private function resolveType(int $sativa, int $indica): string
    {
        if ($sativa == $indica) {
            return self::TYPE_HYBRID;
        }
        return ($sativa > $indica) ? self::TYPE_SATIVA : self::TYPE_INDICA;
    }

    public function getTypeAttribute(): ?string
    {
        if (!empty($this->info) && isset($this->info['sativa']) && isset($this->info['indica'])) {
            return $this->resolveType($this->info['sativa'], $this->info['indica']);
        }
        return null;
    }

    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            return [
                'global_id' => fn(self $instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]

                ),
                'name' => fn(self $instance, $value) => $instance->setAttribute('name', $value),
                // Save external_id from LEAF as an info attribute
                // 'external_id' => fn(self $instance, $value) => $instance->setAttribute('info->external_id', $value),
            ];
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            return [
                // "Id": 201,
                // "Name": "Spring Hill Kush",
                // "TestingStatus": "ThirdParty",
                // "ThcLevel": null,
                // "CbdLevel": null,
                // "IndicaPercentage": 60.0,
                // "SativaPercentage": 40.0,
                // "IsUsed": false,
                // "Genetics": "60% Indica / 40% Sativa"
                'Name' => fn(self $instance, $value) => $instance->firstOrNew(
                    ['name' => $value, 'license_id' => $this->license->id]
                ),
                'Id' => fn(self $instance, $value) => $instance->setAttribute('sync_code', $value),
                'TestingStatus' => fn(self $instance, $value) => $instance->setAttribute(
                    'info->testing_status',
                    $value
                ),
                'ThcLevel' => fn(self $instance, $value) => $instance->setAttribute('info->thc', $value),
                'CbdLevel' => fn(self $instance, $value) => $instance->setAttribute('info->cbd', $value),
                'IndicaPercentage' => fn(self $instance, $value) => $instance->setAttribute('info->indica', $value),
                'SativaPercentage' => fn(self $instance, $value) => $instance->setAttribute('info->sativa', $value),
            ];
        }

        return null;
    }

    public function propagations(): HasMany
    {
        return $this->hasMany(Propagation::class);
    }

    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class);
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function modifier(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function notifications(): MorphMany
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    public function scopeSearch(Builder $query, string $search): Builder
    {
        $search = trim($search);
        if (empty($search)) {
            return $query;
        }

        return $query->where(
            fn(Builder $subQuery) => $subQuery->orWhere('sync_code', 'LIKE', '%' . $search . '%')
                ->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('custom_id', 'LIKE', '%' . $search . '%')
        );
    }

    public function getNotificationLabel(): string
    {
        return $this->name;
    }

    public function getBoardcastingUpdatable(): array
    {
        return ['status', 'name'];
    }
}
