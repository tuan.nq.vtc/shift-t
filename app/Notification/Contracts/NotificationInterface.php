<?php

namespace App\Notification\Contracts;

use Illuminate\Database\Eloquent\Relations\{BelongsTo, MorphMany};

interface NotificationInterface
{
    /**
     * The notification resource must have created_by attribute
     * @return BelongsTo User
     */
    public function creator(): BelongsTo;

    /**
     *  The notification resource must have updated_by attribute
     * @return BelongsTo User
     */
    public function modifier(): BelongsTo;

    /**
     * get name of notification resource
     * @return string
     */
    public function getNotificationLabel(): string;

    /**
     * @return MorphMany
     */
    public function notifications(): MorphMany;

    public function getBoardcastingUpdatable(): array;
}
