<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->string('name')->index();
            $table->uuid('room_type_id')->index()->nullable();
            $table->unsignedTinyInteger('status')->default(1);
            $table->unsignedTinyInteger('is_quarantine')->default(0);
            $table->json('dimension')->nullable();
            $table->string('rfid_tag')->nullable()->index();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->json('info')->nullable();

            // S2S Trace sync[Create/Update/Delete]
            $table->string('sync_code')->nullable()->index();
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_type_id')->references('id')->on('room_types')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
