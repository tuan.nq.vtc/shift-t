<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Traits\Scope\NameFilterable;


/**
 * Class Licensee
 * @package App\Models
 */
class Licensee extends BaseModel
{
    use NameFilterable;

    const REGULATOR_LEAF = 'leaf';
    const REGULATOR_METRC = 'metrc';

    // type of Leaf system
    const TYPE_CO_OP = 'co-op';
    const TYPE_CULTIVATOR = 'cultivator';
    const TYPE_CULTIVATOR_PRODUCTION = 'cultivator_production';
    const TYPE_DISPENSARY = 'dispensary';
    const TYPE_LAB = 'lab';
    const TYPE_PRODUCTION = 'production';
    const TYPE_STATE = 'state';
    const TYPE_TRANSPORTER = 'transporter';
    const TYPE_TRIBE = 'tribe';

    protected $fillable = [
        'regulator',
        'name',
        'address1',
        'address2',
        'city',
        'state_code',
        'country_code',
        'phone',
        'type',
        'code',
        'sync_code',
    ];

    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where($this->getTable() . '.type', $type);
    }

    public function scopeStateCode(Builder $query, $stateCode): Builder
    {
        return $query->where($this->getTable() . '.state_code', $stateCode);
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        return $query->where(
            fn($subQuery) => $subQuery->where('name', 'like', '%' . $value . '%')
                ->orWhere('code', 'like', '%' . $value . '%')
        );
    }
}
