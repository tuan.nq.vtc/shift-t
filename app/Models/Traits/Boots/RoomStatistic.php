<?php
namespace App\Models\Traits\Boots;

use App\Jobs\GenerateRoomStatistic;

trait RoomStatistic
{
    protected static function bootRoomStatistic(): void
    {
        static::saved(
            fn(self $model) => $model->isDirty('room_id') && $model->room && dispatch(new GenerateRoomStatistic($model->room))
        );
    }
}
