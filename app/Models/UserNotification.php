<?php


namespace App\Models;

use App\Models\Traits\Scope\{CreatorFilterable, LicenseFilterable};
use App\Notification\Events\PushNotificationEvent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserNotification extends BaseModel
{
    use CreatorFilterable;
    use LicenseFilterable;

    protected $fillable = ['user_id', 'notification_id', 'is_read', 'license_id', 'event_name', 'created_at'];

    protected $casts = ['is_read' => 'bool'];

    protected static function booted(): void
    {
        parent::booted();
        static::created(
            function (self $model): void {
                event(
                    new PushNotificationEvent(
                        $model->event_name,
                        json_encode((object) array_merge($model->toArray(), ['notification' => $model->notification])),
                        $model->user_id.($model->license_id ? "_".$model->license_id : '')
                    )
                );
            }
        );
    }

    public function notification(): BelongsTo
    {
        return $this->belongsTo(Notification::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
