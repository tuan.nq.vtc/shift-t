#!/bin/bash

set -e

if [[ $* == *--reset* ]] || [[ $* == *--prep* ]] || [[ $* == *--dev* ]] || [[ $* == *--deploy* ]] || [[ $* == *--seed* ]]; then
  echo "---------------------------------------------------------------------------"
  echo "RUNNING BAMBOO-API-TRACE DEV COMMANDS FOR DEVUSER '${DEVUSER}'"
  echo "INGRESS URL: 'https://bamboo-api-trace.${DEVUSER}.vcoder.host'"
  echo "---------------------------------------------------------------------------"
  sed "s/__DEVUSER__/${DEVUSER}/g" devspace.yaml.template > devspace.yaml
  sed "s/__DEVUSER__/${DEVUSER}/g" ./api-trace/files/dev.env > ./api-trace/files/.env
  sed "s/__DEVUSER__/${DEVUSER}/g" ./api-trace/values_dev.yaml > ./api-trace/values.yaml
else
  echo "Error: Your options are:"
  echo "1. 'bash run.sh --prep' to install Composer packages"
  echo "2. 'bash run.sh --dev' for devspace dev (two-way sync hot reloading)"
  echo "3. 'bash run.sh --deploy' for devspace deploy (no sync)"
  echo "4. 'bash run.sh --reset' to uninstall helm chart deployment and reset devspace"
  echo "5. 'bash run.sh --seed' to populate database with sample data"
  exit 1
fi

if [[ $* == *--reset* ]]; then
  devspace purge
  #helm uninstall bamboo-api-trace || true
  (kubectl get deploy -n bamboo --no-headers=true | awk '/bamboo-api-trace/{print $1}' | xargs kubectl delete -n bamboo deploy) || true
  (kubectl get rs -n bamboo --no-headers=true | awk '/bamboo-api-trace/{print $1}' | xargs kubectl delete -n bamboo rs) || true
  devspace cleanup images
  rm -rf ./.devspace
  echo "RESET FINISHED"
fi

if [[ $* == *--prep* ]]; then
  rm -rf ./vendor
  composer install --prefer-dist --no-autoloader --no-scripts --no-progress --no-interaction && \
    composer clear-cache --no-interaction && \
    composer dump-autoload --optimize --classmap-authoritative --no-interaction
fi

if [[ $* == *--dev* ]]; then
  composer install --prefer-dist --no-autoloader --no-scripts --no-progress --no-interaction && \
    composer clear-cache --no-interaction && \
    composer dump-autoload --optimize --classmap-authoritative --no-interaction
  devspace use namespace bamboo
  devspace dev -p dev
fi

if [[ $* == *--deploy* ]]; then
  devspace use namespace bamboo
  devspace deploy
fi

if [[ $* == *--seed* ]]; then
  kubectl exec -i -n bamboo deploy/bamboo-api-trace -- php artisan migrate:fresh --seed
  kubectl exec -i -n bamboo deploy/bamboo-api-trace -- php artisan db:seed --class=SampleSeeder
fi
