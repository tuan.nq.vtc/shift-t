<?php

namespace App\Integrations\FileGenerators\FileStrategies\Ccrs;

use App\Models\Trace;

class RoomFileStrategy extends FileStrategy
{
    protected static string $fileIdentifier = 'area';

    public function getHeaders(): array
    {
        $headers = parent::getHeaders();
        $headers[] = ['LicenseNumber', 'Area', 'IsQuarantine', 'ExternalIdentifierID', 'CreatedBy', 'CreatedDate', 'UpdatedBy', 'UpdatedDate', 'Operation'];
        return $headers;
    }

    public function getRow($model): array
    {
        $room = $model->resource()->withTrashed()->first(); 
        
        return [
            $this->getLicense()->code,
            $room->name,
            $this->boolToString($room->is_quarantine),
            $room->internal_id,
            $this->getShortenedtUserName($room->creator),
            $this->formatDateRow($room->created_at),
            ($model->action == Trace::ACTION_CREATE) ? '' : $this->getShortenedtUserName($room->modifier),
            ($model->action == Trace::ACTION_CREATE) ? '' : $this->formatDateRow($room->updated_at),
            $this->convertActionToOperation($model->action),
        ];
    }
}
