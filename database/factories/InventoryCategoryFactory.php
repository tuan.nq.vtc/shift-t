<?php

/** @var Factory $factory */

use App\Models\{InventoryCategory, State};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Stringable;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(
    InventoryCategory::class,
    function (Faker $faker) {
        $name = new Stringable($faker->words(2, true));
        $uomList = array_merge(
            InventoryCategory::getCountBasedUnitList(),
            InventoryCategory::getWeightBasedUnitList(),
            InventoryCategory::getVolumeBasedUnitList()
        );
        return [
            'regulator' => $faker->randomElement(State::getRegulators()),
            'name' => $name,
            'slug' => $name->slug(),
            'is_strain_required' => $faker->randomElement([0, 1]),
            'uom' => $faker->randomElement($uomList)
        ];
    }
);
