<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait AccountHolderFilterable
{
    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeAccountHolderId(Builder $query, $value)
    {
        return $query->where('account_holder_id', $value);
    }
}
