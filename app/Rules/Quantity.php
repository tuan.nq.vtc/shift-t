<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Builder;

class Quantity implements Rule
{
    protected $entity;
    protected $attribute;
    protected $value;

    protected string $modelClass;
    protected $modelKey;
    protected string $modelKeyName;
    protected string $modelQuantityName;
    protected array $where;

    public function __construct(
        string $modelClass,
        $modelKey,
        string $modelKeyName = 'id',
        string $modelQuantityName = 'quantity',
        array $where = []
    ) {
        $this->modelClass = $modelClass;
        $this->modelKey = $modelKey;
        $this->modelKeyName = $modelKeyName;
        $this->modelQuantityName = $modelQuantityName;
        $this->where = $where;
    }

    public function passes($attribute, $value): bool
    {
        $this->entity = get_classname($this->modelClass, true);
        $this->attribute = $attribute;
        $this->value = $value;

        return $this->buildQuery()->where($this->modelQuantityName, '>=', $value)->count() > 0;
    }

    protected function buildQuery(): Builder
    {
        $query = $this->modelClass::where($this->modelKeyName, $this->modelKey)->withoutGlobalScopes(
            [$this->modelClass::DEFAULT_ORDER_SCOPE]
        );
        foreach ($this->where as $column => $condition) {
            if (is_callable($condition)) {
                $query->where($condition);
            } else {
                $query->where($column, $condition);
            }
        }
        return $query;
    }

    public function message()
    {
        return __(
            'validation.quantity.insufficient',
            ['entity' => $this->entity, 'value' => $this->value]
        );
    }
}

