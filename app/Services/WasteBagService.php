<?php

namespace App\Services;

use App\Models\WasteBag;
use Illuminate\Database\Eloquent\{Builder, Collection};
use Illuminate\Support\{Arr, Facades\DB};
use Throwable;

/**
 * Class WasteBagService
 * @package App\Services
 *
 * @method WasteBag|Collection get($id)
 * @method WasteBag|Builder getModel()
 */
class WasteBagService extends BaseService
{

    public function __construct(WasteBag $wasteBag)
    {
        parent::__construct($wasteBag);
    }

    public function bulkCreate(array $rawData)
    {
        try {
            DB::beginTransaction();
            $wasteBags = collect(Arr::get($rawData, 'waste-bags', []))
                ->each(function ($wasteBagData) use ($rawData) {
                    $wasteBag = $this->create(
                        array_merge(
                            $wasteBagData,
                            [
                                'comment' => $wasteBagData['comment'] ?? '',
                                'license_id' => Arr::get($rawData, 'license_id'),
                            ]
                        )
                    );

                    if (!empty($wasteBagData['strain_ids'])) {
                        $wasteBag->strains()->attach($wasteBagData['strain_ids']);
                    }

                    return $wasteBag;
                });
            DB::commit();
            return $wasteBags;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }
}
