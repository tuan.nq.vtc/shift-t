<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use App\Models\{LabTest, TraceableResource};
use App\Synchronizations\Http\Resources\BaseResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class LabTestResource
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property LabTest $resource
 */
class LabTestResource extends BaseResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_LAB_RESULT;

    protected function create(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = self::$endpoint;

        $analyses = $this->resource->labTestAnalyses;
        $analysesData = [];
        foreach ($analyses as $analysis) {
            $analysesData[$analysis->code] = $analysis->pivot->result;
        }

        $this->requestBody = [
            'lab_result' => [
                array_merge(
                    [
                        'external_id' => $this->resource->id,
                        'tested_at' => $this->resource->tested_at,
                        'testing_status' => $this->resource->tested_at ? 'completed' : 'in_progress',
                        'type' => $this->resource->qaSample->inventory->type->inventoryCategory->parent->slug,
                        'intermediate_type' => $this->resource->qaSample->inventory->type->inventoryCategory->slug,
                        'global_inventory_id' => $this->resource->qaSample->inventory->sync_code,
                        'global_batch_id' => $this->resource->qaSample->inventory->batch->sync_code,
                    ],
                    $analysesData
                ),
            ],
        ];

        return $this;
    }

    protected function update(): self
    {
        if (!$this->resource->sync_code) {
            $error = 'Unable to sync lab test update without LEAF global_id [sync_code]';
            Log::error($error);
            throw new Exception($error);
        }
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = self::$endpoint . '/update';

        $analyses = $this->resource->labTestAnalyses;
        $analysesData = [];
        foreach ($analyses as $analysis) {
            $analysesData[$analysis->code] = $analysis->pivot->result;
        }

        $this->requestBody = [
            'lab_result' => [
                array_merge(
                    [
                        'external_id' => $this->resource->id,
                        'tested_at' => $this->resource->tested_at,
                        'testing_status' => $this->resource->tested_at ? 'completed' : 'in_progress',
                        'type' => $this->resource->qaSample->inventory->type->inventoryCategory->parent->slug,
                        'intermediate_type' => $this->resource->qaSample->inventory->type->inventoryCategory->slug,
                        'global_inventory_id' => $this->resource->qaSample->inventory->sync_code,
                        'global_batch_id' => $this->resource->qaSample->inventory->batch->sync_code,
                        'global_id' => $this->resource->sync_code,
                    ],
                    $analysesData
                ),
            ],
        ];
        $this->updatableMapping = [
            '',
        ];

        return $this;
    }

    protected function delete(): self
    {
        if (!$this->resource->sync_code) {
            $error = 'Unable to sync lab test deletion without LEAF global_id [sync_code]';
            Log::error($error);
            throw new Exception($error);
        }
        $this->httpMethod = Request::METHOD_DELETE;
        $this->requestUrl = sprintf("%s/%s", self::$endpoint, $this->resource->sync_code);

        return $this;
    }
}
