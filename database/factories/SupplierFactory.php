<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\Supplier;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Supplier::class,
    function (Faker $faker) {
        return [
            "internal_id" => fn($data) => ModelHelper::generateInternalId(
                Supplier::class,
                null,
                $data['account_holder_id']
            ),
            "name" => $faker->words(10, true),
            "city" => $faker->city,
            "state" => $faker->state,
            "zip_code" => $faker->countryCode,
            "address" => $faker->address,
            "email" => $faker->email,
            "phone_number" => $faker->phoneNumber,
        ];
    }
);
