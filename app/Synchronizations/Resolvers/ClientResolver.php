<?php

namespace App\Synchronizations\Resolvers;

use App\Models\{License, State};
use App\Synchronizations\Contracts\Http\ClientInterface;
use App\Synchronizations\Http\Clients\{LeafClient, MetrcClient};
use Exception;

class ClientResolver
{
    /**
     * @param License $license
     * @return ClientInterface
     *
     * @throws Exception
     */
    public static function resolve(License $license): ClientInterface
    {
        switch ($license->state->regulator) {
            case State::REGULATOR_LEAF:
                return new LeafClient($license);
            case State::REGULATOR_METRC:
                return new MetrcClient($license);
            default:
                throw new Exception("Can not resolve http client for state [{$license->state_code}]");
        }
    }
}
