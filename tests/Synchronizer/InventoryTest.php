<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch, Inventory, InventoryType, License, Room, Strain, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;

/**
 * Class InventoryTest
 * @package Tests\Synchronizer
 */
class InventoryTest extends TestBase
{
    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );
        $licenseId = $license->id;
        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Inventory::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ],
            ]
        );
        $data = [];

        $inventoryType = factory(InventoryType::class)->create(['license_id' => $licenseId,]);
        $batch = factory(Batch::class)->create(['license_id' => $licenseId,]);
        $strain = factory(Strain::class)->create(['license_id' => $licenseId,]);
        $room = factory(Room::class)->create(['license_id' => $licenseId,]);
        $totalRecord = rand(5, 10);
        for ($j = 1; $j <= $totalRecord; $j++) {
            $inventoryCode = "WAG010101.IN" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.AR4U;
            $data[] = [
                "global_id" => $inventoryCode,
                "external_id" => $this->faker->uuid,
                "global_inventory_type_id" => $inventoryType->sync_code,
                "global_batch_id" => $batch->sync_code,
                "global_strain_id" => $strain->sync_code,
                "global_area_id" => $room->sync_code,
                "uom" => "gm",
                "qty" => rand(1, 1000),
            ];
        }

        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
            'f_date1' => $fromDate->format('m/d/Y'),
            'f_date2' => $toDate->format('m/d/Y'),
        ];
        Http::fake(
            [
                sprintf("/inventories?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response(
                    $responseData,
                    200
                ),
            ]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Inventory::count());
    }

    /** @test */
    public function it_can_push_split_lot_to_leaf_api()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $sourceInventory = factory(Inventory::class)->create(['license_id' => $licenseId]);
        $newInventory = factory(Inventory::class)->create(
            [
                'license_id' => $licenseId,
                'parent_id' => $sourceInventory->id,
                'sync_code' => null,
                'synced_at' => null,
            ]
        );
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Inventory::SYNC_ACTION_SPLIT_LOT,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $newInventory->id,
                'resource_type' => get_classname($newInventory),
                'resource_data' => $newInventory->attributesToArray(),
                'resource_changes' => $newInventory->getChanges(),
            ]
        );

        $inventoryCode = "WAG010101.IN" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.IN7D9O;
        Http::fake(
            [
                '/split_inventory' => Http::response(
                    [
                        'external_id' => $newInventory->id,
                        'global_id' => $inventoryCode,
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
        $this->assertEquals($inventoryCode, $result->resource->sync_code);
    }
}
