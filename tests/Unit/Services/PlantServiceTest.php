<?php

namespace Tests\Unit\Services;

use App\Http\Parameters\Criteria;
use App\Models\Plant;
use App\Services\PlantService;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

class PlantServiceTest extends TestCase
{
    use Traceable;
    use WithFaker;

    protected static string $modelName = Plant::class;

    protected function getService(): PlantService
    {
        return app(PlantService::class);
    }

    /**
     * @test
     */
    public function it_can_get_list_filtered_by_license_id()
    {
        $this->withoutTraceableEvents();
        $expectedTotalRecords = rand(1, 50);
        $expectedLicenseId = $this->faker->uuid;
        factory(static::$modelName, $expectedTotalRecords)->create(['license_id' => $expectedLicenseId]);
        $randomTotalRecords = rand(1, 20);
        $randomLicenseId = $this->faker->uuid;
        factory(static::$modelName, $randomTotalRecords)->create(['license_id' => $randomLicenseId]);

        $criteria = (new Criteria())->setFilters(['license_id' => $expectedLicenseId]);
        // execute
        $result = $this->getService()->list($criteria);

        $this->assertEquals($expectedTotalRecords, $result->total());
    }
}
