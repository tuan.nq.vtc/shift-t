<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\License;
use App\Models\SourceCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    SourceCategory::class,
    function (Faker $faker) {
        return [
            'account_holder_id' => $faker->uuid,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                SourceCategory::class,
                null,
                $data['account_holder_id']
            ),
            'name' => $faker->word,
            'parent_id' => null,
            'description' => $faker->paragraph(),
            'status' => rand(0, 1),
        ];
    }
);
