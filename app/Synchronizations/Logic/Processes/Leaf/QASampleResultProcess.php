<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Cqrs\Commands\SyncQASampleResultCommand;
use App\Cqrs\Commands\SyncQASampleResultHandler;
use Exception;
use Psr\Log\LoggerInterface;
use Throwable;

class QASampleResultProcess extends BaseProcess
{
    /**
     * @return string[]
     */
    protected function listHandlers(): array
    {
        return [
            SyncQASampleResultHandler::class,
        ];
    }

    protected function list() {
        /**
         * @var LoggerInterface $logger
         */
        $logger = app(LoggerInterface::class);

        if (!is_array($this->payload)
            || !isset($this->payload['data'])
            || !is_array($this->payload['data'])) {
            $logger->error('the payload is invalid');
            return;
        }

        foreach ($this->payload['data'] as $item) {
            if (!is_array($item)) {
                continue;
            }

            try {
                $this->processListItem($item);
            } catch (Throwable $e) {
                $logger->error($e->getMessage(), $e->getTrace());
            }
        }
    }

    /**
     * @throws Exception
     */
    protected function processListItem($item): void
    {
        $handler = $this->handlers[SyncQASampleResultHandler::class];
        if ($handler instanceof SyncQASampleResultHandler !== true) {
            return;
        }

        $handler->handle(new SyncQASampleResultCommand($item));
    }
}
