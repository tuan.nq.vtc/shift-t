<?php

/** @var Factory $factory */

use App\Models\SourceCategory;
use App\Models\State;
use App\Models\StateCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    StateCategory::class,
    function (Faker $faker) {
        $state = State::all()->whereNotNull('regulator')->random();
        return [
            'regulator' => $state->regulator,
            'state_code' => $state->code,
            'name' => $faker->word,
            'code' => $faker->word,
            'type' => $faker->randomElement([StateCategory::TYPE_SOURCE, StateCategory::TYPE_END]),
            'parent_id' => null,
            'is_strain_required' => rand(0, 1),
        ];
    }
);
