<?php

use App\Models\Propagation;
use Illuminate\Database\Seeder;

class UpdateMotherCodeForPlant extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        $propagations = Propagation::query()->whereNotNull('mother_code')->get()->load('plants');
        foreach ($propagations as $propagation) {
            $propagation->plants->each->update(['mother_code' => $propagation->mother_code]);
        }
    }
}
