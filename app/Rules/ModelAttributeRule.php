<?php

namespace App\Rules;

class ModelAttributeRule
{
    private static $models = [];

    public static function required(string $model, ?string $uuid, callable $callback)
    {
        if (($modelValue = self::prepareModelValue($model, $uuid)) === '') {
            return '';
        }

        return call_user_func($callback, $modelValue) ? 'required' : 'nullable';
    }

    private static function prepareModelValue(string $model, ?string $uuid)
    {
        if (empty($uuid)) {
            return '';
        }

        $modelValue = self::getModelValue($model, $uuid);
        if (!$modelValue) {
            return '';
        }

        return $modelValue;
    }

    private static function getModelValue(string $model, string $uuid)
    {
        $modelValue = self::$models[$model] ?? null;
        if (!$modelValue) {
            $modelValue = self::$models[$model] = $model::find($uuid);
        }

        return $modelValue;
    }

    public static function in(string $model, ?string $uuid, callable $callback)
    {
        if (($modelValue = self::prepareModelValue($model, $uuid)) === '') {
            return '';
        }

        return call_user_func($callback, $modelValue);
    }

    public static function between(string $model, ?string $uuid, callable $callback, $min = null, $max = null)
    {
        if (($modelValue = self::prepareModelValue($model, $uuid)) === '') {
            return '';
        }

        return (is_null($min) || is_null($max)) ? call_user_func($callback, $modelValue) : "between:$min,$max";
    }
}
