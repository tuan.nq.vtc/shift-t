<?php

namespace App\Jobs\Synchronizations;

use App\Jobs\Job;
use App\Models\Trace;
use App\Synchronizations\Synchronizer;
use Log;
use Throwable;

class SyncPullData extends Job
{
    public $chained = [];
    public $deleteWhenMissingModels = true;
    public $delay = 5;
    public $maxTries = 5;

    private Trace $trace;

    public function __construct(Trace $trace)
    {
        $this->queue = 'sync_pull_data';
        $this->trace = $trace;
    }

    /**
     * Execute the job.
     *
     * @param Synchronizer $synchronizer
     *
     * @return void
     *
     * @throws Throwable
     */
    public function handle(Synchronizer $synchronizer)
    {
        $synchronizer->pull($this->trace);
        $pendingTraces = $this->trace->refresh()->children()->where('status', Trace::STATUS_PENDING)->get();
        foreach ($pendingTraces as $childTrace) {
            // dispatch if there are no pending/processing dependencies
            if ($childTrace->dependencies()->where('status', '!=', Trace::STATUS_COMPLETED)->count() === 0) {
                dispatch(new self($childTrace));
            }
        }
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception)
    {
        Log::error(
            "Sync Pull {$this->queue} [{$this->trace->id}] failed {$exception->getMessage()} [attempts - {$this->attempts() }]"
        );
    }
}
