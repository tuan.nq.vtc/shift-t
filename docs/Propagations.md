### METRC

* Required entities:
    - Strain
    - Location:
        - Equivalent to **Room** in `Trace v1` or **Area** in `LEAF` system.
    - Location Type:
        - Equivalent to **Room Type** in `Trace v1` or **Area Type** in `LEAF` system.
    - Item:
        - Used to track the licensee’s inventory through the supply chain life cycle.
        - Item name used to identify what type of item is packed into a package
    - Category:
        - Defined the item categories to be used by the industry.
    - Package:
        - Created from immature plants, harvest batches, testing lab samples, production batches, and other packages.
    - Plant:
        - Has to be in `flowering` phase.
    - Batch (aka group of `immature plants`)
* Workflow:
    - Create planting (create a new `immature plants` group):
        - Source:
            - From a plant in `flowering` phase.
            - From package of categories:
                - `Clone - Cutting` (with count based `uom`)
                - `Clone - Tissue culture` (with count based `uom`).
                - `Immature plants` (with count based `uom`).
                - `Seeds` (with count based or weight based `uom`)
            - From package of `seed` category
        - Limits 100 `immature plants` per batch
        - Store in a `location`.
        - Belongs to one `strain`.
        - Has one `tag` for the whole batch.
    - In case of creating from a flowering `plant`:
    - In case of creating from a `package`:
        - Decrease quantity of `package`

### LEAF

* Required entities:
    - Strain:
    - Area:
        - Equivalent to **Room** in `Trace v1` or **Location** in `METRC` system.
    - Area Type:
        - Equivalent to **Room Type** in `Trace v1` or **Location Type** in `METRC` system.
    - Batch:
        - Initial `type` is `propagation material`.
    - Inventory Type:
        - Showed as **Category** in web UI
        - Equivalent to **Item** in `METRC` system
    - Inventory Intermediate Type:
        - Showed as **Sub Category** in web UI
        - Equivalent to **Category** in `METRC` system
    - Inventory (aka Lot in `LEAF` web UI and equivalent to **Package** in `Trace v1` or in `LEAF`)
    - Plant:
        - Has to be `mother plant`
* Workflow:
    - Create new `batch`:
        - Type: `propagation material`.
        - Strain: Sync code (LEAF global_id)
        - Room: Sync code (LEAF global_id)
        - Origin (source): Valid (seed, plant, clone, tissue) 
        - Child/Parent batch: none.
    - Create a `inventory type`:
        - Name format: `{strain_name} {batch_type} {source_type}`(ex: **"Blue Dream propagation material seed"**).
        - Category: **Immature Plant**.
        - Sub category: **Seed** for seed, **Clone** for clone, **Plant**, **Tissue**
        - Unit of mesure: *ea** (each).
    - Create an `inventory`:
        - Belongs to the created `batch`.
        - Belongs to the created `inventory type` with `category`, `sub category`, `uom`.
        - Has plant propagation source (`source_type`).
        - Has quantity
        - Has `room` name & `room type` (quarantine or non-quarantine) - (**storage location**)
