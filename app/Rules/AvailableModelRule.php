<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\{Arr, Str};

class AvailableModelRule implements Rule
{
    private $model;

    public function __construct(string $model)
    {
        $this->model = $model;
    }

    public function passes($attribute, $value)
    {
        $ids = Arr::wrap($value);
        return $this->model::whereIn('id', $ids)->count() == count($ids);
    }

    public function message()
    {
        $nameClass = class_basename($this->model);
        return __('exceptions.' . Str::snake($nameClass) . '.not_exist');
    }
}

