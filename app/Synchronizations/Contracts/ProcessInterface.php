<?php

namespace App\Synchronizations\Contracts;

use App\Models\Trace;
use App\Synchronizations\Logic\Processes\PullProcessAggregate;

interface ProcessInterface
{
    /**
     * @param Trace $trace
     * @param array $payload
     */
    public function __construct(Trace $trace, array $payload = []);

    public function execute(string $action): void;
}
