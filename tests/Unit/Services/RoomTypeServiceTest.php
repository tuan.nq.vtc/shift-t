<?php

namespace Tests\Unit\Services;

use App\Models\RoomType;
use App\Services\RoomTypeService;
use Tests\TestCase;

class RoomTypeServiceTest extends TestCase
{
    protected static string $modelName = RoomType::class;

    protected function getService(): RoomTypeService
    {
        return app(RoomTypeService::class);
    }
}
