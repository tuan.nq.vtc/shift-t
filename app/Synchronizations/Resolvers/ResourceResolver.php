<?php

namespace App\Synchronizations\Resolvers;

use App\Models\Trace;
use App\Synchronizations\Contracts\Http\ResourceInterface;
use App\Synchronizations\Exceptions\SyncException;
use Exception;

class ResourceResolver
{
    /**
     * @param Trace $trace
     * @return ResourceInterface
     *
     * @throws Exception
     */
    public static function resolve(Trace $trace): ResourceInterface
    {
        $license = $trace->license;
        if (empty($regulator = $license->state->regulator)) {
            throw new Exception(
                "Regulator of state [{$regulator}] not configured"
            );
        }
        if (empty($stateResources = $license->getStateResources())) {
            throw new Exception(
                "Resources of regulator [{$regulator}] for state [{$license->state_code}] not configured"
            );
        }

        $traceableModel = $trace->getResource();
        foreach ($stateResources as $traceResourceClass => $stateResourceClass) {
            if ($traceableModel instanceof $traceResourceClass) {
                $httpResource = new $stateResourceClass(
                    $traceableModel,
                    $trace->resource_changes ?? [],
                    $trace->resource_conditions ?? []
                );
                try {
                    $httpResource->build($trace->action);
                } catch (Exception $e) {
                    throw SyncException::buildResourceFailed();
                }
                return $httpResource;
            }
        }

        throw new Exception(
            "Can not resolve http resource of vendor [{$regulator}] for state [{$license->state_code}]"
        );
    }
}
