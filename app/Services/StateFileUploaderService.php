<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

/**
 * Class StateFileUploaderService
 *
 * @package App\Services
 */
class StateFileUploaderService
{
    private string $baseUrl;
    private string $uploadPath;
    private PendingRequest $request;

    public function __construct()
    {
        $this->baseUrl = config('app.services.state_file_uploader.base_url');
        $this->uploadPath = config('app.services.state_file_uploader.paths.upload');
        $this->request = Http::withoutVerifying()->baseUrl($this->baseUrl);
    }

   
    public function upload(string $state, array $dependencyGroups): string
    {
        $data = [
            'state' => $state,
            'files' => $dependencyGroups
        ];

        $response = $this->request->post($this->uploadPath, $data);

        if ($response->failed()) {
            throw new Exception($response->body());
        }

        return $response->body();    
    }
}
