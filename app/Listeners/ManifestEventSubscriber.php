<?php

namespace App\Listeners;

use App\Events\Manifest\ManifestItemSaving;
use Illuminate\Events\Dispatcher;

class ManifestEventSubscriber
{
    /**
     * @param ManifestItemSaving $event
     */
    public function onSaving($event)
    {
        $manifestItem = $event->manifestItem;
        if ($manifestItem->isDirty(['quantity', 'price'])) {
            $manifestItem->fill(['total' => round($manifestItem->quantity * $manifestItem->price, 2)]);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [ManifestItemSaving::class],
            'App\Listeners\ManifestEventSubscriber@onSaving'
        );
    }
}
