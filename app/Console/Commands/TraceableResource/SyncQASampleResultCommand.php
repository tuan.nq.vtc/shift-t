<?php

namespace App\Console\Commands\TraceableResource;

use App\Models\QASample;
use App\Models\Trace;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Synchronizer;
use Illuminate\Console\Command;
use Throwable;

class SyncQASampleResultCommand extends Command
{
    protected $signature = 'qa-sample:sync';

    protected $description = 'Sync qa sample results';

    public function handle()
    {
        $qaSamples = QASample::where('status', QASample::STATUS_AWAITING_RESULT)->get();

        if (!$qaSamples->count()) {
            $this->comment('There is no QA Sample is waiting for result');
            return;
        }

        foreach ($qaSamples as $qaSample) {
            if ($qaSample instanceof QASample !== true) {
                continue;
            }

            try {
                $this->info(sprintf('Starting sync data for the QA Sample \'%s\'', $qaSample->getAttribute('id')));
                $trace = $this->createTrace($qaSample->getAttribute('license_id'));
                app(Synchronizer::class)->pull($trace);
                $this->info(sprintf('The QA Sample \'%s\' has been synced', $qaSample->getAttribute('id')));
            } catch (Throwable $e) {
                $this->comment(sprintf('Sync qa sample \'%s\' failed', $qaSample->getAttribute('id')));
                $this->error($e->getMessage());
            }
        }
    }

    private function createTrace($licenseId)
    {

        // TODO need refactor
        /**
         * @var Trace $model
         */
        $model = app(Trace::class);
        $trace = $model->where('status', Trace::STATUS_PENDING)
            ->first();

        if ($trace instanceof Trace === true) {
            return $trace;
        }

        $trace = new Trace([
            'license_id' => $licenseId,
            'action' => TraceableModelInterface::SYNC_ACTION_LIST,
            'status' => Trace::STATUS_PENDING,
            'method' => Trace::METHOD_PULL,
        ]);

        $trace->save();

        return $trace;
    }
}
