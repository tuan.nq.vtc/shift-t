<?php

namespace App\Listeners;

use App\Events\AdditiveInventory\AdditiveInventoryCreating;
use App\Services\AdditiveInventoryService;
use Illuminate\Events\Dispatcher;

class AdditiveInventoryEventSubscriber
{
    /**
     * @var AdditiveInventoryService
     */
    private AdditiveInventoryService $additiveInventoryService;

    public function __construct(AdditiveInventoryService $additiveInventoryService)
    {
        $this->additiveInventoryService = $additiveInventoryService;
    }

    /**
     * @param $event
     */
    public function onCreating($event)
    {
        $additiveInventory = $event->additiveInventory;
        $additiveInventory->id = $additiveInventory->generateUuid();

        $this->additiveInventoryService->calculateOnCreating($additiveInventory);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            AdditiveInventoryCreating::class,
            'App\Listeners\AdditiveInventoryEventSubscriber@onCreating'
        );
    }
}
