<?php

namespace App\Services;

use App\Models\Batch;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class BatchService
 * @package App\Services
 *
 * @method Batch|Collection get($id)
 * @method Batch|Builder getModel()
 */
class BatchService extends BaseService
{
    protected array $relations = [
        'parent',
        'children',
        'inventories',
        'propagations',
        'plants',
        'harvests',
    ];

    public function __construct(Batch $batch)
    {
        parent::__construct($batch);
    }
}
