<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\Licensee;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

/**
 * Class LicenseeService
 * @package App\Services
 */
class LicenseeService extends BaseService
{
    public function __construct(Licensee $licensee)
    {
        parent::__construct($licensee);
    }

    public function getByType(string $type, string $stateCode)
    {
        return Licensee::query()
            ->where('state_code', $stateCode)
            ->where('type', $type)->get();
    }

}
