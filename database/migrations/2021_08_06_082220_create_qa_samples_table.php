<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQaSamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_samples', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();

            $table->tinyInteger('sample_type')->index();
            $table->uuid('lab_id')->index();
            $table->uuid('source_inventory_id')->index();
            $table->uuid('manifest_id')->nullable();
            $table->uuid('strain_id')->nullable();
            $table->double('weight', 10, 2)->default(0);
            $table->tinyInteger('status')->nullable();

            $table->json('results')->nullable();
            $table->string('analysis_type')->default('total_cannabinoid');

            $table->timestamp('tested_at')->nullable();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('strain_id')->references('id')->on('strains')->nullOnDelete();
            $table->foreign('source_inventory_id')->references('id')->on('inventories')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_samples');
    }
}
