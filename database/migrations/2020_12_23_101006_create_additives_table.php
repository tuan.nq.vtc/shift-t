<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(
            'additives',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('name')->index();
                $table->enum('type', ['pesticide', 'nutrient', 'other'])->default('pesticide');
                $table->json('ingredients')->nullable();
                $table->uuid('license_id')->index();
                $table->string('uom', 50)->default('Gallons');
                $table->double('available_quantity', 10, 2)->default(0);
                $table->double('total_quantity', 10, 2)->default(0);
                $table->double('avg_price', 10, 2)->default(0);
                $table->boolean('visibility')->default(true)->index();
                $table->string('epa_regulation_number')->nullable();
                $table->boolean('is_other_type')->default(false);

                $table->uuid('created_by')->nullable();
                $table->uuid('updated_by')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additives');
    }
}
