<?php

/** @var Factory $factory */

use App\Models\{HarvestGroup, License, Room};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    HarvestGroup::class,
    function (Faker $faker) {
        $startDate = $faker->dateTimeThisYear();
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'room_id' => fn($data) => factory(Room::class)->create(['license_id' => $data['license_id']])->id,
            'name' => $faker->word,
            'status' => $faker->randomElement(HarvestGroup::STATUSES),
            'type' => $faker->randomElement(HarvestGroup::TYPES),
            'start_at' => $startDate,
            'est_end_at' => Carbon::parse($startDate)->addDays(rand(5, 15)),
            'finished_at' => fn($data) => $data['status'] === HarvestGroup::STATUS_CLOSED ? Carbon::parse(
                $startDate
            )->addDays(rand(5, 15)) : null,
        ];
    }
);
