<?php

namespace App\Events\Inventory;

use App\Events\Event;
use App\Models\Inventory;
use Illuminate\Support\Collection;

class InventoryLotIsSplit extends Event
{
    /**
     * @var Inventory
     */
    public Inventory $sourceInventory;
    /**
     * @var Collection
     */
    public Collection $newInventories;

    /**
     * @param Inventory $sourceInventory
     * @param Collection $newInventories
     */
    public function __construct(Inventory $sourceInventory, Collection $newInventories)
    {
        $this->sourceInventory = $sourceInventory;
        $this->newInventories = $newInventories;
    }
}
