<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Services\PlantService;
use Illuminate\Http\{JsonResponse, Request};
use Laravel\Lumen\Routing\Controller;

/**
 * Class GrowCyclePlantsController
 * @package App\Http\Controllers
 */
class GrowCyclePlantsController extends Controller
{
    /**
     * @var PlantService
     */
    private PlantService $plantService;

    /**
     * @var Request
     */
    private Request $request;

    public function __construct(PlantService $plantService, Request $request)
    {
        $this->plantService = $plantService;
        $this->request = $request;
    }

    /**
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function index($id)
    {
        return response()->json(
            $this->plantService->listByGrowCycle($id, Criteria::createFromRequest($this->request))
        );
    }

}
