<?php

namespace App\Synchronizations\Resolvers;

use App\Models\State;
use App\Models\Trace;
use App\Synchronizations\Contracts\Http\QueryIteratorInterface;
use App\Synchronizations\Http\QueryIterators\LeafQueryIterator;
use App\Synchronizations\Http\QueryIterators\MetrcQueryIterator;
use Exception;

class QueryIteratorResolver
{
    /**
     * @param Trace $trace
     * @return QueryIteratorInterface
     *
     * @throws Exception
     */
    public function resolve(Trace $trace): QueryIteratorInterface
    {
        switch ($trace->license->state->regulator) {
            case State::REGULATOR_LEAF:
                return new LeafQueryIterator($trace);
            case State::REGULATOR_METRC:
                return new MetrcQueryIterator($trace);
            default:
                throw new Exception("Can not resolve query iterator for state [{$trace->state_code}]");
        }
    }
}
