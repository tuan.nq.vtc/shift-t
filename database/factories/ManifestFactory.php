<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\Driver;
use App\Models\License;
use App\Models\Licensee;
use App\Models\Manifest;
use App\Models\Vehicle;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Manifest::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Manifest::class,
                $data['license_id']
            ),
            'method' => $faker->randomElement([Manifest::METHOD_INBOUND, Manifest::METHOD_OUTBOUND]),
            'type' => $faker->randomElement([Manifest::TYPE_TRANSFER, Manifest::TYPE_ORDER, Manifest::TYPE_QA_SAMPLE]),
            'status' => $faker->randomElement(
                [
                    Manifest::STATUS_OPEN,
                    Manifest::STATUS_IN_TRANSIT,
                    Manifest::STATUS_RECEIVED,
                    Manifest::STATUS_CANCELED,
                    Manifest::STATUS_VOIDED,
                    Manifest::STATUS_REJECTED,
                    Manifest::STATUS_PARTIALLY_REJECTED,
                ]
            ),
            'transport_type' => $faker->randomElement(
                [
                    Manifest::TRANSPORT_TYPE_DELIVERY,
                    Manifest::TRANSPORT_TYPE_PICKUP,
                    Manifest::TRANSPORT_TYPE_LICENSED_TRANSPORTER,
                ]
            ),
            'client_id' => fn() => factory(Licensee::class)->create()->id,
            'invoice_total' => $faker->randomNumber(3),
            'published_at' => $faker->dateTimeThisYear(),
            'est_departure_date' => $faker->dateTimeThisYear(),
            'est_arrival_date' => $faker->dateTimeThisYear(),
            'driver_id' => fn($data) => factory(Driver::class)->create(['license_id' => $data['license_id']])->id,
            'vehicle_id' => fn($data) => factory(Vehicle::class)->create(['license_id' => $data['license_id']])->id,
            'sync_code' => 'WAG010101.IT' . $faker->regexify('[A-Z0-9]{4}'),
            'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
            'synced_at' => $faker->dateTimeThisYear(),
        ];
    }
);

