<?php

namespace App\Services;

use App\Jobs\Synchronizations\SyncPullData;
use App\Jobs\Synchronizations\SyncData;
use Illuminate\Support\Carbon;
use App\Models\{License, Propagation, State, Trace, TraceableModel};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class TraceService
 * @package App\Services
 */
class TraceService
{
    private Trace $model;

    /**
     * TraceService constructor.
     * @param Trace $trace
     */
    public function __construct(Trace $trace)
    {
        $this->model = $trace;
    }

    /**
     * @param TraceableModelInterface|Model $resource
     * @param string $action
     *
     * @param array $dependencies
     * @return Trace|null
     */
    public function create(TraceableModelInterface $resource, string $action, array $dependencies = []): ?Trace
    {
        $trace = $this->model->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $resource->license->id,
                'action' => $action,
                'resource_id' => $resource->id,
                'resource_type' => get_classname($resource),
                'resource_data' => $resource->attributesToArray(),
                'resource_changes' => $resource->getChanges(),
            ]
        );

        if (!empty($dependencies)) {
            $trace->dependencies()->attach($dependencies);
        } else {
            // dispatch sync job
            if(!$resource->license->hasAsyncIntegration()) {
                dispatch(new SyncData($trace));
            }
        }

        return $trace;
    }

    /**
     * @param TraceableModelInterface|Model $resource
     * @param string $action
     *
     * @return Trace|null
     *
     * @throws Throwable
     */
    public function createPush(TraceableModelInterface $resource, string $action): ?Trace
    {
        $trace = $this->model->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $resource->license->id,
                'action' => $action,
                'resource_id' => $resource->id,
                'resource_type' => get_classname($resource),
                'resource_data' => $resource->attributesToArray(),
                'resource_changes' => $resource->getChanges(),
            ]
        );

        // dispatch sync job
        if(!$resource->license->hasAsyncIntegration()) {
            dispatch(new SyncData($trace));
        }

        return $trace;
    }

    /**
     * @param TraceableModelInterface|Model $resource
     * @param array $chainActions
     * @param array $dependencies
     * @return Collection
     */
    public function createChainPushActions(
        TraceableModelInterface $resource,
        array $chainActions,
        array $dependencies = []
    ): Collection {
        $collection = collect();
        $previousTraceId = null;
        $hasDependencies = !empty($dependencies);
        foreach ($chainActions as $action) {
            /** @var Trace $trace */
            $trace = $this->model->create(
                [
                    'method' => Trace::METHOD_PUSH,
                    'license_id' => $resource->license->id,
                    'action' => $action,
                    'resource_id' => $resource->id,
                    'resource_type' => get_classname($resource),
                    'resource_data' => $resource->attributesToArray(),
                    'resource_changes' => $resource->getChanges(),
                ]
            );
            if ($previousTraceId) {
                $trace->dependencies()->attach($previousTraceId);
            } elseif($hasDependencies) {
                $trace->dependencies()->attach($dependencies);
            }
            $previousTraceId = $trace->id;
            $collection->push($trace);
        }

        if (!$hasDependencies && !$resource->license->hasAsyncIntegration()) {
            dispatch(new SyncData($collection->shift()));
        }

        return $collection;
    }

    /**
     * @param License $license
     * @param string $resourceClass
     * @param array $conditions
     * @return Trace
     */
    public function createPull(License $license, string $resourceClass, array $conditions = []): Trace
    {
        $trace = Trace::create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_LIST,
                'resource_type' => $resourceClass,
                'status' => Trace::STATUS_PENDING,
                'resource_conditions' => $conditions,
            ]
        );

        if(!$license->hasAsyncIntegration()) {
            dispatch(new SyncData($trace));
        }

        return $trace;
    }

    /**
     * @param License $license
     * @param array $resources
     * @param array $conditions
     * @return Collection
     * @throws \Exception
     */
    public function createChainPull(License $license, array $resources, array $conditions = []): Collection
    {
        $collection = collect();
        $previousTraceId = null;
        foreach ($resources as $resource) {
            $trace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_LIST,
                    'resource_type' => $resource,
                    'resource_conditions' => $conditions,
                    'status' => Trace::STATUS_PENDING
                ]
            );
            if ($previousTraceId) {
                $trace->dependencies()->attach($previousTraceId);
            }
            $previousTraceId = $trace->id;
            $collection->push($trace);
        }
        
        if(!$license->hasAsyncIntegration()) {    
            dispatch(new SyncData($collection->shift()));
        }

        return $collection;
    }
}
