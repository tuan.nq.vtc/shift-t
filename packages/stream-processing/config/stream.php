<?php

return [
    'default' => env('STREAM_CHANNEL', 'kafka'),
    'channels' => [
        'kafka' => [
            'connection' => [
                'global' => [
                    'group.id' => env('KAFKA_GROUP_ID'),
                    'metadata.broker.list' => env('KAFKA_HOST', 'kafka').':'.env('KAFKA_PORT', '9092'),
                    'enable.auto.commit' => 'false',
//                        'sasl.username' => env('KAFKA_SYNC_PASSWORD'),
//                        'sasl.password' => env('KAFKA_SYNC_USERNAME'),

                ],
                'topic' => ['auto.offset.reset' => 'beginning',],
            ]
        ],
    ],
    'consumers' => [
        'processes' => [
//            env('STREAM_TOPIC_USER', 'user') => UserProcess::class,
        ]
    ]
];
