<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Executable;
use App\Cqrs\Values\QASample;

class CreateQASampleCommand extends Executable
{
    /**
     * @var string
     */
    protected $licenseId;

    /**
     * @var string|null
     */
    protected $userId;

    /**
     * @var QASample[]
     */
    protected $items = [];

    /**
     * @var string[]
     */
    protected $mapping = [
        'licenseId|string' => 'license_id',
        'userId|string' => 'user_id'
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);

        if (!isset($data['inventories']) || !is_array($data['inventories'])) {
            return;
        }

        foreach ($data['inventories'] as $item) {
            $this->items[] = new QASample($item);
        }
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        if (empty($this->userId)
            || empty($this->licenseId)
            || empty($this->items)) {
            return false;
        }

        foreach ($this->items as $item) {
            if (!$item->isValid()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_map(function (QASample $item) {
            return array_merge(['license_id' => $this->licenseId, 'created_by' => $this->userId], $item->toArray());
        }, $this->items);
    }

    /**
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (QASample $item) {
            return $item->getId();
        }, $this->items);
    }

    /**
     * @return string
     */
    public function getLicenseId(): string
    {
        return $this->licenseId;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
}
