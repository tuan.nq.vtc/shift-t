<?php

namespace App\Synchronizations\Http\Resources\Leaf;

class QASampleResultResource  extends BaseLeafResource
{
    protected static string $endpoint = '/lab_results';
}
