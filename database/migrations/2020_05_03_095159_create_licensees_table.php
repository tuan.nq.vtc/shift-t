<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenseesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licensees', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('regulator')->index();
            $table->string('name')->index();
            $table->string('address1')->index();
            $table->string('address2')->nullable();
            $table->string('city')->index()->nullable();
            $table->string('state_code')->index();
            $table->string('phone')->index();
            $table->string('type')->index();
            $table->string('code')->index();
            $table->string('sync_code')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licensees');
    }
}
