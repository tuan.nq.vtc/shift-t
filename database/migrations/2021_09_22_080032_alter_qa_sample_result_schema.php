<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterQaSampleResultSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qa_sample_results', function($table) {
            $table->dropColumn([
                'sync_code',
                'sync_status',
                'synced_at'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qa_sample_results', function($table) {
            $table->string('sync_code')->nullable()->index();
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();
        });
    }
}
