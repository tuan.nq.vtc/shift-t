<?php

namespace App\Services;

use App\Models\LabTest;
use Illuminate\Support\Facades\DB;

/**
 * Class LabTestService
 * @package App\Services
 */
class LabTestService extends BaseService
{
    public function __construct(LabTest $labTest)
    {
        parent::__construct($labTest);
    }

    public function create(array $data): LabTest
    {
        try {
            DB::beginTransaction();

            $analyses = $data['analyses'] ?? [];
            $userId = $data['user_id'] ?? '';

            $labTest = $this->model->create($data);
            $analyses = array_map(function($item) use ($labTest, $userId) {
                $item['lab_test_id'] = $labTest->id;
                $item['editor'] = $userId;
                return $item;
            }, $analyses);
            $labTest->labTestAnalyses()->attach($analyses);

            DB::commit();
            return $labTest;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function update($labTestId, array $data): LabTest
    {
        try {
            DB::beginTransaction();

            $labTest = $this->model->findOrFail($labTestId);

            $analyses = $data['analyses'] ?? [];
            $userId = $data['user_id'] ?? '';

            $labTest->update($data);

            if (count($analyses) > 0) {
                $labTest->labTestAnalyses()->detach();
                $analyses = array_map(function($item) use ($labTest, $userId) {
                    $item['lab_test_id'] = $labTest->id;
                    $item['editor'] = $userId;
                    return $item;
                }, $analyses);
                $labTest->labTestAnalyses()->attach($analyses);
            }

            DB::commit();
            return $labTest;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }
}
