<?php

/** @var Factory $factory */

use App\Models\{Harvest, HarvestPlant, Plant};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    HarvestPlant::class, function (Faker $faker) {
    return [
        'harvest_id' => function () {
            return factory(Harvest::class)->create()->id;
        },
        'plant_id' => function () {
            return factory(Plant::class)->create()->id;
        },
    ];
});
