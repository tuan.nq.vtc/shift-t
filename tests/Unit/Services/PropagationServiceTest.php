<?php

namespace Tests\Unit\Services;

use App\Models\GrowCycle;
use App\Models\Propagation;
use App\Models\Room;
use App\Services\PropagationService;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

class PropagationServiceTest extends TestCase
{
    use WithFaker;
    use Traceable;

    protected static string $modelName = Propagation::class;

    protected function getService(): PropagationService
    {
        return app(PropagationService::class);
    }

    /**
     * @test
     */
    public function it_can_create_item()
    {
        $this->withoutTraceableEvents();
        $expectedData = factory(static::$modelName)->raw();
        unset($expectedData['batch_id']);

        // execute
        $result = $this->getService()->create($expectedData);

        $this->assertEquals($expectedData, $result->only(array_keys($expectedData)));
    }
    /**
     * @test
     */
    public function it_can_move_to_vegetation()
    {
        $this->withoutTraceableEvents();
        $growCycle = factory(GrowCycle::class)->create();

        $transplant = factory(static::$modelName, rand(1, 3))->create(['license_id' => $growCycle->license_id])
            ->reduce(function($acc, $propagation) {
                $acc[] = [
                    'propagation_id' => $propagation->id,
                    'quantity' => $propagation->quantity,
                ];
                return $acc;
            }, []);
        $vegetationData = [
            'transplant' => $transplant,
            'planted_at' => $this->faker->date(),
            'room_id' => factory(Room::class)->create(['license_id' => $growCycle->license_id])->id,
        ];

        // execute
        $result = $this->getService()
            ->setLicenseId($growCycle->license_id)
            ->moveToVegetation($growCycle, $vegetationData);

        $this->assertCount(count($transplant), $result->plantGroups);
    }
}
