<?php

namespace App\Http\Controllers;

use App\Services\{SupplierService};
use Illuminate\Http\{JsonResponse, Request, Response};

/**
 * Class SupplierController
 *
 * @package App\Http\Controllers
 *
 * @property SupplierService $service
 */
class SupplierController extends CRUDController
{
    public function __construct(
        SupplierService $service,
        Request $request
    ) {
        parent::__construct($service, $request);
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => [
                'sometimes',
                'string',
                'min:3',
                'max:150',
                "unique:App\Models\Supplier,name,{$id},id",
            ],
            'city' => ['nullable', 'string',],
            'state' => ['nullable', 'string',],
            'zip_code' => ['nullable', 'string'],
            'email' => ['nullable', 'email'],
            'address' => ['nullable', 'string'],
            'phone_number' => ['nullable', 'string'],
        ];
    }

    /**
     * @return array
     */
    public function getCreateRules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\Supplier,name,null,id'
            ],
            'user_id' => ['required'],
            'city' => ['nullable', 'string',],
            'state' => ['nullable', 'string',],
            'zip_code' => ['nullable', 'string'],
            'email' => ['nullable', 'email'],
            'address' => ['nullable', 'string'],
            'phone_number' => ['nullable', 'string'],
        ];
    }


    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems(), Response::HTTP_OK);
    }

    public function getStatistic($id)
    {
        return response()->json($this->service->get($id)->additives, Response::HTTP_OK);
    }

}
