<?php

namespace App\Listeners;

use App\Events\Disposal\DisposalScheduled;
use App\Events\WasteBag\WasteBagCreated;
use App\Models\Disposal;
use App\Services\AdditiveService;
use App\Services\WasteBagService;
use Carbon\Carbon;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Event;

class WasteBagEventSubscriber
{
    /**
     * @var WasteBagService
     */
    private WasteBagService $wasteBagService;

    private AdditiveService $additiveService;

    /**
     * WasteBagListener constructor.
     *
     * @param WasteBagService $wasteBagService
     * @param AdditiveService $additiveService
     */
    public function __construct(WasteBagService $wasteBagService, AdditiveService $additiveService)
    {
        $this->wasteBagService = $wasteBagService;
        $this->additiveService = $additiveService;
    }

    /**
     * @param WasteBagCreated $event
     */
    public function onWasteBagCreated(WasteBagCreated $event)
    {
        $wasteBag = $event->wasteBag;
        $quarantineEnd = $wasteBag->wasted_at->clone()->addHours(Disposal::DEFAULT_EXTENDED_QUARANTINE_HOUR);
        $status = $quarantineEnd->lt(Carbon::now()) ?
                Disposal::STATUS_READY : Disposal::STATUS_SCHEDULED;
        /** @var Disposal $disposal */
        $disposal = $wasteBag->disposal()->create(
            [
                'license_id' => $wasteBag->license_id,
                'room_id' => $wasteBag->room_id,
                'scheduled_by_id' => $wasteBag->cut_by_id,
                'quantity' => $wasteBag->weight,
                'uom' => Disposal::UOM_GRAM,
                'wasted_at' => $wasteBag->wasted_at,
                'reason' => $wasteBag->reason,
                'comment' => $wasteBag->comment,
                'quarantine_start' => $wasteBag->wasted_at,
                'quarantine_end' => $quarantineEnd,
                'status' => $status,
            ]
        );

        // dispatch disposal scheduled
        Event::dispatch(new DisposalScheduled($disposal));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            WasteBagCreated::class,
            'App\Listeners\WasteBagEventSubscriber@onWasteBagCreated'
        );
    }
}
