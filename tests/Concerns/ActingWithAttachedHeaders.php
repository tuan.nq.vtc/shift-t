<?php

namespace Tests\Concerns;

use App\Models\License;
use App\Models\User;
use Ramsey\Uuid\Uuid;

trait ActingWithAttachedHeaders
{
    private array $headers = [];
    private string $licenseId;
    private string $userId;

    protected function actingWithLicense()
    {
        $this->licenseId = factory(License::class)->create()->id;
    }

    protected function actingAsUser()
    {
        $this->userId = factory(User::class)->create()->id;
    }

    protected function actingWithoutLicense()
    {
        $this->licenseId = '';
    }

    protected function actingAsGuest()
    {
        $this->userId = '';
    }

    /**
     * add params to request header
     *
     * @param array $headers
     * @return array
     */
    protected function transformHeadersToServerVars(array $headers)
    {
        if ($this->licenseId) {
            $headers[config('access.headers.current_license_id')] = $this->licenseId;
        }
        if ($this->userId) {
            $headers[config('app.services.auth.headers.user_uuid')] = $this->userId;
        }

        return parent::transformHeadersToServerVars($headers);
    }

}
