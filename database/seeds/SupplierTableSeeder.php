<?php

use App\Models\{Supplier};
use Illuminate\Database\Seeder;

/**
 * Class SupplierTableSeeder.
 */
class SupplierTableSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {
        factory(Supplier::class, 5)->create(
            [
                'account_holder_id' => 'f136252a-adfd-42a9-96a1-4ece04730600'
            ]
        );
    }
}
