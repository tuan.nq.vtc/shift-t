<?php

namespace App\Models;

use App\Models\Traits\{BelongToStrain,
    Boots\RoomStatistic,
    HasInternalId,
    Scope\DatetimeFilterable,
    Scope\LicenseFilterable,
    Scope\StatusFilterable,
    StoreInRoom};
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, MorphTo};
use Illuminate\Support\Str;

/**
 * Class Disposal
 * @package App\Models
 */
class Disposal extends TraceableModel
{
    use BelongToStrain;
    use StatusFilterable;
    use StoreInRoom;
    use LicenseFilterable;
    use DatetimeFilterable;
    use RoomStatistic;
    use HasInternalId;

    protected $dispatchesEvents = [];

    public const STATUS_SCHEDULED = 0;
    public const STATUS_READY = 1;
    public const STATUS_DESTROYED = 2;
    public const STATUS_UNSCHEDULED = 3;

    public const SYNC_ACTION_SCHEDULE = 'schedule';
    public const SYNC_ACTION_DESTROY = 'destroy';

    public const SOURCE_LEAF_DAILY_PLANT_WASTE = 'daily_plant_waste';
    public const SOURCE_LEAF_PLANT = 'plant';
    public const SOURCE_LEAF_BATCH = 'batch';
    public const SOURCE_LEAF_INVENTORY = 'inventory';

    public const REASON_WASTE = 'waste';

    // Base on logic at BM-288
    public const DEFAULT_EXTENDED_QUARANTINE_HOUR = 72;

    const UOM_EACH = 'ea';
    const UOM_GRAM = 'g';

    /**
     * @return array
     */
    public const REASONS = [
        'failed qa',
        'infestation',
        'quality control',
        'returned',
        'spoilage',
        'unhealthy',
        'mandated',
        'waste',
        'other',
        'pruning',
        'daily_waste', // Only for source daily_plant_waste - LEAF
    ];

    protected $fillable = [
        'status',
        'room_id',
        'quantity',
        'strain_id',
        'uom',
        'destroyed_at',
        'cancelled_at',
        'wastable_id',
        'wastable_type',
        'reason',
        'comment',
        'comment_unschedule',
        'refer_mother_code',
        'scheduled_by_id',
        'destroyed_by_id',
        'quarantine_start',
        'quarantine_end',
        'wasted_at',
        'info',
        'sync_code',
    ];

    protected $appends = ['quarantine_expired'];

    protected $dates = [
        'quarantine_start',
        'quarantine_end',
        'destroyed_at',
        'synced_at',
        'wasted_at',
    ];

    protected $casts = [
        'quantity' => 'decimal:2',
        'cancelled' => 'bool',
        'info' => 'array',
    ];

    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            return [
                'global_id' => fn($instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'reason' => fn($instance, $value) => $instance->setAttribute('reason', $value),
                'qty' => fn($instance, $value) => $instance->setAttribute('quantity', $value),
                'uom' => fn($instance, $value) => $instance->setAttribute('uom', $value),
                'hold_starts_at' => fn($instance, $value) => $instance->setAttribute('quarantine_start', $value),
                'hold_ends_at' => fn($instance, $value) => $instance->setAttribute('quarantine_end', $value),
                'disposal_at' => fn($instance, $value) => $instance->setAttribute('destroyed_at', empty($value) ? null : $value),
                'source' => fn($instance, $value) => $instance->setAttribute('source', $value),
                'global_batch_id' => fn($instance, $value) => $this->processLeafArea($instance, $value),
                'global_area_id' => fn($instance, $value) => $this->processLeafBatch($instance, $value),
                'global_plant_id' => fn($instance, $value) => $this->processLeafPlant($instance, $value),
                'global_inventory_id' => fn($instance, $value) => $this->processLeafInventory($instance, $value),
                // Save external_id from LEAF as an info attribute
                'external_id' => function ($instance, $value) {
                    $status = $instance->destroyed_at ? Disposal::STATUS_DESTROYED :
                        ($instance->quarantine_end->lt(Carbon::now()) ?
                        Disposal::STATUS_READY : Disposal::STATUS_SCHEDULED);
                    $instance->setAttribute('status', $status);
                    return $instance->setAttribute('info', ['external_id' => $value]);
                },
            ];
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            return [
                'Id' => fn($instance, $value) => $instance,
                'Label' => fn($instance, $value) => $this->processMetrcLabel($instance, $value),
                'Name' => function ($instance, $value) {
                    if ($instance->wastable_type === Propagation::class && $value) {
                        // TODO: wait sync propagation
                        if ($batch = Batch::with(['propagation'])->where('sync_code', $value)->first()) {
                            $instance->setAttribute('wastable_id', $batch->propagation->id);
                        }
                    }
                },
                'State' => function ($instance, $value) {
                    if ($value && Str::ucfirst($value) !== 'Destroyed') {
                        $instance = null;
                    }

                    return $instance;
                },
                'DestroyedDate' => function ($instance, $value) {
                    if ($value) {
                        $instance->setAttribute('quarantine_start', $value);
                        $instance->setAttribute('quarantine_end', $value);
                        $instance->setAttribute('destroyed_at', $value);
                    }

                    return $instance;
                },
                'DestroyedNote' => function ($instance, $value) {
                    $value && $instance->setAttribute('reason', $value);

                    return $instance;
                },
                'DestroyedCount' => function ($instance, $value) {
                    if ($instance->wastable_type === Propagation::class && $value) {
                        $instance->setAttribute('quantity', $value);
                        $instance->setAttribute('uom', 'ea');
                    }

                    return $instance;
                }
            ];
        }
        return null;
    }

    private function processLeafBatch($instance, $value) {
        if ($instance->source === self::SOURCE_LEAF_BATCH) {
            $instance->setAttribute('wastable_type', Propagation::class);
            $instance->setAttribute('wastable_id', 0);
        }
        // TODO: wait sync propagation
        // if ($instance->source === self::SOURCE_LEAF_BATCH &&
        //     $batch = Batch::with(['propagation'])->where('sync_code', $value)->first()
        // ) {
        //     $instance->setAttribute('wastable_type', Propagation::class);
        //     $instance->setAttribute('wastable_id', $batch->propagation->id);
        // }
        return $instance;
    }

    private function processLeafArea($instance, $value) {
        if ($instance->source === self::SOURCE_LEAF_DAILY_PLANT_WASTE) {
            $room = Room::where('sync_code', $value)->first();
            $wasteBag = new WasteBag([
                'license_id' => $instance->license->id,
                'room_id' => $room ? $room->id : null,
            ]);
            $wasteBag = $wasteBag->setAttribute($wasteBag->getKeyName(), $wasteBag->generateUuid());
            $wasteBag->saveWithoutEvents();

            $instance->setAttribute('wastable_type', WasteBag::class);
            $instance->setAttribute('wastable_id', $wasteBag->id);
        }
        return $instance;
    }

    private function processLeafPlant($instance, $value) {
        if ($instance->source === self::SOURCE_LEAF_PLANT) {
            $instance->setAttribute('wastable_type', Plant::class);
            $instance->setAttribute('wastable_id', 0);
        }
        // TODO: wait sync plant
        // if ($instance->source === self::SOURCE_LEAF_PLANT &&
        //     $plant = Plant::where('sync_code', $value)->first()
        // ) {
        //     $instance->setAttribute('wastable_type', Plant::class);
        //     $instance->setAttribute('wastable_id', $plant->id);
        // }
        return $instance;
    }

    private function processLeafInventory($instance, $value) {
        if ($instance->source === self::SOURCE_LEAF_INVENTORY) {
            $instance->setAttribute('wastable_type', Inventory::class);
            $instance->setAttribute('wastable_id', 0);
        }
        // TODO: wait sync inventory
        // if ($instance->source === self::SOURCE_LEAF_INVENTORY &&
        //     $inventory = Inventory::where('sync_code', $value)->first()
        // ) {
        //     $instance->setAttribute('wastable_type', Inventory::class);
        //     $instance->setAttribute('wastable_id', $inventory->id);
        // }
        unset($instance->source);
        return $instance;
    }

    private function processMetrcLabel($instance, $value) {
        if ($value) {
            // Disposal for plant
            $instance->setAttribute('wastable_type', Plant::class);
            $instance->setAttribute('wastable_id', 0);

            // TODO: wait sync plant
            // $plant = Plant::where('sync_code', $value)->first();
            // if ($plant) {
            //     $instance->setAttribute('wastable_type', Plant::class);
            //     $instance->setAttribute('wastable_id', $plant->id);
            // }
            $instance->setAttribute('quantity', 1);
            $instance->setAttribute('uom', 'ea');
        } else {
            // Disposal for propagation batch
            $instance->setAttribute('wastable_type', Propagation::class);

            // TODO: don't know how to get destroyed_at
            $instance->setAttribute('destroyed_at', Carbon::now());
        }

        return $instance;
    }

    public function wastable(): MorphTo
    {
        return $this->morphTo();
    }

    public function scheduledBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'scheduled_by_id', 'id');
    }

    public function destroyedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'destroyed_by_id', 'id');
    }

    public function scopeStatus($query, $status = self::STATUS_SCHEDULED)
    {
        return $query->where('status', $status);
    }

    public function scopeWastableType($query, ...$wastableType)
    {
        return $query->whereIn('wastable_type', $wastableType);
    }

    public function scopeUom($query, $uom)
    {
        return $query->where('uom', $uom);
    }

    public function scopeQuantity($query, $quantity)
    {
        return $query->where('quantity', $quantity);
    }

    public function scopeReason($query, ...$reason)
    {
        return $query->whereIn('reason', $reason);
    }

    public function scopeQuarantineStart(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'quarantine_start');
    }

    public function scopeQuarantineEnd(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'quarantine_end');
    }

    public function scopeScheduledById($query, ...$scheduleById)
    {
        return $query->whereIn('scheduled_by_id', $scheduleById);
    }

    public function getQuarantineExpiredAttribute()
    {
        return empty($this->quarantine_end) ? '' : $this->quarantine_end->longAbsoluteDiffForHumans();
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query->where('sync_code', 'like', '%' . $value . '%');
    }
}
