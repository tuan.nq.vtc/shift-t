<?php

namespace App\Models;

use App\Models\Traits\ApplyAdditive;
use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{CreatorFilterable,
    DatetimeFilterable,
    IdsFilterable,
    LicenseFilterable,
    ModifierFilterable,
    NameFilterable,
    StatusFilterable
};
use Illuminate\Database\Eloquent\{Builder,
    Relations\BelongsTo,
    Relations\BelongsToMany,
    Relations\HasMany,
    Relations\HasOne
};

/**
 * Class Room
 * @method Builder available()
 * @property string name
 * @property string sync_code
 * @property bool is_quarantine
 * @package App\Models
 */
class Room extends TraceableModel
{
    use IdsFilterable;
    use ApplyAdditive;
    use LicenseFilterable;
    use NameFilterable;
    use CreatorFilterable;
    use ModifierFilterable;
    use StatusFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected array $traceable = ['name', 'is_quarantine'];

    protected $fillable = [
        'name',
        'room_type_id',
        'status',
        'is_quarantine',
        'dimension',
        'rfid_tag',
        'created_by',
        'updated_by',
        'info',
        'sync_code',
        'synced_at',
    ];

    protected $casts = [
        'dimension' => 'array',
        'rfid_tag' => 'int',
        'status' => 'boolean',
        'is_quarantine' => 'boolean',
        'info' => 'array',
    ];

    protected $hidden = ['plants'];

    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            return [
                'global_id' => fn(self $instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'name' => fn(self $instance, $value) => $instance->setAttribute('name', $value),
                'type' => fn(self $instance, $value) => $instance->setAttribute('is_quarantine', 'quarantine' == $value),
            ];
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            return [
                'Id' => fn(self $instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'Name' => fn(self $instance, $value) => $instance->setAttribute('name', $value),
            ];
        }

        return null;
    }

    public function statistics(): HasOne
    {
        return $this->hasOne(RoomStatistic::class);
    }

    public function roomType(): BelongsTo
    {
        return $this->belongsTo(RoomType::class);
    }

    public function subrooms(): HasMany
    {
        return $this->hasMany(Subroom::class);
    }

    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class);
    }

    public function harvestGroups(): HasMany
    {
        return $this->hasMany(HarvestGroup::class);
    }

    public function propagations(): HasMany
    {
        return $this->hasMany(Propagation::class);
    }

    public function plantStrains(): BelongsToMany
    {
        return $this->belongsToMany(Strain::class, Plant::class);
    }

    public function inventoryStrains(): BelongsToMany
    {
        return $this->belongsToMany(Strain::class, Inventory::class);
    }

    public function growCycles(): BelongsToMany
    {
        return $this->belongsToMany(GrowCycle::class, Plant::class);
    }


    public function inventories(): HasMany
    {
        return $this->hasMany(Inventory::class);
    }

    public function disposals(): HasMany
    {
        return $this->hasMany(Disposal::class);
    }

    public function inventoryTypes(): BelongsToMany
    {
        return $this->belongsToMany(InventoryType::class, Inventory::class, 'room_id', 'type_id');
    }

    public function ipmOperations()
    {
        return $this->additiveOperations()->where('additive_type', '=', AdditiveOperation::ADDITIVE_TYPE_PESTICIDE);
    }

    public function feedOperations()
    {
        return $this->additiveOperations()->where('additive_type', '=', AdditiveOperation::ADDITIVE_TYPE_NUTRIENT);
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function modifier(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeAvailable(Builder $query): Builder
    {
        return $query->where('status', true);
    }

    public function scopeStrainId(Builder $query, ...$strainId): Builder
    {
        return $query->whereHas(
            'plants',
            fn(Builder $plantQuery) => $plantQuery->whereIn('strain_id', $strainId)
        );
    }

    public function scopeGrowStatus(Builder $query, ...$growStatus): Builder
    {
        return $query->whereHas(
            'plants',
            fn(Builder $plantQuery) => $plantQuery->whereIn('grow_status', $growStatus)
        );
    }

    public function scopeInventoryType($query, ...$inventoryType)
    {
        return $query->whereHas(
            'inventories',
            fn(Builder $inventoryQuery) => $inventoryQuery->whereIn('type_id', $inventoryType)
        );
    }

    /**
     * @param Builder $query
     * @param string $isQuarantine
     *
     * @return Builder
     */
    public function scopeIsQuarantine(Builder $query, string $isQuarantine): Builder
    {
        return $query->where('is_quarantine', $isQuarantine);
    }

    /**
     * @param Builder $query
     * @param $roomTypeId
     *
     * @return Builder
     */
    public function scopeRoomTypeId(Builder $query, ...$roomTypeId): Builder
    {
        return $query->whereIn('room_type_id', $roomTypeId);
    }

    public function scopeRoomType(Builder $query, ...$roomTypeId): Builder
    {
        return $this->scopeRoomTypeId($query, ...$roomTypeId);
    }

    /**
     * @param Builder $query
     * @param $isQuarantine
     *
     * @return Builder
     */
    public function scopeQuarantine(Builder $query, $isQuarantine): Builder
    {
        return $query->where('is_quarantine', $isQuarantine);
    }

    public function isDisposal(): bool
    {
        return $this->roomType->category === RoomType::CATEGORY_DISPOSAL;
    }

    public function scopeSearch(Builder $query, string $search): Builder
    {
        $search = trim($search);
        if (empty($search)) {
            return $query;
        }
        return $query->where(
            fn(Builder $subQuery) => $subQuery->where('sync_code', $search)
                ->orWhere('name', 'LIKE', '%' . $search . '%')
        );
    }

    public function scopeCategoryCode(Builder $query, ...$categoryCode): Builder
    {
        return $query->whereHas(
            'roomType',
            fn(Builder $query) => $query->whereIn('category', $categoryCode)
        );
    }

    public function scopeOrderByIsQuarantine(Builder $query, $direction): Builder
    {
        return $query->orderBy($this->getTable() . '.is_quarantine', $direction);
    }
}
