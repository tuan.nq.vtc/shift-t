<?php

namespace App\Services;

use App\Cqrs\Commands\RemoveQASampleCOACommand;
use App\Cqrs\Commands\RemoveQASampleCOAHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;
use Throwable;

class RemoveQASampleCOAService
{
    const MSG_FAILED = 'Remove QA Sample failed';

    /**
     * @var RemoveQASampleCOAHandler
     */
    private $handler;

    /**
     * @param RemoveQASampleCOAHandler $handler
     */
    public function __construct(RemoveQASampleCOAHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param RemoveQASampleCOACommand $command
     * @return JsonResponse|Response|ResponseFactory
     */
    public function __invoke(RemoveQASampleCOACommand $command)
    {
        try {
            $this->handler->handle($command);
            return response()->json(null, Response::HTTP_NO_CONTENT);
        } catch (Throwable $e) {
            return wpApiResponseWithErrors([$e->getMessage()], self::MSG_FAILED);
        }
    }
}
