<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait LicenseFilterable
{
    /**
     * Scope a query to only include records have license that contains $licenseId.
     *
     * @param Builder $query
     * @param string $licenseId
     * @return Builder
     */
    public function scopeLicenseId($query, string $licenseId)
    {
        return $query->where($this->getTable().'.license_id', $licenseId);
    }
}
