<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageIntoStrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('strains', 'logo')) {
            Schema::table('strains', function (Blueprint $table) {
                $table->dropColumn(['logo']);
            });
        }
        if (!Schema::hasColumn('strains', 'image')) {
            Schema::table('strains', function (Blueprint $table) {
                $table->string('image')->nullable()->after('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
