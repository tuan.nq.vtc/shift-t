<?php

namespace App\Rules;

use App\Models\BaseModel;
use Illuminate\Contracts\Validation\Rule;

class ModelsHaveTheSameProp implements Rule
{
    protected $modelClassName;
    protected $property;
    protected $where;
    protected $modelAttribute;
    protected $attribute;
    protected $modelIds;

    public function __construct(string $modelClassName, string $property, string $attribute = 'id', array $where = [])
    {
        $this->modelClassName = $modelClassName;
        $this->property = $property;
        $this->modelAttribute = $attribute;
        $this->where = $where;
    }

    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;
        $this->modelIds = array_unique(array_filter($value));
        $query = $this->modelClassName::withoutGlobalScopes([BaseModel::DEFAULT_ORDER_SCOPE])
            ->select([$this->property])
            ->whereIn($this->modelAttribute, $this->modelIds);
        foreach ($this->where as $column => $condition) {
            if (is_callable($condition)) {
                $query->where($condition);
            } else {
                $query->where($column, $condition);
            }
        }

        return 1 === $query->groupBy([$this->property])->get($this->property)->count();
    }

    public function message(): string
    {
        $modelIds = implode(', ', $this->modelIds);

        $classBasename = class_basename($this->modelClassName);

        return __(
            'validation.models_have_the_same_prop',
            [
                'attribute' => $this->attribute,
                'model' => $classBasename,
                'modelAttribute' => $this->modelAttribute,
                'modelIds' => $modelIds,
                'property' => $this->property,
            ]
        );
    }
}
