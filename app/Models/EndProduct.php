<?php

namespace App\Models;

use App\Events\EndProduct\EndProductSaved;
use App\Models\Traits\Scope\{NameFilterable};
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Product
 *
 * @package App\Models
 */
class EndProduct extends BaseModel
{
    use NameFilterable;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $fillable = [
        'product_id',
        'category_id',
        'state_code',
        'name',
        'status'
    ];

    protected $dispatchesEvents = [
        'saved' => EndProductSaved::class
    ];

    public function scopeCategoryId($query, ...$categoryIds): Builder
    {
        return $query->whereIn('category_id', $categoryIds);
    }
}
