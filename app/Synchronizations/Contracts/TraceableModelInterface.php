<?php

namespace App\Synchronizations\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface TraceableModelInterface
{
    public const SYNC_STATUS_PENDING = 0;
    public const SYNC_STATUS_PROCESSING = 1;
    public const SYNC_STATUS_FAILED = 2;
    public const SYNC_STATUS_SYNCED = 3;

    public const SYNC_ACTION_CREATE = 'create';
    public const SYNC_ACTION_UPDATE = 'update';
    public const SYNC_ACTION_DELETE = 'delete';
    public const SYNC_ACTION_LIST = 'list';
    public const SYNC_ACTION_FETCH_ALL = 'fetchAll';

    public function license(): BelongsTo;

    public function updateSyncStatus(int $status): self;

    public function updateSyncCode(string $code): self;

    public static function withoutTraceableModelEvents(callable $callback);
}
