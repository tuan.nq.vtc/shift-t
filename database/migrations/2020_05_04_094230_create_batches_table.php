<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'batches',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                // alter table batches modify source_id char(36) null
                $table->nullableUuidMorphs('source');
                $table->string('name', 200)->nullable();
                $table->uuid('parent_id')->nullable()->index();
                $table->unsignedSmallInteger( 'status')->default(1);

                // S2S Trace sync[Create/Update/Delete]
                $table->string('sync_code')->nullable()->index();
                /*
                  Add info column for storing fetched data
                  alter table batches add info json null;
                */
                $table->json('info')->nullable();

                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batches');
    }
}
