<?php

namespace Tests\Concerns;

use App\Events\TraceableModel\TraceableModelCreated;
use App\Events\TraceableModel\TraceableModelDeleted;
use App\Events\TraceableModel\TraceableModelUpdated;

trait Traceable
{
    /**
     * @return void
     */
    public function withoutTraceableEvents()
    {
        \Event::fake([TraceableModelCreated::class, TraceableModelUpdated::class, TraceableModelDeleted::class]);
    }
}
