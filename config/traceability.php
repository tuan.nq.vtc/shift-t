<?php

return [
    'states' => require('traceability/states.php'),
    'regulators' => require('traceability/regulators.php'),
    'license_resources' => require('traceability/license_resources.php'),
];
