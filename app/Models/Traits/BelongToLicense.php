<?php

namespace App\Models\Traits;

use App\Models\License;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongToLicense
{
    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class, 'license_id', 'id');
    }
}
