<?php

namespace Tests\Feature;

use App\Models\{Disposal, Plant, Propagation, Room, RoomType, WasteBag};
use Illuminate\Http\Response;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

/**
 * Class DisposalTest
 * @package Tests\Feature
 */
class DisposalTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $endpoint = '/disposals';
    protected static string $modelName = Disposal::class;

    protected static string $propagationEndpoint = 'propagations';
    protected static string $plantEndpoint = 'plants';
    protected static string $wasteBagEndpoint = 'waste-bags';
    protected static string $disposalEndpoint = 'disposals';

    protected static string $propagationModel = Propagation::class;
    protected static string $plantModel = Plant::class;
    protected static string $wasteBagModel = WasteBag::class;
    protected static string $disposalModel = Disposal::class;

    protected static array $destroyItemRequiredFields = ['room_id', 'reason'];

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

    private function getEndpoint($path = '')
    {
        return "/disposals{$path}";
    }

    /** @test */
    public function guest_cannot_call_disposal_endpoints()
    {
        $this->actingAsGuest();
        $this->get($this->getEndpoint("/list/scheduled"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->get($this->getEndpoint("/list/processing"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->get($this->getEndpoint("/list/destroyed"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/destroy"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/unschedule"))->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_call_disposal_endpoints_without_license()
    {
        $this->actingWithoutLicense();
        $this->get($this->getEndpoint("/list/scheduled"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->get($this->getEndpoint("/list/processing"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->get($this->getEndpoint("/list/destroyed"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/destroy"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/unschedule"))->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_can_get_scheduled_for_destruction_list()
    {
        $this->get(static::$endpoint . '/list/scheduled')
            // Validate that a paginator is returned.
            ->seeJsonStructure(
                [
                    'data',
                    'current_page',
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total'
                ]
            );
    }

    /** @test */
    public function user_can_get_ready_for_destruction_list()
    {
        $this->get(static::$endpoint . '/list/processing')
            // Validate that a paginator is returned.
            ->seeJsonStructure(
                [
                    'data',
                    'current_page',
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total'
                ]
            );
    }

    /** @test */
    public function user_can_get_destroyed_list()
    {
        $this->get(static::$endpoint . '/list/destroyed')
            // Validate that a paginator is returned.
            ->seeJsonStructure(
                [
                    'data',
                    'current_page',
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total'
                ]
            );
    }

    /** @test */
    public function user_can_destroy_disposal()
    {
        $items = factory(static::$modelName, rand(1, 5))->create(['license_id' => $this->licenseId]);

        $this->post($this->getEndpoint('/destroy'), ['ids' => $items->pluck('id')])
            ->seeStatusCode(Response::HTTP_OK);
    }

    /** @test */
    public function guest_cannot_destroy_item_without_license()
    {
        $this->actingAsGuest();
        $this->actingWithoutLicense();
        $this->post(static::$propagationEndpoint . "/destroy")->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post(static::$plantEndpoint . "/destroy")->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post(static::$disposalEndpoint . "/destroy")->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_destroy_disposal_without_required_field()
    {
        $room = $this->mockDisposalRoom();
        $mockPropagationData = $this->mockDestroyPropagation($room);
        $mockPlantData = $this->mockDestroyPlant($room);
        foreach (static::$destroyItemRequiredFields as $requiredField) {
            $mockPropagationDataTmp = (new \ArrayObject($mockPropagationData))->getArrayCopy();
            unset($mockPropagationDataTmp[$requiredField]); // remove required field
            $mockPlantDataTmp = (new \ArrayObject($mockPlantData))->getArrayCopy();
            unset($mockPlantDataTmp[$requiredField]); // remove required field
            $this->post(static::$propagationEndpoint . "/destroy", $mockPropagationDataTmp)->seeStatusCode(
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
            $this->post(static::$plantEndpoint . "/destroy", $mockPlantDataTmp)->seeStatusCode(
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /** @test */
    public function user_destroy_propagation_plant()
    {
        $room = $this->mockDisposalRoom();
        $mockPropagationData = $this->mockDestroyPropagation($room);
        $mockPlantData = $this->mockDestroyPlant($room);
        foreach (static::$destroyItemRequiredFields as $requiredField) {
            $this->post(static::$propagationEndpoint . "/destroy", $mockPropagationData)->seeStatusCode(
                Response::HTTP_OK
            );
            $this->post(static::$plantEndpoint . "/destroy", $mockPlantData)->seeStatusCode(
                Response::HTTP_OK
            );
        }
    }

    /**
     * @return Room|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    private function mockDisposalRoom()
    {
        $roomType = RoomType::query()
            ->where('category', RoomType::CATEGORY_DISPOSAL)
            ->get()->first();
        return factory(Room::class)->create(
            [
                'name' => $this->faker->text(20),
                'status' => Room::STATUS_ENABLED,
                "room_type_id" => $roomType->id,
                'license_id' => $this->licenseId,
            ]
        );
    }

    /**
     * @param Room $room
     * @return array
     */
    private function mockDestroyPropagation(Room $room)
    {
        $propagation = factory(Propagation::class)->create(
            [
                'license_id' => $this->licenseId,
            ]
        );
        return [
            "propagation" => [
                [
                    "id" => $propagation->id,
                    "number" => $propagation->quantity - 1,
                ]
            ],
            "reason" => "waste",
            "room_id" => $room->id,
            "wasted_at" => "2020-02-01 00:00:00",
            "comment" => "old propagation"
        ];
    }

    /**
     * @param Room $room
     * @return array
     */
    private function mockDestroyPlant(Room $room)
    {
        $plant = factory(Plant::class)->create(
            [
                'license_id' => $this->licenseId,
            ]
        );
        return [
            "plant" => [
                [
                    "id" => $plant->id,
                ]
            ],
            "reason" => "waste",
            "room_id" => $room->id,
            "wasted_at" => "2020-02-01 00:00:00",
            "comment" => "old propagation"
        ];
    }
}
