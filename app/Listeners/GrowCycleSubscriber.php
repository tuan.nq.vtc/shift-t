<?php

namespace App\Listeners;

use App\Events\GrowCycle\GrowCycleSaved;
use App\Services\GrowCycleService;
use Illuminate\Events\Dispatcher;

class GrowCycleSubscriber
{
    /**
     * @var GrowCycleService
     */
    private GrowCycleService $growCycleService;

    /**
     * GrowCycleService constructor.
     *
     * @param GrowCycleService $growCycleService
     */
    public function __construct(GrowCycleService $growCycleService)
    {
        $this->growCycleService = $growCycleService;
    }

    /**
     * @param GrowCycleSaved $event
     */
    public function onGrowCycleSaved(GrowCycleSaved $event)
    {
        $growCycle = $event->growCycle;

        $growCycle->plants()->update(['est_harvested_at' => $growCycle->est_harvested_at]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            GrowCycleSaved::class,
            'App\Listeners\GrowCycleSubscriber@onGrowCycleSaved'
        );
    }
}
