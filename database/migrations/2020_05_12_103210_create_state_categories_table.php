<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'state_categories',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('regulator')->index();
                $table->string('type')->index();
                $table->string('state_code')->index();
                $table->string('parent_id')->nullable()->index();
                $table->string('name')->index();
                $table->string('code')->index();
                $table->boolean('is_strain_required')->default(false);
                $table->boolean('is_serving_required')->default(false);
                $table->boolean('is_weight_per_unit_required')->default(false);
                $table->boolean('is_volume_per_unit_required')->default(false);
                $table->string('uom', 15)->nullable();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('state_categories');
    }
}
