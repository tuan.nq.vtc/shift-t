<?php

namespace App\Models;

use App\Models\Traits\{ApplyAdditive,
    BelongToStrain,
    Boots\RoomStatistic,
    HasInternalId,
    Scope\DatetimeFilterable,
    Scope\LicenseFilterable,
    Scope\NameFilterable,
    Scope\SourceTypeFilterable,
    StoreInRoom,
    StoreInSubroom,
};
use Illuminate\Database\Eloquent\{Builder,
    Relations\BelongsTo,
    Relations\HasMany,
    Relations\HasOne,
    Relations\MorphMany,
    Relations\MorphOne,
    SoftDeletes
};

/**
 * Class Propagation
 *
 * @property Batch batch
 * @property Inventory[] inventories
 * @property Disposal disposals
 * @property String source_type
 * @property Integer quantity
 * @property mixed qty_allocated
 * @package App\Models
 */
class Propagation extends TraceableModel
{
    use ApplyAdditive;
    use BelongToStrain;
    use DatetimeFilterable;
    use LicenseFilterable;
    use NameFilterable;
    use SoftDeletes;
    use SourceTypeFilterable;
    use StoreInRoom;
    use StoreInSubroom;
    use RoomStatistic;
    use HasInternalId;

    protected $appends = [
        'age',
        'sync_code',
        'qty_available',
    ];

    protected $casts = [
        'destroyed' => 'boolean',
        'quantity' => 'integer',
        'qty_allocated' => 'integer',
    ];

    protected $dates = ['date', 'synced_at'];

    protected $fillable = [
        'license_id',
        'name',
        'mother_code',
        'strain_id',
        'room_id',
        'subroom_id',
        'source_type',
        'quantity',
        'qty_allocated',
        'date',
        'cut_by_id',
        'plugged_by_id',
        'destroyed',
        'sync_status',
        'synced_at',
    ];

    public function batch(): MorphOne
    {
        return $this->morphOne(Batch::class, 'source');
    }

    public function motherPlant(): BelongsTo
    {
        return $this->belongsTo(Plant::class, 'mother_code', 'sync_code');
    }

    public function plantGroup(): HasOne
    {
        return $this->hasOne(PlantGroup::class);
    }

    public function additiveLogs(): HasMany
    {
        return $this->hasMany(AdditiveLog::class);
    }

    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class);
    }

    public function disposals(): MorphMany
    {
        return $this->morphMany(Disposal::class, 'wastable');
    }

    public function inventories(): MorphMany
    {
        return $this->morphMany(Inventory::class, 'source');
    }

    public function cutBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'cut_by_id', 'id');
    }

    public function pluggedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'plugged_by_id', 'id');
    }

    public function manifestItems(): MorphMany
    {
        return $this->morphMany(ManifestItem::class, 'resource');
    }

    public function getAgeAttribute(): string
    {
        return $this->date ? $this->date->longAbsoluteDiffForHumans() : '';
    }

    public function getQtyAvailableAttribute(): int
    {
        return $this->quantity - $this->qty_allocated;
    }

    public function getSyncCodeAttribute(): ?string
    {
        return optional($this->batch)->sync_code;
    }

    public function scopeAvailable(Builder $query, $value)
    {
        return (bool)($value) ? $query->where('quantity', '>', 0) : $query;
    }

    public function scopeDate(Builder $query, $firstValue, $secondValue = null): Builder
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'date');
    }

    public function scopeCutById($query, ...$value): Builder
    {
        return $query->whereIn('cut_by_id', $value);
    }

    public function scopePluggedBy($query, ...$value): Builder
    {
        return $query->whereIn('plugged_by_id', $value);
    }

    public function scopeOrderByPluggedBy(Builder $query, $direction): Builder
    {
        $userTable = $this->pluggedBy()->getRelated()->getTable();

        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($userTable, $this->getTable() . '.plugged_by_id', '=', $userTable . '.id')
            ->orderBy($userTable . '.first_name', $direction);
    }

    public function scopeDestroyed($query, bool $value): Builder
    {
        return $query->where('destroyed', $value);
    }

    public function scopeOrderByQuantity(Builder $query, string $direction = 'asc'): Builder
    {
        return $query->orderBy('quantity', $direction);
    }

    public function scopeOrderByRoomId(Builder $query, string $direction = 'asc'): Builder
    {
        return $query->orderBy('subroom_id', $direction);
    }

    public function scopeOrderByAge(Builder $query, string $direction = 'asc'): Builder
    {
        return $query->orderBy('date', $direction);
    }

    public function scopeOrderByCutBy(Builder $query, $direction): Builder
    {
        $userTable = $this->cutBy()->getRelated()->getTable();

        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($userTable, $this->getTable() . '.cut_by_id', '=', $userTable . '.id')
            ->orderBy($userTable . '.first_name', $direction);
    }

    /**
     * Scope a query to only include records have been synced.
     *
     * @param Builder $query
     * @param bool $isSynced
     *
     * @return Builder
     */
    public function scopeIsSynced(Builder $query, bool $isSynced): Builder
    {
        return $query->whereHas(
            'batch',
            fn(Builder $subQuery) => $isSynced ?
                $subQuery->where('sync_status', TraceableModel::SYNC_STATUS_SYNCED) :
                $subQuery->where('sync_status', '!=', TraceableModel::SYNC_STATUS_SYNCED)
        );
    }

    public function scopeOrderByIsSynced(Builder $query, $direction = 'asc'): Builder
    {
        $batchTable = $this->batch()->getRelated()->getTable();
        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($batchTable, $this->getTable() . '.id', '=', $batchTable . '.source_id')
            ->orderBy($batchTable . '.status', $direction);
    }

    public function scopeOrderBySyncCode(Builder $query, $direction = 'asc'): Builder
    {
        $batchTable = $this->batch()->getRelated()->getTable();
        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($batchTable, $this->getTable() . '.id', '=', $batchTable . '.source_id')
            ->orderBy($batchTable . '.sync_code', $direction);
    }

    /**
     * Search by:
     *  - mother code
     *  - name
     *  - propagation batch sync code
     *  - strain name
     *  - source type name
     *
     * @param Builder $query
     * @param string $search
     *
     * @return Builder
     */
    public function scopeSearch(Builder $query, string $search): Builder
    {
        return $query->where(
            fn(Builder $subQuery) => $subQuery->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('mother_code', 'LIKE', '%' . $search . '%')
                ->orWhereHas(
                    'batch',
                    fn(Builder $batchQuery) => $batchQuery->where('sync_code', 'LIKE', '%' . $search . '%')
                )
                ->orWhere('source_type', 'LIKE', '%' . $search . '%')
                ->orWhereHas(
                    'strain',
                    fn(Builder $strainQuery) => $strainQuery->where('name', 'LIKE', '%' . $search . '%')
                )
        );
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function increaseQty(int $quantity): self
    {
        $this->quantity += $quantity;
        return $this;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function decreaseQty(int $quantity): self
    {
        $this->quantity -= $quantity;
        return $this;
    }

    public function updateBatch(): Batch
    {
        return tap($this->batch)->updateWithoutEvents(['sync_code' => $this->sync_code]);
    }

    public function getMappings(): ?array
    {
        // TODO: Implement getMappings() method.
    }

    /**
     * @return $this
     */
    public function calculateAllocatedQuantity(): self
    {
        $this->setAttribute('qty_allocated', $this->manifestItems()
            ->where('status', ManifestItem::STATUS_PENDING)->sum('quantity'));
        return $this;
    }
}
