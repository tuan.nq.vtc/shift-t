<?php

namespace App\Integrations\FileGenerators\Resolvers;

use App\Integrations\FileGenerators\Contracts\DataProviderInterface;
use App\Integrations\FileGenerators\DataProviders\CcrsDataProvider;
use App\Integrations\FileGenerators\Exceptions\FileGeneratorException;
use App\Models\License;
use App\Models\State;

class DataProviderResolver
{
    public static function resolve(License $license, string $resourceClassName): DataProviderInterface
    {
        $dataProviderClassName = self::resolveClassnameByState($license->state);
        return new $dataProviderClassName($license, $resourceClassName);
    }

    public static function resolveClassnameByState(State $state): string
    {
        switch ($state->regulator) {
            case State::REGULATOR_CCRS:
                return CcrsDataProvider::class;
            default:
                throw new FileGeneratorException("Can not resolve DataProvider for state [{$state->code}]");
        }
    }

}
