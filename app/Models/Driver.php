<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{DatetimeFilterable, LicenseFilterable, NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Driver
 * @package App\Models
 */
class Driver extends BaseModel
{
    use SoftDeletes;
    use LicenseFilterable;
    use NameFilterable;
    use StatusFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $fillable = [
        'license_id',
        'name',
        'birthday',
        'hired_at',
        'license',
        'expired_at',
        'status',
    ];

    /**
     * @param Builder $query
     * @param $firstValue
     * @param null $secondValue
     * @return Builder
     */
    public function scopeBirthday(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'birthday');
    }

    /**
     * @param Builder $query
     * @param $firstValue
     * @param null $secondValue
     * @return Builder|mixed
     */
    public function scopeHiredAt(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'hired_at');
    }

    /**
     * @param Builder $query
     * @param $firstValue
     * @param null $secondValue
     * @return Builder|mixed
     */
    public function scopeExpiredAt(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'expired_at');
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%'.$value.'%');
    }
}
