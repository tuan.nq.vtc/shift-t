<?php


namespace App\Services;

use App\Models\{Notification, UserNotification};
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserNotificationService
 *
 * @package App\Services
 */
class UserNotificationService extends BaseService
{
    protected array $relations = [
        'notification',
        'notification.notifiable',
        'user'
    ];

    public function __construct(UserNotification $userNotification)
    {
        parent::__construct($userNotification);
    }

    /**
     * @param string $userId
     * @param string $licenseId
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function getUserNotifications(string $userId, string $licenseId)
    {
        return $this->model->where('created_at', '>', Carbon::now()->subDays(Notification::MAXIMUM_NUMBER_OF_DAY))
            ->where('user_id', $userId)
            ->where(
                fn(Builder $query) => $query
                    ->where('license_id', $licenseId)
                    ->orWhereNull('license_id')
            )
            ->with($this->relations)
            ->get();
    }

    public function markAsRead(string $userId, array $userNotificationIds): bool
    {
        return $this->newQuery(true, false)
            ->where('user_id', $userId)->whereIn('id', $userNotificationIds)
            ->update(['is_read' => true]);
    }

}
