<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait StatusFilterable
{
    /**
     * @param $query
     * @param ...$status
     * @return mixed
     */
    public function scopeStatus($query, ...$status)
    {
        return $query->whereIn('status', $status);
    }
}
