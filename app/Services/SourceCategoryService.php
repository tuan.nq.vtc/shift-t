<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\SourceCategory;
use App\Models\Taxonomy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\{Arr, Collection, Facades\DB};
use Throwable;

/**
 * @package App\Services
 */
class SourceCategoryService extends BaseService
{
    public array $relations = ['children'];

    public array $counts = ['sourceProducts'];

    public function __construct(SourceCategory $sourceCategory)
    {
        parent::__construct($sourceCategory);
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function getAvailableItems(Criteria $criteria): Collection
    {
        $query = $this->newQuery(false)->scopes($this->loadScopes($criteria->getFilters()));

        return $this->applyOrderBy($query, $criteria->getSorts())->withCount($this->counts)->get();
    }

    /**
     * @param array $data
     * @return Model
     * @throws Throwable
     */
    public function create(array $data): Model
    {
        try {
            DB::beginTransaction();

            /** @var SourceCategory $sourceCategory */
            $sourceCategory = parent::create($data);
            if (Arr::has($data, ['state_category_id', 'license_id'])) {
                $this->assignStateCategory($sourceCategory, $data['license_id'], $data['state_category_id']);
            }

            DB::commit();
            return $sourceCategory;
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $id
     * @param array $data
     * @return Model
     * @throws GeneralException
     * @throws Throwable
     */
    public function update($id, array $data): Model
    {
        if (
            !empty($data['parent_id']) &&
            $this->newQuery()->where('id', $data['parent_id'])
                ->where('path', 'like', '%' . $id . '%')
                ->count()
        ) {
            throw new GeneralException(__('exceptions.source_category.update_failed'));
        }

        try {
            DB::beginTransaction();

            /** @var SourceCategory $sourceCategory */
            $sourceCategory = parent::update($id, $data);
            if (Arr::has($data, ['state_category_id', 'license_id'])) {
                $this->assignStateCategory($sourceCategory, $data['license_id'], $data['state_category_id']);
            }

            DB::commit();
            return $sourceCategory;
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param SourceCategory $sourceCategory
     * @param SourceCategory|null $assignToCategory
     * @throws GeneralException
     */
    public function destroy(SourceCategory $sourceCategory, SourceCategory $assignToCategory = null): void
    {
        if ($sourceCategory->children()->count()) {
            throw new GeneralException(__('exceptions.source_category.destroy_failed'));
        }

        if ($sourceCategory->sourceProducts()->count() && !$assignToCategory) {
            throw new GeneralException(__('exceptions.source_category.must_be_reassign_product'));
        }

        if (
            $assignToCategory
            && Taxonomy::query()
                ->where('license_id', $this->licenseId)
                ->whereIn('category_id', [$sourceCategory->id, optional($assignToCategory)->id])
                ->pluck('state_category_id', 'state_category_id')
                ->count() === 2
        ) {
            throw new GeneralException(__('exceptions.source_category.must_be_same_state_category'));
        }

        try {
            DB::beginTransaction();
            if ($sourceCategory->sourceProducts()->count()) {
                $sourceCategory->sourceProducts()->update([
                    'source_category_id' => $assignToCategory->id
                ]);
            }

            Taxonomy::query()->where('license_id', $this->licenseId)->where('category_id', $sourceCategory->id)->delete();

            $sourceCategory->delete();
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.source_category.destroy_failed'));
        }
    }

    /**
     * @param SourceCategory $category
     * @param string $licenseId
     * @param string $stateCategoryId
     */
    private function assignStateCategory(SourceCategory $category, string $licenseId, string $stateCategoryId)
    {
        $category->taxonomy()->updateOrCreate(
            ['license_id' => $licenseId],
            ['state_category_id' => $stateCategoryId]
        );
    }
}
