<?php

namespace App\Models\Traits;

use App\Models\Strain;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongToStrain
{
    public function strain(): BelongsTo
    {
        return $this->belongsTo(Strain::class);
    }

    public function scopeStrainIds(Builder $query, ...$strainIds): Builder
    {
        return $query->whereIn($this->getTable().'.strain_id', $strainIds);
    }

    public function scopeStrainId(Builder $query, ...$strainIds): Builder
    {
        return $query->whereIn($this->getTable().'.strain_id', $strainIds);
    }

    public function scopeStrain(Builder $query, ...$strainId): Builder
    {
        return $this->scopeStrainId($query, ...$strainId);
    }

    public function scopeOrderByStrain(Builder $query, $direction): Builder
    {
        $strainStable = $this->strain()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($strainStable, $this->getTable().'.strain_id', '=', $strainStable.'.id')
            ->orderBy($strainStable.'.name', $direction);
    }
}
