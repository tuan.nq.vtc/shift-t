<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models\{Harvest, TraceableModel};
use Illuminate\Http\Request;

/**
 * Class HarvestResource
 * @package App\Synchronizations\Http\Resources\Metrc
 *
 * @property Harvest|TraceableModel $traceableModel
 */
class HarvestResource extends BaseMetrcResource
{
    protected string $path = '/plants/v1';

    protected function create(): self
    {
        $plants = [];
        /** @var Harvest $detailHarvest */
        $detailHarvest = $this->traceableModel;
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/harvestplants';
        foreach ($detailHarvest->plants as $plant) {
            $plants[] = [
                "Plant" => $plant->sync_code,
                "Weight" => $plant->flower_dry_weight,
                "UnitOfWeight" => $detailHarvest->uom,
                "DryingLocation" => $detailHarvest->room->name,
                "HarvestName" => $detailHarvest->name." ".$detailHarvest->strain->sync_code,
                "ActualDate" => $plant->harvested_at
            ];
        }

        $this->requestBody = $plants;

        return $this;
    }

    // TODO
    protected function update(): self
    {
        return $this;
    }

    // TODO
    protected function delete(): self
    {
        return $this;
    }
}
