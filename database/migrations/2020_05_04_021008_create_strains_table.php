<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strains', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->string('name')->index();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->unsignedTinyInteger('status')->default(1)->index();
            $table->json('info')->nullable();
            $table->text('custom_id')->nullable();
            $table->uuid('created_by')->nullable()->comment('Store uuid of creator');
            $table->uuid('updated_by')->nullable()->comment('Store uuid of modifier');

            // S2S Trace sync[Create/Update/Delete]
            $table->string('sync_code')->nullable()->index();
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strains');
    }
}
