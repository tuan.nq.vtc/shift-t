<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Models\Harvest;
use App\Models\HarvestGroup;
use App\Synchronizations\{Http\Resources\BaseResource, Constants\LeafConstant};
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class HarvestResource extends BaseResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_BATCH;

    /**
     * @return $this
     * @throws Exception
     */
    protected function create(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = LeafConstant::ENDPOINT_HARVEST_PLANTS;
        /** @var Harvest $harvest */
        $harvest = $this->traceableModel;

        $errorFormat = 'Unable to sync harvest creation %s';
        $context = ['harvest' => $harvest];
        if (!optional($harvest->room)->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }

        $this->requestBody = [
            "external_id" => $harvest->id,
            "harvested_at" => Carbon::parse($harvest->start_at)->format('d/m/Y'),
            "qty_harvest" => $harvest->flower_wet_weight + $harvest->material_wet_weight,
            "flower_wet_weight" => $harvest->flower_wet_weight,
            "other_wet_weight" => $harvest->material_wet_weight,
            "uom" => 'gm',
            "global_area_id" => $harvest->room->sync_code,
            "global_harvest_batch_id" => "",
            "global_plant_ids" => $harvest->plants->reduce(function ($acc, $item) {
                $acc[] = ['global_plant_id' => $item->sync_code];
                return $acc;
            }, [])
        ];

        return $this;
    }

    /**
     * @return $this
     */
    protected function fetchAll(): self
    {
        $this->requestUrl = static::$endpoint;
        $this->httpMethod = Request::METHOD_GET;
        if (!empty($this->conditions['page'])) {
            $this->requestQuery['page'] = $this->conditions['page'];
        }
        $this->requestQuery['f_type'] = LeafConstant::BATCH_TYPE_HARVEST;
        return $this;
    }

    /**
     * @return $this
     */
    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = static::$endpoint;
        $this->requestQuery['f_type'] = LeafConstant::BATCH_TYPE_HARVEST;

        return $this;
    }

    /**
     * @return self
     * @throws Exception
     */
    protected function confirmDry(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = LeafConstant::ENDPOINT_CURE_LOT;
        /** @var Harvest $harvest */
        $harvest = $this->traceableModel;

        $errorFormat = 'Unable to sync harvest confirm dry %s';
        $context = ['harvest' => $harvest];
        if (!optional($harvest->batch)->sync_code) {
            $error = sprintf($errorFormat, 'without batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!optional($harvest->room)->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }

        if ($harvest->group->type === HarvestGroup::TYPE_EXTRACTION) {
            $flowerWeight = $harvest->flower_wet_weight;
            $materialWeight = $harvest->material_wet_weight;
        } else {
            $flowerWeight = $harvest->flower_dry_weight;
            $materialWeight = $harvest->material_dry_weight;
        }
        $this->requestBody = [
            "global_batch_id" => $harvest->batch->sync_code,
            "flower_dry_weight" => $flowerWeight,
            "other_dry_weight" => $materialWeight,
            "global_flower_area_id" => $harvest->room->sync_code,
            "global_other_area_id" => $harvest->room->sync_code,
        ];

        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function finalize(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = LeafConstant::ENDPOINT_FINISH_LOT;
        /** @var Harvest $harvest */
        $harvest = $this->traceableModel;

        $errorFormat = 'Unable to sync harvest finalize %s';
        $context = ['harvest' => $harvest];
        if (!optional($harvest->batch)->sync_code) {
            $error = sprintf($errorFormat, 'without batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if ($harvest->group->type === HarvestGroup::TYPE_EXTRACTION) {
            $flowerWeight = $harvest->flower_wet_weight;
            $materialWeight = $harvest->material_wet_weight;
        } else {
            $flowerWeight = $harvest->flower_dry_weight;
            $materialWeight = $harvest->material_dry_weight;
        }

        $newLotTypes = [];
        if ($flowerWeight > 0) {
            if (!optional($harvest->flowerRoom)->sync_code) {
                $error = sprintf($errorFormat, 'without flower room LEAF GLOBAL_ID [sync_code]');
                $this->logger->error($error, $context);
                throw new Exception($error);
            }
            if (!optional($harvest->flowerInventoryType)->sync_code) {
                $error = sprintf($errorFormat, 'without flower inventory type LEAF GLOBAL_ID [sync_code]');
                $this->logger->error($error, $context);
                throw new Exception($error);
            }
            $newLotTypes[] = [
                'material_type' => 'flower',
                'global_inventory_type_id' => $harvest->flowerInventoryType->sync_code,
                'global_area_id' => $harvest->flowerRoom->sync_code,
                'qty' => $flowerWeight,
                'medically_compliant' => '0'
            ];
        }
        if ($materialWeight > 0) {
            if (!optional($harvest->materialRoom)->sync_code) {
                $error = sprintf($errorFormat, 'without material room LEAF GLOBAL_ID [sync_code]');
                $this->logger->error($error, $context);
                throw new Exception($error);
            }
            if (!optional($harvest->materialInventoryType)->sync_code) {
                $error = sprintf($errorFormat, 'without material inventory type LEAF GLOBAL_ID [sync_code]');
                $this->logger->error($error, $context);
                throw new Exception($error);
            }
            $newLotTypes[] = [
                'material_type' => 'other_material',
                'global_inventory_type_id' => $harvest->materialInventoryType->sync_code,
                'global_area_id' => $harvest->materialRoom->sync_code,
                'qty' => $materialWeight,
                'medically_compliant' => '0'
            ];
        }
        if (empty($newLotTypes)) {
            $error = 'There are no new lots';
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        $this->requestBody = [
            'global_batch_id' => $harvest->batch->sync_code,
            'new_lot_types' => $newLotTypes,
        ];

        return $this;
    }
}
