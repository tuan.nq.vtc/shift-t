<?php

namespace App\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * Class Sales
 * @package App\Facades
 *
 * @method static setState(string $id)
 * @method static fetchProduct(string $id)
 * @method static changeProductQuantity(string $id, int $quantity)
 * @method static Collection getProductsByIds(array $productIds = [])
 * @method static Collection getProducts(array $filters = [])
 *
 * @see \App\Services\SalesService
 */
class Sales extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'sales_service';
    }
}
