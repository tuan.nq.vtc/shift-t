<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Licensee;
use App\Models\State;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Licensee::class,
    function (Faker $faker) {
        return [
            'name' => $faker->name,
            'regulator' => $faker->randomElement(
                [
                    State::REGULATOR_LEAF,
                    State::REGULATOR_METRC,
                ]
            ),
            'address1' => $faker->address,
            'address2' => $faker->address,
            'city' => $faker->city,
            'phone' => $faker->phoneNumber,
            'type' => Licensee::TYPE_LAB,
            'code' => 'MME.LAB'. $faker->unique()->randomNumber(4),
            'sync_code' =>'MME.LAB'. $faker->unique()->randomNumber(4),
            'state_code' => $faker->state,
        ];
    }
);
