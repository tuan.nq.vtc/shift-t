<?php

namespace App\Cqrs\Base;

use Psr\Log\LoggerInterface;

abstract class Handleable
{
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var array
     */
    protected $errorBag = [];

    /**
     * @return array
     */
    public function getErrorBag(): array
    {
        return array_unique($this->errorBag);
    }

    /**
     * @return bool
     */
    public function isRecordedErrors(): bool
    {
        return !empty($this->errorBag);
    }

    /**
     * @param Executable $executable
     * @param  $errorMessage
     * @param array $context
     */
    protected function log(Executable $executable, $errorMessage = null, array $context = [])
    {
        if ($this->logger instanceof LoggerInterface !== true) {
            return;
        }

        $this->logger->info(json_encode($executable->toArray()));

        if (!$errorMessage) {
            return;
        }

        if (is_array($errorMessage)) {
            $this->errorBag = array_merge($this->errorBag, $errorMessage);
            $errorMessage = json_encode($errorMessage);
        } else {
            $this->errorBag[] = $errorMessage;
        }

        $this->logger->error($errorMessage, $context);
    }
}
