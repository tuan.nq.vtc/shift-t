<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use Illuminate\Support\Carbon;
use App\Models\{Inventory, TraceableModel};
use Exception;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class InventoryTypeResource
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property Inventory|TraceableModel $traceableModel
 */
class InventoryResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_INVENTORY;

    protected function fetchAll(): self
    {
        parent::fetchAll();
        $errorFormat = 'Unable to fetch all inventory without %s';
        if (empty($this->conditions['from_date']) || empty($this->conditions['to_date'])
            || !($fromDate = Carbon::createFromDate($this->conditions['from_date']))
            || !($toDate = Carbon::createFromDate($this->conditions['to_date']))) {
            $error = sprintf($errorFormat, 'date range LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error);
            throw new Exception($error);
        }

        // f_date1={mm/dd/yyyy}&f_date2={mm/dd/yyyy}
        $this->requestQuery['f_date1'] = $fromDate->format('m/d/Y');
        $this->requestQuery['f_date2'] = $toDate->format('m/d/Y');

        return $this;
    }

    protected function create(): self
    {
        /** @var Inventory $inventory */
        $inventory = $this->traceableModel;
        $errorFormat = 'Unable to sync inventory create %s';
        $context = ['inventory' => $inventory];

        if (!$inventory->source || !optional($inventory->source->batch)->sync_code) {
            $error = sprintf($errorFormat, 'without batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!optional($inventory->room)->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!optional($inventory->type)->sync_code) {
            $error = sprintf($errorFormat, 'without inventory type LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        return parent::create()
            ->setBody(
                [
                    'inventory' => [
                        [
                            'external_id' => $this->traceableModel->id,
                            'qty' => $this->traceableModel->qty_on_hand,
                            'medically_compliant' => 0,
                            'global_batch_id' => $inventory->source->batch->sync_code,
                            'global_area_id' => $this->traceableModel->room->sync_code,
                            'global_inventory_type_id' => $this->traceableModel->type->sync_code,
                        ]
                    ],
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        /** @var Inventory $inventory */
        $inventory = $this->traceableModel;
        $errorFormat = 'Unable to sync inventory update %s';
        $context = ['inventory' => $inventory];

        if (!$inventory->sync_code) {
            $error = sprintf($errorFormat, 'without LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!$inventory->source || !optional($inventory->source->batch)->sync_code) {
            $error = sprintf($errorFormat, 'without batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!optional($inventory->room)->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!optional($inventory->type)->sync_code) {
            $error = sprintf($errorFormat, 'without inventory type LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }

        return parent::update()
            ->setBody(
                [
                    'inventory' => [
                        'external_id' => $this->traceableModel->id,
                        'qty' => $this->traceableModel->qty_on_hand,
                        'medically_compliant' => 0,
                        'global_id' => $this->traceableModel->sync_code,
                        'global_batch_id' => $inventory->source->batch->sync_code,
                        'global_area_id' => $this->traceableModel->room->sync_code,
                        'global_inventory_type_id' => $this->traceableModel->type->sync_code,
                    ],
                ]
            );
    }

    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync inventory deletion without LEAF global_id [sync_code]')
        );

        return parent::delete();
    }

    protected function splitLot(): self
    {
        /** @var Inventory $inventory */
        $inventory = $this->traceableModel;
        $errorFormat = 'Unable to sync inventory split lot %s';
        $context = ['inventory' => $inventory];

        if (!optional($this->traceableModel->parent)->sync_code) {
            $error = sprintf($errorFormat, 'without source inventory LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!optional($inventory->room)->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = '/split_inventory';
        $this->requestBody = [
            'external_id' => $inventory->id,
            'global_inventory_id' => $inventory->parent->sync_code,
            'global_area_id' => $inventory->room->sync_code,
            'qty' => $inventory->qty_on_hand,
            'uom' => '',
            'net_weight' => '', // required param on API
            'cost' => '', // required param on API
        ];

        return $this;
    }
}
