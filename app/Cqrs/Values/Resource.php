<?php

namespace App\Cqrs\Values;

class Resource
{
    /**
     * @var
     */
    private $id;

    /**
     * @var
     */
    private $clientId;

    /**
     * @var
     */
    private $filePath;

    /**
     * @var
     */
    private $label;

    /**
     * @var
     */
    private $owner;

    /**
     * @var
     */
    private $createdAt;

    /**
     * @var
     */
    private $updatedAt;

    /**
     * @param string $str
     * @return static
     */
    public static function fromString(string $str): self
    {
        $self = new self();

        $obj = json_decode($str);
        if (!is_object($obj) || !property_exists($obj, 'data')) {
            return $self;
        }

        $objData = $obj->data;
        if (!is_object($objData) || !property_exists($objData, 'id')) {
            return $self;
        }

        $self->id = $objData->id;

        if (!property_exists($objData, 'attributes')) {
            return $self;
        }

        $attrs = $objData->attributes;
        if (!is_object($attrs)) {
            return $self;
        }

        if (property_exists($attrs, 'client_id')) {
            $self->clientId = $attrs->client_id;
        }

        if (property_exists($attrs, 'file_path')) {
            $self->filePath = $attrs->file_path;
        }


        if (property_exists($attrs, 'label')) {
            $self->label = $attrs->label;
        }

        if (property_exists($attrs, 'owner')) {
            $self->owner = $attrs->owner;
        }

        if (property_exists($attrs, 'created_at')) {
            $self->createdAt = $attrs->created_at;
        }

        if (property_exists($attrs, 'updated_at')) {
            $self->updatedAt = $attrs->updated_at;
        }

        return $self;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
