<?php

namespace App\Events\Harvest;

use App\Events\Event;
use App\Models\HarvestWetWeight;

class HarvestWetWeightSaved extends Event
{
    /**
     * @var HarvestWetWeight
     */
    public HarvestWetWeight $harvestWetWeight;

    /**
     * @param HarvestWetWeight $harvestWetWeight
     */
    public function __construct(HarvestWetWeight $harvestWetWeight)
    {
        $this->harvestWetWeight = $harvestWetWeight;
    }
}
