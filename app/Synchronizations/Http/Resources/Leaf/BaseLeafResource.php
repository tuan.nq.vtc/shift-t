<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Models\TraceableModel;
use App\Synchronizations\Http\Resources\BaseResource;
use Illuminate\Http\Request;

/**
 * Class BaseLeafResource
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property TraceableModel $traceableModel
 */
class BaseLeafResource extends BaseResource
{
    protected static string $endpoint = '';

    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = static::$endpoint;

        return $this;
    }

    protected function fetchAll(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = static::$endpoint;
        if (!empty($this->conditions['page'])) {
            $this->requestQuery['page'] = $this->conditions['page'];
        }
        return $this;
    }

    protected function create(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = static::$endpoint;

        return $this;
    }

    protected function update(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = sprintf("%s/update", static::$endpoint);

        return $this;
    }

    protected function delete(): self
    {
        $this->httpMethod = Request::METHOD_DELETE;
        $this->requestUrl = sprintf("%s/%s", static::$endpoint, $this->traceableModel->sync_code);

        return $this;
    }
}
