<?php

namespace App\Services;

use App\Events\Disposal\DisposalScheduled;
use App\Events\Inventory\InventoryLotIsSplit;
use App\Events\Inventory\SavingInventoryHistory;
use App\Exceptions\GeneralException;
use App\Facades\Sales;
use App\Http\Parameters\Criteria;
use App\Models\{Disposal,
    EndProduct,
    Harvest,
    Inventory,
    InventoryHistory,
    InventoryType,
    Plant,
    QASampleCoa,
    Room,
    SourceCategory,
    SourceProduct,
    Subroom,
    Taxonomy};
use App\Services\Traits\EndProductHandler;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\{Arr, Collection, Enumerable, Facades\DB, Facades\Event};
use Throwable;

/**
 * Class InventoryService
 * @package App\Services
 *
 * @method Inventory getModel()
 */
class InventoryService extends BaseService
{
    use EndProductHandler;

    protected array $relations = [
        'type:id,name,state_category_id,uom',
        'type.stateCategory:id,name',
        'strain:id,name',
        'qaSamples',
        'children',
        'parent:id,sync_code,state_category_id,type_id',
        'parent.type',
        'parent.stateCategory',
        'conversions',
        'sublots',
        'finalQASample',
        'finalQASample.sourceInventory',
        'histories'
    ];

    public function __construct(Inventory $inventory)
    {
        parent::__construct($inventory);
    }

    public function listLotted(Criteria $criteria, string $type): LengthAwarePaginator
    {
        if (!array_key_exists('is_show_depleted', $criteria->getFilters())) {
            $criteria = $criteria->addFilter('is_hide_depleted', true);
        }

        $query = $this->newQuery(false)
            ->where('is_lotted', true)
            ->scopes($this->loadScopes($criteria->getFilters()));
        switch ($type) {
            case Inventory::TYPE_SOURCE:
                $query->where('product_type', SourceProduct::class);
                break;
            case Inventory::TYPE_END:
                $query->where('product_type', EndProduct::class);
                break;
            default:
                break;
        }
        $relations = [
            'stateCategory:id,name',
            'type:id,name,state_category_id',
            'type.stateCategory:id,name',
            'strain:id,name',
            'room:id,name',
            'subroom:id,name',
            'qaSamples',
            'license',
        ];
        $relations = [...$relations, $type === Inventory::TYPE_SOURCE ? 'product' : 'endProduct'];

        $paginator = $this->applyOrderBy($query, $criteria->getSorts())
            ->with($relations)
            ->withCount('qaSamples')
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());

        $mappedProducts = $this->getMappedProducts($type, $paginator->getCollection()->pluck('product_id')->unique()->toArray());
        $paginator->getCollection()->transform(fn(Inventory $inventory) => $this->listTransform($inventory, $mappedProducts));
        return $paginator;
    }

    /**
     * @param Inventory $inventory
     * @param Collection $mappedProducts
     * @return Inventory
     */
    private function listTransform(Inventory $inventory, Collection $mappedProducts)
    {
        $countCOAFile = 0;
        $qaSampleIds = $inventory->qaSamples->pluck('id')->toArray();
        if (!empty($qaSampleIds)) {
            $countCOAFile += QASampleCoa::query()->whereIn('qa_sample_id', $qaSampleIds)->count();
        }
        $inventory->coa_count = $countCOAFile;
        if ($mappedProduct = $mappedProducts->firstWhere('id', $inventory->product_id)) {
            $inventory->forceFill($mappedProduct->only(['net_weight', 'uom']));
        }

        $inventory->can_create_qa_sample = false;
        if (!empty($inventory->license->state->regulator)) {
            $whitelistCategoryNames = config('qasample.whitelist.' . $inventory->license->state->regulator) ?? [];
            if (in_array(optional($inventory->stateCategory)->name, $whitelistCategoryNames) && in_array(
                    $inventory->qa_status,
                    Inventory::CAN_CREATE_QA_SAMPLE
                )) {
                $inventory->can_create_qa_sample = true;
            }
        }

        return $inventory;
    }

    /**
     * @param string $type
     * @param array $productIds
     * @return Collection
     * @throws GeneralException
     */
    private function getMappedProducts(string $type, array $productIds): Collection
    {
        switch ($type) {
            case Inventory::TYPE_END:
                if (empty($productIds)) {
                    return collect();
                }
                try {
                    return Sales::getProductsByIds($productIds);
                } catch (Throwable $throwable) {
                    throw new GeneralException(__('exceptions.sales_service.fetch_products_failed'));
                }
            case Inventory::TYPE_SOURCE:
                return SourceProduct::query()->whereIn('id', $productIds)->get();
            default:
                return collect();
        }
    }

    /**
     * @param Criteria $criteria
     * @return LengthAwarePaginator
     */
    public function listUnlotted(Criteria $criteria): LengthAwarePaginator
    {
        $query = $this->newQuery(false)
            ->where('is_lotted', false)
            ->scopes($this->loadScopes($criteria->getFilters()));

        $paginator = $this->applyOrderBy($query, $criteria->getSorts())
            ->with(
                [
                    'type:id,name,state_category_id',
                    'type.stateCategory:id,name',
                    'strain:id,name',
                    'room:id,name',
                    'subroom:id,name',
                    'source'
                ]
            )
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage())
            ->loadMorph(
                'source',
                [
                    Harvest::class => ['group:id,name'],
                ]
            );
        $paginator->getCollection()->transform(function (Inventory $inventory) {
            $inventory->uom = Inventory::UOM_GRAMS;
            return $inventory;
        });
        return $paginator;
    }

    /**
     * @param InventoryType $item
     * @param Collection $plants
     * @param Subroom $subroom
     *
     * @return Inventory
     *
     * @throws Throwable
     */
    public function createFromPlants(InventoryType $item, Collection $plants, Subroom $subroom): Inventory
    {
        try {
            DB::beginTransaction();
            $plants->each(fn(Plant $plant) => $plant->update(['is_moved' => true]));
            /** @var Inventory $package */
            $package = $item->inventories()->make(
                [
                    'name' => '',
                    'qty_on_hand' => $plants->count(),
                ]
            );
            $package->type()->associate($item);
            $package->subroom()->associate($subroom);
            $package->save();
            DB::commit();

            return $package;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw  $throwable;
        }
    }

    /**
     * @param Inventory $inventory
     * @param $data
     * @return Inventory
     * @throws Throwable
     */
    public function adjust(Inventory $inventory, $data): Inventory
    {
        $newQtyOnHand = Arr::get($data, 'qty');
        throw_if(!is_numeric($newQtyOnHand) || $newQtyOnHand > $inventory->qty_on_hand,
            new GeneralException(__('exceptions.inventory.adjust_qty_on_hand_invalid')));
        throw_if($newQtyOnHand < ($inventory->qty_on_hold + $inventory->qty_allocated),
            new GeneralException(__('exceptions.inventory.quantity_greater_or_equal',
                ['min' => ($inventory->qty_on_hold + $inventory->qty_allocated)])));
        try {
            DB::beginTransaction();
            Event::dispatch(
                new SavingInventoryHistory(
                    $inventory,
                    InventoryHistory::ACTION_ADJUST,
                    ($inventory->qty_on_hand - $newQtyOnHand), true, null, $data),
            );
            $inventory->qty_on_hand = $newQtyOnHand;
            $inventory->save();
            DB::commit();
            return $inventory;
        } catch (Throwable $exception) {
            DB::rollBack();
            throw  $exception;
        }
    }

    /**
     * @param Inventory $unlottedInventory
     * @param array $data
     *
     * @return Collection
     *
     * @throws GeneralException
     * @throws Throwable
     */
    public function createLots(Inventory $unlottedInventory, array $data): Collection
    {
        if ($unlottedInventory->is_lotted) {
            throw new GeneralException(__('exceptions.inventory.is_lotted'));
        }

        return $this->saveLots($unlottedInventory, $data, false);
    }

    /**
     * @param Inventory $parentInventory
     * @param array $data
     * @return Collection
     * @throws GeneralException
     * @throws Throwable
     */
    public function createSublots(Inventory $parentInventory, array $data): Collection
    {
        if (!$parentInventory->is_lotted) {
            throw new GeneralException(__('exceptions.inventory.must_lotted'));
        }

        if (in_array($parentInventory->qa_status, [
            Inventory::QA_STATUS_AWAITING_RESULTS,
            Inventory::QA_STATUS_SAMPLED_RETEST,
            Inventory::QA_STATUS_SAMPLED_TEST,
        ])) {
            throw new GeneralException(__('exceptions.inventory.sublot.qa_status_invalid'));
        }

        return $this->saveLots($parentInventory, $data, true);
    }

    /**
     * @param Inventory $sourceInventory
     * @param array $data
     * @param false $isSublot
     * @return Collection
     * @throws GeneralException
     * @throws Throwable
     */
    public function saveLots(Inventory $sourceInventory, array $data, bool $isSublot = false): Collection
    {
        try {
            DB::beginTransaction();
            $numberOfLots = $weightPerLot = 0;
            if (!empty($data['number_of_lot'])) {
                $numberOfLots = (int)$data['number_of_lot'];
            }
            if (!empty($data['weight_per_lot'])) {
                $weightPerLot = round($data['weight_per_lot'], 4);
            }

            if (abs($weightPerLot * $numberOfLots) < PHP_FLOAT_EPSILON) {
                throw new GeneralException(__('exceptions.inventory.invalid_number_of_lots'));
            }
            if ($weightPerLot * ($numberOfLots - 1) >= $sourceInventory->qty_available) {
                throw new GeneralException(__('exceptions.inventory.quantity_insufficient'));
            }

            if ($isSublot) {
                $sublotProduct = $this->getValidProduct($data['sublot_product_id'], $data['product_type'], 'invalid_product_sublot');

                if (isset($data['parent_product_id'])) {
                    $this->getValidProduct($data['parent_product_id'], $data['product_type'], 'invalid_product_parent');
                    $parentProductId = $data['parent_product_id'];
                }

                $sublotRoomId = isset($data['sublot_room_id']) ? $data['sublot_room_id'] : $sourceInventory->room_id;
                $sublotSubRoomId = null;
                $productId = $data['sublot_product_id'];
            } else {
                $data['product_type'] = 'source';
                $productId = Arr::get($data, 'product_id');
                $sublotProduct = $this->getValidProduct($productId, $data['product_type'], 'invalid_product');
                $sublotRoomId = $data['room_id'] ?? null;
                $sublotSubRoomId = $data['subroom_id'] ?? null;
            }

            $productType = $this->getProductModelClass($data['product_type']);

            $sublotInventoryType = $this->findTypeBaseOnProduct($sublotProduct, $data['product_type']);
            $checkSublot = false;
            $isLotted = false; // use source inventory as lotted inventory
            /**
             * Sum of number_of_lot and weight_per_lot can be greater than inventory quantity
             * Eg:
             * - inventory quantity = 15.6 lbs
             * - weight_per_lot = 5 lbs
             * - number_of_lot = 4 (3 lots 5 lbs and 1 lot 0.6 lbs)
             */
            $remainingWeight = round($weightPerLot * $numberOfLots, 4) - $sourceInventory->qty_on_hand;
            if ($remainingWeight > 0) {
                $numberOfLots--;
                if ($isSublot) {
                    $checkSublot = true;
                }
                $isLotted = true;
            }
            $inventories = collect();
            $newInventoryData = array_merge(
                $sourceInventory->only(['license_id', 'strain_id']),
                [
                    'is_lotted' => true,
                    'room_id' => $sublotRoomId,
                    'subroom_id' => $sublotSubRoomId,
                    'product_type' => $productType,
                    'product_id' => $productId,
                    'type_id' => $sublotInventoryType->id,
                    'state_category_id' => optional($sublotInventoryType->stateCategory)->id,
                    'final_qa_sample_id' =>
                        $sourceInventory->hasFinalQASampleStatus() && $sourceInventory->final_qa_sample_id ? $sourceInventory->final_qa_sample_id : null,
                    'qa_status' => $sourceInventory->hasFinalQASampleStatus() ? $sourceInventory->qa_status : Inventory::QA_STATUS_NOT_SAMPLED,
                ]
            );

            for ($i = 0; $i < $numberOfLots; $i++) {
                /** @var Inventory $newInventory */
                $newInventory = $sourceInventory->children()->make(array_merge(
                    $newInventoryData,
                    [
                        'name' => "{$sourceInventory->name} lot {$i}",
                        'qty_on_hand' => $weightPerLot,
                    ]
                ));
                $newInventory->source()->associate($sourceInventory->source)->save();
                $inventories->push($newInventory);
                $sourceInventory->decreaseQty($weightPerLot);
            }
            if ($checkSublot || $isLotted) {
                /** @var Inventory $remainingInventory * */
                $remainingInventory = $sourceInventory->children()->make(array_merge(
                    $newInventoryData,
                    [
                        'name' => "{$sourceInventory->name} lot {($numberOfLots +1)}",
                        'qty_on_hand' => $sourceInventory->qty_on_hand,
                    ]
                ));

                $remainingInventory->source()->associate($sourceInventory->source)->save();
                $inventories->push($remainingInventory);
                $sourceInventory->decreaseQty($sourceInventory->qty_on_hand);
                if ($sourceInventory->type_id !== $sublotInventoryType->id) {
                    $sourceInventory->type_id = $sublotInventoryType->id;
                }
            } else if (isset($data['parent_room_id']) || isset($data['parent_product_id'])) {
                $sourceInventory->fill(
                    [
                        'is_lotted' => true,
                        'room_id' => isset($data['parent_room_id']) ? $data['parent_room_id'] : $sourceInventory->room_id,
                        'product_type' => $productType,
                        'state_category_id' => optional($sublotInventoryType->stateCategory)->id,
                    ]
                );
                if (isset($parentProductId)) {
                    $sourceInventory->product_id = $parentProductId;
                }

            }
            $sourceInventory->save();
            DB::commit();

            // dispatch inventory lot is split
            Event::dispatch(new InventoryLotIsSplit($sourceInventory, $inventories));
            return $inventories;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param string $id
     * @param string $type
     * @param string $exception
     * @return mixed
     * @throws GeneralException
     */
    private function getValidProduct(string $id, string $type, string $exception = 'invalid_product')
    {
        $product = null;
        switch ($type) {
            case Inventory::TYPE_SOURCE:
                if (!($product = SourceProduct::where('status', SourceProduct::STATUS_ENABLED)->find($id))) {
                    throw new GeneralException(__("exceptions.inventory.$exception"));
                }
                break;
            case Inventory::TYPE_END:
                try {
                    $product = $this->getAvailableProductObject($id);
                } catch (Throwable $throwable) {
                    throw new GeneralException(__("exceptions.inventory.$exception"));
                }
                break;
            default:
                throw new GeneralException(__('exceptions.inventory.invalid_lot_type'));
        }

        return $product;
    }

    public function getProductModelClass($type = null)
    {
        switch ($type) {
            case Inventory::TYPE_SOURCE:
                return SourceProduct::class;
            case Inventory::TYPE_END:
                return EndProduct::class;
            default:
                return null;
        }
    }

    public function updateSellStatus(Enumerable $inventories, int $sellStatus): Enumerable
    {
        try {
            DB::beginTransaction();
            $inventories = $inventories->map(function (Inventory $inventory) use ($sellStatus) {
                if ($inventory->qa_status !== Inventory::QA_STATUS_PASSED) {
                    throw new GeneralException(__('exceptions.inventory.qa_sample_status_invalid', ['action' => 'change sell status']));
                }
                return tap($inventory, fn(Inventory $instance) => $instance->update(['sell_status' => $sellStatus]));
            });
            DB::commit();
            return $inventories;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param Inventory $inventory
     * @param $qty
     * @return Inventory
     * @throws Throwable
     */
    public function hold(Inventory $inventory, float $qty): Inventory
    {
        throw_if(
            $qty > ($inventory->qty_on_hand - $inventory->qty_allocated) || $qty < 0,
            new GeneralException(__('exceptions.inventory.hold_failed'))
        );

        $inventory->qty_on_hold = $qty;
        $inventory->save();

        return $inventory;
    }

    /**
     * @param Enumerable $inventories
     * @param Room $disposalRoom
     * @param array $data
     * @return Enumerable
     */
    public function moveInventoriesToDisposal(Enumerable $inventories, Room $disposalRoom, array $data): Enumerable
    {
        if (!$disposalRoom->isDisposal()) {
            throw new GeneralException(__('exceptions.rooms.disposal_room_invalid'));
        }
        $quarantineStart = Carbon::now();
        try {
            DB::beginTransaction();
            $inventories->each(
                function (Inventory $inventory, $key) use ($disposalRoom, $quarantineStart, $data) {
                    $mappedProduct = $this->getMappedProducts($inventory->getType(), [$inventory->product_id])->first();
                    $this->moveInventoryToDisposal(
                        $inventory,
                        $disposalRoom,
                        [
                            'license_id' => $inventory->license_id,
                            'status' => Disposal::STATUS_SCHEDULED,
                            'uom' => optional($mappedProduct)->uom,
                            'quarantine_start' => $quarantineStart,
                            'quarantine_end' => $quarantineStart->clone()->addHour(Disposal::DEFAULT_EXTENDED_QUARANTINE_HOUR),
                            'reason' => Arr::get($data, 'reason'),
                            'quantity' => $inventory->qty_on_hand,
                            'comment' => Arr::get($data, 'comment')
                        ]
                    );
                }
            );

            DB::commit();

            return $inventories;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.inventory.destroy_failed'));
        }
    }

    /**
     * @param Inventory $inventory
     * @param Room $room
     * @param array $disposalData
     * @return Inventory
     */
    public function moveInventoryToDisposal(Inventory $inventory, Room $room, array $disposalData): Inventory
    {
        /** @var Disposal $disposal */
        $disposal = $inventory->disposals()->make($disposalData);
        $disposal->room()->associate($room);
        $disposal->strain()->associate($inventory->strain);
        $disposal->save();

        $inventory->delete();

        // dispatch disposal scheduled
        Event::dispatch(new DisposalScheduled($disposal));

        return $inventory;
    }

    public function moveInventoriesToRoom(Enumerable $inventories, Room $room): Enumerable
    {
        try {
            DB::beginTransaction();

            $inventories->each(
                fn(Inventory $inventory) => $this->moveInventoryToRoom($inventory, $room)
            );

            DB::commit();

            return $inventories;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function moveInventoryToRoom(Inventory $inventory, Room $room): Inventory
    {
        try {
            DB::beginTransaction();

            $inventory->room()->associate($room)
                ->subroom()->associate(null);

            $inventory->save();

            DB::commit();

            return $inventory;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.inventory.move_room_failed'));
        }
    }

    /**
     * @throws Throwable
     * @throws GeneralException
     */
    public function mapToProduct(Enumerable $inventories, array $data): Enumerable
    {
        $product = $this->getValidProduct($data['product_id'], $data['product_type']);
        $productType = $this->getProductModelClass($data['product_type']);
        $inventoryType = $this->findTypeBaseOnProduct($product, $data['product_type']);
        $productId = $data['product_id'];

        try {
            DB::beginTransaction();
            $inventories->each(function (Inventory $inventory) use ($productId, $inventoryType, $productType) {
                $inventory->type()->associate($inventoryType);
                $inventory->product_id = $productId;
                $inventory->product_type = $productType;
                $inventory->save();
            });
            DB::commit();
            return $inventories;
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param SourceProduct|Product $product
     * @return InventoryType
     * @throws GeneralException
     */
    protected function findTypeBaseOnProduct($product, string $productType): InventoryType
    {
        if ($productType == Inventory::TYPE_END) {
            $category = $product->category_id;
            $taxonomy = Taxonomy::where('category_id', $category)
                ->where('license_id', $this->licenseId)->with('stateCategory')->first();
        } else {
            /** @var SourceCategory $category */
            $category = $product->category;
            $taxonomy = $category->taxonomy;
        }
        if (!$category || !$taxonomy || !$taxonomy->stateCategory) {
            throw new GeneralException('Product Category is not assigned a State Category');
        }

        return InventoryType::firstOrCreate(
            [
                'license_id' => $this->licenseId,
                'state_category_id' => $taxonomy->stateCategory->id,
                'name' => $product->name,
            ],
        );
    }

    /**
     * @param array $productIds
     * @throws Throwable
     */
    public function updateProductQty(array $productIds)
    {
        try {
            DB::beginTransaction();
            foreach ($productIds as $productId => $productType) {
                $quantity = Inventory::query()
                    ->where('product_id', $productId)
                    ->get()->sum('qty_for_sale');

                switch ($productType) {
                    case EndProduct::class:
                        Sales::changeProductQuantity($productId, $quantity);
                        break;
                    case SourceProduct::class:
                        SourceProduct::query()->where('id', $productId)->update([
                            'quantity' => $quantity
                        ]);
                        break;
                    default:
                        throw new GeneralException(__('exceptions.inventory.invalid_product_type'));
                }
            }
            DB::commit();
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function updateProductName(string $productId, string $productType): void
    {
        $inventoryType = $productType == EndProduct::class ? Inventory::TYPE_END : Inventory::TYPE_SOURCE;
        $product = $this->getValidProduct($productId, $inventoryType);
        Inventory::query()
            ->where('product_id', $productId)
            ->update(['product_name' => $product->name]);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getConversions(string $id)
    {
        $inventory = $this->newQuery()->with('conversions')->findOrFail($id);
        /** @var Collection $conversions */
        $conversions = $inventory->conversions->map(function ($conversion) use ($id) {
            $conversion->allocated = optional($conversion->pivot)->quantity;
            return $conversion->only(['id', 'name', 'uom', 'allocated', 'created_at', 'updated_at']);
        });
        return $conversions;
    }
}
