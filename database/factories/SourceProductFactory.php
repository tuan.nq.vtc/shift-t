<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\License;
use App\Models\SourceCategory;
use App\Models\SourceProduct;
use App\Models\Strain;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    SourceProduct::class,
    function (Faker $faker) {
        return [
            'id' => $faker->uuid,
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                SourceProduct::class,
                $data['license_id']
            ),
            'sku' => $faker->word,
            'name' => $faker->word,
            'status' => 1,
            'price' => $faker->randomNumber(3),
            'source_category_id' => fn() => factory(SourceCategory::class)->create()->id,
            'strain_id' => fn($data) => factory(Strain::class)->create(['license_id' => $data['license_id']])->id,
            'pieces' => 1,
            'description' => $faker->paragraph(),
            'disclaimer' => $faker->paragraph(),
        ];
    }
);
