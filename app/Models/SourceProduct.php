<?php

namespace App\Models;

use App\Events\SourceProduct\SourceProductSaved;
use App\Models\Traits\{BelongToPrintLabel, BelongToSourceCategory, BelongToStrain, HasInternalId};
use App\Models\Traits\Scope\{DatetimeFilterable,
    IdsFilterable,
    LicenseFilterable,
    NameFilterable,
    StatusFilterable,
    UomFilterable
};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, SoftDeletes};

class SourceProduct extends BaseModel
{
    use IdsFilterable;
    use NameFilterable;
    use LicenseFilterable;
    use BelongToStrain;
    use BelongToSourceCategory;
    use BelongToPrintLabel;
    use StatusFilterable;
    use UomFilterable;
    use DatetimeFilterable;
    use SoftDeletes;
    use HasInternalId;
    
    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const UOM_LIST = ['kg', 'g', 'mg', 'lb', 'oz', 'bulk'];

    protected $fillable = [
        'license_id',
        'sku',
        'name',
        'strain_id',
        'source_category_id',
        'print_label_id',
        'price',
        'net_weight',
        'uom',
        'pieces',
        'description',
        'disclaimer',
        'status',
        'quantity'
    ];

    protected $dispatchesEvents = [
        'saved' => SourceProductSaved::class
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(SourceCategory::class, 'source_category_id', 'id');
    }

    public function scopeSourceCategoryId($query, ...$categoryIds): Builder
    {
        return $query->whereIn('source_category_id', $categoryIds);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        if (empty($value)) {
            return $query;
        }

        return $query->where(
            fn(Builder $query) => $query->where('name', 'like', '%' . $value . '%')
                ->orWhere('sku', 'like', '%' . $value . '%')
        );
    }
}
