<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Executable;

class RemoveQASampleCOACommand extends Executable
{
    /**
     * @var string|null
     */
    protected $qaSampleCoaId;

    /**
     * @var string|null
     */
    protected $userId;

    /**
     * @var string[]
     */
    protected $mapping = [
        'qaSampleCoaId' => 'coa_id',
        'userId' => 'user_id'
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
    }

    /**
     * @return string|null
     */
    public function getQaSampleCoaId(): ?string
    {
        return $this->qaSampleCoaId;
    }

    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
}
