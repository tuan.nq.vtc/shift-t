<?php

use App\Models\{Inventory, License, Licensee, QASample, QASampleCoa, State, User};
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;

return [
    'validations' => [
        'create' => [
            'inventories' => [
                'required',
                'array'
            ],
            'inventories.*.source_inventory_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', Inventory::class)
            ],
            'inventories.*.weight' => [
                'required',
                'numeric'
            ],
            'inventories.*.sample_type' => [
                'required',
                Rule::in(QASample::getSampleTypeList())
            ],
            'inventories.*.lab_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', Licensee::class)
            ],
            'license_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', License::class)
            ],
            'user_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', User::class)
            ],
        ],
        'reset' => [
            'license_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', License::class)
            ],
            'user_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', User::class)
            ],
        ],
        'update' => [
            'id' => ['string'],
            'thc' => ['required', 'numeric', 'min:0'],
            'thca' => ['required', 'numeric', 'min:0'],
            'cbd' => ['required', 'numeric', 'min:0'],
            'cbda' => ['required', 'numeric', 'min:0'],
            'total_thc' => ['required', 'numeric', 'min:0'],
            'total_cbd' => ['required', 'numeric', 'min:0'],
            'is_modify_child_lot' => ['required', 'boolean'],
            'analysis_type' => ['required', 'string', new In(QASample::ANALYTICS_TYPES)],
            'status' => ['required', new In([QASample::STATUS_FAILED, QASample::STATUS_PASSED])],
            'sample_type' => ['required', new In(QASample::getSampleTypeList())],
            'user_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', User::class)
            ],
            'lab_id' => ['sometimes', 'uuid', sprintf('exists:%s,id', Licensee::class)],
            'tested_at' => ['sometimes', 'date'],
        ],
        'upload' => [
            'license_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', License::class)
            ],
            'user_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', User::class)
            ],
            'qa_sample_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', QASample::class)
            ],
            'file' => [
                'required',
                'max:20480',
            ],
        ],
        'remove_coa' => [
            'user_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', User::class)
            ],
            'coa_id' => [
                'required',
                'uuid',
                sprintf('exists:%s,id', QASampleCoa::class)
            ]
        ]
    ],
    'whitelist' => [
        'metrc' => [],
        'leaf' => [
            'Harvest Materials - Flower Lots',
            'Intermediate Product - Marijuana Mix',
            'End Product - Packaged Marijuana Mix',
            'End Product - Usable Marijuana',
            'Harvested Material - Other Material Unlotted',
            'Harvested Material - Other Material Lot',
            'Harvested Material - Wet Flower',
            'Harvested Material - Wet Other Material',
            'Harvested Material - Flower Unlotted',
            'Harvested Material - Flower Lot',
            'Harvested Material - Marijuana Mix',

        ],
        'ccrs' => [
            'Harvested Material - Flower Lot',
            'Intermediate Product - Marijuana Mix',
            'End Product - Marijuana Mix Packaged',
            'End Product - Usable Marijuana',
        ],
    ],
    'coaMimeTypes' => [
        'application/pdf'
    ],
    'analysesTypes' => [
        State::REGULATOR_LEAF => [
            [
                'name' => 'THCa (percent)',
                'is_required' => true,
                'code' => 'cannabinoid_d9_thca_percent',
            ],
            [
                'name' => 'THCa (mg)',
                'is_required' => false,
                'code' => 'cannabinoid_d9_thca_mg_g',
            ],
            [
                'name' => 'THC (percent)',
                'is_required' => true,
                'code' => 'cannabinoid_d9_thc_percent',
            ],
            [
                'name' => 'THC (mg)',
                'is_required' => false,
                'code' => 'cannabinoid_d9_thc_mg_g',
            ],
            [
                'name' => 'CBD (percent)',
                'is_required' => true,
                'code' => 'cannabinoid_cbd_percent',
            ],
            [
                'name' => 'CBD (mg)',
                'is_required' => false,
                'code' => 'cannabinoid_cbd_mg_g',
            ],
            [
                'name' => 'CBDa (percent)',
                'is_required' => true,
                'code' => 'cannabinoid_cbda_percent',
            ],
            [
                'name' => 'CBDa (mg)',
                'is_required' => false,
                'code' => 'cannabinoid_cbda_mg_g',
            ],
            [
                'name' => 'Moisture content (percent)',
                'is_required' => false,
                'code' => 'moisture_content_percent',
            ],
            [
                'name' => 'Moisture content water activity rate',
                'is_required' => false,
                'code' => 'moisture_content_water_activity_rate',
            ],
            [
                'name' => 'Microbial Bile Tolerant (CFU/g)',
                'is_required' => false,
                'code' => 'microbial_bile_tolerant_cfu_g',
            ],
            [
                'name' => 'Microbial Pathogenic E Coli (CFU/g)',
                'is_required' => false,
                'code' => 'microbial_pathogenic_e_coli_cfu_g',
            ],
            [
                'name' => 'Microbial Salmonella (CFU/g)',
                'is_required' => false,
                'code' => 'microbial_salmonella_cfu_g',
            ],
            [
                'name' => 'Mycotoxin Aflatoxins (ppb)',
                'is_required' => false,
                'code' => 'mycotoxin_aflatoxins_ppb',
            ],
            [
                'name' => 'Mycotoxin Ochratoxin (ppb)',
                'is_required' => false,
                'code' => 'mycotoxin_ochratoxin_ppb',
            ],
            [
                'name' => 'Metal Arsenic (ppm)',
                'is_required' => false,
                'code' => 'metal_arsenic_ppm',
            ],
            [
                'name' => 'Metal Cadmium (ppm)',
                'is_required' => false,
                'code' => 'metal_cadmium_ppm',
            ],
            [
                'name' => 'Metal Lead (ppm)',
                'is_required' => false,
                'code' => 'metal_lead_ppm',
            ],
            [
                'name' => 'Metal mercury (ppm)',
                'is_required' => false,
                'code' => 'metal_mercury_ppm',
            ],
            [
                'name' => 'Pesticide Abamectin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_abamectin_ppm',
            ],
            [
                'name' => 'Pesticide acephate (ppm)',
                'is_required' => false,
                'code' => 'pesticide_acephate_ppm',
            ],
            [
                'name' => 'Pesticide acequinocyl (ppm)',
                'is_required' => false,
                'code' => 'pesticide_acequinocyl_ppm',
            ],
            [
                'name' => 'Pesticide acetamiprid (ppm)',
                'is_required' => false,
                'code' => 'pesticide_acetamiprid_ppm',
            ],
            [
                'name' => 'Pesticide aldicarb (ppm)',
                'is_required' => false,
                'code' => 'pesticide_aldicarb_ppm',
            ],
            [
                'name' => 'Pesticide azoxystrobin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_azoxystrobin_ppm',
            ],
            [
                'name' => 'Pesticide bifenazate (ppm)',
                'is_required' => false,
                'code' => 'pesticide_bifenazate_ppm',
            ],
            [
                'name' => 'Pesticide bifenthrin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_bifenthrin_ppm',
            ],
            [
                'name' => 'Pesticide boscalid (ppm)',
                'is_required' => false,
                'code' => 'pesticide_boscalid_ppm',
            ],
            [
                'name' => 'Pesticide carbaryl (ppm)',
                'is_required' => false,
                'code' => 'pesticide_carbaryl_ppm',
            ],
            [
                'name' => 'Pesticide carbofuran (ppm)',
                'is_required' => false,
                'code' => 'pesticide_carbofuran_ppm',
            ],
            [
                'name' => 'Pesticide chlorantraniliprole (ppm)',
                'is_required' => false,
                'code' => 'pesticide_chlorantraniliprole_ppm',
            ],
            [
                'name' => 'Pesticide chlorfenapyr (ppm)',
                'is_required' => false,
                'code' => 'pesticide_chlorfenapyr_ppm',
            ],
            [
                'name' => 'Pesticide chlorpyrifos (ppm)',
                'is_required' => false,
                'code' => 'pesticide_chlorpyrifos_ppm',
            ],
            [
                'name' => 'Pesticide clofentezine (ppm)',
                'is_required' => false,
                'code' => 'pesticide_clofentezine_ppm',
            ],
            [
                'name' => 'Pesticide cyfluthrin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_cyfluthrin_ppm',
            ],
            [
                'name' => 'Pesticide cypermethrin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_cypermethrin_ppm',
            ],
            [
                'name' => 'Pesticide daminozide (ppm)',
                'is_required' => false,
                'code' => 'pesticide_daminozide_ppm',
            ],
            [
                'name' => 'Pesticide ddvp dichlorvos (ppm)',
                'is_required' => false,
                'code' => 'pesticide_ddvp_dichlorvos_ppm',
            ],
            [
                'name' => 'Pesticide diazinon (ppm)',
                'is_required' => false,
                'code' => 'pesticide_diazinon_ppm',
            ],
            [
                'name' => 'Pesticide dimethoate (ppm)',
                'is_required' => false,
                'code' => 'pesticide_dimethoate_ppm',
            ],
            [
                'name' => 'Pesticide ethoprophos (ppm)',
                'is_required' => false,
                'code' => 'pesticide_ethoprophos_ppm',
            ],
            [
                'name' => 'Pesticide etofenprox (ppm)',
                'is_required' => false,
                'code' => 'pesticide_etofenprox_ppm',
            ],
            [
                'name' => 'Pesticide etoxazole (ppm)',
                'is_required' => false,
                'code' => 'pesticide_etoxazole_ppm',
            ],
            [
                'name' => 'Pesticide fenoxycarb (ppm)',
                'is_required' => false,
                'code' => 'pesticide_fenoxycarb_ppm',
            ],
            [
                'name' => 'Pesticide fenpyroximate (ppm)',
                'is_required' => false,
                'code' => 'pesticide_fenpyroximate_ppm',
            ],
            [
                'name' => 'Pesticide fipronil (ppm)',
                'is_required' => false,
                'code' => 'pesticide_fipronil_ppm',
            ],
            [
                'name' => 'Pesticide flonicamid (ppm)',
                'is_required' => false,
                'code' => 'pesticide_flonicamid_ppm',
            ],
            [
                'name' => 'Pesticide fludioxonil (ppm)',
                'is_required' => false,
                'code' => 'pesticide_fludioxonil_ppm',
            ],
            [
                'name' => 'Pesticide hexythiazox (ppm)',
                'is_required' => false,
                'code' => 'pesticide_hexythiazox_ppm',
            ],
            [
                'name' => 'Pesticide imazalil (ppm)',
                'is_required' => false,
                'code' => 'pesticide_imazalil_ppm',
            ],
            [
                'name' => 'Pesticide imidacloprid (ppm)',
                'is_required' => false,
                'code' => 'pesticide_imidacloprid_ppm',
            ],
            [
                'name' => 'Pesticide kresoxim methyl (ppm)',
                'is_required' => false,
                'code' => 'pesticide_kresoxim_methyl_ppm',
            ],
            [
                'name' => 'Pesticide malathion (ppm)',
                'is_required' => false,
                'code' => 'pesticide_malathion_ppm',
            ],
            [
                'name' => 'Pesticide metalaxyl (ppm)',
                'is_required' => false,
                'code' => 'pesticide_metalaxyl_ppm',
            ],
            [
                'name' => 'Pesticide methiocarb (ppm)',
                'is_required' => false,
                'code' => 'pesticide_methiocarb_ppm',
            ],
            [
                'name' => 'Pesticide methomyl (ppm)',
                'is_required' => false,
                'code' => 'pesticide_methomyl_ppm',
            ],
            [
                'name' => 'Pesticide methyl parathion (ppm)',
                'is_required' => false,
                'code' => 'pesticide_methyl_parathion_ppm',
            ],
            [
                'name' => 'Pesticide mgk 264 (ppm)',
                'is_required' => false,
                'code' => 'pesticide_mgk_264_ppm',
            ],
            [
                'name' => 'Pesticide myclobutanil (ppm)',
                'is_required' => false,
                'code' => 'pesticide_myclobutanil_ppm',
            ],
            [
                'name' => 'Pesticide naled (ppm)',
                'is_required' => false,
                'code' => 'pesticide_naled_ppm',
            ],
            [
                'name' => 'Pesticide oxamyl (ppm)',
                'is_required' => false,
                'code' => 'pesticide_oxamyl_ppm',
            ],
            [
                'name' => 'Pesticide paclobutrazol (ppm)',
                'is_required' => false,
                'code' => 'pesticide_paclobutrazol_ppm',
            ],
            [
                'name' => 'Pesticide permethrinsa (ppm)',
                'is_required' => false,
                'code' => 'pesticide_permethrinsa_ppm',
            ],
            [
                'name' => 'Pesticide phosmet (ppm)',
                'is_required' => false,
                'code' => 'pesticide_phosmet_ppm',
            ],
            [
                'name' => 'Pesticide piperonyl butoxide b (ppm)',
                'is_required' => false,
                'code' => 'pesticide_piperonyl_butoxide_b_ppm',
            ],
            [
                'name' => 'Pesticide prallethrin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_prallethrin_ppm',
            ],
            [
                'name' => 'Pesticide propiconazole (ppm)',
                'is_required' => false,
                'code' => 'pesticide_propiconazole_ppm',
            ],
            [
                'name' => 'Pesticide propoxur (ppm)',
                'is_required' => false,
                'code' => 'pesticide_propoxur_ppm',
            ],
            [
                'name' => 'Pesticide pyrethrinsbc (ppm)',
                'is_required' => false,
                'code' => 'pesticide_pyrethrinsbc_ppm',
            ],
            [
                'name' => 'Pesticide pyridaben (ppm)',
                'is_required' => false,
                'code' => 'pesticide_pyridaben_ppm',
            ],
            [
                'name' => 'Pesticide spinosad (ppm)',
                'is_required' => false,
                'code' => 'pesticide_spinosad_ppm',
            ],
            [
                'name' => 'Pesticide spiromesifen (ppm)',
                'is_required' => false,
                'code' => 'pesticide_spiromesifen_ppm',
            ],
            [
                'name' => 'Pesticide spirotetramat (ppm)',
                'is_required' => false,
                'code' => 'pesticide_spirotetramat_ppm',
            ],
            [
                'name' => 'Pesticide spiroxamine (ppm)',
                'is_required' => false,
                'code' => 'pesticide_spiroxamine_ppm',
            ],
            [
                'name' => 'Pesticide tebuconazole (ppm)',
                'is_required' => false,
                'code' => 'pesticide_tebuconazole_ppm',
            ],
            [
                'name' => 'Pesticide thiacloprid (ppm)',
                'is_required' => false,
                'code' => 'pesticide_thiacloprid_ppm',
            ],
            [
                'name' => 'Pesticide thiamethoxam (ppm)',
                'is_required' => false,
                'code' => 'pesticide_thiamethoxam_ppm',
            ],
            [
                'name' => 'Pesticide trifloxystrobin (ppm)',
                'is_required' => false,
                'code' => 'pesticide_trifloxystrobin_ppm',
            ],
            [
                'name' => 'Solvent acetone (ppm)',
                'is_required' => false,
                'code' => 'solvent_acetone_ppm',
            ],
            [
                'name' => 'Solvent benzene (ppm)',
                'is_required' => false,
                'code' => 'solvent_benzene_ppm',
            ],
            [
                'name' => 'Solvent butanes (ppm)',
                'is_required' => false,
                'code' => 'solvent_butanes_ppm',
            ],
            [
                'name' => 'Solvent cyclohexane (ppm)',
                'is_required' => false,
                'code' => 'solvent_cyclohexane_ppm',
            ],
            [
                'name' => 'Solvent chloroform (ppm)',
                'is_required' => false,
                'code' => 'solvent_chloroform_ppm',
            ],
            [
                'name' => 'Solvent dichloromethane (ppm)',
                'is_required' => false,
                'code' => 'solvent_dichloromethane_ppm',
            ],
            [
                'name' => 'Solvent ethyl acetate (ppm)',
                'is_required' => false,
                'code' => 'solvent_ethyl_acetate_ppm',
            ],
            [
                'name' => 'Solvent heptanes (ppm)',
                'is_required' => false,
                'code' => 'solvent_heptanes_ppm',
            ],
            [
                'name' => 'Solvent hexanes (ppm)',
                'is_required' => false,
                'code' => 'solvent_hexanes_ppm',
            ],
            [
                'name' => 'Solvent isopropanol (ppm)',
                'is_required' => false,
                'code' => 'solvent_isopropanol_ppm',
            ],
            [
                'name' => 'Solvent methanol (ppm)',
                'is_required' => false,
                'code' => 'solvent_methanol_ppm',
            ],
            [
                'name' => 'Solvent pentanes (ppm)',
                'is_required' => false,
                'code' => 'solvent_pentanes_ppm',
            ],
            [
                'name' => 'Solvent propane (ppm)',
                'is_required' => false,
                'code' => 'solvent_propane_ppm',
            ],
            [
                'name' => 'Solvent toluene (ppm)',
                'is_required' => false,
                'code' => 'solvent_toluene_ppm',
            ],
            [
                'name' => 'Solvent xylene (ppm)',
                'is_required' => false,
                'code' => 'solvent_xylene_ppm',
            ],
        ],
        State::REGULATOR_METRC => [
            [
                'name' => 'Abamectin',
                'is_required' => false,
                'code' => 'Abamectin',
            ],
            [
                'name' => 'Acetone',
                'is_required' => false,
                'code' => 'Acetone',
            ],
            [
                'name' => 'Azoxystrobin',
                'is_required' => false,
                'code' => 'Azoxystrobin',
            ],
            [
                'name' => 'Benzene',
                'is_required' => false,
                'code' => 'Benzene',
            ],
            [
                'name' => 'Benzene (µg/g)',
                'is_required' => false,
                'code' => 'Benzene (µg/g)',
            ],
            [
                'name' => 'Benzene (pass/fail)',
                'is_required' => false,
                'code' => 'Benzene (pass/fail)',
            ],
            [
                'name' => 'Bifenazate',
                'is_required' => false,
                'code' => 'Bifenazate',
            ],
            [
                'name' => 'Butane (pass/fail)',
                'is_required' => false,
                'code' => 'Butane (pass/fail)',
            ],
            [
                'name' => 'Butanes',
                'is_required' => false,
                'code' => 'Butanes',
            ],
            [
                'name' => 'Butanes (µg/g)',
                'is_required' => false,
                'code' => 'Butanes (µg/g)',
            ],
            [
                'name' => 'CBD',
                'is_required' => true,
                'code' => 'CBD',
            ],
            [
                'name' => 'CBDa',
                'is_required' => true,
                'code' => 'CBDa',
            ],
            [
                'name' => 'CBN',
                'is_required' => false,
                'code' => 'CBN',
            ],
            [
                'name' => 'Ethanol',
                'is_required' => false,
                'code' => 'Ethanol',
            ],
            [
                'name' => 'Etoxazole',
                'is_required' => false,
                'code' => 'Etoxazole',
            ],
            [
                'name' => 'Heavy Metals',
                'is_required' => false,
                'code' => 'Heavy Metals',
            ],
            [
                'name' => 'Heptanes',
                'is_required' => false,
                'code' => 'Heptanes',
            ],
            [
                'name' => 'Hexane',
                'is_required' => false,
                'code' => 'Hexane',
            ],
            [
                'name' => 'Homogeneity',
                'is_required' => false,
                'code' => 'Homogeneity',
            ],
            [
                'name' => 'Imazalil',
                'is_required' => false,
                'code' => 'Imazalil',
            ],
            [
                'name' => 'Imidacloprid',
                'is_required' => false,
                'code' => 'Imidacloprid',
            ],
            [
                'name' => 'Isopropyl Alcohol',
                'is_required' => false,
                'code' => 'Isopropyl Alcohol',
            ],
            [
                'name' => 'Malathion',
                'is_required' => false,
                'code' => 'Malathion',
            ],
            [
                'name' => 'Microbials',
                'is_required' => false,
                'code' => 'Microbials',
            ],
            [
                'name' => 'Microbials (Bud & Shake/Trim)',
                'is_required' => false,
                'code' => 'Microbials (Bud & Shake/Trim)',
            ],

            [
                'name' => 'Microbials (For Remediated Concentrates Only)',
                'is_required' => false,
                'code' => 'Microbials (For Remediated Concentrates Only)',
            ],
            [
                'name' => 'Microbials For Remediated Concentrates',
                'is_required' => false,
                'code' => 'Microbials For Remediated Concentrates',
            ],
            [
                'name' => 'Myclobutanil',
                'is_required' => false,
                'code' => 'Myclobutanil',
            ],
            [
                'name' => 'Other Pesticide',
                'is_required' => false,
                'code' => 'Other Pesticide',
            ],
            [
                'name' => 'Other Solvents',
                'is_required' => false,
                'code' => 'Other Solvents',
            ],
            [
                'name' => 'Pentane',
                'is_required' => false,
                'code' => 'Pentane',
            ],
            [
                'name' => 'Permethrin',
                'is_required' => false,
                'code' => 'Permethrin',
            ],
            [
                'name' => 'Pesticide',
                'is_required' => false,
                'code' => 'Pesticide',
            ],
            [
                'name' => 'Pesticides',
                'is_required' => false,
                'code' => 'Pesticides',
            ],
            [
                'name' => 'Pesticides (pass/fail)',
                'is_required' => false,
                'code' => 'Pesticides (pass/fail)',
            ],
            [
                'name' => 'Potency',
                'is_required' => false,
                'code' => 'Potency',
            ],
            [
                'name' => 'Potency (pass/fail)',
                'is_required' => false,
                'code' => 'Potency (pass/fail)',
            ],
            [
                'name' => 'Propane',
                'is_required' => false,
                'code' => 'Propane',
            ],
            [
                'name' => 'R & D Genetics, Terpenes, Other',
                'is_required' => false,
                'code' => 'R & D Genetics, Terpenes, Other',
            ],
            [
                'name' => 'R&D Contaminants',
                'is_required' => false,
                'code' => 'R&D Contaminants',
            ],
            [
                'name' => 'R&D Heavy Metals',
                'is_required' => false,
                'code' => 'R&D Heavy Metals',
            ],
            [
                'name' => 'R&D Homogeneity',
                'is_required' => false,
                'code' => 'R&D Homogeneity',
            ],
            [
                'name' => 'R&D Microbials (Bud & Shake/Trim)',
                'is_required' => false,
                'code' => 'R&D Microbials (Bud & Shake/Trim)',
            ],
            [
                'name' => 'R&D Microbials (Infused Product & Non Solvent Based Concentrates)',
                'is_required' => false,
                'code' => 'R&D Microbials (Infused Product & Non Solvent Based Concentrates)',
            ],
            [
                'name' => 'R&D Microbials (Remediated Solvent Based Concentrates)',
                'is_required' => false,
                'code' => 'R&D Microbials (Remediated Solvent Based Concentrates)',
            ],
            [
                'name' => 'R&D Pesticide',
                'is_required' => false,
                'code' => 'R&D Pesticide',
            ],
            [
                'name' => 'R&D Potency',
                'is_required' => false,
                'code' => 'R&D Potency',
            ],
            [
                'name' => 'R&D Residual Solvents',
                'is_required' => false,
                'code' => 'R&D Residual Solvents',
            ],
            [
                'name' => 'R&D Terpenes',
                'is_required' => false,
                'code' => 'R&D Terpenes',
            ],
            [
                'name' => 'R&D Total Yeast and Mold Count (Bud & Shake/Trim)',
                'is_required' => false,
                'code' => 'R&D Total Yeast and Mold Count (Bud & Shake/Trim)',
            ],
            [
                'name' => 'R&D Total Yeast and Mold Count (Infused Product & Non Solvent Based Concentrates)',
                'is_required' => false,
                'code' => 'R&D Total Yeast and Mold Count (Infused Product & Non Solvent Based Concentrates)',
            ],
            [
                'name' => 'R&D Total Yeast and Mold Count (Remediated Solvent Based Concentrates)',
                'is_required' => false,
                'code' => 'R&D Total Yeast and Mold Count (Remediated Solvent Based Concentrates)',
            ],

            [
                'name' => 'Remediated Solvent Based Concentrate',
                'is_required' => false,
                'code' => 'Remediated Solvent Based Concentrate',
            ],
            [
                'name' => 'Solvents',
                'is_required' => false,
                'code' => 'Solvents',
            ],
            [
                'name' => 'Solvents (pass/fail)',
                'is_required' => false,
                'code' => 'Solvents (pass/fail)',
            ],
            [
                'name' => 'Spinosad',
                'is_required' => false,
                'code' => 'Spinosad',
            ],
            [
                'name' => 'Spiromesifen',
                'is_required' => false,
                'code' => 'Spiromesifen',
            ],
            [
                'name' => 'Spirotetramat',
                'is_required' => false,
                'code' => 'Spirotetramat',
            ],
            [
                'name' => 'Tebuconazole',
                'is_required' => false,
                'code' => 'Tebuconazole',
            ],
            [
                'name' => 'THC',
                'is_required' => true,
                'code' => 'THC',
            ],

            [
                'name' => 'THCa',
                'is_required' => true,
                'code' => 'THCa',
            ],
            [
                'name' => 'Toluene',
                'is_required' => false,
                'code' => 'Toluene',
            ],
            [
                'name' => 'Total Xylenes',
                'is_required' => false,
                'code' => 'Total Xylenes',
            ],
            [
                'name' => 'Total Yeast and Mold',
                'is_required' => false,
                'code' => 'Total Yeast and Mold',
            ],
            [
                'name' => 'Total Yeast and Mold Count',
                'is_required' => false,
                'code' => 'Total Yeast and Mold Count',
            ],
            [
                'name' => 'Total Yeast and Mold Count (For Remediated Concentrates Only)',
                'is_required' => false,
                'code' => 'Total Yeast and Mold Count (For Remediated Concentrates Only)',
            ],
            [
                'name' => 'Total Yeast and Mold for Remediated Concentrates',
                'is_required' => false,
                'code' => 'Total Yeast and Mold for Remediated Concentrates',
            ],
        ]
    ]
];
