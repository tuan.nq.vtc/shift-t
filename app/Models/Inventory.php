<?php

namespace App\Models;

use App\Events\Inventory\InventorySaved;
use App\Models\Traits\{BelongToBatch,
    BelongToStrain,
    Boots\RoomStatistic,
    HasInternalId,
    Scope\DatetimeFilterable,
    Scope\IdsFilterable,
    Scope\LicenseFilterable,
    StoreInRoom,
    StoreInSubroom,
    WithoutTraceableModelEvents
};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, BelongsToMany, HasMany, HasOne, MorphMany, MorphTo};
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Inventory
 * @property string sync_code
 * @property float qty_on_hand
 * @property float qty_available
 * @property float qty_allocated
 * @property float qty_on_hold
 * @property float qty_for_sale
 * @property bool is_lotted
 * @property bool sell_status
 * @property int qa_status
 * @property License license
 * @package App\Models
 */
class Inventory extends BaseModel implements TraceableModelInterface
{
    use IdsFilterable;
    use BelongToBatch;
    use StoreInRoom;
    use StoreInSubroom;
    use LicenseFilterable;
    use BelongToStrain;
    use DatetimeFilterable;
    use WithoutTraceableModelEvents;
    use RoomStatistic;
    use SoftDeletes;
    use HasInternalId;

    public const SYNC_ACTION_SPLIT_LOT = 'splitLot';
    public const UOM_GRAMS = 'g';
    public const UOM_EACH = 'ea';

    public const SELL_STATUS_UNAVAILABLE = 0;
    public const SELL_STATUS_AVAILABLE = 1;

    public const SELL_STATUSES = [
        self::SELL_STATUS_UNAVAILABLE,
        self::SELL_STATUS_AVAILABLE
    ];

    public const QA_STATUS_NOT_SAMPLED = 0;
    public const QA_STATUS_SAMPLED_TEST = 1;
    public const QA_STATUS_SAMPLED_RETEST = 2;
    public const QA_STATUS_AWAITING_RESULTS = 3;
    public const QA_STATUS_PASSED = 4;
    public const QA_STATUS_FAILED = 5;

    public const QA_STATUSES = [
        self::QA_STATUS_NOT_SAMPLED,
        self::QA_STATUS_SAMPLED_TEST,
        self::QA_STATUS_SAMPLED_RETEST,
        self::QA_STATUS_AWAITING_RESULTS,
        self::QA_STATUS_PASSED,
        self::QA_STATUS_FAILED,
    ];

    public const CAN_CREATE_QA_SAMPLE = [
        self::QA_STATUS_NOT_SAMPLED,
        self::QA_STATUS_PASSED,
        self::QA_STATUS_FAILED,
    ];

    public const REASON_DESTROY = [
        'failed qa',
        'quality control',
        'product return',
        'spoilage',
        'mandated',
        'other'
    ];

    public const IS_LOTTED = 1;

    public const TYPE_SOURCE = "source";
    public const TYPE_END = "end";

    protected $fillable = [
        'license_id',
        'type_id',
        'parent_id',
        'state_category_id',
        'final_qa_sample_id',
        'room_id',
        'subroom_id',
        'strain_id',
        'product_type',
        'product_id',
        'product_name',
        'source_type',
        'source_id',
        'is_lotted',
        'for_extraction',
        'qa_status',
        'sell_status',
        'info',
        'qty_on_hand',
        'qty_allocated',
        'qty_on_hold',
        'sync_code',
        'sync_status',
        'synced_at',
    ];

    protected $casts = [
        'is_lotted' => 'bool',
        'sell_status' => 'bool',
        'info' => 'array',
        'qty_allocated' => 'float',
        'qty_on_hand' => 'float',
        'qty_on_hold' => 'float',
    ];

    protected $appends = ['qty_available', 'qty_for_sale', 'is_synced', 'qty_for_conversion', 'unit_size'];

    protected $dispatchesEvents = [
        'saved' => InventorySaved::class
    ];

    /**
     * @param $key
     * @return int
     */
    public static function getQAStatus($key): int
    {
        $resultStatuses = ['passed' => self::QA_STATUS_PASSED, 'failed' => self::QA_STATUS_FAILED];

        if (!array_key_exists($key, $resultStatuses)) {
            return -1;
        }

        return $resultStatuses[$key];
    }

    /**
     * @return int[]
     */
    public static function getQAStatuses(): array
    {
        return [
            self::QA_STATUS_NOT_SAMPLED,
            self::QA_STATUS_SAMPLED_TEST,
            self::QA_STATUS_SAMPLED_RETEST,
            self::QA_STATUS_AWAITING_RESULTS,
            self::QA_STATUS_PASSED,
            self::QA_STATUS_FAILED,
        ];
    }

    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            return [
                'global_id' => fn($instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,],
                    ['name' => 'Inventory ' . $value]
                ),
                'qty' => fn($instance, $value) => $instance->setAttribute('quantity', $value),
                'global_area_id' => fn($instance, $value) => $this->processLeafArea($instance, $value),
                'global_inventory_type_id' => fn($instance, $value) => $this->processLeafInventoryType(
                    $instance,
                    $value
                ),
                // Save external_id from LEAF as an info attribute
                'external_id' => fn($instance, $value) => $instance->setAttribute('info', ['external_id' => $value]),
            ];
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            // TODO: Metrc
            return [
                'Id' => fn($instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'Name' => fn($instance, $value) => $instance->setAttribute('name', $value),
            ];
        }

        return null;
    }

    private function processLeafArea($instance, $value)
    {
        $room = Room::where('sync_code', $value)->first();
        if ($room) {
            return $instance->setAttribute('room_id', $room->id);
        }

        return null;
    }

    private function processLeafInventoryType($instance, $value)
    {
        $inventoryType = InventoryType::where('sync_code', $value)->first();
        if ($inventoryType) {
            return $instance->setAttribute('type_id', $inventoryType->id);
        }

        return null;
    }

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(InventoryType::class, 'type_id', 'id');
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Inventory::class, 'parent_id', 'id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Inventory::class, 'parent_id', 'id');
    }

    public function allChildren(): HasMany
    {
        return $this->children()->with('allChildren');
    }

    public function getAllChildrenIds(): array
    {
        $allChildrenIds = [];

        if ($this->allChildren->isEmpty()) {
            return $allChildrenIds;
        }

        foreach ($this->allChildren as $child) {
            $allChildrenIds[] = $child->id;
            $allChildrenIds = array_merge($allChildrenIds, $child->getAllChildrenIds());
        }
        return $allChildrenIds;
    }

    public function stateCategory(): BelongsTo
    {
        return $this->belongsTo(StateCategory::class, 'state_category_id', 'id');
    }

    public function getStateCategory(): ?StateCategory
    {
        $inventoryType = $this->type()->first();

        if ($inventoryType instanceof InventoryType !== true) {
            return null;
        }

        return $inventoryType->stateCategory()->first();
    }

    public function source(): MorphTo
    {
        return $this->morphTo();
    }

    public function product()
    {
        return $this->morphTo();
    }

    public function endProduct()
    {
        return $this->hasMany(EndProduct::class, 'product_id', 'product_id')->where('status', EndProduct::STATUS_ENABLED);
    }

    public function qaSamples(): HasMany
    {
        return $this->hasMany(QASample::class, 'source_inventory_id', 'id');
    }

    public function sublots(): HasMany
    {
        return $this->hasMany(Inventory::class, 'parent_id', 'id')->where('is_lotted', true);
    }

    public function histories(): HasMany
    {
        return $this->hasMany(InventoryHistory::class);
    }

    public function disposals(): MorphMany
    {
        return $this->morphMany(Disposal::class, 'wastable');
    }

    public function conversions(): BelongsToMany
    {
        return $this->belongsToMany(
            Conversion::class,
            'conversion_input_lot',
            'inventory_id',
            'conversion_id'
        )->withPivot([
            'quantity',
            'product_id'
        ]);
    }

    public function manifestItems(): MorphMany
    {
        return $this->morphMany(ManifestItem::class, 'resource');
    }

    public function finalQASample(): HasOne
    {
        return $this->hasOne(QASample::class, 'id', 'final_qa_sample_id');
    }

    public function getQtyAvailableAttribute(): float
    {
        return round($this->qty_on_hand - $this->qty_allocated - $this->qty_on_hold, 4);
    }

    public function getOriginalQtyAvailableAttribute(): float
    {
        return $this->qty_on_hand - $this->qty_on_hold;
    }

    public function getQtyForSaleAttribute()
    {
        return $this->sell_status ? round($this->qty_on_hand - $this->qty_allocated - $this->qty_on_hold, 4) : 0;
    }

    public function getQtyForConversionAttribute(): float
    {
        return round($this->qty_on_hand - $this->qty_allocated - $this->qty_on_hold, 4);
    }

    public function scopeIsHideDepleted(Builder $query, $isHideDepleted): Builder
    {
        return $isHideDepleted ? $query->where('qty_on_hand', '>', 0) : $query;
    }

    public function scopeIsShowDepleted(Builder $query, $isShowDepleted): Builder
    {
        return $isShowDepleted ? $query->where('qty_on_hand', '<=', 0) : $query;
    }

    public function scopeIsHideTested(Builder $query, $isHideTested): Builder
    {
        return $isHideTested ?
            $query->whereNotIn('qa_status', [Inventory::QA_STATUS_PASSED, Inventory::QA_STATUS_FAILED]) : $query;
    }

    public function scopeIsShowTested(Builder $query, $isShowTested): Builder
    {
        return $isShowTested ?
            $query->whereIn('qa_status', [Inventory::QA_STATUS_PASSED, Inventory::QA_STATUS_FAILED]) : $query;
    }

    public function scopeIsHideQaSampled(Builder $query, $isHideQaSamples): Builder
    {
        return $isHideQaSamples ?
            $query->having('qa_samples_count', '<=', 0) : $query;
    }

    public function scopeIsShowQaSampled(Builder $query, $isShowQaSamples): Builder
    {
        return $isShowQaSamples ?
            $query->having('qa_samples_count', '>', 0) : $query;
    }

    public function scopeIsHideBlockedLots(Builder $query, $isHideBlockedLots): Builder
    {
        return $isHideBlockedLots ?
            $query->where('qty_on_hold', '<=', 0)->orWhereRaw('qty_on_hold <> (qty_on_hand - qty_allocated)') : $query;
    }

    public function scopeIsShowBlockedLots(Builder $query, $isShowBlockedLots): Builder
    {
        return $isShowBlockedLots ?
            $query->where('qty_on_hold', '>', 0)->whereRaw('qty_on_hold = (qty_on_hand - qty_allocated)') : $query;
    }

    /**
     * @return bool
     */
    public function getIsSyncedAttribute(): bool
    {
        return $this->sync_status === self::SYNC_STATUS_SYNCED;
    }

    public function scopeIsLotted(Builder $query, bool $isLotted = true): Builder
    {
        return $query->where('is_lotted', $isLotted);
    }

    public function scopeIsForExtraction(Builder $query, bool $forExtraction = true): Builder
    {
        return $query->where('is_for_extraction', $forExtraction);
    }

    public function scopeStateCategoryIds(Builder $query, ...$stateCategoryIds): Builder
    {
        return $query->whereHas(
            'type',
            fn(Builder $query) => $query->whereIn('state_category_id', $stateCategoryIds)
        );
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        if (empty($value)) {
            return $query;
        }
        return $query->where(
            function ($subQuery) use ($value) {
                $subQuery->where('sync_code', 'like', '%' . $value . '%')
                    ->orWhereHas('strain', fn($strainQuery) => $strainQuery->where('name', 'LIKE', '%' . $value . '%'));
            }
        );
    }

    /**
     * Scope a query to only include records have sync status that contains $status.
     *
     * @param Builder $query
     * @param int $status
     *
     * @return Builder
     */
    public function scopeSyncStatus(Builder $query, int $status): Builder
    {
        return $query->where('sync_status', $status);
    }

    /**
     * Scope a query to only include records have sync code that contains $code.
     *
     * @param Builder $query
     * @param string $code
     *
     * @return Builder
     */
    public function scopeSyncCode(Builder $query, string $code): Builder
    {
        return $query->where('sync_code', $code);
    }

    public function scopeHarvestId(Builder $query, ...$harvestGroupId): Builder
    {
        return $query->whereHasMorph(
            'source',
            Harvest::class,
            fn(Builder $resourceQuery) => $resourceQuery->whereIn('harvest_group_id', $harvestGroupId)
        );
    }

    public function scopeInventoryTypeId(Builder $query, ...$inventoryTypeId): Builder
    {
        return $query->whereIn('type_id', $inventoryTypeId);
    }

    public function updateSyncStatus(int $status): TraceableModelInterface
    {
        $this->update(['sync_status' => $status, 'synced_at' => Carbon::now(),]);

        return $this;
    }

    public function updateSyncCode(string $code): TraceableModelInterface
    {
        static::updateWithoutTraceableModelEvents(['sync_code' => $code,]);

        return $this;
    }

    /**
     * Scope a query to only include records have been synced.
     *
     * @param Builder $query
     * @param bool $isSynced
     *
     * @return Builder
     */
    public function scopeIsSynced(Builder $query, bool $isSynced): Builder
    {
        return $isSynced ?
            $query->where('sync_status', TraceableModel::SYNC_STATUS_SYNCED) :
            $query->where('sync_status', '!=', TraceableModel::SYNC_STATUS_SYNCED);
    }

    public function scopeOrderByIsSynced(Builder $query, $direction = 'asc'): Builder
    {
        return $query->orderBy('sync_status', $direction);
    }

    public function scopeProductIds(Builder $query, ...$productIds): Builder
    {
        return $query->whereIn('product_id', $productIds);
    }

    public function scopeQAStatus(Builder $builder, ...$qaStatus): Builder
    {
        return $builder->whereIn('qa_status', $qaStatus);
    }

    public function scopeLotDate(Builder $query, $fromDate, $toDate = null): Builder
    {
        return $this->buildQueryDateRanger($query, $fromDate, $toDate, 'created_at');
    }

    public function scopeSellStatus(Builder $query, $sellStatus): Builder
    {
        return $query->where('sell_status', $sellStatus);
    }

    public function scopeOrderByProductName(Builder $query, $direction = 'asc'): Builder
    {
        return $query->orderBy('product_name', $direction);
    }

    public function getUnitSizeAttribute()
    {
        return optional($this->type)->uom;
    }

    /**
     * @param float $quantity
     * @return $this
     */
    public function increaseQty(float $quantity): self
    {
        $this->qty_on_hand += $quantity;
        return $this;
    }

    /**
     * @param float $quantity
     * @return $this
     */
    public function decreaseQty(float $quantity): self
    {
        $this->qty_on_hand -= $quantity;
        return $this;
    }

    /**
     * @param $count
     */
    public function setQAStatusByQASampleCount($count)
    {
        if ($count === 1) {
            $this->setAttribute('qa_status', self::QA_STATUS_SAMPLED_TEST);
        }

        if ($count > 1) {
            $this->setAttribute('qa_status', self::QA_STATUS_SAMPLED_RETEST);
        }
    }

    /**
     * @return mixed|string
     */
    public function getRegulator()
    {
        $license = $this->license()->first();
        if ($license instanceof License !== true) {
            return '';
        }

        return $license->getRegulator();
    }

    public function hasFinalQASampleStatus(): bool
    {
        return $this->qa_status === Inventory::QA_STATUS_PASSED || $this->qa_status === Inventory::QA_STATUS_FAILED;
    }

    /**
     * @return $this
     */
    public function calculateAllocatedQuantity(): self
    {
        $allocatedQty = $this->conversions()->sum('quantity') + $this->manifestItems()
                ->where('status', ManifestItem::STATUS_PENDING)->sum('quantity');
        $this->setAttribute('qty_allocated', $allocatedQty);
        return $this;
    }

    /**
     * Get Inventory type
     * @return string|null
     */
    public function getType()
    {
        switch ($this->product_type) {
            case SourceProduct::class:
                return static::TYPE_SOURCE;
            case EndProduct::class:
                return static::TYPE_END;
            default:
                return '';
        }
    }
}
