<?php

namespace App\Services;

use App\Events\GrowCycle\GrowCycleGrowStatusUpdated;
use App\Events\PlantGroup\PlantGroupToRoomMoved;
use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{GrowCycle, Plant, Room, SeedToSale, Subroom};
use App\Services\Traits\PlantHandler;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection};
use Illuminate\Support\{Enumerable, Facades\DB};
use Illuminate\Support\Facades\Event;
use Throwable;

/**
 * Class GrowCycleService
 * @package App\Services
 *
 * @method GrowCycle|Collection get($id)
 * @method GrowCycle|Builder getModel()
 * @method Enumerable|GrowCycle[] getEnumerable(array $ids, $relations = [], $counts = [])
 */
class GrowCycleService extends BaseService
{
    use PlantHandler;

    protected array $relations = [
        'plantGroups',
        'plantGroups.strain',
        'plantGroups.room',
        'plants',
        'plants.motherPropagation',
        'plants.strain:id,name',
        'plants.room:id,name',
        'plants.subroom:id,name',
    ];

    public function __construct(GrowCycle $growCycle)
    {
        parent::__construct($growCycle);
    }

    /**
     * @param Criteria $criteria
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $paginator = parent::list($criteria->addFilter('available', true));
        $paginator->getCollection()->transform(
            function ($growCycle) {
                return $this->listTransform($growCycle);
            }
        );
        return $paginator;
    }

    /**
     * @param GrowCycle $growCycle
     * @return GrowCycle
     */
    private function listTransform(GrowCycle $growCycle)
    {
        $sourceTypes = $strains = $motherCodes = $propagations = $sources = [];
        foreach ($growCycle->plants as $plant) {
            $propagations = $this->pushValue(
                $propagations,
                $plant->propagation_id,
                optional($plant->motherPropagation)->sync_code
            );
            $strains = $this->pushValue($strains, $plant->strain->id, $plant->strain->name);
            $motherCodes = $this->pushValue($motherCodes, $plant->mother_id, $plant->mother_code);
            $sourceTypes = $this->pushValue($sourceTypes, $plant->source_type, 1);
        }
        $growCycle->propagations = $propagations;
        $growCycle->source_types = array_keys($sourceTypes);
        $growCycle->mother_codes = $motherCodes;
        $growCycle->strains = $strains;
        return $growCycle;
    }

    /**
     * @param array $arr
     * @param $key
     * @param $value
     * @return array
     */
    private function pushValue(array $arr, $key, $value)
    {
        if (!isset($arr[$key]) && !empty($value)) {
            $arr[$key] = $value;
        }

        return $arr;
    }

    /**
     * @param Enumerable $growCycles
     * @param string $growStatus
     *
     * @return Enumerable
     *
     * @throws Throwable
     */
    public function updateGrowCyclesGrowStatus(Enumerable $growCycles, string $growStatus): Enumerable
    {
        try {
            DB::beginTransaction();
            $growCycles = $growCycles->map(
                fn(GrowCycle $growCycle) => $this->updateGrowCycleGrowStatus($growCycle, $growStatus)
            );
            DB::commit();

            return $growCycles;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param GrowCycle $growCycle
     * @param string $growStatus
     *
     * @return GrowCycle
     *
     * @throws Throwable
     */
    public function updateGrowCycleGrowStatus(GrowCycle $growCycle, string $growStatus): GrowCycle
    {
        if (!in_array($growStatus, SeedToSale::GROW_CYCLE_GROW_STATUSES)) {
            throw new GeneralException('Invalid grow status');
        }

        $growCycle->update(['grow_status' => $growStatus]);
        Event::dispatch(new GrowCycleGrowStatusUpdated($growCycle));
        // Update grow status every plants of this grow cycle
        $growCycle->plants()->update(['grow_status' => $growStatus]);

        return $growCycle;
    }

    /**
     * @param $licenseId
     * @return mixed
     */
    public function getAvailableItems($licenseId)
    {
        return $this->model
            ->where('license_id', $licenseId)
            ->get(['id', 'name'])
            ->each->setAppends([]);
    }

    /**
     * @param Enumerable $growCycles
     * @param Room $room
     * @param Subroom|null $subroom
     * @return Enumerable
     * @throws Throwable
     */
    public function moveToRoom(Enumerable $growCycles, Room $room, Subroom $subroom = null): Enumerable
    {
        try {
            DB::beginTransaction();
            $groupIds = [];
            $growCycleIterator = $growCycles->getIterator();
            while ($growCycleIterator->valid()) {
                $growCycle = $growCycleIterator->current();
                $plantGroups = $growCycle->plantGroups;
                $groupIds = array_merge($groupIds, $plantGroups->pluck('id')->all());
                /** @var Collection $plantGroups */
                foreach ($plantGroups->where('room_id', '!=', $room->id) as $plantGroup) {
                    $plantGroup->room()->associate($room)->save();
                    // dispatch plant group update room
                    Event::dispatch(new PlantGroupToRoomMoved($plantGroup));
                }
                $growCycleIterator->next();
            }
            Plant::whereIn('plant_group_id', $groupIds)->whereNull(['harvested_at', 'destroyed_at'])
                ->update(['room_id' => $room->id, 'subroom_id' => $subroom ? $subroom->id : null]);
            DB::commit();
            return $growCycles;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }
}
