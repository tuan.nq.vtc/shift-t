<?php

namespace App\Models;

use App\Models\Traits\Scope\{AccountHolderFilterable, IdsFilterable, NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, Relations\HasMany, Relations\MorphOne, SoftDeletes};

class Category extends BaseModel
{
    use IdsFilterable;
    use NameFilterable;
    use StatusFilterable;
    use AccountHolderFilterable;

    use SoftDeletes;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $fillable = [
        'account_holder_id',
        'name',
        'parent_id',
        'path',
        'path_name',
        'description',
        'status',
    ];

    /**
     * Self reference relation
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(EndProduct::class);
    }

    /**
     * @return MorphOne
     */
    public function taxonomy(): MorphOne
    {
        return $this->morphOne(Taxonomy::class, 'category');
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        if (empty($value)) {
            return $query;
        }

        return $query->where('name', 'like', '%' . $value . '%');
    }
}
