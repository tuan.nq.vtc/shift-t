<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\{EndProduct, License};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\{Collection, Facades\Http};

/**
 * Class SalesService
 *
 * @package App\Services
 */
class SalesService
{
    private string $baseUrl;
    private string $productPath;
    private PendingRequest $request;
    private ?License $license = null;
    protected string $stateCode = '';
    protected string $accountHolderId = '';

    public function __construct()
    {
        $this->baseUrl = config('app.services.sales.base_url');
        $this->productPath = config('app.services.sales.paths.products');
        # TODO: Remove verify when on prod env
        $this->request = Http::withoutVerifying()->baseUrl($this->baseUrl);
        $this->setState(request()->get('license_id'));
    }

    public function setState($id)
    {
        if (empty($this->license) || $this->license->getKey() !== $id) {
            $this->license = License::query()->where('id', $id)->first();
        }

        $this->stateCode = optional($this->license)->state_code ?? '';
        $this->accountHolderId = optional($this->license)->account_holder_id ?? '';

        return $this;
    }

    /**
     * @param $id
     * @return mixed|array
     * @throws GeneralException
     */
    public function fetchProduct($id): array
    {
        $response = $this->request->get($this->productPath . $id . '/' . $this->stateCode . '/' . $this->accountHolderId);
        if ($response->failed()) {
            throw new GeneralException(__('exceptions.sales_service.fetch_product_failed'));
        }

        return $response->json();
    }

    /**
     * @param $id
     * @param $quantity
     * @return mixed|array
     * @throws GeneralException
     */
    public function changeProductQuantity($id, $quantity): array
    {
        $response = $this->request->post(
            $this->productPath . $id . '/' . $this->stateCode . '/' . $this->accountHolderId,
            ['quantity' => $quantity]
        );
        if ($response->failed()) {
            throw new GeneralException(__('exceptions.sales_service.update_product_failed'));
        }

        return $response->json();
    }

    /**
     * @param array $productIds
     * @return Collection
     */
    public function getProductsByIds(array $productIds = []): Collection
    {
        if (count($productIds) === 0) {
            return collect();
        }

        $response = $this->request->post(
            $this->productPath . $this->stateCode . '/' . $this->accountHolderId,
            ['ids' => $productIds]
        );

        return $this->collect($response->failed() ? [] : $response->json(), EndProduct::class);
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getProducts(array $filters = []): Collection
    {
        $response = $this->request->post(
            $this->productPath . $this->stateCode . '/' . $this->accountHolderId,
            ['filter' => $filters]
        );

        return $this->collect($response->failed() ? [] : $response->json(), EndProduct::class);
    }

    /**
     * @param array $list
     * @param string $class
     *
     * @return Collection
     */
    private function collect(array $list, string $class): Collection
    {
        $collection = collect();
        foreach ($list as $item) {
            /** @var Model $instance */
            $instance = new $class();
            $instance->forceFill($item);
            $collection->put($instance->id, $instance);
        }

        return $collection;
    }
}
