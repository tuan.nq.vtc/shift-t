<?php

namespace App\Services\Traits;

use App\Exceptions\GeneralException;
use App\Models\{GrowCycle, Plant, PlantGroup, Room, SeedToSale, Subroom};
use Illuminate\Support\Enumerable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

trait PlantHandler
{
    /**
     * @param Plant $plant
     *
     * @return self
     *
     * @throws GeneralException
     */
    protected function validatePlant(Plant $plant): self
    {
        if ($plant->destroyed) {
            throw new GeneralException(__('exceptions.plants.already_destroyed'));
        }

        return $this;
    }

    /**
     * @param Enumerable $plants
     * @param Room $room
     * @param Subroom|null $subroom
     *
     * @return Enumerable|Plant[]
     *
     * @throws Throwable
     */
    public function movePlantsToRoom(Enumerable $plants, Room $room, Subroom $subroom = null): Enumerable
    {
        try {
            DB::beginTransaction();

            $plants = $plants->map(
                fn(Plant $plant) => $this->validatePlant($plant)->movePlantToRoom($plant, $room, $subroom)
            );
            DB::commit();

            return $plants;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param Plant $plant
     * @param Room $room
     * @param Subroom|null $subroom
     *
     * @return Plant
     *
     * @throws Throwable
     */
    public function movePlantToRoom(Plant $plant, Room $room, Subroom $subroom = null): Plant
    {
        try {
            if ($plant->room_id == $room->id) {
                if ($subroom instanceof Subroom) {
                    DB::beginTransaction();
                    $plant->subroom()->associate($subroom)->save();
                    DB::commit();
                }
                return $plant;
            }

            DB::beginTransaction();
            /*
             * Check grow cycle & plant group
             * Grow cycle has any plant group belongs to current plant strain
             * and this plant group is on the new room
             */
            $plantGroup = null;
            if ($plant->growCycle instanceof GrowCycle && $plant->growCycle->plantGroups->isNotEmpty()) {
                $plantGroup = $plant->growCycle->plantGroups->first(
                    fn(PlantGroup $pg) => $pg->strain_id == $plant->strain_id && $pg->room_id == $room->id
                );
            }
            // There's no plant group of current plant grow cycle that matched with the above conditions
            if (!$plantGroup) {
                // Create a new plant group (meaning create a new plant/vegetation batch)
                $plantGroup = $this->createPlantGroup(
                    $plant->license,
                    $plant->strain,
                    $room,
                    $plant->motherPropagation,
                    $plant->growCycle,
                );
            }
            $plant->plantGroup()->associate($plantGroup)
                ->room()->associate($room)
                ->subroom()->associate($subroom);
            $plant->save();
            DB::commit();

            return $plant->fresh(['strain', 'room', 'growCycle', 'plantGroup', 'motherPropagation', 'propagations']);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.plants.move_room_failed'));
        }
    }

    /**
     * @param Enumerable $plants
     * @param string $growStatus
     *
     * @return Enumerable|Plant[]
     *
     * @throws Throwable
     */
    public function updatePlantsGrowStatus(Enumerable $plants, string $growStatus): Enumerable
    {
        try {
            DB::beginTransaction();
            $plants = $plants->map(
                fn(Plant $plant) => $this->validatePlant($plant)->updatePlantGrowStatus($plant, $growStatus)
            );
            DB::commit();

            return $plants;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param Plant $plant
     * @param string $growStatus
     *
     * @return Plant
     *
     * @throws Throwable
     */
    public function updatePlantGrowStatus(Plant $plant, string $growStatus): Plant
    {
        if (!in_array($growStatus, SeedToSale::GROW_CYCLE_GROW_STATUSES)) {
            throw new GeneralException('Invalid grow status');
        }

        if (!$plant->update(['grow_status' => $growStatus])) {
            throw new GeneralException(__('exceptions.plants.update_grow_status_failed'));
        }

        return $plant;
    }
}
