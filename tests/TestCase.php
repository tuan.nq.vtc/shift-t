<?php

namespace Tests;

use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Tests\Concerns\DatabaseTransactions;
use Tests\Concerns\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use DatabaseTransactions;

    /**
     * If true, setup has run at least once.
     *
     * @var boolean
     */
    protected static $setUpRun = false;

    /**
     * Set up the test.
     */
    protected function setUp(): void
    {
        parent::setUp();

        if (!static::$setUpRun) {
            Artisan::call('migrate:fresh');
            Artisan::call(
                'db:seed', ['--class' => 'DatabaseSeeder']
            );
            static::$setUpRun = true;
        }

        $uses = array_flip(class_uses_recursive(get_class($this)));

        if (isset($uses[WithFaker::class])) {
            $this->setUpFaker();
        }

        $this->beginDatabaseTransaction();
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
