<?php

namespace App\Models;

use App\Models\Traits\Scope\{NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FlowerType
 * @package App\Models
 */
class FlowerType extends BaseModel
{
    use SoftDeletes;
    use NameFilterable;
    use StatusFilterable;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    protected $fillable = [
        'name',
        'status',
    ];
}
