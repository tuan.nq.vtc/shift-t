<?php


namespace App\Notification\Listeners;


use App\Jobs\BroadcastingResource;
use App\Models\{Notification, TraceableModel, User};
use App\Notification\Contracts\NotificationInterface;
use App\Notification\Events\{SyncingCompletedEvent};
use Illuminate\Events\Dispatcher;

class SyncingCompletedListener
{
    /**
     * @param SyncingCompletedEvent $event
     */
    public function handle(SyncingCompletedEvent $event)
    {
        $traceableResource = $event->traceableModel;
        if (
            in_array(
                $traceableResource->sync_status,
                [TraceableModel::SYNC_STATUS_SYNCED, TraceableModel::SYNC_STATUS_FAILED]
            ) &&
            $traceableResource instanceof NotificationInterface &&
            $event->actor
        ) {
            dispatch(
                new BroadcastingResource(
                    $traceableResource,
                    $event->actor,
                    $event->action,
                    $traceableResource->sync_status === TraceableModel::SYNC_STATUS_SYNCED ? Notification::ALARM_TYPE_INFO : Notification::ALARM_TYPE_ERROR,
                    $traceableResource->sync_status === TraceableModel::SYNC_STATUS_SYNCED
                )
            );
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            SyncingCompletedEvent::class,
            'App\Notification\Listeners\SyncingCompletedListener@handle'
        );
    }
}
