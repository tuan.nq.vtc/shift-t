<?php

/** @var Factory $factory */

use App\Models\{LabTest};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    LabTest::class,
    function (Faker $faker) {
        return [
            "license_id" => $faker->uuid,
            'name' => $faker->words(2, true),
        ];
    }
);
