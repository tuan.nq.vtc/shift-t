<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'inventory_types',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                // Belongs to a category that means item also belongs to a type
                $table->uuid('state_category_id');
                // May belong to a strain when the category is strain required
                $table->uuid('strain_id')->nullable();
                $table->string('name');
                $table->unsignedTinyInteger('status')->default(1);
                $table->string('uom', 15)->nullable();
                $table->json('options')->nullable();
                $table->json('info')->nullable();

                // S2S Trace sync[Create/Update/Delete]
                $table->string('sync_code')->nullable()->index();
                $table->unsignedTinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('state_category_id')->on('state_categories')->references('id');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_types');
    }
}
