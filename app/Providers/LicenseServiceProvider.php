<?php

namespace App\Providers;

use App\Services\{AdditiveInventoryService,
    AdditiveService,
    BatchService,
    ClientService,
    ConversionService,
    DisposalService,
    DriverService,
    EndProductService,
    GrowCycleService,
    HarvestGroupService,
    HarvestService,
    InventoryService,
    InventoryTypeService,
    LabTestService,
    ManifestService,
    PlantService,
    PropagationService,
    QASampleService,
    RoomService,
    SourceCategoryService,
    SourceProductService,
    StateCategoryService,
    StrainService,
    TaxonomyService,
    VehicleService,
    WasteBagService
};
use Illuminate\Support\ServiceProvider;

class LicenseServiceProvider extends ServiceProvider
{
    protected static array $licenseServices = [
        AdditiveInventoryService::class,
        AdditiveService::class,
        BatchService::class,
        ClientService::class,
        DisposalService::class,
        GrowCycleService::class,
        HarvestGroupService::class,
        HarvestService::class,
        InventoryService::class,
        InventoryTypeService::class,
        LabTestService::class,
        PlantService::class,
        PropagationService::class,
        QASampleService::class,
        RoomService::class,
        StrainService::class,
        WasteBagService::class,
        SourceProductService::class,
        SourceCategoryService::class,
        StateCategoryService::class,
        TaxonomyService::class,
        ConversionService::class,
        DriverService::class,
        VehicleService::class,
        EndProductService::class,
        ManifestService::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (static::$licenseServices as $licenseService) {
            $this->app->resolving(
                $licenseService,
                function ($transistor) {
                    if ($licenseId = request('license_id')) {
                        $transistor->setLicenseId($licenseId);
                    }
                }
            );
        }
    }
}
