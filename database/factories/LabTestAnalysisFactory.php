<?php

/** @var Factory $factory */

use App\Models\{LabTestAnalysis};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    LabTestAnalysis::class,
    function (Faker $faker) {
        return [
            'regulator' => $faker->randomElement(['metrc', 'leaf']),
            'name' => $faker->name,
            'code' => $faker->postcode,
            'is_required' => $faker->randomElement([0, 1]),
        ];
    }
);
