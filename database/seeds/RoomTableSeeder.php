<?php

use App\Models\{License, Room, RoomType, User};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

/**
 * Class RoomTableSeeder.
 */
class RoomTableSeeder extends Seeder
{
    use DisableForeignKeys;
    private ?Collection $users = null;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
        $this->enableForeignKeys();
    }

    protected function iterateLicense(License $license)
    {
        // License name format: "{License Type} License {State Code}"
        if (preg_match("/(.*)\sLicense\s(\w{2})/", $license->name, $matches)) {
            $licenseType = $matches[1];
            if (in_array($licenseType, ['Producer', 'Producer/Processor'])) {
                $producerTypes = [
                    'Cultivation',
                    'Seedling',
                    'Flowering',
                    'Harvest',
                    'Drying',
                    'Quarantine',
                    'Mothers',
                    'Preveg',
                    'Clones',
                    'Waste',
                    'Disposal',
                ];
            } else {
                $producerTypes = ['Inventory'];
            }

            foreach (RoomType::whereIn('name', $producerTypes)->get() as $roomType) {
                factory(Room::class)->create(
                    [
                        'name' => "{$roomType->name} {$licenseType}",
                        'room_type_id' => $roomType->id,
                        'status' => Room::STATUS_ENABLED,
                        'license_id' => $license->id,
                        'created_by' => $this->getRandomUserId(),
                        'updated_by' => $this->getRandomUserId(),
                    ]
                );
            }
        }
    }

    private function getRandomUserId(): ?string
    {
        if (!$this->users) {
            $this->users = User::all();
        }
        if ($this->users->count() === 0) {
            return null;
        }

        return $this->users->random()->id;
    }
}
