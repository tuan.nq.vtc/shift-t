<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Models\{Additive, AdditiveOperation, Supplier};
use App\Rules\{AdditiveQuantity, ModelsExist};
use App\Services\{AdditiveInventoryService,
    AdditiveService,
    GrowCycleService,
    PlantService,
    PropagationService,
    RoomService
};
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Arr;
use Illuminate\Validation\Rules\In;

/**
 * Class AdditiveController
 *
 * @package App\Http\Controllers
 *
 * @property AdditiveService $service
 */
class AdditiveController extends CRUDController
{
    protected AdditiveInventoryService $additiveInventoryService;
    protected PropagationService $propagationService;
    protected PlantService             $plantService;
    protected RoomService $roomService;
    protected GrowCycleService $growCycleService;
    protected string $type;

    /**
     * AdditiveController constructor.
     *
     * @param AdditiveService $service
     * @param AdditiveInventoryService $additiveInventoryService
     * @param PlantService $plantService
     * @param RoomService $roomService
     * @param GrowCycleService $growCycleService
     * @param PropagationService $propagationService
     * @param Request $request
     * @param                          $type
     */
    public function __construct(
        AdditiveService $service,
        AdditiveInventoryService $additiveInventoryService,
        PlantService $plantService,
        RoomService $roomService,
        GrowCycleService $growCycleService,
        PropagationService $propagationService,
        Request $request,
        $type
    ) {
        $request->merge(
            [
                'filter' => array_merge($request->get('filter', []), ['type' => $type])
            ]
        );
        $request->merge(['type' => $type]);
        parent::__construct($service, $request);
        $this->additiveInventoryService = $additiveInventoryService;
        $this->plantService = $plantService;
        $this->roomService = $roomService;
        $this->growCycleService = $growCycleService;
        $this->propagationService = $propagationService;
        $this->type = $type;
    }

    public function bulkCreate()
    {
        return $this->renderResponse(
            $this->service->bulkCreate(
                $this->validate(
                    $this->request,
                    $this->getCreateRules(),
                    [],
                    [
                        'additives.*.name' => 'name',
                        'additives.*.epa_regulation_number' => 'EPA regulation number',
                        'additives.*.uom' => 'UOM',
                        'additives.*.additive_inventory.supplier_id' => 'supplier',
                        'additives.*.additive_inventory.total_quantity' => 'quantity',
                        'additives.*.type' => 'type',
                    ]
                ),
                $this->type
            )
        );
    }

    /**
     * @return array
     */
    public function getCreateRules(): array
    {
        return [
            'additives' => [
                'required',
                'array'
            ],
            'additives.*.name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'distinct',
                'unique:App\Models\Additive,name,null,id,license_id,'.$this->request->get(
                    'license_id'
                ).",type,".$this->type,
            ],
            'additives.*.epa_regulation_number' => ['nullable', 'string', 'min:3', 'max:255'],
            'additives.*.uom' => [
                'required',
                'string',
                new In([Additive::UOM_GALLON, Additive::UOM_LITER, Additive::UOM_OUNCE]),
            ],
            'additives.*.additive_inventory.supplier_id' => ['required', new ModelsExist(Supplier::class)],
            'additives.*.additive_inventory.total_quantity' => ['required', 'numeric', 'min:0', 'not_in:0'],
            'additives.*.type' => ['required', new In(Additive::TYPES)],
            'additives.*.ingredients' => $this->getIngredientRule(),
            'license_id' => ['required'],
            'user_id' => ['required'],
        ];
    }

    private function getIngredientRule()
    {
        return [
            'array',
            function ($attribute, $value, $fail) {
                $ingredients = Arr::wrap($value);
                $errmsg = '';
                $totalPercentage = 0;
                foreach ($ingredients as $ingredient) {
                    if (!is_array($ingredient) ||
                        !isset($ingredient['name']) || !isset($ingredient['percentage'])) {
                        $errmsg = "The ingredient's value has incorrect format";
                        break;
                    }
                    if (blank($ingredient['name'])) {
                        $errmsg = "The ingredient's name can't blank";
                        break;
                    }
                    $totalPercentage += (double)$ingredient['percentage'];
                }
                if ($totalPercentage > 100 || $totalPercentage < 0) {
                    $errmsg = "Total of ingredient's percentage must be between 0 and 100";
                }
                if (!blank($errmsg)) {
                    return $fail($errmsg);
                }
            }
        ];
    }

    /**
     * @param string $id
     *
     * @return array
     */
    public function getUpdateRules(string $id): array
    {
        return [
            'visibility' => ['sometimes', 'boolean'],
            'epa_regulation_number' => ['nullable', 'string', 'min:3', 'max:255'],
            'ingredients' => $this->getIngredientRule(),
            'is_other_type' => ['sometimes', 'boolean'],
            'type' => ['sometimes', new In(Additive::TYPES)],
            'user_id' => ['uuid']
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(
            $this->service->getAvailableItems($this->request->get('license_id'), $this->type),
            Response::HTTP_OK
        );
    }

    /**
     * @param string $target
     *
     * @return JsonResponse
     * @throws GeneralException
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function apply(string $target): JsonResponse
    {
        $modelClass = $this->getImpactObject($target);
        $rules = $this->getApplyRules($modelClass);
        $data = $this->validate($this->request, $rules);

        $targetEnumerable = $this->getEnumerableBaseOnTarget($target, Arr::get($data, 'ids', []));
        return $this->renderResponse(
            $this->service->applyOnTargets(
                $targetEnumerable,
                $data['additive_inventories'],
                Arr::only(
                    $data,
                    ['license_id', 'applied_date', 'application_device', 'applied_by', 'user_id']
                )
            )
        );
    }

    /**
     * @param string $target
     *
     * @return string
     * @throws GeneralException
     */
    protected function getImpactObject(string $target)
    {
        $mappings = [
            AdditiveOperation::PROPAGATION_OPERATION => 'App\Models\Propagation',
            AdditiveOperation::GROW_CYCLE_OPERATION => 'App\Models\GrowCycle',
            AdditiveOperation::ROOM_OPERATION => 'App\Models\Room',
            AdditiveOperation::PLANT_OPERATION => 'App\Models\Plant',
        ];
        $modelClass = $mappings[$target] ?? null;
        if (!$modelClass) {
            throw new GeneralException(__('exceptions.additive.invalid_apply_type', ['type' => $target]));
        }

        return $modelClass;
    }

    protected function getApplyRules(string $modelClass)
    {
        $licenseId = $this->request->get('license_id', '');
        return [
            'license_id' => ['required', 'uuid'],
            'ids' => [
                'required',
                'array',
                new ModelsExist($modelClass, 'id', ['license_id' => $licenseId]),
            ],
            'applied_date' => [
                'required',
                'date_format:Y-m-d H:i:s',
            ],
            'additive_inventories' => ['required', 'array'],
            'additive_inventories.*' => [
                new AdditiveQuantity($licenseId),
            ],
            'additive_inventories.*.id' => [
                'required',
                new ModelsExist('App\Models\AdditiveInventory', 'id', ['license_id' => $licenseId]),
            ],
            'additive_inventory.*.applied_quantity' => [
                'required',
                'numeric',
                'gt:0',

            ],
            'application_device' => ['string', 'sometimes'],
            'applied_by' => ['required', new ModelsExist('App\Models\User'),],
            'user_id' => ['required'],
        ];
    }

    /**
     * @param        $target
     * @param array $ids
     *
     * @param string $additive
     *
     * @return GeneralException|\App\Models\GrowCycle[]|\App\Models\Plant[]|\Illuminate\Support\Enumerable
     */
    protected function getEnumerableBaseOnTarget($target, array $ids, $additive = 'pesticide')
    {
        switch ($target) {
            case AdditiveOperation::GROW_CYCLE_OPERATION:
                return $this->growCycleService->getEnumerable($ids, ['plants']);
            case AdditiveOperation::ROOM_OPERATION:
                return $this->roomService->getEnumerable($ids, ['plants']);
            case AdditiveOperation::PLANT_OPERATION:
                return $this->plantService->getEnumerable($ids);
            case AdditiveOperation::PROPAGATION_OPERATION:
                return $this->propagationService->getEnumerable($ids);
            default:
                return new GeneralException(__('exceptions.'.$additive.'.invalid_apply_target', compact('target')));
        }
    }

    /**
     * @param Request $request
     *
     * @return Request
     */
    protected function setOtherTypeForRequest(Request $request)
    {
        switch ($request->get('type', '')) {
            case Additive::TYPE_OTHER:
                $request->merge(['is_other_type' => true]);
                break;
            case Additive::TYPE_PESTICIDE:
            case Additive::TYPE_NUTRIENT:
                $request->merge(['is_other_type' => false]);
                break;
        }
        return $request;
    }

}
