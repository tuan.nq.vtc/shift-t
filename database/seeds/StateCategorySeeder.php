<?php

use App\Models\SeedToSale;
use App\Models\State;
use App\Models\StateCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class StateCategorySeeder extends Seeder
{
    private const CCRS_CATEGORIES = [
        [
            'code' => 'EndProduct',
            'name' => 'End Product',
            'type' => StateCategory::TYPE_END,
            'children' => [
                [
                    'code' => 'Capsule',
                    'name' => 'Capsule',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Solid Edible',
                    'name' => 'Solid Edible',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Tincture',
                    'name' => 'Tincture',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Liquid Edible',
                    'name' => 'Liquid Edible',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Transdermal',
                    'name' => 'Transdermal',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Topical Ointment',
                    'name' => 'Topical Ointment',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Marijuana Mix Packaged',
                    'name' => 'Marijuana Mix Packaged',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                ],
                [
                    'code' => 'Marijuana Mix Infused',
                    'name' => 'Marijuana Mix Infused',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],
                [
                    'code' => 'Suppository',
                    'name' => 'Suppository',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],      
                [
                    'code' => 'Usable Marijuana',
                    'name' => 'Usable Marijuana',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],       
                [
                    'code' => 'Sample Jar',
                    'name' => 'Sample Jar',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],     
                [
                    'code' => 'Waste',
                    'name' => 'Waste',
                    'type' => StateCategory::TYPE_END,
                    'uom' => SeedToSale::UOM_EACH,
                    'is_serving_required' => true,
                    'is_weight_per_unit_required' => true,
                ],                                 
            ],
        ],
        [
            'code' => 'IntermediateProduct',
            'name' => 'Intermediate Product',
            'type' => StateCategory::TYPE_SOURCE,
            'children' => [
                [
                    'code' => 'Marijuana Mix',
                    'name' => 'Marijuana Mix',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Concentrate for Inhalation',
                    'name' => 'Concentrate for Inhalation',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Non-Solvent based Concentrate',
                    'name' => 'Non-Solvent based Concentrate',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Hydrocarbon Concentrate',
                    'name' => 'Hydrocarbon Concentrate',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'CO2 Concentrate',
                    'name' => 'CO2 Concentrate',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Ethanol Concentrate',
                    'name' => 'Ethanol Concentrate',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Food Grade Solvent Concentrate',
                    'name' => 'Food Grade Solvent Concentrate',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Infused Cooking Medium',
                    'name' => 'Infused Cooking Medium',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'CBD',
                    'name' => 'CBD',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Waste',
                    'name' => 'Waste',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
            ],
        ],
        [
            'code' => 'HarvestedMaterial',
            'name' => 'Harvested Material',
            'type' => StateCategory::TYPE_SOURCE,
            'children' => [
                [
                    'code' => 'Wet Flower',
                    'name' => 'Wet Flower',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Wet Other Material',
                    'name' => 'Wet Other Material',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Flower Unlotted',
                    'name' => 'Flower Unlotted',
                    'type' => StateCategory::TYPE_UNLOTTED,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Flower Lot',
                    'name' => 'Flower Lot',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Other Material Unlotted',
                    'name' => 'Other Material Unlotted',
                    'type' => StateCategory::TYPE_UNLOTTED,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Other Material Lot',
                    'name' => 'Other Material Lot',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Waste',
                    'name' => 'Waste',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
                [
                    'code' => 'Marijuana Mix',
                    'name' => 'Marijuana Mix',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_GRAMS,
                ],
            ],
        ],
        [
            'code' => 'PropagationMaterial',
            'name' => 'Propagation Material',
            'type' => StateCategory::TYPE_SOURCE,
            'children' => [
                [
                    'code' => 'Seed',
                    'name' => 'Seed',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_EACH,
                ],
                [
                    'code' => 'Plant',
                    'name' => 'Plant',
                    'type' => StateCategory::TYPE_SOURCE,
                    'uom' => SeedToSale::UOM_EACH,
                ],
            ],
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ccrsStates = [State::WASHINGTON];

        foreach ($ccrsStates as $ccrsState) {
            $this->createCategories(
                self::CCRS_CATEGORIES,
                null,
                [
                    'regulator' => State::REGULATOR_CCRS,
                    'state_code' => $ccrsState,
                ]
            );
        }
    }

    /**
     * @param array $categories
     * @param StateCategory|null $parent
     * @param array $generalData
     */
    private function createCategories(array $categories, ?StateCategory $parent = null, array $generalData = []): void
    {
        foreach ($categories as $categoryData) {
            $children = [];
            if (isset($categoryData['children'])) {
                $children = $categoryData['children'];
                unset($categoryData['children']);
            }
            if ($parent) {
                $categoryData['parent_id'] = $parent->getKey();
                $stateCategoryQuery = StateCategory::where('parent_id', $parent->getKey());
                $categoryData['name'] = $parent->name . ' - ' . $categoryData['name'];
            } else {
                $stateCategoryQuery = StateCategory::whereNull('parent_id');
            }
            if (!empty($generalData)) {
                $categoryData = array_merge($categoryData, $generalData);
            }
            /** @var StateCategory $category */
            $category = $stateCategoryQuery->updateOrCreate(Arr::only($categoryData, ['code']), $categoryData);

            if (!empty($children)) {
                $this->createCategories($children, $category, $generalData);
            }
        }
    }
}
