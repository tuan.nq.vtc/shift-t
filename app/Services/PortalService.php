<?php

namespace App\Services;

use App\Models\{License, User};
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\{Collection, Facades\Http};

/**
 * Class PortalService
 *
 * @package App\Services
 */
class PortalService
{
    private string $baseUrl;
    private string $companyPath;
    private string $licenseTypePath;
    private string $licensePath;
    private string $userPath;
    private string $organizationPath;
    private string $accountHolderPath;
    private PendingRequest $request;

    public function __construct()
    {
        $this->baseUrl = config('app.services.portal.base_url');
        $this->companyPath = config('app.services.portal.paths.company');
        $this->licenseTypePath = config('app.services.portal.paths.license_type');
        $this->licensePath = config('app.services.portal.paths.license');
        $this->userPath = config('app.services.portal.paths.user');
        $this->organizationPath = config('app.services.portal.paths.organizations');
        $this->accountHolderPath = config('app.services.portal.paths.account_holders');
        # TODO: Remove verify when on prod env
        $this->request = Http::withoutVerifying()->baseUrl($this->baseUrl);
    }

    /**
     * @return Collection
     *
     * @throws Exception
     */
    public function fetchAllLicenses(): Collection
    {
        return $this->collect($this->allLicenses(), License::class);
    }

    /**
     * @param array $list
     * @param string $class
     *
     * @return Collection
     */
    private function collect(array $list, string $class): Collection
    {
        $collection = collect();
        foreach ($list as $item) {
            /** @var Model $instance */
            $instance = new $class();
            $instance->forceFill($item);
            $collection->put($instance->id, $instance);
        }

        return $collection;
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function allLicenses(): array
    {
        $response = $this->request->get($this->licensePath.'all');

        if ($response->failed()) {
            throw new Exception(__('exceptions.portal_service.fetch_licenses_failed'));
        }

        return $response->json();
    }

    /**
     * @param $id
     *
     * @return License
     *
     * @throws Exception
     */
    public function fetchLicense($id): License
    {
        $response = $this->request->get($this->licensePath.$id);

        if ($response->failed()) {
            throw new Exception(__('exceptions.portal_service.fetch_license_failed'));
        }

        return new License($response->json());
    }

    /**
     * @return Collection
     *
     * @throws Exception
     */
    public function fetchAllUsers(): Collection
    {
        return $this->collect($this->allUsers(), User::class);
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function allUsers(): array
    {
        $response = $this->request->get($this->userPath . 'all');
        if ($response->failed()) {
            throw new Exception(__('exceptions.portal_service.fetch_users_failed'));
        }

        return $response->json();
    }

    /**
     * @param $id
     *
     * @return User
     *
     * @throws Exception
     */
    public function fetchUser($id): User
    {
        $response = $this->request->get($this->userPath.$id);

        if ($response->failed()) {
            throw new Exception(__('exceptions.portal_service.fetch_user_failed'));
        }

        return new User($response->json());
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function allOrganizationUsers($id): array
    {
        $response = $this->request->get($this->organizationPath . "$id/users");
        if ($response->failed()) {
            throw new Exception(__('exceptions.portal_service.fetch_organization_users_failed'));
        }

        return $response->json();
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function allAccountHolderUsers($id): array
    {
        $response = $this->request->get($this->accountHolderPath . "$id/users");
        if ($response->failed()) {
            throw new Exception(__('exceptions.portal_service.fetch_account_holder_users_failed'));
        }

        return $response->json();
    }
}
