<?php

namespace App\Streams\ConsumerEventProcessors;

use App\Models\User;

class UserProcessor extends BaseProcessor
{
    protected static string $modelName = User::class;
}
