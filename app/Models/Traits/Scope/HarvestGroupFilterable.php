<?php

namespace App\Models\Traits\Scope;

trait HarvestGroupFilterable
{
    /**
     * @param $query
     * @param $harvestGroupId
     * @return mixed
     */
    public function scopeHarvestGroupId($query, $harvestGroupId)
    {
        return $query->where('harvest_group_id', $harvestGroupId);
    }
}
