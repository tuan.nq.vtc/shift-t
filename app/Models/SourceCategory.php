<?php

namespace App\Models;

use App\Models\Traits\{HasInternalId, WithoutEvents, WithPath};
use App\Models\Traits\Scope\{AccountHolderFilterable,
    IdsFilterable,
    NameFilterable,
    StatusFilterable};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, Relations\HasMany, Relations\MorphOne, SoftDeletes};
use Illuminate\Support\Arr;

/**
 * @property Taxonomy taxonomy
 */
class SourceCategory extends BaseModel
{
    use IdsFilterable;
    use NameFilterable;
    use StatusFilterable;
    use AccountHolderFilterable;
    use WithPath;
    use WithoutEvents;
    use SoftDeletes;
    use HasInternalId;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const PARENT_ROOT_ID = 0;

    protected $fillable = [
        'account_holder_id',
        'name',
        'parent_id',
        'path',
        'path_name',
        'description',
        'status',
    ];

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::created(
            function (self $category): void {
                /** @var SourceCategory $category */
                $category = static::setPathName($category);
                $category = static::setPath($category);
                $category->saveWithoutEvents();
            }
        );
        static::updating(
            function (self $category): void {
                if (Arr::hasAny($category->getDirty(), ['name', 'parent_id'])) {
                    static::setPathName($category);
                }
            }
        );
        static::updated(
            function (self $category): void {
                static::updatePathWithChild($category);
            }
        );
        parent::booted();
    }

    /**
     * Self reference relation
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function sourceProducts(): HasMany
    {
        return $this->hasMany(SourceProduct::class);
    }

    public function scopeParentId($query, $value)
    {
        return $value === self::PARENT_ROOT_ID ? $query->whereNull('parent_id') : $query->where('parent_id', $value);
    }

    /**
     * @return MorphOne
     */
    public function taxonomy(): MorphOne
    {
        return $this->morphOne(Taxonomy::class, 'category');
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query->where(
            fn(Builder $subQuery) => $subQuery->where('name', 'like', '%' . $value . '%')
                ->orWhere('description', 'like', '%' . $value . '%')
                ->orWhere('pathname', 'like', '%' . $value . '%')
        );
    }
}
