<?php

return [
    \App\Models\Batch::class => \App\Synchronizations\Logic\Processes\Metrc\BatchProcess::class,
    \App\Models\Strain::class => \App\Synchronizations\Logic\Processes\Metrc\StrainProcess::class,
    \App\Models\Room::class => \App\Synchronizations\Logic\Processes\Metrc\LocationProcess::class,
    \App\Models\Propagation::class => \App\Synchronizations\Logic\Processes\Metrc\PropagationProcess::class,
    \App\Models\InventoryType::class => \App\Synchronizations\Logic\Processes\Metrc\InventoryTypeProcess::class,
    \App\Models\Harvest::class => \App\Synchronizations\Logic\Processes\Metrc\HarvestProcess::class,
];
