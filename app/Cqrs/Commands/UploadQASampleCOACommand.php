<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Executable;
use Illuminate\Http\UploadedFile;

class UploadQASampleCOACommand extends Executable
{
    /**
     * @var UploadedFile|null
     */
    protected $uploadedFile;

    /**
     * @var string|null
     */
    protected $userId;

    /**
     * @var string|null
     */
    protected $qaSampleId;

    /**
     * @var string[]
     */
    protected $mapping = [
        'uploadedFile' => 'uploaded_file',
        'userId|string' => 'user_id',
        'qaSampleId|string' => 'qa_sample_id'
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'userId' => $this->userId,
            'qaSampleId' => $this->qaSampleId,
            'uploadedFile' => $this->uploadedFile,
        ];
    }

    /**
     * @return string|null
     */
    public function getQaSampleId(): ?string
    {
        return $this->qaSampleId;
    }

    /**
     * @return UploadedFile|null
     */
    public function getUploadedFile(): ?UploadedFile
    {
        return $this->uploadedFile;
    }

    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
}
