<?php

return [
    \App\Models\Strain::class => \App\Synchronizations\Logic\Processes\Leaf\StrainProcess::class,
    \App\Models\Room::class => \App\Synchronizations\Logic\Processes\Leaf\RoomProcess::class,
    \App\Models\InventoryType::class => \App\Synchronizations\Logic\Processes\Leaf\InventoryTypeProcess::class,
    \App\Models\Propagation::class => \App\Synchronizations\Logic\Processes\Leaf\PropagationProcess::class,
    \App\Models\Harvest::class => \App\Synchronizations\Logic\Processes\Leaf\HarvestProcess::class,
    \App\Models\PlantGroup::class => \App\Synchronizations\Logic\Processes\Leaf\PlantGroupProcess::class,
    \App\Models\Plant::class => \App\Synchronizations\Logic\Processes\Leaf\PlantProcess::class,
    \App\Models\Disposal::class => \App\Synchronizations\Logic\Processes\Leaf\DisposalProcess::class,
    \App\Models\Inventory::class => \App\Synchronizations\Logic\Processes\Leaf\InventoryProcess::class,
    \App\Models\QASampleResult::class => \App\Synchronizations\Logic\Processes\Leaf\QASampleResultProcess::class,
];
