<?php

namespace App\Notification\Listeners;

use App\Notification\Contracts\PusherHandlerInterface;
use App\Notification\Events\PushNotificationEvent;
use Illuminate\Events\Dispatcher;

class PushNotificationListener
{

    public function handle(PushNotificationEvent $event)
    {
        $handler = app()->make(PusherHandlerInterface::class);
        $handler->pushNotification($event->channel, $event->event, $event->message);
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            PushNotificationEvent::class,
            'App\Notification\Listeners\PushNotificationListener@handle'
        );
    }
}
