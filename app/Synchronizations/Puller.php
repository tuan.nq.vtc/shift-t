<?php

namespace App\Synchronizations;

use App\Models\{LicenseResource, TraceableModel};
use App\Synchronizations\Contracts\Http\{ClientInterface, ResourceInterface};
use App\Synchronizations\Exceptions\Pull\{PostSaveException, PreSaveException};
use App\Synchronizations\Http\Resources\{Leaf\BaseLeafResource,
    Leaf\PlantResource,
    Metrc\BaseMetrcResource,
    Metrc\BatchResource
};
use App\Synchronizations\Logger\Logger;
use App\Synchronizations\Resolvers\ClientResolver;
use Exception;
use Illuminate\Database\Eloquent\{Collection, Model};
use Illuminate\Http\Client\Response;
use Illuminate\Support\{Arr, LazyCollection};
use Throwable;

class Puller
{
    private ClientResolver $clientResolver;
    private Logger $logger;

    private ?LicenseResource $licenseResource;

    private array $context = [];

    private ?TraceableModel $traceableModel;
    private ?ResourceInterface $httpResource;
    private ?ClientInterface $httpClient;
    private ?array $modelMappings;


    /**
     * @param ClientResolver $clientResolver
     * @param Logger $logger
     */
    public function __construct(ClientResolver $clientResolver, Logger $logger)
    {
        $this->clientResolver = $clientResolver;
        $this->logger = $logger;
    }

    /**
     * @param LicenseResource $licenseResource
     * @param string|null $endpoint
     *
     * @throws Throwable
     */
    public function fetch(LicenseResource $licenseResource, ?string $endpoint): void
    {
        // Load necessary relations
        $this->licenseResource = $licenseResource->load(['license', 'license.state']);
        try {
            $this->initialize();

            $endpoint && $this->httpResource = $this->httpResource->setUrl($endpoint);
            $collection = null;

            if ($this->httpResource instanceof BaseLeafResource) {
                $collection = $this->fetchLeafResources();
            } elseif ($this->httpResource instanceof BaseMetrcResource) {
                $collection = $this->fetchMetrcResources();
            }

            $records = $collection ? $collection->count() : 0;
            // Set synced records to license resource
            $this->licenseResource->update(compact('records'));
            $this->logger->info('['.$records.'] license resources saved', $this->context);
        } catch (Throwable $exception) {
            $this->logger->error('Fetch license resources failed: '.$exception->getMessage(), $this->context);
            throw $exception;
        }
    }

    /**
     * @throws Throwable
     */
    private function initialize(): void
    {
        // Set default context
        $this->context = ['ID' => $this->licenseResource->id, 'Resource' => $this->licenseResource->resource];
        // Init model
        $this->traceableModel = $this->licenseResource->initModel();
        // Load model mappings. Throw exception when model has empty mappings
        $this->modelMappings = $this->getModelMappings();
        // Init http resource
        $this->httpResource = $this->licenseResource->initResource($this->traceableModel);
        // Resolve http client
        $this->httpClient = $this->clientResolver->resolve($this->licenseResource->license);
    }


    /**
     * @return Collection
     *
     * @throws Throwable
     */
    private function fetchLeafResources(): Collection
    {
        $page = 1;

        $collection = $this->traceableModel->newCollection();

        while (true) {
            $this->httpResource = $this->httpResource->addQuery('page', $page);
            $response = $this->sendRequest();
            $json = $response->json();
            throw_if(
                0 == (int)$json['total'] || empty($json['data']),
                new Exception(
                    'Empty data in response body while fetching ['.$this->licenseResource->resource.'] from LEAF'
                )
            );

            $items = new LazyCollection($json['data']);
            $this->logger->info('['.$items->count().'] license resources fetched', $this->context);

            $collection = $collection->merge($this->savedItems($items));
            // Check response has next page url
            if (!$json['next_page_url']) {
                break;
            }
            $page++;
        }

        return $collection;
    }

    /**
     * @return Collection
     *
     * @throws Throwable
     */
    private function fetchMetrcResources(): Collection
    {
        if ($this->httpResource instanceof BatchResource || $this->httpResource instanceof PlantResource) {
            return $this->fetchMetrcResourcesSequentially();
        }

        $response = $this->sendRequest();

        $items = new LazyCollection($response->json());

        $this->logger->info('['.$items->count().'] license resources fetched', $this->context);

        return $this->savedItems($items);
    }

    /**
     * @return Collection
     *
     * @throws Throwable
     */
    private function fetchMetrcResourcesSequentially(): Collection
    {
        $collection = $this->traceableModel->newCollection();
        $syncDateRange = $this->licenseResource->license->sync_date_range;
        $availableDateRanges = config('licenses.date_ranges');
        // Default sync date range is 6 months
        $dateRange = empty($availableDateRanges[$syncDateRange]) ? '-6 months' : '-'.$availableDateRanges[$syncDateRange];
        $date  = $this->licenseResource->created_at;
        $startDate = $date->modify($dateRange);
        $diffDays = $startDate->diffInDays($this->licenseResource->created_at);
        for ($idx = 0; $idx < $diffDays; $idx++) {
            $this->httpResource = $this->httpResource
                ->addQuery('lastModifiedStart', $startDate->format('Y-m-d\TH:i:s'))
                ->addQuery('lastModifiedEnd', $startDate->modify('+1 day')->format('Y-m-d\TH:i:s'));
            $response = $this->sendRequest();
            $items = new LazyCollection($response->json());
            $this->logger->info('['.$items->count().'] license resources fetched', $this->context);
            $collection = $collection->merge($this->savedItems($items));
        }

        return $collection;
    }

    /**
     * @return array
     *
     * @throws Throwable
     */
    private function getModelMappings(): array
    {
        return throw_unless(
            $this->traceableModel->getMappings(),
            new Exception('Invalid mapping fields of ['.$this->licenseResource->resource.'] resource')
        );
    }

    /**
     * @return Response
     *
     * @throws Throwable
     */
    private function sendRequest(): Response
    {
        $response = $this->httpClient->getRequest()->send(
            $this->httpResource->getMethod(),
            $this->httpResource->getUrl(),
            [
                'query' => $this->httpResource->getQuery(),
                'json' => $this->httpResource->getBody(),
            ]
        );

        $this->context['Method'] = $this->httpResource->getMethod();
        $this->context['Url'] = $this->httpResource->getUrl();
        $this->context['Query'] = $this->httpResource->getQuery();

        if ($response->successful()) {
            $this->logger->info('Fetch license resources succeed', $this->context);
        } elseif ($response->clientError()) {
            $this->logger->error('Fetch license resources failed: Client error', $this->context);
        } else {
            $this->logger->error('Fetch license resources failed: Server error', $this->context);
        }

        $response->throw();

        return $response;
    }

    /**
     * @param LazyCollection $items
     *
     * @return Collection $collection
     */
    private function savedItems(LazyCollection $items): Collection
    {
        $collection = $this->traceableModel->newCollection();
        foreach ($items as $item) {
            /** @var Model|TraceableModel $instance */
            $instance = $this->traceableModel->newInstance()->license()->associate($this->licenseResource->license);

            foreach ($this->modelMappings as $key => $attribute) {
                if (is_callable($attribute)) {
                    $instance = $attribute($instance, Arr::get($item, $key));
                } elseif (is_string($attribute) && $instance->isFillable($attribute)) {
                    $instance = $instance->setAttribute($attribute, Arr::get($item, $key));
                }
                if (is_null($instance)) {
                    continue 2;
                }
                // Removed all values have been mapped
                unset($item[$key]);
            }

            // Push rest item values to instance info attribute
            foreach ($item as $key => $value) {
                $instance = $instance->setAttribute('info->'.$key, $value);
            }
            $instance = $instance->setAttribute('sync_status', TraceableModel::SYNC_STATUS_SYNCED);
            if (!$instance->exists) {
                $instance = $instance->setAttribute($instance->getKeyName(), $instance->generateUuid());
            }
            try {
                method_exists($instance, 'preSave') && ($instance = $instance->preSave());
                if (!$instance) {
                    continue;
                }
            } catch (PreSaveException $preSaveException) {
                $this->logger->error($preSaveException->getMessage());
                continue;
            }
            $instance->saveWithoutEvents();
            try {
                method_exists($instance, 'postSave') && ($instance = $instance->postSave());
            } catch (PostSaveException $postSaveException) {
                $this->logger->error($postSaveException->getMessage());
                continue;
            }
            $collection->push($instance);
        }

        return $collection;
    }
}
