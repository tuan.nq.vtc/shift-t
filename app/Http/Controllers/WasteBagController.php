<?php

namespace App\Http\Controllers;

use App\Models\WasteBag;
use App\Services\RoomService;
use App\Services\WasteBagService;
use Illuminate\Http\{Request, Response};

/**
 * Class PropagationController
 * @package App\Http\Controllers
 */
class WasteBagController extends CRUDController
{
    protected RoomService $roomService;

    public function __construct(WasteBagService $service, RoomService $roomService, Request $request)
    {
        parent::__construct($service, $request);
        $this->roomService = $roomService;
    }

    public function getCreateRules(): array
    {
        return [
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
        ];
    }

    public function bulkCreate()
    {
        $rules = [
            'waste-bags' => ['required', 'array'],
            'waste-bags.*.weight' => ['required', 'numeric', 'between:0,100000000'],
            'waste-bags.*.wasted_at' => ['required', 'date'],
            'waste-bags.*.cut_by_id' => ['required', 'uuid', 'exists:App\Models\User,id',],
            'waste-bags.*.reason' => ['required', 'in:daily_waste'],
            'waste-bags.*.room_id' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!RoomService::isDisposal($value, $this->request->get('license_id'))) {
                        $fail('Disposal room is invalid.');
                    }
                }
            ],
            'waste-bags.*.strain_ids' => ['nullable', 'array',],
            'waste-bags.*.strain_ids.*' => [
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,' . $this->request->get('license_id') . ',deleted_at,NULL',
            ],
            'waste-bags.*.comment' => ['nullable', 'string'],
            'license_id' => ['required', 'uuid',],
        ];
        return response()->json(
            $this->service->bulkCreate($this->validate($this->request, $rules)),
            Response::HTTP_CREATED
        );
    }
}
