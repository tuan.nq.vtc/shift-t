<?php

use App\Models\Room;
use App\Models\Subroom;
use Illuminate\Database\Seeder;

/**
 * Class SubroomTableSeeder.
 */
class SubroomTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();
        $roomIteration = function (Room $room) {
            call_user_func([$this, 'iterateRoom'], $room);
        };
        Room::all()->each($roomIteration);
        $this->enableForeignKeys();
    }

    protected function iterateRoom(Room $room)
    {
        if (!($dimension = $room->dimension)) {
            return;
        }
        factory(Subroom::class)->create(
            [
                'room_id' => $room->id,
                'status' => Subroom::STATUS_ENABLED,
                'dimension' => [
                    'width' => rand(0, $dimension['width']),
                    'length' => rand(0, $dimension['length'])
                ]
            ]
        );
    }
}
