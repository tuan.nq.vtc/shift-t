<?php

return [
    \App\Models\Room::class => \App\Synchronizations\Http\Resources\Metrc\LocationResource::class,
    \App\Models\Strain::class => \App\Synchronizations\Http\Resources\Metrc\StrainResource::class,
    \App\Models\InventoryType::class => \App\Synchronizations\Http\Resources\Metrc\InventoryTypeResource::class,
    \App\Models\Propagation::class => \App\Synchronizations\Http\Resources\Metrc\PropagationResource::class,
    \App\Models\Harvest::class => \App\Synchronizations\Http\Resources\Metrc\HarvestResource::class,
    \App\Models\Disposal::class => \App\Synchronizations\Http\Resources\Metrc\DisposalResource::class,
];
