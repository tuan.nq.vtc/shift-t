<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models\{Room, TraceableModel};
use Exception;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class Area
 * @package App\Synchronizations\Http\Resources\Metrc
 *
 * @property Room|TraceableModel $traceableModel
 */
class LocationResource extends BaseMetrcResource
{
    protected $path = '/locations/v1';

    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = $this->path.'/active';

        return $this;
    }

    protected function fetch(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = $this->path.'/'.$this->traceableModel->sync_code;

        return $this;
    }

    protected function create(): self
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/create';
        $this->requestBody = [
            [
                'Name' => $this->traceableModel->name,
                // Metrc use 'Default Location Type' as default type
                'LocationTypeName' => 'Default Location Type', // $this->resource->load(['roomType'])->roomType->name,
            ],
        ];

        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync room update without METRC Id [sync_code]')
        );
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->path.'/update';
        $this->requestBody = [
            [
                'Id' => $this->traceableModel->sync_code,
                'Name' => $this->traceableModel->name,
                // Need to validation of MeTrc
                'LocationTypeName' => 'Default Location Type', // $this->resource->load(['roomType'])->roomType->name,
            ],
        ];

        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync room deletion without METRC Id [sync_code]')
        );
        $this->httpMethod = Request::METHOD_DELETE;
        $this->requestUrl = $this->path.'/'.$this->traceableModel->sync_code;

        return $this;
    }
}
