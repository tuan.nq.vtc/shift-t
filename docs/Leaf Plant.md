Required:
- global_batch_id
- legacy_id
- origin
- plant_created_at
- stage

Modifiable:
- external_id
- global_batch_id
- is_initial_inventory
- is_mother
- plant_harvested_at
- plant_harvested_end_at
- stage