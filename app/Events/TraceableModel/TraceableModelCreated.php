<?php

namespace App\Events\TraceableModel;

use App\Events\Event;
use App\Models\{Trace, TraceableModel};

class TraceableModelCreated extends Event
{
    public TraceableModel $traceableModel;
    public string            $action;

    public function __construct(TraceableModel $traceableModel)
    {
        $this->traceableModel = $traceableModel;
        $this->action = Trace::ACTION_CREATE;
    }
}
