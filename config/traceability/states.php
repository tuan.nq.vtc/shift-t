<?php

use App\Models\State;

return [
    State::CALIFORNIA => [
        'base_url' => env('CA_API_BASE_URL', 'https://sandbox-api-ca.metrc.com'),
    ],
    State::COLORADO => [
        'base_url' => env('CO_API_BASE_URL', 'https://sandbox-api-co.metrc.com'),
    ],
    State::MASSACHUSETTS => [
        'base_url' => env('MA_API_BASE_URL', 'https://sandbox-api-ma.metrc.com'),
    ],
    State::WASHINGTON => [
        'base_url' => env('WA_API_BASE_URL', 'https://watest.leafdatazone.com/api/v1'),
    ]
];
