
### Items & Categories
#### Terms
- **Items** are used to track the licensee’s inventory through the supply chain life cycle. The Item Names are used to identify what type of item is packed into a package. An inventory list of a licensee’s current plants or packaged product is a good starting point to create the items in Metrc.
    - `Item` được dùng để theo dõi việc lưu kho của `licensee` theo vòng đời của chuỗi cung ứng. Tên của `item` đc dùng để xác định kiểu loại của `item` đc đóng gói trong 1 `package`. Một danh sách lưu kho các cây hoặc sản phẩm đã đóng gói của một `licensee` là điểm bắt đầu để tạo các `item` trên Metrc

- Nguyên tắc khi thêm mới `item`:
    1. When creating packages, the term Items identifies what type of item is in a Metrc package.
        - Khi tạo `package`, khái niệm `item` xác định kiểu loại của `item` ở bên trong 1 Metrc `package`.

    2. Items in Metrc allow each industry facility to have their own item names.
        - `Item` trên Metrc cho phép mỗi cơ sở có riêng từng tên `item`.

    3.  An item name cannot be just simply a category name. It must be specific to the item in that package or production batch.
        - Một tên `item` ko thể chỉ đơn giản là tên một danh mục. Tên phải đc chỉ rõ cho `item` chứa trong `package` hoặc lô sản phẩm.
    4.  Unlike Employees, Strains and Locations, Items cannot be created for multiple facilities at one time.
        - Khác với nhân viên, giống cây và địa điểm, `item` ko thể đc tạo cho nhiều cơ sở cùng lúc.

    5.  See Section 3.2 below for information on switching between facilities.
        - Xem Mục 3.2 bên dưới để biết thêm về việc chuyển qua lại giữa các cơ sở.

    6.  Each facility creates its own items that are unique with item name, category and strain. A facility cannot create duplicate item names.
        - Mỗi cơ sở tạo riêng các `item` đơn nhất theo tên, danh mục và giống cây. Một cơ sở ko thể tạo trùng lặp các tên `item`.

    7.  Each item requires a category selection and these categories are determined by the State of California.
        - Mỗi `item` cần thuộc một danh mục và các danh mục này đc định nghĩa bởi bang CA (xem API endpoint `/items/v1/categories`).

    8.  The purpose of the categories is for grouping similar items for reporting purposes.
        - Mục đích của các danh mục nhằm nhóm lại các `item` giống nhau cho mục đích báo cáo.

    9.  The item name will identify what is in the package and the category the item belongs in. 
        - Tên `item` sẽ xác định cái gì bên trong `package` và danh mục của `item`.

    10. The facility that packages an item will assign the item name to the package. The package will retain that item name unless it is re-packaged. 
        - Cơ sở đóng gói một `item` sẽ gán tên `item` vào `package`. `Package` này sẽ tiếp tục dùng tên `item` trừ khi `package` đc đóng gói lại.
  
    11. When creating a package, the item name will be chosen from the list of items previously created
        - Khi tạo một `package`, tên `item` sẽ đc chọn từ danh sách `item` đc tạo trc đó.

- Các danh mục đc định sẵn của bang CA (category):
```
    Category                            Quantity Type           Strain Required         Unit Volume Required        Unit Weight Required
    Capsule                             Count Based             NO                      NO                          YES
    Clone -Cutting                      Count Based             NO                      NO                          NO
    Clone -Tissue Culture               Count Based             NO                      NO                          NO
    Edible (volume -each)               Count Based             NO                      YES                         NO
    Edible (volume)                     Volume Based            NO                      NO                          NO
    Edible (weight - each)              Count Based             NO                      NO                          YES
    Edible (weight)                     Weight Based            NO                      NO                          NO
    Extract (volume - each)             Count Based             NO                      YES                         NO
    Extract (volume)                    Volume Based            NO                      NO                          NO
    Extract (weight - each)             Count Based             NO                      NO                          YES
    Extract (weight)                    Weight Based            NO                      NO                          NO
    Flower                              Weight Based            YES                     NO                          NO
    Flower (packaged 8th - each)        Count Based             YES                     NO                          YES
    Flower (packaged gr - each)         Count Based             YES                     NO                          YES
    Flower (packaged 1/2 ounce - each)  Count Based             YES                     NO                          YES
    Flower (packaged ounce -each)       Count Based             YES                     NO                          YES
    Flower (packaged quarter -each)     Count Based             YES                     NO                          YES
    Fresh Cannabis Plant                Weight Based            YES                     NO                          NO
    Immature Plant                      Count Based             YES                     NO                          NO
    Infused Butter/Oil (volume - each)  Count Based             NO                      YES                         NO
    Infused Butter/Oil(volume)          Volume Based            NO                      NO                          NO
    Infused Butter/Oil (weight - each)  Count Based             NO                      NO                          YES
    Infused Butter/Oil (weight)         Weight Based            NO                      NO                          NO
    Kief                                Weight Based            YES                     NO                          NO
    Leaf                                Weight Based            YES                     NO                          NO
    Shake                               Weight Based            NO                      NO                          NO
    Shake (Packaged Eighth -each)       Count Based             NO                      NO                          YES
    Shake (Packaged Gram -each)         Count Based             NO                      NO                          YES
    Shake (Packaged Half Ounce - each)  Count Based             NO                      NO                          YES
    Shake (Packaged Ounce - each)       Count Based             NO                      NO                          YES
    Shake (Packaged Quarter - each)     Count Based             NO                      NO                          YES
    Other Concentrate (volume - each)   Count Based             NO                      YES                         NO
    Other Concentrate (volume)          Volume Based            NO                      NO                          NO
    Other Concentrate (weight - each)   Count Based             NO                      NO                          YES
    Other Concentrate (weight)          Weight Based            NO                      NO                          NO
    Pre-Roll Flower                     Count Based             YES                     NO                          YES
    Pre-Roll Infused                    Count Based             NO                      NO                          YES
    Pre-Roll Leaf                       Count Based             YES                     NO                          YES
    Seeds (each)                        Count Based             YES                     NO                          NO
    Seeds                               Weight Based            YES                     NO                          NO
    Tincture (volume -each)             Count Based             NO                      YES                         NO
    Tincture(volume)                    Volume Based            NO                      NO                          NO
    Tincture (weight -each)             Count Based             NO                      NO                          YES
    Tincture (weight)                   Weight Based            NO                      NO                          NO
    Topical (volume -each)              Count Based             NO                      YES                         NO
    Topical (volume)                    Volume Based            NO                      NO                          NO
    Topical (weight -each)              Count Based             NO                      NO                          YES
    Topical (weight)                    Weight Based            NO                      NO                          NO
    Vape Cartridge (volume -each)       Count Based             NO                      YES                         NO
    Vape Cartridge (weight -each)       Count Based             NO                      NO                          YES
    Waste                               Weight Based            NO                      NO                          NO
```

- List on UI of California:
    - Seed (each)
        - Plant
    - Leaf
        - Shake Trim
    - Immature plants
        -Plants
    - Flower
        - Buds
    - Clone - Tissue Culture
        - Plants
    - Clone - Cutting
        - Plants

- Đơn vị tính:
    - Trọng lương: `Grams` `Kilograms` `Milligrams` `Ounces`=~0.02835 kg `Pounds`=~0.45359 kg
    - Thể tích: `Fluid Ounces`=~29.57352 ml `Gallons`=~3.78541 l `Liters` `Milliliters` `Pints`=~0.47318 l `Quarts`=~0.94635 l
    - Số lượng: Mỗi `Each`


#### Logic/Workflow
- Create a new package:
    - By repackage (đóng gói lại) one or more `package(s)`
        - Belong to the same category (thuộc cùng một danh mục).
        - ? Do not belong to the same category (ko thuộc cùng một danh mục).
    - Optional choices:
        - Production batch: If yes then specify `product batch number`
        - For donation
- 


---
#### Propagation
- Create planting (create `immature plants`):
    - Source:
        - Plant: `flowering` phase
        - Package: `immature plant` category 
    - Qty limited per `plant batch`: 100
    - Have one `tag` for the whole batch  


- 2 API endpoints available: 
    - `/plantbatches/v1/createplantings`
    ```
        [
            {
                "Name": "B. Kush 5-30",
                "Type": "Clone",
                "Count": 25,
                "Strain": "Spring Hill Kush",
                "Location": null,
                "Room": null,
                "PatientLicenseNumber": "X00001",
                "ActualDate": "2015-12-15"
            },
            {
                "Name": "B. Kush 5-31",
                "Type": "Seed",
                "Count": 50,
                "Strain": "Spring Hill Kush",
                "Location": null,
                "Room": null,
                "PatientLicenseNumber": "X00002",
                "ActualDate": "2015-12-15"
            }
        ]
    ```
    - `/plantbatches/v1/create/plantings`
    ```
        [
          {
            "Id": null,
            "PlantBatch": "Demo Plant Batch 1",
            "Count": 10,
            "Location": null,
            "Room": null,
            "Item": "Immature Plants",
            "Tag": "ABCDEF012345670000020201",
            "PatientLicenseNumber": "P00001",
            "Note": "This is a note.",
            "IsTradeSample": false,
            "IsDonation": false,
            "ActualDate": "2015-12-15"
          },
          {
            "Id": 5,
            "PlantBatch": null,
            "Count": 10,
            "Location": null,
            "Room": null,
            "Item": "Immature Plants",
            "Tag": "ABCDEF012345670000020202",
            "PatientLicenseNumber": "P00002",
            "Note": "",
            "IsTradeSample": true,
            "IsDonation": false,
            "ActualDate": "2015-12-15"
          }
        ]
    ```
- Both endpoints need `PatientLicenseNumber` -> what is it? and where does it come from?


Inventory Type = Item
Inventory = Package
