<?php

namespace App\Services;

use App\Facades\Portal;
use App\Integrations\FileGenerators\Resolvers\DataProviderResolver;
use App\Models\State;
use Illuminate\Support\Str;

class FileGeneratorService {
    public const FILE_UPLOAD_SUCCESS_STATUS = 'succes';
    public const FILE_UPLOAD_ERROR_STATUS = 'error';
    public const FILE_UPLOAD_IN_PROCESS_STATUS = 'in_process';

    public function setFilesUploadStatus($stateCode, $status, $files, $archives = []): bool
    {
        $result = true;

        $state = State::where('code', Str::upper($stateCode))->firstOrFail();
        $dataProviderClassname = DataProviderResolver::resolveClassnameByState($state);

        $fn = 'setFileUpload' . Str::camel($status);

        foreach($files as $fileReference) {
            $newFileReference = isset($archives[$fileReference]) ? $archives[$fileReference] : $fileReference;            
            $result = $result && $dataProviderClassname::$fn($fileReference, $newFileReference);
        }

        return $result;
    }

    public function getFilesToUploadByState(string $stateCode): array
    {
        $state = State::where('code', Str::upper($stateCode))->firstOrFail();

        $dataProviderClassname = DataProviderResolver::resolveClassnameByState($state);

        $licenses = Portal::fetchAllLicenses()->filter(
            fn($license) => ($license->state_code == $state->code && $license->is_test == 0)
        );

        $result = [];

        /*
        The files are returned in order of dependencies grouped by license.
        If the upload process get an error uploading a file, it will skip all subsequent files in the same group.
        */
        foreach ($licenses as $license) {
            $licenseGroup = [];
            $stateResources = $license->getStateResources();
            $dependencyGroups = array_unique(array_map(fn($item): string => $item['dependency_group'], $stateResources));

            foreach($dependencyGroups as $dependencyGroup) {
                $group = [];
                $resourceTypes = array_map(fn($item): string => ($item['dependency_group'] == $dependencyGroup) ? $item['resourceClassName'] : false, $stateResources);
                foreach($resourceTypes as $resourceType) {
                    $group = array_merge($group, $dataProviderClassname::getFilesToUploadByLicense($license, $resourceType));
                }
                if(count($group) > 0) {
                    $licenseGroup[$dependencyGroup] = $group;
                }    
            }

            if(count($licenseGroup) > 0) {
                ksort($licenseGroup);
                $result[$license->code] = array_values($licenseGroup);
            }
        }

        return $result;
    }
}