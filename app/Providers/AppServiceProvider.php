<?php

namespace App\Providers;

use Altek\Accountant\AccountantServiceProvider;
use App\Cqrs\Commands\CreateQASampleHandler;
use App\Cqrs\Commands\RemoveQASampleCOAHandler;
use App\Cqrs\Commands\UploadQASampleCOAHandler;
use App\Cqrs\Values\ClientIdentifier;
use App\Http\Controllers\QASampleController;
use App\Models\Inventory;
use App\Models\QASample;
use App\Models\QASampleCoa;
use App\Services\{FileUploadService, PortalService, SalesService, StateFileUploaderService, TraceService};
use Fruitcake\Cors\CorsServiceProvider;
use GuzzleHttp\Client;
use Illuminate\Cache\CacheManager;
use Illuminate\Queue\Events\{JobFailed, JobProcessed, JobProcessing};
use Illuminate\Redis\RedisServiceProvider;
use Illuminate\Support\{Facades\Log, Facades\Queue, ServiceProvider};
use Knuckles\Scribe\ScribeServiceProvider;
use Laravel\Tinker\TinkerServiceProvider;
use Psr\Log\LoggerInterface;
use Spatie\ValidationRules\ValidationRulesServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(AccountantServiceProvider::class);
        $this->app->register(CorsServiceProvider::class);
        $this->app->register(RedisServiceProvider::class);
        $this->app->register(ValidationRulesServiceProvider::class);
        $this->app->register(LicenseServiceProvider::class);
        $this->app->register(MinIOServiceProvider::class);
        $this->app->register(AccountHolderServiceProvider::class);

        $this->app->alias('cache', CacheManager::class);

        // Facades binding
        $this->app->bind('portal_service', PortalService::class);
        $this->app->bind('trace_service', TraceService::class);
        $this->app->bind('sales_service', SalesService::class);
        $this->app->bind('state_file_uploader', StateFileUploaderService::class);

        if ($this->app->environment() !== 'production') {
            $this->app->register(ScribeServiceProvider::class);
            $this->app->register(TinkerServiceProvider::class);
        }

        $this->app->bind(QASampleController::class, function () {
            return new QASampleController((array)config('qasample.validations'));
        });

        $this->app->bind(CreateQASampleHandler::class, function () {
            return new CreateQASampleHandler(
                $this->app->make(QASample::class),
                $this->app->make(Inventory::class),
                $this->app->make(LoggerInterface::class),
                (array)config('qasample.whitelist')
            );
        });

        $this->app->bind(UploadQASampleCOAHandler::class, function () {
            return new UploadQASampleCOAHandler(
                $this->app->make(QASampleCoa::class),
                $this->app->make(Client::class),
                new ClientIdentifier((array)config('global.storage_handler.authenticate')),
                $this->app->make(LoggerInterface::class),
                sprintf('%s/%s',
                    rtrim((string)config('global.storage_handler.baseUrl'), '\/'),
                    ltrim((string)config('global.storage_handler.endpoints.upload'), '\/')
                ),
                (array)config('qasample.coaMimeTypes')
            );
        });

        $this->app->bind(RemoveQASampleCOAHandler::class, function () {
            return new RemoveQASampleCOAHandler(
                $this->app->make(QASampleCoa::class),
                $this->app->make(Client::class),
                new ClientIdentifier((array)config('global.storage_handler.authenticate')),
                $this->app->make(LoggerInterface::class),
                sprintf('%s/%s',
                    rtrim((string)config('global.storage_handler.baseUrl'), '\/'),
                    ltrim((string)config('global.storage_handler.endpoints.delete'), '\/')
                ),
            );
        });

        $this->app->bind('file_upload', FileUploadService::class);
    }

    public function boot()
    {
        Queue::before(function (JobProcessing $event) {
            // $event->connectionName
            // $event->job
            // $event->job->payload()
        });

        Queue::after(function (JobProcessed $event) {
            // $event->connectionName
            // $event->job
            // $event->job->payload()
        });

        Queue::failing(function (JobFailed $event) {
            // $event->connectionName
            // $event->job
            // $event->exception
            Log::error($event->connectionName);
            Log::error($event->exception->getMessage());
        });
    }
}
