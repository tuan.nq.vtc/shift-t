<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

/**
 * Class BaseModel
 * @package App\Models
 */
class BaseModel extends Model
{
    public const DEFAULT_ORDER_SCOPE = 'defaultOrder';

    public $incrementing = false;

    public $keyType = 'string';

    protected string $orderByColumn = 'created_at';

    protected string $orderByDirection = 'desc';

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(
            function (self $model): void {
                // Automatically generate a UUID if using them, and not provided.
                if (empty($model->{$model->getKeyName()})) {
                    $model->setAttribute($model->getKeyName(), $model->generateUuid());
                }
            }
        );

        // uuid models are ordered by created_at
        static::addGlobalScope(
            self::DEFAULT_ORDER_SCOPE,
            function (Builder $builder) {
                $builder->scopes('defaultOrderBy');
            }
        );
    }

    /**
     * Override to fill common columns
     *
     * @return array
     */
    public function getFillable()
    {
        $fillable = [
            'id',
        ];

        $this->mergeFillable(array_diff($fillable, $this->fillable));

        return parent::getFillable();
    }

    /**
     * Get a new version 4 (random) UUID.
     */
    public function generateUuid(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * Scope a query to set a default order.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDefaultOrderBy($query): Builder
    {
        return $query->orderBy($this->getTable() . '.' . $this->orderByColumn, $this->orderByDirection);
    }
}
