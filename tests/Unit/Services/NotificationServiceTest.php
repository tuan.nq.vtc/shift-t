<?php

namespace Tests\Unit\Services;

use App\Models\{License, Notification, Strain, User};
use App\Services\NotificationService;
use Tests\Concerns\{ActingWithAttachedHeaders, Traceable, WithFaker};
use Tests\TestCase;

class NotificationServiceTest extends TestCase
{
    use Traceable;
    use WithFaker;
    use ActingWithAttachedHeaders;

    protected static string $modelName = Notification::class;
    protected static string $strainModel = Strain::class;
    protected static string $userModel = User::class;

    private function mockUsers($expectedTotalPartners)
    {
        return factory(static::$userModel, $expectedTotalPartners)->create(
            [
                'account_holder_id' => $this->faker->uuid,
                'organization_id' => $this->faker->uuid,
                'first_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName
            ]
        );
    }

    private function mockStrain(User $creator, License $license)
    {
        return factory(static::$strainModel)->create(
            [
                'license_id' => $license->id,
                'created_by' => $creator,
                'updated_by' => $creator,
                'name' => "Strain " . $this->faker->name,
                'status' => Strain::STATUS_ENABLED
            ]
        );
    }

    private function hasUpdatedNotification(Strain $strain, User $user)
    {
        $this->seeInDatabase('notifications',[
            'title' =>     __('notification.strain.updated_title'),
            'message' => __(
                'notification.strain.updated_message',
                [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'label' => $strain->name,
                ]
            )
        ]);
    }

    private function hasnotSyncUpdatedNotification(Strain $strain, User $user, string $regulator)
    {
        $this->notSeeInDatabase('notifications',[
            'title' =>    __('notification.strain.update_sync_completed_title', ['regulator' => $regulator]),
            'message' => __(
                'notification.strain.updated_message',
                [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'label' => $strain->name,
                ]
            )
        ]);
    }

    /**
     * @test
     */
    public function it_can_create_notification_while_creating_strain()
    {
        $this->withoutTraceableEvents();
        $expectedTotalPartners = rand(1, 10);
        $users = $this->mockUsers($expectedTotalPartners);
        $license = factory(License::class)->create();
        $creator = $users->random();
        $strain = $this->mockStrain($creator, $license);

        // execute
        $notification = $this->getService()->prepareDataToAlert(
            $strain,
            $creator,
            $this->getService()->getReceiversBy(
                $license->organization_id,
                $creator->account_holder_id
            ),
            Notification::EVENT_CREATE_ACTION_SYNC_COMPLETED,
            Notification::ALARM_TYPE_INFO
        );
        // assert
        $regulator = ucfirst($license->state->regulator);
        $this->assertEquals($expectedTotalPartners, $notification->userNotifications()->count());
        $this->assertEquals(__('notification.strain.sync_completed_title', ['regulator' => $regulator]), $notification->title);
        $this->assertEquals(
            __(
                'notification.strain.sync_completed_message',
                [
                    'regulator' => $regulator,
                    'label' => $strain->getNotificationLabel(),
                ]
            ),
            $notification->message
        );
    }

    /**
     * With field <Name>, this data will be Sync with State -> system will send 1 Notification only successful sync
     * With field <Status>, this data is not SYNC with State -> system send 1 Notification message update successful.
     * @test
     **/
    public function it_can_create_2_notifications_while_updating_strain_name()
    {
        $this->withoutTraceableEvents();
        $expectedTotalPartners = rand(1, 10);
        $users = $this->mockUsers($expectedTotalPartners);
        $license = factory(License::class)->create();
        $modifier = $users->random();
        $strain = $this->mockStrain($modifier, $license);

        // case update the field which sync to state
        $strain->name = "Strain " . $this->faker->name;
        $strain->save();

        // execute
        $notification = $this->getService()->prepareDataToAlert(
            $strain,
            $modifier,
            $this->getService()->getReceiversBy(
                $license->organization_id,
                $modifier->account_holder_id
            ),
            Notification::EVENT_UPDATE_ACTION_SYNC_COMPLETED,
            Notification::ALARM_TYPE_INFO
        );

        // assert
        $regulator = ucfirst($license->state->regulator);
        $this->assertEquals($expectedTotalPartners, $notification->userNotifications()->count());
        $this->assertEquals(__('notification.strain.update_sync_completed_title', ['regulator' => $regulator]), $notification->title);
        $this->assertEquals(
            __(
                'notification.strain.update_sync_completed_message',
                [
                    'regulator' => $regulator,
                    'label' => $strain->getNotificationLabel(),
                ]
            ),
            $notification->message
        );
       $this->hasUpdatedNotification($strain, $modifier);
    }

    /** @test  */
    public function it_can_create_notification_while_updating_strain_status()
    {
        $this->withoutTraceableEvents();
        $users = $this->mockUsers(rand(1, 10));
        $license = factory(License::class)->create();
        $modifier = $users->random();
        $strain = $this->mockStrain($modifier, $license);
        $strain->status = Strain::STATUS_DISABLED;
        $strain->save();
        // assert
        $this->hasUpdatedNotification($strain, $modifier);
        $this->hasnotSyncUpdatedNotification($strain, $modifier, ucfirst($license->state->regulator));
    }

    protected function getService(): NotificationService
    {
        return app(NotificationService::class);
    }
}
