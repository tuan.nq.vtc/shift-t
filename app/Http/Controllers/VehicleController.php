<?php

namespace App\Http\Controllers;

use App\Services\VehicleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class VehicleController
 * @package App\Http\Controllers
 */
class VehicleController extends CRUDController
{
    public function __construct(VehicleService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:150'],
            'color' => ['required', 'string'],
            'make' => ['required', 'string'],
            'model' => ['required', 'string'],
            'year' => ['required', 'string'],
            'plate' => ['required', 'string'],
            'vin' => ['required', 'string'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'min:3', 'max:150'],
            'color' => ['sometimes', 'required', 'string'],
            'make' => ['sometimes', 'required', 'string'],
            'model' => ['sometimes', 'required', 'string'],
            'year' => ['sometimes', 'required', 'string'],
            'plate' => ['sometimes', 'required', 'string'],
            'vin' => ['sometimes', 'required', 'string'],
            'status' => ['sometimes', 'required', 'boolean'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems());
    }
}
