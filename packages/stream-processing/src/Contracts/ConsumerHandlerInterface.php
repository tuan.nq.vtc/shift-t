<?php

namespace Bamboo\StreamProcessing\Contracts;

use Throwable;

interface ConsumerHandlerInterface
{
    /**
     * @param string $topicName
     * @return StreamEventInterface
     *
     * @throws Throwable
     */
    public function receiveEvent(string $topicName): StreamEventInterface;
}
