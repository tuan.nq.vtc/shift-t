<?php

namespace App\Events\Inventory;

use App\Events\Event;
use App\Models\Inventory;

class SavingInventoryHistory extends Event
{
    /**
     * @var Inventory
     */
    public Inventory $sourceInventory;
    public string $action;
    public float $variableQty;
    public $source;
    public bool $isDecrease;
    public array $extraInfo;

    public function __construct(
        Inventory $sourceInventory,
        string    $action,
                  $variableQty,
        bool      $isDecrease = true,
                  $source = null,
        array     $extraInfo = []
    )
    {
        $this->sourceInventory = $sourceInventory;
        $this->action = $action;
        $this->variableQty = $variableQty;
        $this->source = $source;
        $this->isDecrease = $isDecrease;
        $this->extraInfo = $extraInfo;
    }
}
