<?php

namespace App\Synchronizations\Constants;

class LeafConstant
{
    public const ENDPOINT_AREA = '/areas';
    public const ENDPOINT_BATCH = '/batches';
    public const ENDPOINT_CURE_LOT = '/batches/cure_lot';
    public const ENDPOINT_FINISH_LOT = '/batches/finish_lot';
    public const ENDPOINT_DISPOSAL = '/disposals';
    public const ENDPOINT_INVENTORY_TYPE = '/inventory_types';
    public const ENDPOINT_INVENTORY = '/inventories';
    public const ENDPOINT_LAB_RESULT = '/lab_results';
    public const ENDPOINT_PLANT = '/plants';
    public const ENDPOINT_HARVEST_PLANTS = '/plants/harvest_plants';
    public const ENDPOINT_STRAIN = '/strains';

    //Type of Batches
    public const BATCH_TYPE_PLANT = 'plant';
    public const BATCH_TYPE_HARVEST = 'harvest';
    public const BATCH_TYPE_PROPAGATION = 'propagation material';
    public const BATCH_TYPE_INTERMEDIATE_END_PRODUCT = 'intermediate/ end product';

    public const BATCH_STATUS_OPEN = 'open';
    public const BATCH_STATUS_CLOSED = 'closed';
}
