<?php

namespace App\Models\Traits;

use App\Events\TraceableModel\TraceableModelCreated;
use App\Events\TraceableModel\TraceableModelDeleted;
use App\Events\TraceableModel\TraceableModelUpdated;
use App\Models\TraceableModel;
use Exception;

trait WithoutTraceableModelEvents
{
    public static function withoutTraceableModelEvents(callable $callback)
    {
        $dispatcher = TraceableModel::getEventDispatcher();

        if ($dispatcher) {
            $dispatcher->forget(TraceableModelCreated::class);
            $dispatcher->forget(TraceableModelUpdated::class);
            $dispatcher->forget(TraceableModelDeleted::class);
        }

        try {
            return $callback();
        } finally {
            if ($dispatcher) {
                TraceableModel::setEventDispatcher($dispatcher);
            }
        }
    }

    /**
     * @param array $data
     *
     * @return self
     */
    public function createWithoutTraceableModelEvents(array $data = []): self
    {
        return static::withoutTraceableModelEvents(
            fn() => $this->create($data)
        );
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function updateWithoutTraceableModelEvents(array $data = []): bool
    {
        return static::withoutTraceableModelEvents(
            fn() => $this->update($data)
        );
    }

    /**
     * @return bool|null
     *
     * @throws Exception
     */
    public function deleteWithoutTraceableModelEvents(): ?bool
    {
        return static::withoutTraceableModelEvents(
            fn() => $this->delete()
        );
    }

    /**
     * @return bool
     */
    public function saveWithoutTraceableModelEvents(): bool
    {
        return static::withoutTraceableModelEvents(
            fn() => $this->save()
        );
    }
}
