<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{AdditiveOperation, Plant, Propagation, Room, RoomType, SeedToSale};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection, Model};
use Illuminate\Support\Arr;
use Throwable;

/**
 * Class RoomService
 *
 * @package App\Services
 *
 * @method Room|Builder getModel()
 */
class RoomService extends BaseService
{
    protected array $relations = [
        'roomType',
        'creator',
        'modifier',
        'subrooms',
        'inventories',
        'statistics',
        'plants'
    ];

    protected array $counts = [
    ];

    public function __construct(Room $room)
    {
        parent::__construct($room);
    }

    /**
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    { 
        if(Arr::exists($criteria->getSorts(),'plants_count')) {
            in_array ('plants', $this->counts) ||  $this->counts[] = 'plants';
        }

        if(Arr::exists($criteria->getSorts(),'propagations_count')) {
            in_array ('propagations', $this->counts) ||  $this->counts[] = 'propagations';
        }

        return parent::list($criteria);
    }

    /**
     * @param $id
     * @param $licenseId
     *
     * @return bool
     */
    public static function isDisposal($id, $licenseId)
    {
        return Room::whereHas(
                'roomType',
                function ($query) {
                    $query->where('category', RoomType::CATEGORY_DISPOSAL);
                }
            )->where('id', $id)->where('license_id', $licenseId)->count() > 0;
    }

    /**
     * @param array $data
     *
     * @return Model
     *
     * @throws GeneralException
     */
    public function create(array $data): Room
    {
        if (empty($data['user_id'])) {
            throw new GeneralException(__('exceptions.rooms.create_failed'));
        }
        return parent::create(
            array_merge(
                $data,
                [
                    'created_by' => $data['user_id'],
                    'updated_by' => $data['user_id']
                ]
            )
        );
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return Room
     *
     * @throws GeneralException
     */
    public function update($id, array $data): Room
    {
        if (empty($data['user_id'])) {
            throw new GeneralException(__('exceptions.rooms.update_failed'));
        }
        return parent::update(
            $id,
            array_merge(
                $data,
                [
                    'updated_by' => $data['user_id']
                ]
            )
        );
    }

    /**
     * @param $categoryCode
     *
     * @return Collection
     * @throws GeneralException
     */
    public function getByCategory($categoryCode)
    {
        if (!in_array($categoryCode, RoomType::getCategories())) {
            throw new GeneralException('Room type is invalid');
        }

        return $this->newQuery()
            ->whereNotNull('sync_code')
            ->whereHas(
                'roomType',
                function ($query) use ($categoryCode) {
                    $query->where('category', $categoryCode);
                }
            )
            ->get(['id', 'name', 'sync_status', 'sync_code'])->load('subrooms:id,room_id,name');
    }

    public function index($criteria)
    {
        return $this->newQuery(false)
            ->scopes($this->loadScopes($criteria->getFilters()))
            ->whereNotNull('sync_code')
            ->get(['id', 'name', 'sync_status', 'sync_code'])->load('subrooms:id,room_id,name');
    }

    public function getDetail($id)
    {
        return $this->newQuery()->with(
            [
                ...$this->relations,
                'ipmOperations' => fn($ipmOperationQuery) => $ipmOperationQuery->with(
                    ['applicator', 'additive:id,name']
                )
                    ->orderBy('applied_date', 'desc')
                    ->take(AdditiveOperation::DEFAULT_LIMIT),
                'feedOperations' => fn($feedOperationQuery) => $feedOperationQuery->with(
                    ['applicator', 'additive:id,name']
                )
                    ->orderBy('applied_date', 'desc')
                    ->take(AdditiveOperation::DEFAULT_LIMIT),
            ]
        )
            ->findOrFail($id);
    }

    public function delete($id): void
    {
        try {
            /** @var Room $room */
            $room = $this->newQuery()->withCount(['plants', 'inventories'])->find($id);
            if ($room->plants_count) {
                throw new GeneralException(__('exceptions.rooms.delete_failed_on_storing_plants'));
            }
            if ($room->inventories_count) {
                throw new GeneralException(__('exceptions.rooms.delete_failed_on_storing_inventory_lots'));
            }
            $room->delete();
        } catch (Throwable $throwable) {
            if ($throwable instanceof GeneralException) {
                throw $throwable;
            }
            throw new GeneralException(__('exceptions.rooms.delete_failed'));
        }
    }

    /**
     * @return array
     */
    public function getStatistics(): array
    {
        return [
            'active_rooms_count' => $this->newQuery()->where('status', Room::STATUS_ENABLED)->count(),
            'inactive_rooms_count' => $this->newQuery()->where('status', Room::STATUS_DISABLED)->count(),
            'plants_count' => Plant::where('grow_status', '<>', SeedToSale::GROW_STATUS_HARVESTED)->licenseId($this->licenseId)->count(),
            'mothers_count' => Plant::where('is_mother', true)->licenseId($this->licenseId)->count(),
            'propagations_count' => Propagation::whereIn('source_type', [
                SeedToSale::SOURCE_TYPE_CLONE,
                SeedToSale::SOURCE_TYPE_SEED,
                SeedToSale::SOURCE_TYPE_TISSUE
            ])->licenseId($this->licenseId)->count(),
            'total_propagations' => Propagation::licenseId($this->licenseId)->count(),
            'plant_rooms_count' => Room::licenseId($this->licenseId)
                ->whereHas('roomType', fn($query) => $query->whereIn('category', [
                    RoomType::CATEGORY_PROPAGATION,
                    RoomType::CATEGORY_PLANT
                ]))->count(),
            'inventory_rooms_count' => Room::licenseId($this->licenseId)
                ->whereHas('roomType', fn($query) => $query->where('category', RoomType::CATEGORY_INVENTORY))
                ->count(),
        ];
    }

    public static function checkIsRoomType($id, $licenseId, $roomType)
    {
        return Room::whereHas(
                'roomType',
                fn(Builder $query) => $query->where('category', $roomType)
            )->where('id', $id)->where('license_id', $licenseId)->count() > 0;
    }
}
