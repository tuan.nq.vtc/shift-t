<?php

namespace App\Jobs;

use App\Models\LicenseResource;
use App\Synchronizations\Puller;
use Log;
use Throwable;

class FetchLicenseResource extends Job
{
    public $chained = [];
    public $deleteWhenMissingModels = true;
    public $delay = 5;
    public $maxTries = 5;

    protected LicenseResource $licenseResource;
    protected ?string $endpoint;

    public function __construct(LicenseResource $licenseResource, string $endpoint = null)
    {
        $this->queue = 'fetch_license_resource';
        $this->licenseResource = $licenseResource;
        $this->endpoint = $endpoint;
    }

    /**
     * Execute the job.
     *
     * @param Puller $puller
     *
     * @return void
     *
     * @throws Throwable
     */
    public function handle(Puller $puller)
    {
        $puller->fetch($this->licenseResource, $this->endpoint);
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception)
    {
        $error = "Fetch license resource [{$this->licenseResource->resource}] failed: {$exception->getMessage()}";
        if ('cli' === php_sapi_name()) {
            echo $error.PHP_EOL;
            echo $exception->getTraceAsString();
        }
        Log::error($error);
    }
}
