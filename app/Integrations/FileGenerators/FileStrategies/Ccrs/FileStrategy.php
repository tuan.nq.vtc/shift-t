<?php

namespace App\Integrations\FileGenerators\FileStrategies\Ccrs;

use App\Integrations\FileGenerators\Exceptions\FileGeneratorException;
use App\Integrations\FileGenerators\FileStrategies\CsvFileStrategy;
use App\Models\Trace;
use App\Models\User;
use Carbon\Carbon;

abstract class FileStrategy extends CsvFileStrategy
{
    protected static string $fileIdentifier = '';

    private const OPERATION_INSERT = 'Insert';
    private const OPERATION_UPDATE = 'Update';
    private const OPERATION_DELETE = 'Delete';

    private const VALUE_TRUE = 'TRUE';
    private const VALUE_FALSE = 'FALSE';

    public function getFooters(): array
    {
        return [];
    }

    public function getHeaders(): array
    {
        if (!isset($this->getLicense()->account_holder['name'])) {
            throw new FileGeneratorException('Cannot get Account Holder name for license.');
        }

        return [
            ['SubmittedBy', $this->getLicense()->account_holder['name']],
            ['SubmittedDate', Carbon::now()->format('m/d/Y')],
            ['NumberRecords', $this->getDataCount()],
        ];
    }

    protected function formatDateRow($date): string
    {
        return $date->format('m/d/Y');
    }

    protected function convertActionToOperation(string $action): string
    {
        switch ($action) {
            case Trace::ACTION_CREATE:
                return self::OPERATION_INSERT;

            case Trace::ACTION_UPDATE:
                return self::OPERATION_UPDATE;

            case Trace::ACTION_DELETE:
                return self::OPERATION_DELETE;

            default:
                throw new FileGeneratorException('Action ' . $action . ' cannot be converted to operation');
        }
    }

    protected function boolToString(bool $value): string
    {
        return $value ? self::VALUE_TRUE : self::VALUE_FALSE;
    }

    public function getFilename(): string
    {
        $filename = "ccrs/" . $this->getLicense()->account_holder_id . "/" . $this->license->code . "/";
        $filename .= static::$fileIdentifier . '_' . config('integrations.ccrs.integrator_id') . '_' . Carbon::now()->format(
                'Ymdhis'
            ) . '.csv';

        //Sleep a second to avoid generating files with the same name
        sleep(1);

        return $filename;
    }

    protected function getShortenedtUserName(User $user): string
    {
        return mb_substr($user->first_name, 0, 1) . ". {$user->last_name}";
    }
}
