<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\InventoryType;
use App\Models\SourceProduct;
use App\Models\State;
use App\Models\StateCategory;
use App\Models\Strain;
use App\Models\Trace;
use App\Models\TraceableModel;
use App\Services\LicenseService;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Exceptions\Pull\NoImportableDataException;
use App\Synchronizations\Exceptions\Pull\SkipProcessItemException;
use App\Synchronizations\Helpers\LeafHelper;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class InventoryTypeProcess
 * @property InventoryType $traceableModel
 */
class InventoryTypeProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'm/d/Y G:ia';

    /**
     * @return void
     * @throws Throwable
     */

    /**
     * @return void
     * @throws Throwable
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;

        $hasNextImport = true;
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }
        try {
            $reports = $this->importData($payload['data'] ?? []);
        } catch (NoImportableDataException $e) {
            if ($currentPage !== 1) {
                $hasNextImport = false;
            }
            $reports = [];
        } catch (Throwable $e) {
            throw $e;
        }
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));

        if ($lastPage === 1) {
            $hasNextImport = false;
        }
        $nextPage = $currentPage === 1 ? $lastPage : $currentPage - 1;
        if ($hasNextImport && $nextPage <= 1) {
            $hasNextImport = false;
        }
        if ($hasNextImport) {
            $children = $this->trace->children;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }

            // create trace from last page to second page
            $conditions['page'] = $nextPage;
            $nextPageTrace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => get_classname($this->traceableModel),
                    'status' => Trace::STATUS_PENDING,
                    'resource_conditions' => $conditions,
                ]
            );

            // reassign the children of current trace to next page trace
            $nextPageTrace->children()->attach($children);
            dispatch(new SyncData($nextPageTrace));

            $this->trace->children()->detach();
        }

        // dispatch result after imported
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $hasNextImport ? StreamableResourceInterface::ACTION_UPDATE : 'finish',
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     * @throws Exception
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $licenseId = $this->traceableModel->license->id;
        $conditions = $this->trace->resource_conditions ?? [];
        $fromDate = null;
        $toDate = null;
        if (!empty($conditions['from_date'])) {
            $fromDate = Carbon::createFromDate($conditions['from_date']);
        }
        if (!empty($conditions['to_date'])) {
            $toDate = Carbon::createFromDate($conditions['to_date']);
        }

        // prepare data
        $syncCodes = $categoryCodes = $strainSyncCodes = $sourceProductNames = [];
        $importableData = [];
        foreach ($data as $item) {
            try {
                $updatedAt = LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'updated_at'));
                if (!$updatedAt || ($fromDate && $fromDate->greaterThanOrEqualTo($updatedAt))
                    || ($toDate && $toDate->lessThanOrEqualTo($updatedAt))) {
                    continue;
                }
            } catch (Exception $e) {
                $this->logger->info($item);
                continue;
            }
            if (!empty($item['global_id'])) {
                $syncCodes[$item['global_id']] = 1;
            }
            if (!empty($item['global_strain_id'])) {
                $strainSyncCodes[$item['global_strain_id']] = 1;
            }
            if (!empty($item['intermediate_type'])) {
                $categoryCodes[$item['intermediate_type']] = 1;
            }
            if (!empty($item['name'])) {
                $sourceProductNames[$item['name']] = 1;
            }
            $importableData[] = $item;
        }
        if (empty($importableData)) {
            throw new NoImportableDataException();
        }

        /**
         * ['sync_code' => 'id']
         */
        $availableStrainArray = Strain::licenseId($licenseId)->whereIn('sync_code', array_keys($strainSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $existingInventoryTypeArray = InventoryType::licenseId($licenseId)->whereIn('sync_code', array_keys($syncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['name' => 'id']
         */
        $existingSourceProductArray = SourceProduct::licenseId($licenseId)->whereIn(
            'name',
            array_keys($sourceProductNames)
        )->pluck('id', 'name')->toArray();

        $availableCategoryArray = StateCategory::whereRegulator(State::REGULATOR_LEAF)->whereNotNull('parent_id')
            ->whereIn('code', array_keys($categoryCodes))
            ->select('id', 'code', 'uom')->get()->keyBy('code')->toArray();
        $usedStrainIds = [];
        $newInventoryTypeData = $newSourceProductData = $newEndProductData = [];
        foreach ($importableData as $item) {
            try {
                if (!($categoryCode = Arr::get($item, 'type')) ||
                    !in_array($categoryCode, ['harvest_materials', 'intermediate_product', 'end_product'])) {
                    continue;
                }
                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingInventoryTypeArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }
                $strainId = $uom = null;
                if (($strainSyncCode = Arr::get($item, 'global_strain_id'))
                    && !empty($availableStrainArray[$strainSyncCode])) {
                    $strainId = $availableStrainArray[$strainSyncCode];
                    $usedStrainIds[$strainId] = 1;
                }
                $subCategoryCode = Arr::get($item, 'intermediate_type');
                if (empty($availableCategoryArray[$subCategoryCode])) {
                    throw new Exception('State Category Not Found');
                }
                $categoryId = $availableCategoryArray[$subCategoryCode]['id'];
                $uom = $availableCategoryArray[$subCategoryCode]['uom'];

                if (!$uom && Arr::get($item, 'uom')) {
                    $uom = LeafHelper::mapToLeafUom(Arr::get($item, 'uom'), true);
                }
                $name = Arr::get($item, 'name');
                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                switch ($categoryCode) {
                    case 'intermediate_product':
                        if ($name && empty($existingSourceProductArray[$name])) {
                            $newSourceProductData[] = [
                                'id' => Uuid::uuid4()->toString(),
                                'license_id' => $licenseId,
                                'internal_id' => ModelHelper::generateInternalId(
                                    SourceProduct::class,
                                    $licenseId
                                ),            
                                'name' => $name,
                                'sku' => $this->formatSku($name),
                                'strain_id' => $strainId,
                                'uom' => $uom,
                                'created_at' => $createdAt,
                                'updated_at' => $updatedAt,
                            ];
                        }
                        break;
                    case 'end_product':
                        // $newEndProductData[] = [];
                        break;
                }
                $newInventoryTypeData[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'name' => $name,
                    'license_id' => $licenseId,
                    'state_category_id' => $categoryId,
                    'uom' => $uom,
                    'options' => json_encode(
                        [
                            'weight' => Arr::get($item, 'weight_per_unit_in_grams'),
                            'serving_num' => Arr::get($item, 'serving_num'),
                            'serving_size' => Arr::get($item, 'serving_size'),
                        ]
                    ),
                    'sync_code' => $globalId,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => $today,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        try {
            DB::beginTransaction();
            if (!empty($newInventoryTypeData)) {
                InventoryType::insert($newInventoryTypeData);
            }
            if (!empty($newSourceProductData)) {
                SourceProduct::insert($newSourceProductData);
            }

            // enable used strains
            if (!empty($usedStrainIds)) {
                Strain::query()->where('status', Strain::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedStrainIds))
                    ->update(['status' => Strain::STATUS_ENABLED]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['success' => $success, 'error' => $error];
    }

    /**
     * @param $item
     * @throws Exception
     */
    protected function processListItem($item): void
    {
        if (!($globalId = Arr::get($item, 'global_id'))) {
            throw new Exception('Global ID Not Found');
        }

        $stateCategory = StateCategory::whereRegulator(State::REGULATOR_LEAF)
            ->whereNotNull('parent_id')->where('code', Arr::get($item, 'intermediate_type'))->first();
        if (!$stateCategory) {
            throw new SkipProcessItemException();
        }
        if (Arr::get($item, 'uom')) {
            $uom = LeafHelper::mapToLeafUom(Arr::get($item, 'uom'), true);
        } else {
            $uom = $stateCategory->uom;
        }
        $this->traceableModel->newInstance()->firstOrCreate(
            ['sync_code' => $globalId, 'license_id' => $this->traceableModel->license->id,],
            [
                'name' => Arr::get($item, 'name'),
                'created_at' => LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'created_at')),
                'updated_at' => LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'updated_at')),
                'state_category_id' => $stateCategory->id,
                'uom' => $uom,
                'options' => [
                    'weight' => Arr::get($item, 'weight_per_unit_in_grams'),
                    'serving_num' => Arr::get($item, 'serving_num'),
                    'serving_size' => Arr::get($item, 'serving_size'),
                ],
                'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                'synced_at' => Carbon::now(),
            ]
        );
    }

    /**
     * @param string $string
     * @return string
     */
    private function formatSku(string $string): string
    {
        $string = str_replace(' ', '-', $string);

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }
}
