<?php

use App\Models\{Inventory, License, Manifest, ManifestItem, Plant, Propagation, QASample};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class ManifestSeeder extends Seeder
{
    use DisableForeignKeys;

    private ?Collection $users = null;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
        $this->enableForeignKeys();
    }

    protected function iterateLicense(License $license): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $manifest = factory(Manifest::class)->create(['license_id' => $license->id,]);

            $types = [
                ManifestItem::TYPE_PLANT,
                ManifestItem::TYPE_PROPAGATION,
                ManifestItem::TYPE_QA_SAMPLE,
                ManifestItem::TYPE_UNLOTTED_INVENTORY,
                ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY,
                ManifestItem::TYPE_END_PRODUCT_INVENTORY
            ];
            $resource = null;
            foreach ($types as $type) {
                switch ($type) {
                    case ManifestItem::TYPE_PLANT:
                        $resource = factory(Plant::class)->create(['license_id' => $license->id]);
                        break;
                    case ManifestItem::TYPE_PROPAGATION:
                        $resource = factory(Propagation::class)->create(['license_id' => $license->id]);
                        break;
                    case ManifestItem::TYPE_QA_SAMPLE:
                        $resource = factory(QASample::class)->create(['license_id' => $license->id]);
                        break;
                    case ManifestItem::TYPE_UNLOTTED_INVENTORY || ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY || ManifestItem::TYPE_END_PRODUCT_INVENTORY:
                        $resource = factory(Inventory::class)->create(['license_id' => $license->id]);
                        break;
                }
            }
            factory(ManifestItem::class, 10)->create(
                [
                    'manifest_id' => $manifest->id,
                    'resource_type' => $resource? get_class($resource) : null,
                    'resource_id' => $resource? $resource->id : null,
                    'strain_id' => $resource? $resource->strain_id : null,
                ]
            );
        }
    }
}
