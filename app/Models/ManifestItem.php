<?php

namespace App\Models;

use App\Events\Manifest\ManifestItemSaving;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ManifestItem extends BaseModel
{
    public const TYPE_PLANT = 'plant';
    public const TYPE_PROPAGATION = 'propagation';
    public const TYPE_QA_SAMPLE = 'qa_sample';
    public const TYPE_UNLOTTED_INVENTORY = 'unlotted_inventory';
    public const TYPE_SOURCE_PRODUCT_INVENTORY = 'source_product_inventory';
    public const TYPE_END_PRODUCT_INVENTORY = 'end_product_inventory';

    public const STATUS_PENDING = 0;
    public const STATUS_TRANSFERRED = 1;
    public const STATUS_CANCELED = 2;

    public const TYPES = [
        self::TYPE_PLANT,
        self::TYPE_PROPAGATION,
        self::TYPE_QA_SAMPLE,
        self::TYPE_UNLOTTED_INVENTORY,
        self::TYPE_SOURCE_PRODUCT_INVENTORY,
        self::TYPE_END_PRODUCT_INVENTORY,
    ];

    protected $dispatchesEvents = [
        'saving' => ManifestItemSaving::class
    ];

    protected $fillable = [
        'manifest_id',
        'type',
        'strain_id',
        'resource_type',
        'resource_id',
        'quantity',
        'price',
        'total',
        'is_sample',
        'is_for_extraction',
        'status',
    ];

    protected $casts = [
        'quantity' => 'double',
        'price' => 'double',
        'total' => 'double',
    ];

    protected $hidden = ['resource_type'];

    public function manifest(): BelongsTo
    {
        return $this->belongsTo(Manifest::class);
    }

    public function strain(): BelongsTo
    {
        return $this->belongsTo(Strain::class, 'strain_id', 'id');
    }

    public function resource(): MorphTo
    {
        return $this->morphTo('resource');
    }

    public function scopePending(Builder $query): Builder
    {
        return $query->where($this->getTable() . '.status', self::STATUS_PENDING);
    }
}
