<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateQaSamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qa_samples', function (Blueprint $table) {
            if(!Schema::hasColumn('qa_samples', 'results')) {
                $table->json('results')->nullable();
                $table->string('analysis_type')->default('total_cannabinoid');
                $table->renameColumn('inventory_id', 'source_inventory_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qa_samples', function (Blueprint $table) {
            if(Schema::hasColumn('qa_samples', 'results')) {
                $table->dropColumn(['results', 'analysis_type']);
                $table->renameColumn('source_inventory_id', 'inventory_id');
            }
        });
    }
}
