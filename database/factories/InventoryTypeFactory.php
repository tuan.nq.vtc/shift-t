<?php

/** @var Factory $factory */

use App\Models\InventoryType;
use App\Models\License;
use App\Models\SeedToSale;
use App\Models\StateCategory;
use App\Models\Strain;
use App\Models\TraceableModel;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    InventoryType::class,
    function (Faker $faker) {
        return [
            'name' => $faker->word,
            'license_id' => fn() => factory(License::class)->create()->id,
            'state_category_id' => fn() => factory(StateCategory::class)->create()->id,
            'strain_id' => fn($data) => factory(Strain::class)->create(
                ['license_id' => $data['license_id']]
            )->id,
            'uom' => $faker->randomElement([SeedToSale::UOM_GRAMS, SeedToSale::UOM_EACH]),
            'sync_code' => 'WAG010101.TY' . $faker->regexify('[A-Z0-9]{2}'), // WAG010101.TY94
            'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
            'synced_at' => $faker->dateTimeThisYear(),
        ];
    }
);
