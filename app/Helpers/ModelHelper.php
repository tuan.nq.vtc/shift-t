<?php

namespace App\Helpers;

use App\Models\License;
use App\Services\LicenseService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;

class ModelHelper
{
    private const DEFAULT_INTERNAL_ID_LENGTH = 4;

    private const INTERNAL_ID_GENERATION_ATTEMPTS = 10;
    
    /**
     * @param string $modelClass
     * @param string $licenseId
     * @param string|null $accountHolderId
     * @param string|null $subLevel
     * @return string
     * @throws Exception
     */
    public static function generateInternalId(string $modelClass, ?string $licenseId, ?string $accountHolderId = null, ?string $subLevel = null): string
    {
        $licenseCode = null;
        if (!empty($licenseId)) {
            try {
                $license = License::findOrFail($licenseId);
                $licenseCode = $license->state_code . $license->code;
            } catch(ModelNotFoundException $e) {
                throw new Exception("License $licenseId not found for $modelClass.");
            }
            $relatedEntityFk = 'license_id';
            $relatedEntityId = $licenseId;
        } else {
            $relatedEntityFk = 'account_holder_id';
            $relatedEntityId = $accountHolderId;
        }
        if (empty($relatedEntityId)) {
            throw new Exception('Related Entity is mandatory.');
        }

        $abbreviation = config('internal_id.abbreviation');
        $length = config('internal_id.length');
        if (!isset($abbreviation[$modelClass])) {
            throw new Exception("Model abbreviation is not configured for $modelClass.");
        }
        $modelAbbreviation = $abbreviation[$modelClass];

        if (is_array($modelAbbreviation)) {
            if (!isset($modelAbbreviation[$subLevel])) {
                throw new Exception("Model abbreviation (subLevel) is not configured for $modelClass.");
            }
            $modelAbbreviation = $modelAbbreviation[$subLevel];
        }

        $stringLength = $length[$modelClass] ?? static::DEFAULT_INTERNAL_ID_LENGTH;
        $internalId = '';
        for ($a = 1; $a <= static::INTERNAL_ID_GENERATION_ATTEMPTS; $a++) {
            $pieces = [];
            if(null !== $licenseCode) {
                $pieces[] = $licenseCode;
            }

            $uniqueCode = static::generateRandomString($stringLength);
            if (!$uniqueCode) {
                throw new Exception('Failed to generate unique code.');
            }
            $pieces[] = $modelAbbreviation . $uniqueCode;
            $internalId = implode('.', $pieces);
            if (app($modelClass)->newQuery()->where($relatedEntityFk, $relatedEntityId)
                    ->where('internal_id', $internalId)->count() === 0) {
                break;
            }
        }
        if (!$internalId) {
            throw new Exception('Failed to generate internal id.');
        }

        return $internalId;
    }

    /**
     * @param int $length
     * @return string
     */
    private static function generateRandomString($length = 10): string
    {
        return substr(
            str_shuffle(str_repeat($x = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))),
            1,
            $length
        );
    }
}
