<?php

namespace App\Http\Controllers;

use App\Services\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ClientController
 * @package App\Http\Controllers
 */
class ClientController extends CRUDController
{
    public function __construct(ClientService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:150'],
            'license_id' => ['required', 'string'],
            'license_type_id' => ['required', 'string'],
            'address' => ['required', 'string'],
            'state' => ['required', 'string'],
            'zip_code' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'email' => ['required', 'email'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'min:3', 'max:150'],
            'license_id' => ['sometimes', 'required', 'string'],
            'license_type_id' => ['sometimes', 'required', 'string'],
            'address' => ['sometimes', 'required', 'string'],
            'state' => ['sometimes', 'required', 'string'],
            'zip_code' => ['sometimes', 'required', 'string'],
            'phone' => ['sometimes', 'required', 'string'],
            'email' => ['sometimes', 'required', 'email'],
            'status' => ['sometimes', 'required', 'boolean'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems());
    }
}
