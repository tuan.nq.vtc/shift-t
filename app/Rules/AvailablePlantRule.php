<?php

namespace App\Rules;

use App\Models\Plant;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;

class AvailablePlantRule implements Rule
{
    /** @var string|array */
    protected $message;
    protected string $licenseId;

    public function __construct(string $licenseId)
    {
        $this->licenseId = $licenseId;
    }

    public function passes($attribute, $value)
    {
        /** @var Plant|Collection|Plant[] $plants */
        $plants = Plant::where('license_id', $this->licenseId)->find($value);

        return ($plants instanceof Collection) ? $this->validatePlants($plants) : $this->validatePlant($plants);
    }

    protected function validatePlants(Collection $plants): bool
    {
        if ($plants->isEmpty()) {
            return $this->setError(__('exceptions.plants.not_exist'));
        }
        $messages = [];
        foreach ($plants as $plant) {
            if ($message = $this->validatePlant($plant)) {
                $messages[] = $message;
            }
        }

        return empty($messages) || $this->setError($messages);
    }

    protected function validatePlant(?Plant $plant): ?string
    {
        if (!$plant) {
            return __('exceptions.plants.not_exist');
        }
        if (empty($plant->sync_code)) {
            return __('exceptions.plants.not_synced', ['sync_code' => $plant->sync_code]);
        }
        if ($plant->isDestroyed()) {
            return __('exceptions.plants.is_destroyed', ['sync_code' => $plant->sync_code]);
        }
        if ($plant->isHarvested()) {
            return __('exceptions.plants.is_harvested', ['sync_code' => $plant->sync_code]);
        }

        return null;
    }

    protected function setError($message): bool
    {
        $this->message = $message;
        return false;
    }

    public function message()
    {
        return $this->message;
    }
}

