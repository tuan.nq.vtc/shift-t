<?php
namespace Database\Seeders\DataCorrection;

use App\Models\License;
use Illuminate\Database\Seeder;

/**
 * Class UpdateLicenseCode.
 */
class UpdateLicenseCode extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        $usedCodes = [];
        
        License::query()->where('is_test', true)
            ->where('code', 'regexp', '^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$')
            ->get()
            ->each(function($license) use ($usedCodes) {
                $intent = 0;

                while(in_array($newCode = sprintf("%06d", mt_rand(100000, 999999)),$usedCodes) && $intent < 10) { $intent++; }
                if(in_array($newCode,$usedCodes)) {
                    return;
                }

                $usedCodes[] = $newCode;

                $license->code = $newCode;
                $license->save();
            });
    }
}
