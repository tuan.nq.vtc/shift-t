<?php

namespace App\Events\Harvest;

use App\Events\Event;
use App\Models\HarvestGroup;

class WetWeightConfirmed extends Event
{
    /**
     * @var HarvestGroup
     */
    public HarvestGroup $harvestGroup;

    /**
     * @param HarvestGroup $harvestGroup
     */
    public function __construct(HarvestGroup $harvestGroup)
    {
        $this->harvestGroup = $harvestGroup;
    }
}
