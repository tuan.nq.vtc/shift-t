<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Str;
use App\Models\{License, State, StateCategory};
use Illuminate\Database\Eloquent\Collection;

/**
 * Class LicenseService
 * @package App\Services
 */
class LicenseService extends BaseService
{
    public function __construct(License $license)
    {
        parent::__construct($license);
    }

    /**
     * @param string $organizationId
     * @return Collection
     */
    public function getByOrganization(string $organizationId): Collection
    {
        return License::where('organization_id', $organizationId)->get();
    }

    /**
     * @param string $stateCode
     * @param string $accountHolderId
     * @return Collection
     */
    public function getByState(string $stateCode, string $accountHolderId): Collection
    {
        return License::query()
            ->where('account_holder_id', $accountHolderId)
            ->where(fn($query) => $query->where('state_code', $stateCode)->orWhere('state_code', Str::upper($stateCode)))
            ->get();
    }

    public static function getSyncResourceConfigByModel($modelClass)
    {
        $syncResources = config('licenses.sync_resources');

        foreach ($syncResources as $syncResource) {
            if (!empty($syncResource['model']) && $syncResource['model'] === $modelClass) {
                return $syncResource;
            }
        }

        throw new Exception('Sync resource not found');
    }

    /**
     * @param License $license
     */
    public function syncStateCategories(License $license)
    {
        $regulator = optional($license->state)->regulator;
        $stateCategoryQuery = StateCategory::query()
            ->where('state_code', $license->state_code)
            ->where('regulator', $regulator);
        switch ($regulator) {
            case State::REGULATOR_LEAF:
                $stateCategoryQuery->whereNotNull('parent_id');
                break;
            case State::REGULATOR_METRC:
                break;
        }
        $license->stateCategories()->sync($stateCategoryQuery->pluck('id')->all());
    }
}
