<?php

namespace App\Http\Controllers;

use App\Models\{Disposal, Propagation, SeedToSale};
use Illuminate\Validation\Rule;
use App\Rules\{IsDisposalRoom, IsMotherPlant, SubroomBelongToLicense, TransplantQuantity};
use App\Services\GrowCycleService;
use App\Services\PropagationService;
use App\Services\RoomService;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Class PropagationController
 * @package App\Http\Controllers
 *
 * @property PropagationService $service
 */
class PropagationController extends CRUDController
{
    protected RoomService $roomService;
    protected GrowCycleService $growCycleService;

    public function __construct(
        RoomService $roomService,
        PropagationService $service,
        GrowCycleService $growCycleService,
        Request $request
    )
    {
        $this->roomService = $roomService;
        $this->growCycleService = $growCycleService;
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');

        return [
            'license_id' => [
                'required',
                'uuid',
            ],
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\Propagation,name,NULL,id,license_id,' . $licenseId,
            ],
            'mother_code' => [
                'nullable',
                new IsMotherPlant('sync_code', true, ['license_id' => $licenseId, 'destroyed' => false]),
            ],
            'strain_id' => [
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,' . $licenseId,
            ],
            'source_type' => [
                'required',
                Rule::in(SeedToSale::SOURCE_TYPES)
            ],
            'room_id' => [
                'required',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'subroom_id' => [
                'nullable',
                new SubroomBelongToLicense($roomId, $licenseId),
            ],
            'cut_by_id' => [
                'required',
                'uuid',
                'exists:App\Models\User,id',
            ],
            'plugged_by_id' => [
                'required',
                'uuid',
                'exists:App\Models\User,id',
            ],
            'quantity' => [
                'required',
                'numeric',
                'gt:0',
            ],
            'date' => [
                'required',
                'date',
            ],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');

        return [
            'name' => [
                'sometimes',
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\Propagation,name,' . $id . ',id,license_id,' . $licenseId,
            ],
            'mother_code' => [
                'nullable',
                new IsMotherPlant('sync_code', true, ['license_id' => $licenseId, 'destroyed' => false]),
            ],
            'strain_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,' . $licenseId,
            ],
            'source_type' => [
                'sometimes',
                'required',
                Rule::in(SeedToSale::SOURCE_TYPES)
            ],
            'room_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'subroom_id' => [
                'nullable',
                new SubroomBelongToLicense($roomId, $licenseId),
            ],
            'cut_by_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\User,id',
            ],
            'plugged_by_id' => [
                'sometimes',
                'required',
                'uuid',
                'exists:App\Models\User,id',
            ],
            'quantity' => [
                'sometimes',
                'required',
                'numeric',
                'gt:0',
            ],
            'date' => [
                'sometimes',
                'required',
                'date',
            ],
        ];
    }

    public function bulkCreate()
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');
        $rules = [
            'license_id' => [
                'required',
                'uuid',
            ],
            'propagations' => [
                'required',
                'array',
            ],
            'propagations.*.name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\Propagation,name,NULL,id,license_id,' . $licenseId,
            ],
            'propagations.*.mother_code' => [
                'sometimes',
                'required',
                'exists:App\Models\Plant,sync_code,license_id,' . $licenseId,
                new IsMotherPlant('sync_code', false, ['license_id' => $licenseId, 'destroyed' => false]),
            ],
            'propagations.*.strain_id' => [
                'required',
                'uuid',
                'exists:App\Models\Strain,id,license_id,' . $licenseId,
            ],
            'propagations.*.source_type' => [
                'required',
            ],
            'propagations.*.room_id' => [
                'required',
                'uuid',
                'exists:App\Models\Room,id',
            ],
            'propagations.*.subroom_id' => [
                'nullable',
                new SubroomBelongToLicense($roomId, $licenseId),
            ],
            'propagations.*.cut_by_id' => [
                'required',
                'uuid',
                'exists:App\Models\User,id',
            ],
            'propagations.*.plugged_by_id' => [
                'required',
                'uuid',
                'exists:App\Models\User,id',
            ],
            'propagations.*.quantity' => [
                'required',
                'numeric',
                'gt:0',
            ],
            'propagations.*.date' => [
                'required',
                'date',
            ],
        ];

        return response()->json(
            $this->service->bulkCreate(
                $this->validate($this->request, $rules)
            )
        );
    }

    /**
     * @param null $growCycleId
     *
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function moveToVegetation($growCycleId = null): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');

        $rules = [
            'license_id' => ['required'],
            'transplant' => ['required', 'array'],
            'transplant.*.propagation_id' => [
                'required',
                'exists:App\Models\Propagation,id,license_id,' . $licenseId,
            ],
            'transplant.*.quantity' => [
                'required',
                'numeric',
                'gt:0',
                new TransplantQuantity('propagation_id', true),
            ],
            'planted_at' => ['required', 'date'],
            'room_id' => [
                'required',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ],
            'subroom_id' => [
                'nullable',
                new SubroomBelongToLicense($roomId, $licenseId),
            ],
        ];
        $vegetationData = $this->validate($this->request, $rules);

        try {
            DB::beginTransaction();
            if ($growCycleId) {
                $growCycle = $this->growCycleService->get($growCycleId);
            } else {
                $growCycleRules = [
                    'license_id' => ['required'],
                    'name' => [
                        'required',
                        'string',
                        'max:150',
                        'unique:App\Models\GrowCycle,name,NULL,id,license_id,' . $licenseId,
                    ],
                    'est_harvested_at' => ['required', 'date'],
                ];
                $growCycle = $this->growCycleService->create(
                    array_merge(
                        $this->validate($this->request, $growCycleRules),
                        ['grow_status' => SeedToSale::GROW_STATUS_VEGETATIVE]
                    )
                );
            }

            $this->service->moveToVegetation($growCycle, $vegetationData);
            DB::commit();

            return response()->json($growCycle, Response::HTTP_CREATED);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     */
    public function bulkDestroy()
    {
        $rules = [
            'quantities' => ['required', 'array'],
            'quantities.*' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (empty($value['id'])
                        || !($propagation = Propagation::where('license_id', $this->request->get('license_id'))->find(
                            $value['id']
                        ))) {
                        $fail(__('exceptions.propagation.not_found'));
                        return false;
                    }

                    if (empty($value['quantity']) || $value['quantity'] > $propagation->quantity) {
                        $fail(__('exceptions.disposal.invalid_quantity'));
                        return false;
                    }

                    return true;
                },
            ],
            'scheduled_by_id' => ['required', 'uuid', 'exists:App\Models\User,id',],
            'reason' => ['required', 'in:' . implode(',', Disposal::REASONS)],
            'room_id' => ['required', new IsDisposalRoom($this->request->get('license_id'))],
            'comment' => ['nullable', 'string'],
            'quarantine_start' => ['date_format:Y-m-d H:i:s', 'required'],
            'license_id' => ['required'],
        ];
        $data = $this->validate($this->request, $rules);
        $propagations = $this->service->getEnumerable(Arr::pluck($data['quantities'], 'id'));
        $disposalRoom = $this->roomService->get(Arr::get($data, 'room_id'));

        return response()->json($this->service->bulkDestroy($propagations, $disposalRoom, $data));
    }
}
