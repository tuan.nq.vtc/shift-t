<?php

return [
    'strain' => [
        'sync_completed_title' => 'Strain Synced to :regulator',
        'sync_completed_message' => 'The strain :label were synced to :regulator',
        'updated_title' => 'Strain Updated',
        'updated_message' => ':first_name :last_name updated the strain :label',
        'update_sync_completed_title' => 'Strain Update Synced to :regulator',
        'update_sync_completed_message' => 'Updates to strain :label were synced to :regulator',
        'sync_failed_title' => 'Strains - API failure',
        'sync_failed_message' => 'Some changes to strains did not update to :regulator. Click here to view the issues in the Strain Module',
    ]
];
