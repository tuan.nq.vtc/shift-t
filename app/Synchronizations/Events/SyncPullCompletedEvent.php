<?php

namespace App\Synchronizations\Events;

use App\Events\Event;
use App\Models\Trace;

class SyncPullCompletedEvent extends Event
{
    /**
     * @var Trace
     */
    public Trace $trace;

    public function __construct(Trace $trace)
    {
        $this->trace = $trace;
    }
}
