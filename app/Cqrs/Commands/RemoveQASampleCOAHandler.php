<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Handleable;
use App\Cqrs\Values\ClientIdentifier;
use App\Models\QASampleCoa;
use Exception;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class RemoveQASampleCOAHandler extends Handleable
{
    /**
     * @var QASampleCoa
     */
    protected $qaSampleCoa;

    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * @var ClientIdentifier
     */
    protected $clientIdentifier;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $deleteResourceUrl;

    /**
     * @param QASampleCoa $qaSampleCoa
     * @param ClientInterface $httpClient
     * @param ClientIdentifier $clientIdentifier
     * @param LoggerInterface $logger
     * @param string $deleteResourceUrl
     */
    public function __construct(QASampleCoa      $qaSampleCoa,
                                ClientInterface  $httpClient,
                                ClientIdentifier $clientIdentifier,
                                LoggerInterface  $logger,
                                string           $deleteResourceUrl)
    {
        $this->qaSampleCoa = $qaSampleCoa;
        $this->httpClient = $httpClient;
        $this->clientIdentifier = $clientIdentifier;
        $this->deleteResourceUrl = $deleteResourceUrl;
        $this->logger = $logger;
    }

    /**
     * @throws Throwable
     */
    public function handle(RemoveQASampleCOACommand $command)
    {
        try {
            $coa = $this->qaSampleCoa->find($coaId = $command->getQaSampleCoaId());
            if ($coa instanceof QASampleCoa !== true) {
                throw new Exception(sprintf('The QA Sample COA \'%s\' not found', $coaId));
            }

            DB::beginTransaction();
            if (true !== $this->deleteFile($coa->getAttribute('filename'))) {
                throw new Exception('Delete COA file failed');
            }

            if (true !== $coa->delete()) {
                throw new Exception('Delete QA sample COA failed');
            }

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            $this->log($command, $e->getMessage(), $e->getTrace());
            throw $e;
        }
    }

    /**
     * @param string $fileName
     * @return bool
     */
    private function deleteFile(string $fileName): bool
    {
        try {
            return Storage::delete($fileName);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }
}
