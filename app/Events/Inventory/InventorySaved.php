<?php

namespace App\Events\Inventory;

use App\Events\Event;
use App\Models\HarvestWetWeight;
use App\Models\Inventory;

class InventorySaved extends Event
{
    /**
     * @var Inventory
     */
    public Inventory $inventory;

    /**
     * @param Inventory $inventory
     */
    public function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;
    }
}
