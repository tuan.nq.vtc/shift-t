<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Models\StateCategory;
use App\Services\{CategoryService, SourceCategoryService, StateCategoryService, TaxonomyService};
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class TaxonomyController extends Controller
{
    /**
     * @var Request
     */
    private Request $request;

    /**
     * @var TaxonomyService
     */
    private TaxonomyService $service;

    /**
     * @var StateCategoryService
     */
    private StateCategoryService $stateCategoryService;

    /**
     * @var SourceCategoryService
     */
    private SourceCategoryService $sourceCategoryService;

    /**
     * @var CategoryService
     */
    private CategoryService $categoryService;

    /**
     * @param TaxonomyService $service
     * @param StateCategoryService $stateCategoryService
     * @param SourceCategoryService $sourceCategoryService
     * @param CategoryService $categoryService
     * @param Request $request
     */
    public function __construct(
        TaxonomyService $service,
        StateCategoryService $stateCategoryService,
        SourceCategoryService $sourceCategoryService,
        CategoryService $categoryService,
        Request $request
    ) {
        $this->service = $service;
        $this->request = $request;
        $this->stateCategoryService = $stateCategoryService;
        $this->sourceCategoryService = $sourceCategoryService;
        $this->categoryService = $categoryService;
    }

    /**
     * @param string $type
     * @return JsonResponse
     * @throws Exception
     */
    public function getByType(string $type): JsonResponse
    {
        return response()->json($this->service->getByType($type));
    }

    public function getByCategory(string $type, string $id): JsonResponse
    {
        return response()->json($this->service->getByCategory($type, $id));
    }

    /**
     * @param string $type
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function assignByType(string $type): JsonResponse
    {
        $this->validate(
            $this->request,
            [
                'category_ids' => ['required', 'array'],
                'state_category_id' => ['required', 'uuid', 'exists:App\Models\StateCategory,id',],
            ]
        );

        $stateCategory = $this->stateCategoryService->get($this->request->get('state_category_id'));
        $categoryIds = $this->request->get('category_ids');
        switch ($type) {
            case StateCategory::TYPE_SOURCE:
                $categories = $this->sourceCategoryService->getByIds($categoryIds);
                if ($categories->count() === 0) {
                    throw new GeneralException(__('exceptions.taxonomy.no_assigned_categories'));
                }
                return response()->json($this->service->assignSource($categories, $stateCategory));
            case StateCategory::TYPE_END:
                return response()->json($this->service->assign($categoryIds, $stateCategory));
            default:
                throw new GeneralException(__('exceptions.taxonomy.invalid_type'));
        }
    }
}
