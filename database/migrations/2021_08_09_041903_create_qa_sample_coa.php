<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQaSampleCoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_sample_coa', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('filename');
            $table->uuid('qa_sample_id');
            $table->timestamp('uploaded_at');

            $table->foreign('qa_sample_id')
                ->references('id')
                ->on('qa_samples')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_sample_coa');
    }
}
