# API Leaf Data bang Washington

Tất cả các API, khi gọi đều cần truyền 2 tham số bắt buộc là `api_key` và `mme_code` vào header.

Ví dụ: Tham số cần truyền vào header: 
- api_key: Với api_key là `abc` tương ứng sẽ có header `-H  "x-mjf-key: abc"` khi dùng **curl**
- mme_code: Với mme_code là `xyz` tương ứng sẽ có header `-H  "x-mjf-mme-code: xyz"` khi dùng **curl**

Với các request thêm mới (**Create**) và cập nhật (**Update**), phần dữ liệu truyền lên (gửi đi) có thể là `xml`, `csv` bên cạnh định dạng `json`.
- Với định dạng `xml`, cần sửa lại content type trên header`-H "Content-Type: application/json"` thành `-H "Content-Type: application/xml"`
- Với định dạng `csv`, cần sửa lại content type trên header`-H "Content-Type: application/json"` thành `-H "Content-Type: application/csv"`


## 1. API

### 1.1. Area

#### Description

* Areas represent physical locations at licensed facilities where plants and inventory will be located.
The types of areas are 'quarantine' or 'non-quarantine'. 
Areas with a 'quarantine' designation are for circumstances such as waste/destruction hold periods,
QA quarantine periods, or transfer hold periods.
* `Các khu vực` đại diện cho các địa điểm nơi lưu trữ các `cây` và hàng tồn kho của các cơ sở được cấp phép .
Các loại khu vực là '`cách ly`' hoặc '`không cách ly`'.
Các khu vực có chỉ định '`cách ly`' dành cho các trường hợp như các khoảng thời gian lưu trữ chất thải / tiêu hủy,
thời gian kiểm tra chất lượng hoặc thời gian tạm giữ lúc vận chuyển.

#### Available Actions

##### Fetch:

  curl -X GET https://watest.leafdatazone.com/api/v1/areas \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json" -d ''


##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/batches \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"area" :[{
             "name": "Scott Vault",
             "type": "non-quarantine",
             "external_id": "Backroom vault"
         }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/areas/update \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"area" :{
             "name": "Scott Vault",
             "type": "quarantine",
             "external_id": "Frontroom Vault",
             "global_id": "WAL050505.AR6M"
         }}'

##### Delete:

    curl -X DELETE https://watest.leafdatazone.com/api/v1/areas/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


### 1.2. Batch

#### Descriptions

* Batch types include `propagation material`, `plant`, `harvest`, and `intermediate/end product`(called “extraction” on the back end).
* Các loại lô gồm có `vật liệu nhân giống`, `cây`, `việc thu hoạch`, và `các sản phẩm trung gian/thành phẩm` (còn gọi là  “chiết xuất”).

* 'Propagation Material' batches are used to create inventory lot of seeds, clones, and plant tissue so that these plants can be tracked as inventory throughout their propagation phase.
As plants shift from their propagation to vegetative phase, they are moved to `plants` (see `move_inventory_to_plants` API call), at which point the plant records are associated with a 'plant' type batch.
* Các lô `Vật liệu nhân giống` được sử dụng để tạo ra các lô hạt giống, vô tính và mô thực vật để những `cây` này có thể được theo dõi như là hàng tồn kho trong suốt giai đoạn nhân giống.
Khi `cây` chuyển từ giai đoạn nhân giống sang giai đoạn sinh trưởng, chúng được chuyển sang giai đoạn `cây` (xem API `move_inventory_to_plants`), tại thời điểm đó, hồ sơ `cây` được liên kết với lô loại '`cây`'.

* 'Plant' batches are a group of plants from the same strain, that are growing together within their vegetative and flowering phases. 
Attributes of all of the plants within a batch can be modified at the batch level, which will apply changes across all of the plant records.
Additionally, plant records can be modified individually (see the /plants endpoint).
* Các lô `Cây` là một nhóm `cây` thuộc cùng một chủng, đang phát triển trong cùng các giai đoạn sinh trưởng và ra hoa của chúng.
Các thuộc tính của tất cả các `cây` trong một lô có thể được sửa đổi ở cấp lô, các thay đổi sẽ đc áp dụng trên tất cả các hồ sơ `cây`.
Ngoài ra, hồ sơ `cây` có thể được sửa đổi riêng lẻ (xem API `1.10. Plant`).

* 'Harvest' batches represent a group of harvested material that is all of the same strain.
These types of batches are used to denote both 'wet' and 'dry' weight of 'flower' and 'other material' produced during the harvest.
Resultant dry weight from a harvest batch is separated into 'inventory lots'.
While initial inventory in a harvest stage can be created at the 'batch' endpoint, in a general workflow they are made by using the /harvest_plants API call
* Các lô `Thu hoạch` đại diện cho một nhóm nguyên liệu được thu hoạch là tất cả các chủng giống nhau.
Các loại lô này được sử dụng để biểu thị cả trọng lượng 'ướt' và 'khô' của 'hoa' và 'vật liệu khác' được tạo ra trong vụ thu hoạch.
Trọng lượng khô kết quả từ một đợt thu hoạch được tách thành 'lô hàng tồn kho'.
Mặc dù lượng tồn kho ban đầu trong giai đoạn thu hoạch có thể được tạo bằng cách gọi `/batches` API, nhưng trong quy trình công việc chung, chúng được tạo bằng cách gọi `/harvest_plants` API

* 'Intermediate/end product' (“extraction”) batches are batches that consist of multiple harvest batches being combined, for example, combining two different strains to make a blended concentrate product.
* Các lô `sản phẩm trung gian / thành phẩm` (của quá trình chiết xuất) là các lô bao gồm nhiều lô `thu hoạch` được kết hợp, ví dụ, kết hợp hai chủng khác nhau để tạo ra một sản phẩm cô đặc pha trộn.

* The purpose of using batches to group together plant and inventory records is two-fold.
Batches assist with creating the traceability that the system is designed to offer.
As well, batches allow producers to manage plants in any phase in groups, which enables mass actions to be applied to numerous records simultaneously.
Batches are not intended to constrain activities involving plant movement, as plants can be shifted from one batch to another and do not have exclusive relationships with batches they are added to.
* Mục đích của việc sử dụng các lô để nhóm các hồ sơ `tồn kho` và `cây` lại với nhau là hai lần.
Các lô đc tạo để hỗ trợ việc truy xuất nguồn gốc mà hệ thống được thiết kế để cung cấp.
Đồng thời, các lô cho phép các nhà sản xuất quản lý các `cây` theo bất kỳ giai đoạn nào trong các nhóm, điều này cho phép áp dụng nhiều hành động cho nhiều hồ sơ cùng một lúc.
Các lô không nhằm mục đích hạn chế các hoạt động liên quan đến việc chuyển dịch `cây`, vì `cây` có thể được chuyển từ lô này sang lô khác và không có mối quan hệ duy nhất với các `lô` mà chúng được thêm vào.

* Batch Lifecycle: Propagation material -> Plant -> Harvest -> Intermediate/End product 

#### Parameter List
* Danh sách tham số
    
   **Return params**: 
    - "area_name": "Storage Room" - Name of the batch located area
    - "batch_created_at": "2018-03-22 14:50:02"
    - "created_at": "03/22/2018 02:50pm"
    - "updated_at": "07/19/2018 11:29am"
    - "global_id": "WAG010101.BAR8"
    - "global_mme_id": "WASTATE1.MM18"
    - "global_user_id": "WASTATE1.US13"
    - "mme_name": "Training Producer"
    - "mme_code": "G010101"
    - "qty_packaged_by_product": "195.00"
    - "qty_packaged_flower": "98.00"
    - "status": "open"
    - "strain_name": "open"

    **Modifiable params**:
    - "external_id": "BATCH1234567" - An optional free-form field used to hold any identifying factors of a particular batch
    - "flower_dry_weight": "3970.00" - The total dry weight of the flower associated with the batch
    - "flower_waste": "0.00" - The total waste weight associated with "flower" produced from harvest
    - "flower_wet_weight": "100.00" - The total wet weight of the flower associated with the batch
    - (*)"global_area_id": "WAG010101.AR64"
    - "global_flower_area_id": "WAG010101.AR65"
    - "global_mother_plant_id": null
    - "global_other_area_id": "WAG010101.AR65
    - (*)"global_strain_id": "WAG010101.ST4Y"
    - "harvest_stage": "cure" - Only used for batches of "type" = "harvest": `wet`, `cure`, `finished`
    - (*)"harvested_at": "02/01/2018 12:34PM" - Only for `harvest` type
    - (*)"harvested_end_at": "02/01/2018 12:34PM" - Only for `harvest` type
    - "is_child_batch": "0"
    - "is_parent_batch": "0"
    - (*)"num_plants": 2 - The number of plants that are in the batch; only used for batches of "type" = "`propagation material`", "`plant`", and "`harvest`"
    - (*)"origin": "clone"  - Indicates propagation source of the batch; required for batches of type='plant' or '`propagation material`'
    - "other_dry_weight": "4000.00" - The total wet weight associated with "other material" produced from harvest
    - "other_waste": "0.00" - The total waste weight associated with "other material" produced from harvest
    - "other_dry_weight": "4000.00" - The total dry weight associated with "other material" produced from harvest
    - "packaged_completed_at": "02/01/2018 12:34PM" - The packaged date of the product; required for "`intermediate/ end product`" type
    - "plant_stage": "harvested" - Current development stage of the plants in the batch: `propagation source`, `growing`, `harvested`, `packaged`, `destroyed`
    - "type": "harvest" - Indicates the type of batch: `propagation material`, `plant`, `harvest`, `intermediate/ end product`
    - "uom": "gm" - The unit of measure used to quantify the quantity harvested for this batch (only used for `harvest` batches, should be set to "`gm`")
    
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    | Tên tham số                 |          Mô tả                                     | Kiểu dữ liệu   |         Dữ liệu phù hợp          |           Ví dụ                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *area_name                  | Name of the area where the batch is located        | varchar(255)   | up to 255 characters             |       "Storage Room"              |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *batch_created_at           | The date/time a batch was created                  | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *deleted_at                 | The date/time a batch was deleted                  | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | #est_harvest_at             | This parameter has been deprecated and will be     |                |                                  |                                   |
    |                             | removed in an upcoming release                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | external_id(*)              | An optional free-form field used to                | varchar(40)    | up to 40 characters              |   "BATCH123456789"                |
    |                             | hold any identifying factors of a particular batch |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | flower_dry_weight(*)        | The total dry weight of the flower associated      | decimal(10,2)  | 1234.56                          |   "1234.56"                       |
    |                             | with the batch                                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | flower_wet_weight(*)        | The total wet weight of the flower associated      | decimal(10,2)  | 1234.56                          |   "1234.56"                       |
    |                             | with the batch                                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~global_area_id(*)          | The global ID of the area where the batch          | varchar(255)   | WAX123456.AR1Z2Y3                |   "WAX123456.AR1Z2Y3"             |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *global_id                  | Auto-generated unique ID for the batch             | varchar(255)   | up to 255 characters             |   "WAX123456.AR1Z2Y3"             |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | global_mother_plant_id(*)   | For "propagation material" batches,                | varchar(255)   | up to 255 characters             |   "WAX123456.PL1Z2Y3"             |
    |                             | the global ID of the mother plant                  |                |                                  |                                   |
    |                             | from which the plants were derived                 |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *global_mme_id              | The global ID of the licensee that                 | varchar(255)   | up to 255 characters             |   "WAWA1.MM1Z2Y3"                 |
    |                             | the batch belongs to                               |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~global_strain_id(*)        | The global ID of the strain specific to the batch; | varchar(255)   | up to 255 characters             |   "WAX12346.ST1Z2Y3"              |
    |                             | **required for all batch types except              |                |                                  |                                   |
    |                             | "extraction"**,                                    |                |                                  |                                   |
    |                             | where strain-specificity is optional               |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *global_user_id             | The global ID of the user who created the batch    | varchar(255)   | up to 255 characters             |   "WAWA1.US1Z2Y3"                 |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~harvest_stage              | For "harvest" batches, the stage                   | enum(255)      | wet, cure, finished              |   "finished"                      |
    |                             | of the harvest process;                            |                |                                  |                                   |
    |                             | only used for batches of "type" = "harvest"        |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~harverst_at(*)             | For harvested batches, the date/time of harvest;   | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |                             | only required for batches of "type" = "harvest     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~harvested_end_at(*)        | The date/time at which the harvest                 | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |                             | of the batch ended;                                |                |                                  |                                   |
    |                             | only required for batches of "type" = "harvest"    |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~is_child_batch             | Indicates that this batch is                       | boolean        | 0,1                              |   "1"                             |
    |                             | the product of a previous batch (or batches)       |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~is_parent_batch            | Indicates that later generations of batches        | boolean        | 0,1                              |   "1"                             |
    |                             | have been created from this batch                  |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *mme_code                   | Licensee ID of the licensee that                   | varchar(255    | up to 255 characters             |   "X123456"                       |
    |                             | the batch belongs to                               |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *mme_name                   | Name of the licensee that the batch belongs to     | varchar(255    | up to 255 characters             |   "Training Producer"             |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *num_plants                 | The number of plants that are in the batch;        | varchar(255)   | up to 255 characters             |   "02/01/2018 12:34PM"            |
    |                             | only used for batches of "type" =                  |                |                                  |                                   |
    |                             | "propagation material", "plant", and "harvest"     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~origin(*)                  | Indicates propagation source of the batch          | enum           | seed, clone, plant, tissue       |   "clone"                         |
    |                             | only used for batches of "type" =                  |                |                                  |                                   |
    |                             | "propagation material", "plant", and "harvest"     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~other_dry_weight           | The total dry weight of the other material         | decimal(10,2)  | 1234.56                          |   "1234.56"                       |
    |                             | associated with the batch                          |                |                                  |                                   |   
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~other_wet_weight           | The total wet weight of the other material         | decimal(10,2)  | 1234.56                          |   "1234.56"                       |
    |                             | associated with the batch                          |                |                                  |                                   |   
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~packaged_completed_at      | For "extraction" batches, the date the product     | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |                             | was packaged                                       |                |                                  |                                   |   
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~plant_stage                | Current development stage of the plants            | enum           | propagation source, growing,     |   "02/01/2018 12:34PM"            |
    |                             | in the batch                                       |                | harvested, packaged, destroyed   |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | #planted_at                 | This parameter has been deprecated and will be     |                |                                  |                                   |
    |                             | removed in an upcoming release                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | #qty_accumulated_waste      | This parameter has been deprecated and will be     |                |                                  |                                   |
    |                             | removed in an upcoming release                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | #qty_cure                   | This parameter has been deprecated and will be     |                |                                  |                                   |
    |                             | removed in an upcoming release                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | #qty_harvest                | This parameter has been deprecated and will be     |                |                                  |                                   |
    |                             | removed in an upcoming release                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *qty_packaged_by_product    | Accumulated weight of the plant material           | decimal(10,2)  | 1234.56                          |   "1234.56"                       |
    |                             | that is classified as packaged                     |                |                                  |                                   |
    |                             | "other material"(in grams);                        |                |                                  |                                   |
    |                             | these values are derived from the total            |                |                                  |                                   |
    |                             | "other material" that has been created             |                |                                  |                                   |
    |                             | with the "finish batch" workflow function          |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *qty_packaged_flower        | Accumulated weight of the plant material           | decimal(10,2)  | 1234.56                          |   "1234.56"                       |
    |                             | that is classified as packaged                     |                |                                  |                                   |
    |                             | "flower"(in grams);                                |                |                                  |                                   |
    |                             | these values are derived from the total            |                |                                  |                                   |
    |                             | "other material" that has been created             |                |                                  |                                   |
    |                             | with the "finish batch" workflow function          |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | #source                     | This parameter has been deprecated and will be     |                |                                  |                                   |
    |                             | removed in an upcoming release                     |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *status                     | Identifier for the status of the batch             | enum           | open, closed                     |   "open"                          |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *strain_name                | Name of the strain associated with the batch       | varchar(255)   | up to 255 characters             |   "Dewberry Haze"                 |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | type(*)                     | Indicates the type of batch                        | enum           | propagation material, plant,     |   "harvest"                       |
    |                             | removed in an upcoming release                     |                | harvest, extraction              |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | uom(*)                      | The unit of measure used to quantify the quantity  | enum           | gm                               |   "gm"                            |
    |                             | harvested for this batch (only used for            |                |                                  |                                   |
    |                             | harvest batches, should be set to "gm")            |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | *updated_at                 | The date/time a batch was updated                  | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | ~waste                      | Accumulated weight of the plant material           | datetime       | mm/dd/yyyy hh:mmXM               |   "02/01/2018 12:34PM"            |
    |                             | that is represented as waste (in grams)            |                |                                  |                                   |
    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|


#### Filters Parameter
    - external_id
        ?f_external_id={external_id}
    - global_id
        ?f_global_id={global_id}
    - harvested_at
        ?f_harvested_at1={mm}%2F{dd}%2F{yyyy}&f_harvested_at2={mm}%2F{dd}%2F{yyyy}
    - planted_at
        ?f_planted_at1={mm}%2F{dd}%2F{yyyy}&f_planted_at2={mm}%2F{dd}%2F{yyyy}
    - status
        ?f_status={status}(does not work for batches of "type"="extraction")
    - type
        ?f_type={type}


#### Available Actions

##### Fetch:

    
    curl -X GET https://watest.leafdatazone.com/api/v1/batches \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json" -d ''


##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/batches \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"batch" :[{
             "type": "propagation material",
             "origin": "tissue",
             "global_area_id": "WAG050505.AR6M",
             "global_strain_id": "WAG050505.ST54",
             "num_plants": "35"
         }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/batches/update \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"batch" :{
            "global_id": "WAG050505.BAHJ",
            "plant_stage": "growing",
            "global_area_id": "WAG050505.AR6F"
         }}'

##### Delete:

    
    curl -X DELETE https://watest.leafdatazone.com/api/v1/batches/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''



### 1.3. Disposal

#### Description

#### Available Actions

##### Fetch:

    
    curl -X GET https://watest.leafdatazone.com/api/v1/disposals \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


##### Create:

    
    curl -X POST https://watest.leafdatazone.com/api/v1/disposals \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"disposal" :[{
             "external_id": "",
             "reason": "contamination",
             "method": "",
             "phase": "harvest",
             "type": "plant",
             "qty": "0.00",
             "uom": "gm",
             "source": "batch",
             "global_batch_id": "NVDDAA.BA2DQFA",
             "global_area_id": "",
             "global_plant_id": "",
             "global_inventory_id": "",
             "disposal_cert": "base64-encoded-file"
         }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/disposals/update \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"disposal" :{
             "external_id": "",
             "reason": "contamination",
             "method": "",
             "phase": "harvest",
             "type": "plant",
             "qty": "0.00",
             "uom": "gm",
             "source": "batch",
             "global_batch_id": "NVDDAA.BA2DQFA",
             "global_area_id": "",
             "global_plant_id": "",
             "global_inventory_id": "",
             "global_id": "NVDDAA.DI2DQFA",
             "disposal_cert": "base64-encoded-file"
         }}'

##### Delete:
   
    curl -X DELETE https://watest.leafdatazone.com/api/v1/disposals/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


### 1.4. Inventory Type

#### Description

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/inventory_types \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


#####Create:

    
    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_types \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory_type" :[{
            "external_id": "12345",
            "name": "Charlotte's Web Pre-Packs - 3.5gm",
            "type": "end_product",
            "intermediate_type": "usable_marijuana",
            "weight_per_unit_in_grams": 3.5,
            "uom": "ea"
         }]}'


##### Update:

    
    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_types/update \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory_type" :{
             "external_id": "12345",
             "name": "Charlotte's Web Pre-Packs - 3.5gm",
             "type": "end_product",
             "intermediate_type": "usable_marijuana",
             "uom": "ea",
             "weight_per_unit_in_grams": 3.5,
             "global_id": "WAG12345.TY2O"
         }}'

##### Delete:

    
    curl -X DELETE https://watest.leafdatazone.com/api/v1/inventory_types/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''



### 1.5. Inventory

#### Description

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/inventories \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''
    

##### Create:

    
    curl -X POST https://watest.leafdatazone.com/api/v1/inventories \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory" :[{
            "external_id": "12345",
            "is_initial_inventory": 1,
            "is_active": 1,
            "inventory_created_at": "12/01/2017",
            "inventory_packaged_at": "12/01/2017",
            "medically_compliant": 0,
            "qty": "1248.00",
            "uom": "gm",
            "global_batch_id": "WAG010101.BAH3",
            "global_area_id": "WAG010101.AR64",
            "global_strain_id": "WAG010101.ST4V",
            "global_inventory_type_id": "WAM030303.ITAH",
            "legacy_id": "1234567887654321"
         }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/inventories/update \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory" :{
             "external_id": "",
             "is_initial_inventory": 0,
             "is_active": 1,
             "inventory_created_at": "7/07/2017",
             "inventory_packaged_at": "07/07/2017",
             "medically_compliant": 0,
             "qty": "1357.00",
             "uom": "gm",
             "global_batch_id": "WAG010101.BA2BNA",
             "global_area_id": "WAG010101.AR18RL",
             "global_strain_id": "WAG010101.ST1X1L",
             "global_inventory_type_id": "WAG010101.TY40FP",
             "global_id": "WAG010101.INZA9"
         }}'

##### Delete:

    curl -X DELETE https://watest.leafdatazone.com/api/v1/inventories/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''



### 1.6. Inventory Adjustment

#### Description

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/inventory_adjustments \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_adjustments \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory_adjustment" :[{
             "external_id": "",
             "adjusted_at": "03/25/2017 10:55pm",
             "qty": "-2.00",
             "uom": "gm",
             "reason": "budtender_sample",
             "memo": "",
             "global_inventory_id": "WAG010101.INZFC",
             "global_adjusted_by_user_id": "WASTATE1.US3"
         }]}'



### 1.7. Inventory Transfer

#### Description

#### Available Actions


##### Fetch:

  curl -X GET https://watest.leafdatazone.com/api/v1/inventory_transfers \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_transfers \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory_transfer" :[{
             "manifest_type": "delivery",
             "multi_stop": "0",
             "external_id": "12345",
             "est_departed_at": "10/07/2017 02:00pm",
             "est_arrival_at": "10/07/2017 03:00pm",
             "vehicle_description": "blue mini van",
             "vehicle_license_plate": "RTE123",
             "vehicle_vin": "J1234567890",
             "global_to_mme_id": "WASTATE1.MM24M",
             "transporter_name1": "John",
             "transporter_name2": "",
             "inventory_transfer_items": [
                 {
                     "external_id": "",
                     "is_sample": 1,
                     "sample_type": "product_sample",
                     "product_sample_type": "budtender_sample",
                     "retest": 0,
                     "qty": "1.00",
                     "uom": "gm",
                     "global_inventory_id": "WAG010101.INZFC"
                 }
             ]
         }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/inventories/update \ 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"inventory_transfer" :{
             "manifest_type": "delivery",
             "multi_stop": "0",
             "external_id": "12345",
             "est_departed_at": "10/07/2017 02:00pm",
             "est_arrival_at": "10/07/2017 03:00pm",
             "vehicle_description": "blue mini van",
             "vehicle_license_plate": "RTE123",
             "vehicle_vin": "J1234567890",
             "global_to_mme_id": "WASTATE1.MM24M",
             "transporter_name1": "John",
             "transporter_name2": "",
             "global_id": "WAG010101.IT9GL",
             "inventory_transfer_items": [
                 {
                     "external_id": "",
                     "is_sample": 1,
                     "sample_type": "product_sample",
                     "product_sample_type": "budtender_sample",
                     "retest": 0,
                     "qty": "1.00",
                     "uom": "gm",
                     "global_inventory_id": "WAG010101.INZFC"
                 }
             ]
         }}'



### 8. Lab-Result

#### Description

#### Available Actions

##### Fetch:

    
    curl -X GET https://watest.leafdatazone.com/api/v1/lab_results 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


##### Create:

    
    curl -X POST https://watest.leafdatazone.com/api/v1/lab_results
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"lab_result" :[{
            "external_id": "test",
            "tested_at": "04/18/2018 12:34pm",
            "testing_status": "completed",
            "notes": "test notes",
            "received_at": "01/23/2018 4:56pm",
            "type": "harvest_materials",
            "intermediate_type": "flower_lots",
            "moisture_content_percent": "1",
            "moisture_content_water_activity_rate": ".635",
            "cannabinoid_editor": "WAWA1.US4",
            "cannabinoid_status": "completed",
            "cannabinoid_d9_thca_percent": "13.57",
            "cannabinoid_d9_thca_mg_g": null,
            "cannabinoid_d9_thc_percent": "24.68",
            "cannabinoid_d9_thc_mg_g": null,
            "cannabinoid_cbd_percent": "3.21",
            "cannabinoid_cbd_mg_g": null,
            "cannabinoid_cbda_percent": "1.23",
            "cannabinoid_cbda_mg_g": null,
            "microbial_editor": " WAWA1.US4",
            "microbial_status": "completed",
            "microbial_bile_tolerant_cfu_g": "0.00",
            "microbial_pathogenic_e_coli_cfu_g": "0.00",
            "microbial_salmonella_cfu_g": "0.00",
            "mycotoxin_editor": " WAWA1.US4",
            "mycotoxin_status": "completed",
            "mycotoxin_aflatoxins_ppb": "19.99",
            "mycotoxin_ochratoxin_ppb": "19.99",
            "metal_editor": "",
            "metal_status": "not_started",
            "metal_arsenic_ppm": null,
            "metal_cadmium_ppm": null,
            "metal_lead_ppm": null,
            "metal_mercury_ppm": null,
            "pesticide_editor": "",
            "pesticide_status": "not_started",
            "pesticide_abamectin_ppm": null,
            "pesticide_acephate_ppm": null,
            "pesticide_acequinocyl_ppm": null,
            "pesticide_acetamiprid_ppm": null,
            "pesticide_aldicarb_ppm": null,
            "pesticide_azoxystrobin_ppm": null,
            "pesticide_bifenazate_ppm": null,
            "pesticide_bifenthrin_ppm": null,
            "pesticide_boscalid_ppm": null,
            "pesticide_carbaryl_ppm": null,
            "pesticide_carbofuran_ppm": null,
            "pesticide_chlorantraniliprole_ppm": null,
            "pesticide_chlorfenapyr_ppm": null,
            "pesticide_chlorpyrifos_ppm": null,
            "pesticide_clofentezine_ppm": null,
            "pesticide_cyfluthrin_ppm": null,
            "pesticide_cypermethrin_ppm": null,
            "pesticide_daminozide_ppm": null,
            "pesticide_ddvp_dichlorvos_ppm": null,
            "pesticide_diazinon_ppm": null,
            "pesticide_dimethoate_ppm": null,
            "pesticide_ethoprophos_ppm": null,
            "pesticide_etofenprox_ppm": null,
            "pesticide_etoxazole_ppm": null,
            "pesticide_fenoxycarb_ppm": null,
            "pesticide_fenpyroximate_ppm": null,
            "pesticide_fipronil_ppm": null,
            "pesticide_flonicamid_ppm": null,
            "pesticide_fludioxonil_ppm": null,
            "pesticide_hexythiazox_ppm": null,
            "pesticide_imazalil_ppm": null,
            "pesticide_imidacloprid_ppm": null,
            "pesticide_kresoxim_methyl_ppm": null,
            "pesticide_malathion_ppm": null,
            "pesticide_metalaxyl_ppm": null,
            "pesticide_methiocarb_ppm": null,
            "pesticide_methomyl_ppm": null,
            "pesticide_methyl_parathion_ppm": null,
            "pesticide_mgk_264_ppm": null,
            "pesticide_myclobutanil_ppm": null,
            "pesticide_naled_ppm": null,
            "pesticide_oxamyl_ppm": null,
            "pesticide_paclobutrazol_ppm": null,
            "pesticide_permethrinsa_ppm": null,
            "pesticide_phosmet_ppm": null,
            "pesticide_piperonyl_butoxide_b_ppm": null,
            "pesticide_prallethrin_ppm": null,
            "pesticide_propiconazole_ppm": null,
            "pesticide_propoxur_ppm": null,
            "pesticide_pyrethrinsbc_ppm": null,
            "pesticide_pyridaben_ppm": null,
            "pesticide_spinosad_ppm": null,
            "pesticide_spiromesifen_ppm": null,
            "pesticide_spirotetramat_ppm": null,
            "pesticide_spiroxamine_ppm": null,
            "pesticide_tebuconazole_ppm": null,
            "pesticide_thiacloprid_ppm": null,
            "pesticide_thiamethoxam_ppm": null,
            "pesticide_trifloxystrobin_ppm": null,
            "solvent_editor": "",
            "solvent_status": "not_started",
            "solvent_acetone_ppm": null,
            "solvent_benzene_ppm": null,
            "solvent_butanes_ppm": null,
            "solvent_cyclohexane_ppm": null,
            "solvent_chloroform_ppm": null,
            "solvent_dichloromethane_ppm": null,
            "solvent_ethyl_acetate_ppm": null,
            "solvent_heptanes_ppm": null,
            "solvent_hexanes_ppm": null,
            "solvent_isopropanol_ppm": null,
            "solvent_methanol_ppm": null,
            "solvent_pentanes_ppm": null,
            "solvent_propane_ppm": null,
            "solvent_toluene_ppm": null,
            "solvent_xylene_ppm": null,
            "foreign_matter_stems": "1",
            "foreign_matter_seeds": "0",
            "test_for_terpenes": "0",
            "global_for_mme_id": "WAWA1.MM1VA",
            "global_inventory_id": "WAL400004.IN6I",
            "global_batch_id": "WAL400004.BA5A",
            "global_for_inventory_id": "WAG100001.IN6C"
        }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/lab_results/update
       -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
       -H "x-mjf-key: " -H "x-mjf-mme-code: "
       -H "Content-Type: application/json"
       -d '{"lab_result" :{
            "external_id": "test",
            "tested_at": "04/18/2018 12:34pm",
            "testing_status": "completed",
            "notes": "test notes",
            "received_at": "01/23/2018 4:56pm",
            "type": "harvest_materials",
            "intermediate_type": "flower_lots",
            "moisture_content_percent": "1",
            "moisture_content_water_activity_rate": ".635",
            "cannabinoid_editor": "WAWA1.US4",
            "cannabinoid_status": "completed",
            "cannabinoid_d9_thca_percent": "13.57",
            "cannabinoid_d9_thca_mg_g": null,
            "cannabinoid_d9_thc_percent": "24.68",
            "cannabinoid_d9_thc_mg_g": null,
            "cannabinoid_cbd_percent": "3.21",
            "cannabinoid_cbd_mg_g": null,
            "cannabinoid_cbda_percent": "1.23",
            "cannabinoid_cbda_mg_g": null,
            "microbial_editor": " WAWA1.US4",
            "microbial_status": "completed",
            "microbial_bile_tolerant_cfu_g": "0.00",
            "microbial_pathogenic_e_coli_cfu_g": "0.00",
            "microbial_salmonella_cfu_g": "0.00",
            "mycotoxin_editor": " WAWA1.US4",
            "mycotoxin_status": "completed",
            "mycotoxin_aflatoxins_ppb": "19.99",
            "mycotoxin_ochratoxin_ppb": "19.99",
            "metal_editor": "",
            "metal_status": "not_started",
            "metal_arsenic_ppm": null,
            "metal_cadmium_ppm": null,
            "metal_lead_ppm": null,
            "metal_mercury_ppm": null,
            "pesticide_editor": "",
            "pesticide_status": "not_started",
            "pesticide_abamectin_ppm": null,
            "pesticide_acephate_ppm": null,
            "pesticide_acequinocyl_ppm": null,
            "pesticide_acetamiprid_ppm": null,
            "pesticide_aldicarb_ppm": null,
            "pesticide_azoxystrobin_ppm": null,
            "pesticide_bifenazate_ppm": null,
            "pesticide_bifenthrin_ppm": null,
            "pesticide_boscalid_ppm": null,
            "pesticide_carbaryl_ppm": null,
            "pesticide_carbofuran_ppm": null,
            "pesticide_chlorantraniliprole_ppm": null,
            "pesticide_chlorfenapyr_ppm": null,
            "pesticide_chlorpyrifos_ppm": null,
            "pesticide_clofentezine_ppm": null,
            "pesticide_cyfluthrin_ppm": null,
            "pesticide_cypermethrin_ppm": null,
            "pesticide_daminozide_ppm": null,
            "pesticide_ddvp_dichlorvos_ppm": null,
            "pesticide_diazinon_ppm": null,
            "pesticide_dimethoate_ppm": null,
            "pesticide_ethoprophos_ppm": null,
            "pesticide_etofenprox_ppm": null,
            "pesticide_etoxazole_ppm": null,
            "pesticide_fenoxycarb_ppm": null,
            "pesticide_fenpyroximate_ppm": null,
            "pesticide_fipronil_ppm": null,
            "pesticide_flonicamid_ppm": null,
            "pesticide_fludioxonil_ppm": null,
            "pesticide_hexythiazox_ppm": null,
            "pesticide_imazalil_ppm": null,
            "pesticide_imidacloprid_ppm": null,
            "pesticide_kresoxim_methyl_ppm": null,
            "pesticide_malathion_ppm": null,
            "pesticide_metalaxyl_ppm": null,
            "pesticide_methiocarb_ppm": null,
            "pesticide_methomyl_ppm": null,
            "pesticide_methyl_parathion_ppm": null,
            "pesticide_mgk_264_ppm": null,
            "pesticide_myclobutanil_ppm": null,
            "pesticide_naled_ppm": null,
            "pesticide_oxamyl_ppm": null,
            "pesticide_paclobutrazol_ppm": null,
            "pesticide_permethrinsa_ppm": null,
            "pesticide_phosmet_ppm": null,
            "pesticide_piperonyl_butoxide_b_ppm": null,
            "pesticide_prallethrin_ppm": null,
            "pesticide_propiconazole_ppm": null,
            "pesticide_propoxur_ppm": null,
            "pesticide_pyrethrinsbc_ppm": null,
            "pesticide_pyridaben_ppm": null,
            "pesticide_spinosad_ppm": null,
            "pesticide_spiromesifen_ppm": null,
            "pesticide_spirotetramat_ppm": null,
            "pesticide_spiroxamine_ppm": null,
            "pesticide_tebuconazole_ppm": null,
            "pesticide_thiacloprid_ppm": null,
            "pesticide_thiamethoxam_ppm": null,
            "pesticide_trifloxystrobin_ppm": null,
            "solvent_editor": "",
            "solvent_status": "not_started",
            "solvent_acetone_ppm": null,
            "solvent_benzene_ppm": null,
            "solvent_butanes_ppm": null,
            "solvent_cyclohexane_ppm": null,
            "solvent_chloroform_ppm": null,
            "solvent_dichloromethane_ppm": null,
            "solvent_ethyl_acetate_ppm": null,
            "solvent_hexanes_ppm": null,
            "solvent_isopropanol_ppm": null,
            "solvent_methanol_ppm": null,
            "solvent_pentanes_ppm": null,
            "solvent_propane_ppm": null,
            "solvent_toluene_ppm": null,
            "solvent_xylene_ppm": null,
            "foreign_matter_stems": "1",
            "foreign_matter_seeds": "0",
            "test_for_terpenes": "0",
            "for_inventory_id": "45912",
            "updated_at": "04/19/2018 02:46pm",
            "created_at": "04/19/2018 02:46pm",
            "global_id": "WAL000555.LR2FH",
            "global_for_inventory_id": "WAG010101.INZFC",
            "global_mme_id": "WASTATE1.MM24N",
            "global_user_id": "WASTATE1.US2FE",
            "global_for_mme_id": "WASTATE1.MM24M",
            "global_inventory_id": "WAL000555.IN1008",
            "global_batch_id": "WAL000555.BA2CCN",
            "strain_name": ""
       }}'


##### Delete:

    curl -X DELETE https://watest.leafdatazone.com/api/v1/lab_results/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


### 9. MME

#### Description

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/mmes
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


### 10. Plant

#### Description

#### Available Actions

##### Fetch:

    
    curl -X GET https://watest.leafdatazone.com/api/v1/plants
       -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
       -H "x-mjf-key: " -H "x-mjf-mme-code: "
       -H "Content-Type: application/json"
       -d ''


##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/plants
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"plant" :[{
            "origin": "seed",
            "stage": "growing",
            "global_batch_id": "WAG010101.BADV"
        }]}'

##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/plants/update
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"plant" :[{
            "origin": "seed",
            "stage": "growing",
            "global_batch_id": "WAG010101.BADV"
        }]}'

##### Delete:

    curl -X DELETE https://watest.leafdatazone.com/api/v1/plants/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


### 11. Sale

#### Description

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/sales
       -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
       -H "x-mjf-key: " -H "x-mjf-mme-code: "
       -H "Content-Type: application/json"
       -d ''


##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/sales
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"sale" :[{
            "external_id": "12345",
            "type": "retail_recreational",
            "patient_medical_id": "",
            "caregiver_id": "",
            "sold_at": "12/01/2017",
            "price_total": "30.00",
            "status": "sale",
            "global_sold_by_user_id": "WAR030303.USA7G6",
            "sale_items": [
                {
                    "external_id": "12345",
                    "type": "sale",
                    "sold_at": "12/01/2017",
                    "qty": "2.00",
                    "uom": "ea",
                    "unit_price": "30.00",
                    "price_total": "60.00",
                    "name": "Dewberry Haze Pre-Packs 3.5gm",
                    "global_batch_id": "WAR030303.BAEV",
                    "global_inventory_id": "WAR030303.IN9A"
                }
            ]
         }]}'


##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/plants/update
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"sale" :{
            "global_id": "WAR123123.SA4B",
            "sale_items": [
                {
                    "global_id": "WAR123123.SI3Y",
                    "unit_price": 13
                }
            ]
         }}'



### 12. Strain

#### Description

* Strains represent specific sub-species of cannabis and are an attribute that can be designated to batches of inventory.
Batches of type "propagation_material", "plant", and "harvest" must have a strain assignment.
For "extraction"(intermediate/end product)type batches, a "non_strain_specific" designation is available for items that are no longer strain-specific.
* Các `chủng` đại diện cho các giống `cây` cụ thể và `chủng` là một thuộc tính đc chỉ định của các lô hàng tồn kho.
Lô của `vật liệu nhân giống`, `cây` và `thu hoạch` đều phải đc gán vào một `chủng`.
Với các kiểu `lô` sp trung gian/thành phẩm từ việc `chiết xuất`, có thể gán thuộc tính `ko thuộc một chủng cụ thể` cho các sp ko còn thuộc một `chủng` nào. 

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/strains
       -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
       -H "x-mjf-key: " -H "x-mjf-mme-code: "
       -H "Content-Type: application/json"
       -d ''

##### Create:

    curl -X POST https://watest.leafdatazone.com/api/v1/strains
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"strain" :[{
            "name": "Chem 91"
         }]}'

##### Update:

    curl -X POST https://watest.leafdatazone.com/api/v1/strains/update
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{"strain" :{
            "external_id": "1",
            "name": "Harlequin",
            "global_id": "WAG010101.ST8FX"
         }}'

##### Delete:

    curl -X DELETE https://watest.leafdatazone.com/api/v1/strains/{id}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (11KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''


### 13. User

#### Available Actions

##### Fetch:

    curl -X GET https://watest.leafdatazone.com/api/v1/strains
       -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
       -H "x-mjf-key: " -H "x-mjf-mme-code: "
       -H "Content-Type: application/json"
       -d ''


## 2. Workflow Functions

### 2.1. Conversion

    curl -X POST https://watest.leafdatazone.com/api/v1/conversions/create 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36" 
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "conversion": [
                {
                    "external_id": "777",
                    "global_inventory_type_id": "WAGJVGROW.IT69",
                    "global_area_id": "WAGJVGROW.AR24",
                    "global_strain_id": "WAGJVGROW.ST2I",
                    "uom": "gm",
                    "qty": "100",
                    "qty_waste_total": "3",
                    "started_at": "07/07/2017",
                    "finished_at": "07/07/2017",
                    "inventory_expires_at": "07/07/2017",
                    "product_not_altered": "0",
                    "additives": "",
                    "medically_compliant": null,
                    "inventories": [
                        {
                            "qty": "100",
                            "global_from_inventory_id": "WAGJVGROW.IN6F"
                        }
                    ]
                }
            ]
        }'

### 2.2. Dispose Item

    curl -X POST https://watest.leafdatazone.com/api/v1/disposals/dispose
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_id": "WAM200002.DI82A",
            "disposal_at": "01/20/2018 08:00am"
        }'

### 2.3. Harvest Batch

    curl -X POST https://watest.leafdatazone.com/api/v1/plants/harvest_plants
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "external_id": "3",
            "harvested_at": "05/08/2018",
            "qty_harvest": 134,
            "flower_wet_weight": 101,
            "other_wet_weight": 33,
            "uom": "gm",
            "global_area_id": "WAG100001.AR1R",
            "global_harvest_batch_id": "",
            "global_plant_ids": [
                {
                    "global_plant_id": "WAG100001.PLACI"
                },
                {
                    "global_plant_id": "WAG100001.PLACJ"
                }
            ]
        }'

### 2.4. Cure Batch

    curl -X POST https://watest.leafdatazone.com/api/v1/batches/cure_lot
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_batch_id": "WAG500014.BA6",
            "flower_dry_weight": "200",
            "other_dry_weight": "202",
            "flower_waste": "2.0",
            "other_waste": "2.2",
            "global_flower_area_id": "WAG500014.AR1J",
            "global_other_area_id": "WAG500014.AR1K"
        }'

### 2.5. Finish Batch

    curl -X POST https://watest.leafdatazone.com/api/v1/batches/finish_lot -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_batch_id": "WAG100001.BA4Y",
            "new_lot_types": [
                {
                    "global_inventory_type_id": "WAG100001.TY46",
                    "qty": "1800",
                    "global_area_id": "WALL.AR3Y",
                    "material_type": "flower",
                    "medically_compliant": "0"
                }
            ]
         }'

### 2.6. Inventory Transfer In Transit

    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_transfers/api_in_transit
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_id": "WAG100001.IT5FB"
         }'

### 2.7. Inventory Transfer Void

    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_transfers/void
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_id": "WAG100001.IT5FB"
         }'

### 2.8. MME Find

    curl -X GET https://watest.leafdatazone.com/api/v1/mme/{code}
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''

### 2.9. Move Inventory To Plants

    curl -X POST https://watest.leafdatazone.com/api/v1/move_inventory_to_plants 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_inventory_id": "WAG100001.INAJ2",
            "global_batch_id": "leave blank to create new batch, or add to existing",
            "qty": "5"
        }'

### 2.10. Move Plants To Inventory

    curl -X POST https://watest.leafdatazone.com/api/v1/move_plants_to_inventory
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_plant_ids": [
                "WAG100001.PLABA",
                "WAG100001.PLABB",
                "WAG100001.PLABC"
            ],
            "global_inventory_type_id": "{enter global id or leave blank to create new}",
            "global_area_id": "WAG100001.AR1M"
        }'

### 2.11. Plants By Area

    curl -X GET https://watest.leafdatazone.com/api/v1/plants_by_area 
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d ''

### 2.12. Receive Transfer

    curl -X POST https://watest.leafdatazone.com/api/v1/inventory_transfers/api_receive
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: "
         -H "Content-Type: application/json"
         -d '{
            "global_id": "WAG100001.IT5P",
            "inventory_transfer_items": [
                {
                    "global_id": "WAG100001.II7F",
                    "received_qty": "2599.00",
                    "global_received_area_id": "WAM200002.AR24",
                    "global_received_strain_id": "WAM200002.ST20",
                    "global_received_inventory_type_id": "WAM200002.TY5T"
                }
            ]
        }'

### 2.13 Split Inventory

    curl -X POST https://watest.leafdatazone.com/api/v1/split_inventory
         -A Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
         -H "x-mjf-key: " -H "x-mjf-mme-code: " 
         -H "Content-Type: application/json"
         -d '{
        "global_inventory_id": "WAG100001.IN61",
        "global_area_id": "WAG100001.AR1R",
        "external_id": "SPLIT123",
        "qty": "456",
        "net_weight": "",
        "cost": ""
    }'
