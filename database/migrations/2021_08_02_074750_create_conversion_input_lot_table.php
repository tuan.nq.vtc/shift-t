<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversionInputLotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_input_lot', function (Blueprint $table) {
            $table->uuid('conversion_id')->index();
            $table->uuid('inventory_id')->index();
            $table->uuid('product_id')->index();
            $table->double('quantity', 10, 2)->default(0);
            $table->nullableTimestamps();

            $table->primary(['conversion_id', 'inventory_id'], 'conversion_input_lot_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion_input_lot');
    }
}
