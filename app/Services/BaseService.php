<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\BaseModel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection, Model, ModelNotFoundException};
use Illuminate\Support\{Enumerable, Str};
use Throwable;

/**
 * Class BaseService
 *
 * @package App\Services
 *
 * @property BaseModel|Builder $model
 */
abstract class BaseService
{
    /**
     * @var BaseModel
     */
    protected BaseModel $model;
    protected array $relations = [];
    protected array $counts = [];
    protected string $licenseId = '';

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    public function getModel(): BaseModel
    {
        return $this->model;
    }

    /**
     * @param $licenseId
     *
     * @return $this
     */
    public function setLicenseId($licenseId)
    {
        $this->licenseId = $licenseId;

        return $this;
    }

    /**
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $query = $this->newQuery(false)->scopes($this->loadScopes($criteria->getFilters()));

        return $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->relations)
            ->withCount($this->counts)
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());
    }

    /**
     * @param bool $withoutGlobalOrder
     * @param bool $withLicenseFilter
     *
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    protected function newQuery($withoutGlobalOrder = true, $withLicenseFilter = true)
    {
        $query = $this->model;
        if ($withoutGlobalOrder) {
            $query->withoutGlobalScopes([BaseModel::DEFAULT_ORDER_SCOPE]);
        }
        if ($withLicenseFilter) {
            return $query->scopes($this->loadScopes(['licenseId' => $this->licenseId]));
        }

        return $query;
    }

    /**
     * @param $scopes
     *
     * @return array
     */
    protected function loadScopes($scopes): array
    {
        $namedScoped = [];
        foreach ($scopes as $name => $args) {
            $scopeName = Str::camel($name);
            if (!$this->model->hasNamedScope($scopeName) || is_null($args)) {
                continue;
            }
            $namedScoped[$scopeName] = $args;
        }

        return $namedScoped;
    }

    /**
     * @param $query
     * @param $orderBys
     *
     * @return Builder
     */
    protected function applyOrderBy(Builder $query, $orderBys): Builder
    {
        $orderByScopes = [];
        $isOrderedBy = false;
        foreach ($orderBys as $column => $direction) {
            if (!in_array($direction, ['asc', 'desc'], true)) {
                continue;
            }
            $scopeName = 'OrderBy'.Str::camel($column);
            if ($this->model->hasNamedScope($scopeName)) {
                $orderByScopes[$scopeName] = $direction;
            } else {
                $query->orderBy($column, $direction);
            }
            $isOrderedBy = true;
        }
        if ($isOrderedBy) {
            $query->withoutGlobalScope('defaultOrder');
        }

        return $query->scopes($orderByScopes);
    }

    /**
     * @param string $id
     * @param bool $withRelation
     *
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function get(string $id, $withRelation = true)
    {
        $query = $this->newQuery();
        if ($withRelation) {
            $query->with($this->relations);
        }

        return $query->findOrFail($id);
    }

    /**
     * @param array $ids
     * @param array $relations
     * @param array $counts
     * @return Collection
     */
    public function getByIds(array $ids, $relations = [], $counts = []): Collection
    {
        $query = $this->newQuery(false)->whereIn('id', $ids);
        if (!empty($relations)) {
            $query->with($relations);
        }
        if (!empty($counts)) {
            $query->withCount($counts);
        }

        return $query->get();
    }

    /**
     * @param array $ids
     * @param array $relations
     * @param array $counts
     * @return Enumerable
     */
    public function getEnumerable(array $ids, $relations = [], $counts = []): Enumerable
    {
        $query = $this->newQuery(false)->whereIn('id', $ids);
        if (!empty($relations)) {
            $query->with($relations);
        }
        if (!empty($counts)) {
            $query->withCount($counts);
        }

        return $query->cursor();
    }

    /**
     * @param array $data
     *
     * @return Model
     *
     * @throws GeneralException
     */
    public function create(array $data): Model
    {
        if ($this->licenseId) {
            $data['license_id'] = $this->licenseId;
        }
        return tap(
            $this->newQuery()->create($data),
            function ($instance) {
                if (!$instance) {
                    throw new GeneralException(__('exceptions.actions.create_failed'));
                }
            }
        )->fresh($this->relations);
    }

    /**
     * @param $id
     * @param array $data
     *
     * @return Model
     *
     * @throws ModelNotFoundException|GeneralException
     */
    public function update($id, array $data): Model
    {
        $model = tap(
            $this->newQuery()->findOrFail($id),
            function ($instance) use ($data) {
                if (!$instance->update($data)) {
                    throw new GeneralException(__('exceptions.actions.update_failed'));
                }
            }
        );

        return $model->load($this->relations);
    }

    /**
     * @param $id
     *
     * @throws ModelNotFoundException|GeneralException
     */
    public function delete($id): void
    {
        $model = $this->newQuery()->findOrFail($id);
        try {
            $model->delete();
        } catch (Throwable $e) {
            throw new GeneralException(__('exceptions.actions.delete_failed'));
        }
    }
}
