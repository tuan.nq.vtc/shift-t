<?php

namespace App\Rules;

use App\Models\AdditiveInventory;
use Illuminate\Contracts\Validation\Rule;

class AdditiveQuantity implements Rule
{
    protected $value;
    protected $message;
    protected string $licenseId;

    public function __construct(string $licenseId)
    {
        $this->licenseId = $licenseId;
    }

    public function passes($attribute, $value)
    {
        $this->value = $value;
        if (!is_array($value) && !isset($value['id']) || !isset($value['applied_quantity'])) {
            $this->message = "{$attribute} has wrong format";
            return false;
        }

        if (AdditiveInventory::whereId($value['id'])->licenseId($this->licenseId)
                ->where('available_quantity', '>=', $value['applied_quantity'])->count() <= 0) {
            $this->message = __(
                'validation.quantity.insufficient',
                ['entity' => 'additive inventory', 'value' => $this->value['applied_quantity']]
            );
            return false;
        }

        return true;
    }

    public function message()
    {
        return $this->message;
    }
}

