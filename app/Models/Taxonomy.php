<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Taxonomy extends BaseModel
{
    use HasInternalId;

    protected $fillable = [
        'license_id',
        'state_category_id',
        'category_type',
        'category_id',
    ];

    public function stateCategory(): BelongsTo
    {
        return $this->belongsTo(StateCategory::class, 'state_category_id', 'id');
    }

    public function category(): MorphTo
    {
        return $this->morphTo('category');
    }
}
