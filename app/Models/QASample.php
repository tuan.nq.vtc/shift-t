<?php

namespace App\Models;

use App\Models\Traits\{BelongToStrain,
    HasInternalId,
    Scope\CreatorFilterable,
    Scope\DatetimeFilterable,
    Scope\LicenseFilterable
};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany, HasOne};
use Illuminate\Support\Carbon;

/**
 * Class QASample
 * @property Inventory $sourceInventory
 * @package App\Models
 */
class QASample extends BaseModel
{
    use LicenseFilterable;
    use BelongToStrain;
    use CreatorFilterable;
    use DatetimeFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    public const ANALYTICS_TYPE_TOTAL_CANNABINOID = 'total_cannabinoid';
    public const ANALYTICS_TYPE_TOTAL_TERPENES = 'total_terpenes';
    public const ANALYTICS_TYPE_TOTAL_PULEGONE = 'pulegone';


    public const ANALYTICS_TYPES = [
        self::ANALYTICS_TYPE_TOTAL_CANNABINOID,
        self::ANALYTICS_TYPE_TOTAL_TERPENES,
        self::ANALYTICS_TYPE_TOTAL_PULEGONE,
    ];

    const SAMPLE_MANDATORY = 0;
    const SAMPLE_NON_MANDATORY = 1;

    const STATUS_OPEN = 0;
    const STATUS_AWAITING_RESULT = 1;
    const STATUS_PASSED = 2;
    const STATUS_FAILED = 3;

    /**
     * @var string
     */
    protected $table = 'qa_samples';

    /**
     * @var string[]
     */
    protected $fillable = [
        'license_id',
        'sample_type',
        'lab_id',
        'source_inventory_id',
        'manifest_id',
        'strain_id',
        'weight',
        'status',
        'quantity',
        'tested_at',
        'created_by',
        'updated_by',
        'results',
        'analysis_type',
    ];

    public $casts = [
        'results' => 'array',
    ];

    /**
     * @param $key
     * @return int
     */
    public static function getResultStatus($key): int
    {
        $resultStatues = [
            'passed' => self::STATUS_PASSED,
            'failed' => self::STATUS_FAILED,
        ];

        if (!array_key_exists($key, $resultStatues)) {
            return self::STATUS_AWAITING_RESULT;
        }

        return $resultStatues[$key];
    }

    /**
     * @return string[]
     */
    public static function getSampleTypeList(): array
    {
        return [
            self::SAMPLE_MANDATORY,
            self::SAMPLE_NON_MANDATORY,
        ];
    }

    /**
     * @return string[]
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_OPEN,
            self::STATUS_AWAITING_RESULT,
            self::STATUS_PASSED,
            self::STATUS_FAILED,
        ];
    }

    /**
     * @return BelongsTo
     */
    public function sourceInventory(): BelongsTo
    {
        return $this->belongsTo(Inventory::class, 'source_inventory_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function inventories(): HasMany
    {
        return $this->hasMany(Inventory::class, 'final_qa_sample_id', 'id');
    }

    /**
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByDateCreated(Builder $query, $direction): Builder
    {
        return $query->orderBy('created_at', $direction);
    }

    /**
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByWeight(Builder $query, $direction): Builder
    {
        return $query->orderBy('weight', $direction);
    }

    /**
     * @param Builder $query
     * @param $direction
     * @return Builder
     */
    public function scopeOrderByStatus(Builder $query, $direction): Builder
    {
        return $query->orderBy('status', $direction);
    }

    /**
     * @return HasOne
     */
    public function lab(): HasOne
    {
        return $this->hasOne(Licensee::class, 'id', 'lab_id');
    }

    /**
     * @return HasMany
     */
    public function coas(): HasMany
    {
        return $this->hasMany(QASampleCoa::class, 'qa_sample_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function manifest(): HasOne
    {
        return $this->hasOne(Manifest::class, 'id', 'manifest_id');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeHasManifest(Builder $query, $value): Builder
    {
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);

        if (true === $value) {
            return $query->whereNotNull('manifest_id')
                ->where('manifest_id', '<>', '');
        }

        return $query->whereNull('manifest_id')
            ->orWhere('manifest_id', '');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeStatus(Builder $query, $value): Builder
    {
        return $query->where('status', $value);
    }

    /**
     * @param Builder $query
     * @param ...$value
     * @return Builder
     */
    public function scopeStatuses(Builder $query, ...$value): Builder
    {
        return $query->whereIn('status', $value);
    }

    /**
     * @param Builder $query
     * @param $status
     * @return Builder
     */
    public function scopeSyncStatus(Builder $query, $status): Builder
    {
        return $query->whereHas('inventory', fn($subQuery) => $subQuery->where('sync_status', $status));
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeStrain(Builder $query, $value): Builder
    {
        return $query->whereHas('inventory.strain', fn($subQuery) => $subQuery->where('strain_id', $value));
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeLabId(Builder $query, $value): Builder
    {
        return $query->where('lab_id', trim($value));
    }

    /**
     * @param Builder $query
     * @param ...$value
     * @return Builder
     */
    public function scopeLabIs(Builder $query, ...$value): Builder
    {
        return $query->whereIn('lab_id', $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeInventoryTypeId(Builder $query, $value): Builder
    {
        return $query->whereHas('inventory.type', fn($subQuery) => $subQuery->whereId($value));
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeSampleType(Builder $query, $value): Builder
    {
        return $query->where('sample_type', $value);
    }

    /**
     * @param Builder $query
     * @param ...$values
     * @return Builder
     */
    public function scopeSampleTypes(Builder $query, ...$values): Builder
    {
        return $query->whereIn('sample_type', $values);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeHasLabTest(Builder $query): Builder
    {
        return $query->whereHas('lab');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeDateFrom(Builder $query, $value): Builder
    {
        return $this->buildQueryDateRanger($query, $value, null, 'created_at');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeDateTo(Builder $query, $value): Builder
    {
        return $this->buildQueryDateRanger($query, null, $value, 'created_at');
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeDateCreated(Builder $query, $value): Builder
    {
        return $query->whereDate('created_at', '=', Carbon::createFromDate($value));
    }

    /**
     * @param Builder $query
     * @param $value
     * @return Builder
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query->whereHas('qaSampleResults',
            fn(Builder $subQuery) => $subQuery->where('sync_code', 'like', '%' . $value . '%')
        );
    }

    /**
     * @param $inventoryId
     * @return int
     */
    public function countByInventoryId($inventoryId): int
    {
        return $this->where('source_inventory_id', $inventoryId)->count();
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}
