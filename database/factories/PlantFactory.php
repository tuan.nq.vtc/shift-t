<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{GrowCycle, Plant, Propagation, Room, SeedToSale, Strain, Subroom, TraceableModel};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Plant::class,
    function (Faker $faker) {
        return [
            "license_id" => $faker->uuid,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Plant::class,
                $data['license_id']
            ),
            'strain_id' => function ($plant) {
                return factory(Strain::class)->create(['license_id' => $plant['license_id']])->id;
            },
            'grow_status' => $faker->randomElement(SeedToSale::GROW_CYCLE_GROW_STATUSES),
            'source_type' => $faker->randomElement(SeedToSale::SOURCE_TYPES),
            'propagation_id' => function ($plant) {
                return factory(Propagation::class)->create(['license_id' => $plant['license_id']])->id;
            },
            'room_id' => fn($data) => factory(Room::class)->create(['license_id' => $data['license_id']])->id,
            'subroom_id' => fn($data) => factory(Subroom::class)->create(['room_id' => $data['room_id']])->id,
            'grow_cycle_id' => fn($data) => factory(GrowCycle::class)->create(
                ['license_id' => $data['license_id']]
            )->id,
            'planted_at' => $faker->dateTimeThisYear(),
            'sync_code' => 'WAG010101.PL' . $faker->regexify('[A-Z0-9]{4}'), // WAG010101.PL15AU
            'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
            'synced_at' => $faker->dateTimeThisYear(),
        ];
    }
);
