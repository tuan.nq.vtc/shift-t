<?php

namespace App\Models;

use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Events\TraceableModel\{TraceableModelCreated, TraceableModelDeleted, TraceableModelUpdated};
use App\Models\Traits\{Scope\LicenseFilterable, WithoutTraceableModelEvents, WithoutEvents};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, Relations\MorphMany, SoftDeletes};
use Illuminate\Support\Carbon;

/**
 * Class TraceableModel
 * @method static Builder licenseId($licenseId)
 * @package App\Models
 */
abstract class TraceableModel extends BaseModel implements TraceableModelInterface
{
    use LicenseFilterable;
    use SoftDeletes;
    use WithoutEvents;
    use WithoutTraceableModelEvents;

    protected array $traceable = [];

    protected $dispatchesEvents = [
        'created' => TraceableModelCreated::class,
        'updated' => TraceableModelUpdated::class,
        'deleted' => TraceableModelDeleted::class,
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->append(['is_synced']);
    }

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    /**
     * Morph many relation with Trace model
     *
     * @return MorphMany
     */
    public function traces(): MorphMany
    {
        return $this->morphMany(Trace::class, 'resource');
    }

    public function isSyncing(): bool
    {
        return $this->sync_status === self::SYNC_STATUS_PROCESSING;
    }

    public function isSyncSucceed(): bool
    {
        return $this->sync_status === self::SYNC_STATUS_SYNCED;
    }

    public function isSyncFailed(): bool
    {
        return $this->sync_status === self::SYNC_STATUS_FAILED;
    }

    public function updateSyncStatus(int $status): TraceableModelInterface
    {
        static::updateWithoutTraceableModelEvents(['sync_status' => $status, 'synced_at' => Carbon::now(),]);

        return $this;
    }

    public function updateSyncCode(string $code): TraceableModelInterface
    {
        static::updateWithoutTraceableModelEvents(['sync_code' => $code,]);

        return $this;
    }

    public function getLicenseCodeAttribute(): ?string
    {
        return $this->license ? $this->license->code : null;
    }

    public function getIsSyncedAttribute(): bool
    {
        return $this->isSyncSucceed();
    }

    /**
     * Override to fill common columns
     *
     * @return array
     */
    public function getFillable()
    {
        $fillable = [
            'license_id',
            'sync_status',
            'synced_at',
        ];

        $this->mergeFillable(array_diff($fillable, $this->fillable));

        return parent::getFillable();
    }

    /**
     * @return array
     */
    public function getTraceable(): array
    {
        return $this->traceable;
    }

    /**
     * @return bool
     */
    public function isTraceUpdatable(): bool
    {
        return !empty(array_intersect(array_keys($this->changes), $this->traceable));
    }

    /**
     * Override to push date columns to date casted list
     *
     * @return array
     */
    public function getDates()
    {
        if (!in_array('synced_at', $this->dates)) {
            array_push($this->dates, 'synced_at');
        }

        return parent::getDates();
    }

    /**
     * Scope a query to only include records have sync status that contains $status.
     *
     * @param Builder $query
     * @param int $status
     *
     * @return Builder
     */
    public function scopeSyncStatus(Builder $query, int $status): Builder
    {
        return $query->where('sync_status', $status);
    }

    /**
     * Scope a query to only include records have sync code that contains $code.
     *
     * @param Builder $query
     * @param string $code
     *
     * @return Builder
     */
    public function scopeSyncCode(Builder $query, string $code): Builder
    {
        return $query->where('sync_code', $code);
    }

    /**
     * Scope a query to only include records have been synced.
     *
     * @param Builder $query
     * @param bool $isSynced
     *
     * @return Builder
     */
    public function scopeIsSynced(Builder $query, bool $isSynced): Builder
    {
        return $isSynced ?
            $query->where('sync_status', TraceableModel::SYNC_STATUS_SYNCED) :
            $query->where('sync_status', '!=', TraceableModel::SYNC_STATUS_SYNCED);
    }

    /**
     * Return name when property name exists else return default
     *
     * @return string
     */
    public function __toString(): string
    {
        if (property_exists($this, 'name')) {
            return $this->name;
        }

        return parent::__toString();
    }

    public function scopeOrderByIsSynced(Builder $query, $direction = 'asc'): Builder
    {
        return $query->orderBy('sync_status', $direction);
    }
}
