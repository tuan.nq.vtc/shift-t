<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\AccountHolderFilterable;
use App\Models\Traits\Scope\IdsFilterable;
use App\Models\Traits\Scope\NameFilterable;
use App\Models\Traits\Scope\PrintLabelFilterable;
use App\Models\Traits\Scope\StatusFilterable;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PrintLabel
 * @package App\Models
 *
 * @property int id
 * @property int license_id
 * @property int print_label_template_id
 * @property string category
 * @property string name
 * @property string base64
 * @property array|null $box
 * @property int status
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class PrintLabel extends BaseModel
{
    use SoftDeletes;
    use NameFilterable;
    use StatusFilterable;
    use PrintLabelFilterable;
    use AccountHolderFilterable;
    use IdsFilterable;
    use HasInternalId;
    
    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const CATEGORIES = ['plants', 'product', 'propagation', 'inventory', 'sales_orders','waste'];

    protected $table = 'print_labels';

    protected $fillable = [
        'account_holder_id',
        'print_label_template_id',
        'category',
        'name',
        'base64',
        'box',
        'status',
    ];

    protected $casts = [
        'status' => 'bool',
        'box' => 'array',
    ];

    public function items(): HasMany
    {
        return $this->hasMany(PrintLabelItem::class);
    }

    public function template(): BelongsTo
    {
        return $this->belongsTo(PrintLabelTemplate::class);
    }

     /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        if (empty($value)) {
            return $query;
        }

        return $query->where('name', 'like', '%'.$value.'%');
    }
}
