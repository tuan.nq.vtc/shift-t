<?php

namespace App\Services;

use App\Events\Disposal\DisposalDestroyed;
use App\Http\Parameters\Criteria;
use App\Models\{Disposal, WasteBag};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection, Relations\MorphTo};
use Illuminate\Support\{Arr, Carbon, Enumerable, Facades\DB, Facades\Event};
use Throwable;

/**
 * Class DisposalService
 * @package App\Services
 *
 * @method Disposal|Collection get($id)
 * @method Disposal|Builder getModel()
 * @method Enumerable|Disposal[] getEnumerable(array $disposalIds, $relations = [], $counts = [])
 */
class DisposalService extends BaseService
{
    protected array $relations = [
        'strain:id,name',
        'room:id,name',
        'scheduledBy',
        'destroyedBy',
        'wastable',
        'strain'
    ];

    public function __construct(Disposal $disposal)
    {
        parent::__construct($disposal);
    }

    /**
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $this->relations = array_merge($this->relations, [
            'wastable' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    WasteBag::class => ['strains'],
                ]);
            },
        ]);

        return parent::list($criteria);
    }

    /**
     * @return mixed
     */
    public function changeStatusScheduledDisposal()
    {
        $disposals = Disposal::where('quarantine_end', '<=', Carbon::now())
            ->where('status', Disposal::STATUS_SCHEDULED)->get();

        return $disposals->map(function(Disposal $disposal) {
            return $disposal->update(['status' => Disposal::STATUS_READY]);
        });
    }

    public function destroy(Enumerable $disposals, array $data)
    {
        try {
            DB::beginTransaction();
            $disposals = $disposals->map(
                function (Disposal $disposal) use ($data) {
                    $disposal->update(
                        array_merge(
                            Arr::only($data, ['destroyed_at', 'destroyed_by_id']),
                            ['status' => Disposal::STATUS_DESTROYED]
                        )
                    );
                    // dispatch disposal destroyed
                    Event::dispatch(new DisposalDestroyed($disposal));
                }
            );
            DB::commit();
            return $disposals;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function unschedule(array $data)
    {
        try {
            DB::beginTransaction();
            foreach ($data['disposals'] as $disposalData) {
                $disposal = Disposal::query()->findOrFail($disposalData['id']);
                $disposal->update(
                    [
                        'comment_unschedule' => $data['comment'],
                        'status' => Disposal::STATUS_UNSCHEDULED,
                        'cancelled_at' => Carbon::now(),
                    ]
                );
                $disposal->wastable()
                    ->update(
                        [
                            'subroom_id' => $disposalData['subroom_id'],
                            'grow_cycle_id' => $disposalData['grow_cycle_id'],
                            'destroyed' => false
                        ]
                    );
            }
            DB::commit();
            return $data;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }
}
