<?php

namespace App\Synchronizations\Resolvers;

use App\Models\State;
use App\Models\Trace;
use App\Synchronizations\Contracts\ProcessInterface;
use App\Synchronizations\Logic\Processes\{Leaf\BaseProcess as LeafBaseProcess, Metrc\BaseProcess as MetrcBaseProcess};
use Exception;
use Throwable;

class ProcessResolver
{
    /**
     * @param Trace $trace
     * @param array $responseData
     * @return ProcessInterface
     *
     * @throws Throwable
     */
    public static function resolve(Trace $trace, array $responseData): ProcessInterface
    {
        $license = $trace->license;
        $regulator = throw_unless(
            $license->state->regulator,
            new Exception("Regulator of state [{$license->state_code}] not configured")
        );

        $resourceClass = $trace->resource_type;
        if (!empty($logic = $license->getLogic()) && isset($logic[$resourceClass])) {
            /** @var ProcessInterface $logicResourceClass */
            $logicResourceClass = $logic[$resourceClass];
            $obj = new $logicResourceClass($trace, $responseData);

            if (!method_exists($obj, 'Init')) {
                return $obj;
            }

            $obj->Init();

            return $obj;
        }

        switch ($regulator) {
            case State::REGULATOR_LEAF:
                return new LeafBaseProcess($trace, $responseData);
            case State::REGULATOR_METRC:
                return new MetrcBaseProcess($trace, $responseData);
        }

        throw new Exception('Class not found');
    }
}
