<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Sentry\\Laravel\\' => array($vendorDir . '/sentry/sentry-laravel/src'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'Mpociot' => array($vendorDir . '/mpociot/reflection-docblock/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
);
