<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Services\SubroomService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class RoomSubroomsController
 * @package App\Http\Controllers
 */
class RoomSubroomsController extends BaseController
{
    /**
     * @var SubroomService
     */
    private SubroomService $subroomService;

    /**
     * @var Request
     */
    private Request $request;

    public function __construct(SubroomService $subroomService, Request $request)
    {
        $this->subroomService = $subroomService;
        $this->request = $request;
    }

    /**
     * @param
     *
     * @param $id
     * @return JsonResponse
     *
     * @throws GeneralException
     * @throws ValidationException
     */
    public function store($id)
    {
        $rules = [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                "unique:App\Models\Subroom,name,NULL,id,room_id,\"{$id}\""
            ],
            'dimension.width' => ['numeric'],
            'dimension.length' => ['numeric'],
            'number' => ['required', 'integer', 'gte:1'],
            'type' => ['required'],
            'color' => ['required'],
        ];

        return response()->json(
            $this->subroomService->create(array_merge($this->validate($this->request, $rules), ['room_id' => $id])),
            Response::HTTP_CREATED
        );
    }

}
