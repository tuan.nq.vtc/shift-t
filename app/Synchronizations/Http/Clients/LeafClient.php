<?php

namespace App\Synchronizations\Http\Clients;

use App\Models\License;
use App\Synchronizations\Contracts\Http\ResourceInterface;
use Exception;

class LeafClient extends BaseClient
{
    public const MAX_NUMBER_OF_PAGE = 5000;

    public function __construct(License $license)
    {
        parent::__construct($license);

        $apiConfig = throw_unless($license->api_configuration, new Exception('API not configured'));
        $apiKey = throw_unless($apiConfig['api_key'] ?? null, new Exception('Leaf API key not configured'));
        $code = throw_unless($license->code, new Exception('Leaf license code not configured'));

        $this->request->withHeaders(
            [
                'x-mjf-key' => $apiKey,
                'x-mjf-mme-code' => $code,
            ]
        );
    }
}
