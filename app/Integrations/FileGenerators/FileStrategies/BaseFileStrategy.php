<?php

namespace App\Integrations\FileGenerators\FileStrategies;

use App\Integrations\FileGenerators\Contracts\FileStrategyInterface;
use App\Models\License;

abstract class BaseFileStrategy implements FileStrategyInterface
{
    protected License $license;
    protected array $data;

    public function __construct(License $license)
    {
        $this->license = $license;
        $this->data = [];
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function getLicense(): License
    {
        return $this->license;
    }

    public function getDataCount(): int
    {
        return count($this->data);
    }

    abstract public function getContent(): string;

    abstract public function getContentType(): string;

    abstract public function getFilename(): string;
}
