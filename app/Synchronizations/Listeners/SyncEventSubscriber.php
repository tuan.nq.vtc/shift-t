<?php

namespace App\Synchronizations\Listeners;

use App\Synchronizations\Events\SyncPullCompletedEvent;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Illuminate\Events\Dispatcher;
use Ramsey\Uuid\Uuid;
use Throwable;

class SyncEventSubscriber
{
    /**
     * @param SyncPullCompletedEvent $event
     * @throws Throwable
     */
    public function syncPullCompleted(SyncPullCompletedEvent $event)
    {
        $trace = $event->trace;
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'id' => $trace->id,
                    'license_id' => $trace->license->id,
                ],
                $trace->response_data ?? []
            ),
            [
                'action' => 'finish',
            ]
        );
        dispatch(new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [SyncPullCompletedEvent::class],
            'App\Synchronizations\Listeners\SyncEventSubscriber@syncPullCompleted'
        );
    }
}
