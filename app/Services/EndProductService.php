<?php

namespace App\Services;

use App\Http\Parameters\Criteria;
use App\Models\Category;
use App\Models\EndProduct;
use App\Models\Inventory;
use App\Models\Taxonomy;
use App\Services\Traits\EndProductHandler;
use Illuminate\Support\{Arr, Collection};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * @package App\Services
 */
class EndProductService extends BaseService
{
    use EndProductHandler;

    public array $relations = [];

    public function __construct(EndProduct $product)
    {
        parent::__construct($product);
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function getAvailableItems(Criteria $criteria): Collection
    {
        return $this->newQuery()->get();
    }

    /**
     * @param string $id
     * @param array $data
     */
    public function syncProduct(array $data)
    {
        $productQuery = $this->newQuery();
        if (Arr::has($data, 'state_code')) {
            $productQuery->where('state_code', $data['state_code']);
        } else {
            $productQuery->whereNull('state_code');
        }

        $productQuery->updateOrCreate(
            Arr::only($data, ['product_id']),
            Arr::only($data, ['product_id', 'category_id', 'state_code', 'name', 'status'])
        );
    }

    public function list(Criteria $criteria): LengthAwarePaginator
    {
        if (Arr::get($criteria->getFilters(), 'is_map_state', false)) {
            $criteria->addFilter('category_id', Taxonomy::query()
                ->whereNotNull('state_category_id')
                ->where('category_type', Category::class)
                ->where('license_id', $this->licenseId)
                ->get('category_id')->pluck(['category_id'])->toArray());
        }

        return parent::list($criteria);
    }

    public function updateInventoryProductName(string $productId)
    {
        $endProducts = $this->newQuery()->where('product_id', $productId)->get();
        Inventory::query()->with('license')
            ->where('product_id', $productId)
            ->each(function ($inventory) use ($endProducts) {
                $endProduct = $endProducts->where('state_code', $inventory->license->state_code)->first() ?? $endProducts->whereNull('state_code')->first();
                $inventory->update(['product_name' => $endProduct->name]);
            });
    }
}
