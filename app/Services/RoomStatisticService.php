<?php

namespace App\Services;

use App\Models\{Disposal, HarvestGroup, Room, RoomStatistic, RoomType, SeedToSale};
use Carbon\Carbon;

class RoomStatisticService extends BaseService
{
    /**
     * @param Room $room
     * @return RoomStatistic
     */
    public function generateRoomStatistic(Room $room): RoomStatistic
    {
        $stats = [];
        switch ($room->roomType->category) {
            case RoomType::CATEGORY_PLANT:
                $stats = $this->getGrowStatuesStatistic($room);
                break;
            case RoomType::CATEGORY_PROPAGATION:
                $stats = $this->getSourceType($room);
                break;
            case RoomType::CATEGORY_INVENTORY:
                $stats = $this->getInventoryStatistic($room);
                break;
            case RoomType::CATEGORY_DISPOSAL:
                $stats = $this->getDisposalStatistic($room);
                break;
            case RoomType::CATEGORY_HARVEST:
                $stats = $this->getHarvestStatistic($room);
                break;
        }

        return $room->statistics()->updateOrCreate(['room_id' => $room->id], ['stats' => $stats]);
    }

    /**
     * Grow Cycles - total number of grow cycles associated with the room
     * @param Room $room
     * @return array
     */
    private function getGrowStatuesStatistic(Room $room): array
    {
        $room->loadCount(['plants', 'plantStrains', 'growCycles']);
        $growStatuses = [];
        foreach ($room->plants as $plant) {
            if (!isset($growStatuses[$plant->grow_status])) {
                $growStatuses[$plant->grow_status] = [
                    'name' => $plant->grow_status,
                    'total' => 0
                ];
            }
            $growStatuses[$plant->grow_status]['total']++;
        }

        return [
            'grow_statuses' => array_values($growStatuses),
            'plants_count' => $room->plants_count,
            'grow_cycles_count' => $room->grow_cycles_count,
            'strains_count' => $room->plant_strains_count,
        ];
    }

    /**
     * Lots - Show total number of lots in the room
     * Strains - Show number of strains in the room
     * Weight  - Show total weight of all lots in the room
     * @param Room $room
     * @return array
     */
    private function getInventoryStatistic(Room $room): array
    {
        $room->loadCount(['inventoryStrains']);
        $inventoryTypes = [];
        $totalWeight = $lotCount = 0;
        foreach ($room->inventories as $inventory) {
            $totalWeight += $inventory->quantity;
            if ($inventory->is_lotted) {
                $lotCount++;
            }

            $inventoryType = optional($inventory->inventory_type)->name;
            if (!$inventoryType) {
                continue;
            }

            if (!isset($inventoryTypes[$inventoryType])) {
                $inventoryTypes[$inventoryType] = [
                    'name' => $inventoryType,
                    'total' => 0
                ];
            }
            $inventoryTypes[$inventoryType]['total'] += $inventory->quantity;
        }
        return [
            'inventory_types' => $inventoryTypes,
            'total_weight' => $totalWeight,
            'lot_count' => $lotCount,
            'strains_count' => $room->inventoryStrains_count,
        ];
    }

    /**
     * Seeds - total number of seed propagations in the room
     * Clones - total number of clone propagations in the room
     * Tissue - total number of tissue propagations in the room
     * Plants - total number of plant propagations in the room
     * @param Room $room
     * @return int[]
     */
    private function getSourceType(Room $room): array
    {
        $sourceType = [
            SeedToSale::SOURCE_TYPE_CLONE => 0,
            SeedToSale::SOURCE_TYPE_PLANT => 0,
            SeedToSale::SOURCE_TYPE_SEED => 0,
            SeedToSale::SOURCE_TYPE_TISSUE => 0,
        ];
        foreach ($room->propagations as $propagation) {
            if (!in_array($propagation->source_type, SeedToSale::SOURCE_TYPES)) {
                continue;
            }
            $sourceType[$propagation->source_type]++;
        }
        return $sourceType;
    }

    /**
     * Scheduled - Total number of items ready scheduled for destruction in the room
     * Ready - Total number of items ready for destruction in the room
     * Destroyed Recently - Total number of items destroyed in last 30 days in the room
     * @param Room $room
     * @return int[]
     */
    private function getDisposalStatistic(Room $room): array
    {
        $disposalStatistic = [
            'scheduled' => 0,
            'ready' => 0,
            'destroyed' => 0,
        ];
        foreach ($room->disposals as $disposal) {
            if ($disposal->destroyed_at && Carbon::parse($disposal->destroyed_at)->addDays(30)->greaterThanOrEqualTo(
                    Carbon::now()
                )) {
                continue;
            }
            switch ($disposal->status) {
                case Disposal::STATUS_SCHEDULED:
                    $disposalStatistic['scheduled']++;
                    break;
                case Disposal::STATUS_READY:
                    $disposalStatistic['ready']++;
                    break;
                case Disposal::STATUS_DESTROYED:
                    $disposalStatistic['destroyed']++;
                    break;
            }
        }
        return $disposalStatistic;
    }

    /**
     * Dry Harvests- Total Dry harvests not closed which are associated with this room
     * Extraction Harvests - Total extraction harvests not closed which are associated with this room
     * Drying - Total Harvests in Dry Phase which are associated with this room
     * @param Room $room
     * @return array
     */
    private function getHarvestStatistic(Room $room): array
    {
        $harvestStatistic = [
            'dry_harvests' => 0,
            'extraction_harvests' => 0,
            'drying' => 0
        ];

        foreach ($room->harvestGroups as $harvestGroup) {
            if ($harvestGroup->type === HarvestGroup::TYPE_EXTRACTION && $harvestGroup->status !== HarvestGroup::STATUS_CLOSED) {
                $harvestStatistic['extraction_harvests']++;
            }

            if ($harvestGroup->type === HarvestGroup::TYPE_DRY && $harvestGroup->status !== HarvestGroup::STATUS_CLOSED) {
                $harvestStatistic['dry_harvests']++;
            }

            if ($harvestGroup->type === HarvestGroup::TYPE_DRY && $harvestGroup->status === HarvestGroup::STATUS_WET_CONFIRMED) {
                $harvestStatistic['drying']++;
            }
        }

        return $harvestStatistic;
    }
}
