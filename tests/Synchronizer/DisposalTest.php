<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{Disposal, License, Plant, Room, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;

/**
 * Class DisposalTest
 * @package Tests\Synchronizer
 */
class DisposalTest extends TestBase
{
    /** @test */
    public function it_can_push_schedule_plants_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $plant = factory(Plant::class)->create(['license_id' => $licenseId]);
        $disposal = factory(Disposal::class)->make(['license_id' => $licenseId]);
        $disposal->wastable()->associate($plant)->save();
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Disposal::SYNC_ACTION_SCHEDULE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $disposal->id,
                'resource_type' => get_classname($disposal),
                'resource_data' => $disposal->attributesToArray(),
                'resource_changes' => $disposal->getChanges(),
            ]
        );

        $disposalCode = "WAG010101.DI" . $this->faker->regexify('[A-Z0-9]{4}');
        Http::fake(
            [
                '/disposals' => Http::response(
                    [
                        [
                            'global_id' => $disposalCode,
                            'external_id' => $disposal->id,
                        ]
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($disposalCode, $result->resource->sync_code);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_sync_destroy_plants_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $plant = factory(Plant::class)->create(['license_id' => $licenseId]);
        $disposal = factory(Disposal::class)->make(
            ['license_id' => $licenseId, 'destroyed_at' => $this->faker->dateTimeThisYear()]
        );
        $disposal->wastable()->associate($plant)->save();
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => Disposal::SYNC_ACTION_DESTROY,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $disposal->id,
                'resource_type' => get_classname($disposal),
                'resource_data' => $disposal->attributesToArray(),
                'resource_changes' => $disposal->getChanges(),
            ]
        );

        Http::fake(
            [
                '/disposals/dispose' => Http::response(
                    [
                        'disposal_at' => $disposal->destroyed_at->format('m/d/Y h:ia'),
                        'external_id' => $disposal->id,
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );
        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Disposal::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ],
            ]
        );
        $data = [];
        $room = factory(Room::class)->create(['license_id' => $license->id,]);
        $totalRecord = rand(5, 10);
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = "WAG010101.DI" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.AR4U;
            $data[] = [
                "qty" => rand(1, 1000),
                "reason" => $this->faker->randomElement(Disposal::REASONS),
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
                "disposal_at" => $this->faker->dateTimeThisYear()->format('m/d/Y G:ia'),
                "hold_starts_at" => $this->faker->dateTimeThisYear()->format('m/d/Y G:ia'),
                "hold_ends_at" => $this->faker->dateTimeThisYear()->format('m/d/Y G:ia'),
                "global_area_id" => $room->sync_code,
                "source" => $this->faker->randomElement(
                    [
                        Disposal::SOURCE_LEAF_DAILY_PLANT_WASTE,
                        Disposal::SOURCE_LEAF_PLANT,
                        Disposal::SOURCE_LEAF_BATCH,
                        Disposal::SOURCE_LEAF_INVENTORY
                    ]
                ),
                "uom" => "gm"
            ];
        }

        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
            'f_date1' => $fromDate->format('m/d/Y'),
            'f_date2' => $toDate->format('m/d/Y'),
        ];
        Http::fake(
            [
                sprintf("/disposals?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response(
                    $responseData,
                    200
                ),
            ]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Disposal::count());
    }
}
