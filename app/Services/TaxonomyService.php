<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Facades\Sales;
use App\Models\{Category, EndProduct, Inventory, SourceCategory, StateCategory, Taxonomy};
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * @package App\Services
 */
class TaxonomyService
{
    protected string $licenseId = '';

    /**
     * @param $licenseId
     *
     * @return $this
     */
    public function setLicenseId($licenseId)
    {
        $this->licenseId = $licenseId;

        return $this;
    }

    /**
     * @param string $type
     * @return Collection
     * @throws Exception
     */
    public function getByType(string $type): Collection
    {
        switch ($type) {
            case StateCategory::TYPE_SOURCE;
                $modelClass = SourceCategory::class;
                break;
            case StateCategory::TYPE_END;
                $modelClass = Category::class;
                break;
            default:
                throw new GeneralException(__('exceptions.taxonomy.invalid_type'));
        }
        return Taxonomy::query()->where(
            [
                'category_type' => $modelClass,
                'license_id' => $this->licenseId
            ]
        )->select('state_category_id', 'category_id')->get();
    }

    /**
     * @param string $type
     * @param string $id
     * @return StateCategory
     * @throws Exception
     */
    public function getByCategory(string $type, string $id)
    {
        switch ($type) {
            case StateCategory::TYPE_SOURCE;
                $modelClass = SourceCategory::class;
                break;
            case StateCategory::TYPE_END;
                $modelClass = Category::class;
                break;
            default:
                throw new GeneralException(__('exceptions.taxonomy.invalid_type'));
        }
        return Taxonomy::query()->where(
            [
                'category_type' => $modelClass,
                'category_id' => $id,
                'license_id' => $this->licenseId,
            ]
        )->firstOrFail()->stateCategory;
    }

    /**
     * @param Collection|SourceCategory[]|Category[] $categories
     * @param StateCategory $stateCategory
     * @return Collection
     * @throws Exception
     */
    public function assignSource(Collection $categories, StateCategory $stateCategory): Collection
    {
        try {
            $taxonomies = collect();
            DB::beginTransaction();
            foreach ($categories as $category) {
                $taxonomy = $category->taxonomy()->updateOrCreate(
                    ['license_id' => $this->licenseId,],
                    ['state_category_id' => $stateCategory->id]
                );

                $taxonomies->push($taxonomy);
            }
            DB::commit();
            return $taxonomies;
        } catch (Throwable $e) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.taxonomy.failed_assign_category'));
        }
    }

    /**
     * @param array $categoryIds
     * @param StateCategory $stateCategory
     * @return Collection
     * @throws Exception
     */
    public function assign(array $categoryIds, StateCategory $stateCategory): Collection
    {
        try {
            $taxonomies = collect();
            DB::beginTransaction();
            $updatedCategoryIds = [];
            foreach ($categoryIds as $categoryId) {
                $taxonomy = Taxonomy::query()->updateOrCreate(
                    [
                        'category_type' => Category::class,
                        'category_id' => $categoryId,
                        'license_id' => $this->licenseId
                    ],
                    ['state_category_id' => $stateCategory->id]
                );

                //Update state category of End products in Lotted inventory when taxonomy changed or created
                if ($taxonomy->wasChanged() || $taxonomy->wasRecentlyCreated) {
                    $updatedCategoryIds[] = $categoryId;
                }
                $taxonomies->push($taxonomy);
            }
            if (!empty($updatedCategoryIds)) {
                $products = Sales::getProducts(['category_ids' => $updatedCategoryIds]);
                if ($products->isNotEmpty()) {
                    $productIds = $products->pluck('product_id');
                    Inventory::query()->where('is_lotted', true)
                        ->where('product_type', EndProduct::class)
                        ->where('license_id', $this->licenseId)
                        ->whereIn('product_id', $productIds)
                        ->each(function (Inventory $inventory) use ($stateCategory) {
                            $inventory->update(['state_category_id' => $stateCategory->id]);
                            $inventory->type()->update(['state_category_id' => $stateCategory->id]);
                        });
                }
            }

            DB::commit();
            return $taxonomies;
        } catch (Throwable $e) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.taxonomy.failed_assign_category'));
        }
    }
}
