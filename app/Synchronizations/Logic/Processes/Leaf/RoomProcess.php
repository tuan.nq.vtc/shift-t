<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\{Room, RoomType, Strain, Trace, TraceableModel};
use App\Services\LicenseService;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Helpers\LeafHelper;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * Class RoomProcess
 */
class RoomProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'm/d/Y G:ia';

    /**
     * @throws Exception
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;
        $reports = $this->importData($payload['data'] ?? []);
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }

        // dispatch result after imported
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));
        $isFinished = $currentPage === $lastPage;
        if (!$isFinished && $currentPage === 1) {
            $children = $this->trace->children;
            $isReassigned = false;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }
            while ($currentPage < $lastPage) {
                $currentPage++;
                $conditions['page'] = $currentPage;
                $nextPageTrace = Trace::create(
                    [
                        'method' => Trace::METHOD_PULL,
                        'license_id' => $license->id,
                        'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                        'resource_type' => get_classname($this->traceableModel),
                        'status' => Trace::STATUS_PENDING,
                        'resource_conditions' => $conditions,
                    ]
                );

                // reassign the children of current trace to next page trace
                $nextPageTrace->children()->attach($children);
                $isReassigned = true;
                dispatch(new SyncData($nextPageTrace));
            }
            if ($isReassigned) {
                $this->trace->children()->detach();
            }
        }

        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $isFinished ? 'finish' : StreamableResourceInterface::ACTION_UPDATE,
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $license = $this->traceableModel->license;
        /**
         * ['sync_code' => 'id']
         */
        $existingRoomArray = Strain::licenseId($license->id)
            ->whereIn('sync_code', Arr::pluck($data, 'global_id'))
            ->pluck('id', 'sync_code')->toArray();
        $defaultRoomTypeId = RoomType::firstOrCreate(
            [
                'name' => 'Unassigned',
                'category' => RoomType::CATEGORY_UNASSIGNED,
                'account_holder_id' => $license->account_holder_id
            ]
        )->id;
        $newData = [];
        foreach ($data as $item) {
            try {
                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingStrainArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }
                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                $recordData = [
                    'id' => $existingRoomArray[$globalId] ?? Uuid::uuid4()->toString(),
                    'license_id' => $license->id,
                    'name' => Arr::get($item, 'name'),
                    'status' => Strain::STATUS_DISABLED,
                    'is_quarantine' => Arr::get($item, 'type') === 'quarantine',
                    'room_type_id' => $defaultRoomTypeId,
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                    'sync_code' => $globalId,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => Carbon::now(),
                ];
                if(!isset($existingRoomArray[$globalId])) {
                    $recordData['internal_id'] = ModelHelper::generateInternalId(
                        Room::class,
                        $license->id
                    );
                }
                $newData[] = $recordData;
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        if (!empty($newData)) {
            Room::insert($newData);
        }
        return ['success' => $success, 'error' => $error];
    }
}
