<?php

namespace App\Jobs\Synchronizations;

use App\Jobs\Job;
use App\Models\{Notification, Trace};
use App\Notification\Events\SyncingCompletedEvent;
use App\Synchronizations\Synchronizer;
use Illuminate\Support\Facades\Event;
use Log;
use Throwable;

class SyncPushData extends Job
{
    const FIRST_TIME_TO_FAILED_JOB = 1;
    public $chained = [];
    public $deleteWhenMissingModels = true;
    public $delay = 5;
    public $maxTries = 5;

    protected Trace $trace;

    public function __construct(Trace $trace)
    {
        $this->queue = 'sync_push_data';
        $this->trace = $trace;
    }

    /**
     * Execute the job.
     *
     * @param Synchronizer $synchronizer
     *
     * @return void
     *
     * @throws Throwable
     */
    public function handle(Synchronizer $synchronizer)
    {
        $synchronizer->push($this->trace);
        $pendingTraces = $this->trace->refresh()->children()->where('status', Trace::STATUS_PENDING)->get();
        foreach ($pendingTraces as $childTrace) {
            // dispatch if there are no pending/processing dependencies
            if ($childTrace->dependencies()->where('status', '!=', Trace::STATUS_COMPLETED)->count() === 0) {
                dispatch(new self($childTrace));
            }
        }
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception)
    {
        $this->trace->refresh();
        $resource = $this->trace->resource;
        if ($this->attempts() === self::FIRST_TIME_TO_FAILED_JOB) {
            Event::dispatch(
                new SyncingCompletedEvent(
                    $resource,
                    $this->trace->action === Trace::ACTION_CREATE ? $resource->creator : $resource->modifier,
                    Notification::EVENT_CREATE_ACTION_SYNC_FAILED
                )
            );
        }
        Log::error(
            "Sync Push {$this->queue} [{$this->trace->resource_id}] failed {$exception->getMessage()} [attempts - {$this->attempts() }]"
        );
    }
}
