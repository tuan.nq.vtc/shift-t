<?php


namespace App\Models;


use App\Events\Message\{MessageCreated, MessageDeleted, MessageUpdated};
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    // action
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    protected $dispatchesEvents = [
        'created' => MessageCreated::class,
        'updated' => MessageUpdated::class,
        'deleted' => MessageDeleted::class,
    ];

    public static function getListAction(): array
    {
        return [
            self::ACTION_CREATE,
            self::ACTION_UPDATE,
            self::ACTION_DELETE,
        ];
    }

}
