<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveAccountHolderIdFromPrintLabelItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('print_label_items', 'account_holder_id')) {
            Schema::table('print_label_items', function (Blueprint $table) {
                $table->dropColumn('account_holder_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('print_label_items', function (Blueprint $table) {
            $table->uuid('account_holder_id')->index()->after('id');
        });
    }
}
