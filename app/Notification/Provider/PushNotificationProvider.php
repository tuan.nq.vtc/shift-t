<?php

namespace App\Notification\Provider;

use App\Notification\Commands\TestSyncFailedCommand;
use App\Notification\Contracts\PusherHandlerInterface;
use App\Notification\Handlers\PusherHandler;
use App\Notification\Listeners\{PushNotificationListener, SyncingCompletedListener};
use Illuminate\Support\{Arr, Facades\Log, ServiceProvider};
use Laravel\Lumen\Application;
use Pusher\Pusher;

class PushNotificationProvider extends ServiceProvider
{
    protected $listen = [
        //
    ];

    protected $subscribe = [
        PushNotificationListener::class,
        SyncingCompletedListener::class,
    ];

    protected $commands = [
        TestSyncFailedCommand::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $platform = config('notification.notification_platform', 'pusher');
        $config = [];
        switch ($platform) {
            case 'pusher' :
            default:
                $config = config('notification.'.$platform);
                if (!$config) {
                    Log::error('Can not configure the pusher');
                    return;
                }
        }
        $this->registerConnection($config);
        $this->registerHandlers();
        $this->registerCommands();
    }

    private function registerConnection($config)
    {
        $this->app->singleton(
            Pusher::class,
            fn(Application $application) => new Pusher(
                Arr::get($config, 'app_key'),
                Arr::get($config, 'app_secret'),
                Arr::get($config, 'app_id'),
                [
                    'cluster' => Arr::get($config, 'cluster'),
                    'useTLS' => true,
                    'debug' => true,
                ]
            )
        );
    }

    private function registerHandlers()
    {
        $this->app->bind(PusherHandlerInterface::class, fn(Application $app) => $app->make(PusherHandler::class));
    }

    private function registerCommands()
    {
        $this->commands($this->commands);
    }

    public function boot()
    {
        $events = $this->app->get('events');
        $this->bootEvents($events);
        $this->bootSubscribers($events);
    }

    private function bootEvents($events)
    {
        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    private function bootSubscribers($events)
    {
        foreach ($this->subscribe as $subscriber) {
            $events->subscribe($subscriber);
        }
    }
}
