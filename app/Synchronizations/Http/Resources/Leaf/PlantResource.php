<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use App\Models\{Batch, Plant, SeedToSale, TraceableModel};
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Throwable;

/**
 * Class Strain
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property Plant|TraceableModel $traceableModel
 */
class PlantResource extends BaseLeafResource
{
    /**
     * Modifiable attribute of plant on LEAF:
     * - external_id
     * - *global_batch_id
     * - *stage
     * - is_mother
     * - plant_harvested_at
     * - plant_harvested_end_at
     */
    protected static string $endpoint = LeafConstant::ENDPOINT_PLANT;

    protected function fetchAll(): self
    {
        $this->requestUrl = self::$endpoint;
        $this->httpMethod = Request::METHOD_GET;

        if (!empty($this->conditions['page'])) {
            $this->requestQuery['page'] = $this->conditions['page'];
        }
        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function create(): self
    {
        /** @var Plant $plant * */
        $plant = $this->traceableModel;

        parent::create()
            ->setBody(
                [
                    'plant' => [
                        [
                            'origin' => $plant->source_type,
                            'global_batch_id' => $this->getVegetationBatch()->sync_code,
                            'external_id' => $plant->id,
                            'stage' => $plant->grow_status,
                            'is_mother' => $plant->is_mother ? '1' : '0',
                            'plant_created_at' => $this->traceableModel->created_at->format('d/m/Y'),
                        ],
                    ],
                ]
            );
        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync plant update without LEAF GLOBAL_ID [sync_code]')
        );

        return parent::update()
            ->setBody(
                [
                    'plant' => [
                        'global_batch_id' => $this->getVegetationBatch()->sync_code,
                        'global_id' => $this->traceableModel->sync_code,
                        'origin' => $this->traceableModel->source_type,
                        'stage' => $this->traceableModel->grow_status,
                        'is_mother' => $this->traceableModel->is_mother ? '1' : '0',
                        'external_id' => $this->traceableModel->id
                    ],
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync plant deletion without LEAF GLOBAL_ID [sync_code]')
        );

        return parent::delete();
    }

    /**
     * @return Batch
     *
     * @throws Throwable
     */
    private function getVegetationBatch(): Batch
    {
        $vegetationBatch = $this->traceableModel->plantGroup->batch;
        throw_if(!$vegetationBatch, new Exception('Plant Group is NOT synced'));
        throw_if(
            !$vegetationBatch->sync_code || !$vegetationBatch->isSyncSucceed(),
            new Exception('Unable to sync plant changes before LEAF vegetation batch [' . $vegetationBatch->id . '] synced')
        );

        return $vegetationBatch;
    }
}
