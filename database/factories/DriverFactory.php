<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\Driver;
use App\Models\License;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Driver::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Driver::class,
                $data['license_id']
            ),
            'name' => $faker->words(2, true),
        ];
    }
);
