<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Synchronizations\Contracts\TraceableModelInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use App\Models\{Propagation, TraceableModel, SeedToSale};
use App\Synchronizations\Logic\Processes\BaseProcess;

/**
 * Class PropagationProcess
 * @package App\Synchronizations\Logic\Resources\Metrc
 *
 * @property Propagation|TraceableModel $traceableModel
 */
class PropagationProcess extends BaseProcess
{
    /**
     * @param $item
     * @throws Exception
     */
    protected function processListItem($item): void
    {
        if (!($globalId = Arr::get($item, 'Id'))) {
            throw new Exception('ID Not Found');
        }
        $this->traceableModel->newInstance()->firstOrCreate(
            ['sync_code' => $globalId, 'license_id' => $this->traceableModel->license->id,],
            [
                'name' => Arr::get($item, 'Name'),
                'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                'synced_at' => Carbon::now(),
                'source_type' => SeedToSale::SOURCE_TYPES[array_rand(SeedToSale::SOURCE_TYPES)],
                'date' => Carbon::now()
            ]
        );
    }
}
