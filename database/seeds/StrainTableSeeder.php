<?php

use App\Models\{License, Strain, User};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

/**
 * Class StrainTableSeeder.
 */
class StrainTableSeeder extends Seeder
{
    use DisableForeignKeys;

    private ?Collection $users = null;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
        $this->enableForeignKeys();
    }

    protected function iterateLicense(License $license): void
    {
        // License name format: "{License Type} License {State Code}"
        for ($idx = 1; $idx <= 20; $idx++) {
            $info = null;
            // With Metrc API, strain need to have labs testing information
            if (in_array($license->state_code, ['CA'])) {
                $indicaPercent = rand(20, 80);
                $sativaPercent = 100 - $indicaPercent;
                $info = [
                    'testing_status' => 'none',
                    'thc_level' => rand(50, 250) / 1000,
                    'cbd_level' => rand(50, 250) / 1000,
                    'indica_percentage' => $indicaPercent,
                    'sativa_percentage' => $sativaPercent,
                ];
            }
            factory(Strain::class)->create(
                [
                    'license_id' => $license->id,
                    'status' => Strain::STATUS_ENABLED,
                    'name' => "Strain {$idx} {$license->state_code}",
                    'info' => $info,
                    'created_by' => $this->getRandomUserId(),
                    'updated_by' => $this->getRandomUserId(),
                ]
            );
        }
    }

    /**
     * @return string|null
     */
    private function getRandomUserId(): ?string
    {
        if (!$this->users) {
            $this->users = User::all();
        }
        if ($this->users->count() === 0) {
            return null;
        }

        return $this->users->random()->id;
    }
}
