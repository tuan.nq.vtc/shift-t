<?php

/** @var Factory $factory */

use App\Models\Batch;
use App\Models\License;
use App\Models\TraceableModel;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(
    Batch::class,
    function (Faker $faker) {
        return [
            "license_id" => function() {
                return factory(License::class)->create()->id;
            },
            'parent_id' => null,
            'sync_code' => 'WAG010101.BA'.$faker->regexify('[A-Z0-9]{2}'), // WAG010101.BAHN
            'name' => 'Batch '.$faker->regexify('[A-Z0-9]{2}'),
        ];
    }
);
