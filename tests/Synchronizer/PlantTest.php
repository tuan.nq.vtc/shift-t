<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch, GrowCycle, License, Plant, PlantGroup, Room, SeedToSale, Strain, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;
use Tests\Concerns\{ActingWithAttachedHeaders, Streamable, Traceable, WithFaker};

/**
 * Class PlantTest
 * @package Tests\Synchronizer
 */
class PlantTest extends TestBase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;
    use Streamable;

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );
        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Plant::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                ],
            ]
        );
        $strain = factory(Strain::class)->create(['license_id' => $license->id,]);
        $room = factory(Room::class)->create(['license_id' => $license->id,]);

        $plantGroup = factory(PlantGroup::class)->create(['license_id' => $license->id]);
        $growCycle = factory(GrowCycle::class)
            ->create(['license_id' => $license->id, 'grow_status' => SeedToSale::GROW_STATUS_VEGETATIVE]);
        $growCycle->plantGroups()->save($plantGroup);
        $batch = factory(Batch::class)->create(['license_id' => $license->id]);
        $batch->source()->associate($plantGroup)->save();
        $data = [];
        $totalRecord = 5;
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = $this->generateGlobalId("WAG010101.PL");
            $data[] = [
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
                "global_batch_id" => $batch->sync_code,
                "global_strain_id" => $strain->sync_code,
                "global_area_id" => $room->sync_code,
                "origin" => $this->faker->randomElement(SeedToSale::SOURCE_TYPES),
                "plant_created_at" => $this->faker->dateTimeThisYear()->format('m/d/Y'),
                "updated_at" => $this->faker->dateTimeBetween($fromDate, $toDate)->format('m/d/Y G:ia'),
                "global_mother_plant_id" => null
            ];
        }
        $responseData = [
            'current_page' => $currentPage,
            'last_page' => rand($currentPage, 100),
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
        ];
        Http::fake(
            [sprintf("/plants?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response($responseData, 200),]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Plant::count());
    }
}
