<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait GrowStatusFilterable
{
    /**
     * @param Builder $query
     * @param $growStatus
     * @return Builder
     */
    public function scopeGrowStatus($query, ...$growStatus)
    {
        return $query->whereIn($this->getTable() . '.grow_status', $growStatus);
    }
}
