<?php

namespace App\Notification\Handlers;

use App\Notification\Contracts\PusherHandlerInterface;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherHandler implements PusherHandlerInterface
{
    protected Pusher $pusher;
    private LoggerInterface $logger;

    public function __construct(Pusher $pusher)
    {
        $this->pusher = $pusher;
        $this->logger = Log::channel('notification');
    }

    /**
     * @param string $channel
     * @param string $event
     * @param string $message
     */
    public function pushNotification(string $channel, string $event, string $message)
    {
        try {
            $response = $this->pusher->trigger($channel, $event, $message);
            $this->logger->info(
                'pushed message',
                ['response' => $response, 'channel' => $channel, 'event' => $event, 'message' => $message]
            );
        } catch (PusherException $ex) {
            $this->logger->error(
                'error while push notification',
                [
                    'channel' => $channel,
                    'event' => $event,
                    'message' => $message,
                    'error_message' => $ex->getMessage()
                ]
            );
        }
    }
}
