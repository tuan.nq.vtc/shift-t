<?php

namespace App\Models;

use App\Models\Traits\Scope\{AccountHolderFilterable, NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RoomType
 * @package App\Models
 */
class RoomType extends BaseModel
{
    use NameFilterable;
    use StatusFilterable;
    use SoftDeletes;
    use AccountHolderFilterable;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const CATEGORY_PLANT = 'plant';
    public const CATEGORY_PROPAGATION = 'propagation';
    public const CATEGORY_HARVEST = 'harvest';
    public const CATEGORY_DISPOSAL = 'disposal';
    public const CATEGORY_INVENTORY = 'inventory';
    public const CATEGORY_UNASSIGNED = 'unassigned';

    protected $fillable = [
        'name',
        'status',
        'category',
        'account_holder_id'
    ];

    public static function getCategories()
    {
        return [
            self::CATEGORY_PLANT,
            self::CATEGORY_PROPAGATION,
            self::CATEGORY_HARVEST,
            self::CATEGORY_DISPOSAL,
            self::CATEGORY_INVENTORY,
        ];
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query->where('name', 'like', '%' . $value . '%');
    }

}
