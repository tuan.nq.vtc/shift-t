<?php

namespace App\Models;

/**
 * Class LabTestAnalysis
 * @package App\Models
 */
class LabTestAnalysis extends BaseModel
{
    protected $table = 'lab_test_analyses';

    protected $fillable = [
        'regulator',
        'name',
        'code',
        'is_required',
    ];

    protected $casts = [
        'is_required' => 'boolean',
    ];
}
