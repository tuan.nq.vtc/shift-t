<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'plant_groups',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('propagation_id')->index()->nullable();
                $table->uuid('room_id')->index()->nullable();
                $table->uuid('strain_id')->index()->nullable();
                $table->string('name')->index()->nullable();
                $table->unsignedTinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('propagation_id')->references('id')->on('propagations')->nullOnDelete();
                $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant_groups');
    }
}
