<?php

namespace App\Synchronizations\Helpers;

use App\Models\SeedToSale;
use Carbon\Carbon;
use Exception;

class LeafHelper
{
    public const UOM_GRAMS = 'gm';
    public const UOM_EACH = 'ea';
    /**
     * @param string $uom
     * @param bool $reverse
     * @return string
     * @throws Exception
     */
    public static function mapToLeafUom(string $uom, bool $reverse = false): string
    {
        $mapping = [
            SeedToSale::UOM_EACH => self::UOM_EACH,
            SeedToSale::UOM_GRAMS => self::UOM_GRAMS,
        ];
        if ($reverse) {
            $mapping = array_flip($mapping);
        }

        if (!isset($mapping[$uom])) {
            throw new Exception('There is no UOM to be mapped');
        }

        return $mapping[$uom];
    }

    /**
     * @param string $format
     * @param string|null $dateTime
     * @return Carbon|null
     */
    public static function getDateTimeObject(string $format, ?string $dateTime): ?Carbon
    {
        if (!$dateTime || $dateTime === '0000-00-00 00:00:00') {
            return null;
        }

        return Carbon::createFromFormat($format, $dateTime);
    }
    /**
     * @param string $format
     * @param string|null $date
     * @return Carbon|null
     */
    public static function getDateObject(string $format, ?string $date): ?Carbon
    {
        if (!$date || $date === '0000-00-00') {
            return null;
        }

        return Carbon::createFromFormat($format, $date);
    }
}
