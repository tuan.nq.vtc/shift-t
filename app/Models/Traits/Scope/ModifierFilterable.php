<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait ModifierFilterable
{
    /**
     * @param Builder $query
     * @param ...$value
     * @return Builder
     */
    public function scopeUpdatedBy(Builder $query, ...$value): Builder
    {
        return $query->whereIn('updated_by', $value);
    }

    public function scopeOrderByUpdatedBy(Builder $query, $direction): Builder
    {
        $userStable = $this->modifier()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($userStable, $this->getTable().'.updated_by', '=', $userStable.'.id')
            ->orderBy($userStable.'.first_name', $direction);
    }
}
