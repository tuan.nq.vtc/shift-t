<?php
namespace Database\Seeders\DataCorrection;

use App\Models\AdditiveInventory;
use App\Models\AdditiveLog;
use App\Models\AdditiveOperation;
use App\Models\License;
use App\Models\Propagation;
use App\Models\Supplier;
use Illuminate\Database\Seeder;

class UpdateAccountHolderIdForSuppliers extends Seeder
{
    private $tmpTable;

    protected function setNewSupplierId($object) 
    {
        $license = License::findOrFail($object->license_id);
        $newSupplierId = isset($this->tmpTable[$license->account_holder_id.'|'.$object->supplier_id]) ? $this->tmpTable[$license->account_holder_id.'|'.$object->supplier_id] : null;

        if(is_null($newSupplierId)) {
            $oldSupplier = Supplier::findOrFail($object->supplier_id);
            
            $newSupplier = $oldSupplier->replicate();
            
            $oldSupplier->name = $oldSupplier->name.' OLD';
            $oldSupplier->save();

            $newSupplier->account_holder_id = $license->account_holder_id;
            $newSupplier->save();
            $this->tmpTable[$license->account_holder_id.'|'.$object->supplier_id] = $newSupplierId = $newSupplier->id;
        }

        $object->update(['supplier_id' => $newSupplierId]);
    }
    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->tmpTable = [];

        $setNewSupplierId = fn($object) => call_user_func(
            [$this, 'setNewSupplierId'],
            $object
        ); 
        
        AdditiveInventory::whereHas('supplier', function($query)
        {
            $query->accountHolderId('');
        
        })->each($setNewSupplierId);

        AdditiveLog::whereHas('supplier', function($query)
        {
            $query->accountHolderId('');
        
        })->each($setNewSupplierId);
        
        AdditiveOperation::whereHas('supplier', function($query)
        {
            $query->accountHolderId('');
        
        })->each($setNewSupplierId);

        Supplier::accountHolderId('')->forceDelete();
    }
}
