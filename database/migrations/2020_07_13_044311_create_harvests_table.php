<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarvestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'harvests',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('harvest_group_id')->nullable()->index();
                $table->uuid('room_id')->nullable()->index();
                $table->uuid('strain_id')->index();

                // wet
                $table->double('flower_wet_weight', 10, 2)->default(0);
                $table->double('material_wet_weight', 10, 2)->default(0);
                $table->double('waste_wet_weight', 10, 2)->default(0);

                // dry
                $table->double('flower_dry_weight', 10, 2)->default(0);
                $table->double('material_dry_weight', 10, 2)->default(0);
                $table->double('waste_dry_weight', 10, 2)->default(0);

                $table->uuid('flower_room_id')->nullable();
                $table->uuid('material_room_id')->nullable();
                $table->uuid('waste_room_id')->nullable();

                $table->uuid('flower_inventory_type_id')->nullable();
                $table->uuid('material_inventory_type_id')->nullable();

                $table->date('start_at');
                $table->date('finished_at')->nullable();

                $table->unsignedTinyInteger('sync_status')->default(0);
                $table->timestamp('synced_at')->nullable();

                $table->timestamps();
                $table->softDeletes();

                // Foreign keys
                $table->foreign('strain_id')->references('id')->on('strains');
                $table->foreign('harvest_group_id')->references('id')->on('harvest_groups')->nullOnDelete();
                $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvests');
    }
}

