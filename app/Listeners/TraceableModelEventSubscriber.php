<?php

namespace App\Listeners;

use App\Events\{Disposal\DisposalDestroyed,
    Disposal\DisposalScheduled,
    Harvest\HarvestFinalized,
    Harvest\WetWeightConfirmed,
    Inventory\InventoryLotIsSplit,
    Manifest\ManifestCreated,
    PlantGroup\PlantGroupCreated,
    PlantGroup\PlantGroupToRoomMoved,
    Propagation\PropagationToVegetationMoved,
    TraceableModel\TraceableModelCreated,
    TraceableModel\TraceableModelDeleted,
    TraceableModel\TraceableModelUpdated
};
use App\Events\GrowCycle\GrowCycleGrowStatusUpdated;
use App\Models\{Disposal,
    Harvest,
    HarvestGroup,
    Inventory,
    InventoryType,
    Manifest,
    Notification,
    Plant,
    PlantGroup,
    Room,
    State,
    Trace
};
use App\Notification\Events\SyncingCompletedEvent;
use App\Services\RegulatorMockDataService;
use App\Services\TraceService;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Event;
use Throwable;

class TraceableModelEventSubscriber
{
    /**
     * @param TraceableModelCreated $event
     *
     * @throws Throwable
     */
    public function onCreated(TraceableModelCreated $event)
    {
        // Prevent creating trace record when resource belongs to test license
        if ($event->traceableModel->license->is_test) {
            RegulatorMockDataService::generate(
                $event->traceableModel->fresh(),
                TraceableModelInterface::SYNC_ACTION_CREATE
            );
            Event::dispatch(
                new SyncingCompletedEvent(
                    $event->traceableModel,
                    $event->traceableModel->creator,
                    Notification::EVENT_CREATE_ACTION_SYNC_COMPLETED
                )
            );
            return;
        }

        //Generate sync_code = internal_id (bambooID) for async integration licenses like CCRS
        if ($event->traceableModel->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate(
                $event->traceableModel->fresh(),
                TraceableModelInterface::SYNC_ACTION_CREATE
            );
        }

        app(TraceService::class)->createPush($event->traceableModel, TraceableModelInterface::SYNC_ACTION_CREATE);
    }

    /**
     * @param TraceableModelDeleted $event
     *
     * @throws Throwable
     */
    public function onDeleted(TraceableModelDeleted $event)
    {
        // Prevent creating trace record when resource belongs to test license
        if ($event->traceableModel->license->is_test) {
            return;
        }
        app(TraceService::class)->createPush($event->traceableModel, TraceableModelInterface::SYNC_ACTION_DELETE);
    }

    /**
     * @param TraceableModelUpdated $event
     *
     * @throws Throwable
     */
    public function onUpdated(TraceableModelUpdated $event)
    {
        // Prevent creating trace record when resource belongs to test license
        if ($event->traceableModel->license->is_test) {
            Event::dispatch(
                new SyncingCompletedEvent(
                    $event->traceableModel,
                    $event->traceableModel->modifier,
                    Notification::EVENT_UPDATE_ACTION_SYNC_COMPLETED
                )
            );
            return;
        }

        if (!$event->traceableModel->isTraceUpdatable()) { // skip if the changed field is not traceable
            return;
        }

        //Set sync_status = SYNC_STATUS_PENDING when update for async integration
        if ($event->traceableModel->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate(
                $event->traceableModel->fresh(),
                TraceableModelInterface::SYNC_ACTION_UPDATE
            );
        }
        
        app(TraceService::class)->createPush($event->traceableModel, TraceableModelInterface::SYNC_ACTION_UPDATE);

        if ($event->traceableModel->license->isRegulator(State::REGULATOR_CCRS) 
            && $event->traceableModel instanceof Room 
            && $event->traceableModel->isDirty('is_quarantine')) {
                foreach($event->traceableModel->plants as $plant) {
                    RegulatorMockDataService::generate($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                    app(TraceService::class)->createPush($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                }
        }
    }

    /**
     * @param PropagationToVegetationMoved $event
     * @throws Throwable
     */
    public function propagationToVegetationMoved(PropagationToVegetationMoved $event)
    {
        if ($event->plantGroup->license->isRegulator(State::REGULATOR_LEAF)) {
            if ($event->plantGroup->license->is_test) {
                RegulatorMockDataService::generate($event->plantGroup, PlantGroup::SYNC_ACTION_MOVE_TO_VEGETATION);
                return;
            }
            app(TraceService::class)->createPush($event->plantGroup, PlantGroup::SYNC_ACTION_MOVE_TO_VEGETATION);
            return;
        }

        if ($event->plantGroup->license->isRegulator(State::REGULATOR_CCRS)) {
            foreach ($event->plantGroup->plants as $plant) {
                RegulatorMockDataService::generate($plant, TraceableModelInterface::SYNC_ACTION_CREATE);
                app(TraceService::class)->createPush($plant, TraceableModelInterface::SYNC_ACTION_CREATE);
            }
            return;
        }
    }

    /**
     * @param WetWeightConfirmed $event
     * @throws Throwable
     */
    public function confirmWetWeightHarvest(WetWeightConfirmed $event)
    {
        $harvestGroup = $event->harvestGroup->fresh();
        if ($harvestGroup->license->isRegulator(State::REGULATOR_LEAF)) {
            foreach ($harvestGroup->harvests as $harvest) {
                if ($harvest->license->is_test) {
                    RegulatorMockDataService::generate($harvest, TraceableModelInterface::SYNC_ACTION_CREATE);
                    return;
                }
                app(TraceService::class)->createPush($harvest, TraceableModelInterface::SYNC_ACTION_CREATE);
            }
            return;
        }

        //plants update for CCRS
        if ($harvestGroup->license->isRegulator(State::REGULATOR_CCRS)) {
            foreach ($harvestGroup->harvests as $harvest) {
                foreach($harvest->plants as $plant) {
                    RegulatorMockDataService::generate($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                    app(TraceService::class)->createPush($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                }
            }
        }

        if ($harvestGroup->license->hasAsyncIntegration()) {
            foreach ($harvestGroup->harvests as $harvest) {
                RegulatorMockDataService::generate($harvest, TraceableModelInterface::SYNC_ACTION_CREATE);
                app(TraceService::class)->createPush($harvest, TraceableModelInterface::SYNC_ACTION_CREATE);
            }
            return;
        }
    }

    /**
     * @param HarvestFinalized $event
     */
    public function finalizeHarvest(HarvestFinalized $event): void
    {
        if ($event->harvest->license->is_test) {
            RegulatorMockDataService::generate($event->harvest, Harvest::SYNC_ACTION_FINALIZE);
            return;
        }

        if ($event->harvest->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate($event->harvest, Harvest::SYNC_ACTION_FINALIZE);
        }

        $dependencies = [];
        if ($event->harvest->flowerInventoryType && !$event->harvest->flowerInventoryType->sync_code) {
            $creatingInventoryTypeTrace = Trace::query()->where(
                [
                    'resource_id' => $event->harvest->flowerInventoryType->id,
                    'resource_type' => InventoryType::class,
                    'action' => TraceableModelInterface::SYNC_ACTION_CREATE,
                ]
            )->where('status', '!=', Trace::STATUS_COMPLETED)->first();
            if ($creatingInventoryTypeTrace) {
                $dependencies[] = $creatingInventoryTypeTrace->id;
            }
        }
        if ($event->harvest->materialInventoryType && !$event->harvest->materialInventoryType->sync_code) {
            $creatingInventoryTypeTrace = Trace::query()->where(
                [
                    'resource_id' => $event->harvest->materialInventoryType->id,
                    'resource_type' => InventoryType::class,
                    'action' => TraceableModelInterface::SYNC_ACTION_CREATE,
                ]
            )->where('status', '!=', Trace::STATUS_COMPLETED)->first();
            if ($creatingInventoryTypeTrace) {
                $dependencies[] = $creatingInventoryTypeTrace->id;
            }
        }
        $chainActions = [];
        if ($event->harvest->license->isRegulator(
                State::REGULATOR_LEAF
            ) || $event->harvest->license->hasAsyncIntegration()) {
            if ($event->harvest->group->type === HarvestGroup::TYPE_EXTRACTION) {
                $chainActions[] = Harvest::SYNC_ACTION_CREATE;
            }
            if ($event->harvest->group->status !== HarvestGroup::STATUS_DRY_CONFIRMED) {
                $chainActions[] = Harvest::SYNC_ACTION_CONFIRM_DRY;
            }
        }
        if ($event->harvest->license->isRegulator(State::REGULATOR_METRC)) {
            $chainActions = [Harvest::SYNC_ACTION_CREATE, Harvest::SYNC_ACTION_CREATE_PACKAGE];
        }

        $chainActions[] = Harvest::SYNC_ACTION_FINALIZE;
        app(TraceService::class)->createChainPushActions($event->harvest, $chainActions, $dependencies);

        //plants update for CCRS
        if ($event->harvest->license->isRegulator(State::REGULATOR_CCRS)) {
            foreach($event->harvest->plants as $plant) {
                RegulatorMockDataService::generate($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                app(TraceService::class)->createPush($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
            }
        }
    }

    /**
     * @param InventoryLotIsSplit $event
     * @throws Throwable
     */
    public function splitInventoryLots(InventoryLotIsSplit $event)
    {
        if ($event->sourceInventory->license->hasAsyncIntegration()) {
            foreach ($event->newInventories as $inventory) {
                if ($event->sourceInventory->id !== $inventory->id) {
                    RegulatorMockDataService::generate($inventory, Inventory::SYNC_ACTION_CREATE);
                }
            }
            return;
        }

        if ($event->sourceInventory->license->isRegulator(State::REGULATOR_LEAF)) {
            if ($event->sourceInventory->license->is_test) {
                foreach ($event->newInventories as $inventory) {
                    if ($event->sourceInventory->id !== $inventory->id) {
                        RegulatorMockDataService::generate($inventory, Inventory::SYNC_ACTION_CREATE);
                    }
                }
                return;
            }
            // === Logic ===
            // - Create new inventory type if not exist
            // If inventory type has no sync code
            //      => If is there any inventory type `creating` action is processing.
            //          Yes, use this $traceId as dependency
            //          No, ignore
            // - Then
            // If sourceInventory in newInventories
            //      => update inventory type to source inventory
            //         - If is there any source inventory `update` action is processing.
            //          Yes, use this $traceId as dependency
            //          No, ignore
            //      then split
            // else  => split then update inventory type of new inventories
            $newInventoryLots = [];
            $dependencies = [];
            $newInventoryType = null;
            $shouldUpdateTypeOfNewLot = true;
            foreach ($event->newInventories as $inventory) {
                if ($event->sourceInventory->id === $inventory->id) {
                    $updatingInventoryTrace = app(TraceService::class)->create(
                        $event->sourceInventory,
                        Inventory::SYNC_ACTION_UPDATE
                    );
                    $dependencies[] = $updatingInventoryTrace->id;
                    $shouldUpdateTypeOfNewLot = false;
                    continue;
                }
                if (!$newInventoryType) {
                    $newInventoryType = $inventory->type;
                }

                $newInventoryLots[] = $inventory;
            }

            // check if there is any `Inventory Type` creating sync
            if ($newInventoryType && !$newInventoryType->sync_code) {
                $creatingInventoryTypeTrace = Trace::query()->where(
                    [
                        'resource_id' => $newInventoryType->id,
                        'resource_type' => InventoryType::class,
                        'action' => TraceableModelInterface::SYNC_ACTION_CREATE,
                    ]
                )->where('status', '!=', Trace::STATUS_COMPLETED)->first();
                if ($creatingInventoryTypeTrace) {
                    $dependencies[] = $creatingInventoryTypeTrace->id;
                }
            }
            foreach ($newInventoryLots as $resource) {
                $splitLotTrace = app(TraceService::class)->create(
                    $resource,
                    Inventory::SYNC_ACTION_SPLIT_LOT,
                    $dependencies
                );

                if ($shouldUpdateTypeOfNewLot) {
                    app(TraceService::class)->create($resource, Inventory::SYNC_ACTION_UPDATE, [$splitLotTrace->id]);
                }
            }
        }

        if ($event->sourceInventory->license->isRegulator(State::REGULATOR_METRC)) {
            /*
             * // idea
            $newLotIds = [];
            foreach ($event->newInventories as $inventory) {
                $newLotIds[] = $inventory->id;
            }
            $resourceData = [
                'new_lot_ids' => $newLotIds
            ];
            app(TraceService::class)->createPush(
                $event->sourceInventory,
                Inventory::SYNC_ACTION_SPLIT_LOTS,
                $resourceData
            );*/
        }
    }

    /**
     * @param DisposalScheduled $event
     * @throws Throwable
     */
    public function scheduleDisposal(DisposalScheduled $event)
    {
        if ($event->disposal->license->isRegulator(State::REGULATOR_LEAF)) {
            if ($event->disposal->license->is_test) {
                RegulatorMockDataService::generate($event->disposal, Disposal::SYNC_ACTION_SCHEDULE);
                return;
            }
            app(TraceService::class)->createPush($event->disposal, Disposal::SYNC_ACTION_SCHEDULE);
            return;
        }

        if ($event->disposal->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate($event->disposal, Disposal::SYNC_ACTION_SCHEDULE);
            app(TraceService::class)->createPush($event->disposal, Disposal::SYNC_ACTION_SCHEDULE);
            return;
        }
    }

    /**
     * @param DisposalDestroyed $event
     * @throws Throwable
     */
    public function destroyDisposal(DisposalDestroyed $event)
    {
        if ($event->disposal->license->is_test) {
            RegulatorMockDataService::generate($event->disposal, Disposal::SYNC_ACTION_DESTROY);
            return;
        }

        if ($event->disposal->license->isRegulator(State::REGULATOR_CCRS)) {
            if($event->disposal->wastable instanceof Plant) {
                RegulatorMockDataService::generate($event->disposal->wastable, TraceableModelInterface::SYNC_ACTION_UPDATE);
                app(TraceService::class)->createPush($event->disposal->wastable, Plant::SYNC_ACTION_UPDATE);
            }
        }

        if ($event->disposal->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate($event->disposal, Disposal::SYNC_ACTION_DESTROY);
        }

        app(TraceService::class)->createPush($event->disposal, Disposal::SYNC_ACTION_DESTROY);
    }

    /**
     * @param PlantGroupToRoomMoved $event
     */
    public function moveRoomForPlantGroup(PlantGroupToRoomMoved $event): void
    {
        if ($event->plantGroup->license->is_test) {
            RegulatorMockDataService::generate($event->plantGroup, PlantGroup::SYNC_ACTION_MOVE_TO_ROOM);
            return;
        }

        if ($event->plantGroup->license->isRegulator(State::REGULATOR_CCRS)) {
            foreach ($event->plantGroup->plants as $plant) {
                RegulatorMockDataService::generate($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                app(TraceService::class)->createPush($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
            }
        } else {
            app(TraceService::class)->createPush($event->plantGroup, PlantGroup::SYNC_ACTION_MOVE_TO_ROOM);
        }
    }

    public function updateGrowStatusForGrowCycle(GrowCycleGrowStatusUpdated $event): void 
    {
        if ($event->growCycle->license->is_test) {
            return;
        }

        if ($event->growCycle->license->isRegulator(State::REGULATOR_CCRS)) {
            foreach ($event->growCycle->plants as $plant) {
                if($plant->grow_status != $event->growCycle->grow_status) {
                    RegulatorMockDataService::generate($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);
                    app(TraceService::class)->createPush($plant, TraceableModelInterface::SYNC_ACTION_UPDATE);    
                }
            }
        }        
    }

    /**
     * @param PlantGroupCreated $event
     */
    public function createPlantGroup(PlantGroupCreated $event): void
    {
        if ($event->plantGroup->license->is_test) {
            RegulatorMockDataService::generate($event->plantGroup, PlantGroup::SYNC_ACTION_CREATE);
            return;
        }

        if ($event->plantGroup->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate($event->plantGroup, PlantGroup::SYNC_ACTION_CREATE);
        }
        
        if ($event->plantGroup->license->isRegulator(State::REGULATOR_CCRS)) {
            return;
        }

        app(TraceService::class)->createPush($event->plantGroup, PlantGroup::SYNC_ACTION_CREATE);
    }

    /**
     * @param ManifestCreated $event
     */
    public function createManifest(ManifestCreated $event): void
    {
        if ($event->manifest->license->is_test) {
            RegulatorMockDataService::generate($event->manifest, Manifest::SYNC_ACTION_CREATE);
            return;
        }

        if ($event->manifest->license->hasAsyncIntegration()) {
            RegulatorMockDataService::generate($event->manifest, Manifest::SYNC_ACTION_CREATE);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        // traceable model
        $events->listen(
            TraceableModelCreated::class,
            'App\Listeners\TraceableModelEventSubscriber@onCreated'
        );

        $events->listen(
            TraceableModelDeleted::class,
            'App\Listeners\TraceableModelEventSubscriber@onDeleted'
        );

        $events->listen(
            TraceableModelUpdated::class,
            'App\Listeners\TraceableModelEventSubscriber@onUpdated'
        );

        // propagation
        $events->listen(
            [PropagationToVegetationMoved::class],
            'App\Listeners\TraceableModelEventSubscriber@propagationToVegetationMoved'
        );

        // harvest
        $events->listen(
            [WetWeightConfirmed::class],
            'App\Listeners\TraceableModelEventSubscriber@confirmWetWeightHarvest'
        );
        $events->listen(
            [HarvestFinalized::class],
            'App\Listeners\TraceableModelEventSubscriber@finalizeHarvest'
        );

        // disposal
        $events->listen(
            [DisposalScheduled::class],
            'App\Listeners\TraceableModelEventSubscriber@scheduleDisposal'
        );
        $events->listen(
            [DisposalDestroyed::class],
            'App\Listeners\TraceableModelEventSubscriber@destroyDisposal'
        );

        // plant group
        $events->listen(
            [PlantGroupToRoomMoved::class],
            'App\Listeners\TraceableModelEventSubscriber@moveRoomForPlantGroup'
        );

        $events->listen(
            [PlantGroupCreated::class],
            'App\Listeners\TraceableModelEventSubscriber@createPlantGroup'
        );

        //grow cycle
        $events->listen(
            [GrowCycleGrowStatusUpdated::class],
            'App\Listeners\TraceableModelEventSubscriber@updateGrowStatusForGrowCycle'
        );

        
        // inventory
        $events->listen(
            [InventoryLotIsSplit::class],
            'App\Listeners\TraceableModelEventSubscriber@splitInventoryLots'
        );

        // manifest
        $events->listen(
            [ManifestCreated::class],
            'App\Listeners\TraceableModelEventSubscriber@createManifest'
        );
    }
}
