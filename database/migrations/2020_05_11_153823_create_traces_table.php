<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traces', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->tinyInteger('method')->index();
            $table->string('action', 20)->index();
            $table->nullableUuidMorphs('resource');
            $table->json('resource_data')->nullable();
            $table->json('resource_changes')->nullable();
            $table->json('resource_conditions')->nullable();
            $table->json('response_data')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traces');
    }
}
