<?php

namespace App\Http\Controllers;

use App\Services\DriverService;
use Illuminate\Http\{JsonResponse, Request, Response};

/**
 * Class DriverController
 * @package App\Http\Controllers
 */
class DriverController extends CRUDController
{
    public function __construct(DriverService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:150'],
            'birthday' => ['required', 'date'],
            'hired_at' => ['required', 'date'],
            'license' => ['required', 'string'],
            'expired_at' => ['nullable', 'date'],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'min:3', 'max:150'],
            'birthday' => ['sometimes', 'required', 'date'],
            'hired_at' => ['sometimes', 'required', 'date'],
            'license' => ['sometimes', 'required', 'string'],
            'expired_at' => ['sometimes', 'nullable', 'date'],
            'status' => ['sometimes', 'nullable', 'boolean'],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems());
    }

    /**
     * @return JsonResponse
     *
     * @throws ValidationException|GeneralException
     */
    public function store()
    {
        return response()->json(
            $this->service->create($this->validate($this->request, $this->getCreateRules(), [
                'birthday.date' => __('validation.date', ['attribute' => 'Date of Birth']),
                'hired_at.date' => __('validation.date', ['attribute' => 'Date of Hire']),
            ])),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     *
     * @throws ValidationException
     * @throws ModelNotFoundException
     * @throws GeneralException
     */
    public function update(string $id)
    {
        return response()->json(
            $this->service->update(
                $id,
                $this->validate($this->request, $this->getUpdateRules($id), [
                    'birthday.date' => __('validation.date', ['attribute' => 'Date of Birth']),
                    'hired_at.date' => __('validation.date', ['attribute' => 'Date of Hire']),
                ])
            ),
            Response::HTTP_ACCEPTED
        );
    }
}
