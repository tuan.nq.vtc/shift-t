<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{License, Room, RoomType, TraceableModel, User};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Room::class,
    function (Faker $faker) {
        return [
            "license_id" => fn() => factory(License::class)->create()->id,
            "internal_id" => fn($data) => ModelHelper::generateInternalId(
                Room::class,
                $data['license_id']
            ),
            "name" => $faker->words(2, true),
            "room_type_id" => function () {
                return factory(RoomType::class)->create()->id;
            },
            "status" => rand(0, 1),
            "is_quarantine" => rand(0, 1),
            "dimension" => ["width" => $faker->randomNumber(3), "length" => $faker->randomNumber(3)],
            "rfid_tag" => $faker->randomNumber(8),
            "sync_code" => "WAG010101.AR" . $faker->regexify('[A-Z0-9]{2}'), //'WAG010101.AR64'
            "sync_status" => TraceableModel::SYNC_STATUS_SYNCED,
            "synced_at" => $faker->dateTimeThisYear(),
            "created_by" => function () {
                return factory(User::class)->create()->id;
            },
            "updated_by" => function () {
                return factory(User::class)->create()->id;
            },
        ];
    }
);
