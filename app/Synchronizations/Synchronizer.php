<?php

namespace App\Synchronizations;

use App\Models\Notification;
use App\Models\Trace;
use App\Models\TraceLog;
use App\Notification\Events\SyncingCompletedEvent;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Exceptions\SyncException;
use App\Synchronizations\Resolvers\{ClientResolver, ProcessResolver, ResourceResolver};
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Throwable;

class Synchronizer
{
    private LoggerInterface $logger;

    /**
     * Synchronizer constructor.
     */
    public function __construct()
    {
        $this->logger = Log::channel('sync');
    }

    /**
     * @param Trace $trace
     *
     * @return Trace
     *
     * @throws Throwable
     */
    public function push(Trace $trace): Trace
    {
        if (in_array($trace->status, [Trace::STATUS_PENDING, Trace::STATUS_SEND_FAILED])) {
            $this->doRequest($trace);
        }
        if (in_array($trace->status, [Trace::STATUS_PROCESS_FAILED, Trace::STATUS_SEND_COMPLETED])) {
            $this->processPushResponseData($trace);
        }
        $trace->refresh();
        Event::dispatch(
            new SyncingCompletedEvent(
                $trace->resource,
                $trace->action === Trace::ACTION_CREATE ? $trace->resource->creator : $trace->resource->modifier,
                $this->getNotificationAction($trace)
            )
        );
        return $trace;
    }

    /**
     * @param Trace $trace
     * @return Trace
     * @throws Throwable
     */
    public function pull(Trace $trace)
    {
        $responseData = $this->doRequest($trace);

        try {
            $this->logger->info(
                "Start process pull logic - Trace {$trace->id}",
                [
                    'resource' => $trace->resource,
                    'action' => $trace->action
                ]
            );
            $trace->update(['status' => Trace::STATUS_PROCESS_PROCESSING]);
            $logicProcess = ProcessResolver::resolve($trace, $responseData);
            $logicProcess->execute($trace->action);
            $trace->update(['status' => Trace::STATUS_COMPLETED]);
            $this->logger->info("Process pull logic successfully - Trace {$trace->id}");
            return $trace;
        } catch (Throwable $throwable) {
            $trace->update(['status' => Trace::STATUS_PROCESS_FAILED]);
            $this->logger->info("Process pull logic failed - Trace {$trace->id}");
            $this->logger->error($throwable);
            throw $throwable;
        }
    }

    /**
     * Handle logic after synced
     *
     * @param $trace
     * @return Trace
     *
     * @throws Throwable
     */
    public function processPushResponseData(Trace $trace): Trace
    {
        try {
            $this->logger->info(
                "Start process push request - Trace {$trace->id}",
                [
                    'resource' => $trace->resource,
                    'action' => $trace->action
                ]
            );
            $trace->update(['status' => Trace::STATUS_PROCESS_PROCESSING]);
            $logicProcess = ProcessResolver::resolve($trace, $trace->response_data?? []);
            $logicProcess->execute($trace->action);
            $trace->update(['status' => Trace::STATUS_COMPLETED]);
            $trace->resource->updateSyncStatus(TraceableModelInterface::SYNC_STATUS_SYNCED);
            $this->logger->info("Process push response data successfully - Trace {$trace->id}");
            return $trace;
        } catch (Throwable $throwable) {
            $trace->update(['status' => Trace::STATUS_PROCESS_FAILED]);
            $trace->resource->updateSyncStatus(TraceableModelInterface::SYNC_STATUS_FAILED);
            $this->logger->info("Process push response data failed - Trace {$trace->id}");
            $this->logger->error($throwable);
            throw $throwable;
        }
    }

    /**
     * @param Trace $trace
     * @return string
     */
    private function getNotificationAction(Trace $trace)
    {
        if ($trace->resource->sync_status === TraceableModelInterface::SYNC_STATUS_FAILED) {
            return Notification::EVENT_CREATE_ACTION_SYNC_FAILED;
        }

        return $trace->action === Trace::ACTION_CREATE ? Notification::EVENT_CREATE_ACTION_SYNC_COMPLETED :
            Notification::EVENT_UPDATE_ACTION_SYNC_COMPLETED;
    }


    /**
     * Send request to sync the data
     *
     * @param Trace $trace
     *
     * @return array
     *
     * @throws Exception
     * @throws Throwable
     */
    public function doRequest(Trace $trace): array
    {
        try {
            $httpClient = ClientResolver::resolve($trace->license);
            $httpResource = ResourceResolver::resolve($trace);
        } catch (Exception $e) {
            $trace->update(['status' => Trace::STATUS_SEND_FAILED]);
            throw $e;
        }

        $trace->update(['status' => Trace::STATUS_SEND_PROCESSING]);
        if ($trace->resource) {
            $trace->resource->updateSyncStatus(TraceableModelInterface::SYNC_STATUS_PROCESSING);
        }

        $this->logger->info(
            "Start do request - Trace {$trace->id}",
            [
                'http_method' => $httpResource->getMethod(),
                'request_url' => $httpResource->getUrl(),
                'request_query' => $httpResource->getQuery(),
                'request_body' => $httpResource->getBody(),
            ]
        );
        /** @var TraceLog $traceLog */
        $traceLog = $trace->logs()->create(
            [
                'http_method' => $httpResource->getMethod(),
                'request_url' => $httpResource->getUrl(),
                'request_query' => $httpResource->getQuery(),
                'request_body' => $httpResource->getBody(),
                'sent_at' => Carbon::now(),
            ]
        );

        try {
            $responseData = $httpClient->doRequest($httpResource);
            $trace->status = Trace::STATUS_SEND_COMPLETED;
            $traceLog->status = TraceLog::STATUS_SUCCESS;
            if ($trace->method === Trace::METHOD_PUSH) {
                $trace->response_data = $responseData;
                $traceLog->response_data = $responseData;
            }
            $trace->save();
            $traceLog->save();
            $this->logger->info("Do request successfully - Trace {$trace->id}");

            return $responseData?? [];
        } catch (Throwable $e) {
            $trace->update(['status' => Trace::STATUS_SEND_FAILED]);
            $traceLog->update(['status' => TraceLog::STATUS_FAILED,]);
            if ($trace->resource) {
                $trace->resource->updateSyncStatus(TraceableModelInterface::SYNC_STATUS_FAILED);
            }
            $this->logger->error("Do request failed - Trace {$trace->id}");
            $this->logger->error($e);

            throw SyncException::sendFailed();
        }
    }
}
