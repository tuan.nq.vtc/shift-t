<?php


namespace App\Models;


use App\Models\Traits\Scope\CreatorFilterable;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany, MorphTo};

class Notification extends BaseModel
{
    use CreatorFilterable;

    const ALARM_TYPE_INFO = 1;
    const ALARM_TYPE_ERROR = 2;
    const ALARM_TYPE_WARNING = 3;

    const MAXIMUM_NUMBER_OF_DAY = 3;

    const EVENT_CREATE_ACTION_SYNC_COMPLETED = 'create_sync_completed';
    const EVENT_UPDATE_ACTION_SYNC_COMPLETED = 'update_sync_completed';
    const EVENT_CREATE_ACTION_SYNC_FAILED = 'sync_failed';
    const EVENT_UPDATED = 'updated';
    const EVENT_CREATED = 'created';

    protected $fillable = [
        'title',
        'message',
        'is_success_response',
        'alarm_type',
        'changed_notification_resource_by',
        'report_id',
        'report_type'
    ];

    protected $casts = ['is_success_response' => 'bool'];

    public function userNotifications(): HasMany
    {
        return $this->hasMany(UserNotification::class);
    }

    public function changedNotificationResourceBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'changed_notification_resource_by', 'id');
    }

    public function notifiable(): MorphTo
    {
        return $this->morphTo();
    }
}
