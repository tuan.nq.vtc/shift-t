<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{Batch, License, Propagation, Room, SeedToSale, Strain, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

/**
 * Class PropagationTest
 * @package Tests\Synchronizer
 */
class PropagationTest extends TestBase
{
    /** @test */
    public function it_can_push_create_action_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $propagation = factory(Propagation::class)->create(['license_id' => $licenseId]);
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $propagation->id,
                'resource_type' => get_classname($propagation),
                'resource_data' => $propagation->attributesToArray(),
                'resource_changes' => $propagation->getChanges(),
            ]
        );

        $batchCode = "WAG010101.BA" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.BA7D9O;
        Http::fake(
            [
                '/batches' => Http::response(
                    [
                        [
                            'global_id' => $batchCode,
                            'external_id' => $propagation->id,
                        ]
                    ],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($batchCode, $result->resource->sync_code);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $availableDateRanges = config('licenses.date_ranges');
        $license = factory(License::class)->create(
            [
                'state_code' => 'WA',
                'sync_date_range' => array_rand($availableDateRanges)
            ]
        );

        [$fromDate, $toDate] = $license->getDateRange();
        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PULL,
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Propagation::class,
                'resource_conditions' => [
                    'page' => $currentPage,
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ],
            ]
        );
        $strain = factory(Strain::class)->create(['license_id' => $license->id,]);
        $room = factory(Room::class)->create(['license_id' => $license->id,]);
        $data = [];
        $totalRecord = rand(5, 10);
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = "WAG010101.BA" . $this->faker->regexify('[A-Z0-9]{4}');
            $data[] = [
                "type" => "propagation material",
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
                "global_strain_id" => $strain->sync_code,
                "global_area_id" => $room->sync_code,
                "origin" => $this->faker->randomElement(SeedToSale::SOURCE_TYPES),
                "status" => "open",
                "quantity" => rand(5, 20),
                "batch_created_at" => $this->faker->dateTimeThisYear()->format('Y-m-d H:i:s'),
                "updated_at" => $this->faker->dateTimeBetween($fromDate, $toDate)->format('Y-m-d H:i:s'),
                "global_mother_plant_id" => null
            ];
        }
        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        $query = [
            'page' => $currentPage,
            'f_type' => 'propagation material',
            'f_status' => 'open',
        ];
        Http::fake(
            [sprintf("/batches?%s", http_build_query($query, null, '&', PHP_QUERY_RFC3986)) => Http::response($responseData, 200),]
        );

        $this->expectsJobs(PublishStreamEventJob::class, SyncData::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Batch::count());
        $this->assertEquals($totalRecord, Propagation::count());
    }

    public function it_can_fetch_all_from_metrc()
    {
        $license = factory(License::class)->create(['state_code' => 'CA']);
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $license->id,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Propagation::class,
                'options' => [
                    'sync_date_range' => 'three_months'
                ]
            ]
        );

        $totalRecord = 0;
        $endDate = Carbon::tomorrow();
        $startDate = Carbon::now()->subMonths(3);
        while ($startDate->lessThanOrEqualTo($endDate)) {
            $data = [];
            $totalRecordPerPage = rand(5, 10);
            $totalRecord += $totalRecordPerPage;
            for ($j = 1; $j <= $totalRecordPerPage; $j++) {
                $data[] = ['Id' => $this->faker->randomNumber(), 'Name' => $this->faker->name];
            }
            $queries = [
                'licenseNumber' => $license->code,
                'lastModifiedStart' => $startDate->format('Y-m-d'),
                'lastModifiedEnd' => $startDate->addDay()->format('Y-m-d'),
            ];
            Http::fake(
                [
                    "/plantbatches/v1/active?" . http_build_query($queries) => Http::response(
                        $data,
                        200
                    ),
                ]
            );
        }

        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, $result->response_data['total']);
    }
}
