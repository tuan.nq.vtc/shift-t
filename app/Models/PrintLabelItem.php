<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PrintLabelItem
 * @package App\Models
 *
 * @property int id
 * @property int print_label_id
 * @property int item_id
 * @property array|null $box
 * @property string zIndex
 * @property string font_size
 * @property string barcode_width
 * @property string content
 * @property string content_label
 * @property int show_label
 * @property int auto_fit
 * @property int is_custom
 * @property int status
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class PrintLabelItem extends BaseModel
{
    use SoftDeletes;

    protected $table = 'print_label_items';

    protected $fillable = [
        'account_holder_id',
        'print_label_id',
        'type',
        'mapping_field',
        'box',
        'zIndex',
        'font_size',
        'font_weight',
        'barcode_width',
        'content',
        'label',
        'show_label',
        'auto_fit',
        'is_custom',
        'status',
    ];

    protected $casts = [
        'box' => 'array',
        'show_label' => 'bool',
        'auto_fit' => 'bool',
        'is_custom' => 'bool',
        'status' => 'bool',
    ];

    public function printLabel(): BelongsTo
    {
        return $this->belongsTo(PrintLabel::class);
    }
}
