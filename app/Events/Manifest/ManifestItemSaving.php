<?php

namespace App\Events\Manifest;

use App\Events\Event;
use App\Models\ManifestItem;

class ManifestItemSaving extends Event
{
    /**
     * @var ManifestItem
     */
    public ManifestItem $manifestItem;

    /**
     * @param ManifestItem $manifestItem
     */
    public function __construct(ManifestItem $manifestItem)
    {
        $this->manifestItem = $manifestItem;
    }
}
