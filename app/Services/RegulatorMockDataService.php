<?php

namespace App\Services;

use App\Models\{Batch,
    Disposal,
    Harvest,
    Inventory,
    InventoryType,
    License,
    Manifest,
    Plant,
    PlantGroup,
    Propagation,
    QASample,
    Room,
    State,
    Strain,
    TraceableModel};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class RegulatorMockDataService
{
    /**
     * @param TraceableModelInterface $model
     * @param string $action
     */
    public static function generate(TraceableModelInterface $model, string $action): void
    {
        /** @var License $license */
        $license = $model->license;

        if ($license->isRegulator(State::REGULATOR_LEAF)) {
            self::generateLeaf($model, $license, $action);
        }

        if ($license->hasAsyncIntegration()) {
            self::generateForAsyncIntegration($model, $license, $action);
        }
    }

    private static function generateForAsyncIntegration(
        TraceableModelInterface $model,
        License $license,
        string $action
    ): void {
        if (in_array(
            get_class($model),
            [Strain::class, Room::class, Inventory::class, InventoryType::class, Manifest::class, QASample::class,  Plant::class]
        )) {
            $model->update(
                [
                    'sync_code' => $model->getAttribute('internal_id') ?? '',

                ] +
                ($license->is_test ?
                    [
                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                        'synced_at' => Carbon::now(),
                    ] :
                    [
                        'sync_status' => TraceableModel::SYNC_STATUS_PENDING,
                    ])
            );
            return;
        }

        switch (get_class($model)) {
            case Propagation::class:
                $batch = Batch::create(
                    [
                        'name' => $model->name,
                        'status' => Batch::STATUS_OPEN,
                        'license_id' => $model->license_id,
                    ]
                );
                $batch->update(['sync_code' => $batch->getAttribute('internal_id') ?? '']);

                if ($license->is_test) {
                    $model->update(['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]);
                } else {
                    $model->update(['sync_status' => TraceableModel::SYNC_STATUS_PENDING]);
                }
                $batch->source()->associate($model)->save();

                break;
            case Harvest::class:
                switch ($action) {
                    case TraceableModel::SYNC_ACTION_CREATE:
                        $batch = Batch::create(
                            [
                                'name' => 'Harvest ' . $model->name,
                                'status' => Batch::STATUS_OPEN,
                                'license_id' => $model->license_id,
                            ]
                        );
                        $batch->update(['sync_code' => $batch->getAttribute('internal_id') ?? '']);

                        if ($license->is_test) {
                            $model->update(
                                ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                            );
                        }
                        $batch->source()->associate($model)->save();
                        break;
                    case Harvest::SYNC_ACTION_FINALIZE:
                        foreach ($model->inventories as $inventory) {
                            $inventory->update(
                                [
                                    'sync_code' => $inventory->getAttribute('internal_id') ?? '',

                                ] +
                                ($license->is_test ?
                                    [
                                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                        'synced_at' => Carbon::now(),
                                    ] : [])
                            );
                        }
                        if ($license->is_test) {
                            $model->update(
                                ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                            );
                        }
                        break;
                }

                break;
            case PlantGroup::class:
                switch ($action) {
                    case PlantGroup::SYNC_ACTION_MOVE_TO_VEGETATION:
                        foreach ($model->plants as $plant) {
                            $plant->updateWithoutEvents(
                                [
                                    'sync_code' => $plant->getAttribute('internal_id') ?? '',

                                ] +
                                ($license->is_test ?
                                    [
                                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                        'synced_at' => Carbon::now(),
                                    ] : [])
                            );
                        }
                    case PlantGroup::SYNC_ACTION_CREATE:
                    case PlantGroup::SYNC_ACTION_MOVE_TO_ROOM:
                        $batch = Batch::create(
                            [
                                'name' => $model->name,
                                'status' => Batch::STATUS_OPEN,
                                'license_id' => $model->license_id,
                            ]
                        );
                        $batch->update(['sync_code' => $batch->getAttribute('internal_id') ?? '']);

                        if ($license->is_test) {
                            $model->update(
                                ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                            );
                        }
                        $batch->source()->associate($model)->save();
                        break;
                }
                break;
            case Disposal::class:
                switch ($action) {
                    case Disposal::SYNC_ACTION_SCHEDULE:
                        $model->update(
                            [
                                'sync_code' => $model->getAttribute('internal_id') ?? '',

                            ] +
                            ($license->is_test ?
                                [
                                    'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                    'synced_at' => Carbon::now(),
                                ] : [])
                        );
                        break;
                    default:
                        if ($license->is_test) {
                            $model->update(
                                ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                            );
                        } else {
                            $model->update(
                                ['sync_status' => TraceableModel::SYNC_STATUS_PENDING]
                            );
                        }
                }
                break;
        }
    }

    /**
     * @param TraceableModelInterface $model
     * @param License $license
     * @param string $action
     */
    private static function generateLeaf(TraceableModelInterface $model, License $license, string $action): void
    {
        switch (get_class($model)) {
            case Strain::class:
                $model->update(
                    [
                        'sync_code' => self::generateLeafCode('ST', $license, $model),
                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                        'synced_at' => Carbon::now(),
                    ]
                );
                break;
            case Room::class:
                $model->update(
                    [
                        'sync_code' => self::generateLeafCode('AR', $license, $model),
                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                        'synced_at' => Carbon::now(),
                    ]
                );
                break;
            case Inventory::class:
                $model->update(
                    [
                        'sync_code' => self::generateLeafCode('IN', $license, $model),
                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                        'synced_at' => Carbon::now(),
                    ]
                );
                break;
            case InventoryType::class:
                $model->update(
                    [
                        'sync_code' => self::generateLeafCode('TY', $license, $model),
                        'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                        'synced_at' => Carbon::now(),
                    ]
                );
                break;
            case Propagation::class:
                $batch = Batch::create(
                    [
                        'name' => $model->name,
                        'status' => Batch::STATUS_OPEN,
                        'license_id' => $model->license_id,
                    ]
                );
                $batch->update(['sync_code' => self::generateLeafCode('BA', $license, $batch)]);
                $model->update(['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]);
                $batch->source()->associate($model)->save();

                break;
            case Harvest::class:
                switch ($action) {
                    case TraceableModel::SYNC_ACTION_CREATE:
                        $batch = Batch::create(
                            [
                                'name' => 'Harvest ' . $model->name,
                                'status' => Batch::STATUS_OPEN,
                                'license_id' => $model->license_id,
                            ]
                        );
                        $batch->update(['sync_code' => self::generateLeafCode('BA', $license, $batch)]);
                        $model->update(
                            ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                        );
                        $batch->source()->associate($model)->save();
                        break;
                    case Harvest::SYNC_ACTION_FINALIZE:
                        foreach ($model->inventories as $inventory) {
                            $inventory->update(
                                [
                                    'sync_code' => self::generateLeafCode('IN', $license, $inventory),
                                    'synced_at' => Carbon::now(),
                                    'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                ]
                            );
                        }
                        $model->update(
                            ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                        );
                        break;
                }

                break;
            case PlantGroup::class:
                switch ($action) {
                    case PlantGroup::SYNC_ACTION_MOVE_TO_VEGETATION:
                        foreach ($model->plants as $plant) {
                            $plant->updateWithoutEvents(
                                [
                                    'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                    'sync_code' => self::generateLeafCode('PL', $license, $plant),
                                    'synced_at' => Carbon::now(),
                                ]
                            );
                        }
                    case PlantGroup::SYNC_ACTION_CREATE:
                    case PlantGroup::SYNC_ACTION_MOVE_TO_ROOM:
                        $batch = Batch::create(
                            [
                                'name' => $model->name,
                                'status' => Batch::STATUS_OPEN,
                                'license_id' => $model->license_id,
                            ]
                        );
                        $batch->update(['sync_code' => self::generateLeafCode('BA', $license, $batch)]);
                        $model->update(
                            ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                        );
                        $batch->source()->associate($model)->save();
                        break;
                }
                break;
            case Disposal::class:
                switch ($action) {
                    case Disposal::SYNC_ACTION_SCHEDULE:
                        $model->update(
                            [
                                'sync_code' => self::generateLeafCode('DI', $license, $model),
                                'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                'synced_at' => Carbon::now(),
                            ]
                        );
                        break;
                    default:
                        $model->update(
                            ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'synced_at' => Carbon::now()]
                        );
                }
                break;
            case Manifest::class:
                switch ($action) {
                    case Manifest::SYNC_ACTION_CREATE:
                        $model->update(
                            [
                                'sync_code' => self::generateLeafCode('IT', $license, $model),
                                'sync_status' => TraceableModel::SYNC_STATUS_SYNCED,
                                'synced_at' => Carbon::now(),
                            ]
                        );
                        break;
                }
                break;
        }
    }

    /**
     * @param string $modelCode
     * @param License $license
     * @param Model $model
     * @return string
     */
    private static function generateLeafCode(string $modelCode, License $license, Model $model): string
    {
        return sprintf(
            '%s%s.%s%s',
            $license->state_code,
            $license->code,
            $modelCode,
            self::generateUniqueModelSyncCode($model, 'mixed'),
        );
    }

    /**
     * @param Model $model
     * @param string $type
     * @return string
     */
    private static function generateUniqueModelSyncCode(Model $model, string $type): string
    {
        while (true) {
            $syncCode = generate_sync_code($type);
            if ($model->newQuery()->where('sync_code', $syncCode)->count() === 0) {
                return $syncCode;
            }
        }
    }
}
