# Endpoints


## /




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json

Lumen (7.1.3) (Laravel Components ^7.0)
```

### Request
<small class="badge badge-green">GET</small>
 **`/`**



## strains




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/strains" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 61,
            "license_id": 4,
            "name": "Strain D1 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.242,
                "cbd_level": 0.215,
                "indica_percentage": 31,
                "sativa_percentage": 69
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 70,
            "license_id": 4,
            "name": "Strain D10 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.142,
                "cbd_level": 0.061,
                "indica_percentage": 33,
                "sativa_percentage": 67
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 71,
            "license_id": 4,
            "name": "Strain D11 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.082,
                "cbd_level": 0.104,
                "indica_percentage": 20,
                "sativa_percentage": 80
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 72,
            "license_id": 4,
            "name": "Strain D12 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.121,
                "cbd_level": 0.066,
                "indica_percentage": 31,
                "sativa_percentage": 69
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 73,
            "license_id": 4,
            "name": "Strain D13 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.164,
                "cbd_level": 0.134,
                "indica_percentage": 61,
                "sativa_percentage": 39
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 74,
            "license_id": 4,
            "name": "Strain D14 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.068,
                "cbd_level": 0.228,
                "indica_percentage": 34,
                "sativa_percentage": 66
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 75,
            "license_id": 4,
            "name": "Strain D15 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.096,
                "cbd_level": 0.21,
                "indica_percentage": 40,
                "sativa_percentage": 60
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 76,
            "license_id": 4,
            "name": "Strain D16 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.234,
                "cbd_level": 0.222,
                "indica_percentage": 64,
                "sativa_percentage": 36
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 77,
            "license_id": 4,
            "name": "Strain D17 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.195,
                "cbd_level": 0.057,
                "indica_percentage": 72,
                "sativa_percentage": 28
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 78,
            "license_id": 4,
            "name": "Strain D18 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.193,
                "cbd_level": 0.212,
                "indica_percentage": 74,
                "sativa_percentage": 26
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 79,
            "license_id": 4,
            "name": "Strain D19 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.247,
                "cbd_level": 0.109,
                "indica_percentage": 61,
                "sativa_percentage": 39
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 62,
            "license_id": 4,
            "name": "Strain D2 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.141,
                "cbd_level": 0.111,
                "indica_percentage": 62,
                "sativa_percentage": 38
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 80,
            "license_id": 4,
            "name": "Strain D20 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.142,
                "cbd_level": 0.136,
                "indica_percentage": 69,
                "sativa_percentage": 31
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 63,
            "license_id": 4,
            "name": "Strain D3 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.171,
                "cbd_level": 0.139,
                "indica_percentage": 61,
                "sativa_percentage": 39
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 64,
            "license_id": 4,
            "name": "Strain D4 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.143,
                "cbd_level": 0.147,
                "indica_percentage": 69,
                "sativa_percentage": 31
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 65,
            "license_id": 4,
            "name": "Strain D5 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.247,
                "cbd_level": 0.099,
                "indica_percentage": 61,
                "sativa_percentage": 39
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 66,
            "license_id": 4,
            "name": "Strain D6 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.151,
                "cbd_level": 0.194,
                "indica_percentage": 33,
                "sativa_percentage": 67
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 67,
            "license_id": 4,
            "name": "Strain D7 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.108,
                "cbd_level": 0.185,
                "indica_percentage": 29,
                "sativa_percentage": 71
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 68,
            "license_id": 4,
            "name": "Strain D8 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.103,
                "cbd_level": 0.238,
                "indica_percentage": 55,
                "sativa_percentage": 45
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        },
        {
            "id": 69,
            "license_id": 4,
            "name": "Strain D9 CA",
            "status": true,
            "info": {
                "testing_status": "none",
                "thc_level": 0.16,
                "cbd_level": 0.101,
                "indica_percentage": 62,
                "sativa_percentage": 38
            },
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:35.000000Z",
            "updated_at": "2020-06-16T04:53:35.000000Z",
            "deleted_at": null
        }
    ],
    "first_page_url": "http:\/\/localhost\/strains?=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/strains?=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/strains",
    "per_page": "25",
    "prev_page_url": null,
    "to": 20,
    "total": 20
}
```

### Request
<small class="badge badge-green">GET</small>
 **`strains`**



## strains




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/strains" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-black">POST</small>
 **`strains`**



## strains/{strainId}




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/strains/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Strain A1 WA",
    "status": true,
    "info": null,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:34.000000Z",
    "updated_at": "2020-06-16T04:53:34.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-green">GET</small>
 **`strains/{strainId}`**



## strains/{strainId}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/strains/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`strains/{strainId}`**



## strains/{strainId}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/strains/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`strains/{strainId}`**



## strains/{strainId}/{status}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/strains/1/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Strain A1 WA",
    "status": true,
    "info": null,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:34.000000Z",
    "updated_at": "2020-06-16T04:53:34.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`strains/{strainId}/{status}`**



## strains/{strainId}/{status}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/strains/1/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Strain A1 WA",
    "status": true,
    "info": null,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:34.000000Z",
    "updated_at": "2020-06-16T04:53:34.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`strains/{strainId}/{status}`**



## strains/{strainId}




> Example request:

```bash
curl -X DELETE \
    "http://api-trace.docker.localhost/strains/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```



### Request
<small class="badge badge-red">DELETE</small>
 **`strains/{strainId}`**



## room-types




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/room-types" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "license_id": 1,
            "name": "Producer Propagation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 2,
            "license_id": 1,
            "name": "Producer Cultivation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 3,
            "license_id": 1,
            "name": "Producer Vegetative",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 4,
            "license_id": 1,
            "name": "Producer Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 5,
            "license_id": 1,
            "name": "Producer Waste",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 6,
            "license_id": 2,
            "name": "Producer Propagation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 7,
            "license_id": 2,
            "name": "Producer Cultivation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 8,
            "license_id": 2,
            "name": "Producer Vegetative",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 9,
            "license_id": 2,
            "name": "Producer Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 10,
            "license_id": 2,
            "name": "Producer Waste",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 11,
            "license_id": 3,
            "name": "Producer\/Processor Propagation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 12,
            "license_id": 3,
            "name": "Producer\/Processor Cultivation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 13,
            "license_id": 3,
            "name": "Producer\/Processor Vegetative",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 14,
            "license_id": 3,
            "name": "Producer\/Processor Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 15,
            "license_id": 3,
            "name": "Producer\/Processor Waste",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 16,
            "license_id": 4,
            "name": "Producer\/Processor Propagation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 17,
            "license_id": 4,
            "name": "Producer\/Processor Cultivation",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 18,
            "license_id": 4,
            "name": "Producer\/Processor Vegetative",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 19,
            "license_id": 4,
            "name": "Producer\/Processor Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 20,
            "license_id": 4,
            "name": "Producer\/Processor Waste",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 21,
            "license_id": 5,
            "name": "Processor Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 22,
            "license_id": 5,
            "name": "Processor Waste",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 23,
            "license_id": 6,
            "name": "Processor Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 24,
            "license_id": 6,
            "name": "Processor Waste",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        },
        {
            "id": 25,
            "license_id": 7,
            "name": "Retailer Inventory",
            "status": 1,
            "created_at": "2020-06-16T04:53:33.000000Z",
            "updated_at": "2020-06-16T04:53:33.000000Z",
            "deleted_at": null
        }
    ],
    "first_page_url": "http:\/\/localhost\/room-types?=1",
    "from": 1,
    "last_page": 3,
    "last_page_url": "http:\/\/localhost\/room-types?=3",
    "next_page_url": "http:\/\/localhost\/room-types?=2",
    "path": "http:\/\/localhost\/room-types",
    "per_page": "25",
    "prev_page_url": null,
    "to": 25,
    "total": 56
}
```

### Request
<small class="badge badge-green">GET</small>
 **`room-types`**



## room-types




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/room-types" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-black">POST</small>
 **`room-types`**



## room-types/{roomTypeId}




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/room-types/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Producer Propagation",
    "status": 1,
    "created_at": "2020-06-16T04:53:33.000000Z",
    "updated_at": "2020-06-16T04:53:33.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-green">GET</small>
 **`room-types/{roomTypeId}`**



## room-types/{roomTypeId}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/room-types/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`room-types/{roomTypeId}`**



## room-types/{roomTypeId}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/room-types/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`room-types/{roomTypeId}`**



## room-types/{roomTypeId}/{status}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/room-types/1/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Producer Propagation",
    "status": 1,
    "created_at": "2020-06-16T04:53:33.000000Z",
    "updated_at": "2020-06-16T04:53:33.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`room-types/{roomTypeId}/{status}`**



## room-types/{roomTypeId}/{status}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/room-types/1/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Producer Propagation",
    "status": 1,
    "created_at": "2020-06-16T04:53:33.000000Z",
    "updated_at": "2020-06-16T04:53:33.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`room-types/{roomTypeId}/{status}`**



## room-types/{roomTypeId}




> Example request:

```bash
curl -X DELETE \
    "http://api-trace.docker.localhost/room-types/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```



### Request
<small class="badge badge-red">DELETE</small>
 **`room-types/{roomTypeId}`**



## rooms




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/rooms" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 16,
            "license_id": 4,
            "name": "Producer\/Processor Propagation CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:34.000000Z",
            "updated_at": "2020-06-16T04:53:34.000000Z",
            "deleted_at": null
        },
        {
            "id": 17,
            "license_id": 4,
            "name": "Producer\/Processor Cultivation CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:34.000000Z",
            "updated_at": "2020-06-16T04:53:34.000000Z",
            "deleted_at": null
        },
        {
            "id": 18,
            "license_id": 4,
            "name": "Producer\/Processor Vegetative CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:34.000000Z",
            "updated_at": "2020-06-16T04:53:34.000000Z",
            "deleted_at": null
        },
        {
            "id": 19,
            "license_id": 4,
            "name": "Producer\/Processor Inventory CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:34.000000Z",
            "updated_at": "2020-06-16T04:53:34.000000Z",
            "deleted_at": null
        },
        {
            "id": 20,
            "license_id": 4,
            "name": "Producer\/Processor Waste CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:34.000000Z",
            "updated_at": "2020-06-16T04:53:34.000000Z",
            "deleted_at": null
        },
        {
            "id": 44,
            "license_id": 4,
            "name": "Producer\/Processor Propagation CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:13.000000Z",
            "updated_at": "2020-06-16T04:54:13.000000Z",
            "deleted_at": null
        },
        {
            "id": 45,
            "license_id": 4,
            "name": "Producer\/Processor Cultivation CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:13.000000Z",
            "updated_at": "2020-06-16T04:54:13.000000Z",
            "deleted_at": null
        },
        {
            "id": 46,
            "license_id": 4,
            "name": "Producer\/Processor Vegetative CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:13.000000Z",
            "updated_at": "2020-06-16T04:54:13.000000Z",
            "deleted_at": null
        },
        {
            "id": 47,
            "license_id": 4,
            "name": "Producer\/Processor Inventory CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:13.000000Z",
            "updated_at": "2020-06-16T04:54:13.000000Z",
            "deleted_at": null
        },
        {
            "id": 48,
            "license_id": 4,
            "name": "Producer\/Processor Waste CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:13.000000Z",
            "updated_at": "2020-06-16T04:54:13.000000Z",
            "deleted_at": null
        },
        {
            "id": 72,
            "license_id": 4,
            "name": "Producer\/Processor Propagation CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:14.000000Z",
            "updated_at": "2020-06-16T04:54:14.000000Z",
            "deleted_at": null
        },
        {
            "id": 73,
            "license_id": 4,
            "name": "Producer\/Processor Cultivation CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:14.000000Z",
            "updated_at": "2020-06-16T04:54:14.000000Z",
            "deleted_at": null
        },
        {
            "id": 74,
            "license_id": 4,
            "name": "Producer\/Processor Vegetative CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:14.000000Z",
            "updated_at": "2020-06-16T04:54:14.000000Z",
            "deleted_at": null
        },
        {
            "id": 75,
            "license_id": 4,
            "name": "Producer\/Processor Inventory CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:14.000000Z",
            "updated_at": "2020-06-16T04:54:14.000000Z",
            "deleted_at": null
        },
        {
            "id": 76,
            "license_id": 4,
            "name": "Producer\/Processor Waste CA",
            "status": 1,
            "is_quarantine": 0,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:54:14.000000Z",
            "updated_at": "2020-06-16T04:54:14.000000Z",
            "deleted_at": null
        }
    ],
    "first_page_url": "http:\/\/localhost\/rooms?=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/rooms?=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/rooms",
    "per_page": "25",
    "prev_page_url": null,
    "to": 15,
    "total": 15
}
```

### Request
<small class="badge badge-green">GET</small>
 **`rooms`**



## rooms




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/rooms" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-black">POST</small>
 **`rooms`**



## rooms/{roomId}




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/rooms/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Producer Propagation WA",
    "status": 1,
    "is_quarantine": 0,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:33.000000Z",
    "updated_at": "2020-06-16T04:53:33.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-green">GET</small>
 **`rooms/{roomId}`**



## rooms/{roomId}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/rooms/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`rooms/{roomId}`**



## rooms/{roomId}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/rooms/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "validation.required"
        ],
        "status": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`rooms/{roomId}`**



## rooms/{roomId}/{status}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/rooms/1/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Producer Propagation WA",
    "status": 1,
    "is_quarantine": 0,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:33.000000Z",
    "updated_at": "2020-06-16T04:53:33.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`rooms/{roomId}/{status}`**



## rooms/{roomId}/{status}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/rooms/1/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 1,
    "name": "Producer Propagation WA",
    "status": 1,
    "is_quarantine": 0,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:33.000000Z",
    "updated_at": "2020-06-16T04:53:33.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`rooms/{roomId}/{status}`**



## rooms/{roomId}




> Example request:

```bash
curl -X DELETE \
    "http://api-trace.docker.localhost/rooms/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```



### Request
<small class="badge badge-red">DELETE</small>
 **`rooms/{roomId}`**



## propagations




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/propagations" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 2,
            "license_id": 4,
            "strain_id": 131,
            "mother_id": null,
            "room_id": 7,
            "source_type": 6,
            "manifest_id": null,
            "manifested": 0,
            "quantity": 84,
            "mdk_scheduled": 0,
            "mdk_reason": null,
            "mdk_at": null,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:36.000000Z",
            "updated_at": "2020-06-16T04:53:36.000000Z",
            "deleted_at": null
        },
        {
            "id": 23,
            "license_id": 4,
            "strain_id": 73,
            "mother_id": null,
            "room_id": 7,
            "source_type": 27,
            "manifest_id": null,
            "manifested": 0,
            "quantity": 81,
            "mdk_scheduled": 0,
            "mdk_reason": null,
            "mdk_at": null,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:37.000000Z",
            "updated_at": "2020-06-16T04:53:37.000000Z",
            "deleted_at": null
        },
        {
            "id": 26,
            "license_id": 4,
            "strain_id": 79,
            "mother_id": null,
            "room_id": 2,
            "source_type": 30,
            "manifest_id": null,
            "manifested": 0,
            "quantity": 1,
            "mdk_scheduled": 0,
            "mdk_reason": null,
            "mdk_at": null,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:37.000000Z",
            "updated_at": "2020-06-16T04:53:37.000000Z",
            "deleted_at": null
        },
        {
            "id": 41,
            "license_id": 4,
            "strain_id": 16,
            "mother_id": null,
            "room_id": 20,
            "source_type": 45,
            "manifest_id": null,
            "manifested": 0,
            "quantity": 9,
            "mdk_scheduled": 0,
            "mdk_reason": null,
            "mdk_at": null,
            "sync_code": null,
            "sync_status": 0,
            "synced_at": null,
            "created_at": "2020-06-16T04:53:37.000000Z",
            "updated_at": "2020-06-16T04:53:37.000000Z",
            "deleted_at": null
        }
    ],
    "first_page_url": "http:\/\/localhost\/propagations?=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/propagations?=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/propagations",
    "per_page": "25",
    "prev_page_url": null,
    "to": 4,
    "total": 4
}
```

### Request
<small class="badge badge-green">GET</small>
 **`propagations`**



## propagations




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/propagations" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "strain_id": [
            "validation.required"
        ],
        "source_type": [
            "validation.required"
        ],
        "room_id": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-black">POST</small>
 **`propagations`**



## propagations/{propagationId}




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/propagations/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "id": 1,
    "license_id": 3,
    "strain_id": 153,
    "mother_id": null,
    "room_id": 11,
    "source_type": 5,
    "manifest_id": null,
    "manifested": 0,
    "quantity": 75,
    "mdk_scheduled": 0,
    "mdk_reason": null,
    "mdk_at": null,
    "sync_code": null,
    "sync_status": 0,
    "synced_at": null,
    "created_at": "2020-06-16T04:53:36.000000Z",
    "updated_at": "2020-06-16T04:53:36.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-green">GET</small>
 **`propagations/{propagationId}`**



## propagations/{propagationId}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/propagations/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "strain_id": [
            "validation.required"
        ],
        "source_type": [
            "validation.required"
        ],
        "room_id": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`propagations/{propagationId}`**



## propagations/{propagationId}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/propagations/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "strain_id": [
            "validation.required"
        ],
        "source_type": [
            "validation.required"
        ],
        "room_id": [
            "validation.required"
        ]
    }
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`propagations/{propagationId}`**



## propagations/{propagationId}




> Example request:

```bash
curl -X DELETE \
    "http://api-trace.docker.localhost/propagations/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```



### Request
<small class="badge badge-red">DELETE</small>
 **`propagations/{propagationId}`**



## nutrients




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/nutrients" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [],
    "first_page_url": "http:\/\/localhost\/nutrients?=1",
    "from": null,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/nutrients?=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/nutrients",
    "per_page": "25",
    "prev_page_url": null,
    "to": null,
    "total": 0
}
```

### Request
<small class="badge badge-green">GET</small>
 **`nutrients`**



## nutrients




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/nutrients" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"nutrient_name":"Test 10","default_unit_per_box":15,"default_unit_price":17,"unit_name":"KG","description":"description"}'

```


> Example response (201):

```json
{
    "nutrient_name": "Test 10",
    "default_unit_per_box": 15,
    "default_unit_price": 17,
    "unit_name": "KG",
    "description": "description",
    "license_id": 4,
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "created_at": "2020-06-16T04:54:18.000000Z",
    "id": 2
}
```

### Request
<small class="badge badge-black">POST</small>
 **`nutrients`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>nutrient_name</b></code>&nbsp; <small>string</small>     <br>
    Nutrient Name.

<code><b>default_unit_per_box</b></code>&nbsp; <small>float</small>     <br>
    Default Unit Per Box.

<code><b>default_unit_price</b></code>&nbsp; <small>float</small>     <br>
    Default Unit Price.

<code><b>unit_name</b></code>&nbsp; <small>string</small>     <br>
    Unit Name.

<code><b>description</b></code>&nbsp; <small>string</small>     <br>
    Description.



## nutrients/{nutrientId}




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/nutrients/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "id": 1,
    "license_id": 6,
    "nutrient_name": "Test 01",
    "default_unit_per_box": 15,
    "default_unit_price": 17,
    "unit_name": "KG",
    "description": "Description",
    "total_quantity": 200,
    "status": 1,
    "created_at": "2020-06-16T04:54:00.000000Z",
    "updated_at": "2020-06-16T04:54:06.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-green">GET</small>
 **`nutrients/{nutrientId}`**



## nutrients/{nutrientId}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/nutrients/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"nutrient_name":"Test 10","default_unit_per_box":15,"default_unit_price":17,"unit_name":"KG","description":"description"}'

```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 6,
    "nutrient_name": "Test 10",
    "default_unit_per_box": 15,
    "default_unit_price": 17,
    "unit_name": "KG",
    "description": "description",
    "total_quantity": 200,
    "status": 1,
    "created_at": "2020-06-16T04:54:00.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`nutrients/{nutrientId}`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>nutrient_name</b></code>&nbsp; <small>string</small>     <br>
    Nutrient Name.

<code><b>default_unit_per_box</b></code>&nbsp; <small>float</small>     <br>
    Default Unit Per Box.

<code><b>default_unit_price</b></code>&nbsp; <small>float</small>     <br>
    Default Unit Price.

<code><b>unit_name</b></code>&nbsp; <small>string</small>     <br>
    Unit Name.

<code><b>description</b></code>&nbsp; <small>string</small>     <br>
    Description.



## nutrients/{nutrientId}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/nutrients/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"nutrient_name":"Test 10","default_unit_per_box":15,"default_unit_price":17,"unit_name":"KG","description":"description"}'

```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 6,
    "nutrient_name": "Test 10",
    "default_unit_per_box": 15,
    "default_unit_price": 17,
    "unit_name": "KG",
    "description": "description",
    "total_quantity": 200,
    "status": 1,
    "created_at": "2020-06-16T04:54:00.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`nutrients/{nutrientId}`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>nutrient_name</b></code>&nbsp; <small>string</small>     <br>
    Nutrient Name.

<code><b>default_unit_per_box</b></code>&nbsp; <small>float</small>     <br>
    Default Unit Per Box.

<code><b>default_unit_price</b></code>&nbsp; <small>float</small>     <br>
    Default Unit Price.

<code><b>unit_name</b></code>&nbsp; <small>string</small>     <br>
    Unit Name.

<code><b>description</b></code>&nbsp; <small>string</small>     <br>
    Description.



## nutrients/{nutrientId}




> Example request:

```bash
curl -X DELETE \
    "http://api-trace.docker.localhost/nutrients/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```



### Request
<small class="badge badge-red">DELETE</small>
 **`nutrients/{nutrientId}`**



## nutrients/{nutrientId}/toggleStatus




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/nutrients/1/toggleStatus" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 6,
    "nutrient_name": "Test 01",
    "default_unit_per_box": 15,
    "default_unit_price": 17,
    "unit_name": "KG",
    "description": "Description",
    "total_quantity": 200,
    "status": 0,
    "created_at": "2020-06-16T04:54:00.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "deleted_at": null,
    "nutrient_inventories": [
        {
            "id": 1,
            "nutrient_id": 1,
            "in_operation_info": {
                "supplier": "Supplier1",
                "invoice": "INV0001",
                "no_boxes": 20,
                "units_per_box": 10,
                "total_price": 4000,
                "unit_price": 20
            },
            "out_operation_info": null,
            "current_quantity": 200,
            "operation_at": "2020-06-01T09:43:36.000000Z",
            "created_at": "2020-06-16T04:54:06.000000Z",
            "updated_at": "2020-06-16T04:54:06.000000Z",
            "deleted_at": null
        }
    ],
    "feed_operations": []
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`nutrients/{nutrientId}/toggleStatus`**



## nutrients/{nutrientId}/toggleStatus




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/nutrients/1/toggleStatus" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (202):

```json
{
    "id": 1,
    "license_id": 6,
    "nutrient_name": "Test 01",
    "default_unit_per_box": 15,
    "default_unit_price": 17,
    "unit_name": "KG",
    "description": "Description",
    "total_quantity": 200,
    "status": 0,
    "created_at": "2020-06-16T04:54:00.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "deleted_at": null,
    "nutrient_inventories": [
        {
            "id": 1,
            "nutrient_id": 1,
            "in_operation_info": {
                "supplier": "Supplier1",
                "invoice": "INV0001",
                "no_boxes": 20,
                "units_per_box": 10,
                "total_price": 4000,
                "unit_price": 20
            },
            "out_operation_info": null,
            "current_quantity": 200,
            "operation_at": "2020-06-01T09:43:36.000000Z",
            "created_at": "2020-06-16T04:54:06.000000Z",
            "updated_at": "2020-06-16T04:54:06.000000Z",
            "deleted_at": null
        }
    ],
    "feed_operations": []
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`nutrients/{nutrientId}/toggleStatus`**



## nutrients/{nutrientId}/inventories




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/nutrients/1/inventories" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "nutrient_id": 1,
            "in_operation_info": {
                "supplier": "Supplier1",
                "invoice": "INV0001",
                "no_boxes": 20,
                "units_per_box": 10,
                "total_price": 4000,
                "unit_price": 20
            },
            "out_operation_info": null,
            "current_quantity": 200,
            "operation_at": "2020-06-01T09:43:36.000000Z",
            "created_at": "2020-06-16T04:54:06.000000Z",
            "updated_at": "2020-06-16T04:54:06.000000Z",
            "deleted_at": null
        }
    ],
    "first_page_url": "http:\/\/localhost\/nutrients\/1\/inventories?=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/nutrients\/1\/inventories?=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/nutrients\/1\/inventories",
    "per_page": "25",
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
```

### Request
<small class="badge badge-green">GET</small>
 **`nutrients/{nutrientId}/inventories`**



## nutrients/{nutrientId}/inventories




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/nutrients/1/inventories" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"supplier":"Supplier1","invoice":"INV0005","no_boxes":20,"units_per_box":"10","total_price":4000,"operation_at":"2020-06-01 09:43:36"}'

```


> Example response (201):

```json
{
    "nutrient_id": 1,
    "in_operation_info": {
        "supplier": "Supplier1",
        "invoice": "INV0005",
        "no_boxes": 20,
        "units_per_box": "10",
        "total_price": 4000,
        "unit_price": 20
    },
    "current_quantity": 200,
    "operation_at": "2020-06-01T09:43:36.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "created_at": "2020-06-16T04:54:18.000000Z",
    "id": 2
}
```

### Request
<small class="badge badge-black">POST</small>
 **`nutrients/{nutrientId}/inventories`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>supplier</b></code>&nbsp; <small>string</small>     <br>
    Nutrient Name.

<code><b>invoice</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
    optional Invoice Number.

<code><b>no_boxes</b></code>&nbsp; <small>integer</small>     <br>
    Number of Box.

<code><b>units_per_box</b></code>&nbsp; <small>string</small>     <br>
    Unit Per Box.

<code><b>total_price</b></code>&nbsp; <small>float</small>     <br>
    Total Price.

<code><b>operation_at</b></code>&nbsp; <small>date</small>     <br>
    Operation Date.



## nutrients/{nutrientId}/inventories/{nutrientInventoryId}




> Example request:

```bash
curl -X GET \
    -G "http://api-trace.docker.localhost/nutrients/1/inventories/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```


> Example response (200):

```json
{
    "id": 1,
    "nutrient_id": 1,
    "in_operation_info": {
        "supplier": "Supplier1",
        "invoice": "INV0001",
        "no_boxes": 20,
        "units_per_box": 10,
        "total_price": 4000,
        "unit_price": 20
    },
    "out_operation_info": null,
    "current_quantity": 200,
    "operation_at": "2020-06-01T09:43:36.000000Z",
    "created_at": "2020-06-16T04:54:06.000000Z",
    "updated_at": "2020-06-16T04:54:06.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-green">GET</small>
 **`nutrients/{nutrientId}/inventories/{nutrientInventoryId}`**



## nutrients/{nutrientId}/inventories/{nutrientInventoryId}




> Example request:

```bash
curl -X PUT \
    "http://api-trace.docker.localhost/nutrients/1/inventories/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"supplier":"voluptatem","invoice":"INV0005","no_boxes":20,"units_per_box":"10","total_price":4000,"operation_at":"2020-06-01 09:43:36"}'

```


> Example response (202):

```json
{
    "id": 1,
    "nutrient_id": "1",
    "in_operation_info": {
        "supplier": "voluptatem",
        "invoice": "INV0005",
        "no_boxes": 20,
        "units_per_box": "10",
        "total_price": 4000,
        "unit_price": 20
    },
    "out_operation_info": null,
    "current_quantity": 200,
    "operation_at": "2020-06-01T09:43:36.000000Z",
    "created_at": "2020-06-16T04:54:06.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`nutrients/{nutrientId}/inventories/{nutrientInventoryId}`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>supplier</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
    optional Supplier.

<code><b>invoice</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
    optional Invoice Number.

<code><b>no_boxes</b></code>&nbsp; <small>integer</small>     <br>
    Number of Box.

<code><b>units_per_box</b></code>&nbsp; <small>string</small>     <br>
    Unit Per Box.

<code><b>total_price</b></code>&nbsp; <small>float</small>     <br>
    Total Price.

<code><b>operation_at</b></code>&nbsp; <small>date</small>     <br>
    Operation Date.



## nutrients/{nutrientId}/inventories/{nutrientInventoryId}




> Example request:

```bash
curl -X PATCH \
    "http://api-trace.docker.localhost/nutrients/1/inventories/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"supplier":"nihil","invoice":"INV0005","no_boxes":20,"units_per_box":"10","total_price":4000,"operation_at":"2020-06-01 09:43:36"}'

```


> Example response (202):

```json
{
    "id": 1,
    "nutrient_id": "1",
    "in_operation_info": {
        "supplier": "nihil",
        "invoice": "INV0005",
        "no_boxes": 20,
        "units_per_box": "10",
        "total_price": 4000,
        "unit_price": 20
    },
    "out_operation_info": null,
    "current_quantity": 200,
    "operation_at": "2020-06-01T09:43:36.000000Z",
    "created_at": "2020-06-16T04:54:06.000000Z",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "deleted_at": null
}
```

### Request
<small class="badge badge-purple">PATCH</small>
 **`nutrients/{nutrientId}/inventories/{nutrientInventoryId}`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>supplier</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
    optional Supplier.

<code><b>invoice</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
    optional Invoice Number.

<code><b>no_boxes</b></code>&nbsp; <small>integer</small>     <br>
    Number of Box.

<code><b>units_per_box</b></code>&nbsp; <small>string</small>     <br>
    Unit Per Box.

<code><b>total_price</b></code>&nbsp; <small>float</small>     <br>
    Total Price.

<code><b>operation_at</b></code>&nbsp; <small>date</small>     <br>
    Operation Date.



## nutrients/{nutrientId}/inventories/{nutrientInventoryId}




> Example request:

```bash
curl -X DELETE \
    "http://api-trace.docker.localhost/nutrients/1/inventories/1" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1"
```



### Request
<small class="badge badge-red">DELETE</small>
 **`nutrients/{nutrientId}/inventories/{nutrientInventoryId}`**



## feed_operations




> Example request:

```bash
curl -X POST \
    "http://api-trace.docker.localhost/feed_operations" \
    -H "Content-Type: application/json" \
    -H "X-FORWARD-USER-ID: 1" \
    -d '{"plants":[1],"sources":[{"nutrient_inventory_id":1,"applied_quantity":100,"cost":20}],"nutrient_id":1,"nutrient_quantity":100,"feed_at":"2020-06-02 09:43:36","comment":"Demo Comment"}'

```


> Example response (201):

```json
{
    "nutrient_id": 1,
    "feed_at": "2020-06-02T09:43:36.000000Z",
    "comment": "Demo Comment",
    "license_id": 4,
    "nutrient_name": "Test 01",
    "updated_at": "2020-06-16T04:54:18.000000Z",
    "created_at": "2020-06-16T04:54:18.000000Z",
    "id": 1
}
```

### Request
<small class="badge badge-black">POST</small>
 **`feed_operations`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>plants.*</b></code>&nbsp; <small>integer</small>         <i>optional</i>    <br>
    Array of Plant ID.

<code><b>sources.*.nutrient_inventory_id</b></code>&nbsp; <small>integer</small>         <i>optional</i>    <br>
    Nutrient Inventory Id.

<code><b>sources.*.applied_quantity</b></code>&nbsp; <small>float</small>         <i>optional</i>    <br>
    Applied Quantity.

<code><b>sources.*.cost</b></code>&nbsp; <small>float</small>         <i>optional</i>    <br>
    Cost.

<code><b>nutrient_id</b></code>&nbsp; <small>integer</small>     <br>
    Nutrient ID.

<code><b>nutrient_quantity</b></code>&nbsp; <small>integer</small>     <br>
    Nutrient Quantity.

<code><b>feed_at</b></code>&nbsp; <small>date</small>     <br>
    Operation Date.

<code><b>comment</b></code>&nbsp; <small>string</small>     <br>
    Comment.




