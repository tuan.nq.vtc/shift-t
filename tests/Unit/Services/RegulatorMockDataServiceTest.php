<?php

namespace Tests\Unit\Services;

use App\Models\{License, Strain, TraceableModel};
use App\Services\RegulatorMockDataService;
use Tests\Concerns\{Streamable, Traceable};
use Tests\TestCase;

class RegulatorMockDataServiceTest extends TestCase
{
    use Traceable;
    use Streamable;

    /**
     * @test
     */
    public function it_can_generate_strain_sync_code_leaf()
    {
        $this->withoutTraceableEvents();
        $this->withoutStreamableEvents();
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $strain = factory(Strain::class)->create(
            ['license_id' => $licenseId, 'sync_code' => null, 'sync_status' => 0, 'synced_at' => null]
        );
        RegulatorMockDataService::generate($strain, TraceableModel::SYNC_ACTION_CREATE);

        $strain->fresh();
        $this->assertNotNull($strain->sync_code);
        $this->assertEquals(Strain::SYNC_STATUS_SYNCED, $strain->sync_status);
        $this->assertNotNull($strain->synced_at);
    }
}
