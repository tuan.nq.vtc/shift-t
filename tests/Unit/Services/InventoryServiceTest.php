<?php

namespace Tests\Unit\Services;

use App\Exceptions\GeneralException;
use App\Models\{Harvest, Inventory, InventoryCategory, InventoryType, SourceProduct, StateCategory, Taxonomy};
use App\Services\InventoryService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

class InventoryServiceTest extends TestCase
{
    use Traceable;
    use WithFaker;

    protected static string $modelName = Inventory::class;

    protected function getService(): InventoryService
    {
        return app(InventoryService::class);
    }

    /**
     * @test
     */
    public function it_throws_exception_if_create_lots_with_invalid_number_or_weight()
    {
        $this->withoutTraceableEvents();
        $unlottedInventory = factory(static::$modelName)->create(
            [
                'is_lotted' => false,
            ]
        );
        $mappedProduct = factory(SourceProduct::class)->create(['license_id' => $unlottedInventory->license_id]);

        $this->expectException(GeneralException::class);
        $this->expectExceptionMessage(__('exceptions.inventory.invalid_number_of_lots'));
        // execute
        $this->getService()->createLots(
            $unlottedInventory,
            [
                'number_of_lot' => 0,
                'weight_per_lot' => $this->faker->randomFloat(2, 0, 50),
                'product_id' => $mappedProduct->id,
            ]
        );
    }

    /**
     * @test
     */
    public function it_throws_exception_if_create_lots_with_sum_is_greater_than_quantity()
    {
        $this->withoutTraceableEvents();
        $weightPerLot = $this->faker->randomFloat(2, 0, 50);
        $numberOfLot = rand(1, 10);
        $qty = $weightPerLot * $numberOfLot;
        $unlottedInventory = factory(static::$modelName)->create(
            [
                'is_lotted' => false,
                'qty_on_hand' => $qty,
            ]
        );
        $mappedProduct = factory(SourceProduct::class)->create(['license_id' => $unlottedInventory->license_id]);

        $this->expectException(GeneralException::class);
        $this->expectExceptionMessage(__('exceptions.inventory.quantity_insufficient'));
        // execute
        $this->getService()->createLots(
            $unlottedInventory,
            [
                'number_of_lot' => $numberOfLot + rand(1, 10), // increase the number of lot to make the error
                'weight_per_lot' => $weightPerLot,
                'product_id' => $mappedProduct->id,
            ]
        );
    }

    /**
     * @test case 1: sum of number_of_lot and weight_per_lot is less than inventory quantity
     * Eg:
     * - inventory quantity = 15.6 lbs
     * - weight_per_lot = 5 lbs
     * - number_of_lot = 2
     */
    public function it_can_create_lots_from_unlotted_inventory_case1()
    {
        $this->withoutTraceableEvents();
        $qty = rand(50, 100);
        $expectedWeightPerLot = $this->faker->randomFloat(2, 0, 50);
        $expectedNumberOfLot = floor($qty / $expectedWeightPerLot);
        $unlottedInventory = factory(static::$modelName)->create(
            [
                'is_lotted' => false,
                'qty_on_hand' => $qty,
            ]
        );
        $mappedProduct = factory(SourceProduct::class)->create(['license_id' => $unlottedInventory->license_id]);
        $stateCategory = StateCategory::query()->first();
        Taxonomy::query()->create(
            [
                'license_id' => $unlottedInventory->license_id,
                'state_category_id' => $stateCategory->id,
                'category_type' => 'App\Models\SourceCategory',
                'category_id' => $mappedProduct->source_category_id,
            ]
        );
        // execute
        $inventories = $this->getService()->createLots(
            $unlottedInventory,
            [
                'number_of_lot' => $expectedNumberOfLot,
                'weight_per_lot' => $expectedWeightPerLot,
                'product_id' => $mappedProduct->id,
            ]
        );

        $this->assertCount($expectedNumberOfLot, $inventories);
    }

    /**
     * @test case 2: sum of number_of_lot and weight_per_lot is equal inventory quantity
     * Eg:
     * - inventory quantity = 15 lbs
     * - weight_per_lot = 5 lbs or 15 lbs
     * - number_of_lot = 3 or 1
     */
    public function it_can_create_lots_from_unlotted_inventory_case2()
    {
        $this->withoutTraceableEvents();
        $expectedWeightPerLot = $this->faker->randomFloat(2, 0, 50);
        $expectedNumberOfLot = rand(1, 10);
        $qty = $expectedWeightPerLot * $expectedNumberOfLot;
        $unlottedInventory = factory(static::$modelName)->create(
            [
                'is_lotted' => false,
                'qty_on_hand' => $qty,
            ]
        );
        $mappedProduct = factory(SourceProduct::class)->create(['license_id' => $unlottedInventory->license_id]);
        $stateCategory = StateCategory::query()->first();
        Taxonomy::query()->create(
            [
                'license_id' => $unlottedInventory->license_id,
                'state_category_id' => $stateCategory->id,
                'category_type' => 'App\Models\SourceCategory',
                'category_id' => $mappedProduct->source_category_id,
            ]
        );
        // execute
        $inventories = $this->getService()->createLots(
            $unlottedInventory,
            [
                'number_of_lot' => $expectedNumberOfLot,
                'weight_per_lot' => $expectedWeightPerLot,
                'product_id' => $mappedProduct->id,
            ]
        );

        $this->assertCount($expectedNumberOfLot, $inventories);
    }

    /**
     * @test case 3: sum of number_of_lot and weight_per_lot is greater than inventory quantity in case number_of_lot
     * includes the remaining qty of unlloted inventory.
     * Eg:
     * - inventory quantity = 15.6 lbs
     * - weight_per_lot = 5 lbs
     * - number_of_lot = 4 (3 lots 5 lbs and 1 lot 0.6 lbs)
     */
    public function it_can_create_lots_from_unlotted_inventory_case3()
    {
        $this->withoutTraceableEvents();
        $qty = rand(50, 100);
        $expectedWeightPerLot = $this->faker->randomFloat(2, 0, 50);
        $expectedNumberOfLot = ceil($qty / $expectedWeightPerLot);
        $unlottedInventory = factory(static::$modelName)->create(
            [
                'is_lotted' => false,
                'qty_on_hand' => $qty,
            ]
        );
        $mappedProduct = factory(SourceProduct::class)->create(['license_id' => $unlottedInventory->license_id]);
        $stateCategory = StateCategory::query()->first();
        Taxonomy::query()->create(
            [
                'license_id' => $unlottedInventory->license_id,
                'state_category_id' => $stateCategory->id,
                'category_type' => 'App\Models\SourceCategory',
                'category_id' => $mappedProduct->source_category_id,
            ]
        );
        // execute
        $inventories = $this->getService()->createLots(
            $unlottedInventory,
            [
                'number_of_lot' => $expectedNumberOfLot,
                'weight_per_lot' => $expectedWeightPerLot,
                'product_id' => $mappedProduct->id,
            ]
        );

        $this->assertCount($expectedNumberOfLot, $inventories);
    }
}
