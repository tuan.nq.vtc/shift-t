<?php

namespace Tests\Synchronizer;

use App\Jobs\Synchronizations\SyncData;
use App\Models\{License, Strain, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Http;

/**
 * Class StrainTest
 * @package Tests\Synchronizer
 */
class StrainTest extends TestBase
{
    /** @test */
    public function it_can_push_create_action_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $strain = factory(Strain::class)->create(['license_id' => $licenseId]);
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $strain->id,
                'resource_type' => get_classname($strain),
                'resource_data' => $strain->attributesToArray(),
                'resource_changes' => $strain->getChanges(),
            ]
        );

        $strainCode = "WAG010101.ST" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.ST4U;
        $responseData = [
            [
                "name" => $strain->name,
                "global_id" => $strainCode,
                "external_id" => $strain->id
            ]
        ];
        Http::fake(['/strains' => Http::response($responseData, 200),]);

        $result = app(Synchronizer::class)->push($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($strainCode, $result->resource->sync_code);
    }

    /** @test */
    public function it_can_push_create_action_to_metrc()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $indica = rand(0, 100);
        $strain = factory(Strain::class)->create(
            [
                'license_id' => $licenseId,
                'info' => [
                    'testing_status' => rand(0, 1),
                    'thc' => $this->faker->randomFloat(),
                    'cbd' => $this->faker->randomFloat(),
                    'indica' => $indica,
                    'sativa' => 100 - $indica
                ]
            ]
        );
        $trace = factory(Trace::class)->create(
            [
                'method' => Trace::METHOD_PUSH,
                'license_id' => $licenseId,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $strain->id,
                'resource_type' => get_classname($strain),
                'resource_data' => $strain->attributesToArray(),
                'resource_changes' => $strain->getChanges(),
            ]
        );

        Http::fake(['/strains/v1/create*' => Http::response(null, 200),]);

        $strainId = $this->faker->randomNumber();
        Http::fake(
            [
                '/strains/v1/active*' => Http::response(
                    [['Id' => $strainId, 'Name' => $strain->name]],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($strainId, $result->resource->sync_code);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_push_update_action_to_metrc()
    {
        $this->withoutTraceableEvents();
        $this->withoutStreamableEvents();
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $name = $this->faker->name;
        $syncCode = $this->faker->randomNumber();
        $strain = factory(Strain::class)->create(
            [
                'license_id' => $licenseId,
                'name' => $name,
                'sync_code' => $syncCode,
            ]
        );
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PUSH,
                'action' => TraceableModel::SYNC_ACTION_UPDATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $strain->id,
                'resource_type' => get_classname($strain),
                'resource_data' => $strain->attributesToArray(),
                'resource_changes' => $strain->getChanges(),
            ]
        );
        Http::fake(['/strains/v1/update*' => Http::response(null, 200),]);

        $result = app(Synchronizer::class)->push($trace);

        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;

        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PULL,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Strain::class,
                'resource_conditions' => [
                    'page' => $currentPage
                ],
            ]
        );
        $data = [];
        $totalRecord = rand(5, 10);
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = "WAG010101.ST" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.AR4U;
            $data[] = [
                "name" => $this->faker->name,
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
            ];
        }

        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        Http::fake(["/strains?page={$currentPage}" => Http::response($responseData, 200),]);

        $this->expectsJobs(PublishStreamEventJob::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Strain::count());
    }

    /** @test */
    public function it_can_fetch_all_per_page()
    {
        Bus::fake();
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;

        $currentPage = 1;
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PULL,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Strain::class,
                'resource_conditions' => [
                    'page' => $currentPage
                ],
            ]
        );
        $expectedNumberOfTraces = rand(5, 10);
        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage + $expectedNumberOfTraces,
            'data' => []
        ];
        Http::fake(["/strains?page={$currentPage}" => Http::response($responseData, 200),]);

        $result = app(Synchronizer::class)->pull($trace);
        Bus::assertDispatchedTimes(SyncData::class, $expectedNumberOfTraces);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
    }

    public function it_can_fetch_all_from_metrc()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PULL,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Strain::class,
            ]
        );
        $totalRecord = rand(1, 100);
        $data = [];
        $indicaPercent = rand(0, 100);
        for ($i = 1; $i <= $totalRecord; $i++) {
            $data[] = [
                'Id' => $this->faker->randomNumber(),
                'Name' => $this->faker->name,
                'TestingStatus' => 'None',
                "IndicaPercentage" => $indicaPercent,
                "SativaPercentage" => 100 - $indicaPercent,
                "ThcLevel" => null,
                "CbdLevel" => null,
            ];
        }
        Http::fake(
            [
                '/strains/v1/active*' => Http::response(
                    $data,
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, $result->response_data['total']);
    }
}
