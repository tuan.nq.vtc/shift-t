<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use Illuminate\Support\Carbon;
use App\Models\PlantGroup;
use App\Synchronizations\Contracts\Http\ClientInterface;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Http\Resources\BaseResource;
use App\Synchronizations\Resolvers\ClientResolver;
use App\Synchronizations\Constants\LeafConstant;
use Exception;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class PlantGroupResource
 * @package App\Synchronizations\Http\Resources\Leaf
 * @property PlantGroup $traceableModel
 */
class PlantGroupResource extends BaseResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_BATCH;
    private ClientInterface $client;

    public function __construct(TraceableModelInterface $traceableModel, array $changes = [], array $conditions = [])
    {
        parent::__construct($traceableModel, $changes, $conditions);
        $this->client = (new ClientResolver())->resolve($traceableModel->license);
    }

    protected function fetchAll(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = static::$endpoint;
        if (!empty($this->conditions['page'])) {
            $this->requestQuery['page'] = $this->conditions['page'];
        }
        $this->requestQuery['f_type'] = LeafConstant::BATCH_TYPE_PLANT;
        $this->requestQuery['f_status'] = LeafConstant::BATCH_STATUS_OPEN;
        $errorFormat = 'Unable to fetch all plant groups without %s';
        if (empty($this->conditions['from_date']) || empty($this->conditions['to_date'])
            || !($fromDate = Carbon::createFromDate($this->conditions['from_date']))
            || !($toDate = Carbon::createFromDate($this->conditions['to_date']))) {
            $error = sprintf($errorFormat, 'date range LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error);
            throw new Exception($error);
        }

        // f_planted_at1={mm}%2F{dd}%2F{yyyy}&f_planted_at2={mm}%2F{dd}%2F{yyyy}
        $this->requestQuery['f_planted_at1'] = $fromDate->format('m/d/Y');
        $this->requestQuery['f_planted_at2'] = $toDate->format('m/d/Y');

        return $this;
    }

    protected function moveToVegetation()
    {
        /** @var PlantGroup $plantGroup */
        $plantGroup = $this->traceableModel->loadCount(['plants']);
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = '/move_inventory_to_plants';

        $errorFormat = 'Unable to sync plant group moveToVegetation %s';
        $context = ['plantGroup' => $plantGroup];
        if (!optional($plantGroup->propagation->batch)->sync_code) {
            $error = sprintf($errorFormat, 'without batch LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }

        $this->requestBody = [
            'global_inventory_id' => $this->getInventoryIdByBatchId(
                optional($plantGroup->propagation->batch)->sync_code
            ),
            'qty' => $plantGroup->plants_count,
        ];
    }

    /**
     * @param string $batchId
     * @return string
     * @throws Throwable
     */
    private function getInventoryIdByBatchId(string $batchId): string
    {
        $url = LeafConstant::ENDPOINT_INVENTORY;
        $query = ['f_batch_id' => $batchId];
        $this->logger->info(
            "Start Fetch Inventories For Batch {$batchId}",
            [
                'http_method' => Request::METHOD_GET,
                'request_url' => $url,
                'request_query' => $query,
            ]
        );

        /** @var \Illuminate\Http\Client\Response $response */
        $response = $this->client->getRequest()->get($url, $query);
        try {
            $response->throw();
            $this->logger->info(
                "Fetch Inventories Successfully For Batch {$batchId}",
                [
                    'response_body' => $response->body(),
                ]
            );
            $regulatorInventoryLots = $response->json()?? [];
            throw_if(
                empty($regulatorInventoryLots['total']) || empty($regulatorInventoryLots['data']),
                new Exception('There is no inventory lot fetched from LEAF')
            );
            // Filter the inventory type by the immature plant inventory lot
            $inventory = collect($regulatorInventoryLots['data'])
                ->first(fn($item) => $this->filterInventoryType($item['global_inventory_type_id']));
            if (empty($inventory['global_id'])) {
                throw new Exception('No global id');
            }
            return $inventory['global_id'];
        } catch (Exception $e) {
            $this->logger->info(sprintf("Failed Fetch Inventories For Batch %s", $batchId));
            if (!empty($response->json())) {
                $this->logger->error($e->getMessage(), $response->json());
            }
            throw new Exception(
                sprintf("Failed Fetch Inventories For Batch %s", $batchId)
            );
        }
    }

    /**
     * Available inventory types: immature_plant, waste
     * Filter inventory type immature_plant for propagation batch inventory
     *
     * @param string $inventoryTypeGlobalId
     * @return bool
     *
     * @throws Throwable
     */
    private function filterInventoryType(string $inventoryTypeGlobalId): bool
    {
        $url = LeafConstant::ENDPOINT_INVENTORY_TYPE;
        $query = ['f_global_id' => $inventoryTypeGlobalId];

        $this->logger->info(
            "Start Fetch Inventory Types",
            [
                'http_method' => Request::METHOD_GET,
                'request_url' => $url,
                'request_query' => $query,
            ]
        );
        /** @var \Illuminate\Http\Client\Response $response */
        $response = $this->client->getRequest()->get($url, $query);
        try {
            $response->throw();
            $regulatorInventoryTypes = $response->json()?? [];
            throw_if(
                empty($regulatorInventoryTypes['total']) || empty($regulatorInventoryTypes['data']),
                new Exception('There is no inventory type fetched from LEAF')
            );

            // First record because inventory belong to only one type
            $regulatorInventoryType = $regulatorInventoryTypes['data'][0];
            // Get only type of 'immature_plant' for propagation inventory
            return 'immature_plant' === $regulatorInventoryType['type'];
        } catch (Exception $e) {
            $this->logger->info('Failed To Fetch Inventory Types');
            if (!empty($response->json())) {
                $this->logger->error($e->getMessage(), $response->json());
            }
            throw new Exception('Failed To Fetch Inventory Types');
        }
    }

    /**
     * @return PlantGroupResource
     */
    public function moveToRoom(): self
    {
        return $this->setRequestForPlantGroup(__FUNCTION__);
    }

    public function create(): self
    {
        return $this->setRequestForPlantGroup(__FUNCTION__);
    }

    private function setRequestForPlantGroup($action): PlantGroupResource
    {
        // set value for variables in the move to room method
        $plantGroup = $this->traceableModel;
        $url = static::$endpoint . '/update';
        $data = [
            'origin' => $plantGroup->propagation->source_type,
            'global_area_id' => $plantGroup->room->sync_code,
            'global_strain_id' => $plantGroup->strain->sync_code,
            'num_plants' => $plantGroup->propagation->quantity,
            'type' => LeafConstant::BATCH_TYPE_PLANT,
        ];

        switch ($action) {
            case PlantGroup::SYNC_ACTION_CREATE:
                $array[] = array_merge($data, ['external_id' =>  $plantGroup->id]);
                $data = $array;
                $url = trim($url,'/update');
                break;
            case PlantGroup::SYNC_ACTION_MOVE_TO_ROOM:
                $data['global_id'] =  $plantGroup->batch->sync_code;
                break;
        }
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $url;
        $this->requestBody = ['batch' => $data];
        return $this;
    }
}
