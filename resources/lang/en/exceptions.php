<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */


    'api' => [
        'state_not_configured' => 'API for state :state is not configured.',
    ],
    'batches' => [
        'invalid_fetched_type' => 'Fetched batch has invalid type ":type"'
    ],
    'license' => [
        'invalid' => 'The license is invalid',
    ],
    'portal_service' => [
        'fetch_companies_failed' => 'Can not fetch all companies from Portal service',
        'fetch_company_failed' => 'Can not fetch company from Portal service',
        'fetch_license_types_failed' => 'Can not fetch all license types from Portal service',
        'fetch_licenses_failed' => 'Can not fetch all licenses from Portal service',
        'fetch_license_failed' => 'Can not fetch license from Portal service',
        'fetch_users_failed' => 'Can not fetch all users from Portal service',
        'fetch_user_failed' => 'Can not fetch user from Portal service',
        'fetch_organization_users_failed' => 'Can not fetch all users of Organization from Portal service',
        'fetch_account_holder_users_failed' => 'Can not fetch all users of Account Holder from Portal service',
    ],

    'sales_service' => [
        'fetch_products_failed' => 'Can not fetch products from Sales service',
        'update_product_failed' => 'Can not update product from Sales service',
        'fetch_product_failed' => 'Can not fetch product from Sales service',
        'product_not_available' => 'The product is not available in Sales service.',
        'product_not_exists' => 'The product does not exist in Sales service.',
    ],

    'actions' => [
        'create_failed' => 'Create Failed.',
        'license_id_required' => 'License ID is required',
        'delete_failed' => 'Delete Failed.',
        'update_failed' => 'Update Failed.',
    ],

    'room_types' => [
        'create_failed' => 'There was a problem creating new room  type. Please try again.',
        'delete_failed' => 'There was a problem deleting this room  type. Please try again.',
        'mark_failed' => 'There was a problem updating this room  type. Please try again.',
        'update_failed' => 'There was a problem updating this room  type. Please try again.',
    ],

    'rooms' => [
        'create_failed' => 'There was a problem creating new room. Please try again.',
        'delete_failed_on_storing_plants' => 'This room cannot be deleted until all plants have been moved from this room.',
        'delete_failed_on_storing_inventory_lots' => 'This room cannot be deleted until all inventory lots have been moved from this room.',
        'delete_failed' => 'There was a problem deleting this room. Please try again.',
        'mark_failed' => 'There was a problem updating this room. Please try again.',
        'update_failed' => 'There was a problem updating this room. Please try again.',
        'disposal_room_invalid' => 'Disposal room is invalid.',
    ],

    'plants' => [
        'already_destroyed' => 'Plant(s) have been destroyed',
        'destroy_failed' => 'There was a problem destroying plant(s).',
        'list_for_harvest' => [
            'target_invalid' => 'Cannot get list of plant on this type of object ":target'
        ],
        'make_mother_failed' => 'There was a problem while making plant(s) as mother plant(s).',
        'move_room_failed' => 'There was a problem while moving plant(s) to new room.',
        'update_grow_status_failed' => 'There was a problem while changing the grow status of plant(s).',
        'not_exist' => 'The plant(s) do not exist.',
        'not_synced' => 'The plant is not synced.',
        'is_destroyed' => 'The plant :sync_code is destroyed.',
        'is_harvested' => 'The plant :sync_code is harvested.',
        'update_failed' => 'There was a problem updating this plant. Please try again.',
    ],

    'strains' => [
        'create_failed' => 'There was a problem creating new strain. Please try again.',
        'delete_failed' => 'There was a problem deleting this strain. Please try again.',
        'mark_failed' => 'There was a problem updating this strain. Please try again.',
        'update_failed' => 'There was a problem updating this strain. Please try again.',

    ],

    'nutrients' => [
        'create_failed' => 'There was a problem creating new nutrient. Please try again.',
        'delete_failed' => 'There was a problem deleting this nutrient. Please try again.',
        'mark_failed' => 'There was a problem updating this nutrient. Please try again.',
        'update_failed' => 'There was a problem updating this nutrient. Please try again.',
    ],

    'traces' => [
        'create_failed' => 'There was a problem creating new S2S trace sync. Please try again.',
        'delete_failed' => 'There was a problem deleting this S2S trace sync. Please try again.',
        'update_failed' => 'There was a problem updating this S2S trace sync. Please try again.',
    ],

    'propagation' => [
        'destroy_failed' => 'Destroy propagation(s) failed.',
        'not_found' => 'Propagation not found',
        'quantity_insufficient' => 'Quantity in :name is insufficient.',
        'update_qty_failed' => 'Update Quantity Failed.',
    ],

    'pesticide' => [
        'apply_failed' => 'There was a problem applying pesticide on :target',
        'apply_failed_with_no_plant' => 'There was a problem applying pesticide to :target with no plants.',
        'apply_on_plant_failed' => 'There was a problem applying pesticide on plant',
        'calculate_failed' => 'Calculate the values failed.',
        'create_inventory_failed' => 'There was a problem creating pesticide inventory',
        'invalid_apply_target' => 'Cannot apply pesticide on this target ":target".',
    ],

    'nutrient' => [
        'apply_failed' => 'There was a problem applying nutrient on :target',
        'apply_failed_with_no_plant' => 'There was a problem applying nutrient to :target with no plants.',
        'apply_failed_on_empty_propagation' => 'There was a problem applying nutrient on empty propagation',
        'apply_on_plant_failed' => 'There was a problem applying nutrient on plant',
        'calculate_failed' => 'Calculate the values failed.',
        'create_inventory_failed' => 'There was a problem creating nutrient inventory',
        'invalid_apply_target' => 'Cannot apply nutrient on this target ":target".',
    ],

    'disposal' => [
        'invalid_date' => 'The destruction date is invalid.',
        'invalid_quantity' => 'Invalid disposal quantity.',
        'invalid_room' => 'Invalid disposal room.',
        'invalid_waste_type' => 'The waste type is invalid.',
    ],

    'harvest' => [
        'invalid' => 'The harvest is invalid',
        'create_failed' => 'There was a problem creating new harvest. Please try again.',
        'update_weight' => [
            'already_finish' => 'The harvest has ended.',
            'format' => 'The data has wrong format',
        ],
        'confirm' => [
            'room_invalid' => 'The selected :type room is invalid',
            'harvest_invalid' => 'The harvest is invalid.',
            'weight_invalid' => 'The weight must be great than 0.',
            'final_weight_invalid' => 'The harvest must have one of flower/material weights with a value other than 0.',
            'weight_gt_zero' => 'The :weight_field weight must be great than 0.',
        ],
        'no_harvested_plants' => 'There are no plants to be harvested.',
        'no_deleted_plants' => 'There are no plants to be deleted.',
        'harvest_plants' => [
            'failed_update_plant_grow_status' => 'There was a problem updating grow status. Please try again.',
            'failed_update_grow_cycle_grow_status' => 'There was a problem updating grow status of grow cycles. Please try again.',
        ],
        'failed_update_grow_status' => 'There was a problem updating grow status. Please try again.',
        'action_validation' => 'This action is only allowed with :status status.',
        'wet_weight' => [
            'min_quantity' => 'The weight must be greater than 0.',
            'create_failed' => 'There was a problem creating new wet weight. Please try again.',
            'after_start_date' => 'The harvest date must be after start date.'
        ],
        'dry_weight' => [
            'lt_wet_weight' => 'The dry weight must be less than wet weight.',
        ],
    ],
    'inventory' => [
        'adjust_qty_on_hand_invalid' => 'The new in-stock quantity must be lesser than the current in-stock quantity.',
        'quantity_greater_or_equal' => 'The qty must be greater than or equal :min g.',
        'is_lotted' => 'Inventory is already lotted',
        'invalid_number_of_lots' => 'The total weight of all the lots is larger than the weight of the harvest batch',
        'quantity_insufficient' => 'Quantity is insufficient.',
        'invalid_product' => 'Mapped product is invalid.',
        'invalid_product_sublot' => 'Sublot Mapped product is invalid.',
        'invalid_product_parent' => 'Parent lot Mapped product is invalid.',
        'make_available_failed' => 'There was a problem while making inventory as available inventory.',
        'qa_sample_status_invalid' => 'You cannot :action for sale lots without QA results.',
        'must_lotted' => 'Inventory must be lotted.',
        'hold_failed' => 'Holding the inventory is fail.',
        'sell_status_invalid' => 'The new sell status for inventory is invalid.',
        'move_room_failed' => 'There was a problem while moving inventory to new room.',
        'destroy_failed' => 'There was a problem destroying inventory(es).',
        'invalid_product_type' => 'The product type is invalid.',
        'invalid_room' => 'Invalid inventory room.',
        'must_inventory_room' => 'The room must be an inventory room.',
        'inventory_id_invalid' => 'The Inventory ID is invalid.',
        'invalid_lot_type' => 'The Inventory type is invalid.',
        'sublot' => [
            'qa_status_invalid' => 'The QA status is invalid.',
        ],

    ],
    'qa_samples' => [
        'create_failed' => 'There was a problem creating new qa sample. Please try again.',
        'invalid_quantity' => 'Quantity in :name is insufficient.',
        'invalid_reset' => 'The QA Samples do not have any lab test.',
    ],
    'notification' => [
        'invalid_event' => 'The :model model does not support to push :event event.'
    ],
    'brand' => [
        'destroy_failed' => 'There was a problem destroying brand(s).',
        'not_exist' => 'The brand(s) do not exist.',
    ],
    'product_line' => [
        'destroy_failed' => 'There was a problem destroying product line(s).',
        'not_exist' => 'The product line(s) do not exist.',
    ],
    'additive_log' => [
        'target_invalid' => 'Cannot get logs of additive on this type of object ":target'
    ],
    'taxonomy' => [
        'invalid_type' => 'The taxonomy type is invalid.',
        'no_assigned_categories' => 'There are no categories to be assigned.',
        'failed_assign_category' => 'Failed Assign Category.',
    ],
    'source_category' => [
        'create_failed' => 'There was a problem creating this source category(es). Please try again.',
        'destroy_failed' => 'There was a problem destroying source category(es).',
        'not_exist' => 'The source category(es) do not exist.',
        'update_failed' => 'There was a problem updating this source category(es). Please try again.',
        'must_be_same_state_category' => 'Can\'t re-assign products to another category that has a different State Category.',
        'must_be_reassign_product' => 'Please re-assign product to another Source Category before delete.'
    ],
    'conversion' => [
        'create_failed' => 'There was a problem creating this conversion(s). Please try again.',
        'destroy_failed' => 'There was a problem destroying conversion(s).',
        'not_exist' => 'The conversion(s) do not exist.',
        'update_failed' => 'There was a problem updating this conversion(s). Please try again.',
        'update_finalized_failed' => 'Cannot update the conversion finalized.Please try again.',
        'finalize_failed' => 'Cannot finalize conversion without input lots. Please try again.',
        'invalid_input_lots' => 'Invalid input lots to create conversion(s).',
        'invalid_input_lot_quantity' => 'Invalid input lots quantity to create conversion(s).',
        'invalid_output_type' => 'The Conversion output type is invalid.',
        'output_product_is_not_available' => 'The output product is not available.',
        'cannot_use_lot_mapped_end_product' => 'Can\'t use lots with end products mapped to create conversion.'
    ],

    'product' => [
        'not_exist' => 'The product(s) do not exist.',
    ],
    'manifest' => [
        'invalid_type' => 'Manifest type is invalid.',
        'invalid_transport_type' => 'Manifest transport type is invalid.',
        'client_not_found' => 'Client is not found.',
        'driver_not_found' => 'Driver is not found.',
        'driver_helper_not_found' => 'Driver helper is not found.',
        'vehicle_not_found' => 'Vehicle is not found.',
        'transporter_not_found' => 'Transporter is not found.',
        'item' => [
            'invalid_type' => 'Item type is invalid.',
            'failed_add_to_qa_sample' => 'It is not allowed to add item to qa sample manifest.',
            'resource_is_required' => 'Item resource is required.',
            'duplicated_item' => 'Item is already added.',
            'plant_not_found' => 'Plant is not found.',
            'plant_not_available' => 'Plant :code is not available to transfer.',
            'propagation_not_found' => 'Propagation is not found.',
            'propagation_quantity_insufficient' => 'Quantity of propagation :code is insufficient.',
            'qa_sample_not_found' => 'QA sample is not found.',
            'inventory_not_found' => 'Inventory is not found.',
            'inventory_quantity_insufficient' => 'Quantity of inventory :code is insufficient.',
            'qa_type_not_allowed' => 'It is not allowed to add QA Sample to :type manifest.',
            'qa_status_must_be_open' => 'QA status must be Open.',
        ],
        'failed_create_item' => 'There was a problem adding item to manifest. Please try again.',
        'invalid_item_type' => 'Item type is invalid.',
        'action_not_allowed' => 'This action is not allowed for :status status.',
        'plant_not_found' => 'Plant is not found.',
        'propagation_not_found' => 'Propagation is not found.',
        'qa_sample_not_found' => 'QA sample is not found.',
        'inventory_not_found' => 'Inventory is not found.',
        'plant_not_available' => 'Plant :code is not available to transfer.',
        'publish' => [
            'action_failed' => 'Successful publish :count item(s). Failed publish item(s): :errors',
            'invalid_record' => 'There are an invalid record.',
            'status_not_allowed' => 'Manifest :code is not allowed to publish.',
        ],
        'void' => [
            'action_failed' => 'Successful void :count item(s). Failed void item(s): :errors',
            'invalid_record' => 'There are an invalid record.',
            'status_not_allowed' => 'Manifest :code is not allowed to void.',
        ],
        'cancel' => [
            'action_failed' => 'Successful cancel :count item(s). Failed cancel item(s): :errors',
            'invalid_record' => 'There are an invalid record.',
            'status_not_allowed' => 'Manifest :code is not allowed to cancel.',
        ],
        'delete' => [
            'action_failed' => 'Successful delete :count item(s). Failed delete item(s): :errors',
            'invalid_record' => 'There are an invalid record.',
            'status_not_allowed' => 'Manifest :code is not allowed to delete.',
        ],
        'receive' => [
            'action_failed' => 'Successful receive :count item(s). Failed receive item(s): :errors',
            'invalid_record' => 'There are an invalid record.',
            'status_not_allowed' => 'Manifest :code is not allowed to receive.',
        ],
        'reject' => [
            'action_failed' => 'Successful reject :count item(s). Failed reject item(s): :errors',
            'invalid_record' => 'There are an invalid record.',
            'status_not_allowed' => 'Manifest :code is not allowed to reject.',
        ],
        'mark_in_transit' => [
            'status_not_allowed' => 'Manifest :code is not allowed to mark in transit.',
            'action_failed' => 'Failed mark in transit.',
        ],
        'add_qa_sample' => [
            'type_not_allowed' => 'It is not allowed to add QA Sample to :type manifest.',
        ],
        'invalid_method' => 'The manifest method is invalid.',
        'create_failed' => 'There was a problem creating this manifest. Please try again.',
        'update_failed' => 'There was a problem updating this manifest. Please try again.',
        'delete_failed' => 'There was a problem deleting manifest :code. Please try again.',
    ]
];
