<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInternalIdToSpecifiedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $internalIdClasses = [
            \App\Models\Strain::class,
            \App\Models\Room::class,
            \App\Models\Propagation::class,
            \App\Models\GrowCycle::class,
            \App\Models\Plant::class,
            \App\Models\Harvest::class,
            \App\Models\Additive::class,
            \App\Models\Supplier::class,
            \App\Models\Disposal::class,
            \App\Models\Inventory::class,
            \App\Models\Conversion::class,
            \App\Models\QASample::class,
            \App\Models\SourceCategory::class,
            \App\Models\Taxonomy::class,
            \App\Models\Manifest::class,
            \App\Models\Vehicle::class,
            \App\Models\Driver::class,
            \App\Models\SourceProduct::class,
            \App\Models\PrintLabel::class,
            \App\Models\PrintLabelTemplate::class
        ];

        foreach ($internalIdClasses as $class) {
            $tableName = app($class)->getTable();
            Schema::table($tableName, function (Blueprint $table) use ($tableName) {
                $table->string('internal_id')->index()->after(Schema::hasColumn($tableName, 'license_id') ? 'license_id' : 'account_holder_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $internalIdClasses = collect( array_keys(config('internal_id.abbreviation')) );

        $internalIdClasses->unique()->each(function ($item, $key) {
            $tableName = app($item)->getTable();
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('internal_id');
            });
        });
    }
}
