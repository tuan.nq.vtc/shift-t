<?php

namespace App\Models\Traits;

use Exception;

trait WithoutEvents
{
    /**
     * @param array $data
     *
     * @return self
     */
    public function createWithoutEvents(array $data = []): self
    {
        return static::withoutEvents(
            fn() => $this->create($data)
        );
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function updateWithoutEvents(array $data = []): bool
    {
        return static::withoutEvents(
            fn() => $this->update($data)
        );
    }

    /**
     * @return bool|null
     *
     * @throws Exception
     */
    public function deleteWithoutEvents(): ?bool
    {
        return static::withoutEvents(
            fn() => $this->delete()
        );
    }

    /**
     * @return bool
     */
    public function saveWithoutEvents(): bool
    {
        return static::withoutEvents(
            fn() => $this->save()
        );
    }
}
