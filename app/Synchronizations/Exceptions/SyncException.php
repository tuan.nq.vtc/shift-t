<?php

namespace App\Synchronizations\Exceptions;

use Exception;

class SyncException extends Exception
{
    /**
     * @return static
     */
    public static function sendFailed()
    {
        return new static('Failed to send request');
    }
    /**
     * @return static
     */
    public static function buildResourceFailed()
    {
        return new static('Failed to build resource');
    }
}
