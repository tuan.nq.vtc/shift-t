<?php

namespace App\Cqrs\Base;

use Exception;

abstract class Valuable
{
    /**
     * @var array
     */
    protected $mapping = [];

    /**
     * @return array
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    /**
     * @param array $data
     */
    protected function map(array $data)
    {
        foreach ($this->mapping as $key => $value) {
            if (!isset($data[$value])) {
                continue;
            }

            $this->performMap($key, $data[$value]);
        }
    }

    /***
     * @return bool
     */
    public function isValid(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }

    /**
     * @param $key
     * @param $value
     */
    protected function performMap($key, $value)
    {

        $exp = explode('|', $key);
        $property = $exp[0];

        if (!property_exists($this, $property)) {
            return;
        }

        if (!isset($exp[1])) {
            $this->$property = $value;
            return;
        }

        switch ($exp[1]) {
            case 'string':
                $this->$property = (string)$value;
                break;

            case 'integer':
            case 'int':
                $this->$property = (int)$value;
                break;

            case 'float' :
            case 'decimal':
                $this->$property = (float)$value;
                break;

            case 'array':
                $this->$property = (array)$value;
                break;

            case 'boolean' :
            case 'bool':
                $this->$property = (bool)$value;
                break;

            default:
                $this->$property = $value;
        }

    }

    /**
     * @param array $attrs
     * @return array
     */
    public function checkEmpty(array $attrs): array
    {
        $data = [];

        foreach ($attrs as $attr => $name) {
            try {
                if (!property_exists($this, $attr)) {
                    continue;
                }

                if (empty($this->$attr)) {
                    throw new Exception(sprintf('%s is empty', $name));
                }
            } catch (Exception $e) {
                $data[] = $e->getMessage();
            }
        }

        return $data;
    }
}
