<?php

namespace App\Http\Controllers;

use App\Services\HarvestService;
use App\Services\PlantService;
use App\Services\StatisticService;
use App\Services\StrainService;
use Illuminate\Http\{JsonResponse, Request};
use Laravel\Lumen\Routing\Controller;

/**
 * Class StatisticController
 * @package App\Http\Controllers
 */
class StatisticController extends Controller
{
    private StrainService $strainService;
    private HarvestService $harvestService;
    private PlantService $plantService;
    private Request $request;

    /**
     * @var StatisticService
     */
    private StatisticService $statisticService;

    public function __construct(
        StrainService $strainService,
        PlantService $plantService,
        HarvestService $harvestService,
        StatisticService $statisticService,
        Request $request
    ) {
        $this->strainService = $strainService;
        $this->plantService = $plantService;
        $this->harvestService = $harvestService;
        $this->request = $request;
        $this->statisticService = $statisticService;
    }

    /**
     * @return JsonResponse
     */
    public function harvests(): JsonResponse
    {
        return response()->json($this->statisticService->harvests($this->request->get('license_id')));
    }

    /**
     * Total harvested flower weights
     *
     * @return JsonResponse
     */
    public function harvestedFlowerWeights(): JsonResponse
    {
        return response()->json(
            $this->harvestService->getHarvestedFlowerWeights($this->request->get('license_id'))
        );
    }

    /**
     * Total harvested other material weights
     *
     * @return JsonResponse
     */
    public function harvestedOtherWeights(): JsonResponse
    {
        return response()->json(
            $this->harvestService->getHarvestedOtherMaterialWeights($this->request->get('license_id'))
        );
    }

    /**
     * Count all non-trashed plants by grow status
     *
     * @return JsonResponse
     */
    public function plantsByGrowStatuses(): JsonResponse
    {
        return response()->json(
            $this->statisticService->countPlantsByGrowStatuses($this->request->get('license_id')),
        );
    }

    public function plantsByStrains(): JsonResponse
    {
        $defaultStatisticizedNumber = 3;
        $licenseId = $this->request->get('license_id', '');
        // How many strains will be statisticized beside the last one for the others
        $shownStrains = $this->request->get('shown_strains', 3);
        if ($shownStrains < 3) {
            $shownStrains = $defaultStatisticizedNumber;
        }
        return response()->json(
            [
                'data' => $this->strainService->countPlantsByStrains($licenseId, $shownStrains),
                'total_strain' => $this->strainService->count($licenseId)
            ]
        );
    }
}
