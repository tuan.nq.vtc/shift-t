<?php

namespace App\Console\Commands\LicenseResource;

use App\Models\{License, LicenseResource};
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Throwable;

class CleanCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'Remove resource(s) of removed license';
    /**
     * @var string
     */
    protected $signature = 'license-resource:clean {license?* : The code of removed license or "all" to clean resources of all removed license}
                            {--resource=* : The resource will be clean up [with prefix by regulator like leaf.area or metrc.location]or all}';

    private ?array $licenseResources;

    /**
     * @throws Throwable
     */
    public function handle()
    {
        $this->loadLicenseResources();
        foreach ($this->getLicenseCodes() as $code) {
            $licenses = $this->laravel->make(License::class)->whereCode($code)->with(['licenseResources'])->get();

            if ($licenses->isEmpty()) {
                $this->error("Unable to find any trashed license with code [{$code}].");
            } else {
                $this->cleanLicenses($licenses);
                $this->info("The license [{$code}] and resources have been cleaned!");
                $licenses->forceDelete();
            }
        }
    }

    private function loadLicenseResources(): void
    {
        $this->licenseResources = config('traceability.license_resources', []);
    }

    private function getLicenseCodes(): array
    {
        $licenseCodes = (array)$this->argument('license');

        if (count($licenseCodes) === 1 && $licenseCodes[0] === 'all') {
            return Arr::pluck($this->laravel->make(License::class)->onlyTrashed(), 'code');
        }

        return array_values(array_filter(array_unique($licenseCodes)));
    }

    /**
     * @param Collection|License[] $licenses
     *
     * @throws Throwable
     */
    private function cleanLicenses(Collection $licenses): void
    {
        $resources = $this->getResources();
        $allResources = count($resources) === 1 && $resources[0] === 'all';
        foreach ($licenses as $license) {
            foreach ($license->getSortedResources() as $licenseResource) {
                if ($allResources) {
                    $this->cleanResource($licenseResource);
                } elseif (in_array($licenseResource->resource, $resources)) {
                    $this->cleanResource($licenseResource);
                }
            }
        }
    }

    private function getResources(): array
    {
        return (array)$this->option('resource');
    }

    /**
     * @param LicenseResource $licenseResource
     *
     * @throws Throwable
     */
    private function cleanResource(LicenseResource $licenseResource): void
    {
        $modelClass = $this->licenseResources[$licenseResource->resource]['model'] ?? null;

        if (!$modelClass) {
            $this->error('Not found any model class configured with license resource ['.$licenseResource->resource.']');
            return;
        }

        $this->laravel->make($modelClass)->licenseId($licenseResource->license_id)->forceDelete();
    }
}
