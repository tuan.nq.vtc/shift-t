<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Exceptions\GeneralException;
use App\Models\{InventoryType, TraceableModel};
use Exception;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class InventoryTypeProcess
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 * @property InventoryType|TraceableModel $traceableModel
 */
class InventoryTypeProcess extends BaseProcess
{

    /**
     * @throws Throwable
     */
    public function create()
    {
        try {
            DB::beginTransaction();
            $this->syncInventoryType();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        } catch (Throwable $throwable) {
            throw $throwable;
        }
    }

    /**
     * @throws Throwable
     */
    public function update()
    {
        try {
            DB::beginTransaction();
            $this->syncInventoryType();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        } catch (Throwable $throwable) {
            throw $throwable;
        }
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    private function syncInventoryType(): self
    {
        $activeInventoryItemsResponse = $this->client->getRequest()->get(
            self::INVENTORY_ITEM_ENDPOINT . 'active',
            ['licenseNumber' => $this->traceableModel->license->code]
        );

        if ($activeInventoryItemsResponse->failed()) {
            throw new GeneralException('Unable to fetch inventory items from METRC regulator');
        }

        $activeInventoryItems = $activeInventoryItemsResponse->json();

        foreach ($activeInventoryItems as $item) {
            if ($item['Name'] === $this->traceableModel->name) {
                $this->traceableModel->update(
                    ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'sync_code' => $item['Id']],
                );
                break;
            }
        }

        return $this;
    }
}
