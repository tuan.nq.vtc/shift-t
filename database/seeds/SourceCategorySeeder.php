<?php

use App\Models\License;
use App\Models\SourceCategory;
use Illuminate\Database\Seeder;

class SourceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (License::isTest()->pluck('account_holder_id')->unique() as $accountHolderId) {
            factory(SourceCategory::class, 5)->create(['account_holder_id' => $accountHolderId]);
        }
    }
}
