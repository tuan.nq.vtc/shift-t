<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Facades\FileUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class GenerateMultipleImageSizes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:generate-sizes
                            {--model=}
                            {--ids=}
                            {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store multiple image sizes for models';

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $imageSizeConfigs = config('image_sizes');
        if ($this->option('all')) {
            foreach ($imageSizeConfigs as $modelName => $imageSizes) {
                $this->info('Start generating image sizes for model: ' . $modelName);
                $imageFieldName = self::getImageFieldByModelName($modelName);
                $modelName::whereNotNull($imageFieldName)->chunk(
                    1000,
                    function (Collection $models) use ($imageSizes) {
                        foreach ($models as $model) {
                            $this->generateImageSizesForModel($model, $imageSizes);
                        }
                    }
                );
                $this->info('End generate image sizes for model: ' . $modelName);
            }
            return;
        }

        if (empty($this->option('model'))) {
            $this->error('model argument is required');
            return;
        }
        $modelName = $this->option('model');
        if (empty($imageSizeConfigs[$modelName])) {
            $this->error('Image sizes config for model ' . $modelName . ' does not exist');
            return;
        }

        $imageFieldName = self::getImageFieldByModelName($modelName);
        $models = $modelName::whereNotNull($imageFieldName);
        if (!empty($this->option('ids'))) {
            $models = $modelName::whereIn('id', explode(',' ,$this->option('ids')));
        }

        $models->chunk(1000, function (Collection $models) use ($imageSizeConfigs, $modelName) {
            foreach ($models as $model) {
                $this->info('Start generating image sizes for model: ' . $modelName);
                $this->generateImageSizesForModel($model, $imageSizeConfigs[$modelName]);
                $this->info('End generate image sizes for model: ' . $modelName);
            }
        });
    }

    /**
     * @param Model $model
     * @param array $imageSizes
     * @return void
     * @throws \Throwable
     */
    private function generateImageSizesForModel(Model $model, array $imageSizes): void
    {
        $originalPath = $model->getAttribute(self::getImageFieldByModelName(get_class($model)));
        if (is_null($originalPath)) {
            return;
        }

        $storedPaths = [];
        try {
            foreach ($imageSizes as $imgSize => $config) {
                if (!FileUpload::existsResizedImage($originalPath, $imgSize)) {
                    $storedPaths[] = FileUpload::generateResizedImage($originalPath, $imgSize, $config);
                }
            }
        } catch (\Throwable $e) {
            Storage::delete($storedPaths);
            $this->error('Error in model {' . get_class($model) . "}, id #{$model->id}: " . $e->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    private static function getImageFieldByModelName(string $model): string
    {
        switch ($model) {
            case \App\Models\Strain::class:
                return 'image';
            default:
                throw new \Exception('Cannot get image by model ' . $model);
        }
    }
}
