<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\{AdditiveOperation};
use App\Services\RoomService;
use Illuminate\Http\{Request};

/**
 * Class RoomController
 *
 * @package App\Http\Controllers
 * @property RoomService $service
 */
class RoomController extends CRUDController
{
    /**
     * RoomController constructor.
     *
     * @param RoomService $service
     * @param Request $request
     */
    public function __construct(RoomService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'user_id' => [],
            'license_id' => [],
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                "unique:App\Models\Room,name,NULL,id,license_id,\"{$this->request->get('license_id')}\",deleted_at,NULL"
            ],
            'room_type_id' => ['required'],
            'is_quarantine' => ['sometimes', 'required'],
            'dimension.width' => ['nullable', 'numeric'],
            'dimension.length' => ['nullable', 'numeric'],
            'rfid_tag' => ['nullable', 'numeric'],
        ];
    }

    public function show($id)
    {
        return response()->json(
            $this->service->get(
                $id,
                $this->request->get('ipm_log_limit', AdditiveOperation::DEFAULT_LIMIT),
                $this->request->get('feed_log_limit', AdditiveOperation::DEFAULT_LIMIT)
            )
        );
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'user_id' => [],
            'name' => [
                'sometimes',
                'required',
                'string',
                'min:3',
                'max:150',
                "unique:App\Models\Room,name,{$id},id,license_id,\"{$this->request->get('license_id')}\",deleted_at,NULL"
            ],
            'status' => ['sometimes', 'required', 'boolean'],
            'room_type_id' => ['sometimes', 'required'],
            'is_quarantine' => ['sometimes', 'required', 'boolean'],
            'dimension.width' => ['sometimes', 'numeric'],
            'dimension.length' => ['sometimes', 'numeric'],
            'rfid_tag' => ['nullable', 'numeric'],
        ];
    }


    /**
     * @param $category
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function getByCategory($category)
    {
        return response()->json($this->service->getByCategory($category));
    }

    public function index()
    {
        return response()->json($this->service->index(Criteria::createFromRequest($this->request)));
    }

    public function getDetail($id)
    {
        return response()->json($this->service->getDetail($id));
    }

    public function getStatistics(): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->service->getStatistics());
    }
}
