<?php

namespace Database\Seeders\DataCorrection;

use App\Helpers\ModelHelper;
use Illuminate\Database\Seeder;

class UpdateInternalId extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        $interalIdClasses = collect( array_keys(config('internal_id.abbreviation')) );

        $interalIdClasses->unique()->each(function ($modelClass, $key) {
            app($modelClass)->where('internal_id', '')->chunkById(500, function ($collection) use ($modelClass) {
                foreach ($collection as $model) {
                    try
                    {
                        $model->update(
                            [
                                'internal_id' => ModelHelper::generateInternalId(
                                    $modelClass,
                                    $model->getAttribute('license_id'),
                                    $model->getAttribute('account_holder_id'),
                                    $model->getSubLevelForInternalId()
                                )
                            ]
                        );
                    } 
                    catch (\Exception $e)
                    {
                        continue;
                    }                    
                }
            });
        });
    }
}
