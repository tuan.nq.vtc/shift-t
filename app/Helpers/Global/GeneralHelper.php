<?php

if (!function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string  $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath().'/config'.($path ? '/'.$path : $path);
    }
}

if (!function_exists('get_classname')) {
    /**
     * Get class name.
     *
     * @param  object|string  $instance
     * @param  bool  $short
     *
     * @return string
     */
    function get_classname($instance, $short = false)
    {
        $class = is_object($instance) ? get_class($instance) : $instance;

        return $short ? Illuminate\Support\Str::substr(strrchr($class, "\\"), 1) : $class;
    }
}

if (!function_exists('generate_sync_code')) {
    /**
     * Get class name.
     *
     * @param  string  $type (mixed|number)
     *
     * @return string
     */
    function generate_sync_code(string $type = 'mixed'): string
    {
        if ($type === 'mixed')
            return strtoupper(substr(sha1(mt_rand()), 17, 4));

        if ($type === 'number')
            return str_pad(rand(1, 999999), 6, '0', STR_PAD_LEFT);

        return '';
    }
}
