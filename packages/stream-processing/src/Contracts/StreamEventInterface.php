<?php

namespace Bamboo\StreamProcessing\Contracts;

interface StreamEventInterface
{
    public function getId(): string;

    public function setId(string $key): void;

    public function getPayload(): array;

    public function setPayload(array $payload): void;

    public function getMetadata(): array;

    public function setMetadata(array $metadata): void;

    public function getHeaders(): array;

    public function setHeaders(array $headers): void;

    public function getSender(): string;

    public function setSender(string $sender): void;
}
