<?php

/** @var Factory $factory */

use App\Models\License;
use App\Models\Manifest;
use App\Models\ManifestItem;
use App\Models\Strain;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    ManifestItem::class,
    function (Faker $faker) {
        $quantity = $faker->randomFloat(2, 0, 100);
        $price = $faker->randomFloat(2, 0, 1000);
        return [
            'manifest_id' => fn($data) => factory(Manifest::class)->create()->id,
            'type' => $faker->randomElement(
                [
                    ManifestItem::TYPE_PLANT,
                    ManifestItem::TYPE_PROPAGATION,
                    ManifestItem::TYPE_QA_SAMPLE,
                    ManifestItem::TYPE_UNLOTTED_INVENTORY,
                    ManifestItem::TYPE_SOURCE_PRODUCT_INVENTORY,
                    ManifestItem::TYPE_END_PRODUCT_INVENTORY
                ]
            ),
            'quantity' => $quantity,
            'price' => $price,
            'total' => round($quantity * $price, 2),
            'is_sample' => rand(0, 1),
            'is_for_extraction' => rand(0, 1),
        ];
    }
);

