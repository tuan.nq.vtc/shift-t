<?php


namespace App\Notification\Commands;

use App\Events\TraceableModel\TraceableModelCreated;
use App\Models\{License, State, Strain};
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;

class TestSyncFailedCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'Create the fake strain to test case: sync this strain failed. The system will create new notification of this case to push the client.';
    /**
     * @var string
     */
    protected $signature = 'notification:test-sync-failed {--state= : The state resource(s) synced will be failed}';

    /**
     * @var State
     */
    protected $state;

    /**
     * @var Strain
     */
    protected $strain;

    /**
     * @var License
     */
    protected $license;

    public function handle()
    {
        if (app()->environment() == 'production') {
            $this->error("Can not do this action on production environment");
            exit();
        }

        $stateCode = $this->getStateCode();

        if (!$stateCode) {
            $this->error("Please specify state (ca,wa ...)");
            exit();
        }

        if (!$stateCode || !($this->state = State::whereCode($stateCode)->first())) {
            $this->error("Invalid state [{$stateCode}]");
            exit();
        }

        if (!($this->license = $this->state->licenses()
            ->where('status', License::STATUS_ACTIVE_LINKED)->get()->first())) {
            $this->error("State has not any license [{$stateCode}]");
            exit();
        }

        if (!($this->strain = Strain::licenseId($this->license->id)->get()->first())) {
            $this->error("The license has not any strain [{$this->license->name}]");
            exit();
        }

        Event::dispatch(new TraceableModelCreated($this->cloneStrain()));
    }

    /**
     * @return string|null
     */
    protected function getStateCode(): ?string
    {
        if ($state = $this->option('state')) {
            return Str::upper($state);
        }

        return null;
    }

    /**
     * With LEAF/METRIC system, the name of strain is required field.
     * We made a new strain with empty name attribute --> the syncing process for this strain will be failed.
     *
     * @return Strain
     */
    protected function cloneStrain(): Strain
    {
        $clone = $this->strain->replicate();
        $clone->name = '';
        $clone->id = $clone->generateUuid();
        $clone->saveWithoutEvents();
        return $clone;
    }
}
