<?php

namespace App\Http\Controllers;

use App\Services\EndProductService;
use Illuminate\Http\Request;

class EndProductController extends CRUDController
{
    public function __construct(EndProductService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [];
    }

    public function getUpdateRules(string $id): array
    {
        return [];
    }

}
