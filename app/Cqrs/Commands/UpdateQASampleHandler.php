<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Handleable;
use App\Models\Inventory;
use App\Models\QASample;
use App\Models\StateCategory;
use Exception;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;
use Throwable;

class UpdateQASampleHandler extends Handleable
{
    /**
     * @var QASample
     */
    private $qaSample;


    public function __construct(
        QASample $qaSample,
        LoggerInterface $logger
    ) {
        $this->qaSample = $qaSample;
        $this->logger = $logger;
    }

    /**
     * @param UpdateQASampleCommand $command
     * @return array
     * @throws Throwable
     */
    public function handle(UpdateQASampleCommand $command): array
    {
        $errors = $this->ensureCanUpdateQASample($command);

        if (!empty($errors)) {
            $this->log($command, $errors);
            throw new Exception('Invalid request');
        }
        try {
            return $this->performUpdate($command->getItem(), $command->getUserId());
        } catch (Throwable $e) {
            if (!$this->isRecordedErrors()) {
                $this->log($command, $e->getMessage(), $e->getTrace());
            }
            throw $e;
        }
    }

    /**
     * @param \App\Cqrs\Values\QASample $qaSampleData
     * @param string $userId
     * @return array
     * @throws Exception
     */
    private function performUpdate(\App\Cqrs\Values\QASample $qaSampleData, string $userId): array
    {
        /** @var QASample $qaSample */
        $qaSample = $this->qaSample->find($qaSampleData->getId());
        if ($qaSample instanceof QASample !== true) {
            throw new Exception(sprintf('The QA Sample \'%s\' not found', $qaSampleData->getId()));
        }
        try {
            DB::beginTransaction();
            $qaSample->setAttribute('updated_by', $userId);
            $qaSample->setAttribute('results', $qaSampleData->getResults());
            $qaSample->setAttribute('tested_at', $qaSampleData->getTestedAt());
            $qaSample->setAttribute('lab_id', $qaSampleData->getLabId());
            $qaSample->setAttribute('sample_type', $qaSampleData->getSampleType());
            if ($qaSampleData->getStatus() != $qaSample->getStatus()) {
                $qaSample->setAttribute('status', $qaSampleData->getStatus());
                $inventoryQASampleStatus = $qaSampleData->getStatus() == QASample::STATUS_PASSED ?
                    Inventory::QA_STATUS_PASSED : Inventory::QA_STATUS_FAILED;
                $qaSample->sourceInventory()->update(['qa_status' => $inventoryQASampleStatus,]);

                if ($qaSampleData->getIsModifyChildLot() && $qaSample->sourceInventory) {
                    Inventory::query()->whereIn('id', $qaSample->sourceInventory->getAllChildrenIds())->update([
                        'final_qa_sample_id' => $qaSample->id,
                        'qa_status' => $inventoryQASampleStatus
                    ]);
                }
            }

            $qaSample->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new Exception(sprintf('Update the QA Sample result failed:\%s\' ', $e->getMessage()));
        }

        return $qaSampleData->toArray();
    }

    /**
     * @param UpdateQASampleCommand $command
     * @return array
     */
    private function ensureCanUpdateQASample(UpdateQASampleCommand $command): array
    {
        $errorBag = [];
        try {
            $obj = $this->qaSample->find($command->getId());
            if ($obj instanceof QASample !== true) {
                throw new Exception(sprintf('The QA Sample \'%s\' not found', $command->getId()));
            }
            // TODO confirm with PM about the QASample can be updated with which status
        } catch (Throwable $e) {
            $errorBag[] = $e->getMessage();
        }

        return $errorBag;
    }
}
