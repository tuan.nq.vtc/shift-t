<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\{License, Strain, TraceableModel, User};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Strain::class,
    function (Faker $faker) {
        $indica = rand(0, 100);
        return [
            "license_id" => fn() => factory(License::class)->create()->id,
            "internal_id" => fn($data) => ModelHelper::generateInternalId(
                Strain::class,
                $data['license_id']
            ),
            'name' => $faker->words(2, true) . ' - ' . $faker->randomNumber(2),
            'description' => $faker->text(50),
            'status' => rand(0, 1),
            "sync_code" => "WAG010101.ST" . $faker->regexify('[A-Z0-9]{2}'), // WAG010101.ST4U
            "sync_status" => TraceableModel::SYNC_STATUS_SYNCED,
            'info' => [
                'testing_status' => rand(0, 1),
                'thc' => $faker->randomFloat(),
                'cbd' => $faker->randomFloat(),
                'indica' => $indica,
                'sativa' => 100 - $indica
            ],
            "created_by" => function () {
                return factory(User::class)->create()->id;
            },
            "updated_by" => function () {
                return factory(User::class)->create()->id;
            },
            "synced_at" => $faker->dateTimeThisYear(),
        ];
    }
);
