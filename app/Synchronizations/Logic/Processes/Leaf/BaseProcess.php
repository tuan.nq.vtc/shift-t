<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Models\BaseModel;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Logic\Processes\BaseProcess as LogicBaseProcess;
use Exception;

/**
 * Class BaseProcess
 * @package App\Synchronizations\Logic\Processes\Leaf
 * @property BaseModel|TraceableModelInterface traceableModel
 */
class BaseProcess extends LogicBaseProcess
{
    public const INVENTORY_ENDPOINT = '/inventories/';
    public const INVENTORY_TYPE_ENDPOINT = '/inventory_types/';
    public const MOVE_INVENTORY_TO_PLANTS_ENDPOINT = '/move_inventory_to_plants/';
    public const PLANT_ENDPOINT = '/plants/';

    /**
     * @throws Exception
     */
    protected function create(): void
    {
        $globalId = $this->getGlobalId($this->payload[0] ?? []);
        if (!$globalId) {
            throw new Exception(
                "Not found any record matched with [external_id: {$this->traceableModel->id}] in LEAF API responseData"
            );
        }

        $this->traceableModel->updateSyncCode($globalId);
    }

    /**
     * @param array $responseData
     * @return string|null
     */
    protected function getGlobalId(array $responseData): ?string
    {
        if (empty($responseData['external_id']) || empty($responseData['global_id'])
            || $responseData['external_id'] !== $this->traceableModel->id) {
            return null;
        }

        return $responseData['global_id'];
    }
}
