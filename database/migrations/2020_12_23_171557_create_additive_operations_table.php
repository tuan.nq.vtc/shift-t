<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditiveOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'additive_operations',
            function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->uuid('license_id')->index();
                $table->uuid('additive_id')->references('id')->on('additives')->cascadeOnDelete();
                $table->unsignedTinyInteger('additive_type')->default('0')->comment('0- pesticide, 1- nutrient');
                $table->uuid('additive_inventory_id')->references('id')->on('additive_inventories')->cascadeOnDelete();
                $table->string('target', 50)->nullable()->index();
                $table->double('applied_quantity', 10, 2);
                $table->double('unit_price', 10, 2)->default(0);
                $table->string('uom')->nullable();
                $table->dateTime('applied_date');
                $table->text('comment')->nullable();

                $table->string('target_type', 50);
                $table->uuid('target_id');
                $table->index(['target_type', 'target_id']);

                $table->uuid('supplier_id')->references('id')->on('supplier')->cascadeOnDelete();

                $table->uuid('applied_by')->nullable();
                $table->uuid('created_by')->nullable();
                $table->uuid('updated_by')->nullable();

                $table->boolean('synced')->default(false);

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additive_operations');
    }
}
