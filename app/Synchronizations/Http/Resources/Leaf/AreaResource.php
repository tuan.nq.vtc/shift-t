<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Helpers\LeafHelper;
use Illuminate\Http\Request;
use App\Models\{Room, TraceableModel};
use Exception;
use Throwable;

/**
 * Class Area
 * @package App\Synchronizations\Http\Resources\Leaf
 */
class AreaResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_AREA;

    /**
     * @return self
     */
    protected function create(): self
    {
        return parent::create()
            ->setBody(
                [
                    'area' => [
                        [
                            'name' => $this->traceableModel->name,
                            'type' => $this->traceableModel->is_quarantine == 1 ? 'quarantine' : 'non-quarantine',
                            'external_id' => $this->traceableModel->id,
                        ],
                    ],
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync room update without LEAF global_id [sync_code]')
        );

        return parent::update()
            ->setBody(
                [
                    'area' => [
                        'name' => $this->traceableModel->name,
                        'type' => $this->traceableModel->is_quarantine == 1 ? 'quarantine' : 'non-quarantine',
                        'external_id' => $this->traceableModel->id,
                        'global_id' => $this->traceableModel->sync_code,
                    ],
                ]
            );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync room deletion without LEAF global_id [sync_code]')
        );

        return parent::delete();
    }
}
