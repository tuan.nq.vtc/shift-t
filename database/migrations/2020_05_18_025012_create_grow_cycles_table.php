<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrowCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grow_cycles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->string('grow_status')->index()->nullable();
            $table->string('name')->index();

            $table->date('planted_at')->nullable();
            $table->date('est_harvested_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grow_cycles');
    }
}
