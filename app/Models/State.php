<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class State
 * @package App\Models
 *
 * @property-read string $code
 * @property-read string $name
 * @property-read string $capital
 * @property-read string $regulator
 * @property-read string $api_url
 */
class State extends Model
{
    use \Sushi\Sushi;

    public const REGULATOR_LEAF = 'leaf';
    public const REGULATOR_METRC = 'metrc';
    public const REGULATOR_CCRS = 'ccrs';

    public const ALABAMA = 'AL';
    public const ALASKA = 'AK';

    public const ARIZONA = 'AZ';
    public const ARKANSAS = 'AR';

    public const CALIFORNIA = 'CA';
    public const COLORADO = 'CO';
    public const CONNECTICUT = 'CT';

    public const DELAWARE = 'DE';

    public const FLORIDA = 'FL';

    public const GEORGIA = 'GA';

    public const HAWAII = 'HI';

    public const IDAHO = 'ID';
    public const ILLINOIS = 'IL';
    public const INDIANA = 'IN';
    public const IOWA = 'IA';

    public const KANSAS = 'KS';
    public const KENTUCKY = 'KY';
    public const LOUISIANA = 'LA';

    public const MAINE = 'ME';
    public const MARYLAND = 'MD';
    public const MASSACHUSETTS = 'MA';
    public const MICHIGAN = 'MI';
    public const MINNESOTA = 'MN';
    public const MISSISSIPPI = 'MS';
    public const MISSOURI = 'MO';
    public const MONTANA = 'MT';

    public const NEBRASKA = 'NE';
    public const NEVADA = 'NV';
    public const NEW_HAMPSHIRE = 'NH';
    public const NEW_JERSEY = 'NJ';
    public const NEW_MEXICO = 'NM';
    public const NEW_YORK = 'NY';
    public const NORTH_CAROLINA = 'NC';
    public const NORTH_DAKOTA = 'ND';

    public const OHIO = 'OH';
    public const OKLAHOMA = 'OK';
    public const OREGON = 'OR';

    public const PENNSYLVANIA = 'PA';

    public const RHODE_ISLAND = 'RI';

    public const SOUTH_CAROLINA = 'SC';
    public const SOUTH_DAKOTA = 'SD';

    public const TENNESSEE = 'TN';
    public const TEXAS = 'TX';

    public const UTAH = 'UT';

    public const VERMONT = 'VT';
    public const VIRGINIA = 'VA';

    public const WASHINGTON = 'WA';
    public const WEST_VIRGINIA = 'WV';
    public const WISCONSIN = 'WI';
    public const WYOMING = 'WY';

    protected $rows = [
        [
            'code' => self::ALABAMA,
            'name' => 'Alabama',
            'capital' => 'Montgomery',
            'regulator' => ''
        ],
        [
            'code' => self::ALASKA,
            'name' => 'Alaska',
            'capital' => 'Juneau',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::ARIZONA,
            'name' => 'Arizona',
            'capital' => 'Phoenix',
            'regulator' => ''
        ],
        [
            'code' => self::ARKANSAS,
            'name' => 'Arkansas',
            'capital' => 'Little Rock',
            'regulator' => ''
        ],
        [
            'code' => self::CALIFORNIA,
            'name' => 'California',
            'capital' => 'Sacramento',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::COLORADO,
            'name' => 'Colorado',
            'capital' => 'Denver',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::CONNECTICUT,
            'name' => 'Connecticut',
            'capital' => 'Hartford',
            'regulator' => ''
        ],
        [
            'code' => self::DELAWARE,
            'name' => 'Delaware',
            'capital' => 'Dover',
            'regulator' => ''
        ],
        [
            'code' => self::FLORIDA,
            'name' => 'Florida',
            'capital' => 'Tallahassee',
            'regulator' => ''
        ],
        [
            'code' => self::GEORGIA,
            'name' => 'Georgia',
            'capital' => 'Atlanta',
            'regulator' => ''
        ],
        [
            'code' => self::HAWAII,
            'name' => 'Hawaii',
            'capital' => 'Honolulu',
            'regulator' => ''
        ],
        [
            'code' => self::IDAHO,
            'name' => 'Idaho',
            'capital' => 'Boise',
            'regulator' => ''
        ],
        [
            'code' => self::ILLINOIS,
            'name' => 'Illinois',
            'capital' => 'Springfield',
            'regulator' => ''
        ],
        [
            'code' => self::INDIANA,
            'name' => 'Indiana',
            'capital' => 'Indianapolis',
            'regulator' => ''
        ],
        [
            'code' => self::IOWA,
            'name' => 'Iowa',
            'capital' => 'Des Moines',
            'regulator' => ''
        ],
        [
            'code' => self::KANSAS,
            'name' => 'Kansas',
            'capital' => 'Topeka',
            'regulator' => ''
        ],
        [
            'code' => self::KENTUCKY,
            'name' => 'Kentucky',
            'capital' => 'Frankfort',
            'regulator' => ''
        ],
        [
            'code' => self::LOUISIANA,
            'name' => 'Louisiana',
            'capital' => 'Baton Rouge',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::MAINE,
            'name' => 'Maine',
            'capital' => 'Augusta',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::MARYLAND,
            'name' => 'Maryland',
            'capital' => 'Annapolis',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::MASSACHUSETTS,
            'name' => 'Massachusetts',
            'capital' => 'Boston',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::MICHIGAN,
            'name' => 'Michigan',
            'capital' => 'Lansing',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::MINNESOTA,
            'name' => 'Minnesota',
            'capital' => 'St. Paul',
            'regulator' => ''
        ],
        [
            'code' => self::MISSISSIPPI,
            'name' => 'Mississippi',
            'capital' => 'Jackson',
            'regulator' => ''
        ],
        [
            'code' => self::MISSOURI,
            'name' => 'Missouri',
            'capital' => 'Jefferson City',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::MONTANA,
            'name' => 'Montana',
            'capital' => 'Helena',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::NEBRASKA,
            'name' => 'Nebraska',
            'capital' => 'Lincoln',
            'regulator' => ''
        ],
        [
            'code' => self::NEVADA,
            'name' => 'Nevada',
            'capital' => 'Carson City',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::NEW_HAMPSHIRE,
            'name' => 'New Hampshire',
            'capital' => 'Concord',
            'regulator' => ''
        ],
        [
            'code' => self::NEW_JERSEY,
            'name' => 'New Jersey',
            'capital' => 'Trenton',
            'regulator' => ''
        ],
        [
            'code' => self::NEW_MEXICO,
            'name' => 'New Mexico',
            'capital' => 'Santa Fe',
            'regulator' => ''
        ],
        [
            'code' => self::NEW_YORK,
            'name' => 'New York',
            'capital' => 'Albany',
            'regulator' => ''
        ],
        [
            'code' => self::NORTH_CAROLINA,
            'name' => 'North Carolina',
            'capital' => 'Raleigh',
            'regulator' => ''
        ],
        [
            'code' => self::NORTH_DAKOTA,
            'name' => 'North Dakota',
            'capital' => 'Bismarck',
            'regulator' => ''
        ],
        [
            'code' => self::OHIO,
            'name' => 'Ohio',
            'capital' => 'Columbus',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::OKLAHOMA,
            'name' => 'Oklahoma',
            'capital' => 'Oklahoma City',
            'regulator' => ''
        ],
        [
            'code' => self::OREGON,
            'name' => 'Oregon',
            'capital' => 'Salem',
            'regulator' => self::REGULATOR_METRC
        ],
        [
            'code' => self::PENNSYLVANIA,
            'name' => 'Pennsylvania',
            'capital' => 'Harrisburg',
            'regulator' => ''
        ],
        [
            'code' => self::RHODE_ISLAND,
            'name' => 'Rhode Island',
            'capital' => 'Providence',
            'regulator' => ''
        ],
        [
            'code' => self::SOUTH_CAROLINA,
            'name' => 'South Carolina',
            'capital' => 'Columbia',
            'regulator' => ''
        ],
        [
            'code' => self::SOUTH_DAKOTA,
            'name' => 'South Dakota',
            'capital' => 'Pierre',
            'regulator' => ''
        ],
        [
            'code' => self::TENNESSEE,
            'name' => 'Tennessee',
            'capital' => 'Nashville',
            'regulator' => ''
        ],
        [
            'code' => self::TEXAS,
            'name' => 'Texas',
            'capital' => 'Austin',
            'regulator' => ''
        ],
        [
            'code' => self::UTAH,
            'name' => 'Utah',
            'capital' => 'Salt Lake City',
            'regulator' => ''
        ],
        [
            'code' => self::VERMONT,
            'name' => 'Vermont',
            'capital' => 'Montpelier',
            'regulator' => ''
        ],
        [
            'code' => self::VIRGINIA,
            'name' => 'Virginia',
            'capital' => 'Richmond',
            'regulator' => ''
        ],
        [
            'code' => self::WASHINGTON,
            'name' => 'Washington',
            'capital' => 'Olympia',
            'regulator' => self::REGULATOR_CCRS
        ],
        [
            'code' => self::WEST_VIRGINIA,
            'name' => 'West Virginia',
            'capital' => 'Charleston',
            'regulator' => ''
        ],
        [
            'code' => self::WISCONSIN,
            'name' => 'Wisconsin',
            'capital' => 'Madison',
            'regulator' => ''
        ],
        [
            'code' => self::WYOMING,
            'name' => 'Wyoming',
            'capital' => 'Cheyenne',
            'regulator' => ''
        ]
    ];

    /**
     * List of API regulators
     * Use for seed to sale sync
     *
     * @return string[]
     */
    public static function getRegulators(): array
    {
        return [
            State::REGULATOR_LEAF,
            State::REGULATOR_METRC,
            State::REGULATOR_CCRS,
        ];
    }

    public function licenses(): HasMany
    {
        return $this->hasMany(License::class, 'state_code', 'code');
    }

    public function getRegulatorBaseUrl(): ?string
    {
        return config("traceability.states.{$this->code}.base_url");
    }

    public function getRegulatorResources(): ?array
    {
        return config("traceability.regulators.{$this->regulator}.resources");
    }

    public function getRegulatorLogic(): ?array
    {
        return config("traceability.regulators.{$this->regulator}.logic");
    }

    public function hasAsyncIntegration():bool
    {
        return ($this->regulator == self::REGULATOR_CCRS);
    }
}
