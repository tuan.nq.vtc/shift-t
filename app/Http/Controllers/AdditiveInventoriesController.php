<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\Supplier;
use App\Rules\ModelsExist;
use App\Services\{AdditiveInventoryService, AdditiveService};
use Illuminate\Http\{JsonResponse, Request,Response};
use Illuminate\Support\Arr;
use Laravel\Lumen\Routing\Controller as BaseController;
use Throwable;

/**
 * Class RoomSubroomsController
 *
 * @package App\Http\Controllers
 */
class AdditiveInventoriesController extends BaseController
{
    private AdditiveInventoryService $additiveInventoryService;

    private AdditiveService $additiveService;

    private Request $request;

    public function __construct(
        AdditiveInventoryService $additiveInventoryService,
        AdditiveService $additiveService,
        Request $request
    ) {
        $this->additiveInventoryService = $additiveInventoryService;
        $this->additiveService = $additiveService;
        $this->request = $request;
    }

    /**
     *
     * @param $additiveId
     *
     * @return JsonResponse
     */
    public function list($additiveId): JsonResponse
    {
        return response()->json(
            $this->additiveInventoryService->list(
                Criteria::createFromRequest($this->request)->addFilter('additive_id', $additiveId)
            )
        );
    }

    /**
     * @param $additiveId
     *
     * @return JsonResponse
     */
    public function index($additiveId): JsonResponse
    {
        return response()->json(
            $this->additiveInventoryService->getByAdditive($additiveId,$this->request->get('license_id'))
        );
    }

    /**
     * @param $additiveId
     *
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function store($additiveId): JsonResponse
    {
        $rules = [
            'supplier_id' => [
                'required',
                new ModelsExist(Supplier::class, 'id')
            ],
            'total_quantity' => ['required', 'numeric', 'min:0', 'max:9999', 'not_in:0'],
            'unit_price' => ['sometimes', 'numeric', 'min:0','max:9999', 'not_in:0'],
            'license_id' => ['required', 'uuid'],
            'user_id' => ['required', 'uuid'],
            'order_date' => ['required', 'date_format:Y-m-d']
        ];
        $additive = $this->additiveService->get($additiveId);
        return response()->json(
            $this->additiveService->storeInventory($additive, $this->validate($this->request, $rules)),
            Response::HTTP_CREATED
        );
    }
}
