<?php

namespace App\Services;

use App\Models\HarvestItem;
use Illuminate\Database\Eloquent\{Builder, Collection};
use Illuminate\Support\Enumerable;

/**
 * Class InventoryHarvestService
 * @package App\Services
 *
 * @property HarvestItem|Builder $model
 *
 * @method HarvestItem|Collection get($id)
 * @method HarvestItem|Builder getModel()
 */
class InventoryHarvestService extends BaseService
{

    public function __construct(HarvestItem $inventoryHarvest)
    {
        parent::__construct($inventoryHarvest);
    }

    /**
     * @param array $inventoryHarvestIds
     * @param array $relations
     * @return Enumerable
     */
    public function getHarvestEnumerable(array $inventoryHarvestIds, $relations = []): Enumerable
    {
        $query = $this->model->whereIn('id', $inventoryHarvestIds)
            ->withoutGlobalScopes([$this->model::DEFAULT_ORDER_SCOPE]);
        if ($relations) {
            $query->with($relations);
        }

        return $query->cursor();
    }

}
