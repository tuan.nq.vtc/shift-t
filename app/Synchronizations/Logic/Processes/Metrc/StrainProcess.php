<?php

namespace App\Synchronizations\Logic\Processes\Metrc;

use App\Exceptions\GeneralException;
use App\Models\{Strain, TraceableModel};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class Propagation
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 * @property Strain|TraceableModel $traceableModel
 */
class StrainProcess extends BaseProcess
{
    /**
     * @throws Throwable
     */
    public function create()
    {
        try {
            DB::beginTransaction();
            $this->syncStrain();
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        } catch (Throwable $throwable) {
            throw $throwable;
        }
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    private function syncStrain(): self
    {
        $fetchActiveStrainsResponse = $this->client->getRequest()->get(
            self::STRAIN_ENDPOINT . 'active',
            ['licenseNumber' => $this->traceableModel->license->code]
        );

        if ($fetchActiveStrainsResponse->failed()) {
            throw new GeneralException('Unable to fetch active strains from METRC regulator');
        }

        $activeStrains = $fetchActiveStrainsResponse->json();

        foreach ($activeStrains as $strain) {
            if ($strain['Name'] === $this->traceableModel->name) {
                $this->traceableModel->update(
                    ['sync_status' => TraceableModel::SYNC_STATUS_SYNCED, 'sync_code' => $strain['Id']],
                );
                break;
            }
        }

        return $this;
    }

    /**
     * @param $item
     * @throws Exception
     */
    protected function processListItem($item): void
    {
        if (!($globalId = Arr::get($item, 'Id'))) {
            throw new Exception('ID Not Found');
        }
        $this->traceableModel->newInstance()->firstOrCreate(
            ['sync_code' => $globalId, 'license_id' => $this->traceableModel->license->id,],
            [
                'name' => Arr::get($item, 'Name'),
                'info' => [
                    'thc' => Arr::get($item, 'ThcLevel'),
                    'cbd' => Arr::get($item, 'CbdLevel'),
                    'indica' => Arr::get($item, 'IndicaPercentage'),
                    'sativa' => Arr::get($item, 'SativaPercentage'),
                    'testing_status' => Arr::get($item, 'TestingStatus', 'None'),
                ],
                'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                'synced_at' => Carbon::now(),
            ]
        );
    }
}
