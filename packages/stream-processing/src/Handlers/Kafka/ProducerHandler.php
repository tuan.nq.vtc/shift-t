<?php

namespace Bamboo\StreamProcessing\Handlers\Kafka;

use Bamboo\StreamProcessing\Contracts\{ProducerHandlerInterface, StreamEventInterface};
use Enqueue\RdKafka\RdKafkaConnectionFactory;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Throwable;

class ProducerHandler implements ProducerHandlerInterface
{
    /**
     * @var \Enqueue\RdKafka\RdKafkaContext|\Interop\Queue\Context
     */
    private $context;
    private LoggerInterface $logger;

    /**
     * ConsumerHandler constructor.
     *
     * @param RdKafkaConnectionFactory $connection
     * @throws Throwable
     */
    public function __construct(RdKafkaConnectionFactory $connection)
    {
        $this->logger = Log::channel('stream');
        try {
            $this->context = $connection->createContext();
        } catch (Throwable $throwable) {
            $this->logger->error('Initialize KAFKA consumer handler failed: ' . $throwable->getMessage());
            throw $throwable;
        }
    }

    public function sendEvent(string $topicName, StreamEventInterface $streamEvent): void
    {
        $destination = $this->context->createTopic($topicName);
        $message = $this->context->createMessage(
            serialize($streamEvent->getPayload()),
            $streamEvent->getMetadata(),
            array_merge(
                [
                    'message_id' => $streamEvent->getId(),
                    'sender' => config('stream.channels.kafka.connection.global')['group.id']
                ],
                $streamEvent->getHeaders()
            )
        );
        try {
            $this->context->createProducer()->send($destination, $message);
            $this->logger->info('Message sent', [
                'id' => $message->getMessageId(),
                'header' => $message->getHeaders(),
                'properties' => $message->getProperties(),
                'body' => $message->getBody(),
            ]);
        } catch (Throwable $throwable) {
            $this->logger->error('Error "' . $throwable->getMessage() . '" occurs while sending message', [
                'id' => $message->getMessageId(),
                'header' => $message->getHeaders(),
                'properties' => $message->getProperties(),
                'body' => $message->getBody(),
            ]);
//            $this->handleFailedMessage(FailedStream::HANDLE_CONSUME, $destination, $message, $throwable);
            throw $throwable;
        }
    }

    public function __destruct()
    {
        $this->context->close();
    }
}
