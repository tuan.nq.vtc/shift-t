<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\PrintLabelTemplate;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    PrintLabelTemplate::class,
    function (Faker $faker) {
        return [
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                PrintLabelTemplate::class,
                null,
                $data['account_holder_id']
            ),
            'name' => $faker->words(2, true) . '_' . $faker->numberBetween(1),
            'background' => null,
            'box' => '{"width":1,"height":1,"top":0,"bottom":0,"left":0,"right":0}',
            'status' => rand(0, 1),
        ];
    }
);
