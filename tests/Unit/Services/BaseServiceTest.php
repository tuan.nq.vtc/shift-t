<?php

namespace Tests\Unit\Services;

use App\Http\Parameters\Criteria;
use App\Models\Room;
use App\Models\RoomType;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

class BaseServiceTest extends TestCase
{
    use WithFaker;
    use Traceable;

    protected static string $modelName = Room::class;

    protected static array $expectedRelations = [
        'roomType' => RoomType::class,
    ];

    /**
     * @test
     */
    public function it_can_get_list()
    {
        $this->withoutTraceableEvents();

        $expectedCount = rand(1, 50);
        factory(static::$modelName, $expectedCount)->create();
        $expectedNumberPerPage = rand(1, $expectedCount);
        $expectedPage = rand(1, $expectedCount / $expectedNumberPerPage);
        // execute
        $result = $this->getMockService()->list((new Criteria())->setPagination($expectedPage, $expectedNumberPerPage));
        $items = $result->items();

        $this->assertEquals($expectedCount, $result->total());
        $this->assertEquals($expectedPage, $result->currentPage());
        $this->assertCount($expectedNumberPerPage, $items);

        // relations
        $actualItem = array_shift($items);
        foreach (static::$expectedRelations as $relation => $expectedClassName) {
            $this->assertInstanceOf($expectedClassName, $actualItem->{$relation});
        }
    }

    /**
     * @test
     */
    public function it_can_get_list_sorted_by_id_asc()
    {
        $this->withoutTraceableEvents();
        $collection = factory(static::$modelName, rand(1, 50))->create();

        $criteria = (new Criteria())->setSorts(['created_at' => 'asc']);
        // execute
        $items = $this->getMockService()->list($criteria)->items();

        $this->assertEquals($collection->first()->id, array_shift($items)->id);
    }

    /**
     * @test
     */
    public function it_throws_exception_if_getting_a_not_exist_item()
    {
        $this->expectException(ModelNotFoundException::class);
        // execute
        $this->getMockService()->get($this->faker->randomNumber(3));
    }

    /**
     * @test
     */
    public function it_can_get_item()
    {
        $this->withoutTraceableEvents();

        $expectedId = factory(static::$modelName)->create()->id;

        // execute
        $item = $this->getMockService()->get($expectedId);

        $this->assertEquals($expectedId, $item->id);
        // relations
        foreach (static::$expectedRelations as $relation => $expectedClassName) {
            $this->assertInstanceOf($expectedClassName, $item->{$relation});
        }
    }

    /**
     * @test
     */
    public function it_can_create_item()
    {
        $this->withoutTraceableEvents();

        $expectedData = factory(static::$modelName)->raw();

        // execute
        $result = $this->getMockService()->create($expectedData);

        $this->assertEquals($expectedData, $result->only(array_keys($expectedData)));
    }

    /**
     * @test
     */
    public function it_can_update_item()
    {
        $this->withoutTraceableEvents();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->raw();

        // execute
        $result = $this->getMockService()->update($id, $expectedData);

        $this->assertEquals($expectedData, $result->only(array_keys($expectedData)));
    }

    /**
     * @test
     */
    public function it_throws_exception_if_updating_a_not_exist_item()
    {
        $updateData = factory(static::$modelName)->raw();
        $this->expectException(ModelNotFoundException::class);

        // execute
        $this->getMockService()->update($this->faker->randomNumber(3), $updateData);
    }

    /**
     * @return BaseService|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getMockService()
    {
        return $this->getMockForAbstractClass(
            BaseService::class,
            ['model' => app(static::$modelName)]
        );
    }
}
