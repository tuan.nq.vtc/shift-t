<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\License;
use App\Models\State;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    License::class,
    function (Faker $faker) {
        return [
            'organization_id' => $faker->uuid,
            'account_holder_id' => $faker->uuid,
            'name' => $faker->words(2, true),
            'type' => $faker->word,
            'state_code' => State::inRandomOrder()->select('code')->first()->code,
            'code' => $faker->word,
            'status' => rand(0, 1),
            'api_configuration' =>
                fn($data) => State::where('code', $data['state_code'])->first()->regulator === State::REGULATOR_METRC ? [
                    'vendor_key' => $faker->uuid,
                    'user_key' => $faker->uuid,
                ] : [
                    'api_key' => $faker->uuid,
                ],
        ];
    }
);
