<?php

namespace App\Services;

use App\Http\Parameters\Criteria;
use App\Models\PrintLabelTemplate;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class PrintLabelTemplateService
 * @package App\Services
 *
 * @method PrintLabelTemplate|Collection get($id)
 * @method PrintLabelTemplate|Builder getModel()
 */
class PrintLabelTemplateService extends BaseService
{
    public function __construct(PrintLabelTemplate $printLabelTemplate)
    {
        parent::__construct($printLabelTemplate);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAvailableItems(Criteria $criteria): Collection
    {
        $query = $this->newQuery(false)
            ->scopes($this->loadScopes($criteria->getFilters()))
            ->where('status', PrintLabelTemplate::STATUS_ENABLED);

        return $this->applyOrderBy($query, $criteria->getSorts())->get();
    }
}
