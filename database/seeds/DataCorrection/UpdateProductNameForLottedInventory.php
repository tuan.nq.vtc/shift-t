<?php

use App\Models\{EndProduct, Inventory, SourceProduct};
use App\Facades\Sales;
use Illuminate\Database\Seeder;

class UpdateProductNameForLottedInventory extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {
        Inventory::query()
            ->where('is_lotted', Inventory::IS_LOTTED)
            ->whereNull('product_name')
            ->each(function ($inventory) {
                switch ($inventory->product_type) {
                    case SourceProduct::class:
                        $inventory->update([
                            'product_name' => optional(SourceProduct::find($inventory->product_id))->name
                        ]);
                        break;
                    case EndProduct::class:
                        $productDetail = Sales::setState($inventory->license_id)->fetchProduct($inventory->product_id);
                        $endProduct = new EndProduct($productDetail['state_value'] ?? $productDetail['root_value']);
                        $inventory->update([
                            'product_name' => optional($endProduct)->name
                        ]);
                        break;
                }
            });
    }
}
