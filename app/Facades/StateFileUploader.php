<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class FileUploader
 *
 * @package App\Facades
 *
 * @method static string upload(string $state, array $dependencyGroups)
 *
 * @see \App\Services\StateFileUploaderService
 */
class StateFileUploader extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'state_file_uploader';
    }
}
