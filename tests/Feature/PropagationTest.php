<?php

namespace Tests\Feature;

use App\Models\GrowCycle;
use App\Models\Propagation;
use Illuminate\Http\Response;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

/**
 * Class PropagationTest
 * @package Tests\Feature
 */
class PropagationTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $modelName = Propagation::class;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

    private function getEndpoint($path = '')
    {
        return "/propagations{$path}";
    }

    /** @test */
    public function guest_cannot_call_endpoints()
    {
        $this->actingAsGuest();
        $this->get($this->getEndpoint("/list"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint())->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/create"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/move-to-vegetation"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/move-to-vegetation/randomUUID"))->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_call_endpoints_without_license()
    {
        $this->actingWithoutLicense();
        $this->get($this->getEndpoint("/list"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint())->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/create"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/move-to-vegetation"))->seeStatusCode(Response::HTTP_FORBIDDEN);
        $this->post($this->getEndpoint("/move-to-vegetation/randomUUID"))->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_can_create_item()
    {
        $creatingFields = [
            'name',
            'strain_id',
            'source_type',
            'room_id',
            'subroom_id',
            'cut_by_id',
            'plugged_by_id',
            'quantity',
            'date'
        ];
        $expectedData = factory(static::$modelName)->make(['license_id' => $this->licenseId])->only($creatingFields);

        $this->post($this->getEndpoint(), $expectedData)
            ->seeStatusCode(Response::HTTP_CREATED)
            ->seeJsonStructure(['id', 'batch', 'strain', 'source_type', 'room', 'subroom', 'cut_by', 'plugged_by'])
            ->seeJson($expectedData);
    }

    /** @test */
    public function user_can_update_item()
    {
        $updatableFields = [
            'name',
            'strain_id',
            'source_type',
            'room_id',
            'subroom_id',
            'cut_by_id',
            'plugged_by_id',
            'quantity',
            'date'
        ];
        $id = factory(static::$modelName)->create(['license_id' => $this->licenseId])->id;
        $expectedData = factory(static::$modelName)->make(['license_id' => $this->licenseId])->only($updatableFields);
        $this->put($this->getEndpoint("/{$id}"), $expectedData)
            ->seeStatusCode(Response::HTTP_ACCEPTED)
            ->seeJsonStructure(['id', 'batch', 'strain', 'source_type', 'room', 'subroom', 'cut_by', 'plugged_by'])
            ->seeJson($expectedData);
    }

    /** @test */
    public function user_can_move_propagation_to_vegetation()
    {
        $transplant = factory(static::$modelName, rand(1, 3))->create(['license_id' => $this->licenseId])
            ->reduce(function($acc, $propagation) {
                $acc[] = [
                    'propagation_id' => $propagation->id,
                    'quantity' => $propagation->quantity,
                ];
                return $acc;
            }, []);
        $growCycle = factory(GrowCycle::class)->make(['license_id' => $this->licenseId]);
        $data = [
            'transplant' => $transplant,
            'planted_at' => $this->faker->date(),
            'est_harvested_at' => $this->faker->dateTimeBetween('+0 days', '+1 month'),
            'name' => $growCycle->name,
        ];
        $this->post($this->getEndpoint("/move-to-vegetation"), $data)
            ->seeStatusCode(Response::HTTP_CREATED);
//            ->seeJsonStructure(['id', 'batch', 'strain', 'source_type', 'room', 'subroom', 'cut_by', 'plugged_by'])
//            ->seeJson($expectedData);
    }

}
