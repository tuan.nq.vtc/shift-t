<?php

namespace App\Models;

use App\Models\Traits\{
    Scope\IdsFilterable,
};

use Illuminate\Database\Eloquent\Relations\{BelongsTo, MorphTo};

class InventoryHistory extends BaseModel
{
    use IdsFilterable;

    const ACTION_HOLD = 'hold';
    const ACTION_ADJUST = 'adjust';

    const REASON_RECONCILIATION = 'reconciliation';
    const REASON_THEFT = 'Theft';
    const REASON_SEIZURE = 'Seizure';
    const REASON_MEMBER_LEFT_THE_COOPERATIVE = 'Member left the Cooperative';
    const REASON_INTERNAL_OF_QA_SAMPLE = 'Internal of QA Sample';
    const REASON_BUDTENDER_SAMPLE = 'Budtender Sample';
    const REASON_VENDOR_SAMPLE = 'Vendor Sample';

    const REASONS = [
        self::REASON_RECONCILIATION,
        self::REASON_THEFT,
        self::REASON_SEIZURE,
        self::REASON_MEMBER_LEFT_THE_COOPERATIVE,
        self::REASON_INTERNAL_OF_QA_SAMPLE,
        self::REASON_BUDTENDER_SAMPLE,
        self::REASON_VENDOR_SAMPLE,
    ];

    protected $fillable = [
        'qty_current',
        'inventory_id',
        'qty_variable',
        'action',
        'source_type',
        'is_decrease',
        'reason',
        'comment'
    ];

    protected $casts = [
        'is_decrease' => 'boolean',
    ];

    public function inventory(): BelongsTo
    {
        return $this->belongsTo(Inventory::class);
    }

    public function source(): MorphTo
    {
        return $this->morphTo();
    }
}
