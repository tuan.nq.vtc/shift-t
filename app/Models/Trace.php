<?php

namespace App\Models;

use App\Models\Traits\WithoutEvents;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Illuminate\Database\Eloquent\{Relations\BelongsTo, Relations\BelongsToMany, Relations\HasMany, Relations\MorphTo};
use Illuminate\Support\Str;

/**
 * Class Trace
 *
 * @property string action
 * @property string resource_type
 * @property Integer status
 * @property License license
 * @property TraceableModelInterface resource
 * @property array response_data
 * @property int method
 * @package App\Models
 */
class Trace extends BaseModel
{
    use WithoutEvents;

    public const METHOD_PUSH = 0;
    public const METHOD_PULL = 1;

    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';

    public const STATUS_PENDING = 0;
    public const STATUS_SEND_PROCESSING = 1;
    public const STATUS_SEND_COMPLETED = 2;
    public const STATUS_PROCESS_PROCESSING = 3;
    public const STATUS_COMPLETED = 5;

    public const STATUS_SEND_FAILED = 40;
    public const STATUS_SEND_CONNECTION_ERROR = 41;
    public const STATUS_SEND_SERVER_DOWN = 42;
    public const STATUS_PROCESS_FAILED = 43;

    protected $fillable = [
        'user_id',
        'license_id',
        'method',
        'action',
        'resource_id',
        'resource_type',
        'resource_data',
        'resource_changes',
        'resource_conditions',
        'response_data',
        'file_reference',
        'status',
    ];

    protected $casts = [
        'resource_data' => 'array',
        'resource_changes' => 'array',
        'resource_conditions' => 'array',
        'response_data' => 'array',
    ];

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    /**
     * self reference to retrieve the dependencies of this item
     *
     * @return BelongsToMany
     */
    public function dependencies(): BelongsToMany
    {
        return $this->belongsToMany(Trace::class, 'trace_dependency', 'trace_id', 'dependency_id');
    }

    /**
     * self reference to retrieve the children of this item
     *
     * @return BelongsToMany
     */
    public function children(): BelongsToMany
    {
        return $this->belongsToMany(Trace::class, 'trace_dependency', 'dependency_id', 'trace_id');
    }

    public function resource(): MorphTo
    {
        return $this->morphTo('resource');
    }

    public function logs(): HasMany
    {
        return $this->hasMany(TraceLog::class);
    }

    public function getResourceActionLabel(): ?string
    {
        if (!$this->getActionLabel()) {
            return null;
        }

        $elements = [
            Str::lower($this->license->state_code),
            Str::lower($this->getResourceClass()),
            Str::lower($this->getActionLabel()),
        ];

        return implode('_', $elements);
    }

    public function getActionLabel(): ?string
    {
        switch ($this->action) {
            case self::ACTION_CREATE:
                return 'Creation';

            case self::ACTION_DELETE:
                return 'Deletion';

            case self::ACTION_UPDATE:
                return 'Update';

            default:
                return null;
        }
    }

    /**
     * @return TraceableModelInterface|null
     */
    public function getResource(): ?TraceableModelInterface
    {
        if ($this->resource) {
            return $this->resource;
        }

        if ($this->resource_type) {
            /** @var TraceableModel $newInstance */
            $newInstance = (new $this->resource_type());
            $newInstance->license()->associate($this->license);

            return $newInstance;
        }

        return null;
    }

    public function getResourceClass(): string
    {
        return get_classname($this->resource_type, true);
    }

    public function unserializeResource(): TraceableModelInterface
    {
        $data = $this->resource_data;
        foreach ($data as $key => $value) {
            if ($this->resource->hasAppended($key)) {
                unset($data[$key]);
            }
        }
        $this->resource->forceFill($data);

        $changes = $this->resource_changes;
        foreach ($changes as $key => $value) {
            if ($this->resource->hasAppended($key)) {
                unset($changes[$key]);
            }
        }
        $this->resource->changes = $changes;

        return $this->resource;
    }

    public function getLatestLogAttribute(): ?TraceLog
    {
        return $this->logs()->orderBy('sent_at', 'desc')->first();
    }

    /**
     * @return TraceableModelInterface
     */
    public function getNewResource(): ?TraceableModelInterface
    {
        if ($this->resource_type) {
            /** @var TraceableModel $newInstance */
            $newInstance = (new $this->resource_type());
            $newInstance->license()->associate($this->license);

            return $newInstance;
        }

        return null;
    }
}
