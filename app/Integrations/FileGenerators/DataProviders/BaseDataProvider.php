<?php

namespace App\Integrations\FileGenerators\DataProviders;

use App\Integrations\FileGenerators\Contracts\DataProviderInterface;
use App\Models\License;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

abstract class BaseDataProvider implements DataProviderInterface
{
    private const MAX_RECORDS_PER_FILE_DEFAULT = 5000;

    protected int $maxRecordsPerFile;
    protected License $license;
    protected string $resourceClassName;
    protected Collection $currentRecords;

    public function __construct(License $license, string $resourceClassName)
    {
        $this->maxRecordsPerFile = self::MAX_RECORDS_PER_FILE_DEFAULT;
        $this->license = $license;
        $this->resourceClassName = $resourceClassName;
    }

    protected function uniqueCallback($item)
    {
        return $item->id;
    }

    public function getData(): array
    {
        $this->currentRecords = $this->getBuilder()->limit($this->maxRecordsPerFile)->get();

        $data = [];

        foreach (
            $this->currentRecords->unique(
                function ($item) {
                    return $this->uniqueCallback($item);
                }
            ) as $record
        ) {
            $data[] = $record;
        }

        return $data;
    }

    public function setMaxRecordsPerFile(int $maxRecordsPerFile): void
    {
        $this->maxRecordsPerFile = $maxRecordsPerFile;
    }

    abstract protected function getBuilder(): Builder;
}
