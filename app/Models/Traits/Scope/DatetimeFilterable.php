<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

trait DatetimeFilterable
{
    /**
     * @param Builder $query
     * @param $firstValue
     * @param null $secondValue
     * @return Builder
     */
    public function scopeCreatedAt(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'created_at');
    }

    /**
     * @param Builder $query
     * @param $fromDate
     * @param $toDate
     * @param $field
     * @return Builder
     */
    private function buildQueryDateRanger(Builder $query, $fromDate, $toDate, $field)
    {
        $from = Carbon::createFromDate($fromDate);
        $to = Carbon::createFromDate($toDate);

        if ($fromDate) {
            $query->whereDate($this->getTable().'.'.$field, '>=', $from);
        }
        if ($toDate) {
            $query->whereDate($this->getTable().'.'.$field, '<=', $to);
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $fromDatetime
     * @param $toDatetime
     * @param $field
     * @return Builder
     */
    private function buildQueryDatetimeRanger(Builder $query, $fromDatetime, $toDatetime, $field)
    {
        $from = Carbon::createFromDate($fromDatetime);
        $to = Carbon::createFromDate($toDatetime);

        if ($fromDatetime) {
            $query->where($this->getTable().'.'.$field, '>=', $from);
        }
        if ($toDatetime) {
            $query->where($this->getTable().'.'.$field, '<=', $to);
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param $firstValue
     * @param null $secondValue
     * @return Builder|mixed
     */
    public function scopeUpdatedAt(Builder $query, $firstValue, $secondValue = null)
    {
        return $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'updated_at');
    }
}
