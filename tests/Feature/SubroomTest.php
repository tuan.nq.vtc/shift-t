<?php

namespace Tests\Feature;

use App\Models\Subroom;
use Illuminate\Http\Response;
use Tests\Concerns\ActingWithAttachedHeaders;
use Tests\Concerns\Traceable;
use Tests\Concerns\WithFaker;
use Tests\TestCase;

/**
 * Class SubroomTest
 * @package Tests\Feature
 */
class SubroomTest extends TestCase
{
    use ActingWithAttachedHeaders;
    use WithFaker;
    use Traceable;

    protected static string $endpoint = '/subrooms';

    protected static string $modelName = Subroom::class;

    protected static array $requiredFields = ['name', 'room_id', 'type', 'dimension', 'color',];

    protected static array $expectedRelationFields = [];

    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutTraceableEvents();
        $this->actingAsUser();
        $this->actingWithLicense();
    }

    /** @test */
    public function guest_cannot_view_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $this->get(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function guest_cannot_update_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->put(static::$endpoint . '/' . $id, $expectedData)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function guest_cannot_delete_item()
    {
        $this->actingAsGuest();

        $id = factory(static::$modelName)->create()->id;
        $this->delete(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_view_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $this->get(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_update_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->put(static::$endpoint . '/' . $id, $expectedData)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_cannot_delete_item_without_license()
    {
        $this->actingWithoutLicense();

        $id = factory(static::$modelName)->create()->id;
        $this->delete(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function user_can_view_item()
    {
        $model = factory(static::$modelName)->create();
        $this->get(static::$endpoint . '/' . $model->id)
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonStructure(static::$expectedRelationFields)
            ->seeJson($model->only(array_merge(['id',], static::$requiredFields)));
    }

    /** @test */
    public function user_can_update_item()
    {
        $id = factory(static::$modelName)->create()->id;
        $expectedData = factory(static::$modelName)->make()->only(static::$requiredFields);
        $this->put(static::$endpoint . '/' . $id, $expectedData)
            ->seeStatusCode(Response::HTTP_ACCEPTED)
            ->seeJsonStructure(static::$expectedRelationFields)
            ->seeJson($expectedData);
    }

    /** @test */
    public function user_can_delete_item()
    {
        $id = factory(static::$modelName)->create()->id;
        $this->delete(static::$endpoint . '/' . $id)->seeStatusCode(Response::HTTP_NO_CONTENT);
    }
}
