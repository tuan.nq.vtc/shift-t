<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Facades\Sales;
use App\Http\Parameters\Criteria;
use App\Models\{Category, Conversion, EndProduct, Inventory, SourceCategory, SourceProduct};
use App\Services\Traits\EndProductHandler;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\{Arr, Collection, Facades\DB};
use Throwable;

/**
 * Class ConversionService
 * @package App\Services
 */
class ConversionService extends BaseService
{
    use EndProductHandler;

    protected $license;

    protected array $relations = [
        'outputRoom:id,name',
        'wasteRoom:id,name',
        'inputLots',
        'inputLots.type.stateCategory:id,name',
        'inputLots.strain:id,name',
        'inputLotProducts:id,name',
        'outputStateCategory:id,name',
        'outputEndProducts',
        'outputSourceProduct',
    ];

    public function __construct(Conversion $conversion)
    {
        parent::__construct($conversion);
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function getAvailableItems(Criteria $criteria): Collection
    {
        $query = $this->newQuery(false)->scopes($this->loadScopes($criteria->getFilters()));

        return $this->applyOrderBy($query, $criteria->getSorts())->get();
    }

    public function list(Criteria $criteria): LengthAwarePaginator
    {
        $this->attachRelations();

        $paginator = parent::list($criteria);
        $paginator->getCollection()->transform(
            function ($conversion) {
                return $this->listTransform($conversion);
            }
        );

        return $paginator;
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function search(Criteria $criteria): Collection
    {
        $query = $this->newQuery(false)->scopes($this->loadScopes($criteria->getFilters()));

        $conversions = $this->applyOrderBy($query, $criteria->getSorts())
            ->with($this->relations)
            ->withCount($this->counts)
            ->get();

        return $conversions->transform(fn($conversion) => $this->listTransform($conversion));
    }

    /**
     * @param Conversion $conversion
     * @return Conversion
     */
    private function listTransform(Conversion $conversion)
    {
        $stateCategories = $strains = [];
        foreach ($conversion->inputLots as $inputLot) {
            $strains = $this->pushValue($strains, $inputLot->strain->id, $inputLot->strain->name);
            $stateCategories = $this->pushValue(
                $stateCategories,
                $inputLot->type->stateCategory->id,
                $inputLot->type->stateCategory->name
            );
        }
        $conversion->input_state_categories = $stateCategories;
        $conversion->strains = $strains;
        $conversion->output_product = $this->getOutputProduct($conversion);

        return $conversion;
    }

    public function get(string $id, $withRelation = true)
    {
        $this->attachRelations();
        $conversion = parent::get($id, $withRelation);
        return $this->listTransform($conversion);
    }

    public function create(array $data): Model
    {
        if (empty($data['input_lots'])) {
            throw new GeneralException('exceptions.conversion.invalid_input_lots');
        }

        $this->validateOutputProduct($data['output_product_id'], $data['output_type']);

        try {
            DB::beginTransaction();

            $data['output_product_type'] = $this->getOutputProductModelClass($data['output_type']);
            /** @var Conversion $conversion */
            $conversion = parent::create($data);
            $this->updateInventoryAllocated($conversion, $data['input_lots']);

            // Create lotted inventory
            if ($conversion->isStatus(Conversion::STATUS_CLOSED)) {
                $this->createLottedInventory($conversion);
                $this->releaseInventoryQty($conversion);
            }

            DB::commit();
            return $conversion->load($this->relations);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw  $throwable;
        }
    }

    public function update($id, array $data): Model
    {
        try {
            DB::beginTransaction();

            // Check output type
            if (!empty($data['output_type'])) {
                $data['output_product_type'] = $this->getOutputProductModelClass($data['output_type']);
            }

            /** @var Conversion $conversion */
            $conversion = parent::update($id, $data);

            $this->validateOutputProduct($conversion->output_product_id, $conversion->output_type);

            if ($conversion->isStatus(Conversion::STATUS_CLOSED) && !$conversion->inputLots()->count()) {
                throw new GeneralException(__('exceptions.conversion.finalize_failed'));
            }

            if (!empty($data['input_lots'])) {
                $this->updateInventoryAllocated($conversion, $data['input_lots']);
            }

            // Update lotted inventory
            if ($conversion->wasChanged('status') && $conversion->isStatus(Conversion::STATUS_CLOSED)) {
                $this->createLottedInventory($conversion);
                $this->releaseInventoryQty($conversion);
            }

            DB::commit();
            return $conversion->load($this->relations);
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function listInputLots(string $id): Collection
    {
        $conversion = $this->get($id);
        return $conversion->inputLots;
    }

    public function addLots(string $conversionId, array $data = [])
    {
        try {
            DB::beginTransaction();

            /** @var Conversion $conversion */
            $conversion = $this->model->findOrFail($conversionId);
            if ($conversion->isStatus(Conversion::STATUS_CLOSED)) {
                throw new GeneralException(__('exceptions.conversion.finalize_failed'));
            }

            $mappedInputLots = $conversion->inputLots->pluck('pivot.quantity', 'id')->toArray();
            foreach ($data as $item) {
                if (empty($mappedInputLots[$item['inventory_id']])) {
                    $mappedInputLots[$item['inventory_id']] = (float) $item['quantity'];
                } else {
                    $mappedInputLots[$item['inventory_id']] += (float) $item['quantity'];
                }
            }
            $addLotsData = collect($mappedInputLots)->mapWithKeys(
                fn ($quantity, $id) => [$id => ['inventory_id' => $id, 'quantity' => $quantity]]
            )->toArray();
            $this->updateInventoryAllocated($conversion, $addLotsData);

            DB::commit();
            return $conversion;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    /**
     * @param Conversion $conversion
     * @param array $inputLotsData
     * @throws GeneralException
     */
    public function updateInventoryAllocated(Conversion $conversion, array $inputLotsData = [])
    {
        $inputLotIds = Arr::pluck($inputLotsData, 'inventory_id');
        $this->validateInputLots($inputLotIds);
        $lottedInventoryIds = $conversion->inputLots->pluck('id')->merge($inputLotIds)->values();
        /** @var Collection $lottedInventories */
        $conversionTable = $conversion->getTable();
        $lottedInventories = Inventory::isLotted()->with([
            'conversions' => fn($query) => $query->where("$conversionTable.id", $conversion->id)->orWhere("$conversionTable.status", Conversion::STATUS_OPEN)
        ])->findMany($lottedInventoryIds);

        // validate quantity
        foreach ($inputLotsData as $key => $inputLotData) {
            $lottedInventory = $lottedInventories->firstWhere('id', $inputLotData['inventory_id']);
            $mappedInventory = $conversion->inputLots->firstWhere('id', $lottedInventory->id);
            $allocatedQuantity = $lottedInventory->conversions->where('id', '!=', $conversion->id)->sum('pivot.quantity');
            if ($inputLotData['quantity'] > $lottedInventory->originalQtyAvailable - $allocatedQuantity) {
                throw new GeneralException(__('exceptions.conversion.invalid_input_lot_quantity'));
            }

            // set product_id for first
            $inputLotsData[$key]['product_id'] = $mappedInventory ? $mappedInventory->pivot->product_id : $lottedInventory->product_id;
            $inputLotsData[$key]['quantity'] = round($inputLotData['quantity'], 4);

            // update qty allocated
            $lottedInventory->update([
                'qty_allocated' => $allocatedQuantity + $inputLotData['quantity']
            ]);
        }

        // revert qty available when delete inventory
        $lottedInventories->whereNotIn('id', $inputLotIds)->each(function ($lottedInventory) {
            $lottedInventory->update([
                'qty_allocated' => $lottedInventory->qty_allocated - $lottedInventory->conversions->sum('pivot.quantity')
            ]);
        });

        $conversion->inputLots()->sync(
            collect($inputLotsData)->keyBy('inventory_id')->toArray()
        );
        $conversion->load('inputLots');
    }

    public function createLottedInventory(Conversion $conversion)
    {
        $lottedInventoryData = [
            'license_id' => $conversion->license_id,
            'type_id' => $conversion->inputLots->first()->type_id,
            'room_id' => $conversion->output_room_id,
            'strain_id' => $conversion->inputLots->first()->strain_id,
            'product_type' => $conversion->output_product_type,
            'product_id' => $conversion->output_product_id,
            'is_lotted' => true,
            'sell_status' => true,
            'qty_on_hand' => $conversion->output_quantity,
            'state_category_id' => $conversion->output_state_category_id
        ];

        $inventory = Inventory::query()->create($lottedInventoryData);
        if ($inventory->license->is_test) {
            RegulatorMockDataService::generate($inventory, Inventory::SYNC_ACTION_CREATE);
        }
    }

    private function releaseInventoryQty(Conversion $conversion)
    {
        $conversion->inputLots()
            ->each(function ($inventory) {
                $inventory->update([
                    'qty_on_hand' => $inventory->qty_on_hand - $inventory->pivot->quantity,
                    'qty_allocated' => $inventory->qty_allocated - $inventory->pivot->quantity,
                ]);
            });
    }

    public function getOutputProductModelClass($type = null)
    {
        switch ($type) {
            case Conversion::TYPE_SOURCE:
                return SourceProduct::class;
            case Conversion::TYPE_END:
                return EndProduct::class;
            default:
                return null;
        }
    }

    public function getOutputCategoryModelClass($type = null)
    {
        switch ($type) {
            case Conversion::TYPE_SOURCE:
                return SourceCategory::class;
            case Conversion::TYPE_END:
                return Category::class;
            default:
                return null;
        }
    }

    /**
     * @param array $arr
     * @param $key
     * @param $value
     * @return array
     */
    private function pushValue(array $arr, $key, $value)
    {
        if (!isset($arr[$key]) && !empty($value)) {
            $arr[$key] = $value;
        }

        return $arr;
    }

    /**
     *
     */
    public function attachRelations()
    {
        $license = $this->getLicense();
        $this->relations['outputEndProducts'] = fn($subQuery) => $subQuery->whereNull('state_code')
            ->orWhere('state_code', $license->state_code);
    }

    /**
     * @return Model|mixed
     */
    public function getLicense()
    {
        if (!$this->license || $this->licenseId !== $this->license->getKey()) {
            $this->license = app(LicenseService::class)->get($this->licenseId);
        }

        return $this->license;
    }

    /**
     * @param Conversion $conversion
     * @return |null
     */
    private function getOutputProduct(Conversion $conversion)
    {
        switch ($conversion->output_type) {
            case Conversion::TYPE_SOURCE:
                return $conversion->outputSourceProduct->only(['id', 'name']);
            case Conversion::TYPE_END:
                $productDetail = Sales::getProductsByIds([$conversion->output_product_id])->first();
                if (!$productDetail) {
                    return null;
                }
                return [
                    'id' => $productDetail->product_id,
                    'name' => $productDetail->name
                ];
            default:
                return null;
        }
    }

    /**
     * @param string $id
     * @param string $type
     * @throws GeneralException
     */
    private function validateOutputProduct(string $id, string $type): void
    {
        switch ($type) {
            case Conversion::TYPE_SOURCE:
                if (!SourceProduct::where('status', SourceProduct::STATUS_ENABLED)->find($id)) {
                    throw new GeneralException(__('exceptions.conversion.output_product_is_not_available'));
                };
                break;
            case Conversion::TYPE_END:
                $this->getAvailableProduct($id);
                break;
            default:
                throw new GeneralException(__('exceptions.conversion.invalid_output_type'));
        }
    }

    private function validateInputLots(array $inventoryIds): void
    {
        if (
            Inventory::query()
                ->whereIn('id', $inventoryIds)
                ->where('product_type', EndProduct::class)
                ->count()
        ) {
            throw new GeneralException(__('exceptions.conversion.cannot_use_lot_mapped_end_product'));
        }
    }
}
