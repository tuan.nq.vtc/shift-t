<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\DatetimeFilterable;
use App\Models\Traits\Scope\LicenseFilterable;
use App\Models\Traits\WithoutTraceableModelEvents;
use App\Synchronizations\Contracts\TraceableModelInterface;
use Illuminate\Database\Eloquent\{Builder,
    Collection,
    Relations\BelongsTo,
    Relations\BelongsToMany,
    Relations\HasMany,
    SoftDeletes};
use Illuminate\Support\Carbon;

/**
 * @property ManifestItem[]|Collection items
 */
class Manifest extends BaseModel implements TraceableModelInterface
{
    use SoftDeletes;
    use LicenseFilterable;
    use WithoutTraceableModelEvents;
    use LicenseFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    public const METHOD_INBOUND = 'inbound';
    public const METHOD_OUTBOUND = 'outbound';

    public const TYPE_TRANSFER = 'transfer';
    public const TYPE_ORDER = 'order';
    public const TYPE_QA_SAMPLE = 'qa_sample';

    public const STATUS_OPEN = 0;
    public const STATUS_IN_TRANSIT = 1;
    public const STATUS_READY_FOR_PICKUP = 2;
    public const STATUS_RECEIVED = 3;
    public const STATUS_CANCELED = 4;
    public const STATUS_VOIDED = 5;
    public const STATUS_REJECTED = 6;
    public const STATUS_PARTIALLY_REJECTED = 7;

    public const TRANSPORT_TYPE_DELIVERY = 'delivery';
    public const TRANSPORT_TYPE_PICKUP = 'pickup';
    public const TRANSPORT_TYPE_LICENSED_TRANSPORTER = 'licensed_transporter';

    public const TYPES = [
        self::TYPE_ORDER,
        self::TYPE_QA_SAMPLE,
        self::TYPE_TRANSFER,
    ];

    public const TRANSPORT_TYPES = [
        self::TRANSPORT_TYPE_DELIVERY,
        self::TRANSPORT_TYPE_PICKUP,
        self::TRANSPORT_TYPE_LICENSED_TRANSPORTER,
    ];

    protected $fillable = [
        'license_id',
        'method',
        'type',
        'status',
        'client_id',
        'transport_type',
        'transporter_id',
        'invoice_total',
        'published_at',
        'est_departure_date',
        'est_arrival_date',
        'driver_id',
        'driver_helper_id',
        'vehicle_id',
        'travel_route',
        'sync_code',
        'sync_status',
        'synced_at',
    ];

    protected $dates = [
        'published_at',
        'est_departure_date',
        'est_arrival_date',
    ];

    protected $casts = [
        'invoice_total' => 'double',
        'est_departure_date' => 'date:Y-m-d',
        'est_arrival_date' => 'date:Y-m-d',
    ];

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Licensee::class, 'client_id', 'id');
    }

    public function transporter(): BelongsTo
    {
        return $this->belongsTo(Licensee::class, 'transporter_id', 'id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(ManifestItem::class);
    }

    public function driver(): BelongsTo
    {
        return $this->belongsTo(Driver::class, 'driver_id', 'id');
    }

    public function driverHelper(): BelongsTo
    {
        return $this->belongsTo(Driver::class, 'driver_helper_id', 'id');
    }

    public function vehicle(): BelongsTo
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_id', 'id');
    }

    public function inventories(): BelongsToMany
    {
        return $this->belongsToMany(
            Inventory::class,
            'manifest_inventory',
            'manifest_id',
            'inventory_id',
        )->withPivot('quantity');
    }

    public function updateSyncStatus(int $status): TraceableModelInterface
    {
        $this->update(['sync_status' => $status, 'synced_at' => Carbon::now(),]);

        return $this;
    }

    public function updateSyncCode(string $code): TraceableModelInterface
    {
        static::updateWithoutTraceableModelEvents(['sync_code' => $code,]);

        return $this;
    }

    public function scopeMethod(Builder $query, $method): Builder
    {
        return $query->where($this->getTable() . '.method', $method);
    }

    public function scopeType(Builder $query, $type): Builder
    {
        return $query->where($this->getTable() . '.type', $type);
    }

    public function scopeStatus(Builder $query, ...$status): Builder
    {
        return $query->whereIn($this->getTable() . '.status', $status);
    }

    public function scopeClientIds(Builder $query, ...$clientIds): Builder
    {
        return $query->whereIn($this->getTable() . '.client_id', $clientIds);
    }

    public function scopeTransportTypes(Builder $query, ...$transportTypes): Builder
    {
        return $query->whereIn($this->getTable() . '.transport_type', $transportTypes);
    }

    public function scopeOrderByInvoiceTotal(Builder $query, $direction): Builder
    {
        return $query->orderBy($this->getTable() . '.invoice_total', $direction);
    }

    public function scopeItemsTypes(Builder $query, ...$itemsTypes): Builder
    {
        return $query->whereHas(
            'items',
            fn(Builder $query) => $query->whereIn('type', $itemsTypes)
        );
    }

    public function scopePublishedAt(Builder $query, $publishedAtFrom, $publishedAtTo = null): Builder
    {
        return $this->buildQueryDateRanger($query, $publishedAtFrom, $publishedAtTo, 'published_at');
    }

    public function scopeOrderByClientName(Builder $query, $direction): Builder
    {
        $clientTable = $this->client()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($clientTable, $this->getTable().'.client_id', '=', $clientTable.'.id')
            ->orderBy($clientTable.'.name', $direction);
    }
}
