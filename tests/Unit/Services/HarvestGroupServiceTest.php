<?php

namespace Tests\Unit\Services;

use App\Events\Disposal\DisposalScheduled;
use App\Events\Harvest\HarvestFinalized;
use App\Events\TraceableModel\{TraceableModelCreated, TraceableModelDeleted, TraceableModelUpdated};
use App\Models\{Batch, Harvest, HarvestGroup, License, User};
use App\Services\HarvestGroupService;
use Illuminate\Support\Facades\Event;
use Tests\Concerns\Traceable;
use Tests\TestCase;

class HarvestGroupServiceTest extends TestCase
{
    use Traceable;

    protected static string $modelName = HarvestGroup::class;

    /**
     * @test
     */
    public function it_can_finalize_a_harvest()
    {
        Event::fake(
            [
                TraceableModelCreated::class,
                TraceableModelUpdated::class,
                TraceableModelDeleted::class,
                HarvestFinalized::class,
                DisposalScheduled::class,
            ]
        );
        $userId = factory(User::class)->create()->id;
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $harvestGroup = factory(static::$modelName)->create(
            ['status' => HarvestGroup::STATUS_DRY_CONFIRMED, 'license_id' => $licenseId, 'finished_at' => null]
        );
        $numberOfHarvests = rand(1, 5);
        $harvests = factory(Harvest::class, $numberOfHarvests)->create(
            array_merge(
                $harvestGroup->only(['license_id', 'room_id', 'start_at', 'finished_at']),
                ['harvest_group_id' => $harvestGroup->id,]
            )
        );
        $harvests->each(function (Harvest $harvest) {
            $harvest->batch()->create(factory(Batch::class)->make()->toArray());
        });


        $expectedNumberOfHarvests = rand(1, $numberOfHarvests);
        $finishedHarvests = $harvests->random($expectedNumberOfHarvests);
        $confirmData = $finishedHarvests->map(
            fn($harvest) => $harvest->only(
                [
                    'strain_id',
                    'flower_room_id',
                    'material_room_id',
                    'waste_room_id',
                ]
            )
        )->toArray();

        // execute
        $result = $this->getService()->finalize($harvestGroup, $confirmData, $userId);

        Event::assertDispatched(HarvestFinalized::class, $expectedNumberOfHarvests);

        $this->assertEquals('flower', $result->harvests->first()->flowerInventoryType->stateCategory->code);
        $this->assertEquals('other_material', $result->harvests->first()->materialInventoryType->stateCategory->code);
        $harvestedIds = $finishedHarvests->pluck('id')->all();
        $this->assertCount(
            $expectedNumberOfHarvests,
            $result->harvests->whereIn('id', $harvestedIds)->whereNotNull('finished_at')
        );
    }

    protected function getService(): HarvestGroupService
    {
        return app(HarvestGroupService::class);
    }
}
