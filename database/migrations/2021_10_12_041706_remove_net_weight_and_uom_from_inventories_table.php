<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveNetWeightAndUomFromInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('inventories', 'uom') && Schema::hasColumn('inventories', 'net_weight')) {
            Schema::table('inventories', function (Blueprint $table) {
                $table->dropColumn(['uom', 'net_weight']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->string('uom');
            $table->double('net_weight', 10, 2)->nullable();
        });
    }
}
