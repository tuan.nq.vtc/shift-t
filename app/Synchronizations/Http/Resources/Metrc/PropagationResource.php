<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models\Propagation;
use Exception;
use Illuminate\Http\Request;

/**
 * Class PropagationResource
 * @package App\Synchronizations\Http\Resources\Metrc
 */
class PropagationResource extends BaseMetrcResource
{
    protected $endpoint = '/batches';

    /**
     * @return self
     *
     * @throws Exception
     */
    protected function create(): self
    {
        /** @var Propagation $propagation */
        $propagation = $this->traceableModel;
        $errorFormat = 'Unable to sync propagation creation %s';
        $context = ['propagation' => $propagation];
        if (!$propagation->source_type) {
            $error = sprintf($errorFormat, 'without source type [name]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!($room = $propagation->room)) {
            $error = sprintf($errorFormat, 'with non-existed room');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!$room->sync_code) {
            $error = sprintf($errorFormat, 'without room LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!($strain = $propagation->strain)) {
            $error = sprintf($errorFormat, 'with non-existed strain');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }
        if (!$strain->sync_code) {
            $error = sprintf($errorFormat, 'without strain LEAF GLOBAL_ID [sync_code]');
            $this->logger->error($error, $context);
            throw new Exception($error);
        }

        $this->httpMethod = Request::METHOD_POST;
        /**
         * Metrc only support 2 types of plant batch types: "Seed" and "Clone"
         */
        // POST {{EndPoint}}/plantbatches/v1/createplantings?licenseNumber={{licenseNumber}}
        $this->requestUrl = '/plantbatches/v1/createplantings';
        /*[
          {
            "Name": "B. Kush 5-30",
            "Type": "Clone",
            "Count": 25,
            "Strain": "Spring Hill Kush",
            "Location": null,
            "PatientLicenseNumber": "X00001",
            "ActualDate": "2015-12-15"
          },
          {
            "Name": "B. Kush 5-31",
            "Type": "Seed",
            "Count": 50,
            "Strain": "Spring Hill Kush",
            "Location": null,
            "PatientLicenseNumber": "X00002",
            "ActualDate": "2015-12-15"
          }
        ]*/
        $this->requestBody = [
            [
                "Name" => $propagation->name,
                "Type" => $propagation->source_type,
                "Count" => $propagation->quantity,
                "Strain" => $propagation->strain->name,
                "Location" => $propagation->room->name,
                "PatientLicenseNumber" => "",
                "ActualDate" => $propagation->date ? $propagation->date->format(DATE_ATOM) : '',
            ],
        ];

        return $this;
    }


    /**
     * @return self
     *
     * @throws Exception
     */
    protected function update(): self
    {
        return $this;
    }

    /**
     * @throws Exception
     */
    protected function delete(): self
    {
        return $this;
    }

    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = '/plantbatches/v1/active';

        return $this;
    }
}
