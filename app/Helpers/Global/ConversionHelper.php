<?php


use App\Models\State;

if (!function_exists('getWeightUom')) {
    /**
     * This function get the mapping uom at Leaf/Metrc systems.<br/>
     * <i>P/s: All the weight values store in the database already is grams</i>
     *
     * @param string $vendor
     * @return string
     */
    function getWeightUom(string $vendor = State::REGULATOR_LEAF)
    {
        if (!in_array($vendor, [State::REGULATOR_LEAF, State::REGULATOR_METRC])) {
            return "Grams";
        }

        return $vendor === State::REGULATOR_LEAF ? "gm" : "Grams";
    }
}
