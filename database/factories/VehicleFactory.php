<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\License;
use App\Models\Vehicle;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Vehicle::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Vehicle::class,
                $data['license_id']
            ),
            'name' => $faker->words(2, true),
        ];
    }
);
