<?php

use App\Facades\Portal;
use App\Models\{User};
use Illuminate\Database\Seeder;

class PortalUserSeeder extends Seeder
{
    use DisableForeignKeys;
    use TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('users');
        Portal::fetchAllUsers()->each(
            fn(User $user) => User::forceCreate(
                [
                    'id' => $user->id,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'account_holder_id' => $user->account_holder_id,
                    'active' => $user->active,
                ]
            )
        );
        $this->enableForeignKeys();
    }
}
