<?php

namespace App\Synchronizations\Logic\Processes;

class PullProcessAggregate
{
    /** @var int */
    private int $total = 0;

    /** @var int */
    private int $success = 0;

    /** @var int */
    private int $error = 0;

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function addTotal(int $total): void
    {
        $this->total += $total;
    }

    /**
     * @return int
     */
    public function getSuccess(): int
    {
        return $this->success;
    }

    /**
     * @param int $success
     */
    public function addSuccess(int $success): void
    {
        $this->success += $success;
    }

    /**
     * @return int
     */
    public function getError(): int
    {
        return $this->error;
    }

    /**
     * @param int $error
     */
    public function addError(int $error): void
    {
        $this->error += $error;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'total' => $this->total,
            'success' => $this->success,
            'error' => $this->error,
        ];
    }
}
