<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_labels', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('print_label_template_id')->nullable();
            $table->string('category')->index();
            $table->string('name')->index();
            $table->text('base64')->nullable();
            $table->json('box')->nullable(); // width, height, top, bottom, left, right
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_labels');
    }
}
