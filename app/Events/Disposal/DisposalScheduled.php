<?php

namespace App\Events\Disposal;

use App\Events\Event;
use App\Models\Disposal;

class DisposalScheduled extends Event
{
    public Disposal $disposal;

    public function __construct(Disposal $disposal)
    {
        $this->disposal = $disposal;
    }
}
