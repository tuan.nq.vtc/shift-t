<?php

namespace App\Services;

use App\Models\{Harvest, HarvestGroup, Plant};
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class StatisticService
 * @package App\Services
 */
class StatisticService
{

    public function harvests(string $licenseId): array
    {
        $finishedHarvests = Harvest::withCount('plants')->whereNotNull('finished_at')
            ->where('license_id', $licenseId)->get();
        $totalOnGoingHarvests = Harvest::whereHas(
            'group',
            fn(Builder $q) => $q->whereIn(
                'status',
                [HarvestGroup::STATUS_WET_CONFIRMED, HarvestGroup::STATUS_DRY_CONFIRMED]
            )
        )->where('license_id', $licenseId)->count();
        return [
            'total_harvested_plants' => $finishedHarvests->sum('plants_count'),
            'total_closed_harvests' => $finishedHarvests->count(),
            'total_ongoing_harvests' => $totalOnGoingHarvests
        ];
    }

    public function countPlantsByGrowStatuses($licenseId): array
    {
        return Plant::withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])
            ->licenseId($licenseId)
            ->select(
                [
                    DB::raw('COUNT(id) as plants_count'),
                    'grow_status as name',
                ]
            )
            ->groupBy('grow_status')->get()->toArray();
    }

}
