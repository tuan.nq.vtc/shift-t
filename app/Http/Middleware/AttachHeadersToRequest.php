<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AttachHeadersToRequest
{
    /**
     * Push needed params to request based on user current license id.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = $request->headers->get(config('app.services.auth.headers.user_uuid'));
        $accountHolderId = $request->headers->get(config('access.headers.account_holder_id'));
        $licenseId = $request->headers->get(config('access.headers.current_license_id'));

        if (!$userId && config('app.env') === 'local') {
            $userId = config('app.services.auth.test_user_id');
        }

        $request->merge([
            'user_id' => $userId,
            'license_id' => $licenseId,
            'account_holder_id' => $accountHolderId
        ]);

        return $next($request);
    }
}
