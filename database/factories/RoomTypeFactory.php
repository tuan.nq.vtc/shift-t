<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RoomType;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    RoomType::class,
    function (Faker $faker) {
        return [
            'name' => $faker->words(2, true) . '_' . $faker->numberBetween(1),
            'status' => rand(0, 1),
            'category' => $faker->randomElement(RoomType::getCategories()),
        ];
    }
);
