<?php

namespace App\Providers;

use App\Listeners\{AdditiveEventSubscriber,
    AdditiveInventoryEventSubscriber,
    EndProductEventSubscriber,
    GrowCycleSubscriber,
    HarvestEventSubscriber,
    InventoryEventSubscriber,
    InventoryHistoryEventSubscriber,
    ManifestEventSubscriber,
    SourceProductEventSubscriber,
    TraceableModelEventSubscriber,
    WasteBagEventSubscriber};
use App\Synchronizations\Listeners\SyncEventSubscriber;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        HarvestEventSubscriber::class,
        AdditiveEventSubscriber::class,
        AdditiveInventoryEventSubscriber::class,
        TraceableModelEventSubscriber::class,
        WasteBagEventSubscriber::class,
        GrowCycleSubscriber::class,
        SyncEventSubscriber::class,
        InventoryHistoryEventSubscriber::class,
        InventoryEventSubscriber::class,
        SourceProductEventSubscriber::class,
        EndProductEventSubscriber::class,
        ManifestEventSubscriber::class,
    ];
}
