<?php

namespace App\Models;

use App\Models\Traits\Scope\LicenseFilterable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class AdditiveLog
 * @package App\Models
 */
class AdditiveLog extends BaseModel
{
    use LicenseFilterable;

    public const ADDITIVE_TYPE_PESTICIDE  = 0;
    public const ADDITIVE_TYPE_NUTRIENT  = 1;

    protected $dates = ['applied_date',];

    protected $fillable = [
        'additive_type',
        'license_id',
        'additive_id',
        'additive_operation_id',
        'application_device',
        'propagation_id',
        'grow_cycle_id',
        'room_id',
        'subroom_id',
        'plant_id',
        'cost',
        'applied_date',
        'applied_by',
        'created_by',
        'updated_by',
        'supplier_id',
    ];

    public function additive(): BelongsTo
    {
        return $this->belongsTo(Additive::class, 'additive_id', 'id');
    }

    public function operation(): BelongsTo
    {
        return $this->belongsTo(AdditiveOperation::class, 'additive_operation_id', 'id');
    }

    public function growCycle(): BelongsTo
    {
        return $this->belongsTo(GrowCycle::class, 'grow_cycle_id', 'id');
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function plant(): BelongsTo
    {
        return $this->belongsTo(Plant::class, 'plant_id', 'id');
    }

    public function applicator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'applied_by', 'id');
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function modifier(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function appliedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'applied_by', 'id');
    }

}
