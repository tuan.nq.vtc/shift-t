<?php

namespace Bamboo\StreamProcessing\Console;

use Bamboo\StreamProcessing\Models\FailedEvent;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Carbon;

class ListFailedCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'stream:failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all failed stream events';

    /**
     * The table headers for the command.
     *
     * @var array
     */
    protected $headers = ['ID', 'Topic', 'Payload', 'Metadata', 'Failed At'];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (count($streams = $this->getFailedEvents()) === 0) {
            $this->info('No failed streams!');
            return;
        }

        $this->displayFailedStreams($streams);
    }

    /**
     * Compile the failed jobs into a displayable format.
     *
     * @return array
     *
     * @throws BindingResolutionException
     */
    protected function getFailedEvents(): array
    {
        $failedEvents = $this->laravel->make(FailedEvent::class)->all();

        return $failedEvents->map(fn($failed) => $this->parseFailedEvent($failed->toArray()))->filter()->all();
    }

    /**
     * Parse the failed job row.
     *
     * @param array $failed
     * @return array
     */
    protected function parseFailedEvent(array $failed): array
    {
        return [
            $failed['id'],
            $failed['topic'],
            json_encode($failed['payload']),
            json_encode($failed['metadata']),
            (new Carbon($failed['failed_at']))->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * Display the failed jobs in the console.
     *
     * @param array $jobs
     * @return void
     */
    protected function displayFailedStreams(array $jobs)
    {
        $this->table($this->headers, $jobs);
    }
}
