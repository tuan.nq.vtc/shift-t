<?php

namespace Tests\Synchronizer;

use App\Models\{License, Room, Trace, TraceableModel};
use App\Synchronizations\Synchronizer;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Illuminate\Support\Facades\Http;

/**
 * Class RoomTest
 * @package Tests\Synchronizer
 */
class RoomTest extends TestBase
{
    /** @test */
    public function it_can_push_create_action_to_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;
        $room = factory(Room::class)->create(['license_id' => $licenseId]);
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PUSH,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $room->id,
                'resource_type' => get_classname($room),
                'resource_data' => $room->attributesToArray(),
                'resource_changes' => $room->getChanges(),
            ]
        );

        $roomCode = "WAG010101.AR" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.AR4U;
        $responseData = [
            [
                "name" => $room->name,
                "global_id" => $roomCode,
                "external_id" => $room->id
            ]
        ];
        Http::fake(['/areas' => Http::response($responseData, 200),]);

        $result = app(Synchronizer::class)->push($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($roomCode, $result->resource->sync_code);
    }

    /** @test */
    public function it_can_push_create_action_to_metrc()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $room = factory(Room::class)->create(['license_id' => $licenseId]);
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PUSH,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_id' => $room->id,
                'resource_type' => get_classname($room),
                'resource_data' => $room->attributesToArray(),
                'resource_changes' => $room->getChanges(),
            ]
        );

        Http::fake(['/locations/v1/create*' => Http::response([], 200),]);

        $roomId = $this->faker->randomNumber();
        Http::fake(
            [
                '/locations/v1/active*' => Http::response(
                    [['Id' => $roomId, 'Name' => $room->name]],
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->push($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($roomId, $result->resource->sync_code);
        $this->assertEquals(TraceableModel::SYNC_STATUS_SYNCED, $result->resource->sync_status);
    }

    /** @test */
    public function it_can_fetch_all_from_leaf()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'WA'])->id;

        $currentPage = rand(2, 10);
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PULL,
                'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Room::class,
                'resource_conditions' => [
                    'page' => $currentPage
                ],
            ]
        );
        $data = [];
        $totalRecord = rand(5, 10);
        for ($j = 1; $j <= $totalRecord; $j++) {
            $syncCode = "WAG010101.AR" . $this->faker->regexify('[A-Z0-9]{4}'); // WAG010101.AR4U;
            $data[] = [
                "name" => $this->faker->name,
                "global_id" => $syncCode,
                "external_id" => $this->faker->uuid,
            ];
        }

        $responseData = [
            'current_page' => $currentPage,
            'last_page' => $currentPage,
            'data' => $data
        ];
        Http::fake(["/areas?page={$currentPage}" => Http::response($responseData, 200),]);

        $this->expectsJobs(PublishStreamEventJob::class);
        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, Room::count());
    }

    public function it_can_fetch_all_from_metrc()
    {
        $licenseId = factory(License::class)->create(['state_code' => 'CA'])->id;
        $trace = factory(Trace::class)->create(
            [
                'license_id' => $licenseId,
                'method' => Trace::METHOD_PULL,
                'action' => TraceableModel::SYNC_ACTION_CREATE,
                'status' => Trace::STATUS_PENDING,
                'resource_type' => Room::class,
            ]
        );
        $totalRecord = rand(1, 100);
        $data = [];
        for ($i = 1; $i <= $totalRecord; $i++) {
            $data[] = ['Id' => $this->faker->randomNumber(), 'Name' => $this->faker->name];
        }
        Http::fake(
            [
                '/locations/v1/active*' => Http::response(
                    $data,
                    200
                ),
            ]
        );

        $result = app(Synchronizer::class)->pull($trace);
        $this->assertEquals(Trace::STATUS_COMPLETED, $result->status);
        $this->assertEquals($totalRecord, $result->response_data['total']);
    }
}
