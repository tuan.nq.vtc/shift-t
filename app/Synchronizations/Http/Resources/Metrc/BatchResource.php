<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models\{Batch, Propagation, TraceableModel};
use Exception;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class Propagation
 * @package App\Synchronizations\Http\Resources\Metrc
 *
 * @property Propagation|TraceableModel $traceableModel
 */
class BatchResource extends BaseMetrcResource
{
    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function create(): self
    {
        $propagation = $this->traceableModel->propagation;
        throw_unless(
            $propagation->source_type,
            new Exception('Unable to sync propagation creation without source type [name] to METRC')
        );
        throw_unless(
            $propagation->subroom->room->sync_code,
            new Exception('Unable to sync propagation creation without synced room to METRC')
        );
        throw_unless(
            $propagation->strain->sync_code,
            new Exception('Unable to sync propagation creation without synced strain to METRC')
        );

        switch ($this->traceableModel->type) {
            case  Batch::TYPE_PROPAGATION:
                $this->httpMethod = Request::METHOD_POST;
                /**
                 * Metrc only support 2 types of plant batch types: "Seed" and "Clone"
                 */
                // POST {{EndPoint}}/plantbatches/v1/createplantings?licenseNumber={{licenseNumber}}
                $this->requestUrl = '/plantbatches/v1/createplantings';
                /*[
                  {
                    "Name": "B. Kush 5-30",
                    "Type": "Clone",
                    "Count": 25,
                    "Strain": "Spring Hill Kush",
                    "Location": null,
                    "PatientLicenseNumber": "X00001",
                    "ActualDate": "2015-12-15"
                  },
                  {
                    "Name": "B. Kush 5-31",
                    "Type": "Seed",
                    "Count": 50,
                    "Strain": "Spring Hill Kush",
                    "Location": null,
                    "PatientLicenseNumber": "X00002",
                    "ActualDate": "2015-12-15"
                  }
                ]*/
                $this->requestBody = [
                    [
                        "Name" => $propagation->name,
                        "Type" => $propagation->source_type,
                        "Count" => $propagation->quantity,
                        "Strain" => $propagation->strain->name,
                        "Location" => $propagation->room->name,
                        "PatientLicenseNumber" => "",
                        "ActualDate" => $propagation->date ? $propagation->date->format(DATE_ATOM) : '',
                    ],
                ];

                break;
            case  Batch::TYPE_VEGETATION:
                // Call action with suffix `Vegetation` from trait `Vegetation`
                $this->action .= 'Vegetation';
                $this->plantGroup = $this->traceableModel->plantGroup;
                break;
            case  Batch::TYPE_HARVEST:
                $this->action .= 'Harvest';
                break;
            case  Batch::TYPE_PRODUCTION:
                $this->action .= 'Production';
                // TODO: Check logic of intermediate/end product batch
                $this->harvest = $this->traceableModel->harvest;
                break;
        }

        return $this;
    }

    protected function delete(): self
    {
        $this->httpMethod = Request::METHOD_DELETE;
        $this->requestUrl = '/plantbatches/v1/'.$this->traceableModel->sync_code;

        return $this;
    }
}
