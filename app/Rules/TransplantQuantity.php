<?php

namespace App\Rules;

use App\Models\Propagation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class TransplantQuantity extends Quantity
{
    protected bool $isRequestArray;

    public function __construct(
        string $propagationId,
        bool $isRequestArray,
        array $where = []
    ) {
        $this->isRequestArray = $isRequestArray;
        parent::__construct(Propagation::class, $propagationId, 'id', 'quantity', $where);
    }

    protected function buildQuery(): Builder
    {
        if (!$this->isRequestArray) {
            return parent::buildQuery();
        }

        $quantityStack = explode('.', $this->attribute);
        $quantityKey = end($quantityStack);
        if (Str::contains($this->modelKey, '.*.')) {
            $propagationIdStack = explode('.', $this->modelKey);
            $propagationId = Str::replaceLast($quantityKey, end($propagationIdStack), $this->attribute);
        } else {
            $propagationId = Str::replaceLast($quantityKey, $this->modelKey, $this->attribute);
        }
        $query = $this->modelClass::where($this->modelKeyName, request()->input($propagationId))
            ->withoutGlobalScopes([$this->modelClass::DEFAULT_ORDER_SCOPE]);

        foreach ($this->where as $column => $condition) {
            if (is_callable($condition)) {
                $query->where($condition);
            } else {
                $query->where($column, $condition);
            }
        }

        return $query;
    }
}

