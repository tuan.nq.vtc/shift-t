<?php
return [
    'storage_handler' => [
        'authenticate' => [
            'clientId' => env('STORAGE_HANDLER_CLIENT_ID'),
            'clientSecret' => env('STORAGE_HANDLER_CLIENT_SECRET'),
        ],
        'baseUrl' => env('STORAGE_HANDLER_BASE_URL'),
        'endpoints' => [
            'upload' => env('STORAGE_HANDLER_UPLOAD_API'),
            'delete' => env('STORAGE_HANDLER_DELETE_RESOURCE_API')
        ]
    ]
];
