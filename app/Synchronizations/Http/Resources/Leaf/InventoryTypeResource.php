<?php

namespace App\Synchronizations\Http\Resources\Leaf;

use App\Synchronizations\Constants\LeafConstant;
use App\Synchronizations\Helpers\LeafHelper;
use Illuminate\Http\Request;
use App\Models\{InventoryType, SeedToSale, TraceableModel};
use Exception;
use Throwable;

/**
 * Class InventoryTypeResource
 * @package App\Synchronizations\Http\Resources\Leaf
 *
 * @property InventoryType|TraceableModel $traceableModel
 */
class InventoryTypeResource extends BaseLeafResource
{
    protected static string $endpoint = LeafConstant::ENDPOINT_INVENTORY_TYPE;

    protected function fetchAll(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = self::$endpoint;
        if (!empty($this->conditions['page'])) {
            $this->requestQuery['page'] = $this->conditions['page'];
        }
        return $this;
    }

    protected function create(): self
    {
        $inventoryType = $this->traceableModel;
        $postData = [
            'external_id' => $inventoryType->id,
            'name' => $inventoryType->name,
            'type' => $inventoryType->stateCategory->parent->code,
            'intermediate_type' => $inventoryType->stateCategory->code,
            'uom' => $inventoryType->uom? LeafHelper::mapToLeafUom($inventoryType->uom) : null,
        ];
        if (!empty($inventoryType->options['weight'])) {
            $postData['weight_per_unit_in_grams'] = $inventoryType->options['weight'];
        }
        if (!empty($inventoryType->options['serving_num'])) {
            $postData['serving_num'] = $inventoryType->options['serving_num'];
        }
        if (!empty($inventoryType->options['serving_size'])) {
            $postData['serving_size'] = $inventoryType->options['serving_size'];
        }
        return parent::create()->setBody(['inventory_type' => [$postData],]);
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function update(): self
    {
        $inventoryType = $this->traceableModel;
        throw_unless(
            $inventoryType->sync_code,
            new Exception('Unable to sync inventory update without LEAF global_id [sync_code]')
        );

        return parent::update()->setBody(
            [
                'inventory_type' => [
                    'external_id' => $inventoryType->id,
                    'name' => $inventoryType->name,
                    'type' => $inventoryType->stateCategory->parent->code,
                    'intermediate_type' => $inventoryType->stateCategory->code,
                    'weight_per_unit_in_grams' => $inventoryType->stateCategory->is_weight_per_unit_required ? $inventoryType->options['weight'] : 0,
                    'serving_num' => $inventoryType->stateCategory->is_serving_required ? $inventoryType->options['serving_num'] : 0,
                    'serving_size' => $inventoryType->stateCategory->is_serving_required ? $inventoryType->options['serving_size'] : 0,
                    'uom' => $inventoryType->uom? LeafHelper::mapToLeafUom($inventoryType->uom) : null,
                    'global_id' => $inventoryType->sync_code,
                ],
            ]
        );
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    protected function delete(): self
    {
        throw_unless(
            $this->traceableModel->sync_code,
            new Exception('Unable to sync room deletion without LEAF global_id [sync_code]')
        );

        return parent::delete();
    }
}
