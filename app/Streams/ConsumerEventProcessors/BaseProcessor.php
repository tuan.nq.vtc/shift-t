<?php

namespace App\Streams\ConsumerEventProcessors;

use Bamboo\StreamProcessing\Contracts\ConsumerEventProcessInterface;
use Bamboo\StreamProcessing\Contracts\StreamEventInterface;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

abstract class BaseProcessor implements ConsumerEventProcessInterface
{
    /**
     * @var string
     */
    protected static string $modelName;

    /**
     * @param StreamEventInterface $streamEvent
     */
    public function execute(StreamEventInterface $streamEvent): void
    {
        $metadata = $streamEvent->getMetadata();
        if (empty($metadata['action'])) {
            // skip process the event
            return;
        }
        $action = $metadata['action'];
        if (!method_exists($this, $action)) {
            return;
        }
        call_user_func([$this, $action], $streamEvent);
    }

    /**
     * @return Model
     */
    protected function getModel(): Model
    {
        return app(static::$modelName);
    }

    /**
     * @param StreamEventInterface $streamEvent
     * @throws Exception
     */
    protected function create(StreamEventInterface $streamEvent): void
    {
        $payload = $streamEvent->getPayload();
        $id = $this->getIdFromPayload($payload);
        if ($this->getModel()->find($id)) {
            throw new Exception('The record already exists');
        }

        $this->getModel()->newQuery()->make($payload)->forceFill(['id' => $id])->save();
    }

    /**
     * @param StreamEventInterface $streamEvent
     * @throws Exception
     */
    protected function update(StreamEventInterface $streamEvent): void
    {
        $payload = $streamEvent->getPayload();
        $this->getModel()->findOrFail($this->getIdFromPayload($payload))->update(Arr::except($payload, ['id']));
    }

    /**
     * @param StreamEventInterface $streamEvent
     * @throws Exception
     */
    protected function delete(StreamEventInterface $streamEvent): void
    {
        $this->getModel()->findOrFail($this->getIdFromPayload($streamEvent->getPayload()))->delete();
    }

    /**
     * @param array $payload
     * @return string
     * @throws Exception
     */
    protected function getIdFromPayload(array $payload): string
    {
        $id = Arr::get($payload, 'id', '');
        if (!$id) {
            throw new Exception('ID not found');
        }

        return $id;
    }
}
