<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Inventory, License, Licensee, Manifest, QASample, Strain, User};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    QASample::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                QASample::class,
                $data['license_id']
            ),
            'manifest_id' => fn($data) => factory(Manifest::class)->create(['license_id' => $data['license_id']])->id,
            'inventory_id' => fn($data) => factory(Inventory::class)->create(['license_id' => $data['license_id']])->id,
            'strain_id' => fn($data) => factory(Strain::class)->create(['license_id' => $data['license_id']])->id,
            'sample_type' => $faker->randomElement(QASample::getSampleTypeList()),
            'tested_at' => $faker->dateTimeThisYear(),
            'created_by' => fn() => factory(User::class)->create()->id,
            'weight' => $faker->numberBetween(4, 8),
            'status' => $faker->randomElement(QASample::getStatuses()),
            'lab_id' => factory(Licensee::class)->create()->id,
        ];
    }
);
