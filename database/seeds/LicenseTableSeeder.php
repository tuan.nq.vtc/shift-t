<?php

use App\Facades\Portal;
use App\Models\License;
use App\Services\LicenseService;
use Illuminate\Database\Seeder;

/**
 * Class LicenseTableSeeder.
 */
class LicenseTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     * @param LicenseService $licenseService
     */
    public function run(LicenseService $licenseService)
    {
        License::reguard();
        License::flushEventListeners();

        Portal::fetchAllLicenses()->each(
            function ($portalLicense) use ($licenseService) {
                /** @var License $license */
                $license = License::findOrNew($portalLicense->id);
                $license->forceFill(['id' => $portalLicense->id])->fill($portalLicense->toArray())->save();

                $licenseService->syncStateCategories($license);
            }
        );

        $this->enableForeignKeys();
    }
}
