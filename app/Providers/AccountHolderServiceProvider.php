<?php

namespace App\Providers;

use App\Services\{Contracts\AccountHolderServiceInterface, RoomTypeService, SupplierService};
use Illuminate\Support\ServiceProvider;

class AccountHolderServiceProvider extends ServiceProvider
{
    protected static array $accountHolderServices = [
        SupplierService::class,
        RoomTypeService::class
    ];


    protected function getBindingServices(): array
    {
        return static::$accountHolderServices;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->getBindingServices() as $accountHolderService) {
            $this->app->resolving(
                $accountHolderService,
                function (AccountHolderServiceInterface $transistor) {
                    if ($accountHolderId = request('account_holder_id')) {
                        $transistor->setAccountHolderId($accountHolderId);
                    }
                }
            );
        }
    }
}
