<?php

namespace App\Events\Trace;

use App\Events\Event;
use App\Models\Trace;

class TraceCreated extends Event
{
    /**
     * @var Trace
     */
    public Trace $trace;

    /**
     * @param Trace $trace
     */
    public function __construct(Trace $trace)
    {
        $this->trace = $trace;
    }
}
