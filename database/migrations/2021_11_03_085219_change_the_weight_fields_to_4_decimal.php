<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\{Type, FloatType};

class ChangeTheWeightFieldsTo4Decimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('inventories', function (Blueprint $table) {
            $table->decimal('qty_on_hand', 12, 4)->default(0)->change();
            $table->decimal('qty_allocated', 12, 4)->default(0)->change();
            $table->decimal('qty_on_hold', 12, 4)->default(0)->change();
        });

        Schema::table('inventory_histories', function (Blueprint $table) {
            $table->decimal('qty_current', 12, 4)->default(0)->change();
            $table->decimal('qty_variable', 12, 4)->change();
        });

        Schema::table('harvests', function (Blueprint $table) {
            $table->decimal('flower_wet_weight', 12, 4)->default(0)->change();
            $table->decimal('material_wet_weight', 12, 4)->default(0)->change();
            $table->decimal('waste_wet_weight', 12, 4)->default(0)->change();
            $table->decimal('flower_dry_weight', 12, 4)->default(0)->change();
            $table->decimal('material_dry_weight', 12, 4)->default(0)->change();
            $table->decimal('waste_dry_weight', 12, 4)->default(0)->change();
        });

        Schema::table('conversion_input_lot', function (Blueprint $table) {
            $table->decimal('quantity', 12, 4)->default(0)->change();
        });

        Schema::table('harvest_wet_weights', function (Blueprint $table) {
            $table->decimal('weight', 12, 4)->default(0)->change();
        });

        Schema::table('additives', function (Blueprint $table) {
            $table->decimal('available_quantity', 12, 4)->default(0)->change();
            $table->decimal('total_quantity', 12, 4)->default(0)->change();
            $table->decimal('avg_price', 12, 4)->default(0)->change();
        });

        Schema::table('additive_inventories', function (Blueprint $table) {
            $table->decimal('total_quantity', 12, 4)->default(0)->change();
            $table->decimal('available_quantity', 12, 4)->default(0)->change();
            $table->decimal('unit_price', 12, 4)->default(0)->change();
            $table->decimal('total_price', 12, 4)->default(0)->change();
        });

        Schema::table('additive_operations', function (Blueprint $table) {
            $table->decimal('applied_quantity', 12, 4)->default(0)->change();
            $table->decimal('unit_price', 12, 4)->default(0)->change();
        });

        Schema::table('additive_logs', function (Blueprint $table) {
            $table->decimal('cost', 12, 4)->default(0)->change();
        });

        Schema::table('conversion_input_lot', function (Blueprint $table) {
            $table->decimal('quantity', 12, 4)->default(0)->change();
        });

        Schema::table('qa_samples', function (Blueprint $table) {
            $table->decimal('weight', 12, 4)->default(0)->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->decimal('qty_on_hand', 10, 2)->default(0)->change();
            $table->decimal('qty_allocated', 10, 2)->default(0)->change();
            $table->decimal('qty_on_hold', 10, 2)->default(0)->change();
        });

        Schema::table('inventory_histories', function (Blueprint $table) {
            $table->decimal('qty_current', 10, 2)->default(0)->change();
            $table->decimal('qty_variable', 10, 2)->change();
        });

        Schema::table('harvests', function (Blueprint $table) {
            $table->decimal('flower_wet_weight', 10, 2)->default(0)->change();
            $table->decimal('material_wet_weight', 10, 2)->default(0)->change();
            $table->decimal('waste_wet_weight', 10, 2)->default(0)->change();
            $table->decimal('flower_dry_weight', 10, 2)->default(0)->change();
            $table->decimal('material_dry_weight', 10, 2)->default(0)->change();
            $table->decimal('waste_dry_weight', 10, 2)->default(0)->change();
        });

        Schema::table('conversion_input_lot', function (Blueprint $table) {
            $table->decimal('quantity', 10, 2)->default(0)->change();
        });

        Schema::table('harvest_wet_weights', function (Blueprint $table) {
            $table->decimal('weight', 10, 2)->default(0)->change();
        });

        Schema::table('additives', function (Blueprint $table) {
            $table->decimal('available_quantity', 10, 2)->default(0)->change();
            $table->decimal('total_quantity', 10, 2)->default(0)->change();
            $table->decimal('avg_price', 10, 2)->default(0)->change();
        });

        Schema::table('additive_inventories', function (Blueprint $table) {
            $table->decimal('total_quantity', 10, 2)->default(0)->change();
            $table->decimal('available_quantity', 10, 2)->default(0)->change();
            $table->decimal('unit_price', 10, 2)->default(0)->change();
            $table->decimal('total_price', 10, 2)->default(0)->change();
        });

        Schema::table('additive_operations', function (Blueprint $table) {
            $table->decimal('applied_quantity', 10, 2)->default(0)->change();
            $table->decimal('unit_price', 10, 2)->default(0)->change();
        });

        Schema::table('additive_logs', function (Blueprint $table) {
            $table->decimal('cost', 10, 2)->default(0)->change();
        });

        Schema::table('conversion_input_lot', function (Blueprint $table) {
            $table->decimal('quantity', 10, 2)->default(0)->change();
        });

        Schema::table('qa_samples', function (Blueprint $table) {
            $table->decimal('weight', 10, 2)->default(0)->change();
        });
    }
}
