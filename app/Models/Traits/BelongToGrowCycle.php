<?php

namespace App\Models\Traits;

use App\Models\GrowCycle;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongToGrowCycle
{
    public function growCycle(): BelongsTo
    {
        return $this->belongsTo(GrowCycle::class);
    }

    public function scopeGrowCycleId(Builder $query, ...$growCycleId): Builder
    {
        return $query->whereIn($this->getTable().'.grow_cycle_id', $growCycleId);
    }

    public function scopeGrowCycle(Builder $query, ...$growCycleId): Builder
    {
        return $this->scopeGrowCycleId($query, ...$growCycleId);
    }

    public function scopeOrderByGrowCycle(Builder $query, $direction): Builder
    {
        $growCycleTable = $this->growCycle()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($growCycleTable, $this->getTable().'.grow_cycle_id', '=', $growCycleTable.'.id')
            ->orderBy($growCycleTable.'.name', $direction);
    }
}
