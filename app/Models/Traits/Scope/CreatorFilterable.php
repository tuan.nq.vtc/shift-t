<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;

trait CreatorFilterable
{
    /**
     * @param Builder $query
     * @param ...$value
     * @return Builder
     */
    public function scopeCreatedBy(Builder $query, ...$value): Builder
    {
        return $query->whereIn('created_by', $value);
    }

    public function scopeOrderByCreatedBy(Builder $query, $direction): Builder
    {
        $userStable = $this->creator()->getRelated()->getTable();

        return $query->select([$this->getTable().'.*'])
            ->leftJoin($userStable, $this->getTable().'.created_by', '=', $userStable.'.id')
            ->orderBy($userStable.'.first_name', $direction);
    }
}
