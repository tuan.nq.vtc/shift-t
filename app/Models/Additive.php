<?php

namespace App\Models;

use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{CreatorFilterable,
    DatetimeFilterable,
    LicenseFilterable,
    ModifierFilterable,
    NameFilterable
};
use Illuminate\Database\Eloquent\{Builder, Collection, Relations\BelongsTo, Relations\HasMany};
use Illuminate\Support\Arr;

/**
 * Class Additive
 *
 * @property string $name
 * @property double $available_quantity
 * @property integer $license_id
 * @property double $total_box
 * @property double $avg_price
 * @property string $status
 * @property bool $visibility
 * @property AdditiveInventory[]|Collection $inventories
 * @method Builder available()
 * @package App\Models
 * @property string $id
 * @property string $type
 * @property array|null $ingredients
 * @property string $uom
 * @property string $created_by
 * @property string $updated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $epa_regulation_number
 * @property bool $is_other_type
 * @property-read \App\Models\User $creator
 * @property-read int|null $inventories_count
 * @property-read \App\Models\User $modifier
 * @method static Builder|Additive createdAt($firstValue, $secondValue = null)
 * @method static Builder|Additive createdBy($value)
 * @method static Builder|BaseModel defaultOrderBy()
 * @method static Builder|Additive licenseId(string $licenseId)
 * @method static Builder|Additive name($name)
 * @method static Builder|Additive newModelQuery()
 * @method static Builder|Additive newQuery()
 * @method static Builder|Additive orderByDepleted($direction)
 * @method static Builder|Additive query()
 * @method static Builder|Additive search($value)
 * @method static Builder|Additive status($value)
 * @method static Builder|Additive type($type, $option = null)
 * @method static Builder|Additive uom($value)
 * @method static Builder|Additive updatedAt($firstValue, $secondValue = null)
 * @method static Builder|Additive updatedBy($value)
 * @method static Builder|Additive visibility($value)
 * @method static Builder|Additive whereAvailableQuantity($value)
 * @method static Builder|Additive whereAvgPrice($value)
 * @method static Builder|Additive whereCreatedAt($value)
 * @method static Builder|Additive whereCreatedBy($value)
 * @method static Builder|Additive whereEpaRegulationNumber($value)
 * @method static Builder|Additive whereId($value)
 * @method static Builder|Additive whereIngredients($value)
 * @method static Builder|Additive whereIsOtherType($value)
 * @method static Builder|Additive whereLicenseId($value)
 * @method static Builder|Additive whereName($value)
 * @method static Builder|Additive whereType($value)
 * @method static Builder|Additive whereUom($value)
 * @method static Builder|Additive whereUpdatedAt($value)
 * @method static Builder|Additive whereUpdatedBy($value)
 * @method static Builder|Additive whereVisibility($value)
 * @mixin \Eloquent
 */
class Additive extends BaseModel
{
    use LicenseFilterable;
    use NameFilterable;
    use CreatorFilterable;
    use ModifierFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    // status
    public const STATUS_SUFFICIENT = 'sufficient';
    public const STATUS_MODERATE = 'moderate';
    public const STATUS_LOW = 'low';
    public const STATUS_DEPLETED = 'depleted';

    public const QUANTITY_STATUS_SUFFICIENT = 8000;
    public const QUANTITY_STATUS_MODERATE = 4000;
    public const QUANTITY_STATUS_DEPLETED = 0;

    // visibility
    public const VISIBILITY_ENABLED = 1;
    public const VISIBILITY_DISABLED = 0;

    // uom
    public const UOM_GALLON = 'Gallons';
    public const UOM_LITER = 'Liters';
    public const UOM_OUNCE = 'Ounces';

    public const TYPE_PESTICIDE = 'pesticide';
    public const TYPE_NUTRIENT = 'nutrient';
    public const TYPE_OTHER = 'other';

    public const TYPES = [
        self::TYPE_PESTICIDE,
        self::TYPE_NUTRIENT,
        self::TYPE_OTHER,
    ];

    protected $fillable = [
        'ingredients',
        'type',
        'name',
        'license_id',
        'uom',
        'available_quantity',
        'total_quantity',
        'used_quantity',
        'avg_price',
        'visibility',
        'created_by',
        'updated_by',
        'epa_regulation_number',
        'is_other_type',
    ];

    protected $appends = ['status', 'applied_quantity'];

    protected $casts = [
        'is_other_type' => 'boolean',
        'ingredients' => 'array',
        'visibility' => 'boolean',
        'available_quantity' => 'double',
        'used_quantity' => 'double',
        'avg_price' => 'double',
        'total_quantity' => 'double',
    ];


    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function modifier(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function inventories(): HasMany
    {
        return $this->hasMany(AdditiveInventory::class, 'additive_id', 'id');
    }

    /**
     * @param Builder $query
     * @param         $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        $value = trim($value);
        if (empty($value)) {
            return $query;
        }

        return $query
            ->where('name', 'like', '%' . $value . '%');
    }

    public function scopeType(Builder $query, $value): Builder
    {
        return $query->where('type', $value);
    }

    /**
     * @param Builder $query
     * @param         $value
     *
     * @return mixed
     */
    public function scopeVisibility(Builder $query, $value): Builder
    {
        return $query->where('visibility', $value);
    }

    /**
     * @param Builder $query
     * @param ...$value
     * @return Builder
     */
    public function scopeUom(Builder $query, ...$value): Builder
    {
        return $query->whereIn('uom', $value);
    }

    /**
     * @param Builder $query
     * @param         $value
     *
     * @return Builder
     */
    public function scopeStatus(Builder $query, $value): Builder
    {
        switch ($value) {
            case self::STATUS_SUFFICIENT:
                return $query->where('available_quantity', ">=", self::QUANTITY_STATUS_SUFFICIENT);
            case  self::STATUS_MODERATE:
                return $query->whereBetween(
                    'available_quantity',
                    [self::QUANTITY_STATUS_MODERATE, self::QUANTITY_STATUS_SUFFICIENT]
                );
            case self::STATUS_LOW:
                return $query->whereBetween(
                    'available_quantity',
                    [self::QUANTITY_STATUS_DEPLETED, self::QUANTITY_STATUS_MODERATE]
                );
            case self::STATUS_DEPLETED:
                return $query->where('available_quantity', self::QUANTITY_STATUS_DEPLETED);
            default:
                return $query;
        }
    }

    /**
     * @param Builder $query
     * @param         $direction
     *
     * @return Builder
     */
    public function scopeOrderByDepleted(Builder $query, $direction): Builder
    {
        return $query->orderBy('available_quantity', $direction);
    }

    public function scopeOrderByStatus(Builder $query, $direction): Builder
    {
        return $query->orderBy('available_quantity', $direction);
    }

    public function scopeOrderByTotalQuantity(Builder $query, $direction): Builder
    {
        return $query->orderBy('total_quantity', $direction);
    }

    public function scopeOrderByAppliedQuantity(Builder $query, $direction): Builder
    {
        return $query->orderByRaw(' (total_quantity - available_quantity) ' . $direction);
    }

    public function scopeOrderByType(Builder $query, $direction): Builder
    {
        return $query->orderBy('is_other_type', $direction);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeAvailable(Builder $query): Builder
    {
        return $query->where('available_quantity', '>', 0)
            ->where('visibility', self::VISIBILITY_ENABLED);
    }

    /**
     * In this solution the thresholds would be automatically set at:
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        $availableQuantity = $this->available_quantity;
        if ($availableQuantity >= self::QUANTITY_STATUS_SUFFICIENT) {
            return Additive::STATUS_SUFFICIENT;
        }
        if ($availableQuantity >= self::QUANTITY_STATUS_MODERATE && $availableQuantity < self::QUANTITY_STATUS_SUFFICIENT) {
            return Additive::STATUS_MODERATE;
        }
        if ($availableQuantity < self::QUANTITY_STATUS_MODERATE && $availableQuantity > self::QUANTITY_STATUS_DEPLETED) {
            return Additive::STATUS_LOW;
        }
        return Additive::STATUS_DEPLETED;
    }

    public function getAppliedQuantityAttribute()
    {
        return $this->total_quantity - $this->available_quantity;
    }

    public function getTypeAttribute()
    {
        return $this->is_other_type ? self::TYPE_OTHER : $this->original['type'];
    }

    public function isPesticide(): bool
    {
        return Arr::get($this->original, 'type') === self::TYPE_PESTICIDE;
    }

    public function getSubLevelForInternalId()
    {
        $dataSource = !empty($this->getDirty()) ? $this->getDirty() : $this->original;

        return Arr::get($dataSource, 'is_other_type', false) ? 
            self::TYPE_OTHER : 
            Arr::get($dataSource, 'type', null);
    }
}
