<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarvestWetWeightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_wet_weights', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('harvest_id')->index()->references('id')->on('harvests')->cascadeOnDelete();
            $table->date('harvested_at');
            $table->smallInteger('type');
            $table->double('weight', 10, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvest_wet_weights');
    }
}
