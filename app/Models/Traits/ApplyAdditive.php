<?php

namespace App\Models\Traits;

use App\Models\{AdditiveLog, AdditiveOperation};
use Illuminate\Database\Eloquent\Relations\{HasMany, MorphMany};

trait ApplyAdditive
{
    public function additiveOperations(): MorphMany
    {
        return $this->morphMany(AdditiveOperation::class, 'target');
    }

    public function additiveLogs(): HasMany
    {
        return $this->hasMany(AdditiveLog::class);
    }

}
