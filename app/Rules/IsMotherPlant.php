<?php

namespace App\Rules;

use App\Models\Plant;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class IsMotherPlant implements Rule
{
    protected string $plantAttribute;
    protected bool $nullable;
    protected array $where;
    protected ?string $attribute;
    protected ?array $value;

    public function __construct(string $attribute, bool $nullable, array $where = [])
    {
        $this->plantAttribute = $attribute;
        $this->where = $where;
        $this->nullable = $nullable;
    }

    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;
        $this->value = array_unique(array_filter(Arr::wrap($value)));

        if ($this->nullable && empty($this->value)) {
            return true;
        }

        $query = Plant::withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])
            ->where('is_mother', true)
            ->whereIn($this->plantAttribute, $this->value);
        foreach ($this->where as $column => $condition) {
            if (is_callable($condition)) {
                $query->where($condition);
            } else {
                $query->where($column, $condition);
            }
        }

        return count($this->value) === $query->count();
    }

    public function message(): string
    {
        $values = implode(', ', $this->value);

        return __(
            'validation.is_mother_plant',
            [
                'plant_attribute' => $this->plantAttribute,
                'attribute' => $this->attribute,
                'values' => $values,
            ]
        );
    }
}
