<?php

namespace App\Synchronizations\Http\Resources\Metrc;

use App\Models\{Disposal, Plant, TraceableModel};
use Illuminate\Http\Request;

/**
 * Class DisposalResource
 * @package App\Synchronizations\Http\Resources\Metrc
 *
 * @property Disposal|TraceableModel  $traceableModel
 */
class DisposalResource extends BaseMetrcResource
{
    protected function list(): self
    {
        $this->httpMethod = Request::METHOD_GET;
        $this->requestUrl = '/plants/v1/inactive';
        return $this;
    }

    protected function destroy()
    {
        $this->httpMethod = Request::METHOD_POST;
        $this->requestUrl = $this->traceableModel->wastable instanceof Plant ? '/plants/v1/destroyplants' : '/plantbatches/v1/destroy';
        $this->requestBody = $this->traceableModel->wastable instanceof Plant ? [
            [
                /*
                Plants
                [
                    {
                        "Id": null,
                        "Label": "ABCDEF012345670000000001",
                        "ReasonNote": "",
                        "ActualDate": "2015-12-15"
                    },
                ]
                */
                'Id' => null,
                'Label' => $this->traceableModel->wastable->sync_code,
                'ReasonNote' => $this->traceableModel->reason,
                'ActualDate' => $this->traceableModel->destroyed_at->format('Y-m-d'),
            ]
        ] : [
            [
                /*
                Propagation
                [
                    {
                        "PlantBatch": "AK-47 Clone 1/31/2017",
                        "Count": 25,
                        "ReasonNote": "",
                        "ActualDate": "2015-12-15"
                    },
                ]
                */
                'PlantBatch' => $this->traceableModel->wastable->batch->sync_code,
                'Count' => intval($this->traceableModel->quantity),
                'ReasonNote' => $this->traceableModel->reason,
                'ActualDate' => $this->traceableModel->destroyed_at->format('Y-m-d'),
            ]
        ];
    }
}
