<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Executable;
use App\Cqrs\Values\AnalysisTypeResult;

class SyncQASampleResultCommand extends Executable
{
    /**
     * @var string|null
     */
    protected $syncCode;

    /**
     * @var string|null
     */
    protected $syncCodeInventory;

    /**
     * @var string|null
     */
    protected $labId;

    /**
     * @var array
     */
    protected $analysisResults = [];

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string[]
     */
    protected $mapping = [
        'syncCodeInventory|string' => 'global_for_inventory_id',
        'syncCode|string' => 'global_id',
        'labId|string' => 'mme_id',
        'status|string' => 'status',
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->map($data);
        $this->init($data, (array)config('qasample.analysesTypes'));
    }

    /**
     * @param array $data
     * @param array $analysesTypes
     */
    private function init(array $data, array $analysesTypes)
    {
        foreach ($analysesTypes as $regulator => $types) {
            if (!is_array($types)) {
                continue;
            }

            foreach ($types as $type) {
                if (!array_key_exists('code', $type)
                    || !array_key_exists('name', $type)
                    || !array_key_exists($type['code'], $data)) {
                    continue;
                }

                $this->analysisResults[$regulator][] = new AnalysisTypeResult(
                    array_merge($type, ['result' => $data[$type['code']]])
                );
            }
        }
    }

    /**
     * @return array
     */
    public function getAnalysisResults(): array
    {
        return $this->analysisResults;
    }

    /**
     * @param $regulator
     * @return array
     */
    public function getAnalysisResultsByRegulator($regulator): array
    {
        if (!isset($this->analysisResults[$regulator])) {
            return [];
        }

        return $this->analysisResults[$regulator];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'labId' => $this->labId,
            'syncCodeInventory' => $this->syncCodeInventory,
            'syncCode' => $this->syncCode,
            'analysisResults' => function () {
                $data = [];

                foreach ($this->analysisResults as $regulator => $analysisResults) {
                    if (!is_array($analysisResults)) {
                        continue;
                    }

                    foreach ($analysisResults as $analysisResult) {
                        if ($analysisResult instanceof AnalysisTypeResult !== true) {
                            continue;
                        }

                        $analysisResults[$regulator][] = $analysisResult->toArray();
                    }
                }

                return $data;
            }
        ];
    }

    /**
     * @return string|null
     */
    public function getSyncCode(): ?string
    {
        return $this->syncCode;
    }

    /**
     * @return string|null
     */
    public function getSyncCodeInventory(): ?string
    {
        return $this->syncCodeInventory;
    }

    /**
     * @return string|null
     */
    public function getLabId(): ?string
    {
        return $this->labId;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return (string)$this->status;
    }
}
