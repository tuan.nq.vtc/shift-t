<?php

namespace App\Http\Controllers;

use App\Http\Parameters\Criteria;
use App\Models\RoomType;
use App\Services\RoomTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

/**
 * Class RoomTypeController
 * @package App\Http\Controllers
 */
class RoomTypeController extends CRUDController
{
    public function __construct(RoomTypeService $service, Request $request)
    {
        parent::__construct($service, $request);
    }

    public function getCreateRules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:room_types,name,NULL,id,deleted_at,NULL,account_holder_id,' . $this->request->get('account_holder_id', NULL)
            ],
            'category' => ['required', new In(RoomType::getCategories())],
        ];
    }

    public function getUpdateRules(string $id): array
    {
        return [
            'name' => ['sometimes',
                'required',
                'string',
                'min:3',
                'max:150',
                'unique:room_types,name,' . $id . ',id,deleted_at,NULL,account_holder_id,' . $this->request->get('account_holder_id', NULL)
            ],
            'status' => ['sometimes', 'required', 'boolean'],
            'category' => ['sometimes', 'required', new In(RoomType::getCategories())],
        ];
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json($this->service->getAvailableItems(Criteria::createFromRequest($this->request)));
    }
}
