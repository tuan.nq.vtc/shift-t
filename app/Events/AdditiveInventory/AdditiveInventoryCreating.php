<?php

namespace App\Events\AdditiveInventory;

use App\Events\Event;
use App\Models\AdditiveInventory;

class AdditiveInventoryCreating extends Event
{
    /**
     * @var AdditiveInventory
     */
    public AdditiveInventory $additiveInventory;

    /**
     * @param AdditiveInventory $additiveInventory
     */
    public function __construct(AdditiveInventory $additiveInventory)
    {
        $this->additiveInventory = $additiveInventory;
    }
}
