#### Version 1
    - Trace & CRM

    
#### Architecture
    - Laravel passport for authentication

    - Connection between services
    - CI/CD | Pipeline
    - Pull request - Sprint
    

#### DB
    - UUID for tables - multi DB instance
    - MariaDB - Enterprise
    - Perform of retrieve/insert

    - Table of hundreds col -> Scale
    - Decentralize architure
    - NoSQL: mongoDB - python (used for analyzing & reporting)

#### API
    - Connection problem between services
    - JSON format -> filter/sort/pagination
    - Mock API with mock data for frontend
    - Error format for frontend

#### Testing
    - Automation testing
    - Feature testing
    - BDD - TDD
    - Documentation
    - 

#### Documentation
    - Document services (readme file, ...)
    - Document source code (docblock)
    - Open API
    - Tools: Confluence


#### Hybrid microservices system
    - PHP - Mariadb
    - Python - MongoDB -> Analyze & Report


## 06/25

### Laravel Passport
- JWT
- Grant type: Passcode?
- Gateway middle -> send request with JWT to `passport`

### UUID


