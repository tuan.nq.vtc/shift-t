<?php

namespace App\Models;

use App\Models\Traits\{ApplyAdditive,
    BelongToBatch,
    BelongToGrowCycle,
    BelongToStrain,
    Boots\RoomStatistic,
    HasInternalId,
    PullPlantHandler,
    Scope\DatetimeFilterable,
    Scope\GrowStatusFilterable,
    Scope\IdsFilterable,
    Scope\LicenseFilterable,
    Scope\SourceTypeFilterable,
    Scope\SyncCodesFilterable,
    StoreInRoom,
    StoreInSubroom};
use Carbon\Carbon;
use Illuminate\Database\Eloquent\{Builder,
    Relations\BelongsTo,
    Relations\HasMany,
    Relations\HasOne,
    Relations\MorphMany,
    SoftDeletes
};

/**
 * Class Plant
 * @property string sync_code
 *
 * @package App\Models
 */
class Plant extends TraceableModel
{
    use BelongToBatch;
    use BelongToGrowCycle;
    use BelongToStrain;
    use DatetimeFilterable;
    use GrowStatusFilterable;
    use IdsFilterable;
    use LicenseFilterable;
    use PullPlantHandler;
    use SoftDeletes;
    use SourceTypeFilterable;
    use StoreInRoom;
    use StoreInSubroom;
    use SyncCodesFilterable;
    use ApplyAdditive;
    use RoomStatistic;
    use HasInternalId;

    protected $fillable = [
        'strain_id',
        'is_mother',
        'mother_code',
        'room_id',
        'subroom_id',
        'source_type', // #todo declare the values as const
        'grow_status',
        'grow_cycle_id',
        'plant_group_id',
        'planted_at',
        'propagation_id',
        'est_harvested_at',
        'harvested_at',
        'is_manifested',
        'destroyed',
        'destroy_reason',
        'destroy_at',
        'deleted_at',
        'info',
        'sync_code',
        'synced_at',
    ];

    protected $dates = [
        'planted_at',
        'est_harvested_at',
        'harvested_at',
        'destroy_at',
        'moved_room_at',
        'synced_at'
    ];

    protected $casts = [
        'is_mother' => 'boolean',
        'is_manifested' => 'boolean',
        'destroyed' => 'boolean',
        'harvested' => 'boolean',
        'info' => 'array',
        'planted_at' => 'date:Y-m-d',
        'est_harvested_at' => 'date:Y-m-d',
        'harvested_at' => 'date:Y-m-d',
        'destroy_at' => 'date:Y-m-d',
        'moved_room_at' => 'date:Y-m-d',
    ];

    protected $appends = ['age'];

    protected string $orderByColumn = 'planted_at';

    protected array $traceable = ['is_mother'];

    protected static function booted(): void
    {
        parent::booted();

        static::creating(
            function (self $model): void {
                $model->moved_room_at = Carbon::now();
            }
        );

        static::saving(
            function (self $model): void {
                if ($model->isDirty('room_id')) {
                    $model->moved_room_at = Carbon::now();
                }
            }
        );
    }

    public function plantGroup(): BelongsTo
    {
        return $this->belongsTo(PlantGroup::class);
    }

    public function disposals(): MorphMany
    {
        return $this->morphMany(Disposal::class, 'wastable');
    }

    public function mother(): BelongsTo
    {
        return $this->belongsTo(Plant::class, 'mother_code', 'sync_code');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Plant::class, 'mother_code', 'sync_code');
    }

    /**
     * a plant must be created from a propagation
     */
    public function motherPropagation(): BelongsTo
    {
        return $this->belongsTo(Propagation::class, 'propagation_id', 'id');
    }

    /**
     * a mother plant could have many propagations
     */
    public function propagations(): HasMany
    {
        return $this->hasMany(Propagation::class, 'mother_code', 'sync_code');
    }

    public function additiveLogs(): HasMany
    {
        return $this->hasMany(AdditiveLog::class, 'plant_id', 'id');
    }

    public function harvestedPlants(): HasOne
    {
        return $this->hasOne(HarvestPlant::class);
    }

    public function scopePropagationId(Builder $query, string $propagationId): Builder
    {
        return $query->where($this->getTable().'.propagation_id', 'LIKE', "%{$propagationId}%");
    }

    public function scopeHarvestId(Builder $query, $harvestId): Builder
    {
        return $query->where($this->getTable().'.harvest_id', $harvestId);
    }

    public function scopeHarvested(Builder $query, $isHarvested = false): Builder
    {
        return $query->whereIn(
            $this->getTable().'.grow_status',
            [SeedToSale::GROW_STATUS_HARVESTING, SeedToSale::GROW_STATUS_HARVESTED],
            'and',
            !$isHarvested
        );
    }

    public function scopeIsMother(Builder $query, bool $isMother = true): Builder
    {
        return $query->where($this->getTable().'.is_mother', $isMother);
    }

    public function scopeHideHarvested(Builder $query): Builder
    {
        return $query->whereNull($this->getTable().'.harvested_at');
    }

    public function scopePlantedAt(Builder $query, $fromDate, $toDate = null): Builder
    {
        return $this->buildQueryDateRanger($query, $fromDate, $toDate, 'planted_at');
    }

    public function scopeAvailable(Builder $query): Builder
    {
        return $query->where($this->getTable().'.is_manifested', false)
            ->where($this->getTable().'.destroyed', false);
    }

    public function scopeIsManifested(Builder $query, bool $value): Builder
    {
        return $query->where($this->getTable().'.is_manifested', $value);
    }

    public function scopeDestroyed(Builder $query, bool $value): Builder
    {
        return $query->where($this->getTable().'.destroyed', $value);
    }

    public function scopeOrderByAge(Builder $query, $direction): Builder
    {
        return $query->orderBy($this->getTable().'.planted_at', $direction);
    }

    public function scopeSearch(Builder $query, string $search): Builder
    {
        return $query->where(
            fn(Builder $builder) => $builder->orWhereHas(
                'strain',
                fn(Builder $builder) => $builder->where(
                    $this->strain()->getRelated()->getTable().'.name',
                    'LIKE',
                    '%'.$search.'%'
                )
            )->orWhereHas(
                'growCycle',
                fn(Builder $builder) => $builder->where(
                    $this->growCycle()->getRelated()->getTable().'.name',
                    'LIKE',
                    '%'.$search.'%'
                )
            )->orWhere($this->getTable().'.sync_code', 'LIKE', '%'.$search.'%')
        );
    }

    /**
     * @param Builder $query
     * @param         $growStatuses
     *
     * @return Builder
     */
    public function scopeGrowStatuses($query, ...$growStatuses)
    {
        return $query->whereIn($this->getTable() . '.grow_status', $growStatuses);
    }

    public function getAgeAttribute(): string
    {
        if (!$this->planted_at) {
            return '';
        }
        if (Carbon::parse($this->planted_at)->isToday()) {
            return "0 day";
        }

        return $this->planted_at->longAbsoluteDiffForHumans();
    }

    public function isDestroyed(): bool
    {
        return (bool)$this->destroyed;
    }

    public function isHarvested(): bool
    {
        return $this->grow_status === SeedToSale::GROW_STATUS_HARVESTED;
    }

    public function scopeEstHarvestedAt(Builder $query, $firstValue, $secondValue = null)
    {
        $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'est_harvested_at');
    }

    public function scopeHarvestedAt(Builder $query, $firstValue, $secondValue = null)
    {
        $this->buildQueryDateRanger($query, $firstValue, $secondValue, 'harvested_at');
    }

    public function isAvailable(): bool
    {
        return !$this->isDestroyed() && !$this->isHarvested() && !$this->is_manifested;
    }
   
    public function isTraceUpdatable(): bool
    {
        if($this->license->isRegulator(State::REGULATOR_CCRS)) {
            $this->traceable[] = 'grow_status';
            $this->traceable[] = 'strain_id';
            $this->traceable[] = 'room_id';
            return !empty(array_intersect(array_keys($this->changes), $this->traceable));
        } 
        return parent::isTraceUpdatable();
    }
    
}
