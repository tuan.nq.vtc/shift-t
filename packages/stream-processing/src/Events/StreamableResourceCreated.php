<?php

namespace Bamboo\StreamProcessing\Events;

use App\Events\Event;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;

class StreamableResourceCreated extends Event
{
    /**
     * @var StreamableResourceInterface
     */
    public StreamableResourceInterface $streamableResource;

    public function __construct(StreamableResourceInterface $streamableResource)
    {
        $this->streamableResource = $streamableResource;
    }
}
