<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{Manifest, ManifestItem};
use App\Services\{InventoryService, ManifestService, PlantService, PropagationService, QASampleService};
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;
use Throwable;

/**
 * @package App\Http\Controllers
 */
class ManifestController extends Controller
{
    /**
     * @var ManifestService
     */
    protected ManifestService $service;
    protected QASampleService $qaSampleService;
    protected PlantService $plantService;
    protected InventoryService $inventoryService;
    protected PropagationService $propagationService;
    /**
     * @var Request
     */
    protected Request $request;

    public function __construct(
        ManifestService    $service,
        QASampleService    $qaSampleService,
        PlantService       $plantService,
        PropagationService $propagationService,
        InventoryService   $inventoryService,
        Request            $request
    )
    {
        $this->qaSampleService = $qaSampleService;
        $this->plantService = $plantService;
        $this->propagationService = $propagationService;
        $this->inventoryService = $inventoryService;
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * @param string $method
     * @return JsonResponse
     * @throws GeneralException
     */
    public function listByMethod(string $method): JsonResponse
    {
        $methodMapping = [
            'inbound' => Manifest::METHOD_INBOUND,
            'outbound' => Manifest::METHOD_OUTBOUND,
        ];
        if (!isset($methodMapping[$method])) {
            throw new GeneralException(__('exceptions.manifest.invalid_method'));
        }
        $criteria = Criteria::createFromRequest($this->request);
        $criteria->addFilter('method', $methodMapping[$method]);
        return response()->json($this->service->list($criteria));
    }

    /**
     * @param string $method
     * @return JsonResponse
     * @throws GeneralException
     */
    public function getByMethod(string $method): JsonResponse
    {
        $methodMapping = [
            'inbound' => Manifest::METHOD_INBOUND,
            'outbound' => Manifest::METHOD_OUTBOUND,
        ];
        if (!isset($methodMapping[$method])) {
            throw new GeneralException(__('exceptions.manifest.invalid_method'));
        }
        $criteria = Criteria::createFromRequest($this->request);
        $criteria->addFilter('method', $methodMapping[$method]);
        return response()->json($this->service->getOpenItems($criteria));
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function show(string $id)
    {
        return response()->json($this->service->get($id));
    }

    /**
     * @param string|null $id
     * @return JsonResponse
     * @throws GeneralException
     * @throws Throwable
     * @throws ValidationException
     */
    public function save(?string $id = null)
    {
        $rules = [
            'type' => ['required', 'string'],
            'transport_type' => ['required', 'string', new In(Manifest::TRANSPORT_TYPES)],
            'client_id' => ['required', 'uuid',],
            'travel_route' => ['nullable',],
            'est_departure_date' => ['sometimes', Rule::requiredIf(function () {
                return in_array($this->request->get('transport_type'), [
                    Manifest::TRANSPORT_TYPE_DELIVERY,
                    Manifest::TRANSPORT_TYPE_LICENSED_TRANSPORTER,
                ]);
            }), 'date'],
            'est_arrival_date' => ['sometimes', Rule::requiredIf(function () {
                return in_array($this->request->get('transport_type'), [
                    Manifest::TRANSPORT_TYPE_DELIVERY,
                    Manifest::TRANSPORT_TYPE_LICENSED_TRANSPORTER,
                ]);
            }), 'date'],
            'items' => ['array',],
            'items.*.type' => ['required', 'string',],
            'items.*.resource_id' => ['required',],
            'items.*.is_for_extraction' => ['boolean',],
            'items.*.is_sample' => ['boolean',],
            'items.*.price' => ['numeric',],
            'items.*.quantity' => ['numeric',],
        ];
        switch ($this->request->get('transport_type')) {
            case Manifest::TRANSPORT_TYPE_DELIVERY:
            case Manifest::TRANSPORT_TYPE_PICKUP:
                $rules = array_merge($rules, [
                    'driver_id' => ['required', 'uuid',],
                    'driver_helper_id' => ['nullable', 'uuid',],
                    'vehicle_id' => ['required', 'uuid',],
                ]);
                break;
            case Manifest::TRANSPORT_TYPE_LICENSED_TRANSPORTER:
                $rules = array_merge($rules, [
                    'transporter_id' => ['required', 'uuid',],
                ]);
                break;
        }

        $manifestData = $this->validate($this->request, $rules);
        if ($id) {
            /** @var Manifest $manifest */
            $manifest = $this->service->update($id, $manifestData);
        } else {
            $manifest = $this->service->create($manifestData);
        }

        return response()->json($manifest, $id? Response::HTTP_ACCEPTED : Response::HTTP_OK);
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function addItems(string $id): JsonResponse
    {
        $rules = [
            'items' => ['required', 'array',],
            'items.*.type' => ['required', 'string', new In(ManifestItem::TYPES)],
            'items.*.resource_id' => ['required', 'uuid',],
            'items.*.is_for_extraction' => ['boolean',],
            'items.*.is_sample' => ['boolean',],
            'items.*.price' => ['numeric',],
            'items.*.quantity' => ['numeric',],
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->addItems($id, $this->request->get('items'))
        );
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkPublish(): JsonResponse
    {
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid']
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->bulkPublish($this->request->get('ids'))
        );
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkVoid(): JsonResponse
    {
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid']
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->bulkVoid($this->request->get('ids'))
        );
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkCancel(): JsonResponse
    {
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid']
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->bulkCancel($this->request->get('ids'))
        );
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkDelete(): JsonResponse
    {
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid']
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->bulkDelete($this->request->get('ids'))
        );
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkReceive(): JsonResponse
    {
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid']
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->bulkReceive($this->request->get('ids'))
        );
    }

    /**
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function bulkReject(): JsonResponse
    {
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid']
        ];

        $this->validate($this->request, $rules);
        return response()->json(
            $this->service->bulkReject($this->request->get('ids'))
        );
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws Throwable
     */
    public function publish($id): JsonResponse
    {
        return response()->json($this->service->publish($id));
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function markInTransit(string $id): JsonResponse
    {
        return \response()->json($this->service->markInTransit($id));
    }
}
