<?php

namespace App\Models;

use App\Models\Traits\Scope\NameFilterable;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

/**
 * @package App\Models
 */
class StateCategory extends BaseModel
{
    use NameFilterable;

    public const TYPE_UNLOTTED = 'unlotted';
    public const TYPE_SOURCE = 'source';
    public const TYPE_END = 'end';

    protected $fillable = [
        'regulator',
        'type',
        'state_code',
        'parent_id',
        'name',
        'code',
        'is_strain_required',
        'is_serving_required',
        'is_weight_per_unit_required',
        'is_volume_per_unit_required',
        'uom',
    ];

    /**
     * @return BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class, 'state_code', 'code');
    }

    /**
     * Relation with Inventory Type
     *
     * @return HasMany
     */
    public function inventoryTypes(): HasMany
    {
        return $this->hasMany(InventoryType::class, 'state_category_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * Self reference relation
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }
}
