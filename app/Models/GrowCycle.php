<?php

namespace App\Models;

use App\Events\GrowCycle\GrowCycleSaved;
use App\Models\Traits\{ApplyAdditive,
    BelongToLicense,
    HasInternalId,
    Scope\DatetimeFilterable,
    Scope\GrowStatusFilterable,
    Scope\IdsFilterable,
    Scope\LicenseFilterable,
    Scope\NameFilterable,
    WithoutEvents
};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsToMany, Relations\HasMany, SoftDeletes};
use Illuminate\Support\Collection;

/**
 * Class GrowCycle
 * @property string grow_status
 * @package App\Models
 */
class GrowCycle extends BaseModel
{
    use IdsFilterable;
    use ApplyAdditive;
    use DatetimeFilterable;
    use GrowStatusFilterable;
    use LicenseFilterable;
    use NameFilterable;
    use SoftDeletes;
    use WithoutEvents;
    use HasInternalId;
    use BelongToLicense;

    protected $appends = [
        'quantity',
        'rooms',
        'strains'
    ];

    protected $fillable = [
        'name',
        'license_id',
        'planted_at',
        'est_harvested_at',
        'grow_status',
    ];

    protected $dates = [
        'planted_at',
        'est_harvested_at',
    ];

    protected $hidden = ['plants'];

    protected $dispatchesEvents = [
        'saved' => GrowCycleSaved::class,
    ];

    public function plantGroups(): BelongsToMany
    {
        return $this->belongsToMany(PlantGroup::class);
    }

    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class);
    }

    public function getQuantityAttribute(): int
    {
        return $this->plants()->count();
    }

    public function scopeAvailable(Builder $query, bool $isDeleted = false): Builder
    {
        $query->withCount(['plants'])->having('plants_count', '>', 0);
        return $isDeleted ? $query->whereNull('deleted_at') : $query->whereNotNull('deleted_at');
    }

    public function scopePlantedAt(Builder $query, $fromDate, $toDate = null): Builder
    {
        return $this->buildQueryDateRanger($query, $fromDate, $toDate, 'planted_at');
    }

    public function scopeEstHarvestedAt(Builder $query, $fromDate, $toDate = null)
    {
        return $this->buildQueryDateRanger($query, $fromDate, $toDate, 'est_harvested_at');
    }

    public function scopeStrain(Builder $query, ...$strainId): Builder
    {
        return $query->whereHas(
            'plants',
            fn(Builder $plantQuery) => $plantQuery->whereIn('strain_id', $strainId)
        );
    }

    public function scopeSourceType(Builder $query, ...$sourceType): Builder
    {
        return $query->whereHas(
            'plants',
            function (Builder $plantQuery) use ($sourceType) {
                $plantQuery->whereIn('source_type', $sourceType);
            }
        );
    }

    public function scopeOrderByQuantity(Builder $query, $direction): Builder
    {
        return $query->withCount('plants')->orderBy('plants_count', $direction);
    }

    public function scopeOrderByEstHarvestAt(Builder $query, $direction): Builder
    {
        return $query->orderBy('planted_at', $direction);
    }

    /**
     * Search by:
     *  - grow cycle name
     *  - propagation mother code
     *  - propagation batch sync code
     *  - propagation strain
     *  - propagation source
     *  - propagation source and grow status name
     *
     * @param Builder $query
     * @param string $search
     *
     * @return Builder
     */
    public function scopeSearch(Builder $query, string $search): Builder
    {
        return $query->where(
            fn(Builder $subQuery) => $subQuery->orWhere('name', 'LIKE', '%' . $search . '%')
                ->orWhere('grow_status', 'LIKE', '%' . $search . '%')
                ->orWhereHas(
                    'plantGroups.propagation',
                    fn(Builder $propagationQuery) => $propagationQuery->where('mother_code', 'LIKE', '%' . $search . '%')
                        ->orWhereHas(
                            'batch',
                            fn(Builder $batchQuery) => $batchQuery->where('sync_code', 'LIKE', '%' . $search . '%')
                        )
                        ->orWhereHas(
                            'strain',
                            fn(Builder $strainQuery) => $strainQuery->where('name', 'LIKE', '%' . $search . '%')
                        )
                )
        );
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->plants()->count() === 0;
    }


    public function getRoomsAttribute(): Collection
    {
        $rooms = collect();
        if ($this->relationLoaded('plantGroups')) {
            foreach ($this->plantGroups as
                     $plantGroup) {
                if ($plantGroup->room instanceof Room) {
                    $rooms->put($plantGroup->room->id, $plantGroup->room);
                }
            }
        }

        return $rooms->values();
    }

    public function scopeRoom($query, ...$room)
    {
        return $query->whereHas(
            'plantGroups',
            fn(Builder $plantGroupQuery) => $plantGroupQuery->whereIn('room_id', $room)
        );
    }

    public function getStrainsAttribute(): Collection
    {
        $strains = collect();
        if ($this->relationLoaded('plantGroups')) {
            foreach ($this->plantGroups as $plantGroup) {
                if ($plantGroup->strain instanceof Strain) {
                    $strains->put($plantGroup->strain->id, $plantGroup->strain);
                }
            }
        }

        return $strains->values();
    }
}
