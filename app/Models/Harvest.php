<?php

namespace App\Models;

use App\Models\Traits\{BelongToStrain,
    Boots\RoomStatistic,
    HasInternalId,
    Scope\HarvestGroupFilterable,
    Scope\LicenseFilterable,
    StoreInRoom,
    WithoutTraceableModelEvents
};
use App\Synchronizations\Contracts\TraceableModelInterface;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany, HasManyThrough, MorphMany, MorphOne};
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\Harvest
 *
 * @property Batch batch
 * @property HarvestGroup group
 * @property Collection|Plant[] plants
 * @property Collection|HarvestPlant[] harvestPlants
 * @property Room room
 * @property Carbon start_at
 * @property Carbon finished_at
 * @property float flower_wet_weight
 * @property float material_wet_weight
 * @property float waste_wet_weight
 * @property float flower_dry_weight
 * @property float material_dry_weight
 * @property float waste_dry_weight
 * @property InventoryType flowerInventoryType
 * @property InventoryType materialInventoryType
 * @property Room flowerRoom
 * @property Room materialRoom
 * @property Room wasteRoom
 * @property string flower_room_id
 * @property string material_room_id
 * @property string waste_room_id
 * @property License license
 * @property Strain strain
 * @property string strain_id
 */
class Harvest extends BaseModel implements TraceableModelInterface
{
    use SoftDeletes;
    use BelongToStrain;
    use StoreInRoom;
    use LicenseFilterable;
    use HarvestGroupFilterable;
    use WithoutTraceableModelEvents;
    use RoomStatistic;
    use HasInternalId;

    public const SYNC_ACTION_CONFIRM_DRY = 'confirmDry';
    public const SYNC_ACTION_CREATE_PACKAGE = 'createPackage';
    public const SYNC_ACTION_FINALIZE = 'finalize';

    protected $fillable = [
        'license_id',
        'strain_id',
        'harvest_group_id',
        'room_id',
        'flower_wet_weight',
        'material_wet_weight',
        'waste_wet_weight',
        'flower_dry_weight',
        'waste_dry_weight',
        'material_dry_weight',
        'flower_room_id',
        'material_room_id',
        'waste_room_id',
        'flower_inventory_type_id',
        'material_inventory_type_id',
        'start_at',
        'finished_at',
        'sync_status',
        'synced_at',
    ];
    protected $casts = [
        'flower_wet_weight' => 'float',
        'flower_dry_weight' => 'float',
        'waste_wet_weight' => 'float',
        'waste_dry_weight' => 'float',
        'material_wet_weight' => 'float',
        'material_dry_weight' => 'float',
    ];

    protected $dates = [
        'start_at',
        'finished_at'
    ];

    protected $hidden = ['plants'];

    protected $dispatchesEvents = [];

    public function getMappings(): ?array
    {
        // TODO: Implement getMappings() method.
        return null;
    }

    public function batch(): MorphOne
    {
        return $this->morphOne(Batch::class, 'source');
    }

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function group(): BelongsTo
    {
        return $this->belongsTo(HarvestGroup::class, 'harvest_group_id', 'id');
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }

    public function harvestPlants(): HasMany
    {
        return $this->hasMany(HarvestPlant::class);
    }

    public function plants(): HasManyThrough
    {
        return $this->hasManyThrough(
            Plant::class,
            HarvestPlant::class,
            'harvest_id',
            'id',
            'id',
            'plant_id',
        );
    }

    public function inventories(): MorphMany
    {
        return $this->morphMany(Inventory::class, 'source');
    }

    public function wetWeights(): HasMany
    {
        return $this->hasMany(HarvestWetWeight::class);
    }

    public function disposals(): MorphMany
    {
        return $this->morphMany(Disposal::class, 'wastable');
    }

    public function flowerRoom(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'flower_room_id', 'id');
    }

    public function materialRoom(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'material_room_id', 'id');
    }

    public function flowerInventoryType(): BelongsTo
    {
        return $this->belongsTo(InventoryType::class, 'flower_inventory_type_id', 'id');
    }

    public function materialInventoryType(): BelongsTo
    {
        return $this->belongsTo(InventoryType::class, 'material_inventory_type_id', 'id');
    }

    public function strain(): BelongsTo
    {
        return $this->belongsTo(Strain::class, 'strain_id', 'id');
    }

    public function updateSyncStatus(int $status): TraceableModelInterface
    {
        $this->update(['sync_status' => $status, 'synced_at' => Carbon::now(),]);

        return $this;
    }

    public function updateSyncCode(string $code): TraceableModelInterface
    {
        return $this;
    }
}
