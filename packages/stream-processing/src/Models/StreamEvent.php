<?php

namespace Bamboo\StreamProcessing\Models;

use Bamboo\StreamProcessing\Contracts\StreamEventInterface;

class StreamEvent implements StreamEventInterface
{
    private string $id;
    private array $payload;
    private array $metadata;
    private array $headers;

    public function __construct(string $id, array $payload, array $metadata = [], array $headers = [])
    {
        $this->id = $id;
        $this->payload = $payload;
        $this->metadata = $metadata;
        $this->headers = $headers;
    }
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): void
    {
        $this->metadata = $metadata;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getSender(): string
    {
        return array_key_exists('sender', $this->headers) ? $this->headers['sender'] : '';
    }

    /**
     * @param string $sender
     */
    public function setSender(string $sender): void
    {
        $this->headers['sender'] = $sender;
    }
}
