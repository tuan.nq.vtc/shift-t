<?php

use App\Models\{Batch, Disposal, Inventory, InventoryType, LabTest, LicenseResource, Plant, Room, Strain};

return [
    // TODO: Add mappings
    LicenseResource::LEAF_AREA_RESOURCE => [
        'model' => Room::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_STRAIN_RESOURCE => [
        'model' => Strain::class,
        'endpoints' => [
        ]
    ],
//    LicenseResource::LEAF_BATCH_RESOURCE => [
//        'model' => Batch::class,
//        'endpoints' => [
//        ]
//    ],
    LicenseResource::LEAF_PLANT_RESOURCE => [
        'model' => Plant::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_INVENTORY_TYPE_RESOURCE => [
        'model' => InventoryType::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_INVENTORY_LOT_RESOURCE => [
        'model' => Inventory::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_INVENTORY_TRANSFER_RESOURCE => [
        'model' => '',
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_LAB_RESULT_RESOURCE => [
        'model' => LabTest::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_WASTE_RESOURCE => [
        'model' => Disposal::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::LEAF_SALES_RESOURCE => [
        'model' => '',
        'endpoints' => [
        ]
    ],

    LicenseResource::METRC_LOCATION_RESOURCE => [
        'model' => Room::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::METRC_STRAIN_RESOURCE => [
        'model' => Strain::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::METRC_INVENTORY_ITEM_RESOURCE => [
        'model' => InventoryType::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::METRC_INVENTORY_PACKAGE_RESOURCE => [
        'model' => Inventory::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::METRC_LAB_RESULT_RESOURCE => [
        'model' => '',
        'endpoints' => [
        ]
    ],
    LicenseResource::METRC_PLANTING_RESOURCE => [
        'model' => Batch::class,
        'endpoints' => [
            'plantbatches/v1/active',
            'plantbatches/v1/inactive',
        ]
    ],
    LicenseResource::METRC_PLANT_RESOURCE => [
        'model' => Plant::class,
        'endpoints' => [
        ]
    ],
    LicenseResource::METRC_WASTE_RESOURCE => [
        'model' => Disposal::class,
        'endpoints' => [
            'plants/v1/inactive',
            // TODO: need clear logic sync
            // 'plantbatches/v1/active',
            // 'plantbatches/v1/inactive',
        ]
    ],
    LicenseResource::METRC_SALES_RESOURCE => [
        'model' => '',
        'endpoints' => [
        ]
    ],
];
