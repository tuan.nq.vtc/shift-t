<?php

namespace Bamboo\StreamProcessing\Contracts;

interface StreamableResourceInterface
{
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';

    /**
     * @return string
     */
    public function getTopic(): string;

    /**
     * @return array
     */
    public function getStreamEventPayload(): array;

    /**
     * @return bool
     */
    public function isStreamUpdatable(): bool;
}
