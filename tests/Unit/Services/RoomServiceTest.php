<?php

namespace Tests\Unit\Services;

use App\Http\Parameters\Criteria;
use App\Models\Plant;
use App\Models\Propagation;
use App\Models\Room;
use App\Models\SeedToSale;
use App\Models\Strain;
use App\Models\Subroom;
use App\Services\RoomService;
use Tests\Concerns\Traceable;
use Tests\TestCase;

class RoomServiceTest extends TestCase
{
    use Traceable;

    protected static string $modelName = Room::class;

    protected function getService(): RoomService
    {
        return app(RoomService::class);
    }

    /**
     * @test
     */
    public function it_can_get_stats_in_plant_purpose_list()
    {
        $this->withoutTraceableEvents();
        $room = factory(static::$modelName)->create(['purpose' => Room::PURPOSE_PLANT]);
        $subrooms = factory(Subroom::class, rand(1, 5))->create(['room_id' => $room->id]);

        $numberOfPlants = 0;
        $expectedTotalStrains = rand(3, 5);
        $strains = factory(Strain::class, $expectedTotalStrains)->create();

        $expectedTotalGrowStatuses = rand(5, 10);

        foreach ($subrooms as $subroom) {
            foreach ($strains as $strain) {
                $propagation = factory(Propagation::class)->create(
                    [
                        'license_id' => $room->license_id,
                        'subroom_id' => $subroom->id,
                        'strain_id' => $strain->id,
                    ]
                );

                foreach (SeedToSale::GROW_CYCLE_GROW_STATUSES as $growStatus) {
                    $numberOfPlants += $totalPlants = rand(1, 10);
                    factory(Plant::class, $totalPlants)->create(
                        [
                            'license_id' => $room->license_id,
                            'subroom_id' => $subroom->id,
                            'strain_id' => $strain->id,
                            'grow_status' => $growStatus->id,
                            'propagation_id' => $propagation->id
                        ]
                    );
                }
            }
        }

        // execute
        $result = $this->getService()->list((new Criteria())->setFilters(['purpose' => Room::PURPOSE_PLANT]));
        $items = $result->items();
        $expectedItem = array_shift($items);

        $this->assertCount($expectedTotalGrowStatuses, $expectedItem->grow_statuses);
        $this->assertCount($expectedTotalStrains, $expectedItem->strains);
        $this->assertEquals($numberOfPlants, $expectedItem->number_of_plants);
    }
}
