<?php

namespace App\Integrations\FileGenerators\FileStrategies;

abstract class CsvFileStrategy extends BaseFileStrategy
{
    private const DEFAULT_DELIMITER = ',';
    private const DEFAULT_ENCLOSURE = '"';
    private int $maxRowLength = 0;

    private function convertArrayToCsvString(
        $data,
        $delimiter = self::DEFAULT_DELIMITER,
        $enclosure = self::DEFAULT_ENCLOSURE
    ): string {
        $contents = '';
        $handle = fopen('php://temp', 'r+');
        foreach ($data as $row) {
            fputcsv($handle, $this->normalizeRow($row), $delimiter, $enclosure);
        }
        rewind($handle);
        while (!feof($handle)) {
            $contents .= fread($handle, 8192);
        }
        fclose($handle);
        return $contents;
    }

    public function getContent(): string
    {
        $content = [];

        $header = $this->getHeaders();
        if (!empty($header)) {
            $this->multiPush($content, $header);
        }

        foreach ($this->data as $record) {
            $row = $this->getRow($record);
            if (!empty($row)) {
                $this->multiPush($content, $row);
            }
        }

        $footer = $this->getFooters();
        if (!empty($footer)) {
            $this->multiPush($content, $footer);
        }

        return $this->convertArrayToCsvString($content);
    }

    private function isMultiArray(array $data): bool
    {
        rsort($data);
        return isset($data[0]) && is_array($data[0]);
    }

    private function multiPush(array &$content, array $element): array
    {
        if ($this->isMultiArray($element)) {
            foreach ($element as $e) {
                $this->setMaxRowLength($e);
                $content[] = $e;
            }
        } else {
            $this->setMaxRowLength($element);
            $content[] = $element;
        }

        return $content;
    }

    private function setMaxRowLength(array $row): void
    {
        if ($this->maxRowLength < count($row)) {
            $this->maxRowLength = count($row);
        }
    }

    public function getContentType(): string
    {
        return 'text/csv';
    }

    protected function normalizeRow(array $row): array
    {
        return (count($row) < $this->maxRowLength) ? array_pad($row, $this->maxRowLength, '') : array_slice(
            $row,
            0,
            $this->maxRowLength
        );
    }


    abstract public function getHeaders(): array;

    abstract public function getFooters(): array;

    abstract public function getRow($model): array;
}
