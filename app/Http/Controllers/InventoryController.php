<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Parameters\Criteria;
use App\Models\{Inventory, InventoryHistory};
use App\Rules\{IsDisposalRoom, IsInventoryRoom, SubroomBelongToLicense};
use App\Services\{InventoryService, RoomService, SourceProductService};
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;
use Throwable;

/**
 * Class InventoryController
 * @package App\Http\Controllers
 *
 * @property InventoryService $service
 */
class InventoryController extends Controller
{
    /**
     * @var InventoryService
     */
    protected InventoryService $service;

    /**
     * @var SourceProductService
     */
    protected SourceProductService $sourceProductService;

    protected RoomService $roomService;

    /**
     * @var Request
     */
    protected Request $request;

    public function __construct(InventoryService $service, SourceProductService $sourceProductService, RoomService $roomService, Request $request)
    {
        $this->service = $service;
        $this->sourceProductService = $sourceProductService;
        $this->request = $request;
        $this->roomService = $roomService;
    }

    /**
     * @return JsonResponse
     */
    public function listUnlotted(): JsonResponse
    {
        return response()->json(
            $this->service->listUnlotted(Criteria::createFromRequest($this->request))
        );
    }

    /**
     * @return JsonResponse
     */
    public function listLotted(string $type): JsonResponse
    {
        return response()->json(
            $this->service->listLotted(Criteria::createFromRequest($this->request), $type)
        );
    }

    public function adjust(string $id): JsonResponse
    {
        /** @var Inventory $inventory */
        $inventory = $this->service->get($id);
        return response()->json(
            $this->service->adjust($inventory,
                $this->validate($this->request, [
                    'qty' => ['required', 'numeric', 'lte:' . $inventory->qty_on_hand],
                    'reason' => ['required', 'string', Rule::in(InventoryHistory::REASONS)],
                    'comment' => ['sometimes', 'nullable']
                ])
            )
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     *
     * @throws Throwable
     * @throws GeneralException
     * @throws ValidationException
     */
    public function createLots(string $id): JsonResponse
    {
        $inventory = $this->service->get($id);
        $licenseId = $this->request->get('license_id');
        $roomId = $this->request->get('room_id');
        $data = $this->validate(
            $this->request,
            [
                'weight_per_lot' => ['required', 'numeric', 'gt:0'],
                'number_of_lot' => [
                    'required',
                    'integer',
                    'gte:1',
                ],
                'room_id' => [
                    'required',
                    'uuid',
                    new IsInventoryRoom($licenseId),
                ],
                'subroom_id' => [
                    'nullable',
                    'uuid',
                    new SubroomBelongToLicense($roomId, $licenseId),
                ],
                'product_id' => [
                    'required',
                    'uuid',
                    'exists:App\Models\SourceProduct,id,deleted_at,NULL,license_id,' . $licenseId,
                ],
            ]
        );

        return response()->json(
            $this->service->createLots($inventory, $data),
            Response::HTTP_CREATED
        );
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     * @throws GeneralException
     */
    public function createSublots(string $id): JsonResponse
    {
        $inventory = $this->service->get($id);
        $licenseId = $this->request->get('license_id');
        $data = $this->validate(
            $this->request,
            [
                'weight_per_lot' => ['required', 'numeric', 'gt:0'],
                'number_of_lot' => ['required', 'integer', 'gte:1'],
                'sublot_product_id' => ['required', 'uuid'],
                'sublot_room_id' => ['nullable', new IsInventoryRoom($licenseId)],
                'parent_product_id' => ['sometimes', 'required', 'uuid'],
                'parent_room_id' => ['nullable', new IsInventoryRoom($licenseId)],
                'product_type' => ['required', new In([Inventory::TYPE_SOURCE, Inventory::TYPE_END])],
            ]
        );

        return response()->json(
            $this->service->createSublots($inventory, $data),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws GeneralException
     * @throws ValidationException|Throwable
     */
    public function hold($id): JsonResponse
    {
        /** @var Inventory $inventory */
        $inventory = $this->service->get($id);
        if (!$inventory->is_lotted || empty($inventory->product_id)) {
            throw new GeneralException(__('exceptions.inventory.must_lotted'));
        }

        $data = $this->validate($this->request,
            [
                'quantity' => ['required', 'numeric', 'min:0', 'lte:' . ($inventory->qty_on_hand - $inventory->qty_allocated)]
            ]);
        return response()->json($this->service->hold($inventory, (float)$data['quantity']));
    }

    /**
     * @return JsonResponse
     * @throws GeneralException
     * @throws ValidationException
     */
    public function bulkDestroy(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $rules = [
            'ids' => ['required', 'array',],
            'ids.*' => ['uuid', 'exists:App\Models\Inventory,id,deleted_at,NULL,license_id,' . $licenseId],
            'reason' => ['required', new In(Inventory::REASON_DESTROY)],
            'room_id' => ['required', new IsDisposalRoom($licenseId)],
            'comment' => ['nullable', 'string']
        ];
        $data = $this->validate($this->request, $rules);
        return response()->json(
            $this->service->moveInventoriesToDisposal(
                $this->service->getEnumerable(Arr::get($data, 'ids')),
                $this->roomService->get(Arr::get($data, 'room_id')),
                $data
            )
        );
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function moveToRoom(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $rules = [
            'ids' => ['required', 'array'],
            'id.*' => ['uuid', 'exists:App\Models\Inventory,id,deleted_at,NULL,license_id,' . $licenseId],
            'room_id' => [
                'required',
                'uuid',
                'exists:App\Models\Room,id,license_id,' . $licenseId . ',deleted_at,NULL',
            ]
        ];

        $data = $this->validate($this->request, $rules);

        return \response()->json(
            $this->service->moveInventoriesToRoom(
                $this->service->getEnumerable(Arr::get($data, 'ids')),
                $this->roomService->get(Arr::get($data, 'room_id'))
            )
        );
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     * @throws GeneralException
     */
    public function mapToProduct(): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $data = $this->validate($this->request,
            [
                'ids' => ['required', 'array',],
                'ids.*' => ['uuid', 'exists:App\Models\Inventory,id,deleted_at,NULL,license_id,' . $licenseId],
                'product_id' => ['required', 'uuid'],
                'product_type' => ['required', new In([Inventory::TYPE_SOURCE, Inventory::TYPE_END])],
            ]);
        $inventories = $this->service->getEnumerable($data['ids']);

        return response()->json($this->service->mapToProduct($inventories, $data));
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     * @throws GeneralException
     */
    public function changeSellStatus(string $sellStatus): JsonResponse
    {
        $newSellStatus = '';
        switch ($sellStatus) {
            case 'available':
                $newSellStatus = Inventory::SELL_STATUS_AVAILABLE;
                break;
            case 'unavailable':
                $newSellStatus = Inventory::SELL_STATUS_UNAVAILABLE;
                break;
            default:
                throw new GeneralException(__('exceptions.inventory.sell_status_invalid'));
        }

        $licenseId = $this->request->get('license_id');
        $rules = [
            'ids' => [
                'required',
                'array',
                function ($attribute, $value, $fail) use ($licenseId) {
                    $ids = Arr::wrap($value);
                    $errors = [];
                    $inventories = Inventory::query()->where('license_id', $licenseId)
                        ->whereIn('id', $ids)
                        ->whereNull('deleted_at')
                        ->get();
                    if (!$inventories || count($ids) !== count($inventories)) {
                        $errors = ['There are some invalid records in list inventory, the system can not  update the sell status of these inventories '];
                        return $fail($errors);
                    }

                    foreach ($inventories as $inventory) {
                        if ($inventory->qa_status !== Inventory::QA_STATUS_PASSED) {
                            $errors[] = "Can not update sell status for " . $inventory->sync_code . "  inventory  without QA results.";
                        }
                    }

                    if (!empty($errors)) {
                        return $fail($errors);
                    }
                }
            ]
        ];

        return response()->json(
            $this->service->updateSellStatus(
                $this->service->getEnumerable(
                    Arr::get($this->validate($this->request, $rules), 'ids')
                ),
                $newSellStatus
            )
        );
    }

    public function getDetail(string $id): JsonResponse
    {
        return response()->json(
            $this->service->get($id)->load([
                'stateCategory:id,name',
                'qaSamples',
                'parent',
                'children',
                'conversions',
                'product',
                'parent.type',
                'parent.stateCategory',
            ])
        );
    }

    public function getConversions($id): JsonResponse
    {
        return response()->json(
            $this->service->getConversions($id)
        );
    }

}
