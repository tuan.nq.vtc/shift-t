<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Additive, License, User};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Additive::class,
    function (Faker $faker) {
        return [
            "name" => $faker->words(2, true),
            "epa_regulation_number" => strtoupper($faker->words(12, true)),
            "license_id" => function () {
                return factory(License::class)->create()->id;
            },
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Additive::class,
                $data['license_id'],
                null,
                $data['type']
            ),
            "ingredients" => [
                ["name" => "Phosphorous", "percentage" => $faker->randomFloat(null, 0, 25)],
                ["name" => "Nitrogen", "percentage" => $faker->randomFloat(null, 0, 25)],
                ["name" => "Potassium", "percentage" => $faker->randomFloat(null, 0, 25)],
            ],
            "uom" => Additive::UOM_GALLON,
            "created_by" => function () {
                return factory(User::class)->create()->id;
            },
            "updated_by" => function () {
                return factory(User::class)->create()->id;
            },
            "visibility" => $faker->boolean
        ];
    }
);
