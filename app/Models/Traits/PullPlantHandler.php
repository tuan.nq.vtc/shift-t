<?php

namespace App\Models\Traits;

use App\Models\{Batch, GrowCycle, PlantGroup, Room, SeedToSale, State, Strain, TraceableModel};
use App\Synchronizations\Exceptions\Pull\PreSaveException;
use Illuminate\Support\Carbon;
use Throwable;

trait PullPlantHandler
{
    public function getMappings(): ?array
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            /*
            "created_at": "12/01/2020 12:20am",
            "updated_at": "12/01/2020 12:33am",
            "external_id": "f88d17ce-c9a4-4e4a-b790-860966a36d16",
            "plant_created_at": "01/12/2020",
            "plant_harvested_at": "",
            "is_initial_inventory": "0",
            "origin": "seed",
            "stage": "growing",
            "notes": "",
            "group_name": "",
            "pesticides": "",
            "nutrients": "",
            "additives": "",
            "is_mother": "0",
            "deleted_at": null,
            "last_moved_at": null,
            "plant_harvested_end_at": "",
            "global_id": "WAJ1965.PLBD1P",
            "legacy_id": null,
            "global_area_id": "WAJ1965.AR4XKI",
            "area_name": "Cutivation Room 02",
            "global_batch_id": "WAJ1965.BA77YD",
            "batch_source": "inhouse",
            "global_mme_id": "WASTATE1.MM2IV",
            "mme_name": "Bamboo_P/P",
            "mme_code": "J1965",
            "global_user_id": "WASTATE1.US2WU",
            "global_strain_id": "WAJ1965.ST65S6",
            "strain_name": "Default fffff",
            "global_mother_plant_id": null
            */
            return [
                'global_id' => fn(self $instance, $value) => $instance->firstOrNew(
                    ['sync_code' => $value, 'license_id' => $instance->license->id,]
                ),
                'is_mother' => 'is_mother',
            ];
        }

        return null;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    public function preSave(): self
    {
        if ($this->license->isRegulator(State::REGULATOR_LEAF)) {
            $info = $this->getAttribute('info');
            $errorFormat = 'Cannot create plant without %s';
            // Find batch & plant group then associate with plant
            $batchSyncCode = throw_unless(
                $info['global_batch_id'] ?? null,
                new PreSaveException(sprintf($errorFormat, 'batch'))
            );
            $batch = $this->firstOrNewResource(Batch::class, $batchSyncCode);
            throw_unless(
                $batch instanceof Batch && $batch->source instanceof PlantGroup,
                new PreSaveException(sprintf($errorFormat, 'a valid plant batch'))
            );
            $this->plantGroup()->associate($batch->source);
            // Use first grow cycle related to plant group & Allow nullable grow cycle
            $growCycle = optional(optional($batch->source)->growCycles)->first();
            $this->growCycle()->associate($growCycle);
            // Find strain then associate with plant
            $strainSyncCode = throw_unless(
                $info['global_strain_id'] ?? null,
                new PreSaveException(sprintf($errorFormat, 'strain'))
            );
            $strain = $this->firstOrNewResource(Strain::class, $strainSyncCode);
            throw_unless(
                $strain instanceof Strain && $strain->exists,
                new PreSaveException(sprintf($errorFormat, 'strain'))
            );
            if ($strain instanceof Strain && $strain->exists) {
                $this->strain()->associate($strain);
            }
            // Find room then associate with plant
            $roomSyncCode = throw_unless(
                $info['global_area_id'] ?? null,
                new PreSaveException(sprintf($errorFormat, 'room'))
            );
            $room = $this->firstOrNewResource(Room::class, $roomSyncCode);
            throw_unless(
                $room instanceof Room && $room->exists,
                new PreSaveException(sprintf($errorFormat, 'room'))
            );
            $this->room()->associate($room);
            // Handle source type
            $this->source_type = throw_unless(
                SeedToSale::getLeafSourceType($this->info['origin']),
                new PreSaveException(sprintf($errorFormat, 'source type'))
            );
            // Handle grow status
            $this->grow_status = throw_unless(
                SeedToSale::getLeafGrowStatus($this->info['stage']),
                new PreSaveException(sprintf($errorFormat, 'grow status'))
            );
            // Handle planted at. Use created_at when plant_created_at is empty or not a valid
            if (!blank($this->info['plant_created_at'])) {
                $this->planted_at = Carbon::createFromFormat('d/m/Y', $this->info['plant_created_at']);
            } else {
                $this->planted_at = Carbon::createFromFormat('d/m/Y H:ia', $this->info['created_at']);
            }
        }

        if ($this->license->isRegulator(State::REGULATOR_METRC)) {
            // TODO: Handle pre save plant from Metrc
        }

        return $this;
    }

    /**
     * @return self
     *
     * @throws Throwable
     */
    public function postSave(): self
    {
        return $this;
    }

    protected function firstOrNewResource(string $class, string $syncCode, array $attributes = []): TraceableModel
    {
        return $class::firstOrNew(
            array_merge(
                ['sync_code' => $syncCode, 'license_id' => $this->license->id,],
                $attributes
            )
        );
    }
}
