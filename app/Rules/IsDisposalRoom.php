<?php

namespace App\Rules;

use App\Services\RoomService;
use Illuminate\Contracts\Validation\Rule;

class IsDisposalRoom implements Rule
{
    private string $licenseId;

    public function __construct($licenseId)
    {
        $this->licenseId = $licenseId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return RoomService::isDisposal($value, $this->licenseId);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('exceptions.disposal.invalid_room');
    }
}

