<?php

namespace App\Jobs;

use App\Facades\FileUpload;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Throwable;

class GenerateMultipleImageSizesJob extends Job
{
    public $queue = 'generate_image_sizes';

    private string $imagePath;
    private Model $model;

    /**
     * Create a new job instance.
     *
     * @return void
     * @throws Exception
     */
    public function __construct(Model $model, string $imagePath)
    {
        if (!Storage::exists($imagePath)) {
            throw new Exception('Image not found');
        }

        $this->imagePath = $imagePath;
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Throwable
     */
    public function handle()
    {
        if (!Storage::exists($this->imagePath)) {
            return;
        }

        $imageSizeConfigs = config('image_sizes');
        if (empty($imageSizeConfigs[get_class($this->model)])) {
            return;
        }
        $storedPaths = [];
        try {
            foreach($imageSizeConfigs[get_class($this->model)] as $imageSize => $config) {
                $storedPaths[] = FileUpload::generateResizedImage($this->imagePath, $imageSize, $config);
            }
        } catch (Throwable $exception) {
            Storage::delete($storedPaths);
            throw $exception;
        }
    }

    /**
     * @param Throwable $exception
     * @return void
     */
    public function failed(\Throwable $exception)
    {
        $errorMsg = "[StoreMultipleImageSizesJob] [queue:{$this->queue}] " .
            get_class($this->model) . ': ' . $this->model->id .
            " was failed {$exception->getMessage()}";
        Log::error($errorMsg);
        throw new \Exception($errorMsg);
    }
}
