<?php

use Illuminate\Database\{Eloquent\Model, Seeder};

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PortalUserSeeder::class);
        $this->call(StateCategorySeeder::class);
        $this->call(RoomTypeTableSeeder::class);
        $this->call(LabTestAnalysesSeeder::class);
        $this->call(PrintLabelTemplateSeeder::class);

        Model::reguard();
    }
}
