<?php

namespace App\Models;

use App\Models\Traits\Scope\{NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\{Builder, Relations\BelongsTo, Relations\HasMany, SoftDeletes};

/**
 * Class Subroom
 * @package App\Models
 */
class Subroom extends BaseModel
{
    use SoftDeletes;
    use NameFilterable;
    use StatusFilterable;

    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const TYPE_RACK = 'rack';
    public const TYPE_BED = 'bed';

    protected $fillable = [
        'name',
        'room_id',
        'status',
        'number',
        'type',
        'dimension',
        'color',
    ];

    protected $casts = [
        'dimension' => 'array',
    ];

    protected $hidden = ['plants'];

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }

    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class);
    }

    /**
     * @param  Builder  $query
     * @param  string  $type
     *
     * @return Builder
     */
    public function scopeType($query, string $type)
    {
        return $query->where('type', $type);
    }

    /**
     * @param  Builder  $query
     * @param  string  $roomId
     *
     * @return Builder
     */
    public function scopeRoomId($query, string $roomId)
    {
        return $query->where('room_id', $roomId);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeAvailable(Builder $query)
    {
        return $query->where('status', true);
    }
}
