<?php

namespace App\Models\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ConversionInputLotPivot extends Pivot
{
    protected $casts = [
        'quantity' => 'float',
    ];
}