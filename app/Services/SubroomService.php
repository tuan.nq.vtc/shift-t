<?php

namespace App\Services;

use App\Models\Subroom;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class SubroomService
 * @package App\Services
 *
 * @method Subroom|Collection get($id)
 * @method Subroom|Builder getModel()
 */
class SubroomService extends BaseService
{
    public function __construct(Subroom $subroom)
    {
        parent::__construct($subroom);
    }

}
