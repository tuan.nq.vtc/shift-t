<?php

/** @var Factory $factory */

use App\Helpers\ModelHelper;
use App\Models\{Inventory, InventoryType, License, Room, StateCategory, Strain, Subroom};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    Inventory::class,
    function (Faker $faker) {
        return [
            'license_id' => fn() => factory(License::class)->create()->id,
            'internal_id' => fn($data) => ModelHelper::generateInternalId(
                Inventory::class,
                $data['license_id']
            ),
            'type_id' => function ($data) {
                return factory(InventoryType::class)->create(['license_id' => $data['license_id']])->id;
            },
            'state_category_id' => fn() => factory(StateCategory::class)->create()->id,
            'strain_id' => function ($data) {
                return factory(Strain::class)->create(['license_id' => $data['license_id']])->id;
            },
            'room_id' => function ($data) {
                return factory(Room::class)->create(['license_id' => $data['license_id']])->id;
            },
            'subroom_id' => function ($data) {
                return factory(Subroom::class)->create(['room_id' => $data['room_id']])->id;
            },
            'is_lotted' => rand(0, 1),
            'for_extraction' => rand(0, 1),
            'qa_status' => rand(0, 1),
            'sell_status' => rand(0, 1),
            'qty_on_hand' => $faker->randomNumber(3),
            'qty_allocated' => 0,
            'qty_on_hold' => 0,
            'sync_code' => 'WAG010101.IN' . $faker->regexify('[A-Z0-9]{4}'), // WAG010101.IN7EAI
            'synced_at' => $faker->dateTimeThisYear(),
        ];
    }
);
