<?php

use App\Models\Room;
use App\Models\Subroom;
use App\Models\License;
use App\Models\Conversion;
use App\Models\Inventory;
use Illuminate\Database\Seeder;

/**
 * Class SubroomTableSeeder.
 */
class ConversionTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();
        $licenses = License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get();
        $rooms = Room::whereHas('inventories')->get();

        $rooms->each(function(Room $room) use ($licenses) {
            factory(Conversion::class)
                ->create([
                    'license_id' => $licenses->random(1)->first()->getKey(),
                    'output_room_id' => $room->id,
                    'waste_room_id' => null,
                ]);
        });
        $this->enableForeignKeys();
    }
}
