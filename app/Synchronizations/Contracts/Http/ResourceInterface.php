<?php

namespace App\Synchronizations\Contracts\Http;

use Throwable;

interface ResourceInterface
{
    public const ACTION_CREATE = 'create';
    public const ACTION_DELETE = 'delete';
    public const ACTION_FETCH = 'fetch';
    public const ACTION_LIST = 'list';
    public const ACTION_UPDATE = 'update';

    /**
     * @param string $action
     * @return void
     *
     * @throws Throwable
     */
    public function build(string $action): void;

    /**
     * @param $method
     * @return self
     */
    public function setMethod(string $method): self;

    /**
     * @return string|null
     */
    public function getMethod(): string;

    /**
     * @param $url
     * @return self
     */
    public function setUrl($url): self;

    /**
     * @return string|null
     */
    public function getUrl(): string;

    /**
     * @param $query
     * @return self
     */
    public function setQuery($query): self;

    /**
     * @return array|null
     */
    public function getQuery(): array;

    /**
     * @param $param
     * @param $value
     * @return self
     */
    public function addQuery($param, $value): self;

    /**
     * @param $body
     * @return self
     */
    public function setBody($body): self;

    /**
     * @return array|null
     */
    public function getBody(): array;
}
