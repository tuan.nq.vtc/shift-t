<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Models\{Harvest, HarvestGroup, HarvestWetWeight};
use Illuminate\Database\Eloquent\{Builder, Collection, Model};
use Illuminate\Support\{Arr, Carbon, Facades\DB};

/**
 * Class HarvestService
 * @package App\Services
 *
 * @property Harvest|Builder $model
 *
 * @method Harvest get($id)
 * @method Harvest|Builder getModel()
 */
class HarvestService extends BaseService
{
    protected array $relations = [
        'wetWeights'
    ];

    public function __construct(Harvest $harvest)
    {
        parent::__construct($harvest);
    }

    /**
     * @param $id
     * @param array $weights
     * @return Harvest
     * @throws GeneralException
     */
    public function updateDryWeights($id, array $weights): Harvest
    {
        $harvest = $this->get($id);
        if ($harvest->group->status !== HarvestGroup::STATUS_WET_CONFIRMED) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'wet confirmed']));
        }
        if (Arr::get($weights, 'flower_dry_weight', 0) > $harvest->flower_wet_weight
            || Arr::get($weights, 'material_dry_weight', 0) > $harvest->material_wet_weight
            || Arr::get($weights, 'waste_dry_weight', 0) > $harvest->waste_wet_weight) {
            throw new GeneralException(__('exceptions.harvest.dry_weight.lt_wet_weight'));
        }
        if (!$harvest->update($weights)) {
            throw new GeneralException(__('exceptions.actions.update_failed'));
        }

        return $harvest;
    }

    /**
     * @param string $id
     * @param array $weightData
     * @return Model | Harvest
     * @throws GeneralException
     */
    public function addWetWeight(string $id, array $weightData): Harvest
    {
        $harvest = $this->get($id);
        if ($harvest->group->status !== HarvestGroup::STATUS_OPEN) {
            throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'wet confirmed']));
        }

        $weight = (float)Arr::get($weightData, 'weight', 0);
        if ($weight <= 0) {
            throw new GeneralException(__('exceptions.harvest.wet_weight.min_quantity'));
        }
        $harvestedAt = Arr::get($weightData, 'harvested_at');
        if (!$harvestedAt || $harvest->start_at->gt(Carbon::parse($harvestedAt))) {
            throw new GeneralException(__('exceptions.harvest.wet_weight.after_start_date'));
        }
        if ($harvestWeight = $harvest->wetWeights()->firstWhere(Arr::only($weightData, ['harvested_at', 'type']))) {
            $harvestWeight->increaseWeight($weight);
        } elseif (!$harvest->wetWeights()->create($weightData)) {
            throw new GeneralException(__('exceptions.harvest.wet_weight.create_failed'));
        }

        // dispatch add wet weight event
        return $harvest->fresh($this->relations);
    }

    /**
     * @param $licenseId
     *
     * @return Collection
     */
    public function getHarvestedFlowerWeights($licenseId): Collection
    {
        return $this->newQuery()
            ->withoutGlobalScope('defaultOrder')
            ->select(
                [
                    DB::raw('SUM(flower_wet_weight) AS total_flower_wet_weight'),
                    DB::raw('SUM(flower_dry_weight) AS total_flower_dry_weight'),
                ]
            )
            ->licenseId($licenseId)
            ->get();
    }

    /**
     * @param $licenseId
     *
     * @return Collection
     */
    public function getHarvestedOtherMaterialWeights($licenseId): Collection
    {
        return $this->newQuery()
            ->withoutGlobalScope('defaultOrder')
            ->select(
                [
                    DB::raw('SUM(material_wet_weight) AS total_material_wet_weight'),
                    DB::raw('SUM(material_dry_weight) AS total_material_dry_weight'),
                ]
            )
            ->licenseId($licenseId)
            ->get();
    }

    /**
     * @param Harvest $harvest
     * @return Harvest
     */
    public function calculateWetWeights(Harvest $harvest): Harvest
    {
        $wetWeights = $harvest->wetWeights;
        $harvest->update(
            [
                'flower_wet_weight' => $this->totalWeightsByType($wetWeights, HarvestWetWeight::TYPE_FLOWER),
                'material_wet_weight' => $this->totalWeightsByType($wetWeights, HarvestWetWeight::TYPE_MATERIAL),
                'waste_wet_weight' => $this->totalWeightsByType($wetWeights, HarvestWetWeight::TYPE_WASTE),
            ]
        );

        return $harvest;
    }

    /**
     * @param Collection $wetWeights
     * @param $type
     * @return float
     */
    private function totalWeightsByType(Collection $wetWeights, $type)
    {
        return round(
            $wetWeights->filter(fn($object) => $object->type === $type)->sum('weight'),
            4
        );
    }
}
