<?php

namespace App\Services\Traits;

use App\Exceptions\GeneralException;
use App\Facades\Sales;
use App\Models\{EndProduct};
use Illuminate\Support\Collection;
use Throwable;

trait EndProductHandler
{
    /**
     * @param string $id
     * @return mixed|array
     * @throws GeneralException
     */
    public function getAvailableProduct(string $id): array
    {
        try {
            $productData = Sales::fetchProduct($id);
        } catch (Throwable $throwable) {
            throw new GeneralException(__('exceptions.sales_service.product_not_exists'));
        }

        $productDetail = $this->getStateValues($productData);
        if ($productDetail['status'] === EndProduct::STATUS_DISABLED) {
            throw new GeneralException(__('exceptions.sales_service.product_not_available'));
        }

        return $productData;
    }

    public function getStateValues(array $productData): array
    {
        return $productData['state_value'] ?? $productData['root_value'];
    }

    public function getAvailableProductObject(string $id)
    {
        $dataProduct = $this->getAvailableProduct($id);
        $productDetail = $this->getStateValues($dataProduct);

        if (!$productDetail) {
            throw new GeneralException(__('exceptions.sales_service.product_not_available'));
        }

        return new EndProduct($productDetail);
    }
}
