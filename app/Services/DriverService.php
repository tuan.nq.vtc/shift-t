<?php

namespace App\Services;

use App\Models\Driver;
use Illuminate\Support\Collection;

/**
 * Class DriverService
 * @package App\Services
 */
class DriverService extends BaseService
{
    public function __construct(Driver $driver)
    {
        parent::__construct($driver);
    }

    /**
     * @return Collection
     */
    public function getAvailableItems(): Collection
    {
        return $this->newQuery()->where('status', Driver::STATUS_ENABLED)->get();
    }
}
