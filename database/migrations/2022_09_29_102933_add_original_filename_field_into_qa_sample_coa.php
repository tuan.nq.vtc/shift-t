<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOriginalFilenameFieldIntoQaSampleCoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qa_sample_coa', function (Blueprint $table) {
            if (!Schema::hasColumn('qa_sample_coa', 'original_filename')) {
                $table->string('original_filename');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qa_sample_coa', function (Blueprint $table) {
            if (Schema::hasColumn('qa_sample_coa', 'original_filename')) {
                $table->dropColumn(['original_filename']);
            }
        });
    }
}
