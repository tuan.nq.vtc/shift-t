<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Facades\FileUpload;
use App\Http\Parameters\Criteria;
use App\Models\{License, Plant, Strain};
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\{Builder, Collection, Model};
use Illuminate\Support\Arr;
use Illuminate\Support\Enumerable;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class StrainService
 *
 * @package App\Services
 *
 * @method Strain|Collection get($id)
 * @method Strain|Builder getModel()
 */
class StrainService extends BaseService
{
    /**
     * @var array|string[]
     */
    public array $relations = ['creator', 'modifier'];

    public function __construct(Strain $strain)
    {
        parent::__construct($strain);
    }

    /**
     * @return Collection
     */
    public function getAvailableItems(): Collection
    {
        return $this->newQuery()->where('status', Strain::STATUS_ENABLED)->get(['id', 'name'])->each->setAppends([]);
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function getItemByIds(array $ids, $strainName = ''): Collection
    {
        $query = $this->getModel()->whereIn('id', $ids);
        if($strainName) {
            $query = $query->where('name', 'like', '%' . $strainName . '%');
        }
        return $query->whereIn('id', $ids)->with(['license:id,name'])->get(['id', 'name', 'image', 'description', 'license_id'])->each->setAppends(['image_url']);
    }

    /**
     * @param array $data
     *
     * @return Collection
     *
     * @throws Throwable
     */
    public function bulkCreate(array $data): Collection
    {
        if (empty($data['user_id']) || empty($data['license_id'])
            || !is_array(Arr::get($data, 'strains'))
        ) {
            throw new GeneralException(__('exceptions.strains.create_failed'));
        }
        $strains = $this->getModel()->newCollection();

        try {
            DB::beginTransaction();
            foreach (Arr::get($data, 'strains', []) as $strainData) {
                if (!isset($strainData['name'])) {
                    throw new GeneralException(__('exceptions.strains.create_failed'));
                }
                $strain = $this->create(
                    [
                        'name' => $strainData['name'],
                        'image' => $strainData['image'] ?? '',
                        'description' => $strainData['description'] ?? '',
                        'custom_id' => $strainData['custom_id'] ?? '',
                        'info' => Arr::only($strainData, ['thc', 'cbd', 'indica', 'sativa', 'testing_status']),
                        'license_id' => $data['license_id'],
                        'created_by' => $data['user_id'],
                        'updated_by' => $data['user_id'],
                    ]
                );
                $strains->push($strain);
            }
            DB::commit();

            return $strains;
        } catch (Throwable $e) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.actions.create_failed'));
        }
    }

    /**
     * @param array $data
     * @return Model
     * @throws GeneralException
     */
    public function create(array $data): Model
    {
        /** @var Strain $strain */
        $strain = parent::create(Arr::except($data, ['image']));
        if (!empty($data['image'])) {
            FileUpload::uploadImage($strain, $data['image'], $strain->id, 'image');
        }
        return $strain;
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return Model
     * @throws GeneralException
     */
    public function update($id, array $data): Model
    {
        if (empty($data['user_id'])) {
            throw new GeneralException(__('exceptions.strains.update_failed'));
        }
        $newInfo = Arr::only($data, ['testing_status', 'thc', 'cbd', 'indica', 'sativa']);
        $strain = $this->model->findOrFail($id);

        if (!empty($data['image'])) {
            FileUpload::uploadImage($strain, $data['image'], $strain->id, 'image');
            unset($data['image']);
        }

        $strain->update(
            array_merge(
                Arr::only($data, ['name', 'description', 'status', 'custom_id']),
                [
                    'info' => is_null($strain->info) ? $newInfo : array_merge(
                        $strain->info,
                        $newInfo
                    ),
                    'updated_by' => $data['user_id'],
                ]
            )
        );
        return $strain->load($this->relations);
    }

    /**
     * @param Criteria $criteria
     *
     * @return LengthAwarePaginator
     */
    public function list(Criteria $criteria): LengthAwarePaginator
    {
        return parent::list($criteria->addFilter('available', true));
    }


    /**
     * Count non-disposed (destroyed) & non-moved (to inventory) & non-trashed plants by strains
     * Result will be ordered by plants counted, strain creation time
     * Show only $shownStrains (most dominant strains) and one for the rest strains
     *
     * @param     $licenseId
     * @param int $shownStatisticized
     *
     * @return array
     */
    public function countPlantsByStrains($licenseId, int $shownStatisticized): array
    {
        $plantsCount = fn($query) => $query->withoutGlobalScopes([Plant::DEFAULT_ORDER_SCOPE])
            ->licenseId($licenseId)
            ->destroyed(false)
            ->isManifested(false)
            ->withoutTrashed();

        $plantsCountEnumerable = $this->newQuery()
            ->select(['name'])
            ->status()
            ->withCount(['plants' => $plantsCount])
            ->orderByDesc('plants_count')
            ->orderByDesc('created_at')
            ->cursor();

        $plantsCountedByStrain = $plantsCountEnumerable->take($shownStatisticized)->toArray();
        $plantsCountedByStrain[] = [
            'name' => 'Others',
            'plants_count' => $plantsCountEnumerable->skip($shownStatisticized)->sum('plants_count'),
        ];

        return $plantsCountedByStrain;
    }

    /**
     * @param string $licenseId
     *
     * @return mixed
     */
    public function count(string $licenseId): int
    {
        return $this->newQuery()
            ->licenseId($licenseId)
            ->count();
    }

    /**
     * @param Enumerable $strains
     * @param string $active
     *
     * @return GeneralException|\Exception|Enumerable|Throwable
     * @throws Throwable
     */
    public function changeStatus(Enumerable $strains, $status = 'enabled')
    {
        if (!in_array($status, ['enabled', 'disabled'])) {
            return new GeneralException(__('exceptions.strains.update_failed'));
        }
        try {
            DB::beginTransaction();
            $strains->each(
                function (Strain $strain) use ($status) {
                    return tap(
                        $strain,
                        fn(Strain $strain) => $strain->update(
                            [
                                'status' => $status === 'enabled'
                            ]
                        )
                    );
                }
            );
            DB::commit();
        } catch (\Throwable $throwable) {
            DB::rollBack();
            return $throwable;
        }
        return $strains;
    }

    /**
     * @param Collection|null $licenses
     * @param Criteria|null $criteria
     * @return array
     */
    public function getByLicenses(?Collection $licenses, ?Criteria $criteria): LengthAwarePaginator
    {
        if (!$licenses) {
            return [];
        }
        $scopes = $criteria->getFilters();
        $licenseIds = $licenses->pluck('id')->toArray();
        return $this->getModel()
            ->select(['id', 'name'])
            ->whereIn('license_id', $licenseIds)
            ->where('status', Strain::STATUS_ENABLED)
            ->scopes($this->loadScopes($scopes))
            ->paginate($criteria->getLimit(), ['*'], config('pagination.page_name'), $criteria->getPage());
    }

    public function getAllByLicenses(?Collection $licenses)
    {
        if (!$licenses) {
            return [];
        }
        $licenseIds = $licenses->pluck('id')->toArray();
        return $this->getModel()
            ->whereIn('license_id', $licenseIds)
            ->where('status', Strain::STATUS_ENABLED)
            ->get(['id', 'name'])->each->setAppends([]);
    }

    /**
     * @param $id
     * @param $accountHolderId
     * @return array|Builder|Builder[]|Collection|Model
     */
    public function getDetailByAccountHolder($id, $accountHolderId)
    {
        $licenses = License::query()
            ->where('account_holder_id', $accountHolderId)
            ->get();
        if (!$licenses) {
            return [];
        }
        $licenseIds = $licenses->pluck('id')->toArray();
        return $this->getModel()
            ->where('status', Strain::STATUS_ENABLED)
            ->whereIn('license_id', $licenseIds)->findOrFail($id);
    }

    /**
     * @param array $ids
     * @param $accountHolderId
     * @return array|Builder[]|Collection
     */
    public function getByAccountHolder(array $ids, $accountHolderId)
    {
        $licenses = License::query()
            ->where('account_holder_id', $accountHolderId)
            ->get();
        if (!$licenses) {
            return [];
        }
        $licenseIds = $licenses->pluck('id')->toArray();
        $strainQuery = $this->getModel()
            ->where('status', Strain::STATUS_ENABLED)
            ->whereIn('license_id', $licenseIds);

        if (count($ids)) {
            $strainQuery->whereIn('id', $ids);
        }

        return $strainQuery->get(['id', 'name']);
    }

    /**
     * @param array $data
     * @return Collection
     * @throws GeneralException
     */
    public function import(array $data): Collection
    {
        if (empty($data['user_id']) || empty($data['license_id']) ||
            !is_array(Arr::get($data, 'strains'))) {
            throw new GeneralException(__('exceptions.strains.create_failed'));
        }
        $strains = $this->getModel()->newCollection();

        try {
            DB::beginTransaction();
            foreach (Arr::get($data, 'strains', []) as $itemData) {
                if (!isset($itemData['name'])) {
                    throw new GeneralException(__('exceptions.strains.create_failed'));
                }
                $strainData = [
                    'name' => $itemData['name'],
                    'custom_id' => $itemData['custom_id'] ?? '',
                    'info' => Arr::only($itemData, ['thc', 'cbd', 'indica', 'sativa', 'testing_status']),
                    'license_id' => $data['license_id'],
                    'created_by' => $data['user_id'],
                    'updated_by' => $data['user_id'],
                ];
                $strain = $this->model->firstOrCreate(
                    Arr::only($strainData, ['name', 'license_id']),
                    $strainData
                );
                $strains->push($strain);
            }
            DB::commit();

            return $strains;
        } catch (Throwable $e) {
            DB::rollBack();
            throw new GeneralException(__('exceptions.actions.create_failed'));
        }
    }

}
