<?php

namespace App\Listeners;

use App\Events\Inventory\InventorySaved;
use App\Models\Inventory;
use App\Services\InventoryService;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Log;

class InventoryEventSubscriber
{
    /**
     * @param InventorySaved $event
     */
    public function handler($event)
    {
        $this->calculateProductQuantity($event->inventory);
        $this->updateProductName($event->inventory);
    }

    /**
     * @param Inventory $inventory
     * @throws \Throwable
     */
    private function calculateProductQuantity($inventory)
    {
        if ($inventory->isDirty(['qty_allocated', 'qty_on_hold', 'qty_on_hand', 'product_id'])) {
            $productIds = [];
            if ($inventory->product_id) {
                $productIds[$inventory->product_id] = $inventory->product_type;
            }

            if ($inventory->isDirty('product_id') && $inventory->getOriginal('product_id')) {
                $productIds[$inventory->getOriginal('product_id')] = $inventory->getOriginal('product_type');
            }

            app(InventoryService::class)->updateProductQty($productIds);
        }
    }

    /**
     * @param Inventory $inventory
     */
    private function updateProductName($inventory)
    {
        if ($inventory->isDirty(['product_id'])) {
            app(InventoryService::class)->updateProductName($inventory->product_id, $inventory->product_type);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [InventorySaved::class],
            'App\Listeners\InventoryEventSubscriber@handler'
        );
    }
}
