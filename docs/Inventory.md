#### Tables
- Inventory - Packages 
    - Explanation -  Giải thích: Các gói `package` nguyên liệu `part` & thành phẩm `product` đc lưu kho
    - Attributes - Thuộc tính:
        - Belong to licensee
        - Belong to a batch (Propagation, Plant, Harvest, Intermediate/End product)
        - Identification: Code tag
        - What's inside: Item
        - Where store: Room
        - Quantity

- Inventory Forms - Items
    - Explanation:  Kiểu mục `item` của các gói đc lưu kho
    - Attributes:
        - Belong to licensee
        - Name
        - Type
        - With or without Test Lab result
        - Source: Propagation, Plant, Harvest, Extraction (From another package)


- Lab Test


- Inventory Types - Item Type - Loại kiểu mục `item` của gói lưu kho
    - Explanation
    - Attributes:
        - Belong to licensee
        - Name
        - Category
        - Sub Category


- Inventory Categories - Item Categories - Danh mục của các kiểu mục `item`
    - Explanation
    - Attributes:
        - Name
        - Strain required

- Inventory Sub Categories - Item Sub Categories - Danh mục con của các kiểu mục `item`
    - Explanation
    - Attributes:
        - Name
        - Category
        - Unit of mesurement

#### LEAF: Inventory Type ~ METRC: Item
##### Types & Intermediate Types:
1. intermediate_product
    - marijuana_mix
    - non-solvent_based_concentrate
    - hydrocarbon_concentrate
    - co2_concentrate
    - ethanol_concentrate
    - food_grade_solvent_concentrate
    - infused_cooking_medium
1. end_product
    - liquid_edible
    - solid_edible
    - concentrate_for_inhalation
    - topical,
    - infused_mix
    - packaged_marijuana_mix
    - sample_jar
    - usable_marijuana
    - capsules
    - tinctures
    - transdermal_patches
    - suppositories
1. immature_plant
    - seeds
    - clones
    - plant_tissue
1. mature_plant
    - mature_plant
    - non_mandatory_plant_sample
1. harvest_materials
    - flower
    - other_material
    - flower_lots
    - other_material_lots
1. waste
    - waste