<?php

namespace Tests\Unit\Services;

use App\Models\FlowerType;
use App\Services\FlowerTypeService;
use Tests\TestCase;

class FlowerTypeServiceTest extends TestCase
{
    protected static string $modelName = FlowerType::class;

    private function getService()
    {
        return app(FlowerTypeService::class);
    }

    /**
     * @test
     */
    public function it_can_get_available_items()
    {
        $expectedCount = rand(1, 50);
        factory(static::$modelName, $expectedCount)->create(['status' => FlowerType::STATUS_ENABLED]);

        // create disabled items
        factory(static::$modelName, rand(1, 50))->create(['status' => FlowerType::STATUS_DISABLED]);

        // execute
        $items = $this->getService()->getAvailableItems();

        $this->assertCount($expectedCount, $items);
    }
}
