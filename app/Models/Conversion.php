<?php

namespace App\Models;

use App\Models\Pivots\ConversionInputLotPivot;
use App\Models\Traits\HasInternalId;
use App\Models\Traits\Scope\{DatetimeFilterable, IdsFilterable, LicenseFilterable, NameFilterable, StatusFilterable};
use Illuminate\Database\Eloquent\{Builder,
    Relations\BelongsTo,
    Relations\BelongsToMany,
    Relations\HasMany,
    Relations\MorphOne,
    Relations\MorphTo,
    SoftDeletes
};

class Conversion extends BaseModel
{
    use IdsFilterable;
    use NameFilterable;
    use StatusFilterable;
    use LicenseFilterable;
    use DatetimeFilterable;
    use HasInternalId;

    use SoftDeletes;

    public const UOM_LIST = ['kg', 'g', 'mg', 'lb', 'oz'];

    public const STATUS_OPEN = 0;
    public const STATUS_CLOSED = 1;

    public const IS_ALTERED = 0;
    public const IS_NOT_ALTERED = 1;

    public const TYPE_SOURCE = 'source';
    public const TYPE_END = 'end';

    protected $fillable = [
        'license_id',
        'name',
        'output_room_id',
        'waste_quantity',
        'uom',
        'waste_room_id',
        'output_product_id',
        'output_product_type',
        'output_state_category_id',
        'output_quantity',
        'is_altered',
        'status',
    ];

    protected $casts = [
        'is_altered' => 'bool'
    ];

    protected $appends = ['output_type'];

    protected $hidden = ['outputEndProducts', 'outputSourceProduct'];

    public function outputRoom(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'output_room_id');
    }

    public function wasteRoom(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'waste_room_id');
    }

    public function outputEndProducts()
    {
        return $this->hasMany(EndProduct::class, 'product_id', 'output_product_id');
    }

    public function outputSourceProduct()
    {
        return $this->belongsTo(SourceProduct::class, 'output_product_id', 'id');
    }

    public function inputLots(): BelongsToMany
    {
        return $this->belongsToMany(
            Inventory::class,
            'conversion_input_lot',
            'conversion_id',
            'inventory_id'
        )->using(ConversionInputLotPivot::class)
            ->withPivot([
            'quantity',
            'product_id'
        ]);
    }

    public function outputStateCategory()
    {
        return $this->belongsTo(StateCategory::class, 'output_state_category_id');
    }

    public function inputLotProducts()
    {
        return $this->belongsToMany(
            SourceProduct::class,
            'conversion_input_lot',
            'conversion_id',
            'product_id'
        )->withPivot([
            'quantity',
            'product_id'
        ]);
    }

    public function scopeOutputStateCategoryId(Builder $query, ...$outputStateCategoryIds): Builder
    {
        return $query->whereIn('output_state_category_id', $outputStateCategoryIds);
    }

    public function scopeOrderByOutputStateCategory(Builder $query, $direction): Builder
    {
        $stateCategoryTable = $this->outputStateCategory()->getRelated()->getTable();

        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($stateCategoryTable, $this->getTable() . '.output_state_category_id', '=', $stateCategoryTable . '.id')
            ->orderBy($stateCategoryTable . '.name', $direction);
    }

    public function scopeOutputProductId(Builder $query, ...$outputProductIds): Builder
    {
        return $query->whereIn('output_product_id', $outputProductIds);
    }

    public function scopeOutputType(Builder $query, $type): Builder
    {
        return $query->where('output_product_type', static::getOutputProductType($type));
    }

    public function scopeOutputRoomId(Builder $query, ...$ids): Builder
    {
        return $query->whereIn('output_room_id', $ids);
    }

    public function scopeOrderByOutputRoom(Builder $query, $direction): Builder
    {
        $outputRoomTable = $this->outputRoom()->getRelated()->getTable();

        return $query->select([$this->getTable() . '.*'])
            ->leftJoin($outputRoomTable, $this->getTable() . '.output_room_id', '=', $outputRoomTable . '.id')
            ->orderBy($outputRoomTable . '.name', $direction);
    }

    public function scopeInputStrainId(Builder $query, ...$strainIds): Builder
    {
        $inventoryTable = $this->inputLots()->getRelated()->getTable();
        return $query->whereHas(
            'inputLots',
            fn($subQuery) => $subQuery->whereIn($inventoryTable . '.strain_id', $strainIds)
        );
    }

    public function scopeInputStateCategoryId(Builder $query, ...$stateCategoryIds): Builder
    {
        return $query->whereHas(
            'inputLots.type',
            fn($subQuery) => $subQuery->whereIn('inventory_types.state_category_id', $stateCategoryIds)
        );
    }

    public function getOutputTypeAttribute(): string
    {
        switch ($this->output_product_type) {
            case EndProduct::class:
                return self::TYPE_END;
            case SourceProduct::class:
                return self::TYPE_SOURCE;
            default:
                return '';
        }
    }

    public function getOutputProductType($type): string
    {
        switch ($type) {
            case self::TYPE_END:
                return EndProduct::class;
            case self::TYPE_SOURCE:
                return SourceProduct::class;
            default:
                return '';
        }
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return mixed
     */
    public function scopeSearch(Builder $query, $value): Builder
    {
        if (empty($value)) {
            return $query;
        }

        return $query->where(
            fn($subQuery) => $subQuery->where('name', 'like', '%' . $value . '%')
                ->orWhere('output_product_id', $value)
        );
    }

    public function isStatus(int $status): bool
    {
        return $this->status === $status;
    }
}
