<?php

return [
    'abbreviation' => [
        \App\Models\Strain::class => 'ST', 
        \App\Models\Room::class => 'RM',
        \App\Models\Propagation::class => 'PP',
        \App\Models\GrowCycle::class => 'GC',
        \App\Models\Plant::class => 'PL',
        \App\Models\Harvest::class => 'HV',
        \App\Models\Additive::class => [
            \App\Models\Additive::TYPE_PESTICIDE => 'AP',
            \App\Models\Additive::TYPE_NUTRIENT => 'AN',
            \App\Models\Additive::TYPE_OTHER => 'AO',

        ],
        \App\Models\Supplier::class => 'AS', // does not have license_id, has account_holder_id
        \App\Models\Disposal::class => 'DI',
        \App\Models\Inventory::class => 'IN',
        \App\Models\Conversion::class => 'CN',
        \App\Models\QASample::class => 'QA',
        \App\Models\SourceCategory::class => 'TC', // does not have license_id, has account_holder_id
        \App\Models\Taxonomy::class => 'TX',
        \App\Models\Manifest::class => 'TR',
        \App\Models\Vehicle::class => 'VH',
        \App\Models\Driver::class => 'DR',
        \App\Models\SourceProduct::class => 'TP',
        \App\Models\PrintLabel::class => 'LB', // does not have license_id, has account_holder_id
        \App\Models\PrintLabelTemplate::class => 'LT', // does not have license_id, has account_holder_id
        \App\Models\InventoryType::class => 'TY',
        \App\Models\Batch::class => 'BA',

    ],
    'length' => [
        \App\Models\Plant::class => 5,
    ],
];
