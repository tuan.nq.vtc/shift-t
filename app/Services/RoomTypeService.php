<?php

namespace App\Services;

use App\Http\Parameters\Criteria;
use App\Models\RoomType;
use App\Services\Contracts\AccountHolderServiceInterface;
use App\Services\Traits\HasAccountHolderId;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class RoomTypeService
 * @package App\Services
 *
 * @method RoomType|Collection get($id)
 * @method RoomType|Builder getModel()
 */
class RoomTypeService extends BaseService implements AccountHolderServiceInterface
{
    use HasAccountHolderId;

    public function __construct(RoomType $roomType)
    {
        parent::__construct($roomType);
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function getAvailableItems(Criteria $criteria): Collection
    {
        $query = $this->newQuery(false)->scopes($this->loadScopes($criteria->getFilters()));
        $query->where('status', RoomType::STATUS_ENABLED)
            ->whereIn('category', RoomType::getCategories());

        return $this->applyOrderBy($query, $criteria->getSorts())->get();
    }

    public function getNotDestructionRoomType(): Collection
    {
        return $this->newQuery()
            ->where('status', RoomType::STATUS_ENABLED)
            ->where('category', '<>', RoomType::CATEGORY_DISPOSAL)
            ->get();
    }
}
