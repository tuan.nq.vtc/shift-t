<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarvestGroupsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_groups', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id')->index();
            $table->string('name')->index();
            $table->json('source_rooms')->nullable();
            $table->uuid('room_id')->index()->nullable();
            $table->unsignedTinyInteger('status')->default(0)->index();
            $table->unsignedTinyInteger('type')->default(0)->index();
            $table->date('start_at');
            $table->date('est_end_at')->nullable();
            $table->date('finished_at')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_id')->references('id')->on('rooms')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_harvests');
    }
}
