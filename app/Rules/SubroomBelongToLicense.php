<?php

namespace App\Rules;

use App\Models\Subroom;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class SubroomBelongToLicense implements Rule
{
    protected $attribute;
    protected $value;
    protected ?string $roomId;
    protected ?string $licenseId;

    public function __construct(string $roomId = null, string $licenseId = null)
    {
        $this->roomId = $roomId;
        $this->licenseId = $licenseId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $this->value = $value;
        $subroomIds = Arr::wrap($value);

        return count($subroomIds) == Subroom::whereHas(
                'room',
                fn($subQuery) => $subQuery->where('license_id', $this->licenseId)->where('id', $this->roomId)
            )->whereIn('id', $subroomIds)->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __(
            'validation.belong.subroom_to_license',
            [
                'attribute' => $this->attribute,
                'license' => $this->licenseId,
                'value' => $this->value,
            ]
        );
    }
}

