## I.Info
- Author: tony.n
- Date: 28 May 2020

## II.Database

1. nutrients (phân bón)
    - **id**
    - **license_id**
    - **nutrient_name**
    - **default_unit_per_box**: mỗi box có bao nhiêu đơn vị (giá trị default, có thể bị thay đổi mỗi lần nhập)
    - **default_unit_price**: giá tiền của mỗi đơn vị (giá trị default, có thể bị thay đổi mỗi lần nhập)
    - **unit_name**: định nghĩa đơn vị (KG / PC / M ...)
    - **description**: mô tả
    - **status**: 0 (inactive) - 1 (active)
    - **total_quantity**: tổng số unit hiện có (default = 0)
    - **created_at**
    - **updated_at**
2. nutrient_inventories (log lại tình trạng nhập xuất của từng nutrient)
    - **id**
    - **nutrient_id**
    - **in_operation_info**: ở dạng json {"supplier":"Supplier 01","invoice":"INVN00001","no_boxes":"10","units_per_box":20,"total_price":10000, "unit_price": 500.00}
    - **out_operation_info**: ở dạng json \[{"feed_operation_source_id":12,"quantity":10.05},{"feed_operation_source_id":13,"quantity":5}\]
    - **current_quantity**: tổng số unit còn hiện tại = (số box x unit_per_box) - tổng quantity của các lần out
    - **operation_at**
    - **created_at**
    - **updated_at**
3. feed_operations (thông tin cơ bản về hoạt động chăm bón cho cây)
    - **id**
    - **license_id**
    - **feed_at**
    - **nutrient_id**
    - **nutrient_name**
    - **comment**
    - **created_at**
    - **updated_at**
4. feed_operation_sources (thông tin về các loại phân bón sử dụng chăm bón cho cây)
    - **id**
    - **feed_id**
    - **nutrient_inventory_id**: lấy ra từ nguồn nhập phân bón nào
    - **nutrient_quantity**: lấy ra số lượng bao nhiêu unit
    - **nutrient_unit_cost**: giá trị của từng unit là bao nhiêu
    - **nutrient_total_cost**: tổng giá trị của toàn bộ các unit là bao nhiêu
    - **created_at**
    - **updated_at**
5. feed_plants (thông tin về chăm bón cho từng cây)
    - **id**
    - **plant_id**
    - **feed_id**
    - **quantity**: số lượng unit phân bón được bón cho cây này
    - **cost**: số tiền tương ứng với số lượng unit phân bón trên là bao nhiêu
    - **feed_at**
    - **comment**
    - **created_at**
    - **updated_at**

## III.ERD

**feed_operations** 1 --- n **feed_operation_sources**

**feed_operations** 1 --- n **feed_plants**

**nutrients** 1 --- n **nutrient_inventories**

**nutrient_inventories** 1 --- n **feed_operation_sources**

**nutrients** 1 --- n **feed_operations**


## IV.Main flow (Các luồng chính)

1. Tạo phân bón:
    * Thêm record vào bảng nutrients
2. Nhập phân bón
    * Thêm record vào bảng nutrients_inventories
    * Update - Cộng dồn quantity vào bảng nutrients
3. Chăm bón cho 1 hoặc nhiều cây
    * Thêm record vào bảng feed_operations
    * Thêm record(s) vào bảng feed_operation_source
    * Thêm record(s) vào bảng feed_plants
    * Update - thêm json item vào trường out_operation_info và trừ quantity vào trường current_quantity của bảng nutrient_inventories
    * Update - trừ dồn quantity nutrients

## V.Roadmap

- Chuẩn bị

- [x] Tìm hiểu luồng code cũ
- [x] Thiết kế database migration
- [x] Thiết kế model
- [x] Thiết kế relationship trait
- [x] Thiết kế database seeder
- [x] Config phpunit

- Thực thi
 
- [x] Viết unit test cho tạo phân bón
- [x] Code phần tạo phân bón
- [x] Viết unit test cho nhập phân bón
- [x] Code phần nhập phân bón
- [x] Viết unit test cho chăm bón cây
- [x] Code phần chăm bón cây
