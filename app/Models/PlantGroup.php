<?php

namespace App\Models;

use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Models\Traits\{BelongToLicense, Scope\LicenseFilterable, StoreInRoom, WithoutTraceableModelEvents};
use Illuminate\Database\Eloquent\Relations\{BelongsTo, BelongsToMany, HasMany, MorphOne};
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

/**
 * Class PlantGroup
 *
 * @property Propagation propagation
 * @property Batch batch
 * @property Room room
 * @property Strain strain
 * @property Uuid id
 * @property string sync_code
 * @property string sync_status
 * @property string synced_at
 * @package App\Models
 */
class PlantGroup extends BaseModel implements TraceableModelInterface
{
    use BelongToLicense;
    use StoreInRoom;
    use LicenseFilterable;
    use WithoutTraceableModelEvents;

    public const SYNC_ACTION_MOVE_TO_VEGETATION = 'moveToVegetation';
    public const SYNC_ACTION_MOVE_TO_ROOM = 'moveToRoom';
    public const MOVE_PROPAGATION_LOGIC = 'move_propagation_to_plant';
    public const MOVE_ROOM_LOGIC = 'move_room';
    public const EDIT_STRAIN_LOGIC = 'edit_strain';
    public const CHANGE_GROW_CYCLE_LOGIC = 'change_grow_cycle';

    protected $dates = ['synced_at',];

    protected $fillable = [
        'license_id',
        'propagation_id',
        'room_id',
        'strain_id',
        'name',
        'sync_status',
        'synced_at',
    ];

    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function batch(): MorphOne
    {
        return $this->morphOne(Batch::class, 'source');
    }

    public function growCycles(): BelongsToMany
    {
        return $this->belongsToMany(GrowCycle::class);
    }

    public function propagation(): BelongsTo
    {
        return $this->belongsTo(Propagation::class);
    }

    public function strain(): BelongsTo
    {
        return $this->belongsTo(Strain::class);
    }

    public function plants(): HasMany
    {
        return $this->hasMany(Plant::class);
    }

    public function getMappings(): ?array
    {
        // TODO: Implement getMappings() method.
    }

    public function updateSyncStatus(int $status): TraceableModelInterface
    {
        $this->update(['sync_status' => $status, 'synced_at' => Carbon::now(),]);

        return $this;
    }

    public function updateSyncCode(string $code): TraceableModelInterface
    {
        return $this;
    }
}
