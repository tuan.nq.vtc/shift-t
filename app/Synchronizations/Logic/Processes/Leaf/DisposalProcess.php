<?php

namespace App\Synchronizations\Logic\Processes\Leaf;

use App\Helpers\ModelHelper;
use App\Jobs\Synchronizations\SyncData;
use App\Models\Disposal;
use App\Models\Room;
use App\Models\Trace;
use App\Models\TraceableModel;
use App\Services\LicenseService;
use App\Synchronizations\Contracts\TraceableModelInterface;
use App\Synchronizations\Exceptions\Pull\NoImportableDataException;
use App\Synchronizations\Helpers\LeafHelper;
use Bamboo\StreamProcessing\Contracts\StreamableResourceInterface;
use Bamboo\StreamProcessing\Jobs\PublishStreamEventJob;
use Bamboo\StreamProcessing\Models\StreamEvent;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class Propagation
 * @package App\Synchronizations\Logic\Resources\Leaf
 *
 * @property Disposal $traceableModel
 */
class DisposalProcess extends BaseProcess
{
    private const DATETIME_FORMAT = 'm/d/Y G:ia';

    /**
     * @param array $payload
     * @throws Exception
     */
    protected function schedule(): void
    {
        parent::create();
    }

    /**
     * @return void
     * @throws Throwable
     */

    /**
     * @return void
     * @throws Throwable
     */
    protected function fetchAll(): void
    {
        $payload = $this->payload;

        $hasNextImport = true;
        $currentPage = $lastPage = 1;
        if (!empty($payload['current_page'])) {
            $currentPage = (int)$payload['current_page'];
        }
        if (!empty($payload['last_page'])) {
            $lastPage = (int)$payload['last_page'];
        }
        try {
            $reports = $this->importData($payload['data'] ?? []);
        } catch (NoImportableDataException $e) {
            if ($currentPage !== 1) {
                $hasNextImport = false;
            }
            $reports = [];
        } catch (Throwable $e) {
            throw $e;
        }
        $syncResource = LicenseService::getSyncResourceConfigByModel(get_classname($this->traceableModel));

        if ($lastPage === 1) {
            $hasNextImport = false;
        }
        $nextPage = $currentPage === 1 ? $lastPage : $currentPage - 1;
        if ($hasNextImport && $nextPage <= 1) {
            $hasNextImport = false;
        }
        if ($hasNextImport) {
            $children = $this->trace->children;
            $license = $this->traceableModel->license;

            $conditions = [];
            if ($syncResource['filter_by_date'] ?? false && $license->getDateRange()) {
                [$fromDate, $toDate] = $license->getDateRange();
                $conditions = [
                    'from_date' => $fromDate->format('m/d/Y'),
                    'to_date' => $toDate->format('m/d/Y'),
                ];
            }

            // create trace from last page to second page
            $conditions['page'] = $nextPage;
            $nextPageTrace = Trace::create(
                [
                    'method' => Trace::METHOD_PULL,
                    'license_id' => $license->id,
                    'action' => TraceableModel::SYNC_ACTION_FETCH_ALL,
                    'resource_type' => get_classname($this->traceableModel),
                    'status' => Trace::STATUS_PENDING,
                    'resource_conditions' => $conditions,
                ]
            );

            // reassign the children of current trace to next page trace
            $nextPageTrace->children()->attach($children);
            dispatch(new SyncData($nextPageTrace));

            $this->trace->children()->detach();
        }

        // dispatch result after imported
        $messageEvent = new StreamEvent(
            Uuid::uuid4()->toString(),
            array_merge(
                [
                    'license_id' => $this->traceableModel->license->id,
                    'name' => $syncResource['name'] ?? '',
                ],
                $reports
            ),
            [
                'action' => $hasNextImport ? StreamableResourceInterface::ACTION_UPDATE : 'finish',
            ]
        );
        dispatch(
            new PublishStreamEventJob(config('stream.producers.topics.license-sync-aggregate'), $messageEvent)
        );
    }

    /**
     * @param $data
     * @return int[]
     * @throws Exception
     */
    protected function importData($data): array
    {
        $success = $error = 0;
        $licenseId = $this->traceableModel->license->id;

        // prepare data
        $syncCodes = $roomSyncCodes = [];
        $importableData = [];
        foreach ($data as $item) {
            if (!empty($item['global_id'])) {
                $syncCodes[$item['global_id']] = 1;
            }
            if (!empty($item['global_area_id'])) {
                $roomSyncCodes[$item['global_area_id']] = 1;
            }
            $importableData[] = $item;
        }

        /**
         * ['sync_code' => 'id']
         */
        $availableRoomArray = Room::licenseId($licenseId)->whereIn('sync_code', array_keys($roomSyncCodes))
            ->pluck('id', 'sync_code')->toArray();

        /**
         * ['sync_code' => 'id']
         */
        $existingDisposalArray = Disposal::licenseId($licenseId)->whereIn('sync_code', array_keys($syncCodes))
            ->pluck('id', 'sync_code')->toArray();

        $usedRoomIds = [];
        $newDisposalData = [];
        foreach ($importableData as $item) {
            try {
                if (!($globalId = Arr::get($item, 'global_id'))) {
                    throw new Exception('Global ID Not Found');
                }
                if (!empty($existingDisposalArray[$globalId])) {
                    continue; // skip existing record
                }
                if (Arr::get($item, 'deleted_at')) {
                    continue; // skip deleted record
                }
                if (!($source = Arr::get($item, 'source'))) {
                    throw new Exception('Source Not Found');
                }

                $roomId = null;
                if (($roomSyncCode = Arr::get($item, 'global_area_id'))
                    && !empty($availableRoomArray[$roomSyncCode])) {
                    $roomId = $availableRoomArray[$roomSyncCode];
                    $usedRoomIds[$roomId] = 1;
                }

                $today = Carbon::now();
                $createdAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'created_at')
                    ) ?? $today;
                $updatedAt = LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'updated_at')
                    ) ?? $today;
                $destroyedAt = LeafHelper::getDateTimeObject(self::DATETIME_FORMAT, Arr::get($item, 'disposal_at'));
                $newDisposalData[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'license_id' => $licenseId,
                    'internal_id' => ModelHelper::generateInternalId(
                        Disposal::class,
                        $licenseId
                    ),
                    'quantity' => Arr::get($item, 'qty', 0),
                    'reason' => Arr::get($item, 'reason'),
                    'uom' => Arr::get($item, 'uom'),
                    'room_id' => $roomId,
                    'quarantine_start' => LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'hold_starts_at')
                    ),
                    'quarantine_end' => LeafHelper::getDateTimeObject(
                        self::DATETIME_FORMAT,
                        Arr::get($item, 'hold_ends_at')
                    ),
                    'status' => $destroyedAt ? Disposal::STATUS_DESTROYED : Disposal::STATUS_READY,
                    'destroyed_at' => $destroyedAt,
                    'sync_code' => $globalId,
                    'sync_status' => TraceableModelInterface::SYNC_STATUS_SYNCED,
                    'synced_at' => Carbon::now(),
                    'created_at' => $createdAt,
                    'updated_at' => $updatedAt,
                ];
                $success++;
            } catch (Exception $e) {
                $this->logger->error($e);
                $this->logger->info($item);
                $error++;
            }
        }

        try {
            DB::beginTransaction();
            if (!empty($newDisposalData)) {
                Disposal::insert($newDisposalData);
            }

            // enable used rooms
            if (!empty($usedRoomIds)) {
                Room::query()->where('status', Room::STATUS_DISABLED)
                    ->whereIn('id', array_keys($usedRoomIds))
                    ->update(['status' => Room::STATUS_ENABLED]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return ['success' => $success, 'error' => $error];
    }

    /**
     * @throws Exception
     */
    protected function list(): void
    {
        foreach ($this->payload as $item) {
            try {
                $this->processListItem($item);
            } catch (Exception $e) {
                $this->logger->error('Disposal pull - ' . $e->getMessage(), $item);
                $this->logger->error($e);
            }
        }
    }
}
