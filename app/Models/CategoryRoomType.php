<?php

namespace App\Models;

class CategoryRoomType extends BaseModel
{
    protected $table = 'category_room_type';

    protected $fillable = ['category_id', 'room_type_id',];
}
