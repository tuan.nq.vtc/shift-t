<?php

namespace App\Rules;

use App\Models\RoomType;
use App\Services\RoomService;
use Illuminate\Contracts\Validation\Rule;

class IsInventoryRoom implements Rule
{
    private string $licenseId;

    public function __construct($licenseId)
    {
        $this->licenseId = $licenseId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value) {
            return RoomService::checkIsRoomType($value, $this->licenseId, RoomType::CATEGORY_INVENTORY);
        }
    }

    public function message()
    {
        return __('exceptions.inventory.must_inventory_room');
    }
}
