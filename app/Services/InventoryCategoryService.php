<?php

namespace App\Services;

use App\Models\InventoryCategory;
use Illuminate\Database\Eloquent\{Builder, Collection};

/**
 * Class InventoryCategoryService
 * @package App\Services
 *
 * @method InventoryCategory|Collection get($id)
 * @method InventoryCategory|Builder getModel()
 */
class InventoryCategoryService extends BaseService
{
    public function __construct(InventoryCategory $inventoryCategory)
    {
        parent::__construct($inventoryCategory);
    }

    public function index()
    {
        return $this->newQuery()
            ->with(['children' => fn($query) => $query->where('show_on_frontend', true)])
            ->whereNull('parent_id')
            ->get();
    }
}
