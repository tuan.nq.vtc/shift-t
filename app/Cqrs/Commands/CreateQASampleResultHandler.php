<?php

namespace App\Cqrs\Commands;

use App\Cqrs\Base\Handleable;
use App\Models\LabTestAnalysis;
use App\Models\QASample;
use Exception;
use Psr\Log\LoggerInterface;
use Throwable;

class CreateQASampleResultHandler extends Handleable
{
    /**
     * @var QASample
     */
    private $qaSample;

    /**
     * @var LabTestAnalysis
     */
    private $labTestAnalysis;

    /**
     * @param LoggerInterface $logger
     * @param QASample $qaSample
     * @param LabTestAnalysis $labTestAnalysis
     */
    public function __construct(LoggerInterface $logger,
                                QASample        $qaSample,
                                LabTestAnalysis $labTestAnalysis)
    {
        $this->logger = $logger;
        $this->qaSample = $qaSample;
        $this->labTestAnalysis = $labTestAnalysis;
    }

    /**
     * @throws Throwable
     */
    public function handle(CreateQASampleResultCommand $command)
    {
        try {
            $this->ensureCanCreateQASampleResult($command);

            $model =  null;

            // TODO

            return $model;
        } catch (Throwable $e) {
            $this->log($command, $e->getMessage());
            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    private function ensureCanCreateQASampleResult(CreateQASampleResultCommand $command)
    {
        if (!$command->getQaSampleId()) {
            throw new Exception('The QASampleId is required');
        }

        if (!$command->getAnalysisId()) {
            throw new Exception('The AnalysisId is required');
        }

        $qaSample = $this->qaSample->find($command->getQaSampleId());
        if ($qaSample instanceof QASample !== true) {
            throw new Exception(sprintf('The QA Sample \'%s\' not found', $command->getQaSampleId()));
        }

        $analysis = $this->labTestAnalysis->find($command->getAnalysisId());
        if ($analysis instanceof LabTestAnalysis !== true) {
            throw new Exception(sprintf('The Analysis \'%s\' not found', $command->getAnalysisId()));
        }
    }
}
