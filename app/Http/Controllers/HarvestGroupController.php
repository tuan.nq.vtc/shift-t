<?php

namespace App\Http\Controllers;

use App\Events\Harvest\WetWeightConfirmed;
use App\Exceptions\GeneralException;
use App\Models\Harvest;
use App\Models\HarvestGroup;
use App\Models\Plant;
use App\Models\SeedToSale;
use App\Rules\HarvestingRoomRule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use App\Rules\ModelsExist;
use App\Services\{GrowCycleService, HarvestGroupService, HarvestService, PlantService, RoomService, RoomTypeService};
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Class HarvestGroupController
 *
 * @package App\Http\Controllers
 *
 * @property HarvestGroupService $service
 * @property RoomTypeService $roomTypeService
 * @property PlantService $plantService
 */
class HarvestGroupController extends CRUDController
{
    private RoomTypeService $roomTypeService;
    private PlantService $plantService;
    private RoomService $roomService;
    private HarvestService $harvestService;
    /**
     * @var GrowCycleService
     */
    private GrowCycleService $growCycleService;

    public function __construct(HarvestGroupService $service, RoomTypeService $roomTypeService, PlantService $plantService, RoomService $roomService, GrowCycleService $growCycleService, HarvestService $harvestService, Request $request)
    {
        $this->plantService = $plantService;
        $this->roomTypeService = $roomTypeService;
        $this->roomService = $roomService;
        $this->harvestService = $harvestService;
        $this->growCycleService = $growCycleService;
        parent::__construct($service, $request);
    }

    /**
     * @return array
     */
    public function getCreateRules(): array
    {
        return [];
    }

    /**
     * @param string $id
     *
     * @return array
     */
    public function getUpdateRules(string $id): array
    {
        $licenseId = $this->request->get('license_id');
        return [
            'room_id' => [
                'sometimes',
                new ModelsExist(
                    'App\Models\Room',
                    'id',
                    [
                        'license_id' => $licenseId,
                        'room_type_id' => $this->roomTypeService->getNotDestructionRoomType()->pluck(
                            'id'
                        )->toArray()
                    ]
                ),
            ],
            'start_at' => ['date_format:Y-m-d', 'required_with:est_end_at'],
            'est_end_at' => ['date_format:Y-m-d', 'required_with:start_at', 'after_or_equal:start_at',],
            'name' => [
                'sometimes',
                'string',
                'min:3',
                'max:150',
                'unique:App\Models\HarvestGroup,name,' . $id . ',id,license_id,' . $this->request->get('license_id')]
        ];
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function show(string $id)
    {
        return response()->json($this->service->get($id));
    }

    /**
     * @param string|null $id
     * @param string $source
     * @return JsonResponse
     * @throws GeneralException
     * @throws Throwable
     * @throws ValidationException
     */
    public function addPlants(string $id = null, string $source = ''): JsonResponse
    {
        $licenseId = $this->request->get('license_id');

        try {
            DB::beginTransaction();
            if ($id) {
                $harvestGroup = $this->service->get($id);
            } else {
                /** @var HarvestGroup $harvestGroup */
                $harvestGroup = $this->service->create(
                    $this->validate(
                        $this->request,
                        [
                            'name' => [
                                'required',
                                'string',
                                'min:3',
                                'max:150',
                                'unique:App\Models\HarvestGroup,name,null,id,license_id,' . $licenseId,
                            ],
                            'room_id' => [
                                'required',
                                'exists:App\Models\Room,id,license_id,' . $licenseId . ',sync_code,NOT_NULL,deleted_at,NULL',
                            ],
                            'type' => ['required', new In(HarvestGroup::TYPES)],
                            'start_at' => ['date_format:Y-m-d', 'required'],
                            'est_end_at' => ['date_format:Y-m-d', 'required', 'after_or_equal:start_at'],
                        ],
                        [],
                        ['room_id' => 'room']
                    )
                );
            }
            switch ($source) {
                case 'room':
                    $this->validate(
                        $this->request,
                        [
                            'harvested_room_id' => [
                                'required',
                                'exists:App\Models\Room,id,license_id,' . $licenseId . ',sync_code,NOT_NULL,deleted_at,NULL',
                            ],
                        ]
                    );

                    $room = $this->roomService->get($this->request->get('harvested_room_id'));
                    $harvestGroup = $this->service->harvestPlants($harvestGroup, $room->plants);
                    break;
                case 'plants':
                    $this->validate(
                        $this->request,
                        [
                            'plant_ids' => [
                                'required',
                                new ModelsExist(
                                    Plant::class,
                                    'id',
                                    [
                                        'license_id' => $licenseId,
                                        'destroyed' => false,
                                        'sync_code' => function (Builder $query) {
                                            return $query->whereNotNull('sync_code');
                                        },
                                        'grow_status' => function (Builder $query) {
                                            return $query->whereIn(
                                                'grow_status',
                                                [
                                                    SeedToSale::GROW_STATUS_VEGETATIVE,
                                                    SeedToSale::GROW_STATUS_FLOWERING,
                                                ]
                                            );
                                        }
                                    ]
                                ),
                            ],
                        ]
                    );

                    $plants = $this->plantService->getEnumerable($this->request->get('plant_ids'));
                    $harvestGroup = $this->service->harvestPlants($harvestGroup, $plants);
                    break;
                case 'grow-cycle':
                    $this->validate($this->request, ['grow_cycle_id' => ['required',],]);

                    $growCycle = $this->growCycleService->get($this->request->get('grow_cycle_id'));
                    $harvestGroup = $this->service->harvestPlants($harvestGroup, $growCycle->plants);
                    break;
                default:
                    throw new GeneralException('Invalid Source');
            }
            DB::commit();
            return response()->json($harvestGroup);
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws ValidationException|GeneralException
     */
    public function removePlants(string $id): JsonResponse
    {
        $licenseId = $this->request->get('license_id');
        $this->validate(
            $this->request,
            [
                'plant_ids' => [
                    'required',
                    new ModelsExist(
                        Plant::class,
                        'id',
                        [
                            'license_id' => $licenseId,
                            'destroyed' => false,
                            'sync_code' => function (Builder $query) {
                                return $query->whereNotNull('sync_code');
                            }
                        ]
                    ),
                ],
            ]
        );
        $plants = $this->plantService->getEnumerable($this->request->get('plant_ids'));
        return response()->json($this->service->removePlants($id, $plants));
    }

    /**
     * @param string $id
     * @param string $phase
     * @return JsonResponse
     * @throws GeneralException
     * @throws ValidationException
     */
    public function confirm(string $id, string $phase): JsonResponse
    {
        $harvestGroup = $this->service->get($id);
        switch ($phase) {
            case 'wet':
                try {
                    DB::beginTransaction();
                    if ($harvestGroup->status !== HarvestGroup::STATUS_OPEN) {
                        throw new GeneralException(__('exceptions.harvest.action_validation', ['status' => 'open']));
                    }
                    switch ($harvestGroup->type) {
                        case HarvestGroup::TYPE_EXTRACTION:
                            $harvestGroup = $this->finalizeHarvest($harvestGroup);
                            break;
                        case HarvestGroup::TYPE_DRY:
                            $harvestGroup->update(['status' => HarvestGroup::STATUS_WET_CONFIRMED]);

                            // dispatch wet confirmed event
                            Event::dispatch(new WetWeightConfirmed($harvestGroup));
                            break;
                    }

                    $this->service->updateGrowStatus($harvestGroup);
                    DB::commit();
                } catch (Throwable $e) {
                    DB::rollBack();
                    throw $e;
                }
                break;
            case 'dry':
                if ($harvestGroup->type !== HarvestGroup::TYPE_DRY
                    || $harvestGroup->status !== HarvestGroup::STATUS_WET_CONFIRMED) {
                    throw new GeneralException(
                        __('exceptions.harvest.action_validation', ['status' => 'wet confirmed'])
                    );
                }
                $harvestGroup = $this->finalizeHarvest($harvestGroup);
                break;
            default:
                throw new GeneralException('Invalid Source');
        }

        return response()->json($harvestGroup, Response::HTTP_ACCEPTED);
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws GeneralException
     * @throws Throwable
     * @throws ValidationException
     */
    public function finalize(string $id): JsonResponse
    {
        $harvestGroup = $this->service->get($id);
        if ($harvestGroup->type !== HarvestGroup::TYPE_DRY
            || $harvestGroup->status !== HarvestGroup::STATUS_DRY_CONFIRMED) {
            throw new GeneralException(
                __('exceptions.harvest.action_validation', ['status' => 'dry confirmed'])
            );
        }

        return response()->json($this->finalizeHarvest($harvestGroup), Response::HTTP_ACCEPTED);
    }

    /**
     * @param HarvestGroup $harvestGroup
     * @return HarvestGroup
     * @throws ValidationException|Throwable
     */
    private function finalizeHarvest(HarvestGroup $harvestGroup)
    {
        $harvestRequest = $this->request->get('harvests', []);
        $licenseId = $this->request->get('license_id');
        $validateRoomCallback = function ($attribute, $value, $fail) use ($harvestRequest, $harvestGroup) {
            HarvestingRoomRule::requireif($harvestGroup, $harvestRequest, $attribute, $value, $fail);
        };
        $this->validate(
            $this->request,
            [
                'harvests' => ['array'],
                'harvests.*.strain_id' => [
                    'required',
                    'exists:App\Models\Strain,id,license_id,' . $licenseId . ',sync_code,NOT_NULL,deleted_at,NULL',
                ],
                'harvests.*.flower_room_id' => $validateRoomCallback,
                'harvests.*.material_room_id' => $validateRoomCallback,
                'harvests.*.waste_room_id' => $validateRoomCallback,
            ],
            [],
            [
                'harvests.*.strain_id' => 'strain',
                'harvests.*.flower_room_id' => 'flower room',
                'harvests.*.material_room_id' => 'material room',
                'harvests.*.waste_room_id' => 'waste room'
            ]
        );
        return $this->service->finalize($harvestGroup, $this->request->get('harvests', []), $this->request->get('user_id'));
    }

    /**
     * @param string $harvestId
     * @return JsonResponse
     * @throws GeneralException
     * @throws ValidationException
     */
    public function updateDryWeights(string $harvestId): JsonResponse
    {
        return response()->json(
            $this->harvestService->updateDryWeights(
                $harvestId,
                $this->validate(
                    $this->request,
                    [
                        'flower_dry_weight' => ['sometimes', 'numeric', 'gte:0'],
                        'material_dry_weight' => ['sometimes', 'numeric', 'gte:0'],
                        'waste_dry_weight' => ['sometimes', 'numeric', 'gte:0'],
                    ]
                )
            )
        );
    }
}
