<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RoomStatistic extends Model
{
    protected $fillable = [
        'room_id',
        'stats'
    ];

    protected $casts = [
        'stats' => 'array'
    ];

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }
}
