<?php


namespace App\Http\Controllers;


use App\Exceptions\GeneralException;
use App\Services\{AdditiveLogService, PlantService, PropagationService};
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class AdditiveLogController extends Controller
{
    /**
     * @var AdditiveLogService
     */
    private AdditiveLogService $additiveLogService;
    private PlantService $plantService;
    private PropagationService $propagationService;

    public function __construct(
        AdditiveLogService $additiveLogService,
        PlantService $plantService,
        PropagationService $propagationService
    ) {
        $this->additiveLogService = $additiveLogService;
        $this->plantService = $plantService;
        $this->propagationService = $propagationService;
    }

    /**
     * @param Request $request
     * @param string $target
     * @param $targetId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function listBaseOnTarget(Request $request, string $target, $targetId)
    {
        $rules = [];
        $licenseId = $request->get('license_id');
        $request->merge(['target_id' => $targetId]);
        switch ($target) {
            case 'plant':
                $rules['target_id'] = ['exists:App\Models\Plant,id,license_id,'.$licenseId.',deleted_at,NULL',];
                break;
            case 'propagation':
                $rules['target_id'] = ['exists:App\Models\Propagation,id,license_id,'.$licenseId.',deleted_at,NULL',];
                break;
            default:
                throw new GeneralException(__('exceptions.additive_log.target_invalid', ['target' => $target]));
        }
        $this->validate($request, $rules);
        return response()->json($this->additiveLogService->getLogs($target, $targetId));
    }
}
