<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Facades\Storage;

class QASampleCoa extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'qa_sample_coa';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public string $orderByColumn = 'uploaded_at';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'filename',
        'qa_sample_id',
        'original_filename'
    ];

    protected $appends = [
        'file_url',
    ];

    public function getFileUrlAttribute(): string
    {
        if ($this->filename) {
            try {
                return Storage::temporaryUrl($this->filename, (new \DateTime())->modify('+ 1 hour'));
            } catch (Exception $e) {
                // ignore it
            }
        }

        return '';
    }
}
