<?php

use App\Models\{GrowCycle, License, SeedToSale};
use Illuminate\Database\Seeder;

/**
 * Class GrowthCycleTableSeeder
 */
class GrowCycleTableSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {
        License::isTest()->where('account_holder_id', 'f136252a-adfd-42a9-96a1-4ece04730600')->get()->each(fn(License $license) => $this->iterateLicense($license));
    }

    private function iterateLicense(License $license): void
    {
        foreach (SeedToSale::GROW_CYCLE_GROW_STATUSES as $growStatus) {
            factory(GrowCycle::class, 5)->create(
                [
                    'name' => "Golden {$license->id} " . rand(0, 999),
                    'license_id' => $license->id,
                    'grow_status' => $growStatus,
                ]
            );
        }
    }
}
