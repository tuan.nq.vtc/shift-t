<?php

namespace App\Console\Commands\TraceableResource;

use App\Facades\Portal;
use App\Models\Trace;
use App\Synchronizations\Synchronizer;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

class SyncCommand extends Command
{
    /**
     * @var string
     */
    protected $description = 'Sync all traceable resources to state API';
    /**
     * @var string
     */
    protected $signature = 'trace:sync {--state= : The state resource(s) will be synced}
                {--resource= : The resource type [Strain, Location, Propagation, Plant, ...] will be synced}';

    public function handle(Synchronizer $synchronizer)
    {
        $stateCode = $this->getStateCode();
        $resourceType = $this->getResourceType();

        $licenseIds = null;
        if ($stateCode) {
            $licenseIds = Portal::fetchAllLicenses()->filter(
                fn($license) => $license->state_code == $stateCode
            )->pluck('id');
        }

        /** @var Trace|Builder $builder */
        $builder = Trace::with('license');
        if ($licenseIds) {
            $builder->whereIn('license_id', $licenseIds);
        }
        if ($resourceType) {
            $builder->whereResourceType("App\Models\\{$resourceType}");
        }

        $total = $builder->count();
        $this->validate($total, $stateCode, $resourceType);

        $pageNo = 1;
        $limit = 50;

        while (true) {
            /** @var Collection $collection */
            $offset = ($pageNo - 1) * $limit;
            $collection = $builder->limit($limit)->offset($offset)->get();
            $collection->each(
                function (Trace $trace) use ($synchronizer) {
                    $resource = get_classname($trace->resource_type, true);
                    switch ($trace->action) {
                        case Trace::ACTION_CREATE:
                            $action = "creation";
                            break;
                        case Trace::ACTION_DELETE:
                            $action = "deletion";
                            break;
                        case Trace::ACTION_UPDATE:
                            $action = "update";
                            break;
                        default:
                            $action = '';
                    }

                    if ($action) {
                        $this->line(
                            "Syncing {$resource} [{$trace->resource_id}] {$action} to [{$trace->license->state_code}] API"
                        );
                        try {
                            $synchronizer->push($trace);
                            $this->line(" Synced");
                        } catch (Throwable $throwable) {
                            Log::error($throwable->getMessage());
                            $this->error(" Failed: {$throwable->getMessage()}");
                        }
                    }
                }
            );

            if ($offset >= $total) {
                break;
            }

            $pageNo++;
        }
    }

    protected function getStateCode(): ?string
    {
        if ($state = $this->option('state')) {
            return Str::upper($state);
        }

        return null;
    }

    protected function getResourceType(): ?string
    {
        if ($resource = $this->option('resource')) {
            return Str::ucfirst($resource);
        }

        return null;
    }

    protected function validate($total, $stateCode, $resource): void
    {
        if (!$total) {
            if (isset($stateCode) && isset($resource)) {
                $this->error("There no [{$resource}] resource belongs to state [{$stateCode}]");
                exit();
            }
            if (isset($stateCode)) {
                $this->error(
                    "Invalid state [{$stateCode}] or there is no traceable resource belongs to state [{$stateCode}]"
                );
                exit();
            }
            if (isset($resource)) {
                $this->error(
                    "Invalid traceable resource [{$resource}] or there is no traceable resource [{$resource}] exists!"
                );
                exit();
            }
            $this->error("There is no traceable resource need to be synced");
            exit();
        }
    }
}
