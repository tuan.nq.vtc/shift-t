<?php

namespace App\Integrations\FileGenerators\Contracts;

use App\Models\License;

interface DataProviderInterface
{
    public function __construct(License $license, string $resourceClassName);

    public function getData(): array;

    public function setMaxRecordsPerFile(int $maxRecordsPerFile): void;

    public function setRecordsInProcess(): void;

    public function setRecordsCompleted(string $fileReference = ''): void;

    public function setRecordsFailed(): void;

    public function prepareData(): void;

    public static function getFilesToUploadByLicense(License $license, string $resourceClassName): array;

    public static function setFileUploadSuccess(string $fileReference, string $newFileReference): bool;

    public static function setFileUploadError(string $fileReference, string $newFileReference): bool;

    public static function setFileUploadInProcess(string $fileReference): bool;
}
