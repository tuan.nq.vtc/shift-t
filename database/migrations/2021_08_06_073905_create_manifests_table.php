<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManifestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifests', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('license_id');
            $table->string('method', 32)->index();
            $table->string('type', 32)->index();
            $table->uuid('client_id')->nullable()->index();
            $table->string('transport_type', 32)->index()->nullable();
            $table->unsignedSmallInteger('status')->index()->default(0);
            $table->uuid('transporter_id')->nullable()->index();
            $table->decimal('invoice_total', 12, 2)->default(0);
            $table->timestamp('published_at')->nullable()->index();
            $table->date('est_departure_date')->nullable();
            $table->date('est_arrival_date')->nullable();
            $table->uuid('driver_id')->nullable();
            $table->uuid('driver_helper_id')->nullable();
            $table->uuid('vehicle_id')->nullable();
            $table->longText('travel_route')->nullable();
            $table->string('sync_code')->nullable()->index();
            $table->unsignedTinyInteger('sync_status')->default(0);
            $table->timestamp('synced_at')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')->references('id')->on('licensees')->nullOnDelete();
            $table->foreign('driver_id')->references('id')->on('drivers')->nullOnDelete();
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manifests');
    }
}
