<?php

/** @var Factory $factory */

use App\Models\{User, Room, WasteBag};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
|   Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    WasteBag::class,
    function (Faker $faker) {
        return [
            'license_id' => $faker->uuid,
            'cut_by_id' => function () {
                return factory(User::class)->create()->uuid;
            },
            'room_id' => function ($data) {
                return factory(Room::class)->create(['license_id' => $data['license_id']])->id;
            },
            'reason' => $faker->randomElement(WasteBag::REASONS),
            'weight' => $faker->randomFloat(),
        ];
    }
);
